<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  ============================================================
  Created by Leon Dijk
  - http://gwcode.com/
  ------------------------------------------------------------
  This plugin is licensed under The BSD 3-Clause License.
  - http://www.opensource.org/licenses/bsd-3-clause
  ============================================================
 */

$plugin_info = array(
    'pi_name' => 'GWcode CatMenu (CUSTOMISED)',
    'pi_version' => '2.0.0 XXX',
    'pi_author' => 'Leon Dijk | James McFall',
    'pi_author_url' => 'http://gwcode.com/add-ons/gwcode-catmenu',
    'pi_description' => 'Create flexible, dynamic, multi-level category based menus.',
    'pi_usage' => Gwcode_catmenu::usage()
);

class Gwcode_catmenu {

    public $return_data = '';
    private $tagdata = '';
    private $active_categories = array(); // Contains categories that should be marked as active
    private $group_id_arr = array(); // contains category group IDs that we need to get categories from to create our menu
    private $group_ids = ''; // comma separated string of group id's to be used in sql queries.
    private $channel_id_arr = array(); // channel id's to be used in sql queries.
    private $categories = array(); // contains all category data
    private $custom_fields_arr = array(); // array with custom category field id's and names. key = group_id; value = array
    private $min_depth_in_output = 1; // this will store the lowest depth number in the final output.
    private $active_branch_root_cat_ids = array(); // array which contains active root category id's (if a category with depth 3 for example is marked as active, the root category for that branch will be stored).

    public function Gwcode_catmenu() {
        $this->__construct();
    }

    public function __construct() {
        $this->EE = & get_instance();

        // start: fetch & validate plugin params
        $this->site_id = $this->EE->TMPL->fetch_param('site_id', 1);
        $this->site_ids = str_replace('|', ',', $this->site_id);
        if (!ctype_digit(str_replace('|', '', $this->site_ids))) {
            $this->EE->TMPL->log_item('Error: the "site_id" parameter value needs to contain a valid number or valid numbers seperated by pipe characters.');
            return;
        }
        $this->channel = $this->EE->TMPL->fetch_param('channel');
        $this->channel_id = $this->EE->TMPL->fetch_param('channel_id');
        if (!empty($this->channel_id) && !ctype_digit(str_replace('|', '', $this->channel_id))) {
            $this->EE->TMPL->log_item('Error: the "channel_id" parameter value needs to contain a valid number or valid numbers seperated by pipe characters.');
            return;
        }
        $this->group_id = $this->EE->TMPL->fetch_param('group_id');
        if (!empty($this->group_id) && !ctype_digit(str_replace('|', '', $this->group_id))) {
            $this->EE->TMPL->log_item('Error: the "group_id" parameter value needs to contain a valid number or valid numbers seperated by pipe characters.');
            return;
        }
        /**
         * For the Synergy website some products/services are specific to 
         * countries (NZ/AU) which is a custom field against each. This means
         * the "empty" functionality for this plugin wont work. This "for country" 
         * field is a work around.
         * 
         * @author James McFall <james@96black.co.nz>
         */
        $this->synergy_for_country = $this->EE->TMPL->fetch_param('country');
                
        $this->cat_id = $this->EE->TMPL->fetch_param('cat_id');
        if (!empty($this->cat_id) && !ctype_digit(str_replace('|', '', $this->cat_id))) {
            $this->EE->TMPL->log_item('Error: the "cat_id" parameter value needs to contain a valid number or valid numbers seperated by pipe characters.');
            return;
        }
        $this->cat_url_title = $this->EE->TMPL->fetch_param('cat_url_title');
        $this->entry_id = $this->EE->TMPL->fetch_param('entry_id');
        if (!empty($this->entry_id) && !ctype_digit($this->entry_id)) {
            $this->EE->TMPL->log_item('Error: the "entry_id" parameter value needs to contain a valid number and you can only provide a single entry ID.');
            return;
        }
        $this->url_title = $this->EE->TMPL->fetch_param('url_title');
        $this->depth_open = $this->EE->TMPL->fetch_param('depth_open'); // open categories with this depth, in active or non-active top categories
        if (!empty($this->depth_open) && !ctype_digit(str_replace('|', '', $this->depth_open))) {
            $this->EE->TMPL->log_item('Error: the "depth_open" parameter value needs to contain a valid number or valid numbers seperated by pipe characters.');
            return;
        }
        $this->active_depth_open = $this->EE->TMPL->fetch_param('active_depth_open'); // open categories with this depth for active top categories only
        if (!empty($this->active_depth_open) && !ctype_digit(str_replace('|', '', $this->active_depth_open))) {
            $this->EE->TMPL->log_item('Error: the "active_depth_open" parameter value needs to contain a valid number or valid numbers seperated by pipe characters.');
            return;
        }
        $this->id = $this->EE->TMPL->fetch_param('id');
        $this->class = $this->EE->TMPL->fetch_param('class');
        $this->class_open = $this->EE->TMPL->fetch_param('class_open', 'open');
        $this->class_active = $this->EE->TMPL->fetch_param('class_active', 'active');
        $this->class_depth = $this->EE->TMPL->fetch_param('class_depth', 'depth');
        $this->list_type = strtolower($this->EE->TMPL->fetch_param('list_type', 'ul'));
        if ($this->list_type != 'ul' && $this->list_type != 'ol') {
            $this->EE->TMPL->log_item('Error: the "list_type" parameter value needs to be either "ul" or "ol".');
            return;
        }
        $this->entry_count = strtolower($this->EE->TMPL->fetch_param('entry_count', 'no'));
        if ($this->entry_count != 'yes' && $this->entry_count != 'no') {
            $this->EE->TMPL->log_item('Error: the "entry_count" parameter value needs to be either "yes" or "no".');
            return;
        }
        $this->status = $this->EE->TMPL->fetch_param('status', 'Open');
        $this->style = strtolower($this->EE->TMPL->fetch_param('style', 'nested'));
        if ($this->style != 'nested' && $this->style != 'linear' && $this->style != 'simple') {
            $this->EE->TMPL->log_item('Error: the "style" parameter value needs to be either "nested", "linear" or "simple".');
            return;
        }
        $this->backspace = $this->EE->TMPL->fetch_param('backspace');
        if (!empty($this->backspace) && !ctype_digit($this->backspace)) {
            $this->EE->TMPL->log_item('Error: the "backspace" parameter value needs to contain a valid number.');
            return;
        }
        if (!empty($this->backspace) && $this->style != 'linear') {
            $this->EE->TMPL->log_item('Warning: the "backspace" parameter can only be used in combination with style="linear".');
        }
        $this->show_empty = strtolower($this->EE->TMPL->fetch_param('show_empty', 'yes'));
        if ($this->show_empty != 'yes' && $this->show_empty != 'no') {
            $this->EE->TMPL->log_item('Error: the "show_empty" parameter value needs to be either "yes" or "no".');
            return;
        }
        if ($this->show_empty == 'no' && $this->status == '' && $this->entry_count == 'no') { // user didn't provide entry_count or status parameter, which we need
            $this->entry_count = 'yes';
        }
        $this->excl_cat_id = $this->EE->TMPL->fetch_param('excl_cat_id');
        if (!empty($this->excl_cat_id) && !ctype_digit(str_replace('|', '', $this->excl_cat_id))) {
            $this->EE->TMPL->log_item('Error: the "excl_cat_id" parameter value needs to contain a valid number or valid numbers seperated by pipe characters.');
            return;
        }
        $this->add_class_url_title = strtolower($this->EE->TMPL->fetch_param('add_class_url_title', 'no'));
        if ($this->add_class_url_title != 'yes' && $this->add_class_url_title != 'no') {
            $this->EE->TMPL->log_item('Error: the "add_class_url_title" parameter value needs to be either "yes" or "no".');
            return;
        }
        $this->id_group_heading = $this->EE->TMPL->fetch_param('id_group_heading', 'group');
        $this->class_group_heading = $this->EE->TMPL->fetch_param('class_group_heading', 'group-heading');
        $this->excl_group_id = $this->EE->TMPL->fetch_param('excl_group_id');
        if (!empty($this->excl_group_id) && !ctype_digit(str_replace('|', '', $this->excl_group_id))) {
            $this->EE->TMPL->log_item('Error: the "excl_group_id" parameter value needs to contain a valid number or valid numbers seperated by pipe characters.');
            return;
        }
        $this->class_entry_count = $this->EE->TMPL->fetch_param('class_entry_count');
        if (!empty($this->class_entry_count)) {
            $extraValid = array('-', '_');
            if (!ctype_alnum(str_replace($extraValid, '', $this->class_entry_count))) {
                $this->EE->TMPL->log_item('Error: you can only use letters, numbers, dashes and/or underscores for the "class_entry_count" parameter value.');
                return;
            }
        }
        $this->class_current = $this->EE->TMPL->fetch_param('class_current', 'current');
        $this->custom_fields = strtolower($this->EE->TMPL->fetch_param('custom_fields', 'no'));
        if ($this->custom_fields != 'yes' && $this->custom_fields != 'no') {
            $this->EE->TMPL->log_item('Error: the "custom_fields" parameter value needs to be either "yes" or "no".');
            return;
        }
        $this->min_depth = $this->EE->TMPL->fetch_param('min_depth');
        if (!empty($this->min_depth) && !is_numeric($this->min_depth)) {
            $this->EE->TMPL->log_item('Error: the "min_depth" parameter value needs to be numeric.');
            return;
        }
        if (!empty($this->min_depth)) {
            $this->min_depth_in_output = $this->min_depth;
        }
        $this->max_depth = $this->EE->TMPL->fetch_param('max_depth');
        if (!empty($this->max_depth) && !is_numeric($this->max_depth)) {
            $this->EE->TMPL->log_item('Error: the "max_depth" parameter value needs to be numeric.');
            return;
        }
        $this->var_prefix = $this->EE->TMPL->fetch_param('variable_prefix', '');
        $this->active_branch_only = strtolower($this->EE->TMPL->fetch_param('active_branch_only', 'no'));
        if ($this->active_branch_only != 'yes' && $this->active_branch_only != 'no') {
            $this->EE->TMPL->log_item('Error: the "active_branch_only" parameter value needs to be either "yes" or "no".');
            return;
        }
        // end: fetch & validate plugin params

        $this->tagdata = $this->EE->TMPL->tagdata;
        if (trim($this->tagdata) == '') { // use default tagdata
            $this->tagdata = '{' . $this->var_prefix . 'cat_name}';
        }

        // get all category group IDs we need and determine which categories should be active
        if (!empty($this->group_id)) { // add category group IDs provided with the group_id parameter
            $this->group_id_arr = explode('|', $this->group_id);
        }
        if (!empty($this->channel_id)) { // add category group IDs for one or more channels provided with the channel_id parameter
            $this->_process_param_channel('id');
        }
        if (!empty($this->channel)) { // add category group IDs for one or more channels provided with the channel parameter
            $this->_process_param_channel('short_name');
        }
        if (!empty($this->cat_id)) { // add category group IDs for one or more categories provided with the cat_id parameter and mark the categories as active
            $this->_process_param_category('id');
        }
        if (!empty($this->cat_url_title)) { // add category group IDs for one or more categories provided with the cat_url_title parameter and mark the categories as active
            $this->_process_param_category('cat_url_title');
        }
        if (ctype_digit($this->entry_id)) { // add category group IDs for an entry provided with the entry_id parameter and mark the categories the entry has been posted in as active
            $this->_process_param_entry('id');
        }
        if (!empty($this->url_title)) { // add category group IDs for an entry provided with the url_title parameter and mark the categories the entry has been posted in as active
            $this->_process_param_entry('url_title');
        }

        if (!empty($this->excl_group_id)) {
            // remove category groups that have been provided with the excl_group_id parameter
            $this->_exclude_category_groups();
        }

        if (empty($this->group_id_arr)) {
            $this->return_data = $this->EE->TMPL->no_results();
            return;
        }

        $this->group_ids = implode(',', $this->group_id_arr);

        $this->group_id_arr = array(); // change group_id_arr into an array with group_id as key and array with group_name and field_html_formatting as values
        $sql = 'SELECT group_id, group_name, field_html_formatting FROM exp_category_groups WHERE site_id IN (' . $this->EE->db->escape_str($this->site_ids) . ') AND group_id IN (' . $this->EE->db->escape_str($this->group_ids) . ')';
        $gcm_result = $this->EE->db->query($sql);
        if ($gcm_result->num_rows() > 0) {
            foreach ($gcm_result->result_array() as $row) {
                $this->group_id_arr[$row['group_id']] = array('group_name' => $row['group_name'], 'field_html_formatting' => $row['field_html_formatting']);
            }
        }

        // get all categories for the category groups we've collected
        $this->_get_categories();

        // remove categories and potential subcategories that have been provided with the excl_cat_id parameter
        if (!empty($this->excl_cat_id)) {
            $this->_exclude_categories();
        }

        // remove categories based on the min/max depth params
        if (!empty($this->min_depth) || !empty($this->max_depth)) {
            $this->_remove_categories_by_depth();
        }

        // add open, active and total_entry_count values to categories
        $this->_add_extra_values();

        // remove categories which are not in an active root branch
        if ($this->active_branch_only == 'yes') {
            $this->_remove_inactive_branches();
        }

        // remove subcategories from output for categories that are not open
        $this->_remove_subcategories_not_open();

        if ($this->show_empty == 'no') {
            // remove categories that have no entries, but only if none of its sub (or subsub etc) categories have entries either
            $this->_remove_empty_categories();
        }

        // create menu
        $this->_generate_output();
    }

// end function __construct

    private function _process_param_channel($type) {
        /*
          Get categories for channel by id or channel short name.
          Fill $this->channel_id_arr which we need for _get_categories()
         */
        if ($type == 'id') {
            $channel_ids = str_replace('|', ',', $this->channel_id);
            $sql = 'SELECT channel_id, cat_group FROM exp_channels WHERE site_id IN(' . $this->EE->db->escape_str($this->site_ids) . ') AND channel_id IN (' . $this->EE->db->escape_str($channel_ids) . ')';
        } else {
            $channels = $this->EE->db->escape_str($this->channel);
            $channels = str_replace('|', "','", $channels);
            $sql = 'SELECT channel_id, cat_group FROM exp_channels WHERE site_id IN(' . $this->EE->db->escape_str($this->site_ids) . ") AND channel_name IN ('" . $channels . "')";
        }
        $gcm_result = $this->EE->db->query($sql);
        if ($gcm_result->num_rows() > 0) {
            foreach ($gcm_result->result_array() as $row) {
                if (!in_array($row['channel_id'], $this->channel_id_arr)) {
                    $this->channel_id_arr[] = $row['channel_id'];
                }
                $cat_group_arr_exploded = explode('|', $row['cat_group']); // multiple category groups can be assigned to a channel
                foreach ($cat_group_arr_exploded as $key => $value) {
                    if (!in_array($value, $this->group_id_arr)) {
                        $this->group_id_arr[] = $value;
                    }
                }
            }
        }
    }

// end function _process_param_channel

    private function _process_param_category($type) {
        if ($type == 'id') { // get categories by id
            $cat_ids = str_replace('|', ',', $this->cat_id);
            $sql = 'SELECT group_id, cat_id, parent_id FROM exp_categories WHERE site_id IN(' . $this->EE->db->escape_str($this->site_ids) . ') AND cat_id IN (' . $this->EE->db->escape_str($cat_ids) . ')';
        } else { // get categories by cat_url_title
            $group_ids = implode(',', $this->group_id_arr);
            $cat_url_title = $this->EE->db->escape_str($this->cat_url_title);
            $cat_url_title = str_replace('|', "','", $cat_url_title);
            if (!empty($group_ids)) { // add the group ID's we've already found (if any) because category url titles don't have to be unique
                $sql = 'SELECT group_id, cat_id, parent_id FROM exp_categories WHERE site_id IN(' . $this->EE->db->escape_str($this->site_ids) . ') AND group_id IN(' . $this->EE->db->escape_str($group_ids) . ") AND cat_url_title IN ('" . $cat_url_title . "')";
            } else {
                $sql = 'SELECT group_id, cat_id, parent_id FROM exp_categories WHERE site_id IN(' . $this->EE->db->escape_str($this->site_ids) . ") AND cat_url_title IN ('" . $cat_url_title . "')";
            }
        }
        $gcm_result = $this->EE->db->query($sql);
        if ($gcm_result->num_rows() > 0) {
            foreach ($gcm_result->result_array() as $row) {
                if (!in_array($row['group_id'], $this->group_id_arr)) {
                    $this->group_id_arr[] = $row['group_id'];
                }
                $this->active_categories[] = $row['cat_id'];
            }
        }
    }

// end function _process_param_category

    private function _process_param_entry($type) {
        if ($type == 'id') { // get all categories for this entry by entry_id
            $sql = 'SELECT group_id, c.cat_id, parent_id FROM exp_category_posts cp, exp_categories c WHERE c.cat_id=cp.cat_id AND site_id IN(' . $this->EE->db->escape_str($this->site_ids) . ') AND entry_id=' . $this->EE->db->escape_str($this->entry_id);
        } else { // get all categories for this entry by url_title
            $sql = 'SELECT group_id, c.cat_id, parent_id FROM exp_category_posts cp, exp_categories c, exp_channel_titles wt WHERE c.cat_id=cp.cat_id AND cp.entry_id=wt.entry_id AND c.site_id=wt.site_id AND c.site_id IN(' . $this->EE->db->escape_str($this->site_ids) . ") AND url_title='" . $this->EE->db->escape_str($this->url_title) . "'";
        }
        $gcm_result = $this->EE->db->query($sql);
        if ($gcm_result->num_rows() > 0) {
            foreach ($gcm_result->result_array() as $row) {
                if (!in_array($row['group_id'], $this->group_id_arr)) {
                    $this->group_id_arr[] = $row['group_id'];
                }
                $this->active_categories[] = $row['cat_id'];
            }
        }
    }

// end function _process_param_entry

    private function _get_categories() {
        $cat_array = array();

        if ($this->custom_fields == 'yes') {
            // load typography library and grab info for custom category fields. Makes $this->custom_fields_arr available for use
            $this->_get_custom_fields();
        }

        if ($this->EE->TMPL->fetch_param('status') != '' || $this->entry_count == 'yes') { // advanced query, we need to look in the exp_category_posts and exp_channel_titles tables to count the number of entries
            $status = $this->EE->db->escape_str($this->status);
            $status = str_replace('|', "','", $status);
            $channel_sql = (!empty($this->channel_id_arr)) ? ' AND ct.channel_id IN (' . implode(',', $this->channel_id_arr) . ')' : ''; // if the channel_id or channel parameter has been used, we need to add it in the sql query to count entries for those channels only. Thanks @Iain
            if ($this->custom_fields == 'no') {
                $sql = 'SELECT c.site_id, c.cat_id, c.group_id, c.parent_id, c.cat_name, c.cat_url_title, c.cat_description, c.cat_image, c.cat_order, ' .
                        '(' .
                        'SELECT COUNT(ct.entry_id) ' .
                        'FROM exp_channel_titles ct, exp_category_posts cp ' .
                        'WHERE cp.entry_id=ct.entry_id AND ct.status IN (\'' . $status . '\') AND cp.cat_id=c.cat_id' .
                        $channel_sql .
                        ') AS entry_count ' .
                        'FROM exp_categories c ' .
                        'WHERE c.site_id IN (' . $this->EE->db->escape_str($this->site_ids) . ') AND c.group_id IN (' . $this->EE->db->escape_str($this->group_ids) . ') ' .
                        'ORDER BY site_id, FIELD(c.group_id, ' . $this->EE->db->escape_str($this->group_ids) . '), parent_id, cat_order';
            } else {
                $sql = 'SELECT c.parent_id, c.cat_name, c.cat_url_title, c.cat_description, c.cat_image, c.cat_order, ' .
                        '(' .
                        'SELECT COUNT(ct.entry_id) ' .
                        'FROM exp_channel_titles ct, exp_category_posts cp ' .
                        'WHERE cp.entry_id=ct.entry_id AND ct.status IN (\'' . $status . '\') AND cp.cat_id=c.cat_id' .
                        $channel_sql .
                        ') AS entry_count, cfd.* ' .
                        'FROM exp_categories c ' .
                        'LEFT JOIN exp_category_field_data cfd ON cfd.cat_id=c.cat_id ' .
                        'WHERE c.site_id=cfd.site_id AND c.site_id IN (' . $this->EE->db->escape_str($this->site_ids) . ') AND c.group_id IN (' . $this->EE->db->escape_str($this->group_ids) . ') ' .
                        'ORDER BY site_id, FIELD(c.group_id, ' . $this->EE->db->escape_str($this->group_ids) . '), parent_id, cat_order';
            }
        } else { // simpler query, no need to count entries
            if ($this->custom_fields == 'no') {
                $sql = 'SELECT * FROM exp_categories ' .
                        'WHERE site_id IN (' . $this->EE->db->escape_str($this->site_ids) . ') AND group_id IN (' . $this->EE->db->escape_str($this->group_ids) . ') ' .
                        'ORDER BY site_id, FIELD(group_id, ' . $this->EE->db->escape_str($this->group_ids) . '), parent_id, cat_order';
            } else {
                $sql = 'SELECT c.parent_id, c.cat_name, c.cat_url_title, c.cat_description, c.cat_image, c.cat_order, cfd.* ' .
                        'FROM exp_categories c ' .
                        'LEFT JOIN exp_category_field_data cfd ON cfd.cat_id=c.cat_id ' .
                        'WHERE c.site_id=cfd.site_id AND c.group_id=cfd.group_id AND c.site_id IN (' . $this->EE->db->escape_str($this->site_ids) . ') AND c.group_id IN (' . $this->EE->db->escape_str($this->group_ids) . ') ' .
                        'ORDER BY site_id, FIELD(c.group_id, ' . $this->EE->db->escape_str($this->group_ids) . '), parent_id, cat_order';
            }
        }

        $gcm_result = $this->EE->db->query($sql);
        if ($gcm_result->num_rows() == 0) {
            return false;
        }
        foreach ($gcm_result->result_array() as $row) {
            
            /**
             * If this category does not have entries for the current site 
             * region then we want to exclude them.
             * 
             * @author James McFall <james@96black.co.nz>
             */
            if (isset($this->synergy_for_country) && in_array($this->synergy_for_country, array("NZ", "AU"))) {
                
                $synergyCountryFilterField = null;
                switch($this->group_ids) {
                    # Products channel
                    case "1":
                        $synergyCountryFilterField = 55;
                        break;
                    
                    # Services
                    case "2":
                        $synergyCountryFilterField = 47;
                        break;
                }
                
                $emptySQL = "
                    SELECT COUNT(*) AS `posts_for_country` 
                    FROM exp_channel_data
                    LEFT JOIN exp_category_posts
                    ON exp_channel_data.entry_id = exp_category_posts.entry_id
                    INNER JOIN exp_channel_titles 
                    ON exp_channel_data.entry_id = exp_channel_titles.entry_id
                    WHERE (field_id_".$synergyCountryFilterField." = '" . $this->synergy_for_country . "' OR field_id_".$synergyCountryFilterField." = 'All')
                    AND exp_channel_titles.status = 'open'
                    AND exp_category_posts.cat_id = " . $row['cat_id'];
                        
                $emptyResult = $this->EE->db->query($emptySQL)->row();
                
                # If there are no posts for this category in this country, exclude it
                if ($emptyResult->posts_for_country == 0) {
                    continue;
                }
            }
            
            
            $entry_count = (array_key_exists('entry_count', $row)) ? $row['entry_count'] : '';
            $cat_array[$row['cat_id']] = array(
                'site_id' => $row['site_id'],
                'cat_name' => $row['cat_name'],
                'cat_url_title' => $row['cat_url_title'],
                'cat_description' => $row['cat_description'],
                'cat_image' => $row['cat_image'],
                'cat_order' => $row['cat_order'],
                'parent_id' => $row['parent_id'],
                'parent_url_title' => '',
                'parent_name' => '',
                'group_id' => $row['group_id'],
                'group_name' => $this->group_id_arr[$row['group_id']]['group_name'],
                'entry_count' => $entry_count,
                'total_entry_count' => '',
                'has_children' => false
            );

            if ($this->custom_fields == 'yes' && array_key_exists($row['group_id'], $this->custom_fields_arr)) { // if this category group has custom category fields)
                // add custom field data
                foreach ($this->custom_fields_arr[$row['group_id']] as $cf_arr_key => $cf_field_array) {
                    $custom_field_formatted = $this->EE->typography->parse_type($row['field_id_' . $cf_field_array['field_id']], array('text_format' => $row['field_ft_' . $cf_field_array['field_id']], 'html_format' => $this->group_id_arr[$row['group_id']]['field_html_formatting'], 'auto_links' => 'n', 'allow_img_url' => 'y'));
                    $cat_array[$row['cat_id']][$cf_field_array['field_name']] = $custom_field_formatted;
                }
            }
        }
        // the $cat_array now contains all categories for the category group. It's not in the correct order yet, so let's create the correct order.
        // we're also going to add the depth and complete_path values and set the parent_url_title and parent_name values in the _category_subtree function.
        foreach ($cat_array as $key => $val) {
            if ($val['parent_id'] == 0) {
                $depth = 1;
                $output_path_arr = array();
                if ($this->min_depth_in_output == 1) {
                    $output_path_arr[$key] = $val['cat_url_title'];
                }
                $this->categories[$key] = array_merge(array('cat_id' => $key, 'depth' => $depth, 'complete_path' => array($key => $val['cat_url_title']), 'output_path' => $output_path_arr, 'original_root_cat_id' => $key), $cat_array[$key]);
                $this->_category_subtree($key, $val['cat_url_title'], $val['cat_name'], $cat_array, $depth);
            }
        }
        // $this->categories is now in the correct order.
    }

// end function _get_categories

    private function _category_subtree($parentcat_id, $cat_url_title, $cat_name, $cat_array, $depth) {
        // borrowed from Api_channel_categories.php
        $depth++;
        foreach ($cat_array as $key => $val) {
            if ($val['parent_id'] == $parentcat_id) { // for every direct child category
                $this->categories[$val['parent_id']]['has_children'] = true; // the parent category has children
                $this->categories[$key] = array_merge(array('cat_id' => $key, 'depth' => $depth, 'complete_path' => $this->categories[$parentcat_id]['complete_path'], 'output_path' => $this->categories[$parentcat_id]['output_path'], 'original_root_cat_id' => $this->categories[$parentcat_id]['original_root_cat_id']), $cat_array[$key]);
                $this->categories[$key]['complete_path'][$key] = $val['cat_url_title'];
                if ($depth >= $this->min_depth_in_output) {
                    $this->categories[$key]['output_path'][$key] = $val['cat_url_title'];
                }
                $this->categories[$key]['parent_url_title'] = $cat_url_title;
                $this->categories[$key]['parent_name'] = $cat_name;
                $this->_category_subtree($key, $this->categories[$key]['cat_url_title'], $this->categories[$key]['cat_name'], $cat_array, $depth);
            }
        }
    }

// end function _category_subtree

    private function _add_extra_values() {
        /*
          - add open value (true/false) for every category
          - add active value (true/false) for every category
          - add total_entry_count value for every category (number of entries for this category plus the entries in any of its child categories)
          - add current value (true/false) for every category. This value is set to true when a category has been provided by the cat_id or cat_url_title parameter, or for categories an entry has been added to when using the entry_id or url_title parameter.
         */

        $gw_total_count = count($this->categories);
        $depth_open_arr = (strpos($this->depth_open, '|') !== false) ? explode('|', $this->depth_open) : array($this->depth_open);
        $active_depth_open_arr = (strpos($this->active_depth_open, '|') !== false) ? explode('|', $this->active_depth_open) : array($this->active_depth_open);
        $current_arr = array();

        // if there are any active categories, make sure their parents are also marked as active
        if (!empty($this->active_categories)) {
            $current_arr = $this->active_categories; // $current_arr will now contain only categories provided with cat_id, cat_url_title, or categories an entry has been added to when using the entry_id or url_title parameter.
            $additional_active_cats = array();
            foreach ($this->active_categories as $key => $cat_id) {
                if (array_key_exists($cat_id, $this->categories)) { // (we need to check with array_key_exists because the category may have been removed by the excl_cat_id or min/max_depth parameters)
                    foreach ($this->categories[$cat_id]['output_path'] as $cat_arr_key => $cat_url_title) {
                        $this->categories[$cat_arr_key]['active'] = true;
                        if (!in_array($cat_arr_key, $this->active_categories)) {
                            $additional_active_cats[] = $cat_arr_key;
                        }
                    }
                }
            }
            $this->active_categories = array_merge($this->active_categories, $additional_active_cats);
        }

        foreach ($this->categories as $key => $val) {
            $this->categories[$key]['total_entry_count'] = $this->categories[$key]['entry_count'];

            // add open/active values
            if (in_array($val['cat_id'], $this->active_categories)) { // the category is active
                // look back and set all parent categories and current category to active and open
                foreach ($this->categories[$key]['output_path'] as $cat_arr_key => $cat_url_title) {
                    $this->categories[$cat_arr_key]['active'] = $this->categories[$cat_arr_key]['open'] = true;
                }
            } else {
                $this->categories[$key]['active'] = false;

                // we may need to open this category based on the active_depth_open and depth_open parameters
                reset($this->categories[$key]['output_path']);
                $first_catid_in_output_path = key($this->categories[$key]['output_path']);
                if ((in_array($val['depth'], $active_depth_open_arr) && $this->categories[$first_catid_in_output_path]['active']) || in_array($val['depth'], $depth_open_arr)) {
                    // look back and set all parent categories and current category to open
                    foreach ($this->categories[$key]['output_path'] as $cat_arr_key => $cat_url_title) {
                        $this->categories[$cat_arr_key]['open'] = true;
                    }
                } else {
                    $this->categories[$key]['open'] = false;
                }
            }

            // add current values
            if (in_array($key, $current_arr)) {
                $this->categories[$key]['current'] = true;
                $this->active_branch_root_cat_ids[] = $this->categories[$key]['original_root_cat_id'];
            } else {
                $this->categories[$key]['current'] = false;
            }

            // increase total_entry_count for parent categories if needed
            if (($this->categories[$key]['depth'] > $this->min_depth_in_output) && $this->categories[$key]['entry_count'] != 0) {
                // look back and increase total_entry_count for all parents
                foreach ($this->categories[$key]['output_path'] as $cat_arr_key => $cat_url_title) {
                    if ($cat_arr_key != $key) {
                        $this->categories[$cat_arr_key]['total_entry_count'] += $this->categories[$key]['entry_count'];
                    }
                }
            }
            $prev_depth = $val['depth'];
        }
    }

// end function _add_extra_values

    private function _remove_inactive_branches() {
        foreach ($this->categories as $key => $val) {
            if (!in_array($val['original_root_cat_id'], $this->active_branch_root_cat_ids)) {
                unset($this->categories[$key]);
            }
        }
    }

// end function _remove_inactive_branches

    private function _remove_subcategories_not_open() {
        // - remove subcategories from categories that are not open
        foreach ($this->categories as $key => $category) {
            if (!$category['open'] && $category['depth'] != $this->min_depth_in_output) {
                if (isset($this->categories[$category['parent_id']])) {
                    if (!$this->categories[$category['parent_id']]['open']) {
                        unset($this->categories[$key]);
                    }
                } else { // parent has already been removed
                    unset($this->categories[$key]);
                }
            }
        }
        // clean up array
        $this->categories = array_values($this->categories);
    }

// end function _remove_subcategories_not_active

    private function _remove_categories_by_depth() {
        // remove categories based on min_depth or max_depth params
        foreach ($this->categories as $key => $val) {
            $add_to_output = true;
            if (!empty($this->min_depth) && $val['depth'] < $this->min_depth) {
                $add_to_output = false;
            }
            if (!empty($this->max_depth) && $val['depth'] > $this->max_depth) {
                $add_to_output = false;
            }
            if (!$add_to_output) {
                unset($this->categories[$key]);
            }
        }
    }

// end function _remove_categories_by_depth

    private function _remove_empty_categories() {
        // - remove categories where total_entry_count == 0.
        foreach ($this->categories as $key => $val) {
            if ($this->categories[$key]['total_entry_count'] == 0) {
                unset($this->categories[$key]);
            }
        }
        // we may have removed something in the array, clean up array
        $this->categories = array_values($this->categories);
    }

// end function _remove_empty_categories

    private function _exclude_categories() {
        // - remove categories and potential subcategories provided with the excl_cat_id parameter
        $excl_arr = explode('|', $this->excl_cat_id);
        foreach ($this->categories as $key => $category) {
            if (in_array($category['parent_id'], $excl_arr)) {
                $excl_arr[] = $category['cat_id'];
            }
            if (in_array($category['cat_id'], $excl_arr)) {
                unset($this->categories[$key]);
            }
        }
    }

// end function _exclude_categories

    private function _exclude_category_groups() {
        // - remove category groups provided with the excl_group_id parameter
        $excl_arr = explode('|', $this->excl_group_id);
        foreach ($this->group_id_arr as $key => $category_group_id) {
            if (in_array($category_group_id, $excl_arr)) {
                unset($this->group_id_arr[$key]);
            }
        }
        // we may have removed something in the array, clean up array
        $this->group_id_arr = array_values($this->group_id_arr);
    }

// end function _exclude_category_groups

    private function _get_custom_fields() {
        // load typography library and grab custom category fields

        $this->EE->load->library('typography');
        $this->EE->typography->initialize();
        $this->EE->typography->convert_curly = false;

        // grab custom field names and add them to array
        $sql = 'SELECT field_id, field_name, group_id FROM exp_category_fields WHERE site_id IN (' . $this->EE->db->escape_str($this->site_ids) . ') AND group_id IN(' . $this->EE->db->escape_str($this->group_ids) . ')';
        $gwc_result = $this->EE->db->query($sql);
        if ($gwc_result->num_rows() != 0) {
            foreach ($gwc_result->result_array() as $row) {
                $this->custom_fields_arr[$row['group_id']][] = array('field_id' => $row['field_id'], 'field_name' => $row['field_name']);
            }
        }
    }

    private function _generate_output() {
        $total_results = count($this->categories);
        if ($total_results == 0) {
            $this->return_data = $this->EE->TMPL->no_results();
            return;
        }
        $gw_output = '';
        $add_class_url_title_arr = array();
        $prev_group_id = $current_ul_depth = 0;

        if (version_compare(APP_VER, '2.4', '>=')) { // we need to load the file_field library in v 2.4.0 and up to parse filedir_x variables
            $this->EE->load->library('file_field');
        }

        if ($this->style == 'linear') {
            $linear_parse_vars_arr = array();
        } else {
            $gw_output .= '<' . $this->list_type;
            if (!empty($this->id)) {
                $gw_output .= ' id="' . $this->id . '"';
            }
            if (!empty($this->class)) {
                $gw_output .= ' class="' . $this->class . '"';
            }
            $gw_output .= '>' . "\n";
            $current_ul_depth = 1;
        }

        if ($this->style != 'linear') {
            // let's see if the {group_heading}..{/group_heading} tag pair is used.
            // if so, $matches_arr[1][0] = code between tag pairs. $matches_arr[0][0] = {group_heading}..{/group_heading}
            $pattern = LD . 'group_heading' . RD . '(.*?)' . LD . '\/group_heading' . RD;
            $group_heading_count = preg_match_all('/' . $pattern . '/msi', $this->tagdata, $matches_arr);
        }

        for ($gw_i = 0; $gw_i < $total_results; $gw_i++) { // for every category we need to display
            $tagdata = $this->tagdata;
            $new_group = ($this->categories[$gw_i]['group_id'] != $prev_group_id) ? true : false; // do we start a new category group?
            $last_cat_to_display = ($gw_i + 1 == $total_results) ? true : false; // is this the very last category we're going to display?
            // does this category have any children in output?
            if (!$last_cat_to_display) {
                $has_children_in_output = ($this->categories[$gw_i + 1]['parent_id'] == $this->categories[$gw_i]['cat_id']) ? true : false;
            } else {
                $has_children_in_output = false;
            }

            if ($this->style != 'linear') {
                if ($group_heading_count > 0) { // {group_heading}..{/group_heading} tag pair was found
                    if ($new_group) {
                        // parse group variables in between {group_heading}..{/group_heading} tag pair
                        $group_heading = str_replace(array('{' . $this->var_prefix . 'group_id}', '{' . $this->var_prefix . 'group_name}'), array($this->categories[$gw_i]['group_id'], $this->categories[$gw_i]['group_name']), $matches_arr[1][0]);
                        $css_idclass = '';
                        if ($this->id_group_heading != '-') {
                            $css_idclass .= ' id="' . $this->id_group_heading . $this->categories[$gw_i]['group_id'] . '"';
                        }
                        if ($this->class_group_heading != '-') {
                            $css_idclass .= ' class="' . $this->class_group_heading . '"';
                        }
                        $gw_output .= '<li' . $css_idclass . '>' . $group_heading . "</li>\n";
                    }
                    $tagdata = str_replace($matches_arr[0][0], '', $tagdata); // remove {group_heading}..{/group_heading} tag pair
                }

                $css_class = '';
                if ($this->class_depth != '-') {
                    $css_class .= $this->class_depth . $current_ul_depth;
                }
                if ($this->categories[$gw_i]['open'] && $this->class_open != '-') {
                    $css_class .= (empty($css_class)) ? $this->class_open : ' ' . $this->class_open;
                }
                if ($this->categories[$gw_i]['active'] && $this->class_active != '-') {
                    $css_class .= (empty($css_class)) ? $this->class_active : ' ' . $this->class_active;
                }
                if (($this->entry_count == 'yes' || $this->status != '') && $this->class_entry_count != '') {
                    $css_class .= (empty($css_class)) ? $this->class_entry_count . $this->categories[$gw_i]['entry_count'] : ' ' . $this->class_entry_count . $this->categories[$gw_i]['entry_count'];
                }
                if ($this->class_current != '-' && $this->categories[$gw_i]['current']) {
                    $css_class .= (empty($css_class)) ? $this->class_current : ' ' . $this->class_current;
                }
                if ($this->add_class_url_title == 'yes') {
                    // (use a "gw-" prefix since cat_url_title can start with a number, which would result in an invalid css class)
                    // (the class has to be unique, so add an increment number to the class for non-unique url titles)
                    if (array_key_exists($this->categories[$gw_i]['cat_url_title'], $add_class_url_title_arr)) {
                        $incr_nr = $add_class_url_title_arr[$this->categories[$gw_i]['cat_url_title']];
                        $incr_nr++;
                        $add_class_url_title_arr[$this->categories[$gw_i]['cat_url_title']] = $incr_nr;
                        $css_class .= (empty($css_class)) ? 'gw-' . $this->categories[$gw_i]['cat_url_title'] . $incr_nr : ' gw-' . $this->categories[$gw_i]['cat_url_title'] . $incr_nr;
                    } else {
                        $add_class_url_title_arr[$this->categories[$gw_i]['cat_url_title']] = 1;
                        $css_class .= (empty($css_class)) ? 'gw-' . $this->categories[$gw_i]['cat_url_title'] : ' gw-' . $this->categories[$gw_i]['cat_url_title'];
                    }
                }
                $gw_output .= (empty($css_class)) ? '<li>' . "\n" : '<li class="' . $css_class . '">' . "\n";
            }

            $cat_image = $this->categories[$gw_i]['cat_image'];
            if (version_compare(APP_VER, '2.4', '>=')) {
                $cat_image = ($cat_image == '0' || $cat_image == '') ? '' : $this->EE->file_field->parse_string($cat_image);
            }

            $var_values_arr = array(
                $this->var_prefix . 'site_id' => $this->categories[$gw_i]['site_id'],
                $this->var_prefix . 'cat_id' => $this->categories[$gw_i]['cat_id'],
                $this->var_prefix . 'cat_name' => $this->categories[$gw_i]['cat_name'],
                $this->var_prefix . 'cat_url_title' => $this->categories[$gw_i]['cat_url_title'],
                $this->var_prefix . 'cat_description' => $this->categories[$gw_i]['cat_description'],
                $this->var_prefix . 'cat_image' => $cat_image,
                $this->var_prefix . 'cat_order' => $this->categories[$gw_i]['cat_order'],
                $this->var_prefix . 'parent_id' => $this->categories[$gw_i]['parent_id'],
                $this->var_prefix . 'parent_url_title' => $this->categories[$gw_i]['parent_url_title'],
                $this->var_prefix . 'parent_name' => $this->categories[$gw_i]['parent_name'],
                $this->var_prefix . 'group_id' => $this->categories[$gw_i]['group_id'],
                $this->var_prefix . 'group_name' => $this->categories[$gw_i]['group_name'],
                $this->var_prefix . 'depth' => $this->categories[$gw_i]['depth'],
                $this->var_prefix . 'count' => $gw_i + 1,
                $this->var_prefix . 'total_results' => $total_results,
                $this->var_prefix . 'has_children' => $this->categories[$gw_i]['has_children'],
                $this->var_prefix . 'has_children_in_output' => $has_children_in_output,
                $this->var_prefix . 'entry_count' => $this->categories[$gw_i]['entry_count'],
                $this->var_prefix . 'total_entry_count' => $this->categories[$gw_i]['total_entry_count'],
                $this->var_prefix . 'active' => $this->categories[$gw_i]['active'],
                $this->var_prefix . 'open' => $this->categories[$gw_i]['open'],
                $this->var_prefix . 'current' => $this->categories[$gw_i]['current'],
                $this->var_prefix . 'new_group' => $new_group,
                $this->var_prefix . 'complete_path' => implode('/', $this->categories[$gw_i]['complete_path']),
                $this->var_prefix . 'output_path' => implode('/', $this->categories[$gw_i]['output_path'])
            );

            if ($this->custom_fields == 'yes' && !empty($this->custom_fields_arr)) {
                foreach ($this->custom_fields_arr as $cf_group_id => $cf_group_arr) {
                    if ($this->categories[$gw_i]['group_id'] == $cf_group_id) {
                        // add custom field data
                        foreach ($this->custom_fields_arr[$this->categories[$gw_i]['group_id']] as $cf_arr_key => $cf_field_array) {
                            $var_values_arr[$this->var_prefix . $cf_field_array['field_name']] = $this->categories[$gw_i][$cf_field_array['field_name']];
                        }
                    } else {
                        // replace custom fields from other groups with empty string
                        foreach ($this->custom_fields_arr[$cf_group_id] as $cf_arr_key => $cf_field_array) {
                            $var_values_arr[$this->var_prefix . $cf_field_array['field_name']] = '';
                        }
                    }
                }
            }

            if ($this->style != 'linear') {
                $tagdata = $this->EE->TMPL->parse_variables_row($tagdata, $var_values_arr);
                $gw_output .= $tagdata;

                if ($this->style == 'simple') {
                    // we're creating a simple 1 depth ul/ol list

                    $gw_output .= '</li>' . "\n";
                    if ($last_cat_to_display) { // if this is the last category we're displaying
                        $gw_output .= '</' . $this->list_type . '>' . "\n";
                    }
                } elseif ($this->style == 'nested') {
                    if (!$last_cat_to_display) { // if this is not the last category we're displaying
                        if ($this->categories[$gw_i + 1]['depth'] > $this->categories[$gw_i]['depth']) { // the next category has a higher depth --> create new ul/ol
                            $gw_output .= '<' . $this->list_type . '>' . "\n";
                            $current_ul_depth++;
                        } elseif ($this->categories[$gw_i + 1]['depth'] == $this->categories[$gw_i]['depth']) { // the next category has the same depth
                            $gw_output .= '</li>' . "\n";
                        } else { // the next category has a lower depth --> close open li's and ul's/ol's
                            $diff = $this->categories[$gw_i]['depth'] - $this->categories[$gw_i + 1]['depth'];
                            for ($j = 0; $j < $diff; $j++) {
                                $gw_output .= '</li>' . "\n" . '</' . $this->list_type . '>' . "\n";
                                $current_ul_depth--;
                            }
                            $gw_output .= '</li>' . "\n";
                        }
                    } else { // if this is the last item --> close open li's and ul's/ol's
                        for ($j = 0; $j < $current_ul_depth; $j++) {
                            $gw_output .= '</li>' . "\n" . '</' . $this->list_type . '>' . "\n";
                        }
                    }
                }
            } else {
                $linear_parse_vars_arr[] = $var_values_arr;
            }
            $prev_group_id = $this->categories[$gw_i]['group_id'];
        }
        $this->return_data = ($this->style != 'linear') ? $gw_output : $this->EE->TMPL->parse_variables(rtrim($this->tagdata), $linear_parse_vars_arr);
    }

// end function _generate_output
    // ----------------------------------------
    // Plugin Usage
    // ----------------------------------------
    // This function describes how the plugin is used.
    public function usage() {
        ob_start();
        ?>
        PARAMETERS:
        site_id
        channel_id
        channel
        group_id
        cat_id
        cat_url_title
        entry_id
        url_title
        depth_open
        active_depth_open
        entry_count
        status
        style
        backspace
        show_empty
        excl_cat_id
        excl_group_id
        min_depth
        max_depth
        active_branch_only
        list_type
        id
        class
        class_open
        class_active
        class_depth
        id_group_heading
        class_group_heading
        class_entry_count
        class_current
        add_class_url_title
        custom_fields
        variable_prefix


        VARIABLES:
        {site_id}
        {cat_id}
        {cat_name}
        {cat_url_title}
        {cat_description}
        {cat_image}
        {cat_order}
        {parent_id}
        {parent_url_title}
        {parent_name}
        {depth}
        {count}
        {entry_count}
        {total_entry_count}
        {total_results}
        {active}
        {open}
        {current}
        {has_children}
        {has_children_in_output}
        {complete_path}
        {output_path}
        {group_id}
        {group_name}
        {group_heading}..{/group_heading}
        {new_group}


        You can use {if no_results}No results{/if} as well.
        Custom category fields you may have created are available as variables when the custom_fields parameter has been set to "yes".

        EXAMPLE - A basic category based menu:

        {exp:gwcode_catmenu cat_url_title="{last_segment}"}
        {if no_results}
        <!--
        no valid category was provided in the URL segment
        show only root categories as our menu
        -->
        {exp:channel:categories channel="products" parent_only="yes" disable="member_data|pagination"}
        <a href="{path='products/{category_url_title}'}">{category_name}</a>
        {/exp:channel:categories}
        {/if}
        <a href="{path='products/{complete_path}'}"{if current} class="current"{/if}>{cat_name}</a>
        {/exp:gwcode_catmenu}

        FOR MORE EXAMPLES, SEE:
        http://gwcode.com/add-ons/gwcode-catmenu
        <?php
        $buffer = ob_get_contents();
        ob_end_clean();
        return $buffer;
    }

}

// END CLASS
?>
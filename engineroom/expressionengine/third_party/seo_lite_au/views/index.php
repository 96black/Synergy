<style type="text/css">
    .fullwidth {
        width:100%;
    }

    #instructions {
        display:none;
    }
</style>

<script type="text/javascript">

    $(document).ready(
            function() {

                $('#view_instructions').click(function(){
                    if($('#instructions').is(':visible')) {
                        $('#instructions').hide('fast');
                        $('#view_instructions').html('Show instructions');
                    }
                    else
                    {
                        $('#instructions').show('fast');
                        $('#view_instructions').html('Hide instructions');
                    }
                });
                
            });

</script>

<h3><a href="#" id="view_instructions">View instructions</a></h3>

    <div id="instructions">
        <p>Put one of these tags in your template:</p>

        <p>By <strong>segment</strong>: <input type='text' class="fullwidth" value='{exp:seo_lite_au url_title="{segment_3}"}' readonly/> </p>
        <p>By <strong>entry_id</strong>: <input type='text' class="fullwidth" value='{exp:seo_lite_au entry_id="{entry_id}"}' readonly/></p>
        <p><strong>Intelligent mode</strong> aka Use-Last-Segment-Mode: <input type='text' class="fullwidth" value='{exp:seo_lite_au use_last_segment="yes"}' readonly/></p>
        <p><strong>Static mode</strong> aka I-Will-Provide-Values-In-Template: (this will output "About Us" for the title tag but still use the default keywords/description for the site) <input type='text' class="fullwidth" value='{exp:seo_lite_au default_title="About us"}' readonly/></p></p>
        <p><strong>Static mode</strong> with everything overridden: <input type='text' class="fullwidth" value='{exp:seo_lite_au default_title="About us" default_keywords="new, keywords" default_description="This description is unique for this page"}' readonly/></p></p>

        <p>&nbsp;</p>
        <p><em>Either of these tags will output the template below with the title/keywords/description specific for the content. The template below is parsed as a normal EE template, so you can use any EE global variavbles and conditionals etc.</em> <a href="http://ee.bybjorn.com/seo_lite_au">More instructions available here.</a></p>

        <p>&nbsp;</p>
    </div>

<?php
	$this->table->set_template($cp_table_template);
	$this->table->set_heading(array(
			array('data' => lang('setting'), 'width' => '50%'),
			lang('current_value')
		)
	);
?>

<?=form_open($_form_base.'&method=save_settings')?>

	<?php 

        $this->table->add_row(array(
                lang('template', 'seoliteau_template'),
                form_error('seoliteau_template').
                form_textarea('seoliteau_template', set_value('seoliteau_template', $template), 'id="seoliteau_template"')
            )
        );

		$this->table->add_row(array(
				lang('default_keywords', 'seoliteau_default_keywords'),
				form_error('seoliteau_default_keywords').
				form_input('seoliteau_default_keywords', set_value('seoliteau_default_keywords', $default_keywords), 'id="seoliteau_default_keywords"')
			)
		);
		
        $this->table->add_row(array(
                lang('default_description', 'seoliteau_default_description'),
                form_error('seoliteau_default_description').
                form_textarea('seoliteau_default_description', set_value('seoliteau_default_description', $default_description), 'id="seoliteau_default_description"')
            )
        );


        $this->table->add_row(array(
            lang('default_title_postfix', 'seoliteau_default_title_postfix'),
            form_error('seoliteau_default_title_postfix').
            form_input('seoliteau_default_title_postfix', set_value('seoliteau_default_title_postfix', $default_title_postfix), 'id="seoliteau_default_title_postfix"')
            )
        );

		echo $this->table->generate();
	?>
	<p>
		<?=form_submit(array('name' => 'submit', 'value' => lang('update'), 'class' => 'submit'))?>
	</p>

<?=form_close()?>

<?php
/* End of file index.php */
/* Location: ./system/expressionengine/third_party/seo_lite_au/views/index.php */
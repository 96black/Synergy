<?php

$plugin_info = array(
    'pi_name'           => 'Channel4entry',
    'pi_version'        => '0.1',
    'pi_author'         => 'James McFall',
    'pi_author_url'     => 'http://mcfall.geek.nz/',
    'pi_description'    => 'A simple plugin to return the channel name for a url
                            title.',
    'pi_usage'          => null
);

class Channel4entry {

    /**
     * Constructor
     */
    function __construct() {
        $this->EE = & get_instance();
        
        $url_title = $this->EE->TMPL->fetch_param('url_title');
        
        $result = $this->EE->db->query("
            SELECT channel_name
            FROM exp_channels
            LEFT JOIN exp_channel_titles
            ON exp_channel_titles.channel_id = exp_channels.channel_id
            WHERE exp_channel_titles.url_title = '" . $url_title . "'");
                
        if ($result->num_rows()) {
            $row = $result->row();
            $this->return_data = $row->channel_name;
        } else {
            $this->return_data = "FALSE";
        }
    }

    
}

?>

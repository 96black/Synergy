<?php

$plugin_info = array(
    'pi_name'           => 'Email To Link',
    'pi_version'        => '0.1',
    'pi_author'         => 'James McFall',
    'pi_author_url'     => 'http://mcfall.geek.nz/',
    'pi_description'    => 'Wrap a string in the email_to_link tags to convert
                            any email addresses in the string to mailto: anchor
                            links',
    'pi_usage'          => null
);

class Email_to_link {

    /**
     * Constructor
     */
    public function __construct() {
        $this->EE = & get_instance();
        $string = $this->EE->TMPL->tagdata;
        
        $string_with_emails = $this->_replace_emails_with_mailto($string);
        
        $this->return_data = $string_with_emails;
    }
    
    
    /**
     * Replace email addresses in a string with mailto: anchor links
     * 
     * @param <string> $string
     * @return <string>
     */
    private function _replace_emails_with_mailto($string) {
        
        # replace string@string.string with mailto link.
        $regex = '/(\S+@\S+\.\S+)/';
        $replace = '<a href="mailto:$1">$1</a>';

        return preg_replace($regex, $replace, $string);    
    }
}

?>

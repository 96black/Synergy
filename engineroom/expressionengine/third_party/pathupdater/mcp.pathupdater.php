<?php

class Pathupdater_mcp {

    public function __construct() {
        $this->EE = &get_instance();
    }
    
    public function index() {
        $this->EE->load->library('table');

        $data = array();
        
        return $this->EE->load->view('index', $data, TRUE);
    }

}

?>

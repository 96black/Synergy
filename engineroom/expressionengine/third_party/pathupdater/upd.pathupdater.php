<?php

class Pathupdater_upd {

    var $version = "1.0";

    public function __construct() {
        $this->EE = &get_instance();
    }

    public function install() {

        # Modules table entry
        $r = $this->EE->db->insert('modules', array(
            'module_name' => 'Pathupdater',
            'module_version' => $this->version,
            'has_cp_backend' => 'y',
            'has_publish_fields' => 'n'
        ));
        
        return TRUE;
    }

    function update($current = '') {
        # Necessary to get EE to update the version number
        return TRUE;
    }

    public function uninstall() {

        # Remove from the modules table
        $this->EE->db->delete('modules', array('module_name' => 'Path Updater'));
        
        return TRUE;
    }

}

?>
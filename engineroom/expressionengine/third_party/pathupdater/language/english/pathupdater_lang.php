<?php
$lang = array(

// -------------------------------------------
//  Module CP
// -------------------------------------------

'pathupdater_module_name' => "Path Updater",
'pathupdater_module_description' => "@todo",

'pathupdater_no_module' => 'You haven’t installed pathupdater’s module yet.',

// Pages
'pathupdater_settings' => 'Settings',
'pathupdater_configs' => 'Editor Configurations',

// Settings
'pathupdater_license_key' => 'License Key',
'pathupdater_file_browser' => 'File Browser',
'pathupdater_file_browser_desc' => 'Which file browser should be used when browsing for images and files from your pathupdater fields?',

// Configs
'pathupdater_clone' => 'Clone',
'pathupdater_no_configs' => 'There are currently no configurations.',

// Edit Config
'pathupdater_create_config' => 'Create a New Configuration',
'pathupdater_edit_config' => 'Edit Configuration',
'pathupdater_config_settings' => 'Configuration Settings',
'pathupdater_config_name' => 'Configuration Name',
'pathupdater_toolbar' => 'Customize  the Toolbar',
'pathupdater_toolbar_help1' => 'Select toolgroups by dragging<br />them from the bottom bucket<br />into the sample toolbar.',
'pathupdater_toolbar_help2' => 'You can also toggle which<br />buttons should be included<br />by single-clicking on them.',
'pathupdater_height' => 'Height',
'pathupdater_resizable' => 'Resizable',
'pathupdater_css' => 'CSS File',
'pathupdater_css_desc' => 'Enter the URL to a CSS file you want to be applied to your field contents.',
'pathupdater_upload_dir' => 'Upload Directory',
'pathupdater_advanced' => 'Advanced Settings',
'pathupdater_config_saved' => 'Your configuration saved successfully.',

// Delete Config
'pathupdater_delete_config' => 'Delete Configuration',
'pathupdater_delete_config_confirm' => 'Are you sure you want to permanently delete this configuration?',
'pathupdater_config_deleted' => 'Your configuration was deleted successfully.',

// -------------------------------------------
//  Field Settings
// -------------------------------------------

'pathupdater_convert_entries' => 'Convert existing entries?',
'pathupdater_convert_entries_desc' => 'pathupdater can convert your previous entries’ field data to HTML for you.',
'pathupdater_convert_rows' => 'Convert existing rows?',
'pathupdater_editor_config' => 'Editor Configuration',
'pathupdater_edit_configs' => 'Edit&nbsp;Configurations',
'pathupdater_defer' => 'Defer CKEditor initialization?',
'pathupdater_defer_desc' => 'If you select “Yes”, pathupdater won’t initialize CKEditor until the field is clicked on.',

// -------------------------------------------
//  Fields
// -------------------------------------------

'pathupdater_site_page' => 'Site Page',
'pathupdater_site_template' => 'Site Template',

''=>''
);

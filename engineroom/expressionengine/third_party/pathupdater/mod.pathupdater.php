<?php

class Pathupdater_mod {

    public function __construct() {
        $this->EE = &get_instance();
    }
    
    /**
     * This function grabs all the rows from the exp_upload_prefs, converts them to
     * stdClass objects and adds them to an array.
     * 
     * @return <array> $pathObjects[path_name] = object;
     */
    public function getAllUploadPaths() {
        
        # Pull all of the upload paths out of the database to display
        $pathResults = mysql_query("SELECT * FROM `exp_upload_prefs`");
        $pathObjects = array();

        while ($row = mysql_fetch_object($pathResults)) {
            $row->prepped_field_name = lcfirst(str_replace(' ', '', $row->name));
            $pathObjects[$row->name] = $row;
        }

        return $pathObjects;
    }

}
<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  ============================================================
  This ExpressionEngine plugin was created by Leon Dijk
  - http://gwcode.com/
  ============================================================
  This plugin is licensed under a
  Creative Commons Attribution-NoDerivs 3.0 Unported License.
  - http://creativecommons.org/licenses/by-nd/3.0/
  ============================================================
  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
  KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
  WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
  PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
  OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
  OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
  OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ============================================================
 */

$plugin_info = array(
    'pi_name' => 'GWcode HasChildren',
    'pi_version' => '1.0.0',
    'pi_author' => 'Leon Dijk',
    'pi_author_url' => 'http://gwcode.com/add-ons/gwcode-haschildren',
    'pi_description' => 'A simple plugin to check if a category has child categories.',
    'pi_usage' => Gwcode_haschildren::usage()
);

class Gwcode_haschildren {

    private $get_by = ''; // valid values: cat_id, cat_url_title

    public function __construct() {
        $this->EE = & get_instance();

        // fetch parameters
        $this->cat_id = $this->EE->TMPL->fetch_param('cat_id', '');
        if (!empty($this->cat_id)) {
            $this->get_by = 'cat_id';
        }
        $this->group_id = $this->EE->TMPL->fetch_param('group_id', '');
        $this->cat_url_title = $this->EE->TMPL->fetch_param('cat_url_title', '');
        if (!empty($this->cat_url_title) && empty($this->get_by)) {
            $this->get_by = 'cat_url_title';
        }
    }

// end function __construct

    public function check() {
        if ($this->get_by == 'cat_id') {
            if (!ctype_digit($this->cat_id)) {
                $this->EE->TMPL->log_item('[GWcode HasChildren]: The "cat_id" parameter contains invalid input. Returning false.');
                $this->EE->config->_global_vars['gwcode_haschildren'] = false;
                return;
            }
            $sql = 'SELECT COUNT(cat_id) AS children_count FROM exp_categories WHERE parent_id=' . $this->EE->db->escape($this->cat_id);
            if (!empty($this->group_id)) {
                if (!ctype_digit($this->group_id)) {
                    $this->EE->TMPL->log_item('[GWcode HasChildren]: The "group_id" parameter contains invalid input. The parameter will be ignored.');
                } else {
                    $sql .= ' AND group_id=' . $this->EE->db->escape($this->group_id);
                }
            }
        } elseif ($this->get_by == 'cat_url_title') {
            
            $sql = 'SELECT COUNT(cat_id) AS children_count FROM exp_categories WHERE parent_id IN (';
            $sql .= 'SELECT cat_id FROM exp_categories WHERE cat_url_title=' . $this->EE->db->escape($this->cat_url_title);
            if (!empty($this->group_id)) {
                if (!ctype_digit($this->group_id)) {
                    $this->EE->TMPL->log_item('[GWcode HasChildren]: The "group_id" parameter contains invalid input. The parameter will be ignored.');
                } else {
                    $sql .= ' AND group_id=' . $this->EE->db->escape($this->group_id);
                }
            }
            $sql .= ')';
        } else {
            $this->EE->TMPL->log_item('[GWcode HasChildren]: The "cat_id" or "cat_url_title" parameter is missing. Returning false.');
            $this->EE->config->_global_vars['gwcode_haschildren'] = false;
            return;
        }
        $gw_result = $this->EE->db->query($sql);
                
        if ($gw_result->num_rows() == 0 || $gw_result->row('children_count') == 0) {
            $this->EE->config->_global_vars['gwcode_haschildren'] = false;
            return "FALSE";
        } else {
            $this->EE->config->_global_vars['gwcode_haschildren'] = true;
            return "TRUE";
        }
    }

// end function check

    public static function usage() {
        ob_start();
        ?>
        In these examples, I'm going to assume segment_3 holds your category url title.
        Change segment_3 with something else when needed.
        -------------------------------------------------------------------------------

        First, make sure you call the plugin to check if a category has child categories.
        You only have to do this once in the template, somewhere at the top for example.

        If you already have Low Seg2Cat installed, you can use this:
        {exp:gwcode_haschildren:check cat_id="{segment_3_category_id}"}

        Without Low Seg2Cat, you can use this:
        {exp:gwcode_haschildren:check cat_url_title="{segment_3}"}


        You can also add the group_id parameter if you want to limit the search for the cat_url_title in a certain category group
        (different category groups can have categories with the same category url title):
        {exp:gwcode_haschildren:check group_id="1" cat_url_title="{segment_3}"}


        Then, you can use conditionals like this in your template:
        {if gwcode_haschildren}
        The currently viewed category has child categories! Show something.
        {if:else}
        No child categories! Show something else.
        {/if}
        <?php
        $buffer = ob_get_contents();
        ob_end_clean();
        return $buffer;
    }

// end function usage
}

// end class
?>
<?php
$plugin_info = array(
    'pi_name' => 'Number Format',
    'pi_version' => '1.0',
    'pi_author' => '96black',
    'pi_author_url' => 'http://96black.co.nz/',
    'pi_description' => 'This plugin is a wrapper for the php function number_format().
                         The point is to format a number with grouped thousands.
                         http://php.net/manual/en/function.number-format.php',
    'pi_usage' => Number_format::usage()
);

class Number_format {

    public $return_data = null;

    /**
     * Constructor
     */
    function __construct() {

        $this->EE = & get_instance();

        # Get the number we want to format
        $contents = $this->EE->TMPL->tagdata;

        # The default values for number_format http://php.net/manual/en/function.number-format.php
        $decimals = 0;
        $dec_point = ".";
        $thousands_sep = ",";


        # If the number_format() parameters have been specified in tags, use them.
        # If not the default values will be used.
        if ($this->EE->TMPL->fetch_param('decimals'))
            $decimals = $this->EE->TMPL->fetch_param('decimals');

        if ($this->EE->TMPL->fetch_param('dec_point'))
            $dec_point = $this->EE->TMPL->fetch_param('dec_point');

        if ($this->EE->TMPL->fetch_param('thousands_sep'))
            $thousands_sep = $this->EE->TMPL->fetch_param('thousands_sep');


        # Run the number_format() function with the specified parameters
        $this->return_data = number_format($contents, $decimals, $dec_point, $thousands_sep);
    }

    /**
     * This function describes how the plugin is used.
     *
     * @access  public
     * @return  string
     */
    public static function usage() {
        ob_start();
        ?>

        The Number Format plugin is a simple wrapper for the php number_format() function. This allows the functionality to be used without having to have php enabled or to worry about the php input mode.
        
        It has the same parameters as the number_format function, all of which are optional http://php.net/manual/en/function.number-format.php
        
        A usage example is below. The below example will return 100,000.00.
        
        {exp:number_format decimals="2" dec_point="." thousands_sep=","}100000{/exp:number_format}
        
        Note decimals, dec_point and thousands_sep aren't required.
        <?php
        $buffer = ob_get_contents();
        ob_end_clean();

        return $buffer;
    }

}
?>

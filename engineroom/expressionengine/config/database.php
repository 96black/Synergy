<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$active_group = 'expressionengine';
$active_record = TRUE;

$devUrls = array(
    "synergy.dev.96black.co.nz",
    "synergy-au.dev.96black.co.nz"
);

$localUrls = array(
    "synergy.local"
);

if (in_array($_SERVER["HTTP_HOST"], $devUrls)):
    $db['expressionengine']['hostname'] = "127.0.0.1";
    $db['expressionengine']['username'] = "root";
    $db['expressionengine']['password'] = "EGm4YPH7oXt1FngaW9VfEX3rsneAubXGguDtuKAn7V_XdOCCIynbJGf";
    $db['expressionengine']['database'] = "synergy_db";
elseif (in_array($_SERVER["HTTP_HOST"], $localUrls)):
    $db['expressionengine']['hostname'] = "localhost";
    $db['expressionengine']['username'] = "root";
    $db['expressionengine']['password'] = "root";
    $db['expressionengine']['database'] = "synergy_db";
else:
    $db['expressionengine']['hostname'] = $_SERVER["DB_HOST"];
    $db['expressionengine']['username'] = $_SERVER["DB_USER"];
    $db['expressionengine']['password'] = $_SERVER["DB_PASS"];
    $db['expressionengine']['database'] = $_SERVER["DB_NAME"];
endif;

$db['expressionengine']['dbdriver'] = 'mysql';
$db['expressionengine']['pconnect'] = FALSE;
$db['expressionengine']['dbprefix'] = 'exp_';
$db['expressionengine']['swap_pre'] = 'exp_';
$db['expressionengine']['db_debug'] = TRUE;
$db['expressionengine']['cache_on'] = FALSE;
$db['expressionengine']['autoinit'] = FALSE;
$db['expressionengine']['char_set'] = 'utf8';
$db['expressionengine']['dbcollat'] = 'utf8_general_ci';
$db['expressionengine']['cachedir'] = '/Users/james/Projects/96black/Synergy/engineroom/expressionengine/cache/db_cache/';

/* End of file database.php */
/* Location: ./system/expressionengine/config/database.php */
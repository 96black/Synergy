<?php

/**
 * Synergy_Utils Class
 * ===================
 * This class provides a selection of static methods to help with the Synergy
 * website.
 * 
 * @author James McFall <james@96black.co.nz>
 */
class Synergy_Utils {

    /**
     * Get The Channel For An Entry
     * 
     * @param <string> $urlTitle
     * @param <array> $limitToChannels - An array of channel IDs to limit the search to
     * @return <string>|<boolean> Channel name or false
     */
    public function getChannelForEntry($urlTitle, $limitToChannels = array()) {

        $EE = & get_instance();

        $query = "
            SELECT channel_name
            FROM exp_channels
            LEFT JOIN exp_channel_titles
            ON exp_channel_titles.channel_id = exp_channels.channel_id
            WHERE exp_channel_titles.url_title = '" . $urlTitle . "'";

        if (!empty($limitToChannels)) {
            $query .= "AND exp_channels.channel_id IN (" . implode(",", $limitToChannels) . ")";
        }
        
        $result = $EE->db->query($query);
        
        if ($result->num_rows()) {
            $row = $result->row();
            return $row->channel_name;
        }

        return false;
    }

    /**
     * Get the 2 letter country code based on the URL
     * 
     * @return <string> 2 letter country code
     * @throws Exception - If none of the specified URLs are matched
     */
    public function getSiteCountry() {

        # New Zealand Based URLs
        $nzURLs = array(
            "synergy.local",
            "synergy.dev.96black.co.nz",
            "synergypositioning.co.nz",
            "www.synergypositioning.co.nz",
            "www.synergypositioning.com",
            "www.synergyhire.co.nz",
            "www.synergyhire.com",
        );

        # Australian Based URLs
        $auURLs = array(
            "synergy-au.local",
            "synergy-au.dev.96black.co.nz",
            "synergypositioning.com.au",
            "www.synergypositioning.com.au"
        );

        # New Zealan URL
        if (in_array($_SERVER['HTTP_HOST'], $nzURLs)) {
            return "NZ";
        }

        # Australian URL
        else if (in_array($_SERVER['HTTP_HOST'], $auURLs)) {
            return "AU";
        }

        # Something bad has happened... we're not matching any of the URLs
        else {
            throw new Exception("Synergy Utils Exception: Unspecified URL in use in getSiteCountry().");
        }
    }

    /**
     * Get bottom level category for entry.
     * 
     * This function is designed to get the top level category for a post.
     * This is used to create a canonical URL for a product as they are 
     * accessible on multiple URLs.
     * 
     * @param <int> $entryId
     */
    public function getTopCatForEntry($entryId) {

        $EE = & get_instance();
        $topCat = false;


        # Get the top level category for this post so we can work our way down.
        $result = $EE->db->query("
            SELECT exp_categories.*
            FROM exp_categories
            LEFT JOIN exp_category_posts  
            ON exp_category_posts.cat_id = exp_categories.cat_id
            WHERE exp_categories.parent_id = 0
            AND exp_category_posts.entry_id = '" . $entryId . "'
        ");

        if ($result->num_rows()) {
            $topCat = $result->row();
        }
        return $topCat;
    }

    /**
     * Get the entry ID for a given URL title
     * 
     * @param <string> $urlTitle
     * @return <boolean>|<int> False or the entry ID
     */
    public function getEntryIdForURLTitle($urlTitle) {

        $this->EE->db->select("entry_id")
                ->from("exp_channel_titles")
                ->where("url_title", $urlTitle)
                ->limit(1);

        $result = $this->EE->db->get();

        if ($result->num_rows()) {
            $row = $result->row();
            return $row->entry_id;
        }

        return false;
    }

    /**
     * Get the category id using the URL title.
     * 
     * @param <string> $urlTitle
     * @return <int|boolean> $cat_id (either the id or false).
     */
    public function getCategoryIDFromURLTitle($urlTitle) {
        # Get this category from the DB
        $this->EE->db->select("*")
                ->from("exp_categories")
                ->where("cat_url_title", $urlTitle)
                ->limit(1);

        $result = $this->EE->db->get();

        if ($result->num_rows()) {
            $row = $result->row();
            return $row->cat_id;
        }

        return false;
    }

    /**
     * This method returns services category rows from the database providing 
     * they have entries that are available in the supplied region.
     * 
     * @param <int> $catGroup - the category group ID
     * @param <string> $region ("NZ", "AU").
     * @return <array>
     */
    public static function getNonEmptyCategoriesForRegion($catGroup, $region = "NZ") {

        $EE = &get_instance();

        $categories = array();
        
        # Get all categories
        # Where count(posts in NZ/ALL) > 0
        $result = $EE->db->query("
            SELECT *
            FROM `exp_categories` 
            LEFT JOIN `exp_category_field_data`
            ON `exp_category_field_data`.`cat_id` = `exp_categories`.`cat_id`
            WHERE (
                SELECT COUNT(*) 
                FROM exp_channel_data
                LEFT JOIN exp_category_posts
                ON exp_channel_data.entry_id = exp_category_posts.entry_id
                WHERE (field_id_47 = '" . $region . "' OR field_id_47 = 'All')
                AND exp_category_posts.cat_id = exp_categories.cat_id
            ) > 0
            AND `exp_categories`.`group_id` = '" . $catGroup . "'
            ORDER BY `cat_order`");

        foreach ($result->result() as $row) {
            
            // If we're on AU (service cats), overwrite the category description with the AU one
            if ($catGroup == 2 && self::getSiteCountry() == "AU") {
                $row->cat_description = $row->field_id_15;
            }
            
            // If we're on AU (prod cats), overwrite the category description with the AU one
            if ($catGroup == 1 && self::getSiteCountry() == "AU") {
                $row->cat_description = $row->field_id_14;
            }
            
            $categories[] = $row;
        }

        return $categories;
    }
    
    
    /**
     * Get all of the non-empty product categories where the products are 
     * available in the given region.
     * 
     * @param <string> $region ("NZ", "AU").
     * @param <array> $parents You can supply an array of parent ID's to get the 
     *                          children (i.e. if you were wanting to get the
     *                          second level categories after getting the parents)
     * @return <array>
     */
    public static function getNonEmptyProductCategoriesForRegion($region = "NZ", $topLevelOnly = false, $parentIds = array()) {
        
        $EE = &get_instance();
        $categories = array();
        
        $parentSearch = "";
        if ($topLevelOnly) {
            $parentSearch = " AND parent_id = 0 ";
        } else {
            $parentIds = implode(",", $parentIds);
            $parentSearch = " AND parent_id IN ($parentIds) ";
        }
        
        # Get all categories
        # Where count(posts in NZ/ALL) > 0
        $result = $EE->db->query("
            SELECT *
            FROM `exp_categories` 
            LEFT JOIN `exp_category_field_data`
            ON `exp_category_field_data`.`cat_id` = `exp_categories`.`cat_id`
            WHERE (
                SELECT COUNT(*) 
                FROM exp_channel_data
                LEFT JOIN exp_category_posts
                ON exp_channel_data.entry_id = exp_category_posts.entry_id
                LEFT JOIN exp_channel_titles
                ON exp_channel_data.entry_id = exp_channel_titles.entry_id
                WHERE (field_id_55 = '" . $region . "' OR field_id_55 = 'All')
                AND exp_category_posts.cat_id = exp_categories.cat_id
                AND exp_channel_titles.status = 'open'
            ) > 0
            " . $parentSearch . "
            AND `exp_categories`.`group_id` = '1'
            ORDER BY `cat_order`");
        
        foreach ($result->result() as $row) {
            
            // If we're on AU, overwrite the category description with the AU one
            if (self::getSiteCountry() == "AU") {
                $row->cat_description = $row->field_id_14;
            }
            
            $categories[] = $row;
        }

        return $categories;
    }

    /**
     * This method returns services category rows from the database providing 
     * they have entries that are available in the supplied region.
     * 
     * @param <int> $catGroup - the category group ID
     * @param <string> $region ("NZ", "AU").
     * @return <array>
     */
    public static function getNonEmptyCaseStudyCategoriesForRegion($catGroup, $region = "NZ") {

        $EE = &get_instance();

        $categories = array();
        
        # Get all categories
        # Where count(posts in NZ/ALL) > 0
        $result = $EE->db->query("
            SELECT *
            FROM `exp_categories` 
            LEFT JOIN `exp_category_field_data`
            ON `exp_category_field_data`.`cat_id` = `exp_categories`.`cat_id`
            WHERE (
                SELECT COUNT(*) 
                FROM exp_channel_data
                LEFT JOIN exp_category_posts
                ON exp_channel_data.entry_id = exp_category_posts.entry_id
                WHERE (field_id_45 = '" . $region . "' OR field_id_45 = 'All')
                AND exp_category_posts.cat_id = exp_categories.cat_id
            ) > 0
            AND `exp_categories`.`group_id` = '" . $catGroup . "'
            ORDER BY `cat_order`");

        foreach ($result->result() as $row) {
            
            // If we're on AU, overwrite the category description with the AU one
            //if ($catGroup == 3 && self::getSiteCountry() == "AU") {
            //    $row->cat_description = $row->field_id_15;
            //}
            
            $categories[] = $row;
        }

        return $categories;
    }


    
    /**
     * Parse the {filedir_x}filename.jpg DB values for file types into proper
     * file URLs.
     * 
     * @param <string> $fileFieldValue
     * @return <string>
     */
    public static function parseFileField($fileFieldValue) {

        $EE = &get_instance();
        $fileName = "";

        $matches = array();
        preg_match("/^\{filedir\_([^]]*)\}/", $fileFieldValue, $matches);

        # No matches (or no second match is detected, see below).
        if (count($matches) == 0 && !isset($matches[1])) {
            return "";
        }

        # The first match will be {filedir_x}, the second will be just the number x.
        $fileDirID = $matches[1];

        # Look up the file dir in the DB
        $EE->db->select("*")->from("exp_upload_prefs")->where("id", $fileDirID);
        $result = $EE->db->get();

        if ($result->num_rows()) {
            $row = $result->row();

            # Replace out the {filedir_x} portion with the full path dir
            $fileName = preg_replace("/^\{filedir\_([^]]*)\}/", $row->url, $fileFieldValue);
        }

        return $fileName;
    }

    /**
     * Parse the file size passed in in bytes.
     * 
     * @param <int> $bytes
     * @param <int> $dp - The decimal points you want returned in the result
     * @return <string> File size in a sensible format.
     */
    public function parseFileSize($bytes, $dp = 2) {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, $dp) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, $dp) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, $dp) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }

        return $bytes;
    }
    
    
    
    /**
     * Get an EE category
     * 
     * @param <int> $id
     * @return <stdClass>
     */
    public static function getCategory($id) {
        
        $EE = &get_instance();
            
        # Get all categories
        # Where count(posts in NZ/ALL) > 0
        $result = $EE->db->query("
            SELECT *
            FROM `exp_categories` 
            LEFT JOIN `exp_category_field_data`
            ON `exp_category_field_data`.`cat_id` = `exp_categories`.`cat_id`
            WHERE `exp_categories`.`cat_id` = '" . $id . "'");
        
        if ($result->num_rows() > 0)
            return $result->row();
        
        return false;
    }
    
    /**
     * Get the category from the database if the last segment is a category url title.
     * 
     * @return <boolean|stdClass>
     */
    public static function getCategoryForLastSegment() {
        $EE = &get_instance();
        
        $result = $EE->db->get_where("exp_categories", 
                array("cat_url_title" => self::getLastSegment()));
        
        if ($result->num_rows() == 0)
            return false;
        
        return self::getCategory($result->row()->cat_id);
    }
    
    /**
     * Return the last URL segment
     * @return <string>
     */
    public static function getLastSegment() {
        $EE = &get_instance();
        $segments = $EE->uri->segment_array();
        
        # If there are no segments we want the "last segment" to be empty.
        if (empty($segments))
            return "";
 
        return $segments[count($segments)];
    }
    
    /**
     * Get all non-empty product category child categories.
     * 
     * @param <int> $catId
     * @param <string> $site
     * @return <array> Array of categories
     */
    public static function getProductCatChildren($catId, $site = "NZ") {
        
        $EE = &get_instance();
        $categories = array();
        
        # Get all categories
        # Where count(posts in NZ/ALL) > 0
        $result = $EE->db->query("
            SELECT *
            FROM `exp_categories` 
            LEFT JOIN `exp_category_field_data`
            ON `exp_category_field_data`.`cat_id` = `exp_categories`.`cat_id`
            WHERE (
                SELECT COUNT(*) 
                FROM exp_channel_data
                LEFT JOIN exp_category_posts
                ON exp_channel_data.entry_id = exp_category_posts.entry_id
                LEFT JOIN exp_channel_titles
                ON exp_channel_data.entry_id = exp_channel_titles.entry_id
                WHERE (field_id_55 = '" . $site . "' OR field_id_55 = 'All')
                AND exp_category_posts.cat_id = exp_categories.cat_id
                AND exp_channel_titles.status = 'open'
            ) > 0
            AND parent_id = '" . $catId . "'
            ORDER BY `cat_order`");
        
        foreach ($result->result() as $row) {
            
            // If we're on AU, overwrite the category description with the AU one
            if (self::getSiteCountry() == "AU") {
                $row->cat_description = $row->field_id_14;
            }
            
            $categories[] = $row;
        }

        return $categories;
    }
    
    
    /**
     * Since SEO Lite doesn't work for categories, we've had to add some custom
     * fields for the category SEO data. This plugin builds the same markup 
     * that's currently in SEO Lite and populates it with the category data.
     * 
     * @param <int> $categoryId
     * @return <boolean|string>
     */
    public static function outputCategorySEOTags($categoryId) {
        
        # Get the category (and all the category fields)
        $category = self::getCategory($categoryId);
        
        if (!$category)
            return false;
        
        # This is the basic markup template
        $markup = '
        <title>{title} {extra} |  Synergy Positioning Systems</title>
        <meta name="keywords" content="{meta_keywords}" />
        <meta name="description" content="{meta_description}" />';
        
        # Note: Group ID 1 is products, 2 is services.
        switch ($category->group_id) {
            
            # Products
            case 1:
                # We've got different category fields for NZ and AU
                if (Synergy_Utils::getSiteCountry() == "NZ") {
                    $titleField = $category->field_id_3;
                    $keywordsField = $category->field_id_2;
                    $descriptionField = $category->field_id_1;
                    $extra = "| Authorised Topcon Dealer";
                } else {
                    $titleField = $category->field_id_9;
                    $keywordsField = $category->field_id_8;
                    $descriptionField = $category->field_id_7;
                    $extra = "";
                }
                break;

            # Services
            case 2:
                # We've got different category fields for NZ and AU
                if (Synergy_Utils::getSiteCountry() == "NZ") {
                    $titleField = $category->field_id_6;
                    $keywordsField = $category->field_id_5;
                    $descriptionField = $category->field_id_4;
                    $extra = "| Authorised Topcon Dealer";
                } else {
                    $titleField = $category->field_id_12;
                    $keywordsField = $category->field_id_11;
                    $descriptionField = $category->field_id_10;
                    $extra = "";
                }
                break;
            default:
                $markup = false;
                break;
        }
        
        
        # If the title SEO field is used, use it, but default to category name
        $title = strlen($titleField) > 0 ? $titleField : $category->cat_name;

        # Replace out the markup tags
        $markup = str_replace('{title}', $title , $markup);
        $markup = str_replace('{meta_keywords}', $keywordsField, $markup);
        $markup = str_replace('{meta_description}', $descriptionField, $markup);
        $markup = str_replace('{extra}', $extra, $markup);
        
        return $markup;
    }
    
    
    /**
     * Check if a category has child categories
     * 
     * @param <int> $catId
     * @return <boolean>
     */
    public static function doesCatHaveChildren($catId) {
        
        $CI = &get_instance();
        
        # See if we can find any categories with this category as the parent.
        $result = $CI->db->get_where("categories", array(
            "parent_id" => $catId
        ));
        
        if ($result->num_rows > 0)
            return true;
        
        return false;
    }
}

?>

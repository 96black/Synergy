<?php
/**
 * This class sends out any notifications the Synergy websites require.
 * 
 * @author James McFall <james@96black.co.nz>
 * @date 18 March 2013
 * @version V1.0
 */
class Synergy_Notifications extends CI_Notifications {

    # Used to pull in the email templates
    protected $_emailTemplateDir = null;
    protected $_siteUrl          = null;
    
    /**
     * Set up all the fields from the parent AHCD_Notifications library object.
     */
    public function __construct() {
        
        # Email Configuration
        $this->_emailFromAddress    = 'no_reply@synergypositioning.co.nz';
        $this->_emailFromName       = 'Synergy Website';
        $this->_emailReplyToAddress = 'no_reply@synergypositioning.co.nz';
        $this->_emailTemplateDir    = 'public/email-templates/';
        
        # This is used for image/link paths etc.
        $this->_siteUrl = 'http://' . $_SERVER['HTTP_HOST'];
    }
    
    /**
     * Send the contact form email.
     * 
     * @param <string> $name
     * @param <string> $company
     * @param <string> $email
     * @param <string> $phone
     * @param <string> $message
     * @param <string> $country
     * @return <boolean> Success or failure of sending the message
     */
    public function sendContactEmail($name, $company, $email, $phone, $message, $country) {
        
        # Override the reply to address and the country specific portions
        $this->_emailReplyToAddress = $email;
        
        # Read in template contents
        $template = file_get_contents($this->_emailTemplateDir . 'contact-submission.html');
        
        # Replace template variables
        $template = str_replace('{name}', $name, $template);
        $template = str_replace('{company}', $company, $template);
        $template = str_replace('{email}', $email, $template);
        $template = str_replace('{phone}', $phone, $template);
        $template = str_replace('{message}', $message, $template);
        $template = str_replace('{site_country}', $country, $template);
        $template = str_replace('{site_url}', $this->_siteUrl, $template);
                
        # Send the email
        return parent::_sendEmailNotification(
                    "james@96black.co.nz", 
                    "Synergy Contact Form Submission.", 
                    $template, 
                    ''
                ); 
    }
}
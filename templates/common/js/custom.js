var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('header').outerHeight();

// place any jQuery/helper plugins in here, instead of separate, slower script files.
jQuery(document).ready(function($){
    
    $("input#freeform_project_start_date").datepicker({
        dateFormat: "dd-mm-yy",
        constrainInput: true
    });

    $("input#freeform_access_from_date").datepicker({
        dateFormat: "dd-mm-yy",
        constrainInput: true
    });

    $("input#freeform_access_to_date").datepicker({
        dateFormat: "dd-mm-yy",
        constrainInput: true
    });
    
    // Home Page Large Banner Area
	
    $("#homeBanner ul").delay(500).fadeIn(500);
	
    $('#homeBanner')
    .before('<div id="pagerNav">').cycle({
        fx: 'fade',
        delay: 1000,
        slideExpr: 'li',
        speed: 1000,
        timeout: 6000,
        pause: true,
        pager: '#pagerNav'
    });
    
    $('#homeBannerMobile')
    .before('<div id="pagerNav">').cycle({
        fx: 'fade',
        delay: 1000,
        slideExpr: 'li',
        speed: 1000,
        timeout: 6000,
        pause: true,
        pager: '#pagerNav'
    });

    // Cycle kills the links inside the home page banners, so make them work again.
    $(".bannerLink").click(function() {
        window.location = $(this).attr("href");
    });

    // Category Listings Feature Slider
    if ($("ul#featureSlider li").length > 1) {
        $('#featureSlider').after('<div class="featureSliderNav"></div>').cycle({
            fx: 'fade',
            delay: 3000,
            slideExpr: 'li',
            speed: 1000,
            next:   '.rightArrow',
            prev: '.leftArrow',
            timeout: 12000,
            pager:  '.featureSliderNav',
            pagerEvent: 'mouseover',
            pagerAnchorBuilder: function(index, el) {
                return '<a href="#"> </a>';
            }
        });
    } else {
        // Remove the carousel height restriction if it's a single image.
        $("ul#featureSlider").css({height: "auto"});
    }

    if($('.additionalBlocks ul li').length > 1){
        $('.additionalBlocks ul').addClass('multiple');
    }else{
		
    }

    // Product Detail Image Gallery
    if ($('#productImageGallery li').length > 1) {
        $('#productImageGallery').after('<span id="slidePagerNext"></span><span id="slidePagerPrev" class="disabled"></span><div id="slidePager"><ul id="productImageGalleryNav"></ul></div>').cycle({
            fx: 'fade',
            delay: 9999999,
            speed: 1000,
            timeout: 12000,
            pagerEvent: 'mouseover',
            pager:  '#productImageGalleryNav',
            pagerAnchorBuilder: function(id, slide) {

                var s=$(slide);
                var imgsource=s.find('img').attr('src');
                var new_src = imgsource.substring(0,imgsource.length-4);

                new_src += imgsource.substring(imgsource.length-4, imgsource.length);
                return '<li><a href="#" style="background-image:url('+ new_src +'); filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='+ new_src +',sizingMethod=scale);}"></a></li>';
                
            } ,
            onPrevNextEvent: function(dir, id, el) {
                if (dir === true) {
                    if (id >= 3) { //replace '3' with your number of visible elements
                        $('#slidePagerNext').click();
                    }
                }
                else {
                    if (id >= 1) {
                        $('#slidePagerPrev').click();
                    }
                }
            }
        });

        $("#slidePager").jCarouselLite({
            btnNext: "#slidePagerNext",
            btnPrev: "#slidePagerPrev",
            circular: false,
            visible: 3
        });
    }


    // Case Study Detail Image Gallery
    if ($('#casestudyImageGallery li').length > 1) {
        $('#casestudyImageGallery').after('<div id="caption"></div><span id="slidePagerNext"></span><span id="slidePagerPrev" class="disabled"></span><div id="slidePagerCS"><ul id="casestudyImageGalleryNav"></ul></div>').cycle({
            fx: 'fade',
            delay: 9999999,
            speed: 1000,
            timeout: 12000,
            pagerEvent: 'mouseover',
            pager:  '#casestudyImageGalleryNav',
            before:  function() {
                $('#caption').html($(this).find('img').attr('alt'));
            },
            pagerAnchorBuilder: function(id, slide) {

                var s=$(slide);
                var imgsource=s.find('img').attr('src');
                var new_src = imgsource.substring(0,imgsource.length-4);

                new_src += imgsource.substring(imgsource.length-4, imgsource.length);
                return '<li><a href="#" style="background-image:url('+ new_src +'); filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='+ new_src +',sizingMethod=scale);}"></a></li>';
                
            } ,
            onPrevNextEvent: function(dir, id, el) {
                if (dir === true) {
                    if (id >= 3) { //replace '3' with your number of visible elements
                        $('#slidePagerNext').click();
                    }
                }
                else {
                    if (id >= 1) {
                        $('#slidePagerPrev').click();
                    }
                }
            }
        });

        $("#slidePagerCS").jCarouselLite({
            btnNext: "#slidePagerNext",
            btnPrev: "#slidePagerPrev",
            circular: false,
            visible: 5
        });
    }




    // give every first and last li a selector as IE doesn't support the CSS3 selectors
    $("ul li:first-child").addClass("firstChild");
    $("ul li:last-child").addClass("lastChild");


    // PIE using JS
    // Include all elements that need PIE below.
    $(function() {
        if (window.PIE) {
            $('.example, #example').each(function() {
                PIE.attach(this);
            });
        }
    });


    // IE6 li hover fix
    if($('#menu')) {
        var sfEls = document.getElementById("menu").getElementsByTagName("li");
        for (var i=0; i<sfEls.length; i++)
        {
            sfEls[i].onmouseover=function()
            {
                this.className+=" sfhover";
            }
            sfEls[i].onmouseout=function()
            {
                this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
            }
        }
    }

    /**
     * Home tabs over fix (for green out the side of the bars).
     * This is done by adding a left/right class to the home tabs container.
     */
    $("#homeTabs ul.middle > li").hover(function() {
        // Remove all of the classes to start
        $("#homeTabs").removeClass("left right");
		var $whichone = $(this).attr("id");
			if ($whichone == "homeHoverPanelLeft") {
				$('#homeTabs').addClass('left');
			}
			
			if ($whichone == "homeHoverPanelRight") {
				$('#homeTabs').addClass('right');
			}
    });
    
    $("#homeTabs ul.middle > li").mouseleave(function() {
      $("#homeTabs").removeClass("left right");
    });

    // Mega Menu Display
    $("#menu li")
    .mouseenter(function(){
        $(this).children("div.megaMenu").stop(1,1).fadeIn();
    })
    .mouseleave(function(){
        $(this).children("div.megaMenu").stop(1,1).fadeOut();
    })


    /**
     * Product rental/enquiry forms
     * 
     * Please note these forms have a hidden #product_name field which needs to 
     * be populated by the JS.
     */
    var URLSegments = window.location.pathname.split('/');

    $("a.enquire").click(function(e) {
        e.preventDefault();
        
        $(".error_message").html("");
        
        // This is available on product pages and category product listings
        var productName = $(".productName").html();
        $("#freeform_about").val(productName);
        
        $("div#enquireForm").fadeOut("slow", function() {
            $("#sideBar").addClass("show");
            $("div#enquireForm").fadeIn("slow")
        });
        
        $("div#rentalForm").fadeOut("slow");
    });

    $("a.rent").click(function(e) {

        e.preventDefault();
        
        $(".error_message").html("");
        
        var productName = $(".productName").html();
        //$("#product_rental_form #freeform_product_name").val(productName);
        $("#product_rental_form #freeform_about").val(productName);
        
        $("div#rentalForm").fadeOut("slow", function() {
            $("#sideBar").addClass("show");
            $("div#rentalForm").fadeIn("slow");
        });
		
        $("#content.std.productsSection .middle #copy").animate({
            minHeight: "1030px"
        }, 1500 );
		
        $("div#enquireForm").fadeOut("slow");
    });

    $("a.hideForm").click(function() {
        $("div.sideForm").fadeOut("slow");
        $("#sideBar").removeClass("show");
    });


    // Product Detail Tabs

    $(function () {
        var tabContainers = $('div.tabs > div.block');

        tabContainers.hide().filter(':first').show();

        $('div.tabs ul.tabsNav a').click(function () {
            tabContainers.hide();
            tabContainers.filter(this.hash).show();
            $('div.tabs ul.tabsNav a').removeClass('current');
            $(this).addClass('current');
            return false;
        }).filter(':first').click();
    });


    // Product Detail Top Options

    $('.options a.enquire').hover(
        function(){
            $('.options a.rent').addClass('fade');
        },
        function(){
            $('.options a.rent').removeClass('fade');
        }
        )

    $('.options a.rent').hover(
        function(){
            $('.options a.enquire').addClass('fade');
        },
        function(){
            $('.options a.enquire').removeClass('fade');
        }
        )



    // Product Detail Read More / Read Less

    // override default options (also overrides global overrides)
    $('.description').expander({
        slicePoint:       720,
        expandPrefix:     ' ',
        expandText:       'Read more',
        collapseTimer:    0,
        userCollapseText: 'Read less',
        expandEffect: 'fadeIn',
        expandSpeed: 250,
        collapseEffect: 'fadeOut'
    });


    //Management team page profile pop up


    //add class to a tag with no image in it to be able to remove in lightbox
    $('ul.profileList li a').not(':has(img)').addClass('viewProfile');


    $('ul.profileList li').click(function(e){
        e.preventDefault()
        var thisLi = $(this)
        thisLi.addClass('current').siblings()
        $.fancybox({
            'content'		: thisLi.clone(),
            'autoDimensions': false,
            'width'         : 800,
            'padding'		: 20,
            'overlayOpacity': 0.85,
            'overlayColor'	: '#ffffff',
            'height'        : 450,
            'transitionIn'	: 'elastic',
            'transitionOut'	: 'elastic'
        });
    });

    // Automatically adds wmode=transparent to YouTube Videos.
    // Fixes table hover bug when looking at other tabs.
    $('iframe').each(function() {
        var url = $(this).attr("src");
        if ($(this).attr("src").indexOf("?") > 0) {
            $(this).attr({
                "src" : url + "&wmode=transparent",
                "wmode" : "Opaque"
            });
        }
        else {
            $(this).attr({
                "src" : url + "?wmode=transparent",
                "wmode" : "Opaque"
            });
        }
    });

    // Fades Tool Tip In on Hover


    $("div.location").hover(function(){
        $(this).find(".toolTip").filter(':not(:animated)').fadeIn();
    }, function() {
        $(this).find(".toolTip").fadeOut();
    });


    /**
     * Email Subscription Form Validation
     */
    $("form#frmSS31, form#frmSS32").submit(function(e) {
        e.preventDefault(); // Stop the form submitting until we validate.
        
        var form = $(this);
        
        $(".nameError, .emailError").hide();
        
        var errors = {
            name:false, 
            email: false
        };
        
        if ($("input#name", form).val().length == 0) {
            $(".nameError").show();
            errors.name = true;
        }
        
        if (!validateEmail($("input#email", form).val())) {
            $(".emailError").show();
            errors.email = true;
        }
        
        // If no errors, submit the form.
        if (!errors.email && !errors.name) {
            $(this).unbind('submit').submit();
        }
        
    });
	
	
	
    $('.home #menu li.products').hover(
        function(){
            $('.megaCover').fadeIn('fast')
        },
        function(){
            $('.megaCover').fadeOut('fast')
        }
        )

    $('.home #menu li.services').hover(
        function(){
            $('.megaCover').fadeIn('fast')
        },
        function(){
            $('.megaCover').fadeOut('fast')
        }
        )

    $('#menu li.services').hover(
        function(){
            $('#content.std').animate({
                'opacity':'0.4'
            }, 100)
        },
        function(){
            $('#content.std').animate({
                'opacity':'1.0'
            }, 100)
        }
        )

    $('#menu li.products').hover(
        function(){
            $('#content.std').animate({
                'opacity':'0.4'
            }, 100)
        },
        function(){
            $('#content.std').animate({
                'opacity':'1.0'
            }, 100)
        }
    )

    // Sticky Header

    $(window).scroll(function(event){
        didScroll = true;
    });

    setInterval(function() {
        if (didScroll) {
            hasScrolled();
            didScroll = false;
        }
    }, 250);

    // Mobile Navigation
    $('.mobile-toggle').click(function() {
        if ($('.main_h').hasClass('open-nav')) {
            $('.main_h').removeClass('open-nav');
        } else {
            $('.main_h').addClass('open-nav');
        }
    });

    $('.expander').click(function(e) {
        e.preventDefault();
        $(this).parent().toggleClass('open');
    });

    $('span[style*="color:#47aa42;"]').css('color', '#201c3e');
    $('font[color="#47aa42"]').attr('color', '#201c3e');

    var formHeight = $('#content.landingPage .middle'); /* cache the selector */
    $('.formSide').css({ 'height': formHeight.height() });

});

function hasScrolled() {
    var st = $(this).scrollTop();
    
    // Make sure they scroll more than delta
    if(Math.abs(lastScrollTop - st) <= delta)
        return;
    
    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight){
        // Scroll Down
        $('.main_h').addClass('sticky-up');
    } else {
        // Scroll Up
        if(st + $(window).height() < $(document).height()) {
            $('.main_h').removeClass('sticky-up');
        }
    }
    
    lastScrollTop = st;
}


$(window).load(larg);
$(window).resize(larg);

function larg(){
    
    var heightPromo = $('#homeBanner ul li').height(); 
    $("#homeBanner ul li img").height(heightPromo);
		
    var heightPromo2 = $('#homeBannerMobile ul li').height(); 
    $("#homeBannerMobile ul li img").height(heightPromo);
		
}

/**
 * JS Email Validation
 * 
 * @return <boolean>
 */
function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 


// PIE using JS
$(function() {
    if (window.PIE) {
        $('#content.std #sideBar form ul li input, a.largeBtn, .largeBtn, footer .mailingList input, header .search form input.text, header .search form input.submit, #content.std #sideBar form ul li textarea, #copy.productDetail .productMedia span.imageCover, #copy.productDetail .productInformation .tabs .tabsNav li a, #fancybox-outer, #fancybox-content, .lt-ie9 span.toolTip' ).each(function() {
            PIE.attach(this);
        });
    }
});

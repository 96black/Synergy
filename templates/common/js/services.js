jQuery(document).ready(function($){

    /**
     * Services Enquiry Form
     */
    var enquiryForm = $("#service_enquiry_form");
    var errorDiv = $("#formErrors");

    enquiryForm.submit(function(e) {

        // Stop the form from submitting
        e.preventDefault();

        // Empty out the error container
        errorDiv.html("");
        $(".error_message").html("");
        
        $("#formSuccess").hide()

        //jquery ajax shortcut
        $.post(
            enquiryForm.attr('action'),
            enquiryForm.serialize(),
            function(response) {
                // If there were errors...
                if (response.success == false) {
                    $.each(response.errors, function(i, item){

                        var errorHolder = $('[name="' + i + '"]').
                        parent().find('.error_message');

                        var error = ($.isArray(item) ? item.join('<br/>') : item);

                        //does the error holder field exist?
                        if (errorHolder.length > 0) {
                            errorHolder.append('<p>' + error + '</p>').show();
                        } else {
                            console.log("Appending")
                            errorDiv.append('<p>' + error + '</p>').show();
                            console.log(errorDiv.html())
                        }
                    });
                } else if (response.success) {
                    $("#formSuccess").show()
                    $("input[type=text]", enquiryForm).val("");
                    $("textarea", enquiryForm).val("").html("");
                }
            });

        e.preventDefault();
        return false;
    });
    
    
    
    
    
    /**
     * Product Rental Form
     */
    var rentalForm = $("#product_rental_form");
    var errorDiv = $("#rentalFormErrors");

    rentalForm.submit(function(e) {

        // Stop the form from submitting
        e.preventDefault();

        // Empty out the error container
        errorDiv.html("");
        $(".error_message").html("");
        
        $("#rentalFormSuccess").hide()

        //jquery ajax shortcut
        $.post(
            rentalForm.attr('action'),
            rentalForm.serialize(),
            function(response) {
                // If there were errors...
                if (response.success == false) {
                    $.each(response.errors, function(i, item){

                        console.log(i);
                        console.log(item);
                        console.log("============================");

                        var errorHolder = $('[name="' + i + '"]').
                        parent().find('.error_message');

                        var error = ($.isArray(item) ? item.join('<br/>') : item);

                        //does the error holder field exist?
                        if (errorHolder.length > 0) {
                            errorHolder.append('<p>' + error + '</p>').show();
                        } else {
                            console.log("Appending")
                            errorDiv.append('<p>' + error + '</p>').show();
                            console.log(errorDiv.html())
                        }
                    });
                } else if (response.success) {
                    $("#rentalFormSuccess").show()
                    $("input[type=text]", enquiryForm).val("");
                    $("textarea", enquiryForm).val("").html("");
                }
            });

        e.preventDefault();
        return false;
    });
});
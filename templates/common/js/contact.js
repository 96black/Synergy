/**
 * Contact us page specific JS
 */
jQuery(document).ready(function($){
    
    // Load up the Google map
    initializeGmap();

    
    
    /**
     * Change the branch page map when the branch is clicked.
     */
    /*
    $("a.mapSwitcher").click(function() {
        // Hide all the other map images
        $("img.mapImage").hide();
        
        // Show the current map
        $("#map_" + $(this).attr("id")).show();
        $("html, body").animate({
            scrollTop: 0
        }, "slow");
    });
    */
   
    /**
     * Enable clicking on the tool tip (cheat and click the previous map switcher)
     */
    /*
    $("span.toolTip").click(function() {
        $("a.mapSwitcher", $(this).parent()).trigger("click");
    })
    */

});


var map, lat, lng, initialLocation, addressString;
// NZ Map
if ($("#mapCanvas").hasClass("NZMap")) {
    lat = -36.74206;
    lng = 174.72703;
    initialLocation = new google.maps.LatLng(lat, lng);
    addressString = "<b>Auckland Office</b><br />";
    addressString += "3/52 Arrenway Drive, <br />Albany, <br />Auckland, <br />New Zealand";
} 
// AUS Map
else {
    lat = -27.66400;
    lng = 153.14259;
    initialLocation = new google.maps.LatLng(lat, lng);
    addressString = "<b>Queensland Office</b><br />";
    addressString += "U3/17 University Drive, <br />Meadowbrook, <br />QLD 4131, <br />Australia";
}


/**
 * Initialize the Google map on the contact page.
 */
function initializeGmap() {
    
    var infowindow = new google.maps.InfoWindow();

    var mapOptions = {
        center: new google.maps.LatLng(lat, lng),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl:false
    };
    
    map = new google.maps.Map(document.getElementById('mapCanvas'), mapOptions);


    // Custom marker icon
    var image = new google.maps.MarkerImage('/templates/common/images/map-pin.png',
        // This marker is 40 pixels wide by 60 pixels tall.
        new google.maps.Size(37, 45),
        // The origin for this image is 0,0.
        new google.maps.Point(0,0),
        // The anchor for this image is the base of the flagpole at 0,32.
        new google.maps.Point(18, 22)
        );
    var shadow = new google.maps.MarkerImage('/templates/common/images/map-pin-shadow.png',
        // The shadow image is larger in the horizontal dimension
        // while the position and offset are the same as for the main image.
        new google.maps.Size(32, 14),
        new google.maps.Point(0,0),
        new google.maps.Point(0, 0)
        );
            
    // Build the marker and add it to the map.
    var marker = new google.maps.Marker({
        position: initialLocation,
        map: map,
        icon: image,
        shadow: shadow
    });
    
    
    // Onclick event to show the info window popup.
    google.maps.event.addListener(marker, 'click', function() {
       infowindow.setContent(addressString);
       infowindow.open(map,marker);
    });
}
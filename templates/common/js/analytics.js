jQuery(document).ready(function($) {
    
    /**
     * Attach a Google analytics tracking call to a link with the "trackDownload" 
     * class.
     */
    $("a.trackDownload").each(function(index) {
        
        var linkEl = $(this);
        
        // Get the file name (if not supplied, read from URL)
        var documentName = "";
        if (typeof $(this).data("trackname") !== "undefined") {
            documentName = $(this).data("trackname");
        } else {
            documentName = $(this).attr("href").trim();
        }
        
        // Remove the trailing slash if there is one
        documentName = documentName.trim().replace(/\/$/, "");
        documentName = /[^|]*$/.exec(documentName)[0]; // For the asset repo, take from the last |
        documentName = /[^/]*$/.exec(documentName)[0];
        
        // Get whether it's an "Asset Repository Download" etc
        var documentType = $(this).data("tracktype");
        
        // Build the GA track string and create an onclick attribute.
        var trackString = "ga('send', 'event', 'Document Download', '" + documentType + "', '" + documentName + "')";
        
        linkEl.attr("onclick", trackString);
    });
    
    
    /**
     * Analytics tracking for email links
     */
    $('a[href^="mailto:"]').each(function() {
        
        var linkEl = $(this);
        
        var trackString = "ga('send', 'event', 'Email Link Click', 'Email Link Click', '" + linkEl.attr("href") + "')";
        
        linkEl.attr("onclick", trackString);
        
    });
    
    
    /**
     * Track the product rental and enquiry form submissions.
     */
    $("#product_enquiry_form, #product_rental_form, #service_enquiry_form").submit(function (e) {
        
        // Which type of form did we submit?
        var mode = "";
        var prodOrServiceName = "";
        if ($(this).attr("id") == "product_rental_form") {
            mode = "Rental"
            prodOrServiceName = $("#product_rental_form #freeform_about").val();
        } else if ($(this).attr("id") == "product_enquiry_form") {
            mode = "Enquiry";
            prodOrServiceName = $("#freeform_about").val();
        } else {
            mode = "Service enquiry";
            prodOrServiceName = $("#freeform_about").val();
        }
        
        // Trigger the GA event
        ga( 
            'send', 
            'event', 
            'Form Submission', 
            mode + " Form Submission", 
            prodOrServiceName
        );
    });
    
});
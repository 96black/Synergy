/* Table structure for table `exp_accessories` */
DROP TABLE IF EXISTS `exp_accessories`;

CREATE TABLE `exp_accessories` (
  `accessory_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(75) NOT NULL DEFAULT '',
  `member_groups` varchar(50) NOT NULL DEFAULT 'all',
  `controllers` text,
  `accessory_version` varchar(12) NOT NULL,
  PRIMARY KEY (`accessory_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_accessories` */
INSERT INTO `exp_accessories` VALUES(1, 'Expressionengine_info_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.0');
INSERT INTO `exp_accessories` VALUES(2, 'Mx_cloner_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|sites|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.0.3');

/* Table structure for table `exp_actions` */
DROP TABLE IF EXISTS `exp_actions`;

CREATE TABLE `exp_actions` (
  `action_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_actions` */
INSERT INTO `exp_actions` VALUES(1, 'Comment', 'insert_new_comment');
INSERT INTO `exp_actions` VALUES(2, 'Comment_mcp', 'delete_comment_notification');
INSERT INTO `exp_actions` VALUES(3, 'Comment', 'comment_subscribe');
INSERT INTO `exp_actions` VALUES(4, 'Comment', 'edit_comment');
INSERT INTO `exp_actions` VALUES(5, 'Email', 'send_email');
INSERT INTO `exp_actions` VALUES(6, 'Safecracker', 'submit_entry');
INSERT INTO `exp_actions` VALUES(7, 'Safecracker', 'combo_loader');
INSERT INTO `exp_actions` VALUES(8, 'Search', 'do_search');
INSERT INTO `exp_actions` VALUES(9, 'Channel', 'insert_new_entry');
INSERT INTO `exp_actions` VALUES(10, 'Channel', 'filemanager_endpoint');
INSERT INTO `exp_actions` VALUES(11, 'Channel', 'smiley_pop');
INSERT INTO `exp_actions` VALUES(12, 'Member', 'registration_form');
INSERT INTO `exp_actions` VALUES(13, 'Member', 'register_member');
INSERT INTO `exp_actions` VALUES(14, 'Member', 'activate_member');
INSERT INTO `exp_actions` VALUES(15, 'Member', 'member_login');
INSERT INTO `exp_actions` VALUES(16, 'Member', 'member_logout');
INSERT INTO `exp_actions` VALUES(17, 'Member', 'retrieve_password');
INSERT INTO `exp_actions` VALUES(18, 'Member', 'reset_password');
INSERT INTO `exp_actions` VALUES(19, 'Member', 'send_member_email');
INSERT INTO `exp_actions` VALUES(20, 'Member', 'update_un_pw');
INSERT INTO `exp_actions` VALUES(21, 'Member', 'member_search');
INSERT INTO `exp_actions` VALUES(22, 'Member', 'member_delete');
INSERT INTO `exp_actions` VALUES(23, 'Rte', 'get_js');
INSERT INTO `exp_actions` VALUES(24, 'Playa_mcp', 'filter_entries');
INSERT INTO `exp_actions` VALUES(25, 'Freeform', 'save_form');

/* Table structure for table `exp_captcha` */
DROP TABLE IF EXISTS `exp_captcha`;

CREATE TABLE `exp_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_categories` */
DROP TABLE IF EXISTS `exp_categories`;

CREATE TABLE `exp_categories` (
  `cat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `parent_id` int(4) unsigned NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_url_title` varchar(75) NOT NULL,
  `cat_description` text,
  `cat_image` varchar(120) DEFAULT NULL,
  `cat_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `group_id` (`group_id`),
  KEY `cat_name` (`cat_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_categories` */
INSERT INTO `exp_categories` VALUES(1, 1, 1, 0, 'UAV', 'uav', '', '{filedir_3}cat-image.jpg', 3);
INSERT INTO `exp_categories` VALUES(2, 1, 1, 0, 'Optical', 'optical', 'Category description here', '{filedir_3}cat-image.jpg', 2);
INSERT INTO `exp_categories` VALUES(3, 1, 1, 2, 'Total Station', 'total-station', '', '{filedir_3}cat-image.jpg', 3);
INSERT INTO `exp_categories` VALUES(4, 1, 1, 3, 'Robotic', 'robotic', '', '{filedir_3}cat-image.jpg', 2);
INSERT INTO `exp_categories` VALUES(5, 1, 1, 3, 'Windows Based', 'windows-based', '', '{filedir_3}cat-image.jpg', 3);
INSERT INTO `exp_categories` VALUES(6, 1, 1, 3, 'Construction', 'construction', '', '{filedir_3}cat-image.jpg', 1);
INSERT INTO `exp_categories` VALUES(7, 1, 1, 0, 'GPS Equipment', 'gps-equipment', 'Lorem ipsum stuff', '{filedir_3}cat-image.jpg', 1);
INSERT INTO `exp_categories` VALUES(8, 1, 1, 2, 'Levels', 'levels', 'Test content', '{filedir_3}cat-image.jpg', 1);
INSERT INTO `exp_categories` VALUES(9, 1, 1, 2, 'Theyodolites', 'theyodolites', 'Test content', '{filedir_3}cat-image.jpg', 2);
INSERT INTO `exp_categories` VALUES(10, 1, 2, 0, 'Roads And Surfaces', 'roads-and-surfaces', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim. ', '{filedir_3}services-temp-top.jpg', 3);
INSERT INTO `exp_categories` VALUES(11, 1, 2, 0, 'Mapping', 'mapping', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim. ', '{filedir_3}services-temp-top.jpg', 2);
INSERT INTO `exp_categories` VALUES(12, 1, 2, 0, 'Lorem ipsum', 'lorem-ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim. ', '{filedir_3}services-temp-top.jpg', 1);

/* Table structure for table `exp_category_field_data` */
DROP TABLE IF EXISTS `exp_category_field_data`;

CREATE TABLE `exp_category_field_data` (
  `cat_id` int(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_category_field_data` */
INSERT INTO `exp_category_field_data` VALUES(1, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(2, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(3, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(4, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(5, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(6, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(7, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(8, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(9, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(10, 1, 2);
INSERT INTO `exp_category_field_data` VALUES(11, 1, 2);
INSERT INTO `exp_category_field_data` VALUES(12, 1, 2);

/* Table structure for table `exp_category_fields` */
DROP TABLE IF EXISTS `exp_category_fields`;

CREATE TABLE `exp_category_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `field_label` varchar(50) NOT NULL DEFAULT '',
  `field_type` varchar(12) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_maxl` smallint(3) NOT NULL DEFAULT '128',
  `field_ta_rows` tinyint(2) NOT NULL DEFAULT '8',
  `field_default_fmt` varchar(40) NOT NULL DEFAULT 'none',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`field_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_category_groups` */
DROP TABLE IF EXISTS `exp_category_groups`;

CREATE TABLE `exp_category_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `sort_order` char(1) NOT NULL DEFAULT 'a',
  `exclude_group` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `can_edit_categories` text,
  `can_delete_categories` text,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_category_groups` */
INSERT INTO `exp_category_groups` VALUES(1, 1, 'Product Categories', 'a', 0, 'all', '', '');
INSERT INTO `exp_category_groups` VALUES(2, 1, 'Service Categories', 'a', 0, 'all', '', '');

/* Table structure for table `exp_category_posts` */
DROP TABLE IF EXISTS `exp_category_posts`;

CREATE TABLE `exp_category_posts` (
  `entry_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_category_posts` */
INSERT INTO `exp_category_posts` VALUES(1, 2);
INSERT INTO `exp_category_posts` VALUES(1, 3);
INSERT INTO `exp_category_posts` VALUES(1, 6);
INSERT INTO `exp_category_posts` VALUES(2, 2);
INSERT INTO `exp_category_posts` VALUES(2, 3);
INSERT INTO `exp_category_posts` VALUES(2, 6);
INSERT INTO `exp_category_posts` VALUES(3, 2);
INSERT INTO `exp_category_posts` VALUES(3, 3);
INSERT INTO `exp_category_posts` VALUES(3, 6);
INSERT INTO `exp_category_posts` VALUES(26, 10);

/* Table structure for table `exp_channel_data` */
DROP TABLE IF EXISTS `exp_channel_data`;

CREATE TABLE `exp_channel_data` (
  `entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `field_id_1` text,
  `field_ft_1` tinytext,
  `field_id_2` text,
  `field_ft_2` tinytext,
  `field_id_3` text,
  `field_ft_3` tinytext,
  `field_id_4` text,
  `field_ft_4` tinytext,
  `field_id_5` text,
  `field_ft_5` tinytext,
  `field_id_6` text,
  `field_ft_6` tinytext,
  `field_id_7` text,
  `field_ft_7` tinytext,
  `field_id_8` text,
  `field_ft_8` tinytext,
  `field_id_9` text,
  `field_ft_9` tinytext,
  `field_id_10` text,
  `field_ft_10` tinytext,
  `field_id_11` text,
  `field_ft_11` tinytext,
  `field_id_12` text,
  `field_ft_12` tinytext,
  `field_id_13` text,
  `field_ft_13` tinytext,
  `field_id_14` text,
  `field_ft_14` tinytext,
  `field_id_15` text,
  `field_ft_15` tinytext,
  `field_id_16` int(10) DEFAULT '0',
  `field_ft_16` tinytext,
  `field_id_17` text,
  `field_ft_17` tinytext,
  `field_id_18` text,
  `field_ft_18` tinytext,
  `field_id_19` text,
  `field_ft_19` tinytext,
  `field_id_20` text,
  `field_ft_20` tinytext,
  `field_id_22` text,
  `field_ft_22` tinytext,
  `field_id_23` text,
  `field_ft_23` tinytext,
  `field_id_24` text,
  `field_ft_24` tinytext,
  `field_id_25` text,
  `field_ft_25` tinytext,
  `field_id_26` text,
  `field_ft_26` tinytext,
  `field_id_27` text,
  `field_ft_27` tinytext,
  `field_id_28` text,
  `field_ft_28` tinytext,
  `field_id_29` text,
  `field_ft_29` tinytext,
  `field_id_30` text,
  `field_ft_30` tinytext,
  `field_id_31` text,
  `field_ft_31` tinytext,
  `field_id_32` text,
  `field_ft_32` tinytext,
  `field_id_33` text,
  `field_ft_33` tinytext,
  `field_id_34` text,
  `field_ft_34` tinytext,
  `field_id_35` text,
  `field_ft_35` tinytext,
  `field_id_36` text,
  `field_ft_36` tinytext,
  `field_id_37` int(10) DEFAULT '0',
  `field_ft_37` tinytext,
  `field_id_38` text,
  `field_ft_38` tinytext,
  `field_id_39` text,
  `field_ft_39` tinytext,
  `field_id_40` text,
  `field_ft_40` tinytext,
  `field_id_42` text,
  `field_ft_42` tinytext,
  `field_id_43` text,
  `field_ft_43` tinytext,
  `field_id_44` text,
  `field_ft_44` tinytext,
  `field_id_45` text,
  `field_ft_45` tinytext,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_channel_data` */
INSERT INTO `exp_channel_data` VALUES(1, 1, 1, 'NZD\nAUD\n', 'none', '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', 'Yes', 'none', '1', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(2, 1, 1, 'NZD\nAUD\n', 'none', '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', 'Yes', 'none', '1', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '{filedir_1}mega-menu-feature-product-image_(1).jpg', 'none', '', 'none', '', 'none', '', 'none', 5, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(3, 1, 1, 'NZD\nAUD\n', 'none', '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '', 'none', '', 'none', '1', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(4, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'All', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(5, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'All', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(6, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'All', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(7, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'All', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(8, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'All', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(9, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'All', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(10, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'All', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(11, 1, 4, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quam justo, egestas sed tincidunt id, feugiat eu urna. Suspendisse ornare, magna cursus tristique ullamcorper, massa velit ultricies metus, eu ultrices massa eros nec tortor. Sed auctor vehicula quam, ac condimentum mauris tristique non. Sed sed sodales nisi. Aliquam erat volutpat. Vivamus sit amet felis et urna lacinia ullamcorper eget et metus. Nunc laoreet eleifend molestie.</p>', 'none', '1', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', 'NZ\nAU\n', 'none', '1', 'none', '<p>\n	G&#39;day Mate.</p>\n<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quam justo, egestas sed tincidunt id, feugiat eu urna. Suspendisse ornare, magna cursus tristique ullamcorper, massa velit ultricies metus, eu ultrices massa eros nec tortor. Sed auctor vehicula quam, ac condimentum mauris tristique non. Sed sed sodales nisi. Aliquam erat volutpat. Vivamus sit amet felis et urna lacinia ullamcorper eget et metus. Nunc laoreet eleifend molestie.</p>', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(12, 1, 2, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 1, 'none', '1', 'none', 'The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(13, 1, 2, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 2, 'none', '1', 'none', 'The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(14, 1, 2, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 3, 'none', '1', 'none', 'The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(15, 1, 2, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 4, 'none', '1', 'none', 'The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(16, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', 'All', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(17, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', 'All', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(18, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', 'All', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(19, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', 'All', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(20, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', 'All', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(21, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', 'All', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(22, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', 'All', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(23, 1, 8, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '1', 'none', '1', 'none', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non felis dignissim leo faucibus commodo. Aliquam urna velit, sollicitudin eget mollis ac, condimentum quis sapien. Aliquam aliquam vestibulum libero. Donec scelerisque lorem sollicitudin leo rhoncus eget dignissim felis blandit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ', 'none', '{filedir_6}services.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(24, 1, 8, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '1', 'none', '1', 'none', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non felis dignissim leo faucibus commodo. Aliquam urna velit, sollicitudin eget mollis ac, condimentum quis sapien. Aliquam aliquam vestibulum libero. Donec scelerisque lorem sollicitudin leo rhoncus eget dignissim felis blandit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ', 'none', '{filedir_6}products.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(25, 1, 8, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '1', 'none', '1', 'none', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non felis dignissim leo faucibus commodo. Aliquam urna velit, sollicitudin eget mollis ac, condimentum quis sapien. Aliquam aliquam vestibulum libero. Donec scelerisque lorem sollicitudin leo rhoncus eget dignissim felis blandit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ', 'none', '{filedir_6}about.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(26, 1, 7, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '1', 'none', '{filedir_7}services-temp-top.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(27, 1, 10, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<h1>\n	<strong style=\"font-size: 12px;\">Synergy Positioning Systems Ltd is 100% New Zealand owned and operated. It was formed when two of the industry&rsquo;s most trusted companies merged in 2006 - Synergy Equipment and Systems Ltd and Geodetic Instruments Ltd. The result is a company that brings you over 30 years experience delivering the most advanced and innovative technologies combined with a commitment to outstanding customer service.</strong></h1>\n<p>\n	Our aim is to make your job faster and easier, more accurate and cost efficient. We&#39;ll do this by giving you expert and honest advice and by offering you the best solution from our range of innovative positioning systems.<br />\n	Reliability is paramount - equipment breakages cost time and money. There are many low quality brands on the market that just don&#39;t stand up to the rigours of our construction industry. We only sell reliable brands we can trust and service.</p>\n<p>\n	Our experienced team are always on hand for after sales support, product training and servicing, giving you the confidence and skills to get the best out of your equipment. Our product range is extensive and always growing.</p>\n<h2>\n	Why choose synergy?</h2>', 'none', '1', 'none', '{filedir_8}about-temp.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '1', 'none', '<p>\n	<strong>AUS CONTENT Synergy Positioning Systems Ltd is 100% New Zealand owned and operated. It was formed when two of the industry&rsquo;s most trusted companies merged in 2006 - Synergy Equipment and Systems Ltd and Geodetic Instruments Ltd. The result is a company that brings you over 30 years experience delivering the most advanced and innovative technologies combined with a commitment to outstanding customer service.</strong></p>\n<p>\n	Our aim is to make your job faster and easier, more accurate and cost efficient. We&#39;ll do this by giving you expert and honest advice and by offering you the best solution from our range of innovative positioning systems.<br />\n	Reliability is paramount - equipment breakages cost time and money. There are many low quality brands on the market that just don&#39;t stand up to the rigours of our construction industry. We only sell reliable brands we can trust and service.</p>\n<p>\n	Our experienced team are always on hand for after sales support, product training and servicing, giving you the confidence and skills to get the best out of your equipment. Our product range is extensive and always growing.</p>\n<h2>\n	Why choose synergy?</h2>', 'none', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(28, 1, 9, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<p>\n	<strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</strong></p>\n<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet.</p>', 'none', '', null, '', null, '', null, 'NZ\nAU\n', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(29, 1, 11, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '{filedir_10}footer-topcon.gif', 'none', '{filedir_10}topcon-logo.gif', 'none', '{filedir_10}mega-menu-feature-product-logo.jpg', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(30, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<p>\n	Test NZ article</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, 'NZ', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(31, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<p>\n	AU only!</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, 'AU', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(32, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	NZ only case study</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, 'NZ', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(33, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	AU only.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, 'AU', 'xhtml');

/* Table structure for table `exp_channel_entries_autosave` */
DROP TABLE IF EXISTS `exp_channel_entries_autosave`;

CREATE TABLE `exp_channel_entries_autosave` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original_entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  `entry_data` text,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_channel_fields` */
DROP TABLE IF EXISTS `exp_channel_fields`;

CREATE TABLE `exp_channel_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL,
  `field_label` varchar(50) NOT NULL,
  `field_instructions` text,
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_pre_populate` char(1) NOT NULL DEFAULT 'n',
  `field_pre_channel_id` int(6) unsigned DEFAULT NULL,
  `field_pre_field_id` int(6) unsigned DEFAULT NULL,
  `field_related_to` varchar(12) NOT NULL DEFAULT 'channel',
  `field_related_id` int(6) unsigned NOT NULL DEFAULT '0',
  `field_related_orderby` varchar(12) NOT NULL DEFAULT 'date',
  `field_related_sort` varchar(4) NOT NULL DEFAULT 'desc',
  `field_related_max` smallint(4) NOT NULL DEFAULT '0',
  `field_ta_rows` tinyint(2) DEFAULT '8',
  `field_maxl` smallint(3) DEFAULT NULL,
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_search` char(1) NOT NULL DEFAULT 'n',
  `field_is_hidden` char(1) NOT NULL DEFAULT 'n',
  `field_fmt` varchar(40) NOT NULL DEFAULT 'xhtml',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_order` int(3) unsigned NOT NULL,
  `field_content_type` varchar(20) NOT NULL DEFAULT 'any',
  `field_settings` text,
  PRIMARY KEY (`field_id`),
  KEY `group_id` (`group_id`),
  KEY `field_type` (`field_type`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_channel_fields` */
INSERT INTO `exp_channel_fields` VALUES(1, 1, 1, 'product_prices', 'Product Prices', 'Enter the AU and NZ prices for this product in here and they will be dynamically output to the customer based on the website they are viewing.', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjIiO3M6ODoibWF4X3Jvd3MiO3M6MToiMiI7czo3OiJjb2xfaWRzIjthOjI6e2k6MDtzOjE6IjEiO2k6MTtzOjE6IjIiO319');
INSERT INTO `exp_channel_fields` VALUES(2, 1, 1, 'product_description', 'Product Description', 'On the product page, only the first couple of paragraphs will be shown until the user presses \"read more\"', 'wygwam', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIyIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(3, 1, 1, 'technical_specifications', 'Technical Specifications', 'Below you can create the technical specifications section for this product. Once a title is specified in the left column (i.e. Measurement Range) you can create a table in the right column of specifications relevant to this title. ', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MToiMyI7aToxO3M6MToiNCI7fX0=');
INSERT INTO `exp_channel_fields` VALUES(4, 1, 1, 'product_videos', 'Product Videos', 'Use this field to embed a Youtube video and give a title/description to the video. Any videos will appear under the \"Videos\" tab for this product.', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 4, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mzp7aTowO2k6NjtpOjE7aTo3O2k6MjtpOjg7fX0=');
INSERT INTO `exp_channel_fields` VALUES(5, 1, 1, 'rental_sections', 'Rental Sections', 'Add as many paragraphs to the rental section as you want.', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO2k6OTtpOjE7aToxMDt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(6, 1, 1, 'rental_pricing_nz', 'Rental Pricing (NZ)', 'This field controls the contents of the \"Availability & Costs\" section under the rental tab (for the NZ site).', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 6, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMTEiO2k6MTtzOjI6IjEyIjt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(7, 1, 1, 'rental_pricing_au', 'Rental Pricing (AU)', 'This field controls the contents of the \"Availability & Costs\" section under the rental tab (for the AU site).', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 7, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMTMiO2k6MTtzOjI6IjE0Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(8, 1, 1, 'featured_carousel_images', 'Featured Carousel Images', 'If this product is to be a featured product, you can upload many banner images to appear at the top of its featured page.\n\nThe banner will cycle between these images.', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 8, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMTUiO2k6MTtzOjI6IjI0Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(9, 1, 1, 'is_featured_product', 'Is Featured Product?', 'If you want this product to be a featured product, tick this checkbox.\n\nPlease make sure you have created some featured product entries for this product first (in the publish menu).', 'checkboxes', 'Yes', 'n', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 9, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');
INSERT INTO `exp_channel_fields` VALUES(10, 1, 1, 'product_images', 'Product Images', '', 'matrix', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 10, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6MTp7aTowO3M6MjoiMTYiO319');
INSERT INTO `exp_channel_fields` VALUES(11, 1, 1, 'product_downloads', 'Product Downloads', 'Upload files to be available in the \"downloads\" tab', 'matrix', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 11, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMTciO2k6MTtzOjI6IjE4Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(12, 1, 3, 'article_content', 'Article Content', 'This is the main body content of the news item. Please note the first paragraph will be automatically made bold.', 'wygwam', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(13, 1, 3, 'article_banner_image', 'Article Banner Image', 'This is the image that appears at the top of the news article and on the news page. Please note the images should be exactly 866px by 373px and will have a thumbnail automatically cropped to 396px by 88px for the news page.', 'file', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiNCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(14, 1, 4, 'contact_introduction', 'Contact Introduction', 'The initial content before the branch information.', 'wygwam', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(15, 1, 4, 'branches', 'Branches', 'Please note the first branch entered here will be shown on the home page for the New Zealand site.', 'matrix', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'y', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6NTp7aTowO3M6MjoiMTkiO2k6MTtzOjI6IjIwIjtpOjI7czoyOiIyMSI7aTozO3M6MjoiMjIiO2k6NDtzOjI6IjIzIjt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(16, 1, 2, 'featured_product', 'Featured Product', 'Pick the featured product this sub page is related to.', 'rel', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'y', 'n', 'none', 'n', 1, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');
INSERT INTO `exp_channel_fields` VALUES(17, 1, 2, 'featured_content', 'Featured Content', 'Each block of content you add will create a new block separated by a horizontal rule.', 'matrix', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6MTp7aTowO3M6MjoiMjUiO319');
INSERT INTO `exp_channel_fields` VALUES(18, 1, 2, 'featured_content_intro', 'Featured Content Intro', 'This is shown on the featured product page as the preview for this page.', 'textarea', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');
INSERT INTO `exp_channel_fields` VALUES(19, 1, 5, 'study_content', 'Study Content', 'This is the main body content of the news item. Please note the first paragraph will be automatically made bold.', 'wygwam', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(20, 1, 5, 'study_banner_image', 'Study Banner Image', '', 'file', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiNSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(22, 1, 7, 'home_marketing_message', 'Home Marketing Message', 'This is the large and smaller text that appears over the top of the banner image.', 'matrix', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO2k6MjY7aToxO2k6Mjc7fX0=');
INSERT INTO `exp_channel_fields` VALUES(23, 1, 7, 'home_banner_links', 'Home Banner Links', 'This field controls the content and location of the home page banner links (both over the banner and below in the tabs).', 'matrix', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 0, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMjgiO2k6MTtzOjI6IjI5Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(24, 1, 7, 'home_banner_tab_content', 'Home Banner Tab Content', 'The content of the tabs below the banners. A limit of 190 characters will be shown from this box.', 'textarea', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 3, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 4, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');
INSERT INTO `exp_channel_fields` VALUES(25, 1, 7, 'home_banner_image', 'Home Banner Image', 'This is the large background image shown in the banner.', 'file', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiNiI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(26, 1, 6, 'service_content', 'Service Content', 'Each block of content you add will create a new block separated by a horizontal rule.', 'matrix', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6MTp7aTowO2k6MzA7fX0=');
INSERT INTO `exp_channel_fields` VALUES(27, 1, 6, 'services_banner', 'Services Banner', 'An image with dimensions of 866px - 305px is best.', 'file', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiNyI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(28, 1, 8, 'management_page_content', 'Page Content', 'This is the content at the top of the management page.', 'wygwam', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo5OntzOjc6ImNvbnZlcnQiO3M6MDoiIjtzOjY6ImNvbmZpZyI7czoxOiI1IjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(29, 1, 9, 'about_page_content', 'Page Content', '', 'wygwam', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiI1IjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(30, 1, 9, 'about_us_grid', 'About Us Grid', 'Add as many blocks as you want to the bottom of the about us page', 'matrix', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMzIiO2k6MTtzOjI6IjMxIjt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(31, 1, 9, 'about_page_banner', 'About Page Banner', 'This should be a banner image ideally 866px x 305px, however it may be larger. The image will be constricted width-wise to 866px.', 'file', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiOCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(32, 1, 8, 'staff_members', 'Staff Members', 'Enter a row for each staff member here.', 'matrix', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6NDp7aTowO3M6MjoiMzMiO2k6MTtzOjI6IjM0IjtpOjI7czoyOiIzNSI7aTozO2k6NDY7fX0=');
INSERT INTO `exp_channel_fields` VALUES(33, 1, 1, 'featured_megamenu_icon', 'Featured Megamenu Icon', 'This is the small image that appears in the mega menu for features products. This needs to be an image with the dimensions 302px x 89px and will be constrained to this when displayed. Please note if this image is not supplied on a featured product, it will not be displayed in the featured section of the products megamenu.', 'file', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 12, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiMSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(34, 1, 10, 'manufacturer_footer_image', 'Footer Image', 'This image is output in the footer. It should be a grey/white text on a background colour of #3c3c3c.\nIt must be 30px in height and no wider than 225px (it can be smaller).', 'file', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MjoiMTAiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(35, 1, 10, 'manufacturer_product_page_image', 'Product Page Image', 'This image is used on the product page when you select this manufacturer for the given product. The logo in the design is 140px wide and 24px high (so we would recommend something similar), however you can upload different sizes as long as they fit in your page.', 'file', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MjoiMTAiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(36, 1, 10, 'featured_product_megamenu_image', 'Featured Product Megamenu Image', 'This image is only ever output in the product mega menu if a product with this manufacturer is a featured product. This is a fixed 166px by 49px image and will be constrained to this. Please note if this is not set and this product is featured, the product will not show in the featured megamenu block.', 'file', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 3, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MjoiMTAiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(37, 1, 1, 'manufacturer', 'Manufacturer', 'This field is used to display the manufacturer logo on the product page.', 'rel', '', '0', 0, 0, 'channel', 11, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 13, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');
INSERT INTO `exp_channel_fields` VALUES(38, 1, 4, 'header_contact_details', 'Header Contact Details', 'This is the contact information that is shown in the header of the website.', 'matrix', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjIiO3M6ODoibWF4X3Jvd3MiO3M6MToiMiI7czo3OiJjb2xfaWRzIjthOjM6e2k6MDtzOjI6IjM4IjtpOjE7czoyOiIzNiI7aToyO3M6MjoiMzciO319');
INSERT INTO `exp_channel_fields` VALUES(39, 1, 4, 'branches_au', 'Branches', 'Please note the first branch entered here will be shown on the home page for the Australian site.', 'matrix', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'y', 'n', 'none', 'n', 4, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6NTp7aTowO3M6MjoiMzkiO2k6MTtzOjI6IjQwIjtpOjI7czoyOiI0MSI7aTozO2k6NDI7aTo0O2k6NDM7fX0=');
INSERT INTO `exp_channel_fields` VALUES(40, 1, 4, 'contact_introduction_au', 'Contact Introduction', 'The initial content before the branch information.', 'wygwam', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(42, 1, 9, 'about_us_grid_au', 'About Us Grid', 'Add as many blocks as you want to the bottom of the about us page', 'matrix', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiNDQiO2k6MTtzOjI6IjQ1Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(43, 1, 9, 'about_page_content_au', 'Page Content', '', 'wygwam', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiI1IjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(44, 1, 3, 'article_country', 'Article Country', 'Use this to select whether the article should only be available on NZ, AU or both countries.', 'pt_dropdown', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'y', 'n', 'xhtml', 'n', 3, 'any', 'YTo3OntzOjc6Im9wdGlvbnMiO2E6Mzp7czozOiJBbGwiO3M6MzoiQWxsIjtzOjI6Ik5aIjtzOjI6Ik5aIjtzOjI6IkFVIjtzOjI6IkFVIjt9czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(45, 1, 5, 'study_country', 'Study Country', 'Use this to select whether the case study should only be available on NZ, AU or both countries.', 'pt_dropdown', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'y', 'n', 'xhtml', 'n', 3, 'any', 'YTo3OntzOjc6Im9wdGlvbnMiO2E6Mzp7czozOiJBbGwiO3M6MzoiQWxsIjtzOjI6Ik5aIjtzOjI6Ik5aIjtzOjI6IkFVIjtzOjI6IkFVIjt9czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');

/* Table structure for table `exp_channel_member_groups` */
DROP TABLE IF EXISTS `exp_channel_member_groups`;

CREATE TABLE `exp_channel_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `channel_id` int(6) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_channel_titles` */
DROP TABLE IF EXISTS `exp_channel_titles`;

CREATE TABLE `exp_channel_titles` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_channel_titles` */
INSERT INTO `exp_channel_titles` VALUES(1, 1, 1, 1, 0, null, '127.0.0.1', 'Topcon GTS-100N Construction', 'topcon-gts-100n-construction', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358293518, 'n', '2013', '01', '16', 0, 0, 20130411225519, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(2, 1, 1, 1, 0, null, '127.0.0.1', 'Topcon GTS-100N Robotic', 'topcon-gts-100n-robotic', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358293544, 'n', '2013', '01', '16', 0, 0, 20130412020645, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(3, 1, 1, 1, 0, null, '127.0.0.1', 'Topcon GTS-100N Windows Based', 'topcon-gts-100n-windows-based', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358293500, 'n', '2013', '01', '16', 0, 0, 20130313211401, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(4, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 1', 'news-article-1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961454, 'n', '2013', '03', '11', 0, 0, 20130415223715, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(5, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 2', 'news-article-2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961449, 'n', '2013', '03', '11', 0, 0, 20130415223710, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(6, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 6', 'news-article-6', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961456, 'n', '2013', '03', '11', 0, 0, 20130415223717, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(7, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 5', 'news-article-5', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961446, 'n', '2013', '03', '11', 0, 0, 20130415223707, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(8, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 4', 'news-article-4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961461, 'n', '2013', '03', '11', 0, 0, 20130415223722, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(9, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 3', 'news-article-3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961459, 'n', '2013', '03', '11', 0, 0, 20130415223720, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(10, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 7', 'news-article-7', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961451, 'n', '2013', '03', '11', 0, 0, 20130415223712, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(11, 1, 4, 1, 0, null, '127.0.0.1', 'Contact Us', 'contact-us', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362967301, 'n', '2013', '03', '11', 0, 0, 20130415000242, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(12, 1, 2, 1, 0, null, '127.0.0.1', 'BRAMOR Orthophoto', 'bramor-orthophoto', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363822454, 'n', '2013', '03', '21', 0, 0, 20130320235515, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(13, 1, 2, 1, 0, null, '127.0.0.1', 'BRAMOR Orthophoto 2', 'bramor-orthophoto-2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363822493, 'n', '2013', '03', '21', 0, 0, 20130321123453, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(14, 1, 2, 1, 0, null, '127.0.0.1', 'BRAMOR Orthophoto 3', 'bramor-orthophoto-3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363822446, 'n', '2013', '03', '21', 0, 0, 20130321123406, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(15, 1, 2, 1, 0, null, '127.0.0.1', 'BRAMOR Orthophoto 4', 'bramor-orthophoto-4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363822463, 'n', '2013', '03', '21', 0, 0, 20130321123423, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(16, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study', 'test-case-study', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903639, 'n', '2013', '03', '22', 0, 0, 20130415230120, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(17, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 1', 'test-case-study-1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903727, 'n', '2013', '03', '22', 0, 0, 20130415230148, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(18, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 2', 'test-case-study-2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903691, 'n', '2013', '03', '22', 0, 0, 20130415230212, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(19, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 3', 'test-case-study-3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903655, 'n', '2013', '03', '22', 0, 0, 20130415230136, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(20, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 4', 'test-case-study-4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903737, 'n', '2013', '03', '22', 0, 0, 20130415230158, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(21, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 5', 'test-case-study-5', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903721, 'n', '2013', '03', '22', 0, 0, 20130415230142, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(22, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 6', 'test-case-study-6', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903698, 'n', '2013', '03', '22', 0, 0, 20130415230219, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(23, 1, 8, 1, 0, null, '127.0.0.1', 'Services', 'services', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365459670, 'n', '2013', '04', '09', 0, 0, 20130409112110, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(24, 1, 8, 1, 0, null, '127.0.0.1', 'Products', 'products', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365460036, 'n', '2013', '04', '09', 0, 0, 20130409112716, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(25, 1, 8, 1, 0, null, '127.0.0.1', 'About Us', 'about-us', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365460123, 'n', '2013', '04', '09', 0, 0, 20130414232744, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(26, 1, 7, 1, 0, null, '127.0.0.1', 'Survey & Design', 'survey-design', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365483859, 'n', '2013', '04', '09', 0, 0, 20130410025520, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(27, 1, 10, 1, 0, null, '127.0.0.1', 'About Us', 'about-us', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365633138, 'n', '2013', '04', '11', 0, 0, 20130415002319, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(28, 1, 9, 1, 0, null, '127.0.0.1', 'Management', 'management', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365635717, 'n', '2013', '04', '11', 0, 0, 20130415004818, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(29, 1, 11, 1, 0, null, '127.0.0.1', 'Topcon', 'topcon', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365731435, 'n', '2013', '04', '12', 0, 0, 20130412145035, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(30, 1, 3, 1, 0, null, '127.0.0.1', 'Test NZ only article', 'test-nz-only-article', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1366065476, 'n', '2013', '04', '16', 0, 0, 20130416113756, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(31, 1, 3, 1, 0, null, '127.0.0.1', 'Test AU only article', 'test-au-only-article', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1366065432, 'n', '2013', '04', '16', 0, 0, 20130416113712, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(32, 1, 5, 1, 0, null, '127.0.0.1', 'NZ Only Case Study', 'nz-only-case-study', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1366066443, 'n', '2013', '04', '16', 0, 0, 20130415230204, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(33, 1, 5, 1, 0, null, '127.0.0.1', 'AU Only Case Study', 'au-only-case-study', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1366066488, 'n', '2013', '04', '16', 0, 0, 20130415225849, 0, 0);

/* Table structure for table `exp_channels` */
DROP TABLE IF EXISTS `exp_channels`;

CREATE TABLE `exp_channels` (
  `channel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_name` varchar(40) NOT NULL,
  `channel_title` varchar(100) NOT NULL,
  `channel_url` varchar(100) NOT NULL,
  `channel_description` varchar(225) DEFAULT NULL,
  `channel_lang` varchar(12) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `cat_group` varchar(255) DEFAULT NULL,
  `status_group` int(4) unsigned DEFAULT NULL,
  `deft_status` varchar(50) NOT NULL DEFAULT 'open',
  `field_group` int(4) unsigned DEFAULT NULL,
  `search_excerpt` int(4) unsigned DEFAULT NULL,
  `deft_category` varchar(60) DEFAULT NULL,
  `deft_comments` char(1) NOT NULL DEFAULT 'y',
  `channel_require_membership` char(1) NOT NULL DEFAULT 'y',
  `channel_max_chars` int(5) unsigned DEFAULT NULL,
  `channel_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `channel_allow_img_urls` char(1) NOT NULL DEFAULT 'y',
  `channel_auto_link_urls` char(1) NOT NULL DEFAULT 'n',
  `channel_notify` char(1) NOT NULL DEFAULT 'n',
  `channel_notify_emails` varchar(255) DEFAULT NULL,
  `comment_url` varchar(80) DEFAULT NULL,
  `comment_system_enabled` char(1) NOT NULL DEFAULT 'y',
  `comment_require_membership` char(1) NOT NULL DEFAULT 'n',
  `comment_use_captcha` char(1) NOT NULL DEFAULT 'n',
  `comment_moderate` char(1) NOT NULL DEFAULT 'n',
  `comment_max_chars` int(5) unsigned DEFAULT '5000',
  `comment_timelock` int(5) unsigned NOT NULL DEFAULT '0',
  `comment_require_email` char(1) NOT NULL DEFAULT 'y',
  `comment_text_formatting` char(5) NOT NULL DEFAULT 'xhtml',
  `comment_html_formatting` char(4) NOT NULL DEFAULT 'safe',
  `comment_allow_img_urls` char(1) NOT NULL DEFAULT 'n',
  `comment_auto_link_urls` char(1) NOT NULL DEFAULT 'y',
  `comment_notify` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_authors` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_emails` varchar(255) DEFAULT NULL,
  `comment_expiration` int(4) unsigned NOT NULL DEFAULT '0',
  `search_results_url` varchar(80) DEFAULT NULL,
  `ping_return_url` varchar(80) DEFAULT NULL,
  `show_button_cluster` char(1) NOT NULL DEFAULT 'y',
  `rss_url` varchar(80) DEFAULT NULL,
  `enable_versioning` char(1) NOT NULL DEFAULT 'n',
  `max_revisions` smallint(4) unsigned NOT NULL DEFAULT '10',
  `default_entry_title` varchar(100) DEFAULT NULL,
  `url_title_prefix` varchar(80) DEFAULT NULL,
  `live_look_template` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`),
  KEY `cat_group` (`cat_group`),
  KEY `status_group` (`status_group`),
  KEY `field_group` (`field_group`),
  KEY `channel_name` (`channel_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_channels` */
INSERT INTO `exp_channels` VALUES(1, 1, 'products', 'Products', 'http://synergy.local/', null, 'en', 3, 0, 1358293544, 0, '1', 1, 'open', 1, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(2, 1, 'featured_product_pages', 'Featured Product Pages', 'http://synergy.local/', null, 'en', 4, 0, 1363822493, 0, '', 1, 'open', 2, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(3, 1, 'news', 'News', 'http://synergy.local/', null, 'en', 9, 0, 1366065476, 0, '', 1, 'open', 3, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(4, 1, 'contact_us', 'Contact Us', 'http://synergy.local/', null, 'en', 1, 0, 1362967301, 0, '', 1, 'open', 4, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(5, 1, 'case_studies', 'Case Studies', 'http://synergy.local/', null, 'en', 9, 0, 1366066488, 0, '', 1, 'open', 5, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(7, 1, 'services', 'Services', 'http://synergy.local/', null, 'en', 1, 0, 1365483859, 0, '2', 1, 'open', 6, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(8, 1, 'home_page_banners', 'Home Page Banners', 'http://synergy.local/', null, 'en', 3, 0, 1365460123, 0, '', 1, 'open', 7, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(9, 1, 'management', 'Management', 'http://synergy.local/', null, 'en', 1, 0, 1365635717, 0, '', 1, 'open', 8, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(10, 1, 'about_us', 'About Us', 'http://synergy.local/', null, 'en', 1, 0, 1365633138, 0, '', 1, 'open', 9, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(11, 1, 'manufacturers', 'Manufacturers', 'http://synergy.local/', null, 'en', 1, 0, 1365731435, 0, '', 1, 'open', 10, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);

/* Table structure for table `exp_comment_subscriptions` */
DROP TABLE IF EXISTS `exp_comment_subscriptions`;

CREATE TABLE `exp_comment_subscriptions` (
  `subscription_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `member_id` int(10) DEFAULT '0',
  `email` varchar(50) DEFAULT NULL,
  `subscription_date` varchar(10) DEFAULT NULL,
  `notification_sent` char(1) DEFAULT 'n',
  `hash` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`subscription_id`),
  KEY `entry_id` (`entry_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_comments` */
DROP TABLE IF EXISTS `exp_comments`;

CREATE TABLE `exp_comments` (
  `comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT '0',
  `channel_id` int(4) unsigned DEFAULT '1',
  `author_id` int(10) unsigned DEFAULT '0',
  `status` char(1) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `url` varchar(75) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `comment_date` int(10) DEFAULT NULL,
  `edit_date` int(10) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`comment_id`),
  KEY `entry_id` (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `status` (`status`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_cp_log` */
DROP TABLE IF EXISTS `exp_cp_log`;

CREATE TABLE `exp_cp_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `username` varchar(32) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `act_date` int(10) NOT NULL,
  `action` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_cp_log` */
INSERT INTO `exp_cp_log` VALUES(1, 1, 1, '96black', '127.0.0.1', 1355881673, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(2, 1, 1, '96black', '127.0.0.1', 1355883062, 'Site Updated&nbsp;&nbsp;Synergy Positioning Systems (NZ)');
INSERT INTO `exp_cp_log` VALUES(3, 1, 1, '96black', '127.0.0.1', 1355883097, 'Site Created&nbsp;&nbsp;Synergy Positioning Systems (AU)');
INSERT INTO `exp_cp_log` VALUES(4, 2, 1, '96black', '127.0.0.1', 1355943021, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(5, 2, 1, '96black', '127.0.0.1', 1355945651, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(6, 1, 1, '96black', '127.0.0.1', 1356034419, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(7, 2, 1, '96black', '127.0.0.1', 1356039515, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(8, 2, 1, '96black', '127.0.0.1', 1356039525, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(9, 1, 1, '96black', '127.0.0.1', 1358202174, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(10, 1, 1, '96black', '127.0.0.1', 1358202207, 'Channel Created:&nbsp;&nbsp;Products');
INSERT INTO `exp_cp_log` VALUES(11, 1, 1, '96black', '127.0.0.1', 1358202239, 'Field Group Created:&nbsp;Products');
INSERT INTO `exp_cp_log` VALUES(12, 1, 1, '96black', '127.0.0.1', 1358210877, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(13, 1, 1, '96black', '127.0.0.1', 1358218127, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(14, 1, 1, '96black', '127.0.0.1', 1358218180, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(15, 1, 1, '96black', '127.0.0.1', 1358218184, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(16, 1, 1, '96black', '127.0.0.1', 1358218208, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(17, 1, 1, '96black', '127.0.0.1', 1358220028, 'Channel Created:&nbsp;&nbsp;Featured Product Pages');
INSERT INTO `exp_cp_log` VALUES(18, 1, 1, '96black', '127.0.0.1', 1358220074, 'Field Group Created:&nbsp;Featured Product Pages');
INSERT INTO `exp_cp_log` VALUES(19, 1, 1, '96black', '127.0.0.1', 1358220742, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(20, 1, 1, '96black', '127.0.0.1', 1358274914, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(21, 1, 1, '96black', '127.0.0.1', 1358279266, 'Category Group Created:&nbsp;&nbsp;Product Categories');
INSERT INTO `exp_cp_log` VALUES(22, 1, 1, '96black', '127.0.0.1', 1358293233, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(23, 1, 1, '96black', '127.0.0.1', 1358362941, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(24, 1, 1, '96black', '127.0.0.1', 1359494506, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(25, 1, 1, '96black', '127.0.0.1', 1362948766, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(26, 1, 1, '96black', '127.0.0.1', 1362960488, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(27, 1, 1, '96black', '127.0.0.1', 1362960564, 'Channel Created:&nbsp;&nbsp;News');
INSERT INTO `exp_cp_log` VALUES(28, 1, 1, '96black', '127.0.0.1', 1362960570, 'Field Group Created:&nbsp;News');
INSERT INTO `exp_cp_log` VALUES(29, 1, 1, '96black', '127.0.0.1', 1362966730, 'Field Group Created:&nbsp;Contact Us');
INSERT INTO `exp_cp_log` VALUES(30, 1, 1, '96black', '127.0.0.1', 1362966734, 'Channel Created:&nbsp;&nbsp;Contact Us');
INSERT INTO `exp_cp_log` VALUES(31, 1, 1, '96black', '127.0.0.1', 1362974544, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(32, 1, 1, '96black', '127.0.0.1', 1362976053, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(33, 1, 1, '96black', '127.0.0.1', 1362976069, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(34, 1, 1, '96black', '127.0.0.1', 1362976093, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(35, 1, 1, '96black', '127.0.0.1', 1362992592, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(36, 1, 1, '96black', '127.0.0.1', 1363143606, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(37, 1, 1, '96black', '127.0.0.1', 1363209017, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(38, 1, 1, '96black', '127.0.0.1', 1363320699, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(39, 1, 1, '96black', '127.0.0.1', 1363567001, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(40, 1, 1, '96black', '127.0.0.1', 1363577373, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(41, 1, 1, '96black', '127.0.0.1', 1363578978, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(42, 1, 1, '96black', '127.0.0.1', 1363579481, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(43, 1, 1, '96black', '127.0.0.1', 1363660090, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(44, 1, 1, '96black', '127.0.0.1', 1363729623, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(45, 1, 1, '96black', '127.0.0.1', 1363736810, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(46, 1, 1, '96black', '127.0.0.1', 1363739939, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(47, 1, 1, '96black', '127.0.0.1', 1363739963, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(48, 1, 1, '96black', '127.0.0.1', 1363747179, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(49, 1, 1, '96black', '127.0.0.1', 1363747203, 'Category Group Created:&nbsp;&nbsp;Service Categories');
INSERT INTO `exp_cp_log` VALUES(50, 1, 1, '96black', '127.0.0.1', 1363820443, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(51, 1, 1, '96black', '127.0.0.1', 1363829155, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(52, 1, 1, '96black', '127.0.0.1', 1363833477, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(53, 1, 1, '96black', '127.0.0.1', 1363840598, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(54, 1, 1, '96black', '127.0.0.1', 1363902966, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(55, 1, 1, '96black', '127.0.0.1', 1363903295, 'Channel Created:&nbsp;&nbsp;Case Studies');
INSERT INTO `exp_cp_log` VALUES(56, 1, 1, '96black', '127.0.0.1', 1363903335, 'Field Group Created:&nbsp;Case Studies');
INSERT INTO `exp_cp_log` VALUES(57, 1, 1, '96black', '127.0.0.1', 1364159867, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(58, 1, 1, '96black', '127.0.0.1', 1364160031, 'Channel Created:&nbsp;&nbsp;Servoces');
INSERT INTO `exp_cp_log` VALUES(59, 1, 1, '96black', '127.0.0.1', 1364160036, 'Field Group Created:&nbsp;Services');
INSERT INTO `exp_cp_log` VALUES(60, 1, 1, '96black', '127.0.0.1', 1364160044, 'Channel Deleted:&nbsp;&nbsp;Servoces');
INSERT INTO `exp_cp_log` VALUES(61, 1, 1, '96black', '127.0.0.1', 1364160059, 'Channel Created:&nbsp;&nbsp;Services');
INSERT INTO `exp_cp_log` VALUES(62, 1, 1, '96black', '127.0.0.1', 1364160673, 'Channel Created:&nbsp;&nbsp;Home Page Banners');
INSERT INTO `exp_cp_log` VALUES(63, 1, 1, '96black', '127.0.0.1', 1364160678, 'Field Group Created:&nbsp;Home Page Banners');
INSERT INTO `exp_cp_log` VALUES(64, 1, 1, '96black', '127.0.0.1', 1365458478, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(65, 1, 1, '96black', '127.0.0.1', 1365459691, 'Custom Field Deleted:&nbsp;Home Main Heading');
INSERT INTO `exp_cp_log` VALUES(66, 1, 1, '96black', '127.0.0.1', 1365466907, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(67, 1, 1, '96black', '127.0.0.1', 1365467178, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(68, 1, 1, '96black', '127.0.0.1', 1365467422, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(69, 1, 1, '96black', '127.0.0.1', 1365467428, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(70, 1, 1, '96black', '127.0.0.1', 1365467460, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(71, 1, 1, '96black', '127.0.0.1', 1365467784, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(72, 1, 1, '96black', '127.0.0.1', 1365467819, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(73, 1, 1, '96black', '127.0.0.1', 1365467842, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(74, 1, 1, '96black', '127.0.0.1', 1365471782, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(75, 1, 1, '96black', '127.0.0.1', 1365472101, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(76, 1, 1, '96black', '127.0.0.1', 1365479239, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(77, 1, 1, '96black', '127.0.0.1', 1365479505, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(78, 1, 1, '96black', '127.0.0.1', 1365479533, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(79, 1, 1, '96black', '127.0.0.1', 1365558230, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(80, 1, 1, '96black', '127.0.0.1', 1365630778, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(81, 1, 1, '96black', '127.0.0.1', 1365630802, 'Channel Created:&nbsp;&nbsp;Management');
INSERT INTO `exp_cp_log` VALUES(82, 1, 1, '96black', '127.0.0.1', 1365630809, 'Field Group Created:&nbsp;Management');
INSERT INTO `exp_cp_log` VALUES(83, 1, 1, '96black', '127.0.0.1', 1365632300, 'Channel Created:&nbsp;&nbsp;About Us');
INSERT INTO `exp_cp_log` VALUES(84, 1, 1, '96black', '127.0.0.1', 1365632892, 'Field Group Created:&nbsp;About Us');
INSERT INTO `exp_cp_log` VALUES(85, 1, 1, '96black', '127.0.0.1', 1365641498, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(86, 1, 1, '96black', '127.0.0.1', 1365644175, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(87, 1, 1, '96black', '127.0.0.1', 1365653399, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(88, 1, 1, '96black', '127.0.0.1', 1365708737, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(89, 1, 1, '96black', '127.0.0.1', 1365725809, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(90, 1, 1, '96black', '127.0.0.1', 1365728123, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(91, 1, 1, '96black', '127.0.0.1', 1365729769, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(92, 1, 1, '96black', '127.0.0.1', 1365729854, 'Channel Created:&nbsp;&nbsp;Manufacturers');
INSERT INTO `exp_cp_log` VALUES(93, 1, 1, '96black', '127.0.0.1', 1365729858, 'Field Group Created:&nbsp;Manufacturers');
INSERT INTO `exp_cp_log` VALUES(94, 1, 1, '96black', '127.0.0.1', 1365737079, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(95, 1, 1, '96black', '127.0.0.1', 1365741053, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(96, 1, 1, '96black', '127.0.0.1', 1365741080, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(97, 1, 1, '96black', '127.0.0.1', 1365845178, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(98, 2, 1, '96black', '127.0.0.1', 1365848014, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(99, 2, 1, '96black', '127.0.0.1', 1365848029, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(100, 1, 1, '96black', '127.0.0.1', 1365848058, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(101, 2, 1, '96black', '127.0.0.1', 1365929769, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(102, 1, 1, '96black', '127.0.0.1', 1365964296, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(103, 1, 1, '96black', '127.0.0.1', 1365970813, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(104, 1, 1, '96black', '127.0.0.1', 1365973435, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(105, 1, 1, '96black', '127.0.0.1', 1365981775, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(106, 1, 1, '96black', '127.0.0.1', 1365985288, 'Custom Field Deleted:&nbsp;Page Content AU');
INSERT INTO `exp_cp_log` VALUES(107, 1, 1, '96black', '127.0.0.1', 1366065334, 'Logged in');

/* Table structure for table `exp_cp_search_index` */
DROP TABLE IF EXISTS `exp_cp_search_index`;

CREATE TABLE `exp_cp_search_index` (
  `search_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(20) DEFAULT NULL,
  `method` varchar(50) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `access` varchar(50) DEFAULT NULL,
  `keywords` text,
  PRIMARY KEY (`search_id`),
  FULLTEXT KEY `keywords` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


/* Table structure for table `exp_developer_log` */
DROP TABLE IF EXISTS `exp_developer_log`;

CREATE TABLE `exp_developer_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(10) unsigned NOT NULL,
  `viewed` char(1) NOT NULL DEFAULT 'n',
  `description` text,
  `function` varchar(100) DEFAULT NULL,
  `line` int(10) unsigned DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `deprecated_since` varchar(10) DEFAULT NULL,
  `use_instead` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_cache` */
DROP TABLE IF EXISTS `exp_email_cache`;

CREATE TABLE `exp_email_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `total_sent` int(6) unsigned NOT NULL,
  `from_name` varchar(70) NOT NULL,
  `from_email` varchar(70) NOT NULL,
  `recipient` text NOT NULL,
  `cc` text NOT NULL,
  `bcc` text NOT NULL,
  `recipient_array` mediumtext NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  `plaintext_alt` mediumtext NOT NULL,
  `mailinglist` char(1) NOT NULL DEFAULT 'n',
  `mailtype` varchar(6) NOT NULL,
  `text_fmt` varchar(40) NOT NULL,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `priority` char(1) NOT NULL DEFAULT '3',
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_cache_mg` */
DROP TABLE IF EXISTS `exp_email_cache_mg`;

CREATE TABLE `exp_email_cache_mg` (
  `cache_id` int(6) unsigned NOT NULL,
  `group_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_cache_ml` */
DROP TABLE IF EXISTS `exp_email_cache_ml`;

CREATE TABLE `exp_email_cache_ml` (
  `cache_id` int(6) unsigned NOT NULL,
  `list_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_console_cache` */
DROP TABLE IF EXISTS `exp_email_console_cache`;

CREATE TABLE `exp_email_console_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `member_id` int(10) unsigned NOT NULL,
  `member_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `recipient` varchar(70) NOT NULL,
  `recipient_name` varchar(50) NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_tracker` */
DROP TABLE IF EXISTS `exp_email_tracker`;

CREATE TABLE `exp_email_tracker` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_ip` varchar(16) NOT NULL,
  `sender_email` varchar(75) NOT NULL,
  `sender_username` varchar(50) NOT NULL,
  `number_recipients` int(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_entry_ping_status` */
DROP TABLE IF EXISTS `exp_entry_ping_status`;

CREATE TABLE `exp_entry_ping_status` (
  `entry_id` int(10) unsigned NOT NULL,
  `ping_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`ping_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_entry_versioning` */
DROP TABLE IF EXISTS `exp_entry_versioning`;

CREATE TABLE `exp_entry_versioning` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL,
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `version_date` int(10) NOT NULL,
  `version_data` mediumtext NOT NULL,
  PRIMARY KEY (`version_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_extensions` */
DROP TABLE IF EXISTS `exp_extensions`;

CREATE TABLE `exp_extensions` (
  `extension_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL DEFAULT '',
  `method` varchar(50) NOT NULL DEFAULT '',
  `hook` varchar(50) NOT NULL DEFAULT '',
  `settings` text NOT NULL,
  `priority` int(2) NOT NULL DEFAULT '10',
  `version` varchar(10) NOT NULL DEFAULT '',
  `enabled` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`extension_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_extensions` */
INSERT INTO `exp_extensions` VALUES(1, 'Safecracker_ext', 'form_declaration_modify_data', 'form_declaration_modify_data', '', 10, '2.1', 'y');
INSERT INTO `exp_extensions` VALUES(2, 'Rte_ext', 'myaccount_nav_setup', 'myaccount_nav_setup', '', 10, '1.0', 'y');
INSERT INTO `exp_extensions` VALUES(3, 'Rte_ext', 'cp_menu_array', 'cp_menu_array', '', 10, '1.0', 'y');
INSERT INTO `exp_extensions` VALUES(4, 'Rte_ext', 'publish_form_entry_data', 'publish_form_entry_data', '', 10, '1.0', 'y');
INSERT INTO `exp_extensions` VALUES(5, 'Matrix_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 10, '2.5.2', 'y');
INSERT INTO `exp_extensions` VALUES(6, 'Playa_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 9, '4.3.3', 'y');
INSERT INTO `exp_extensions` VALUES(7, 'Low_seg2cat_ext', 'sessions_end', 'sessions_end', 'a:3:{s:15:\"category_groups\";a:0:{}s:11:\"uri_pattern\";s:0:\"\";s:16:\"set_all_segments\";s:1:\"n\";}', 1, '2.6.3', 'y');
INSERT INTO `exp_extensions` VALUES(8, 'Mx_cloner_ext', 'publish_form_entry_data', 'publish_form_entry_data', 'a:1:{s:13:\"multilanguage\";s:1:\"n\";}', 10, '1.0.3', 'y');

/* Table structure for table `exp_field_formatting` */
DROP TABLE IF EXISTS `exp_field_formatting`;

CREATE TABLE `exp_field_formatting` (
  `formatting_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(10) unsigned NOT NULL,
  `field_fmt` varchar(40) NOT NULL,
  PRIMARY KEY (`formatting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_field_formatting` */
INSERT INTO `exp_field_formatting` VALUES(1, 1, 'none');
INSERT INTO `exp_field_formatting` VALUES(2, 1, 'br');
INSERT INTO `exp_field_formatting` VALUES(3, 1, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(4, 2, 'none');
INSERT INTO `exp_field_formatting` VALUES(5, 2, 'br');
INSERT INTO `exp_field_formatting` VALUES(6, 2, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(7, 3, 'none');
INSERT INTO `exp_field_formatting` VALUES(8, 3, 'br');
INSERT INTO `exp_field_formatting` VALUES(9, 3, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(10, 4, 'none');
INSERT INTO `exp_field_formatting` VALUES(11, 4, 'br');
INSERT INTO `exp_field_formatting` VALUES(12, 4, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(13, 5, 'none');
INSERT INTO `exp_field_formatting` VALUES(14, 5, 'br');
INSERT INTO `exp_field_formatting` VALUES(15, 5, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(16, 6, 'none');
INSERT INTO `exp_field_formatting` VALUES(17, 6, 'br');
INSERT INTO `exp_field_formatting` VALUES(18, 6, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(19, 7, 'none');
INSERT INTO `exp_field_formatting` VALUES(20, 7, 'br');
INSERT INTO `exp_field_formatting` VALUES(21, 7, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(22, 8, 'none');
INSERT INTO `exp_field_formatting` VALUES(23, 8, 'br');
INSERT INTO `exp_field_formatting` VALUES(24, 8, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(25, 9, 'none');
INSERT INTO `exp_field_formatting` VALUES(26, 9, 'br');
INSERT INTO `exp_field_formatting` VALUES(27, 9, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(28, 10, 'none');
INSERT INTO `exp_field_formatting` VALUES(29, 10, 'br');
INSERT INTO `exp_field_formatting` VALUES(30, 10, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(31, 11, 'none');
INSERT INTO `exp_field_formatting` VALUES(32, 11, 'br');
INSERT INTO `exp_field_formatting` VALUES(33, 11, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(34, 12, 'none');
INSERT INTO `exp_field_formatting` VALUES(35, 12, 'br');
INSERT INTO `exp_field_formatting` VALUES(36, 12, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(37, 13, 'none');
INSERT INTO `exp_field_formatting` VALUES(38, 13, 'br');
INSERT INTO `exp_field_formatting` VALUES(39, 13, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(40, 14, 'none');
INSERT INTO `exp_field_formatting` VALUES(41, 14, 'br');
INSERT INTO `exp_field_formatting` VALUES(42, 14, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(43, 15, 'none');
INSERT INTO `exp_field_formatting` VALUES(44, 15, 'br');
INSERT INTO `exp_field_formatting` VALUES(45, 15, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(46, 16, 'none');
INSERT INTO `exp_field_formatting` VALUES(47, 16, 'br');
INSERT INTO `exp_field_formatting` VALUES(48, 16, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(49, 17, 'none');
INSERT INTO `exp_field_formatting` VALUES(50, 17, 'br');
INSERT INTO `exp_field_formatting` VALUES(51, 17, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(52, 18, 'none');
INSERT INTO `exp_field_formatting` VALUES(53, 18, 'br');
INSERT INTO `exp_field_formatting` VALUES(54, 18, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(55, 19, 'none');
INSERT INTO `exp_field_formatting` VALUES(56, 19, 'br');
INSERT INTO `exp_field_formatting` VALUES(57, 19, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(58, 20, 'none');
INSERT INTO `exp_field_formatting` VALUES(59, 20, 'br');
INSERT INTO `exp_field_formatting` VALUES(60, 20, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(64, 22, 'none');
INSERT INTO `exp_field_formatting` VALUES(65, 22, 'br');
INSERT INTO `exp_field_formatting` VALUES(66, 22, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(67, 23, 'none');
INSERT INTO `exp_field_formatting` VALUES(68, 23, 'br');
INSERT INTO `exp_field_formatting` VALUES(69, 23, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(70, 24, 'none');
INSERT INTO `exp_field_formatting` VALUES(71, 24, 'br');
INSERT INTO `exp_field_formatting` VALUES(72, 24, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(73, 25, 'none');
INSERT INTO `exp_field_formatting` VALUES(74, 25, 'br');
INSERT INTO `exp_field_formatting` VALUES(75, 25, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(76, 26, 'none');
INSERT INTO `exp_field_formatting` VALUES(77, 26, 'br');
INSERT INTO `exp_field_formatting` VALUES(78, 26, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(79, 27, 'none');
INSERT INTO `exp_field_formatting` VALUES(80, 27, 'br');
INSERT INTO `exp_field_formatting` VALUES(81, 27, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(82, 28, 'none');
INSERT INTO `exp_field_formatting` VALUES(83, 28, 'br');
INSERT INTO `exp_field_formatting` VALUES(84, 28, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(85, 29, 'none');
INSERT INTO `exp_field_formatting` VALUES(86, 29, 'br');
INSERT INTO `exp_field_formatting` VALUES(87, 29, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(88, 30, 'none');
INSERT INTO `exp_field_formatting` VALUES(89, 30, 'br');
INSERT INTO `exp_field_formatting` VALUES(90, 30, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(91, 31, 'none');
INSERT INTO `exp_field_formatting` VALUES(92, 31, 'br');
INSERT INTO `exp_field_formatting` VALUES(93, 31, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(94, 32, 'none');
INSERT INTO `exp_field_formatting` VALUES(95, 32, 'br');
INSERT INTO `exp_field_formatting` VALUES(96, 32, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(97, 33, 'none');
INSERT INTO `exp_field_formatting` VALUES(98, 33, 'br');
INSERT INTO `exp_field_formatting` VALUES(99, 33, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(100, 34, 'none');
INSERT INTO `exp_field_formatting` VALUES(101, 34, 'br');
INSERT INTO `exp_field_formatting` VALUES(102, 34, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(103, 35, 'none');
INSERT INTO `exp_field_formatting` VALUES(104, 35, 'br');
INSERT INTO `exp_field_formatting` VALUES(105, 35, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(106, 36, 'none');
INSERT INTO `exp_field_formatting` VALUES(107, 36, 'br');
INSERT INTO `exp_field_formatting` VALUES(108, 36, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(109, 37, 'none');
INSERT INTO `exp_field_formatting` VALUES(110, 37, 'br');
INSERT INTO `exp_field_formatting` VALUES(111, 37, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(112, 38, 'none');
INSERT INTO `exp_field_formatting` VALUES(113, 38, 'br');
INSERT INTO `exp_field_formatting` VALUES(114, 38, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(115, 39, 'none');
INSERT INTO `exp_field_formatting` VALUES(116, 39, 'br');
INSERT INTO `exp_field_formatting` VALUES(117, 39, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(118, 40, 'none');
INSERT INTO `exp_field_formatting` VALUES(119, 40, 'br');
INSERT INTO `exp_field_formatting` VALUES(120, 40, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(124, 42, 'none');
INSERT INTO `exp_field_formatting` VALUES(125, 42, 'br');
INSERT INTO `exp_field_formatting` VALUES(126, 42, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(127, 43, 'none');
INSERT INTO `exp_field_formatting` VALUES(128, 43, 'br');
INSERT INTO `exp_field_formatting` VALUES(129, 43, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(130, 44, 'none');
INSERT INTO `exp_field_formatting` VALUES(131, 44, 'br');
INSERT INTO `exp_field_formatting` VALUES(132, 44, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(133, 45, 'none');
INSERT INTO `exp_field_formatting` VALUES(134, 45, 'br');
INSERT INTO `exp_field_formatting` VALUES(135, 45, 'xhtml');

/* Table structure for table `exp_field_groups` */
DROP TABLE IF EXISTS `exp_field_groups`;

CREATE TABLE `exp_field_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_field_groups` */
INSERT INTO `exp_field_groups` VALUES(1, 1, 'Products');
INSERT INTO `exp_field_groups` VALUES(2, 1, 'Featured Product Pages');
INSERT INTO `exp_field_groups` VALUES(3, 1, 'News');
INSERT INTO `exp_field_groups` VALUES(4, 1, 'Contact Us');
INSERT INTO `exp_field_groups` VALUES(5, 1, 'Case Studies');
INSERT INTO `exp_field_groups` VALUES(6, 1, 'Services');
INSERT INTO `exp_field_groups` VALUES(7, 1, 'Home Page Banners');
INSERT INTO `exp_field_groups` VALUES(8, 1, 'Management');
INSERT INTO `exp_field_groups` VALUES(9, 1, 'About Us');
INSERT INTO `exp_field_groups` VALUES(10, 1, 'Manufacturers');

/* Table structure for table `exp_fieldtypes` */
DROP TABLE IF EXISTS `exp_fieldtypes`;

CREATE TABLE `exp_fieldtypes` (
  `fieldtype_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `version` varchar(12) NOT NULL,
  `settings` text,
  `has_global_settings` char(1) DEFAULT 'n',
  PRIMARY KEY (`fieldtype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_fieldtypes` */
INSERT INTO `exp_fieldtypes` VALUES(1, 'select', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(2, 'text', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(3, 'textarea', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(4, 'date', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(5, 'file', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(6, 'multi_select', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(7, 'checkboxes', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(8, 'radio', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(9, 'rel', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(10, 'rte', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(11, 'matrix', '2.5.2', 'YTowOnt9', 'y');
INSERT INTO `exp_fieldtypes` VALUES(12, 'playa', '4.3.3', 'YTowOnt9', 'y');
INSERT INTO `exp_fieldtypes` VALUES(13, 'wygwam', '2.6.3', 'YTowOnt9', 'y');
INSERT INTO `exp_fieldtypes` VALUES(14, 'pt_checkboxes', '1.0.3', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(15, 'pt_dropdown', '1.0.3', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(16, 'pt_multiselect', '1.0.3', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(17, 'pt_radio_buttons', '1.0.3', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(18, 'nolan', '1.0.3.1', 'YTowOnt9', 'n');

/* Table structure for table `exp_file_categories` */
DROP TABLE IF EXISTS `exp_file_categories`;

CREATE TABLE `exp_file_categories` (
  `file_id` int(10) unsigned DEFAULT NULL,
  `cat_id` int(10) unsigned DEFAULT NULL,
  `sort` int(10) unsigned DEFAULT '0',
  `is_cover` char(1) DEFAULT 'n',
  KEY `file_id` (`file_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_file_dimensions` */
DROP TABLE IF EXISTS `exp_file_dimensions`;

CREATE TABLE `exp_file_dimensions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `upload_location_id` int(4) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT '',
  `short_name` varchar(255) DEFAULT '',
  `resize_type` varchar(50) DEFAULT '',
  `width` int(10) DEFAULT '0',
  `height` int(10) DEFAULT '0',
  `watermark_id` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `upload_location_id` (`upload_location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_file_dimensions` */
INSERT INTO `exp_file_dimensions` VALUES(1, 1, 4, 'article-list-thumb', 'article-list-thumb', 'crop', 396, 88, 0);
INSERT INTO `exp_file_dimensions` VALUES(2, 1, 5, 'case-study-list-thumb', 'case-study-list-thumb', 'crop', 396, 88, 0);

/* Table structure for table `exp_file_watermarks` */
DROP TABLE IF EXISTS `exp_file_watermarks`;

CREATE TABLE `exp_file_watermarks` (
  `wm_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `wm_name` varchar(80) DEFAULT NULL,
  `wm_type` varchar(10) DEFAULT 'text',
  `wm_image_path` varchar(100) DEFAULT NULL,
  `wm_test_image_path` varchar(100) DEFAULT NULL,
  `wm_use_font` char(1) DEFAULT 'y',
  `wm_font` varchar(30) DEFAULT NULL,
  `wm_font_size` int(3) unsigned DEFAULT NULL,
  `wm_text` varchar(100) DEFAULT NULL,
  `wm_vrt_alignment` varchar(10) DEFAULT 'top',
  `wm_hor_alignment` varchar(10) DEFAULT 'left',
  `wm_padding` int(3) unsigned DEFAULT NULL,
  `wm_opacity` int(3) unsigned DEFAULT NULL,
  `wm_hor_offset` int(4) unsigned DEFAULT NULL,
  `wm_vrt_offset` int(4) unsigned DEFAULT NULL,
  `wm_x_transp` int(4) DEFAULT NULL,
  `wm_y_transp` int(4) DEFAULT NULL,
  `wm_font_color` varchar(7) DEFAULT NULL,
  `wm_use_drop_shadow` char(1) DEFAULT 'y',
  `wm_shadow_distance` int(3) unsigned DEFAULT NULL,
  `wm_shadow_color` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`wm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_files` */
DROP TABLE IF EXISTS `exp_files`;

CREATE TABLE `exp_files` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `title` varchar(255) DEFAULT NULL,
  `upload_location_id` int(4) unsigned DEFAULT '0',
  `rel_path` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_size` int(10) DEFAULT '0',
  `description` text,
  `credit` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `uploaded_by_member_id` int(10) unsigned DEFAULT '0',
  `upload_date` int(10) DEFAULT NULL,
  `modified_by_member_id` int(10) unsigned DEFAULT '0',
  `modified_date` int(10) DEFAULT NULL,
  `file_hw_original` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`file_id`),
  KEY `upload_location_id` (`upload_location_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_files` */
INSERT INTO `exp_files` VALUES(1, 1, 'large-product-image.jpg', 1, '/Users/james/Projects/96black/Synergy/uploads/product-images/large-product-image.jpg', 'image/jpeg', 'large-product-image.jpg', 82647, null, null, null, 1, 1358293635, 1, 1358293635, '485 352');
INSERT INTO `exp_files` VALUES(3, 1, 'large-product-image2.jpg', 2, '/Users/james/Projects/96black/Synergy/uploads/product-images/large-product-image2.jpg', 'image/jpeg', 'large-product-image2.jpg', 82647, null, null, null, 1, 1358294245, 1, 1358294248, '485 352');
INSERT INTO `exp_files` VALUES(4, 1, 'large-product-image3.jpg', 2, '/Users/james/Projects/96black/Synergy/uploads/product-images/large-product-image3.jpg', 'image/jpeg', 'large-product-image3.jpg', 82647, null, null, null, 1, 1358294270, 1, 1358294270, '485 352');
INSERT INTO `exp_files` VALUES(5, 1, 'large-product-image4.jpg', 2, '/Users/james/Projects/96black/Synergy/uploads/product-downloads/large-product-image4.jpg', 'image/jpeg', 'large-product-image4.jpg', 82647, null, null, null, 1, 1358294478, 1, 1358294478, '485 352');
INSERT INTO `exp_files` VALUES(6, 1, 'large-product-image6.jpg', 1, '/Users/james/Projects/96black/Synergy/uploads/product-images/large-product-image6.jpg', 'image/jpeg', 'large-product-image6.jpg', 82647, null, null, null, 1, 1358363557, 1, 1358363557, '485 352');
INSERT INTO `exp_files` VALUES(7, 1, 'cat-image.jpg', 3, '/Users/jamesmcfall/Projects/Synergy/uploads/category-images/cat-image.jpg', 'image/jpeg', 'cat-image.jpg', 8376, null, null, null, 1, 1362950904, 1, 1362950904, '211 153');
INSERT INTO `exp_files` VALUES(8, 1, 'news-temp-top.jpg', 4, '/Users/jamesmcfall/Projects/Synergy/uploads/news-images/news-temp-top.jpg', 'image/jpeg', 'news-temp-top.jpg', 56244, null, null, null, 1, 1362961635, 1, 1362961635, '373 866');
INSERT INTO `exp_files` VALUES(9, 1, 'cat-feature-slide1.jpg', 1, '/Users/jamesmcfall/Projects/Synergy/uploads/product-images/cat-feature-slide1.jpg', 'image/jpeg', 'cat-feature-slide1.jpg', 19917, null, null, null, 1, 1363820695, 1, 1363820695, '411 866');
INSERT INTO `exp_files` VALUES(10, 1, 'cat-feature-slide3.jpg', 1, '/Users/jamesmcfall/Projects/Synergy/uploads/product-images/cat-feature-slide3.jpg', 'image/jpeg', 'cat-feature-slide3.jpg', 20183, null, null, null, 1, 1363820736, 1, 1363820736, '411 866');
INSERT INTO `exp_files` VALUES(11, 1, 'news-temp-top_(1).jpg', 5, '/Users/jamesmcfall/Projects/Synergy/uploads/case-study-image/news-temp-top_(1).jpg', 'image/jpeg', 'news-temp-top_(1).jpg', 56300, null, null, null, 1, 1363903862, 1, 1363903862, '373 866');
INSERT INTO `exp_files` VALUES(12, 1, 'news-temp-top_(2).jpg', 5, '/Users/jamesmcfall/Projects/Synergy/uploads/case-study-image/news-temp-top_(2).jpg', 'image/jpeg', 'news-temp-top_(2).jpg', 56300, null, null, null, 1, 1363904837, 1, 1363904841, '373 866');
INSERT INTO `exp_files` VALUES(13, 1, 'news-temp-top_(3).jpg', 5, '/Users/jamesmcfall/Projects/Synergy/uploads/case-study-image/news-temp-top_(3).jpg', 'image/jpeg', 'news-temp-top_(3).jpg', 56300, null, null, null, 1, 1363904916, 1, 1363904920, '373 866');
INSERT INTO `exp_files` VALUES(14, 1, 'services.jpg', 6, '/Users/jamesmcfall/Projects/Synergy/uploads/home-page-banners/services.jpg', 'image/jpeg', 'services.jpg', 88125, null, null, null, 1, 1365459810, 1, 1365459810, '714 1600');
INSERT INTO `exp_files` VALUES(15, 1, 'products.jpg', 6, '/Users/jamesmcfall/Projects/Synergy/uploads/home-page-banners/products.jpg', 'image/jpeg', 'products.jpg', 85228, null, null, null, 1, 1365460093, 1, 1365460093, '714 1600');
INSERT INTO `exp_files` VALUES(16, 1, 'about.jpg', 6, '/Users/jamesmcfall/Projects/Synergy/uploads/home-page-banners/about.jpg', 'image/jpeg', 'about.jpg', 111165, null, null, null, 1, 1365460222, 1, 1365460222, '714 1600');
INSERT INTO `exp_files` VALUES(17, 1, 'services-temp-top.jpg', 3, '/Users/jamesmcfall/Projects/Synergy/uploads/category-images/services-temp-top.jpg', 'image/jpeg', 'services-temp-top.jpg', 94945, null, null, null, 1, 1365480417, 1, 1365480417, '305 866');
INSERT INTO `exp_files` VALUES(18, 1, 'services-temp-top.jpg', 7, '/Users/jamesmcfall/Projects/Synergy/uploads/services-banners/services-temp-top.jpg', 'image/jpeg', 'services-temp-top.jpg', 94945, null, null, null, 1, 1365562517, 1, 1365562517, '305 866');
INSERT INTO `exp_files` VALUES(19, 1, 'about-temp.jpg', 8, '/Users/jamesmcfall/Projects/Synergy/uploads/about-banners/about-temp.jpg', 'image/jpeg', 'about-temp.jpg', 55081, null, null, null, 1, 1365633903, 1, 1365633903, '305 867');
INSERT INTO `exp_files` VALUES(20, 1, 'james.jpg', 9, '/Users/jamesmcfall/Projects/Synergy/uploads/staff-pictures/james.jpg', 'image/jpeg', 'james.jpg', 8591, null, null, null, 1, 1365636639, 1, 1365636639, '211 153');
INSERT INTO `exp_files` VALUES(21, 1, 'mega-menu-feature-product-image_(1).jpg', 1, '/Users/jamesmcfall/Projects/Synergy/uploads/product-images/mega-menu-feature-product-image_(1).jpg', 'image/jpeg', 'mega-menu-feature-product-image_(1).jpg', 10854, null, null, null, 1, 1365720765, 1, 1365720765, '89 302');
INSERT INTO `exp_files` VALUES(22, 1, 'footer-topcon.gif', 10, '/Users/jamesmcfall/Projects/Synergy/uploads/manufacturer-images/footer-topcon.gif', 'image/gif', 'footer-topcon.gif', 3881, null, null, null, 1, 1365731455, 1, 1365731455, '28 173');
INSERT INTO `exp_files` VALUES(23, 1, 'topcon-logo.gif', 10, '/Users/jamesmcfall/Projects/Synergy/uploads/manufacturer-images/topcon-logo.gif', 'image/gif', 'topcon-logo.gif', 3820, null, null, null, 1, 1365731467, 1, 1365731467, '24 140');
INSERT INTO `exp_files` VALUES(24, 1, 'mega-menu-feature-product-logo.jpg', 10, '/Users/jamesmcfall/Projects/Synergy/uploads/manufacturer-images/mega-menu-feature-product-logo.jpg', 'image/jpeg', 'mega-menu-feature-product-logo.jpg', 2918, null, null, null, 1, 1365731492, 1, 1365731492, '48 166');

/* Table structure for table `exp_freeform_composer_layouts` */
DROP TABLE IF EXISTS `exp_freeform_composer_layouts`;

CREATE TABLE `exp_freeform_composer_layouts` (
  `composer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `composer_data` text,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `preview` char(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`composer_id`),
  KEY `preview` (`preview`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_freeform_composer_templates` */
DROP TABLE IF EXISTS `exp_freeform_composer_templates`;

CREATE TABLE `exp_freeform_composer_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `template_name` varchar(150) NOT NULL DEFAULT 'default',
  `template_label` varchar(150) NOT NULL DEFAULT 'default',
  `template_description` text,
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_data` text,
  `param_data` text,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_freeform_fields` */
DROP TABLE IF EXISTS `exp_freeform_fields`;

CREATE TABLE `exp_freeform_fields` (
  `field_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `field_name` varchar(150) NOT NULL DEFAULT 'default',
  `field_label` varchar(150) NOT NULL DEFAULT 'default',
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `settings` text,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `required` char(1) NOT NULL DEFAULT 'n',
  `submissions_page` char(1) NOT NULL DEFAULT 'y',
  `moderation_page` char(1) NOT NULL DEFAULT 'y',
  `composer_use` char(1) NOT NULL DEFAULT 'y',
  `field_description` text,
  PRIMARY KEY (`field_id`),
  KEY `field_name` (`field_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_fields` */
INSERT INTO `exp_freeform_fields` VALUES(1, 1, 'first_name', 'First Name', 'text', '{\"field_length\":150,\"field_content_type\":\"any\"}', 1, 1365716048, 0, 'n', 'y', 'y', 'y', 'This field contains the user\'s first name.');
INSERT INTO `exp_freeform_fields` VALUES(2, 1, 'last_name', 'Last Name', 'text', '{\"field_length\":150,\"field_content_type\":\"any\"}', 1, 1365716048, 0, 'n', 'y', 'y', 'y', 'This field contains the user\'s last name.');
INSERT INTO `exp_freeform_fields` VALUES(3, 1, 'email', 'Email', 'text', '{\"field_length\":150,\"field_content_type\":\"email\"}', 1, 1365716048, 0, 'n', 'y', 'y', 'y', 'A basic email field for collecting stuff like an email address.');
INSERT INTO `exp_freeform_fields` VALUES(4, 1, 'user_message', 'Message', 'textarea', '{\"field_ta_rows\":6}', 1, 1365716048, 0, 'n', 'y', 'y', 'y', 'This field contains the user\'s message.');
INSERT INTO `exp_freeform_fields` VALUES(5, 1, 'company_name', 'Company Name', 'text', '{\"field_length\":\"255\",\"field_content_type\":\"any\",\"disallow_html_rendering\":\"y\"}', 1, 1365716329, 0, 'n', 'y', 'y', 'y', 'This field contains the company name.');
INSERT INTO `exp_freeform_fields` VALUES(6, 1, 'full_name', 'Full Name', 'text', '{\"field_length\":\"150\",\"field_content_type\":\"any\",\"disallow_html_rendering\":\"y\"}', 1, 1365716586, 0, 'n', 'y', 'y', 'y', 'This contains the full name.');
INSERT INTO `exp_freeform_fields` VALUES(7, 1, 'phone_number', 'Phone Number', 'text', '{\"field_length\":\"150\",\"field_content_type\":\"number\",\"disallow_html_rendering\":\"y\"}', 1, 1365716640, 0, 'n', 'y', 'y', 'y', '');

/* Table structure for table `exp_freeform_fieldtypes` */
DROP TABLE IF EXISTS `exp_freeform_fieldtypes`;

CREATE TABLE `exp_freeform_fieldtypes` (
  `fieldtype_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fieldtype_name` varchar(250) DEFAULT NULL,
  `settings` text,
  `default_field` char(1) NOT NULL DEFAULT 'n',
  `version` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`fieldtype_id`),
  KEY `fieldtype_name` (`fieldtype_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_fieldtypes` */
INSERT INTO `exp_freeform_fieldtypes` VALUES(1, 'file_upload', '[]', 'n', '4.0.11');
INSERT INTO `exp_freeform_fieldtypes` VALUES(2, 'mailinglist', '[]', 'n', '4.0.11');
INSERT INTO `exp_freeform_fieldtypes` VALUES(3, 'text', '[]', 'n', '4.0.11');
INSERT INTO `exp_freeform_fieldtypes` VALUES(4, 'textarea', '[]', 'n', '4.0.11');

/* Table structure for table `exp_freeform_file_uploads` */
DROP TABLE IF EXISTS `exp_freeform_file_uploads`;

CREATE TABLE `exp_freeform_file_uploads` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `field_id` int(10) unsigned NOT NULL DEFAULT '0',
  `server_path` varchar(750) DEFAULT NULL,
  `filename` varchar(250) DEFAULT NULL,
  `extension` varchar(20) DEFAULT NULL,
  `filesize` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`file_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `extension` (`extension`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_freeform_form_entries_1` */
DROP TABLE IF EXISTS `exp_freeform_form_entries_1`;

CREATE TABLE `exp_freeform_form_entries_1` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `complete` varchar(1) NOT NULL DEFAULT 'y',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(50) DEFAULT NULL,
  `form_field_3` text,
  `form_field_4` text,
  `form_field_6` text,
  `form_field_7` text,
  `form_field_5` text,
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_form_entries_1` */
INSERT INTO `exp_freeform_form_entries_1` VALUES(1, 1, 0, 'y', '127.0.0.1', 1365716048, 0, 'pending', 'support@solspace.com', 'Welcome to Freeform. We hope that you will enjoy Solspace software.', null, null, null);
INSERT INTO `exp_freeform_form_entries_1` VALUES(2, 1, 1, 'y', '127.0.0.1', 1365717771, 0, 'pending', '', '', '', '', '');
INSERT INTO `exp_freeform_form_entries_1` VALUES(3, 1, 1, 'y', '127.0.0.1', 1365718729, 0, 'pending', 'james@96black.co.nz', 'Test message submission.', 'James McFall', '093603493', '96black');

/* Table structure for table `exp_freeform_form_entries_2` */
DROP TABLE IF EXISTS `exp_freeform_form_entries_2`;

CREATE TABLE `exp_freeform_form_entries_2` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `complete` varchar(1) NOT NULL DEFAULT 'y',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(50) DEFAULT NULL,
  `form_field_5` text,
  `form_field_6` text,
  `form_field_7` text,
  `form_field_3` text,
  `form_field_4` text,
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_form_entries_2` */
INSERT INTO `exp_freeform_form_entries_2` VALUES(1, 1, 1, 'y', '127.0.0.1', 1365971016, 0, 'pending', '', '', '', '', 'fgsdfgdsf');
INSERT INTO `exp_freeform_form_entries_2` VALUES(2, 1, 1, 'y', '127.0.0.1', 1365971102, 0, 'pending', '', 'sdfsdfds', '', '', '');
INSERT INTO `exp_freeform_form_entries_2` VALUES(3, 1, 1, 'y', '127.0.0.1', 1365972061, 0, 'pending', 'Test', 'James', '12345', 'james@96black.co.nz', 'asfsaf');
INSERT INTO `exp_freeform_form_entries_2` VALUES(4, 1, 0, 'y', '127.0.0.1', 1365973613, 0, 'pending', 'asfadsf', 'asfdsa', '13213', 'sdf@sfsdfd.com', 'safasdfasdf');

/* Table structure for table `exp_freeform_forms` */
DROP TABLE IF EXISTS `exp_freeform_forms`;

CREATE TABLE `exp_freeform_forms` (
  `form_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_name` varchar(150) NOT NULL DEFAULT 'default',
  `form_label` varchar(150) NOT NULL DEFAULT 'default',
  `default_status` varchar(150) NOT NULL DEFAULT 'default',
  `notify_user` char(1) NOT NULL DEFAULT 'n',
  `notify_admin` char(1) NOT NULL DEFAULT 'n',
  `user_email_field` varchar(150) NOT NULL DEFAULT '',
  `user_notification_id` int(10) unsigned NOT NULL DEFAULT '0',
  `admin_notification_id` int(10) unsigned NOT NULL DEFAULT '0',
  `admin_notification_email` text,
  `form_description` text,
  `field_ids` text,
  `field_order` text,
  `template_id` int(10) unsigned NOT NULL DEFAULT '0',
  `composer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `settings` text,
  PRIMARY KEY (`form_id`),
  KEY `form_name` (`form_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_forms` */
INSERT INTO `exp_freeform_forms` VALUES(1, 1, 'contact', 'Contact', 'pending', 'n', 'y', '', 0, 0, 'james@96black.co.nz', 'This is a basic contact form.', '3|4|5|6|7', '6|5|3|7|4', 0, 0, 1, 1365716048, 1365716871, null);
INSERT INTO `exp_freeform_forms` VALUES(2, 1, 'product_enquiry_form', 'Product Enquiry Form', 'pending', 'n', 'y', '', 0, 0, 'james@96black.co.nz', 'This form handles product enquiries.', '3|4|5|6|7', '6|5|7|3|4', 0, 0, 1, 1365716187, 1365716735, null);

/* Table structure for table `exp_freeform_multipage_hashes` */
DROP TABLE IF EXISTS `exp_freeform_multipage_hashes`;

CREATE TABLE `exp_freeform_multipage_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `hash` varchar(32) NOT NULL DEFAULT '',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit` char(1) NOT NULL DEFAULT 'n',
  `data` text,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`),
  KEY `ip_address` (`ip_address`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_freeform_notification_templates` */
DROP TABLE IF EXISTS `exp_freeform_notification_templates`;

CREATE TABLE `exp_freeform_notification_templates` (
  `notification_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `notification_name` varchar(150) NOT NULL DEFAULT 'default',
  `notification_label` varchar(150) NOT NULL DEFAULT 'default',
  `notification_description` text,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `allow_html` char(1) NOT NULL DEFAULT 'n',
  `from_name` varchar(150) NOT NULL DEFAULT '',
  `from_email` varchar(250) NOT NULL DEFAULT '',
  `reply_to_email` varchar(250) NOT NULL DEFAULT '',
  `email_subject` varchar(128) NOT NULL DEFAULT 'default',
  `include_attachments` char(1) NOT NULL DEFAULT 'n',
  `template_data` text,
  PRIMARY KEY (`notification_id`),
  KEY `notification_name` (`notification_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_freeform_params` */
DROP TABLE IF EXISTS `exp_freeform_params`;

CREATE TABLE `exp_freeform_params` (
  `params_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text,
  PRIMARY KEY (`params_id`)
) ENGINE=InnoDB AUTO_INCREMENT=186 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_params` */
INSERT INTO `exp_freeform_params` VALUES(183, 1366064668, '{\"form_id\":\"1\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"Contact-us\\/index\\/%%entry_id%%\",\"inline_error_return\":\"contact-us\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":true,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(184, 1366064678, '{\"form_id\":\"1\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"Contact-us\\/index\\/%%entry_id%%\",\"inline_error_return\":\"contact-us\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":true,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(185, 1366066119, '{\"form_id\":\"2\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/products\",\"inline_error_return\":\"products\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');

/* Table structure for table `exp_freeform_preferences` */
DROP TABLE IF EXISTS `exp_freeform_preferences`;

CREATE TABLE `exp_freeform_preferences` (
  `preference_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `preference_name` varchar(80) DEFAULT NULL,
  `preference_value` text,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`preference_id`),
  KEY `preference_name` (`preference_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_preferences` */
INSERT INTO `exp_freeform_preferences` VALUES(1, 'ffp', 'n', 0);
INSERT INTO `exp_freeform_preferences` VALUES(2, 'field_layout_prefs', '{\"entry_layout_prefs\":{\"member\":{\"1\":{\"visible\":[\"6\",\"5\",\"7\",\"3\",\"4\",\"author\",\"ip_address\",\"entry_date\",\"edit_date\",\"status\"],\"hidden\":[\"complete\",\"entry_id\",\"site_id\"]}}}}', 1);

/* Table structure for table `exp_freeform_user_email` */
DROP TABLE IF EXISTS `exp_freeform_user_email`;

CREATE TABLE `exp_freeform_user_email` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `email_count` int(10) unsigned NOT NULL DEFAULT '0',
  `email_addresses` text,
  PRIMARY KEY (`email_id`),
  KEY `ip_address` (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_global_variables` */
DROP TABLE IF EXISTS `exp_global_variables`;

CREATE TABLE `exp_global_variables` (
  `variable_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `variable_name` varchar(50) NOT NULL,
  `variable_data` text NOT NULL,
  PRIMARY KEY (`variable_id`),
  KEY `variable_name` (`variable_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_html_buttons` */
DROP TABLE IF EXISTS `exp_html_buttons`;

CREATE TABLE `exp_html_buttons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `tag_name` varchar(32) NOT NULL,
  `tag_open` varchar(120) NOT NULL,
  `tag_close` varchar(120) NOT NULL,
  `accesskey` varchar(32) NOT NULL,
  `tag_order` int(3) unsigned NOT NULL,
  `tag_row` char(1) NOT NULL DEFAULT '1',
  `classname` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_html_buttons` */
INSERT INTO `exp_html_buttons` VALUES(1, 1, 0, 'b', '<strong>', '</strong>', 'b', 1, '1', 'btn_b');
INSERT INTO `exp_html_buttons` VALUES(2, 1, 0, 'i', '<em>', '</em>', 'i', 2, '1', 'btn_i');
INSERT INTO `exp_html_buttons` VALUES(3, 1, 0, 'blockquote', '<blockquote>', '</blockquote>', 'q', 3, '1', 'btn_blockquote');
INSERT INTO `exp_html_buttons` VALUES(4, 1, 0, 'a', '<a href=\"[![Link:!:http://]!]\"(!( title=\"[![Title]!]\")!)>', '</a>', 'a', 4, '1', 'btn_a');
INSERT INTO `exp_html_buttons` VALUES(5, 1, 0, 'img', '<img src=\"[![Link:!:http://]!]\" alt=\"[![Alternative text]!]\" />', '', '', 5, '1', 'btn_img');
INSERT INTO `exp_html_buttons` VALUES(6, 2, 0, 'b', '<strong>', '</strong>', 'b', 1, '1', 'btn_b');
INSERT INTO `exp_html_buttons` VALUES(7, 2, 0, 'i', '<em>', '</em>', 'i', 2, '1', 'btn_i');
INSERT INTO `exp_html_buttons` VALUES(8, 2, 0, 'blockquote', '<blockquote>', '</blockquote>', 'q', 3, '1', 'btn_blockquote');
INSERT INTO `exp_html_buttons` VALUES(9, 2, 0, 'a', '<a href=\"[![Link:!:http://]!]\"(!( title=\"[![Title]!]\")!)>', '</a>', 'a', 4, '1', 'btn_a');
INSERT INTO `exp_html_buttons` VALUES(10, 2, 0, 'img', '<img src=\"[![Link:!:http://]!]\" alt=\"[![Alternative text]!]\" />', '', '', 5, '1', 'btn_img');

/* Table structure for table `exp_layout_publish` */
DROP TABLE IF EXISTS `exp_layout_publish`;

CREATE TABLE `exp_layout_publish` (
  `layout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_group` int(4) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(4) unsigned NOT NULL DEFAULT '0',
  `field_layout` text,
  PRIMARY KEY (`layout_id`),
  KEY `site_id` (`site_id`),
  KEY `member_group` (`member_group`),
  KEY `channel_id` (`channel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_layout_publish` */
INSERT INTO `exp_layout_publish` VALUES(10, 1, 1, 2, 'a:4:{s:7:\"publish\";a:6:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:16;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:18;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:17;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:10:\"Categories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}');
INSERT INTO `exp_layout_publish` VALUES(12, 1, 1, 1, 'a:9:{s:7:\"publish\";a:7:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:37;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:1;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:2;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:10;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:15:\"technical_specs\";a:2:{s:10:\"_tab_label\";s:15:\"Technical Specs\";i:3;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"downloads\";a:2:{s:10:\"_tab_label\";s:9:\"Downloads\";i:11;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:6:\"videos\";a:2:{s:10:\"_tab_label\";s:6:\"Videos\";i:4;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:14:\"rental_details\";a:4:{s:10:\"_tab_label\";s:14:\"Rental Details\";i:5;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:6;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:7;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:16:\"featured_details\";a:4:{s:10:\"_tab_label\";s:16:\"Featured Details\";i:9;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:33;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:8;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:10:\"Categories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}');
INSERT INTO `exp_layout_publish` VALUES(16, 1, 1, 4, 'a:6:{s:7:\"publish\";a:4:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:38;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:19:\"new_zealand_content\";a:3:{s:10:\"_tab_label\";s:19:\"New Zealand Content\";i:14;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:15;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:18:\"australian_content\";a:3:{s:10:\"_tab_label\";s:18:\"Australian Content\";i:40;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:39;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:10:\"Categories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}');
INSERT INTO `exp_layout_publish` VALUES(20, 1, 1, 10, 'a:4:{s:7:\"publish\";a:7:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:31;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:19:\"new_zealand_content\";a:3:{s:10:\"_tab_label\";s:19:\"New Zealand Content\";i:29;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:30;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:18:\"australian_content\";a:3:{s:10:\"_tab_label\";s:18:\"Australian Content\";i:43;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:42;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:7:{s:10:\"_tab_label\";s:7:\"Options\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}');

/* Table structure for table `exp_matrix_cols` */
DROP TABLE IF EXISTS `exp_matrix_cols`;

CREATE TABLE `exp_matrix_cols` (
  `col_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `col_name` varchar(32) DEFAULT NULL,
  `col_label` varchar(50) DEFAULT NULL,
  `col_instructions` text,
  `col_type` varchar(50) DEFAULT 'text',
  `col_required` char(1) DEFAULT 'n',
  `col_search` char(1) DEFAULT 'n',
  `col_order` int(3) unsigned DEFAULT NULL,
  `col_width` varchar(4) DEFAULT NULL,
  `col_settings` text,
  PRIMARY KEY (`col_id`),
  KEY `site_id` (`site_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_matrix_cols` */
INSERT INTO `exp_matrix_cols` VALUES(1, 1, 1, null, 'currency', 'Currency', '', 'pt_dropdown', 'n', 'y', 0, '33%', 'YToxOntzOjc6Im9wdGlvbnMiO2E6Mjp7czozOiJOWkQiO3M6MzoiTlpEIjtzOjM6IkFVRCI7czozOiJBVUQiO319');
INSERT INTO `exp_matrix_cols` VALUES(2, 1, 1, null, 'amount', 'Amount', 'Please enter the dollar value (without &#36; or any decimals as they will be output automatically)', 'number', 'n', 'n', 1, '', 'YTozOntzOjk6Im1pbl92YWx1ZSI7czowOiIiO3M6OToibWF4X3ZhbHVlIjtzOjA6IiI7czo4OiJkZWNpbWFscyI7czoxOiIwIjt9');
INSERT INTO `exp_matrix_cols` VALUES(3, 1, 3, null, 'section_title', 'Section Title', 'i.e. Dimensions', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(4, 1, 3, null, 'specification_details', 'Specification Details', 'This is where you enter i.e. Length, 200mm', 'nolan', 'n', 'n', 1, '', 'YToyOntzOjE2OiJub2xhbl9jb2xfbGFiZWxzIjtzOjQwOiJTcGVjaWZpY2F0aW9uIE5hbWUgfCBTcGVjaWZpY2F0aW9uIFZhbHVlIjtzOjE1OiJub2xhbl9jb2xfbmFtZXMiO3M6NDA6InNwZWNpZmljYXRpb25fbmFtZSB8IHNwZWNpZmljYXRpb25fdmFsdWUiO30=');
INSERT INTO `exp_matrix_cols` VALUES(6, 1, 4, null, 'video_heading', 'Video Heading', 'The heading to appear above the video', 'text', 'n', 'n', 0, '25%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(7, 1, 4, null, 'video_embed_url', 'Video Embed URL', 'The embed url from Youtube', 'text', 'n', 'n', 1, '25%', 'YTo0OntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(8, 1, 4, null, 'video_description', 'Video Description', 'This description appears beneath the video', 'wygwam', 'n', 'n', 2, '50%', 'YTozOntzOjc6ImNvbnZlcnQiO3M6MDoiIjtzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(9, 1, 5, null, 'rental_heading', 'Rental Heading', '', 'text', 'n', 'n', 0, '25%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(10, 1, 5, null, 'rental_content', 'Rental Content', '', 'wygwam', 'n', 'n', 1, '75%', 'YTozOntzOjc6ImNvbnZlcnQiO3M6MDoiIjtzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(11, 1, 6, null, 'rental_duration_nz', 'Rental Duration (NZ)', 'i.e. 1-3 days', 'text', 'n', 'n', 0, '20%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(12, 1, 6, null, 'rental_pricing_nz', 'Rental Pricing (NZ)', 'Please exclude the &#36; sign and decimal values.', 'number', 'n', 'n', 1, '', 'YTozOntzOjk6Im1pbl92YWx1ZSI7czowOiIiO3M6OToibWF4X3ZhbHVlIjtzOjA6IiI7czo4OiJkZWNpbWFscyI7czoxOiIwIjt9');
INSERT INTO `exp_matrix_cols` VALUES(13, 1, 7, null, 'rental_duration_au', 'Rental Duration (AU)', 'i.e. 1-3 days', 'text', 'n', 'n', 0, '20%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(14, 1, 7, null, 'rental_pricing_au', 'Rental Pricing (AU)', 'Please exclude the &#36; sign and decimal values.', 'number', 'n', 'n', 1, '', 'YTozOntzOjk6Im1pbl92YWx1ZSI7czowOiIiO3M6OToibWF4X3ZhbHVlIjtzOjA6IiI7czo4OiJkZWNpbWFscyI7czoxOiIwIjt9');
INSERT INTO `exp_matrix_cols` VALUES(15, 1, 8, null, 'featured_banner_image', 'Featured Banner Image', '', 'file', 'n', 'n', 0, '33%', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiIxIjtzOjEyOiJjb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO30=');
INSERT INTO `exp_matrix_cols` VALUES(16, 1, 10, null, 'product_image', 'Product Image', '', 'file', 'n', 'n', 0, '33%', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiIxIjtzOjEyOiJjb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO30=');
INSERT INTO `exp_matrix_cols` VALUES(17, 1, 11, null, 'file_title', 'File Title', '', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(18, 1, 11, null, 'download_file', 'Files', '', 'file', 'n', 'n', 1, '', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiIyIjtzOjEyOiJjb250ZW50X3R5cGUiO3M6MzoiYWxsIjt9');
INSERT INTO `exp_matrix_cols` VALUES(19, 1, 15, null, 'branch_region', 'Branch Region', 'i.e. Auckland/Christchurch', 'text', 'y', 'n', 0, '20%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(20, 1, 15, null, 'contact_information', 'Contact Information', '', 'nolan', 'y', 'n', 1, '', 'YToyOntzOjE2OiJub2xhbl9jb2xfbGFiZWxzIjtzOjI0OiJDb250YWN0IE1ldGhvZCB8IERldGFpbHMiO3M6MTU6Im5vbGFuX2NvbF9uYW1lcyI7czozMjoiY29udGFjdF9tZXRob2QgfCBjb250YWN0X2RldGFpbHMiO30=');
INSERT INTO `exp_matrix_cols` VALUES(21, 1, 15, null, 'operating_hours', 'Operating Hours', 'i.e. 8:00am to 5:00pm', 'text', 'y', 'n', 2, '', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(22, 1, 15, null, 'physical_address', 'Physical Address', '', 'text', 'y', 'n', 3, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czoyOiJiciI7czozOiJkaXIiO3M6MzoibHRyIjt9');
INSERT INTO `exp_matrix_cols` VALUES(23, 1, 15, null, 'postal_address', 'Postal Address', '', 'text', 'y', 'n', 4, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czoyOiJiciI7czozOiJkaXIiO3M6MzoibHRyIjt9');
INSERT INTO `exp_matrix_cols` VALUES(24, 1, 8, null, 'featured_banner_description', 'Featured Banner Description', '', 'text', 'n', 'n', 1, '', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(25, 1, 17, null, 'content', 'Content', '', 'wygwam', 'y', 'n', 0, '', 'YToyOntzOjY6ImNvbmZpZyI7czoxOiI0IjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(26, 1, 22, null, 'banner_large_text', 'Banner Large Text', '', 'text', 'y', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(27, 1, 22, null, 'banner_small_text', 'Banner Small Text', '', 'text', 'n', 'n', 1, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(28, 1, 23, null, 'link_text', 'Link Text', '', 'text', 'y', 'n', 0, '50%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(29, 1, 23, null, 'link_location', 'Link Location', 'The page you want the link to point to. It must be prefixed with \"/\". For example \"/services\"', 'text', 'y', 'n', 1, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(30, 1, 26, null, 'content', 'Content', '', 'wygwam', 'n', 'n', 0, '', 'YTozOntzOjc6ImNvbnZlcnQiO3M6MDoiIjtzOjY6ImNvbmZpZyI7czoxOiI0IjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(31, 1, 30, null, 'grid_content', 'Grid Content', '', 'wygwam', 'n', 'n', 1, '', 'YToyOntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(32, 1, 30, null, 'grid_title', 'Grid Title', '', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(33, 1, 32, null, 'name', 'Name', '', 'text', 'y', 'n', 0, '', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(34, 1, 32, null, 'position', 'Position', '', 'text', 'y', 'n', 1, '', 'YTozOntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(35, 1, 32, null, 'image', 'Image', 'Image dimensions: 153px x 211px. If not supplied a default image is used.', 'file', 'n', 'n', 2, '', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiI5IjtzOjEyOiJjb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO30=');
INSERT INTO `exp_matrix_cols` VALUES(36, 1, 38, null, 'contact_number', 'Contact Number', '', 'text', 'y', 'n', 1, '40%', 'YTozOntzOjQ6Im1heGwiO3M6MzoiMTAwIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(37, 1, 38, null, 'email_address', 'Email Address', '', 'text', 'y', 'n', 2, '40%', 'YTozOntzOjQ6Im1heGwiO3M6MzoiMTAwIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(38, 1, 38, null, 'country', 'Country', '\"AU\" or \"NZ\"', 'pt_dropdown', 'y', 'y', 0, '20%', 'YToxOntzOjc6Im9wdGlvbnMiO2E6Mjp7czoyOiJBVSI7czoyOiJBVSI7czoyOiJOWiI7czoyOiJOWiI7fX0=');
INSERT INTO `exp_matrix_cols` VALUES(39, 1, 39, null, 'branch_region', 'Branch Region', 'i.e. Auckland/Christchurch', 'text', 'y', 'n', 0, '20%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(40, 1, 39, null, 'contact_information', 'Contact Information', '', 'nolan', 'y', 'n', 1, '', 'YToyOntzOjE2OiJub2xhbl9jb2xfbGFiZWxzIjtzOjI0OiJDb250YWN0IE1ldGhvZCB8IERldGFpbHMiO3M6MTU6Im5vbGFuX2NvbF9uYW1lcyI7czozMjoiY29udGFjdF9tZXRob2QgfCBjb250YWN0X2RldGFpbHMiO30=');
INSERT INTO `exp_matrix_cols` VALUES(41, 1, 39, null, 'operating_hours', 'Operating Hours', 'i.e. 8:00am to 5:00pm', 'text', 'y', 'n', 2, '', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(42, 1, 39, null, 'physical_address', 'Physical Address', '', 'text', 'y', 'n', 3, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czoyOiJiciI7czozOiJkaXIiO3M6MzoibHRyIjt9');
INSERT INTO `exp_matrix_cols` VALUES(43, 1, 39, null, 'postal_address', 'Postal Address', '', 'text', 'y', 'n', 4, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czoyOiJiciI7czozOiJkaXIiO3M6MzoibHRyIjt9');
INSERT INTO `exp_matrix_cols` VALUES(44, 1, 42, null, 'grid_title', 'Grid Title', '', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(45, 1, 42, null, 'grid_content', 'Grid Content', '', 'wygwam', 'n', 'n', 1, '', 'YToyOntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(46, 1, 32, null, 'country', 'Staff Country', '', 'pt_dropdown', 'y', 'y', 3, '', 'YToxOntzOjc6Im9wdGlvbnMiO2E6Mjp7czoyOiJOWiI7czoyOiJOWiI7czoyOiJBVSI7czoyOiJBVSI7fX0=');

/* Table structure for table `exp_matrix_data` */
DROP TABLE IF EXISTS `exp_matrix_data`;

CREATE TABLE `exp_matrix_data` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `is_draft` tinyint(1) unsigned DEFAULT '0',
  `row_order` int(4) unsigned DEFAULT NULL,
  `col_id_1` text,
  `col_id_2` int(11) DEFAULT '0',
  `col_id_3` text,
  `col_id_4` text,
  `col_id_6` text,
  `col_id_7` text,
  `col_id_8` text,
  `col_id_9` text,
  `col_id_10` text,
  `col_id_11` text,
  `col_id_12` int(11) DEFAULT '0',
  `col_id_13` text,
  `col_id_14` int(11) DEFAULT '0',
  `col_id_15` text,
  `col_id_16` text,
  `col_id_17` text,
  `col_id_18` text,
  `col_id_19` text,
  `col_id_20` text,
  `col_id_21` text,
  `col_id_22` text,
  `col_id_23` text,
  `col_id_24` text,
  `col_id_25` text,
  `col_id_26` text,
  `col_id_27` text,
  `col_id_28` text,
  `col_id_29` text,
  `col_id_30` text,
  `col_id_31` text,
  `col_id_32` text,
  `col_id_33` text,
  `col_id_34` text,
  `col_id_35` text,
  `col_id_36` text,
  `col_id_37` text,
  `col_id_38` text,
  `col_id_39` text,
  `col_id_40` text,
  `col_id_41` text,
  `col_id_42` text,
  `col_id_43` text,
  `col_id_44` text,
  `col_id_45` text,
  `col_id_46` text,
  PRIMARY KEY (`row_id`),
  KEY `site_id` (`site_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_matrix_data` */
INSERT INTO `exp_matrix_data` VALUES(1, 1, 1, 1, null, 0, 1, 'NZD', 45000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(2, 1, 1, 1, null, 0, 2, 'AUD', 40000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(3, 1, 1, 3, null, 0, 1, null, 0, 'Telescope', 'a:7:{i:0;a:2:{s:18:\"specification_name\";s:6:\"Length\";s:19:\"specification_value\";s:15:\"150 Millimeters\";}i:1;a:2:{s:18:\"specification_name\";s:23:\"Objective Lens Diameter\";s:19:\"specification_value\";s:15:\"45mm (EDM:50mm)\";}i:2;a:2:{s:18:\"specification_name\";s:13:\"Magnification\";s:19:\"specification_value\";s:3:\"30x\";}i:3;a:2:{s:18:\"specification_name\";s:5:\"Image\";s:19:\"specification_value\";s:5:\"Erect\";}i:4;a:2:{s:18:\"specification_name\";s:13:\"Field of View\";s:19:\"specification_value\";s:3:\"130\";}i:5;a:2:{s:18:\"specification_name\";s:13:\"Resolve Power\";s:19:\"specification_value\";s:2:\"3\"\";}i:6;a:2:{s:18:\"specification_name\";s:18:\"Min Focus Distance\";s:19:\"specification_value\";s:10:\"1.3 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(4, 1, 1, 3, null, 0, 2, null, 0, 'Measurement Range', 'a:2:{i:0;a:2:{s:18:\"specification_name\";s:8:\"1 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}i:1;a:2:{s:18:\"specification_name\";s:8:\"3 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(5, 1, 1, 10, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(6, 1, 1, 11, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, 'Test uploaded file', '{filedir_2}large-product-image4.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(7, 1, 1, 4, null, 0, 1, null, 0, null, null, 'Test Video Of My 4x4', 'http://www.youtube.com/embed/HNIA-nesZEU', '<p>\n	Test vid of my 4x4.&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae gravida tortor. Maecenas turpis purus, imperdiet id elementum eget, feugiat in tellus. Duis molestie, nulla nec sollicitudin consectetur, magna nisl condimentum neque, at consequat magna enim a tellus. Maecenas porttitor mattis aliquet. Ut lobortis, tortor eget lacinia commodo, mi sapien molestie neque, quis mattis metus lorem posuere massa. Vivamus id pretium lacus. Aliquam ac mauris orci, vulputate ullamcorper elit. Donec eget eros eros. Etiam ac sapien a sem ullamcorper consequat. Suspendisse ut elementum arcu. Cras fringilla tortor pellentesque nibh pharetra pellentesque. Sed et mauris sit amet ligula scelerisque sodales sed nec lorem. In vel quam sit amet eros lobortis consectetur sit amet egestas nisl. Cras erat turpis, suscipit posuere mollis nec, dictum eget lectus.</p>', null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(8, 1, 1, 5, null, 0, 1, null, 0, null, null, null, null, null, 'Test heading', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(9, 1, 1, 5, null, 0, 2, null, 0, null, null, null, null, null, 'Test heading 2', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(10, 1, 1, 6, null, 0, 1, null, 0, null, null, null, null, null, null, null, '1-3 days', 100, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(11, 1, 1, 6, null, 0, 2, null, 0, null, null, null, null, null, null, null, '4-6 days', 180, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(12, 1, 1, 7, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, '1-3 days', 80, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(13, 1, 1, 7, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, '4-6 days', 160, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(14, 1, 1, 10, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image6.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(15, 1, 2, 1, null, 0, 1, 'NZD', 45000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(16, 1, 2, 1, null, 0, 2, 'AUD', 40000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(17, 1, 2, 3, null, 0, 1, null, 0, 'Telescope', 'a:7:{i:0;a:2:{s:18:\"specification_name\";s:6:\"Length\";s:19:\"specification_value\";s:15:\"150 Millimeters\";}i:1;a:2:{s:18:\"specification_name\";s:23:\"Objective Lens Diameter\";s:19:\"specification_value\";s:15:\"45mm (EDM:50mm)\";}i:2;a:2:{s:18:\"specification_name\";s:13:\"Magnification\";s:19:\"specification_value\";s:3:\"30x\";}i:3;a:2:{s:18:\"specification_name\";s:5:\"Image\";s:19:\"specification_value\";s:5:\"Erect\";}i:4;a:2:{s:18:\"specification_name\";s:13:\"Field of View\";s:19:\"specification_value\";s:3:\"130\";}i:5;a:2:{s:18:\"specification_name\";s:13:\"Resolve Power\";s:19:\"specification_value\";s:2:\"3\"\";}i:6;a:2:{s:18:\"specification_name\";s:18:\"Min Focus Distance\";s:19:\"specification_value\";s:10:\"1.3 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(18, 1, 2, 3, null, 0, 2, null, 0, 'Measurement Range', 'a:2:{i:0;a:2:{s:18:\"specification_name\";s:8:\"1 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}i:1;a:2:{s:18:\"specification_name\";s:8:\"3 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(19, 1, 2, 4, null, 0, 1, null, 0, null, null, 'Test Video Of My 4x4', 'http://www.youtube.com/embed/HNIA-nesZEU', '<p>\n	Test vid of my 4x4.&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae gravida tortor. Maecenas turpis purus, imperdiet id elementum eget, feugiat in tellus. Duis molestie, nulla nec sollicitudin consectetur, magna nisl condimentum neque, at consequat magna enim a tellus. Maecenas porttitor mattis aliquet. Ut lobortis, tortor eget lacinia commodo, mi sapien molestie neque, quis mattis metus lorem posuere massa. Vivamus id pretium lacus. Aliquam ac mauris orci, vulputate ullamcorper elit. Donec eget eros eros. Etiam ac sapien a sem ullamcorper consequat. Suspendisse ut elementum arcu. Cras fringilla tortor pellentesque nibh pharetra pellentesque. Sed et mauris sit amet ligula scelerisque sodales sed nec lorem. In vel quam sit amet eros lobortis consectetur sit amet egestas nisl. Cras erat turpis, suscipit posuere mollis nec, dictum eget lectus.</p>', null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(20, 1, 2, 5, null, 0, 1, null, 0, null, null, null, null, null, 'Test heading', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(21, 1, 2, 5, null, 0, 2, null, 0, null, null, null, null, null, 'Test heading 2', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(22, 1, 2, 6, null, 0, 1, null, 0, null, null, null, null, null, null, null, '1-3 days', 100, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(23, 1, 2, 6, null, 0, 2, null, 0, null, null, null, null, null, null, null, '4-6 days', 180, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(24, 1, 2, 7, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, '1-3 days', 80, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(25, 1, 2, 7, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, '4-6 days', 160, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(26, 1, 2, 10, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(27, 1, 2, 10, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image6.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(28, 1, 2, 11, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, 'Test uploaded file', '{filedir_2}large-product-image4.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(29, 1, 3, 1, null, 0, 1, 'NZD', 45000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(30, 1, 3, 1, null, 0, 2, 'AUD', 40000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(31, 1, 3, 3, null, 0, 1, null, 0, 'Telescope', 'a:7:{i:0;a:2:{s:18:\"specification_name\";s:6:\"Length\";s:19:\"specification_value\";s:15:\"150 Millimeters\";}i:1;a:2:{s:18:\"specification_name\";s:23:\"Objective Lens Diameter\";s:19:\"specification_value\";s:15:\"45mm (EDM:50mm)\";}i:2;a:2:{s:18:\"specification_name\";s:13:\"Magnification\";s:19:\"specification_value\";s:3:\"30x\";}i:3;a:2:{s:18:\"specification_name\";s:5:\"Image\";s:19:\"specification_value\";s:5:\"Erect\";}i:4;a:2:{s:18:\"specification_name\";s:13:\"Field of View\";s:19:\"specification_value\";s:3:\"130\";}i:5;a:2:{s:18:\"specification_name\";s:13:\"Resolve Power\";s:19:\"specification_value\";s:2:\"3\"\";}i:6;a:2:{s:18:\"specification_name\";s:18:\"Min Focus Distance\";s:19:\"specification_value\";s:10:\"1.3 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(32, 1, 3, 3, null, 0, 2, null, 0, 'Measurement Range', 'a:2:{i:0;a:2:{s:18:\"specification_name\";s:8:\"1 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}i:1;a:2:{s:18:\"specification_name\";s:8:\"3 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(33, 1, 3, 4, null, 0, 1, null, 0, null, null, 'Test Video Of My 4x4', 'http://www.youtube.com/embed/HNIA-nesZEU', '<p>\n	Test vid of my 4x4.&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae gravida tortor. Maecenas turpis purus, imperdiet id elementum eget, feugiat in tellus. Duis molestie, nulla nec sollicitudin consectetur, magna nisl condimentum neque, at consequat magna enim a tellus. Maecenas porttitor mattis aliquet. Ut lobortis, tortor eget lacinia commodo, mi sapien molestie neque, quis mattis metus lorem posuere massa. Vivamus id pretium lacus. Aliquam ac mauris orci, vulputate ullamcorper elit. Donec eget eros eros. Etiam ac sapien a sem ullamcorper consequat. Suspendisse ut elementum arcu. Cras fringilla tortor pellentesque nibh pharetra pellentesque. Sed et mauris sit amet ligula scelerisque sodales sed nec lorem. In vel quam sit amet eros lobortis consectetur sit amet egestas nisl. Cras erat turpis, suscipit posuere mollis nec, dictum eget lectus.</p>', null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(34, 1, 3, 5, null, 0, 1, null, 0, null, null, null, null, null, 'Test heading', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(35, 1, 3, 5, null, 0, 2, null, 0, null, null, null, null, null, 'Test heading 2', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(36, 1, 3, 6, null, 0, 1, null, 0, null, null, null, null, null, null, null, '1-3 days', 100, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(37, 1, 3, 6, null, 0, 2, null, 0, null, null, null, null, null, null, null, '4-6 days', 180, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(38, 1, 3, 7, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, '1-3 days', 80, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(39, 1, 3, 7, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, '4-6 days', 160, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(40, 1, 3, 10, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(41, 1, 3, 10, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image6.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(42, 1, 3, 11, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, 'Test uploaded file', '{filedir_2}large-product-image4.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(43, 1, 11, 15, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, 'Auckland', 'a:4:{i:0;a:2:{s:14:\"contact_method\";s:9:\"Free Call\";s:15:\"contact_details\";s:12:\"0800-867-266\";}i:1;a:2:{s:14:\"contact_method\";s:5:\"Phone\";s:15:\"contact_details\";s:14:\"+64-9-476-5151\";}i:2;a:2:{s:14:\"contact_method\";s:3:\"Fax\";s:15:\"contact_details\";s:14:\"+63-9-476-5140\";}i:3;a:2:{s:14:\"contact_method\";s:5:\"Email\";s:15:\"contact_details\";s:30:\" info@synergypositioning.co.nz\";}}', '8:00am to 5:00pm', '3/52 Arrenway Drive \nAlbany \nAuckland \nNew Zealand', 'P O Box 100450 \nNSMC \nAuckland \nNew Zealand \n0745', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(44, 1, 11, 15, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, 'Christchurch', 'a:4:{i:0;a:2:{s:14:\"contact_method\";s:9:\"Free Call\";s:15:\"contact_details\";s:13:\" 0800-867-266\";}i:1;a:2:{s:14:\"contact_method\";s:5:\"Phone\";s:15:\"contact_details\";s:14:\"+64-9-476-5151\";}i:2;a:2:{s:14:\"contact_method\";s:3:\"Fax\";s:15:\"contact_details\";s:14:\"+63-9-476-5140\";}i:3;a:2:{s:14:\"contact_method\";s:5:\"Email\";s:15:\"contact_details\";s:30:\" info@synergypositioning.co.nz\";}}', '8:00am to 5:00pm', '3/52 Arrenway Drive \nAlbany \nAuckland \nNew Zealand', 'P O Box 100450 \nNSMC \nAuckland \nNew Zealand \n0745', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(45, 1, 3, 10, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image6.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(46, 1, 2, 8, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, '{filedir_1}cat-feature-slide1.jpg', null, null, null, null, null, null, null, null, 'Test description 1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(47, 1, 2, 8, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, '{filedir_1}cat-feature-slide3.jpg', null, null, null, null, null, null, null, null, 'Test description 2', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(48, 1, 12, 17, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	BRAMOR Orthophoto</h2>\n<p>\n	The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability, ergonomic and user friendly ground control station, durability and mobility. The airframe is manufactured with CAD/CAM technology from advanced composite materials (Kevlar/Carbon/Vectran) that provide a high level of survivability. A high visibility color scheme and strobe and NAV lights are optional. Embedded autonomous flight procedures include an array of failsafe options based on man-rated standards.</p>\n<p>\n	The system consists of the air vehicle with a 25 megapixel DSLR E/O sensor, a foldable portable take off ramp and rugged ground control station. The whole system fits into one rugged transport case. The system is flight ready in under five minutes. The system is safely operated by one operator/pilot in command.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(49, 1, 12, 17, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>\n<p>\n	Solutpat metus, nec auctor mi metus eu nibh. Integer id nibh natis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malsapien, et vulputate libero. Praesent interdum risus a libero consequat dapibus. Nulla facilisi. Aliquam erat nisi, placerat id laoreet quis, fringilla ac dolor. Nam interdum tristique augue.</p>\n<p>\n	Nullam accumsan suscipit magna, non vestibulum tortor eleifend eget. Fusce mollis venenatis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(50, 1, 12, 17, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel.</p>\n<p>\n	Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(51, 1, 13, 17, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	BRAMOR Orthophoto</h2>\n<p>\n	The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability, ergonomic and user friendly ground control station, durability and mobility. The airframe is manufactured with CAD/CAM technology from advanced composite materials (Kevlar/Carbon/Vectran) that provide a high level of survivability. A high visibility color scheme and strobe and NAV lights are optional. Embedded autonomous flight procedures include an array of failsafe options based on man-rated standards.</p>\n<p>\n	The system consists of the air vehicle with a 25 megapixel DSLR E/O sensor, a foldable portable take off ramp and rugged ground control station. The whole system fits into one rugged transport case. The system is flight ready in under five minutes. The system is safely operated by one operator/pilot in command.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(52, 1, 13, 17, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>\n<p>\n	Solutpat metus, nec auctor mi metus eu nibh. Integer id nibh natis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malsapien, et vulputate libero. Praesent interdum risus a libero consequat dapibus. Nulla facilisi. Aliquam erat nisi, placerat id laoreet quis, fringilla ac dolor. Nam interdum tristique augue.</p>\n<p>\n	Nullam accumsan suscipit magna, non vestibulum tortor eleifend eget. Fusce mollis venenatis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(53, 1, 13, 17, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel.</p>\n<p>\n	Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(54, 1, 14, 17, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	BRAMOR Orthophoto</h2>\n<p>\n	The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability, ergonomic and user friendly ground control station, durability and mobility. The airframe is manufactured with CAD/CAM technology from advanced composite materials (Kevlar/Carbon/Vectran) that provide a high level of survivability. A high visibility color scheme and strobe and NAV lights are optional. Embedded autonomous flight procedures include an array of failsafe options based on man-rated standards.</p>\n<p>\n	The system consists of the air vehicle with a 25 megapixel DSLR E/O sensor, a foldable portable take off ramp and rugged ground control station. The whole system fits into one rugged transport case. The system is flight ready in under five minutes. The system is safely operated by one operator/pilot in command.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(55, 1, 14, 17, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>\n<p>\n	Solutpat metus, nec auctor mi metus eu nibh. Integer id nibh natis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malsapien, et vulputate libero. Praesent interdum risus a libero consequat dapibus. Nulla facilisi. Aliquam erat nisi, placerat id laoreet quis, fringilla ac dolor. Nam interdum tristique augue.</p>\n<p>\n	Nullam accumsan suscipit magna, non vestibulum tortor eleifend eget. Fusce mollis venenatis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(56, 1, 14, 17, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel.</p>\n<p>\n	Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(57, 1, 15, 17, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	BRAMOR Orthophoto</h2>\n<p>\n	The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability, ergonomic and user friendly ground control station, durability and mobility. The airframe is manufactured with CAD/CAM technology from advanced composite materials (Kevlar/Carbon/Vectran) that provide a high level of survivability. A high visibility color scheme and strobe and NAV lights are optional. Embedded autonomous flight procedures include an array of failsafe options based on man-rated standards.</p>\n<p>\n	The system consists of the air vehicle with a 25 megapixel DSLR E/O sensor, a foldable portable take off ramp and rugged ground control station. The whole system fits into one rugged transport case. The system is flight ready in under five minutes. The system is safely operated by one operator/pilot in command.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(58, 1, 15, 17, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>\n<p>\n	Solutpat metus, nec auctor mi metus eu nibh. Integer id nibh natis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malsapien, et vulputate libero. Praesent interdum risus a libero consequat dapibus. Nulla facilisi. Aliquam erat nisi, placerat id laoreet quis, fringilla ac dolor. Nam interdum tristique augue.</p>\n<p>\n	Nullam accumsan suscipit magna, non vestibulum tortor eleifend eget. Fusce mollis venenatis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(59, 1, 15, 17, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel.</p>\n<p>\n	Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(60, 1, 23, 22, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, 'Services.', 'Marketing message here', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(61, 1, 23, 23, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, 'Read more about services', '/services', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(62, 1, 24, 22, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, 'Trusted precision and control.', 'Marketing message here', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(63, 1, 24, 23, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, 'Browse, enquire & hire from our product range', '/products', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(64, 1, 25, 22, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, 'Synergy Positioning Systems', 'Marketing message here.', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(65, 1, 25, 23, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, 'Read more about Synergy', '/about-us', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(66, 1, 26, 26, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(67, 1, 26, 26, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	<img alt=\"\" src=\"{filedir_5}news-temp-top_(1).jpg\" style=\"width: 200px; height: 86px; float: right;\" /></p>\n<p>\n	L</p>\n<p>\n	orem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odi</p>\n<p>\n	o, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(68, 1, 27, 30, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	You are dealing with an established company representing world renowned brands that have been tried and tested in increasing productivity and reducing costs.</p>', 'Simple really....', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(69, 1, 27, 30, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	With our equipment and systems, you gain efficiency &amp; reduce costs, saving you money on all aspects of the job including: fuel, wages, wear &amp; tear, materials &amp; processing.</p>', 'Your profit...\n', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(70, 1, 27, 30, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	We only represent equipment &amp; brands that we trust. Topcon, world leaders in positioning equipment for over 80 years &amp; well established throughout the world, offers reliable &amp; robust equipment for any application.</p>', 'Our Products\n', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(71, 1, 27, 30, null, 0, 4, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	Synergy Positioning Systems Ltd is 100% New Zealand owned and operated. Our industry exprience spans more than 30 years.</p>', 'Our Experience\n', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(72, 1, 27, 30, null, 0, 5, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	We are commited to our customers. Your success is our reward. Our factory trained technicians and in-house industry experts ensure that what we sell is fully supported.</p>', 'Our Service\n', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(73, 1, 28, 32, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'James McFall', 'The man', '{filedir_9}james.jpg', null, null, null, null, null, null, null, null, null, null, 'NZ');
INSERT INTO `exp_matrix_data` VALUES(74, 1, 28, 32, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'John Smith', 'Unknown', null, null, null, null, null, null, null, null, null, null, null, 'AU');
INSERT INTO `exp_matrix_data` VALUES(75, 1, 1, 8, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, '{filedir_1}cat-feature-slide3.jpg', null, null, null, null, null, null, null, null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(76, 1, 11, 38, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0800-867-266', 'info@96black.co.nz', 'NZ', null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(77, 1, 11, 38, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0900-123-456', 'info@96black.co.nz', 'AU', null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(78, 1, 11, 39, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'Sydney', 'a:4:{i:0;a:2:{s:14:\"contact_method\";s:9:\"Free Call\";s:15:\"contact_details\";s:13:\"0900-123-4567\";}i:1;a:2:{s:14:\"contact_method\";s:5:\"Phone\";s:15:\"contact_details\";s:6:\"123456\";}i:2;a:2:{s:14:\"contact_method\";s:3:\"Fax\";s:15:\"contact_details\";s:6:\"123456\";}i:3;a:2:{s:14:\"contact_method\";s:5:\"Email\";s:15:\"contact_details\";s:31:\" info@synergypositioning.com.au\";}}', '8:00am to 5:00pm', '123 Some Steet', 'Some PO Box', null, null, null);
INSERT INTO `exp_matrix_data` VALUES(79, 1, 27, 42, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'Test grid item', '<p>\n	This is a test grid item</p>', null);

/* Table structure for table `exp_member_bulletin_board` */
DROP TABLE IF EXISTS `exp_member_bulletin_board`;

CREATE TABLE `exp_member_bulletin_board` (
  `bulletin_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `bulletin_group` int(8) unsigned NOT NULL,
  `bulletin_date` int(10) unsigned NOT NULL,
  `hash` varchar(10) NOT NULL DEFAULT '',
  `bulletin_expires` int(10) unsigned NOT NULL DEFAULT '0',
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_id`),
  KEY `sender_id` (`sender_id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_member_data` */
DROP TABLE IF EXISTS `exp_member_data`;

CREATE TABLE `exp_member_data` (
  `member_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_member_data` */
INSERT INTO `exp_member_data` VALUES(1);

/* Table structure for table `exp_member_fields` */
DROP TABLE IF EXISTS `exp_member_fields`;

CREATE TABLE `exp_member_fields` (
  `m_field_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `m_field_name` varchar(32) NOT NULL,
  `m_field_label` varchar(50) NOT NULL,
  `m_field_description` text NOT NULL,
  `m_field_type` varchar(12) NOT NULL DEFAULT 'text',
  `m_field_list_items` text NOT NULL,
  `m_field_ta_rows` tinyint(2) DEFAULT '8',
  `m_field_maxl` smallint(3) NOT NULL,
  `m_field_width` varchar(6) NOT NULL,
  `m_field_search` char(1) NOT NULL DEFAULT 'y',
  `m_field_required` char(1) NOT NULL DEFAULT 'n',
  `m_field_public` char(1) NOT NULL DEFAULT 'y',
  `m_field_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_cp_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_fmt` char(5) NOT NULL DEFAULT 'none',
  `m_field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`m_field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_member_groups` */
DROP TABLE IF EXISTS `exp_member_groups`;

CREATE TABLE `exp_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_title` varchar(100) NOT NULL,
  `group_description` text NOT NULL,
  `is_locked` char(1) NOT NULL DEFAULT 'y',
  `can_view_offline_system` char(1) NOT NULL DEFAULT 'n',
  `can_view_online_system` char(1) NOT NULL DEFAULT 'y',
  `can_access_cp` char(1) NOT NULL DEFAULT 'y',
  `can_access_content` char(1) NOT NULL DEFAULT 'n',
  `can_access_publish` char(1) NOT NULL DEFAULT 'n',
  `can_access_edit` char(1) NOT NULL DEFAULT 'n',
  `can_access_files` char(1) NOT NULL DEFAULT 'n',
  `can_access_fieldtypes` char(1) NOT NULL DEFAULT 'n',
  `can_access_design` char(1) NOT NULL DEFAULT 'n',
  `can_access_addons` char(1) NOT NULL DEFAULT 'n',
  `can_access_modules` char(1) NOT NULL DEFAULT 'n',
  `can_access_extensions` char(1) NOT NULL DEFAULT 'n',
  `can_access_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_access_plugins` char(1) NOT NULL DEFAULT 'n',
  `can_access_members` char(1) NOT NULL DEFAULT 'n',
  `can_access_admin` char(1) NOT NULL DEFAULT 'n',
  `can_access_sys_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_content_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_tools` char(1) NOT NULL DEFAULT 'n',
  `can_access_comm` char(1) NOT NULL DEFAULT 'n',
  `can_access_utilities` char(1) NOT NULL DEFAULT 'n',
  `can_access_data` char(1) NOT NULL DEFAULT 'n',
  `can_access_logs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_channels` char(1) NOT NULL DEFAULT 'n',
  `can_admin_upload_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_design` char(1) NOT NULL DEFAULT 'n',
  `can_admin_members` char(1) NOT NULL DEFAULT 'n',
  `can_delete_members` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_groups` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_templates` char(1) NOT NULL DEFAULT 'n',
  `can_ban_users` char(1) NOT NULL DEFAULT 'n',
  `can_admin_modules` char(1) NOT NULL DEFAULT 'n',
  `can_admin_templates` char(1) NOT NULL DEFAULT 'n',
  `can_admin_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_edit_categories` char(1) NOT NULL DEFAULT 'n',
  `can_delete_categories` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_edit_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_assign_post_authors` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self_entries` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_entries` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_moderate_comments` char(1) NOT NULL DEFAULT 'n',
  `can_send_email` char(1) NOT NULL DEFAULT 'n',
  `can_send_cached_email` char(1) NOT NULL DEFAULT 'n',
  `can_email_member_groups` char(1) NOT NULL DEFAULT 'n',
  `can_email_mailinglist` char(1) NOT NULL DEFAULT 'n',
  `can_email_from_profile` char(1) NOT NULL DEFAULT 'n',
  `can_view_profiles` char(1) NOT NULL DEFAULT 'n',
  `can_edit_html_buttons` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self` char(1) NOT NULL DEFAULT 'n',
  `mbr_delete_notify_emails` varchar(255) DEFAULT NULL,
  `can_post_comments` char(1) NOT NULL DEFAULT 'n',
  `exclude_from_moderation` char(1) NOT NULL DEFAULT 'n',
  `can_search` char(1) NOT NULL DEFAULT 'n',
  `search_flood_control` mediumint(5) unsigned NOT NULL,
  `can_send_private_messages` char(1) NOT NULL DEFAULT 'n',
  `prv_msg_send_limit` smallint(5) unsigned NOT NULL DEFAULT '20',
  `prv_msg_storage_limit` smallint(5) unsigned NOT NULL DEFAULT '60',
  `can_attach_in_private_messages` char(1) NOT NULL DEFAULT 'n',
  `can_send_bulletins` char(1) NOT NULL DEFAULT 'n',
  `include_in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `include_in_memberlist` char(1) NOT NULL DEFAULT 'y',
  `include_in_mailinglists` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`group_id`,`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_member_groups` */
INSERT INTO `exp_member_groups` VALUES(1, 1, 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y');
INSERT INTO `exp_member_groups` VALUES(1, 2, 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y');
INSERT INTO `exp_member_groups` VALUES(2, 1, 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', 60, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(2, 2, 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', 60, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(3, 1, 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(3, 2, 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(4, 1, 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(4, 2, 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(5, 1, 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', 10, 'y', 20, 60, 'y', 'n', 'n', 'y', 'y');
INSERT INTO `exp_member_groups` VALUES(5, 2, 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', 10, 'y', 20, 60, 'y', 'n', 'n', 'y', 'y');

/* Table structure for table `exp_member_homepage` */
DROP TABLE IF EXISTS `exp_member_homepage`;

CREATE TABLE `exp_member_homepage` (
  `member_id` int(10) unsigned NOT NULL,
  `recent_entries` char(1) NOT NULL DEFAULT 'l',
  `recent_entries_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_comments` char(1) NOT NULL DEFAULT 'l',
  `recent_comments_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_members` char(1) NOT NULL DEFAULT 'n',
  `recent_members_order` int(3) unsigned NOT NULL DEFAULT '0',
  `site_statistics` char(1) NOT NULL DEFAULT 'r',
  `site_statistics_order` int(3) unsigned NOT NULL DEFAULT '0',
  `member_search_form` char(1) NOT NULL DEFAULT 'n',
  `member_search_form_order` int(3) unsigned NOT NULL DEFAULT '0',
  `notepad` char(1) NOT NULL DEFAULT 'r',
  `notepad_order` int(3) unsigned NOT NULL DEFAULT '0',
  `bulletin_board` char(1) NOT NULL DEFAULT 'r',
  `bulletin_board_order` int(3) unsigned NOT NULL DEFAULT '0',
  `pmachine_news_feed` char(1) NOT NULL DEFAULT 'n',
  `pmachine_news_feed_order` int(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_member_homepage` */
INSERT INTO `exp_member_homepage` VALUES(1, 'l', 1, 'l', 2, 'n', 0, 'r', 1, 'n', 0, 'r', 2, 'r', 0, 'l', 0);

/* Table structure for table `exp_member_search` */
DROP TABLE IF EXISTS `exp_member_search`;

CREATE TABLE `exp_member_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `search_date` int(10) unsigned NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `fields` varchar(200) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(8) unsigned NOT NULL,
  `query` text NOT NULL,
  PRIMARY KEY (`search_id`),
  KEY `member_id` (`member_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_members` */
DROP TABLE IF EXISTS `exp_members`;

CREATE TABLE `exp_members` (
  `member_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` smallint(4) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL DEFAULT '',
  `unique_id` varchar(40) NOT NULL,
  `crypt_key` varchar(40) DEFAULT NULL,
  `authcode` varchar(10) DEFAULT NULL,
  `email` varchar(72) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `occupation` varchar(80) DEFAULT NULL,
  `interests` varchar(120) DEFAULT NULL,
  `bday_d` int(2) DEFAULT NULL,
  `bday_m` int(2) DEFAULT NULL,
  `bday_y` int(4) DEFAULT NULL,
  `aol_im` varchar(50) DEFAULT NULL,
  `yahoo_im` varchar(50) DEFAULT NULL,
  `msn_im` varchar(50) DEFAULT NULL,
  `icq` varchar(50) DEFAULT NULL,
  `bio` text,
  `signature` text,
  `avatar_filename` varchar(120) DEFAULT NULL,
  `avatar_width` int(4) unsigned DEFAULT NULL,
  `avatar_height` int(4) unsigned DEFAULT NULL,
  `photo_filename` varchar(120) DEFAULT NULL,
  `photo_width` int(4) unsigned DEFAULT NULL,
  `photo_height` int(4) unsigned DEFAULT NULL,
  `sig_img_filename` varchar(120) DEFAULT NULL,
  `sig_img_width` int(4) unsigned DEFAULT NULL,
  `sig_img_height` int(4) unsigned DEFAULT NULL,
  `ignore_list` text,
  `private_messages` int(4) unsigned NOT NULL DEFAULT '0',
  `accept_messages` char(1) NOT NULL DEFAULT 'y',
  `last_view_bulletins` int(10) NOT NULL DEFAULT '0',
  `last_bulletin_date` int(10) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `join_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visit` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `total_entries` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_comments` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `accept_admin_email` char(1) NOT NULL DEFAULT 'y',
  `accept_user_email` char(1) NOT NULL DEFAULT 'y',
  `notify_by_default` char(1) NOT NULL DEFAULT 'y',
  `notify_of_pm` char(1) NOT NULL DEFAULT 'y',
  `display_avatars` char(1) NOT NULL DEFAULT 'y',
  `display_signatures` char(1) NOT NULL DEFAULT 'y',
  `parse_smileys` char(1) NOT NULL DEFAULT 'y',
  `smart_notifications` char(1) NOT NULL DEFAULT 'y',
  `language` varchar(50) NOT NULL,
  `timezone` varchar(8) NOT NULL,
  `daylight_savings` char(1) NOT NULL DEFAULT 'n',
  `localization_is_site_default` char(1) NOT NULL DEFAULT 'n',
  `time_format` char(2) NOT NULL DEFAULT 'us',
  `cp_theme` varchar(32) DEFAULT NULL,
  `profile_theme` varchar(32) DEFAULT NULL,
  `forum_theme` varchar(32) DEFAULT NULL,
  `tracker` text,
  `template_size` varchar(2) NOT NULL DEFAULT '20',
  `notepad` text,
  `notepad_size` varchar(2) NOT NULL DEFAULT '18',
  `quick_links` text,
  `quick_tabs` text,
  `show_sidebar` char(1) NOT NULL DEFAULT 'n',
  `pmember_id` int(10) NOT NULL DEFAULT '0',
  `rte_enabled` char(1) NOT NULL DEFAULT 'y',
  `rte_toolset_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`),
  KEY `group_id` (`group_id`),
  KEY `unique_id` (`unique_id`),
  KEY `password` (`password`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_members` */
INSERT INTO `exp_members` VALUES(1, 1, '96black', '96black', '106cfaa54b9caa63de2a620edb474b900a68d8e76e79847360bbef4323d51b29ea822b5dfe7f5352be760a6ae6a6de7b8abb78f37ad429db302a103d864acad8', '=WDztY<`dk>S{94N7[sY$1\'WCM{:qPT;Z{P44_rlj\'GU8,WH]l$%3d27{+oz,-=(&Ed*nE>q)!+z4p=a;?`yYL|]l:.`/U.=P3lU@\"n*jX_-=GO>!E+?()Cqj<ai>Q=a', '2d3c4dab12f0973e5396766a007ac2efd798eb16', 'b9cbee325c2edd1db5f8b3ef603a2ad0bb317a5f', null, 'james@96black.co.nz', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 0, 'y', 0, 0, '127.0.0.1', 1355879065, 1365987404, 1366071294, 33, 0, 0, 0, 1366066489, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UP12', 'y', 'n', 'us', null, null, null, null, '20', null, '18', '', 'Template Manager|C=design&M=manager|1', 'n', 0, 'y', 0);

/* Table structure for table `exp_message_attachments` */
DROP TABLE IF EXISTS `exp_message_attachments`;

CREATE TABLE `exp_message_attachments` (
  `attachment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_name` varchar(50) NOT NULL DEFAULT '',
  `attachment_hash` varchar(40) NOT NULL DEFAULT '',
  `attachment_extension` varchar(20) NOT NULL DEFAULT '',
  `attachment_location` varchar(150) NOT NULL DEFAULT '',
  `attachment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_size` int(10) unsigned NOT NULL DEFAULT '0',
  `is_temp` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`attachment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_message_copies` */
DROP TABLE IF EXISTS `exp_message_copies`;

CREATE TABLE `exp_message_copies` (
  `copy_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `recipient_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_received` char(1) NOT NULL DEFAULT 'n',
  `message_read` char(1) NOT NULL DEFAULT 'n',
  `message_time_read` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_downloaded` char(1) NOT NULL DEFAULT 'n',
  `message_folder` int(10) unsigned NOT NULL DEFAULT '1',
  `message_authcode` varchar(10) NOT NULL DEFAULT '',
  `message_deleted` char(1) NOT NULL DEFAULT 'n',
  `message_status` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`copy_id`),
  KEY `message_id` (`message_id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_message_data` */
DROP TABLE IF EXISTS `exp_message_data`;

CREATE TABLE `exp_message_data` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_date` int(10) unsigned NOT NULL DEFAULT '0',
  `message_subject` varchar(255) NOT NULL DEFAULT '',
  `message_body` text NOT NULL,
  `message_tracking` char(1) NOT NULL DEFAULT 'y',
  `message_attachments` char(1) NOT NULL DEFAULT 'n',
  `message_recipients` varchar(200) NOT NULL DEFAULT '',
  `message_cc` varchar(200) NOT NULL DEFAULT '',
  `message_hide_cc` char(1) NOT NULL DEFAULT 'n',
  `message_sent_copy` char(1) NOT NULL DEFAULT 'n',
  `total_recipients` int(5) unsigned NOT NULL DEFAULT '0',
  `message_status` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`message_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_message_folders` */
DROP TABLE IF EXISTS `exp_message_folders`;

CREATE TABLE `exp_message_folders` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `folder1_name` varchar(50) NOT NULL DEFAULT 'InBox',
  `folder2_name` varchar(50) NOT NULL DEFAULT 'Sent',
  `folder3_name` varchar(50) NOT NULL DEFAULT '',
  `folder4_name` varchar(50) NOT NULL DEFAULT '',
  `folder5_name` varchar(50) NOT NULL DEFAULT '',
  `folder6_name` varchar(50) NOT NULL DEFAULT '',
  `folder7_name` varchar(50) NOT NULL DEFAULT '',
  `folder8_name` varchar(50) NOT NULL DEFAULT '',
  `folder9_name` varchar(50) NOT NULL DEFAULT '',
  `folder10_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_message_folders` */
INSERT INTO `exp_message_folders` VALUES(1, 'InBox', 'Sent', '', '', '', '', '', '', '', '');

/* Table structure for table `exp_message_listed` */
DROP TABLE IF EXISTS `exp_message_listed`;

CREATE TABLE `exp_message_listed` (
  `listed_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_member` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_description` varchar(100) NOT NULL DEFAULT '',
  `listed_type` varchar(10) NOT NULL DEFAULT 'blocked',
  PRIMARY KEY (`listed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_module_member_groups` */
DROP TABLE IF EXISTS `exp_module_member_groups`;

CREATE TABLE `exp_module_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `module_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_modules` */
DROP TABLE IF EXISTS `exp_modules`;

CREATE TABLE `exp_modules` (
  `module_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(50) NOT NULL,
  `module_version` varchar(12) NOT NULL,
  `has_cp_backend` char(1) NOT NULL DEFAULT 'n',
  `has_publish_fields` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_modules` */
INSERT INTO `exp_modules` VALUES(1, 'Comment', '2.3', 'y', 'n');
INSERT INTO `exp_modules` VALUES(2, 'Email', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(3, 'Emoticon', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(4, 'File', '1.0.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(5, 'Jquery', '1.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(6, 'Rss', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(7, 'Safecracker', '2.1', 'y', 'n');
INSERT INTO `exp_modules` VALUES(8, 'Search', '2.2', 'n', 'n');
INSERT INTO `exp_modules` VALUES(9, 'Channel', '2.0.1', 'n', 'n');
INSERT INTO `exp_modules` VALUES(10, 'Member', '2.1', 'n', 'n');
INSERT INTO `exp_modules` VALUES(11, 'Stats', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(12, 'Rte', '1.0', 'y', 'n');
INSERT INTO `exp_modules` VALUES(13, 'Playa', '4.3.3', 'n', 'n');
INSERT INTO `exp_modules` VALUES(14, 'Wygwam', '2.6.3', 'y', 'n');
INSERT INTO `exp_modules` VALUES(15, 'Query', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(16, 'Freeform', '4.0.11', 'y', 'n');

/* Table structure for table `exp_online_users` */
DROP TABLE IF EXISTS `exp_online_users`;

CREATE TABLE `exp_online_users` (
  `online_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `in_forum` char(1) NOT NULL DEFAULT 'n',
  `name` varchar(50) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `anon` char(1) NOT NULL,
  PRIMARY KEY (`online_id`),
  KEY `date` (`date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=178 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_online_users` */
INSERT INTO `exp_online_users` VALUES(6, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(8, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(9, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(129, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(140, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(153, 1, 0, 'n', '', '127.0.0.1', 1366071435, '');
INSERT INTO `exp_online_users` VALUES(154, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(156, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(157, 1, 0, 'n', '', '127.0.0.1', 1366071435, '');
INSERT INTO `exp_online_users` VALUES(159, 1, 0, 'n', '', '127.0.0.1', 1366071435, '');
INSERT INTO `exp_online_users` VALUES(160, 1, 0, 'n', '', '127.0.0.1', 1366071435, '');
INSERT INTO `exp_online_users` VALUES(163, 1, 0, 'n', '', '127.0.0.1', 1366071435, '');
INSERT INTO `exp_online_users` VALUES(164, 1, 0, 'n', '', '127.0.0.1', 1366071435, '');
INSERT INTO `exp_online_users` VALUES(165, 1, 0, 'n', '', '127.0.0.1', 1366071435, '');
INSERT INTO `exp_online_users` VALUES(166, 1, 0, 'n', '', '127.0.0.1', 1366071435, '');
INSERT INTO `exp_online_users` VALUES(169, 1, 0, 'n', '', '127.0.0.1', 1366071435, '');
INSERT INTO `exp_online_users` VALUES(170, 1, 0, 'n', '', '127.0.0.1', 1366071435, '');
INSERT INTO `exp_online_users` VALUES(171, 1, 0, 'n', '', '127.0.0.1', 1366071435, '');
INSERT INTO `exp_online_users` VALUES(172, 1, 0, 'n', '', '127.0.0.1', 1366071435, '');
INSERT INTO `exp_online_users` VALUES(173, 1, 1, 'n', '96black', '127.0.0.1', 1366071434, '');
INSERT INTO `exp_online_users` VALUES(174, 1, 0, 'n', '', '127.0.0.1', 1366071435, '');
INSERT INTO `exp_online_users` VALUES(175, 1, 1, 'n', '96black', '127.0.0.1', 1366071434, '');
INSERT INTO `exp_online_users` VALUES(176, 1, 1, 'n', '96black', '127.0.0.1', 1366071434, '');
INSERT INTO `exp_online_users` VALUES(177, 1, 0, 'n', '', '127.0.0.1', 1366071435, '');

/* Table structure for table `exp_password_lockout` */
DROP TABLE IF EXISTS `exp_password_lockout`;

CREATE TABLE `exp_password_lockout` (
  `lockout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`lockout_id`),
  KEY `login_date` (`login_date`),
  KEY `ip_address` (`ip_address`),
  KEY `user_agent` (`user_agent`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_password_lockout` */
INSERT INTO `exp_password_lockout` VALUES(1, 1365558225, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.160 Safari/537.22', '96black');

/* Table structure for table `exp_ping_servers` */
DROP TABLE IF EXISTS `exp_ping_servers`;

CREATE TABLE `exp_ping_servers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `server_name` varchar(32) NOT NULL,
  `server_url` varchar(150) NOT NULL,
  `port` varchar(4) NOT NULL DEFAULT '80',
  `ping_protocol` varchar(12) NOT NULL DEFAULT 'xmlrpc',
  `is_default` char(1) NOT NULL DEFAULT 'y',
  `server_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_playa_relationships` */
DROP TABLE IF EXISTS `exp_playa_relationships`;

CREATE TABLE `exp_playa_relationships` (
  `rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_entry_id` int(10) unsigned DEFAULT NULL,
  `parent_field_id` int(6) unsigned DEFAULT NULL,
  `parent_col_id` int(6) unsigned DEFAULT NULL,
  `parent_row_id` int(10) unsigned DEFAULT NULL,
  `parent_var_id` int(6) unsigned DEFAULT NULL,
  `child_entry_id` int(10) unsigned DEFAULT NULL,
  `rel_order` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `parent_entry_id` (`parent_entry_id`),
  KEY `parent_field_id` (`parent_field_id`),
  KEY `parent_col_id` (`parent_col_id`),
  KEY `parent_row_id` (`parent_row_id`),
  KEY `parent_var_id` (`parent_var_id`),
  KEY `child_entry_id` (`child_entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_relationships` */
DROP TABLE IF EXISTS `exp_relationships`;

CREATE TABLE `exp_relationships` (
  `rel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `rel_parent_id` int(10) NOT NULL DEFAULT '0',
  `rel_child_id` int(10) NOT NULL DEFAULT '0',
  `rel_type` varchar(12) NOT NULL,
  `rel_data` mediumtext NOT NULL,
  `reverse_rel_data` mediumtext NOT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `rel_parent_id` (`rel_parent_id`),
  KEY `rel_child_id` (`rel_child_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_relationships` */
INSERT INTO `exp_relationships` VALUES(1, 12, 2, 'channel', '', '');
INSERT INTO `exp_relationships` VALUES(2, 13, 2, 'channel', '', '');
INSERT INTO `exp_relationships` VALUES(3, 14, 2, 'channel', '', '');
INSERT INTO `exp_relationships` VALUES(4, 15, 2, 'channel', '', '');
INSERT INTO `exp_relationships` VALUES(5, 2, 29, 'channel', 'a:3:{s:5:\"query\";O:18:\"CI_DB_mysql_result\":7:{s:7:\"conn_id\";i:0;s:9:\"result_id\";i:0;s:12:\"result_array\";a:1:{i:0;a:147:{s:8:\"entry_id\";s:2:\"29\";s:10:\"channel_id\";s:2:\"11\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:6:\"Topcon\";s:9:\"url_title\";s:6:\"topcon\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1365731435\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"04\";s:3:\"day\";s:2:\"12\";s:9:\"edit_date\";s:14:\"20130412145035\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:13:\"Manufacturers\";s:12:\"channel_name\";s:13:\"manufacturers\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"0\";s:11:\"field_ft_16\";N;s:11:\"field_id_17\";s:0:\"\";s:11:\"field_ft_17\";N;s:11:\"field_id_18\";s:0:\"\";s:11:\"field_ft_18\";N;s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";N;s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";N;s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";N;s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";N;s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";N;s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";N;s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";N;s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";N;s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";N;s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";N;s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";N;s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";N;s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";N;s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";N;s:11:\"field_id_34\";s:29:\"{filedir_10}footer-topcon.gif\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:27:\"{filedir_10}topcon-logo.gif\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:46:\"{filedir_10}mega-menu-feature-product-logo.jpg\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";s:11:\"field_id_38\";s:0:\"\";s:11:\"field_ft_38\";s:4:\"none\";s:11:\"field_id_39\";s:0:\"\";s:11:\"field_ft_39\";s:4:\"none\";s:11:\"field_id_40\";s:0:\"\";s:11:\"field_ft_40\";s:4:\"none\";s:11:\"field_id_42\";s:0:\"\";s:11:\"field_ft_42\";s:4:\"none\";s:11:\"field_id_43\";s:0:\"\";s:11:\"field_ft_43\";s:4:\"none\";s:11:\"field_id_44\";s:0:\"\";s:11:\"field_ft_44\";s:5:\"xhtml\";s:11:\"field_id_45\";s:0:\"\";s:11:\"field_ft_45\";s:5:\"xhtml\";}}s:13:\"result_object\";a:0:{}s:11:\"current_row\";i:0;s:8:\"num_rows\";i:1;s:8:\"row_data\";a:147:{s:8:\"entry_id\";s:2:\"29\";s:10:\"channel_id\";s:2:\"11\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:6:\"Topcon\";s:9:\"url_title\";s:6:\"topcon\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1365731435\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"04\";s:3:\"day\";s:2:\"12\";s:9:\"edit_date\";s:14:\"20130412145035\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:13:\"Manufacturers\";s:12:\"channel_name\";s:13:\"manufacturers\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"0\";s:11:\"field_ft_16\";N;s:11:\"field_id_17\";s:0:\"\";s:11:\"field_ft_17\";N;s:11:\"field_id_18\";s:0:\"\";s:11:\"field_ft_18\";N;s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";N;s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";N;s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";N;s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";N;s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";N;s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";N;s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";N;s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";N;s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";N;s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";N;s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";N;s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";N;s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";N;s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";N;s:11:\"field_id_34\";s:29:\"{filedir_10}footer-topcon.gif\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:27:\"{filedir_10}topcon-logo.gif\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:46:\"{filedir_10}mega-menu-feature-product-logo.jpg\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";s:11:\"field_id_38\";s:0:\"\";s:11:\"field_ft_38\";s:4:\"none\";s:11:\"field_id_39\";s:0:\"\";s:11:\"field_ft_39\";s:4:\"none\";s:11:\"field_id_40\";s:0:\"\";s:11:\"field_ft_40\";s:4:\"none\";s:11:\"field_id_42\";s:0:\"\";s:11:\"field_ft_42\";s:4:\"none\";s:11:\"field_id_43\";s:0:\"\";s:11:\"field_ft_43\";s:4:\"none\";s:11:\"field_id_44\";s:0:\"\";s:11:\"field_ft_44\";s:5:\"xhtml\";s:11:\"field_id_45\";s:0:\"\";s:11:\"field_ft_45\";s:5:\"xhtml\";}}s:10:\"cats_fixed\";s:1:\"1\";s:10:\"categories\";a:0:{}}', '');

/* Table structure for table `exp_remember_me` */
DROP TABLE IF EXISTS `exp_remember_me`;

CREATE TABLE `exp_remember_me` (
  `remember_me_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) DEFAULT '0',
  `ip_address` varchar(45) DEFAULT '0',
  `user_agent` varchar(120) DEFAULT '',
  `admin_sess` tinyint(1) DEFAULT '0',
  `site_id` int(4) DEFAULT '1',
  `expiration` int(10) DEFAULT '0',
  `last_refresh` int(10) DEFAULT '0',
  PRIMARY KEY (`remember_me_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_reset_password` */
DROP TABLE IF EXISTS `exp_reset_password`;

CREATE TABLE `exp_reset_password` (
  `reset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL,
  `resetcode` varchar(12) NOT NULL,
  `date` int(10) NOT NULL,
  PRIMARY KEY (`reset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_revision_tracker` */
DROP TABLE IF EXISTS `exp_revision_tracker`;

CREATE TABLE `exp_revision_tracker` (
  `tracker_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `item_table` varchar(20) NOT NULL,
  `item_field` varchar(20) NOT NULL,
  `item_date` int(10) NOT NULL,
  `item_author_id` int(10) unsigned NOT NULL,
  `item_data` mediumtext NOT NULL,
  PRIMARY KEY (`tracker_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_rte_tools` */
DROP TABLE IF EXISTS `exp_rte_tools`;

CREATE TABLE `exp_rte_tools` (
  `tool_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(75) DEFAULT NULL,
  `class` varchar(75) DEFAULT NULL,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`tool_id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_rte_tools` */
INSERT INTO `exp_rte_tools` VALUES(1, 'Blockquote', 'Blockquote_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(2, 'Bold', 'Bold_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(3, 'Headings', 'Headings_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(4, 'Image', 'Image_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(5, 'Italic', 'Italic_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(6, 'Link', 'Link_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(7, 'Ordered List', 'Ordered_list_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(8, 'Underline', 'Underline_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(9, 'Unordered List', 'Unordered_list_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(10, 'View Source', 'View_source_rte', 'y');

/* Table structure for table `exp_rte_toolsets` */
DROP TABLE IF EXISTS `exp_rte_toolsets`;

CREATE TABLE `exp_rte_toolsets` (
  `toolset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `tools` text,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`toolset_id`),
  KEY `member_id` (`member_id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_rte_toolsets` */
INSERT INTO `exp_rte_toolsets` VALUES(1, 0, 'Default', '3|2|5|1|9|7|6|4|10', 'y');

/* Table structure for table `exp_search` */
DROP TABLE IF EXISTS `exp_search`;

CREATE TABLE `exp_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) NOT NULL DEFAULT '1',
  `search_date` int(10) NOT NULL,
  `keywords` varchar(60) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(6) NOT NULL,
  `per_page` tinyint(3) unsigned NOT NULL,
  `query` mediumtext,
  `custom_fields` mediumtext,
  `result_page` varchar(70) NOT NULL,
  PRIMARY KEY (`search_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/* dumping data for table `exp_search` */
INSERT INTO `exp_search` VALUES('2d4b59555b66e5667243577eeb02ab56', 1, 1365964656, 'top', 1, '127.0.0.1', 3, 50, 's:1416:\\\"SELECT DISTINCT(t.entry_id), t.entry_id, t.channel_id, t.forum_topic_id, t.author_id, t.ip_address, t.title, t.url_title, t.status, t.dst_enabled, t.view_count_one, t.view_count_two, t.view_count_three, t.view_count_four, t.allow_comments, t.comment_expiration_date, t.sticky, t.entry_date, t.year, t.month, t.day, t.entry_date, t.edit_date, t.expiration_date, t.recent_comment_date, t.comment_total, t.site_id as entry_site_id,\n				w.channel_title, w.channel_name, w.search_results_url, w.search_excerpt, w.channel_url, w.comment_url, w.comment_moderate, w.channel_html_formatting, w.channel_allow_img_urls, w.channel_auto_link_urls, w.comment_system_enabled, \n				m.username, m.email, m.url, m.screen_name, m.location, m.occupation, m.interests, m.aol_im, m.yahoo_im, m.msn_im, m.icq, m.signature, m.sig_img_filename, m.sig_img_width, m.sig_img_height, m.avatar_filename, m.avatar_width, m.avatar_height, m.photo_filename, m.photo_width, m.photo_height, m.group_id, m.member_id, m.bday_d, m.bday_m, m.bday_y, m.bio,\n				md.*,\n				wd.*\n			FROM MDBMPREFIXchannel_titles		AS t\n			LEFT JOIN MDBMPREFIXchannels 		AS w  ON t.channel_id = w.channel_id \n			LEFT JOIN MDBMPREFIXchannel_data	AS wd ON t.entry_id = wd.entry_id \n			LEFT JOIN MDBMPREFIXmembers		AS m  ON m.member_id = t.author_id \n			LEFT JOIN MDBMPREFIXmember_data	AS md ON md.member_id = m.member_id \n			WHERE t.entry_id IN (1,2,3)  ORDER BY entry_date  desc\\\";', 'a:15:{s:14:\\\"product_prices\\\";a:2:{i:0;s:1:\\\"1\\\";i:1;s:1:\\\"n\\\";}s:19:\\\"product_description\\\";a:2:{i:0;s:1:\\\"2\\\";i:1;s:1:\\\"n\\\";}s:24:\\\"technical_specifications\\\";a:2:{i:0;s:1:\\\"3\\\";i:1;s:1:\\\"n\\\";}s:14:\\\"product_videos\\\";a:2:{i:0;s:1:\\\"4\\\";i:1;s:1:\\\"n\\\";}s:15:\\\"rental_sections\\\";a:2:{i:0;s:1:\\\"5\\\";i:1;s:1:\\\"n\\\";}s:17:\\\"rental_pricing_nz\\\";a:2:{i:0;s:1:\\\"6\\\";i:1;s:1:\\\"n\\\";}s:17:\\\"rental_pricing_au\\\";a:2:{i:0;s:1:\\\"7\\\";i:1;s:1:\\\"n\\\";}s:24:\\\"featured_carousel_images\\\";a:2:{i:0;s:1:\\\"8\\\";i:1;s:1:\\\"n\\\";}s:19:\\\"is_featured_product\\\";a:2:{i:0;s:1:\\\"9\\\";i:1;s:1:\\\"y\\\";}s:14:\\\"product_images\\\";a:2:{i:0;s:2:\\\"10\\\";i:1;s:1:\\\"n\\\";}s:17:\\\"product_downloads\\\";a:2:{i:0;s:2:\\\"11\\\";i:1;s:1:\\\"n\\\";}s:15:\\\"service_content\\\";a:2:{i:0;s:2:\\\"26\\\";i:1;s:1:\\\"n\\\";}s:15:\\\"services_banner\\\";a:2:{i:0;s:2:\\\"27\\\";i:1;s:1:\\\"n\\\";}s:22:\\\"featured_megamenu_icon\\\";a:2:{i:0;s:2:\\\"33\\\";i:1;s:1:\\\"y\\\";}s:12:\\\"manufacturer\\\";a:2:{i:0;s:2:\\\"37\\\";i:1;s:1:\\\"y\\\";}}', 'search/_results');
INSERT INTO `exp_search` VALUES('ec159181bef8fbf671fdeb0f2753bde2', 1, 1365964773, 'top', 1, '127.0.0.1', 3, 50, 's:1416:\\\"SELECT DISTINCT(t.entry_id), t.entry_id, t.channel_id, t.forum_topic_id, t.author_id, t.ip_address, t.title, t.url_title, t.status, t.dst_enabled, t.view_count_one, t.view_count_two, t.view_count_three, t.view_count_four, t.allow_comments, t.comment_expiration_date, t.sticky, t.entry_date, t.year, t.month, t.day, t.entry_date, t.edit_date, t.expiration_date, t.recent_comment_date, t.comment_total, t.site_id as entry_site_id,\n				w.channel_title, w.channel_name, w.search_results_url, w.search_excerpt, w.channel_url, w.comment_url, w.comment_moderate, w.channel_html_formatting, w.channel_allow_img_urls, w.channel_auto_link_urls, w.comment_system_enabled, \n				m.username, m.email, m.url, m.screen_name, m.location, m.occupation, m.interests, m.aol_im, m.yahoo_im, m.msn_im, m.icq, m.signature, m.sig_img_filename, m.sig_img_width, m.sig_img_height, m.avatar_filename, m.avatar_width, m.avatar_height, m.photo_filename, m.photo_width, m.photo_height, m.group_id, m.member_id, m.bday_d, m.bday_m, m.bday_y, m.bio,\n				md.*,\n				wd.*\n			FROM MDBMPREFIXchannel_titles		AS t\n			LEFT JOIN MDBMPREFIXchannels 		AS w  ON t.channel_id = w.channel_id \n			LEFT JOIN MDBMPREFIXchannel_data	AS wd ON t.entry_id = wd.entry_id \n			LEFT JOIN MDBMPREFIXmembers		AS m  ON m.member_id = t.author_id \n			LEFT JOIN MDBMPREFIXmember_data	AS md ON md.member_id = m.member_id \n			WHERE t.entry_id IN (1,2,3)  ORDER BY entry_date  desc\\\";', 'a:15:{s:14:\\\"product_prices\\\";a:2:{i:0;s:1:\\\"1\\\";i:1;s:1:\\\"n\\\";}s:19:\\\"product_description\\\";a:2:{i:0;s:1:\\\"2\\\";i:1;s:1:\\\"n\\\";}s:24:\\\"technical_specifications\\\";a:2:{i:0;s:1:\\\"3\\\";i:1;s:1:\\\"n\\\";}s:14:\\\"product_videos\\\";a:2:{i:0;s:1:\\\"4\\\";i:1;s:1:\\\"n\\\";}s:15:\\\"rental_sections\\\";a:2:{i:0;s:1:\\\"5\\\";i:1;s:1:\\\"n\\\";}s:17:\\\"rental_pricing_nz\\\";a:2:{i:0;s:1:\\\"6\\\";i:1;s:1:\\\"n\\\";}s:17:\\\"rental_pricing_au\\\";a:2:{i:0;s:1:\\\"7\\\";i:1;s:1:\\\"n\\\";}s:24:\\\"featured_carousel_images\\\";a:2:{i:0;s:1:\\\"8\\\";i:1;s:1:\\\"n\\\";}s:19:\\\"is_featured_product\\\";a:2:{i:0;s:1:\\\"9\\\";i:1;s:1:\\\"y\\\";}s:14:\\\"product_images\\\";a:2:{i:0;s:2:\\\"10\\\";i:1;s:1:\\\"n\\\";}s:17:\\\"product_downloads\\\";a:2:{i:0;s:2:\\\"11\\\";i:1;s:1:\\\"n\\\";}s:15:\\\"service_content\\\";a:2:{i:0;s:2:\\\"26\\\";i:1;s:1:\\\"n\\\";}s:15:\\\"services_banner\\\";a:2:{i:0;s:2:\\\"27\\\";i:1;s:1:\\\"n\\\";}s:22:\\\"featured_megamenu_icon\\\";a:2:{i:0;s:2:\\\"33\\\";i:1;s:1:\\\"y\\\";}s:12:\\\"manufacturer\\\";a:2:{i:0;s:2:\\\"37\\\";i:1;s:1:\\\"y\\\";}}', 'search/_results');
INSERT INTO `exp_search` VALUES('f6e496ab23b6b6381cc7234db0b6f173', 1, 1365964789, 'top', 1, '127.0.0.1', 3, 50, 's:1416:\\\"SELECT DISTINCT(t.entry_id), t.entry_id, t.channel_id, t.forum_topic_id, t.author_id, t.ip_address, t.title, t.url_title, t.status, t.dst_enabled, t.view_count_one, t.view_count_two, t.view_count_three, t.view_count_four, t.allow_comments, t.comment_expiration_date, t.sticky, t.entry_date, t.year, t.month, t.day, t.entry_date, t.edit_date, t.expiration_date, t.recent_comment_date, t.comment_total, t.site_id as entry_site_id,\n				w.channel_title, w.channel_name, w.search_results_url, w.search_excerpt, w.channel_url, w.comment_url, w.comment_moderate, w.channel_html_formatting, w.channel_allow_img_urls, w.channel_auto_link_urls, w.comment_system_enabled, \n				m.username, m.email, m.url, m.screen_name, m.location, m.occupation, m.interests, m.aol_im, m.yahoo_im, m.msn_im, m.icq, m.signature, m.sig_img_filename, m.sig_img_width, m.sig_img_height, m.avatar_filename, m.avatar_width, m.avatar_height, m.photo_filename, m.photo_width, m.photo_height, m.group_id, m.member_id, m.bday_d, m.bday_m, m.bday_y, m.bio,\n				md.*,\n				wd.*\n			FROM MDBMPREFIXchannel_titles		AS t\n			LEFT JOIN MDBMPREFIXchannels 		AS w  ON t.channel_id = w.channel_id \n			LEFT JOIN MDBMPREFIXchannel_data	AS wd ON t.entry_id = wd.entry_id \n			LEFT JOIN MDBMPREFIXmembers		AS m  ON m.member_id = t.author_id \n			LEFT JOIN MDBMPREFIXmember_data	AS md ON md.member_id = m.member_id \n			WHERE t.entry_id IN (1,2,3)  ORDER BY entry_date  desc\\\";', 'a:15:{s:14:\\\"product_prices\\\";a:2:{i:0;s:1:\\\"1\\\";i:1;s:1:\\\"n\\\";}s:19:\\\"product_description\\\";a:2:{i:0;s:1:\\\"2\\\";i:1;s:1:\\\"n\\\";}s:24:\\\"technical_specifications\\\";a:2:{i:0;s:1:\\\"3\\\";i:1;s:1:\\\"n\\\";}s:14:\\\"product_videos\\\";a:2:{i:0;s:1:\\\"4\\\";i:1;s:1:\\\"n\\\";}s:15:\\\"rental_sections\\\";a:2:{i:0;s:1:\\\"5\\\";i:1;s:1:\\\"n\\\";}s:17:\\\"rental_pricing_nz\\\";a:2:{i:0;s:1:\\\"6\\\";i:1;s:1:\\\"n\\\";}s:17:\\\"rental_pricing_au\\\";a:2:{i:0;s:1:\\\"7\\\";i:1;s:1:\\\"n\\\";}s:24:\\\"featured_carousel_images\\\";a:2:{i:0;s:1:\\\"8\\\";i:1;s:1:\\\"n\\\";}s:19:\\\"is_featured_product\\\";a:2:{i:0;s:1:\\\"9\\\";i:1;s:1:\\\"y\\\";}s:14:\\\"product_images\\\";a:2:{i:0;s:2:\\\"10\\\";i:1;s:1:\\\"n\\\";}s:17:\\\"product_downloads\\\";a:2:{i:0;s:2:\\\"11\\\";i:1;s:1:\\\"n\\\";}s:15:\\\"service_content\\\";a:2:{i:0;s:2:\\\"26\\\";i:1;s:1:\\\"n\\\";}s:15:\\\"services_banner\\\";a:2:{i:0;s:2:\\\"27\\\";i:1;s:1:\\\"n\\\";}s:22:\\\"featured_megamenu_icon\\\";a:2:{i:0;s:2:\\\"33\\\";i:1;s:1:\\\"y\\\";}s:12:\\\"manufacturer\\\";a:2:{i:0;s:2:\\\"37\\\";i:1;s:1:\\\"y\\\";}}', 'search/_results');
INSERT INTO `exp_search` VALUES('28f33f4686f43a2e00aa3412d0ac9875', 1, 1365965109, 'topc', 1, '127.0.0.1', 3, 50, 's:1416:\\\"SELECT DISTINCT(t.entry_id), t.entry_id, t.channel_id, t.forum_topic_id, t.author_id, t.ip_address, t.title, t.url_title, t.status, t.dst_enabled, t.view_count_one, t.view_count_two, t.view_count_three, t.view_count_four, t.allow_comments, t.comment_expiration_date, t.sticky, t.entry_date, t.year, t.month, t.day, t.entry_date, t.edit_date, t.expiration_date, t.recent_comment_date, t.comment_total, t.site_id as entry_site_id,\n				w.channel_title, w.channel_name, w.search_results_url, w.search_excerpt, w.channel_url, w.comment_url, w.comment_moderate, w.channel_html_formatting, w.channel_allow_img_urls, w.channel_auto_link_urls, w.comment_system_enabled, \n				m.username, m.email, m.url, m.screen_name, m.location, m.occupation, m.interests, m.aol_im, m.yahoo_im, m.msn_im, m.icq, m.signature, m.sig_img_filename, m.sig_img_width, m.sig_img_height, m.avatar_filename, m.avatar_width, m.avatar_height, m.photo_filename, m.photo_width, m.photo_height, m.group_id, m.member_id, m.bday_d, m.bday_m, m.bday_y, m.bio,\n				md.*,\n				wd.*\n			FROM MDBMPREFIXchannel_titles		AS t\n			LEFT JOIN MDBMPREFIXchannels 		AS w  ON t.channel_id = w.channel_id \n			LEFT JOIN MDBMPREFIXchannel_data	AS wd ON t.entry_id = wd.entry_id \n			LEFT JOIN MDBMPREFIXmembers		AS m  ON m.member_id = t.author_id \n			LEFT JOIN MDBMPREFIXmember_data	AS md ON md.member_id = m.member_id \n			WHERE t.entry_id IN (1,2,3)  ORDER BY entry_date  desc\\\";', 'a:15:{s:14:\\\"product_prices\\\";a:2:{i:0;s:1:\\\"1\\\";i:1;s:1:\\\"n\\\";}s:19:\\\"product_description\\\";a:2:{i:0;s:1:\\\"2\\\";i:1;s:1:\\\"n\\\";}s:24:\\\"technical_specifications\\\";a:2:{i:0;s:1:\\\"3\\\";i:1;s:1:\\\"n\\\";}s:14:\\\"product_videos\\\";a:2:{i:0;s:1:\\\"4\\\";i:1;s:1:\\\"n\\\";}s:15:\\\"rental_sections\\\";a:2:{i:0;s:1:\\\"5\\\";i:1;s:1:\\\"n\\\";}s:17:\\\"rental_pricing_nz\\\";a:2:{i:0;s:1:\\\"6\\\";i:1;s:1:\\\"n\\\";}s:17:\\\"rental_pricing_au\\\";a:2:{i:0;s:1:\\\"7\\\";i:1;s:1:\\\"n\\\";}s:24:\\\"featured_carousel_images\\\";a:2:{i:0;s:1:\\\"8\\\";i:1;s:1:\\\"n\\\";}s:19:\\\"is_featured_product\\\";a:2:{i:0;s:1:\\\"9\\\";i:1;s:1:\\\"y\\\";}s:14:\\\"product_images\\\";a:2:{i:0;s:2:\\\"10\\\";i:1;s:1:\\\"n\\\";}s:17:\\\"product_downloads\\\";a:2:{i:0;s:2:\\\"11\\\";i:1;s:1:\\\"n\\\";}s:15:\\\"service_content\\\";a:2:{i:0;s:2:\\\"26\\\";i:1;s:1:\\\"n\\\";}s:15:\\\"services_banner\\\";a:2:{i:0;s:2:\\\"27\\\";i:1;s:1:\\\"n\\\";}s:22:\\\"featured_megamenu_icon\\\";a:2:{i:0;s:2:\\\"33\\\";i:1;s:1:\\\"y\\\";}s:12:\\\"manufacturer\\\";a:2:{i:0;s:2:\\\"37\\\";i:1;s:1:\\\"y\\\";}}', 'search/_results');
INSERT INTO `exp_search` VALUES('ac62d1b723e016f1786685986255eaa3', 1, 1365965121, 'topc', 1, '127.0.0.1', 3, 50, 's:1416:\\\"SELECT DISTINCT(t.entry_id), t.entry_id, t.channel_id, t.forum_topic_id, t.author_id, t.ip_address, t.title, t.url_title, t.status, t.dst_enabled, t.view_count_one, t.view_count_two, t.view_count_three, t.view_count_four, t.allow_comments, t.comment_expiration_date, t.sticky, t.entry_date, t.year, t.month, t.day, t.entry_date, t.edit_date, t.expiration_date, t.recent_comment_date, t.comment_total, t.site_id as entry_site_id,\n				w.channel_title, w.channel_name, w.search_results_url, w.search_excerpt, w.channel_url, w.comment_url, w.comment_moderate, w.channel_html_formatting, w.channel_allow_img_urls, w.channel_auto_link_urls, w.comment_system_enabled, \n				m.username, m.email, m.url, m.screen_name, m.location, m.occupation, m.interests, m.aol_im, m.yahoo_im, m.msn_im, m.icq, m.signature, m.sig_img_filename, m.sig_img_width, m.sig_img_height, m.avatar_filename, m.avatar_width, m.avatar_height, m.photo_filename, m.photo_width, m.photo_height, m.group_id, m.member_id, m.bday_d, m.bday_m, m.bday_y, m.bio,\n				md.*,\n				wd.*\n			FROM MDBMPREFIXchannel_titles		AS t\n			LEFT JOIN MDBMPREFIXchannels 		AS w  ON t.channel_id = w.channel_id \n			LEFT JOIN MDBMPREFIXchannel_data	AS wd ON t.entry_id = wd.entry_id \n			LEFT JOIN MDBMPREFIXmembers		AS m  ON m.member_id = t.author_id \n			LEFT JOIN MDBMPREFIXmember_data	AS md ON md.member_id = m.member_id \n			WHERE t.entry_id IN (1,2,3)  ORDER BY entry_date  desc\\\";', 'a:15:{s:14:\\\"product_prices\\\";a:2:{i:0;s:1:\\\"1\\\";i:1;s:1:\\\"n\\\";}s:19:\\\"product_description\\\";a:2:{i:0;s:1:\\\"2\\\";i:1;s:1:\\\"n\\\";}s:24:\\\"technical_specifications\\\";a:2:{i:0;s:1:\\\"3\\\";i:1;s:1:\\\"n\\\";}s:14:\\\"product_videos\\\";a:2:{i:0;s:1:\\\"4\\\";i:1;s:1:\\\"n\\\";}s:15:\\\"rental_sections\\\";a:2:{i:0;s:1:\\\"5\\\";i:1;s:1:\\\"n\\\";}s:17:\\\"rental_pricing_nz\\\";a:2:{i:0;s:1:\\\"6\\\";i:1;s:1:\\\"n\\\";}s:17:\\\"rental_pricing_au\\\";a:2:{i:0;s:1:\\\"7\\\";i:1;s:1:\\\"n\\\";}s:24:\\\"featured_carousel_images\\\";a:2:{i:0;s:1:\\\"8\\\";i:1;s:1:\\\"n\\\";}s:19:\\\"is_featured_product\\\";a:2:{i:0;s:1:\\\"9\\\";i:1;s:1:\\\"y\\\";}s:14:\\\"product_images\\\";a:2:{i:0;s:2:\\\"10\\\";i:1;s:1:\\\"n\\\";}s:17:\\\"product_downloads\\\";a:2:{i:0;s:2:\\\"11\\\";i:1;s:1:\\\"n\\\";}s:15:\\\"service_content\\\";a:2:{i:0;s:2:\\\"26\\\";i:1;s:1:\\\"n\\\";}s:15:\\\"services_banner\\\";a:2:{i:0;s:2:\\\"27\\\";i:1;s:1:\\\"n\\\";}s:22:\\\"featured_megamenu_icon\\\";a:2:{i:0;s:2:\\\"33\\\";i:1;s:1:\\\"y\\\";}s:12:\\\"manufacturer\\\";a:2:{i:0;s:2:\\\"37\\\";i:1;s:1:\\\"y\\\";}}', 'search/results');

/* Table structure for table `exp_search_log` */
DROP TABLE IF EXISTS `exp_search_log`;

CREATE TABLE `exp_search_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `search_date` int(10) NOT NULL,
  `search_type` varchar(32) NOT NULL,
  `search_terms` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_search_log` */
INSERT INTO `exp_search_log` VALUES(1, 1, 1, '96black', '127.0.0.1', 1365964656, 'site', 'top');
INSERT INTO `exp_search_log` VALUES(2, 1, 1, '96black', '127.0.0.1', 1365964773, 'site', 'top');
INSERT INTO `exp_search_log` VALUES(3, 1, 1, '96black', '127.0.0.1', 1365964785, 'site', 'tes');
INSERT INTO `exp_search_log` VALUES(4, 1, 1, '96black', '127.0.0.1', 1365964789, 'site', 'top');
INSERT INTO `exp_search_log` VALUES(5, 1, 1, '96black', '127.0.0.1', 1365965109, 'site', 'topc');
INSERT INTO `exp_search_log` VALUES(6, 1, 1, '96black', '127.0.0.1', 1365965121, 'site', 'topc');

/* Table structure for table `exp_security_hashes` */
DROP TABLE IF EXISTS `exp_security_hashes`;

CREATE TABLE `exp_security_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `hash` varchar(40) NOT NULL,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB AUTO_INCREMENT=3137 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_security_hashes` */
INSERT INTO `exp_security_hashes` VALUES(2349, 1366063801, '127.0.0.1', '3f9e3c537718068389df3754f3d3ea04b6f996bc');
INSERT INTO `exp_security_hashes` VALUES(2350, 1366063801, '127.0.0.1', 'cbb1cf9264b234ca4e46f30b81cf4fec93f0eca6');
INSERT INTO `exp_security_hashes` VALUES(2351, 1366063802, '127.0.0.1', '8ef0344efd0f62d0452cd9694813cbef8a4db6ef');
INSERT INTO `exp_security_hashes` VALUES(2352, 1366063837, '127.0.0.1', 'd1b118af9666b3e153b0ab93a3a6267b66a81023');
INSERT INTO `exp_security_hashes` VALUES(2353, 1366063838, '127.0.0.1', 'c1612be95fe0791433bf0462378703aecaea9c41');
INSERT INTO `exp_security_hashes` VALUES(2354, 1366063838, '127.0.0.1', '3ea8795a15fd3e12cff64653772a7491e9807ef8');
INSERT INTO `exp_security_hashes` VALUES(2355, 1366064669, '127.0.0.1', 'bd8a9850f1ea4449f3163037e7d50cfd070dddce');
INSERT INTO `exp_security_hashes` VALUES(2356, 1366064669, '127.0.0.1', 'b1d57f3c92423ffe24d03cca18495c79037f7656');
INSERT INTO `exp_security_hashes` VALUES(2357, 1366064669, '127.0.0.1', 'fee0e6c797a9b2aa92c13a5418df2d75c6aa1c64');
INSERT INTO `exp_security_hashes` VALUES(2358, 1366064670, '127.0.0.1', 'c5b1ca6c094921fbc6f6fe476ef2f6da5ac522ba');
INSERT INTO `exp_security_hashes` VALUES(2359, 1366064678, '127.0.0.1', '6425dcfb118a8d993b51d793e02035f50a11be07');
INSERT INTO `exp_security_hashes` VALUES(2360, 1366064678, '127.0.0.1', 'bd011b891db8c54ce2660b7067faf84a58aafd80');
INSERT INTO `exp_security_hashes` VALUES(2361, 1366064679, '127.0.0.1', 'fa4470fc0e639065c6967591300fdfca3725e921');
INSERT INTO `exp_security_hashes` VALUES(2362, 1366064680, '127.0.0.1', '3ee8321472ee196c43cb2ac31136332a4d518b5c');
INSERT INTO `exp_security_hashes` VALUES(2363, 1366064687, '127.0.0.1', '2c4849ad85a910bda6ada1cf4e40528d5c121c55');
INSERT INTO `exp_security_hashes` VALUES(2364, 1366064687, '127.0.0.1', '8a9a6af470a4c0135989965250f02384f66ff1fe');
INSERT INTO `exp_security_hashes` VALUES(2365, 1366064688, '127.0.0.1', '580649a17c2852386ed9c310652d2fd73a6a118b');
INSERT INTO `exp_security_hashes` VALUES(2366, 1366065328, '127.0.0.1', '1e6bfb7c784e8432a43bdde5db7cf82bf0105e0a');
INSERT INTO `exp_security_hashes` VALUES(2367, 1366065328, '127.0.0.1', 'b7ab5858ffb2c1460520b78d427f0064df93bae7');
INSERT INTO `exp_security_hashes` VALUES(2368, 1366065329, '127.0.0.1', '988f7716e1129b11c54e07b4b9d64c3861868a7b');
INSERT INTO `exp_security_hashes` VALUES(2369, 1366065334, '127.0.0.1', 'dede9e619e1c040a06f19851e247404f3b747e5c');
INSERT INTO `exp_security_hashes` VALUES(2370, 1366065335, '127.0.0.1', '15210f1a17c10a15530162e37b553dc4448a12ed');
INSERT INTO `exp_security_hashes` VALUES(2371, 1366065336, '127.0.0.1', '108840ad9cfcae3a0db40ff3ab3ad2a7894f9565');
INSERT INTO `exp_security_hashes` VALUES(2372, 1366065342, '127.0.0.1', 'a1c76b03846852a6ef9501d31308fb7c77cc9897');
INSERT INTO `exp_security_hashes` VALUES(2373, 1366065343, '127.0.0.1', '8803293fbec1bde745d558e23dc216621801d1cc');
INSERT INTO `exp_security_hashes` VALUES(2374, 1366065344, '127.0.0.1', '5f4d40f7aa25b7eef8efdefa91e41ece99fba9bb');
INSERT INTO `exp_security_hashes` VALUES(2375, 1366065345, '127.0.0.1', 'ecafaa7267f6a68e85618a2e2c44cf8f86336575');
INSERT INTO `exp_security_hashes` VALUES(2376, 1366065346, '127.0.0.1', 'a1e138e00bac326d9815423459a4be45a59194c2');
INSERT INTO `exp_security_hashes` VALUES(2377, 1366065347, '127.0.0.1', 'fb5ae75437302fb6514db9fdfd34163c8ae66ee0');
INSERT INTO `exp_security_hashes` VALUES(2378, 1366065398, '127.0.0.1', '5296e6794578f6c411e7d21cd779de20516b08d4');
INSERT INTO `exp_security_hashes` VALUES(2379, 1366065398, '127.0.0.1', '66aa11ded8662e45260828c4b210e09cfc6f4700');
INSERT INTO `exp_security_hashes` VALUES(2380, 1366065398, '127.0.0.1', '1030bf3a13d2b5715228ff198a35ebdfb8753d89');
INSERT INTO `exp_security_hashes` VALUES(2381, 1366065403, '127.0.0.1', '14bf4a6dd21d23da8acfe89fd6b563f0c2e05eae');
INSERT INTO `exp_security_hashes` VALUES(2382, 1366065404, '127.0.0.1', '5cbfaccfa9e789c7cbc83438290882ef3f919905');
INSERT INTO `exp_security_hashes` VALUES(2383, 1366065409, '127.0.0.1', '53d619a0d8f1092fb55d066896313c6bf7debde5');
INSERT INTO `exp_security_hashes` VALUES(2384, 1366065409, '127.0.0.1', '4743be6a95c54f2364d664bbb5691cb15910bae2');
INSERT INTO `exp_security_hashes` VALUES(2385, 1366065410, '127.0.0.1', 'a4a8202ff895fc9a6c4afa6f93f923f36e5258b0');
INSERT INTO `exp_security_hashes` VALUES(2386, 1366065417, '127.0.0.1', 'e55df8d0738a15de99d21d71c2b47dfcde2d0087');
INSERT INTO `exp_security_hashes` VALUES(2387, 1366065418, '127.0.0.1', 'c6b5ab609204fda14dbb521df5cbe88c6ee45a34');
INSERT INTO `exp_security_hashes` VALUES(2388, 1366065419, '127.0.0.1', '8298fcf932b5660fabb360f0d7932dcc760d4ba1');
INSERT INTO `exp_security_hashes` VALUES(2389, 1366065420, '127.0.0.1', '90a7307a4b606b491f8ef40d5bfb033c0b6039c7');
INSERT INTO `exp_security_hashes` VALUES(2390, 1366065420, '127.0.0.1', '6c10746b829e2992f95205a06ce89b35c1144df4');
INSERT INTO `exp_security_hashes` VALUES(2391, 1366065420, '127.0.0.1', '8e574bc81ace1b6ebec2432d2caed3295b8361b7');
INSERT INTO `exp_security_hashes` VALUES(2392, 1366065420, '127.0.0.1', 'fe773209d65c20f56628edd9eed83710f51c3d7b');
INSERT INTO `exp_security_hashes` VALUES(2393, 1366065420, '127.0.0.1', 'e5e5bd9391bec32ba58dc5b1845ec3896c32f2bb');
INSERT INTO `exp_security_hashes` VALUES(2394, 1366065420, '127.0.0.1', 'c8efacb814a1246746a478b9ebeeedd6de82aa19');
INSERT INTO `exp_security_hashes` VALUES(2395, 1366065420, '127.0.0.1', '4997b0139bc00538f83e8af601d14fa8738909b9');
INSERT INTO `exp_security_hashes` VALUES(2396, 1366065420, '127.0.0.1', '9303485b349c8e5677c6d65b96b2c34c28dd3c3b');
INSERT INTO `exp_security_hashes` VALUES(2397, 1366065420, '127.0.0.1', '547b65e3f6a056700e6546f4fa2ce274d86843ec');
INSERT INTO `exp_security_hashes` VALUES(2398, 1366065421, '127.0.0.1', '102c714a8e340f0e9316d8a933229a9eab909383');
INSERT INTO `exp_security_hashes` VALUES(2399, 1366065421, '127.0.0.1', 'b85e2b1991128cde05d646ed77e45d50dfb9125c');
INSERT INTO `exp_security_hashes` VALUES(2400, 1366065421, '127.0.0.1', '6b6c346b2f8913ac045c058797edce7e2f46cbce');
INSERT INTO `exp_security_hashes` VALUES(2401, 1366065421, '127.0.0.1', '29ac1b67772e7cc21167844db56951038a43c087');
INSERT INTO `exp_security_hashes` VALUES(2402, 1366065421, '127.0.0.1', '1910188fbe52861fc6b65b6feb6c08d561da102f');
INSERT INTO `exp_security_hashes` VALUES(2403, 1366065421, '127.0.0.1', 'c9fa5dfd8b27878f1ec1c5a55c9da5ee464a8501');
INSERT INTO `exp_security_hashes` VALUES(2404, 1366065421, '127.0.0.1', '5ac7b779d8fb01c3bb5d24301f3e4578faf579df');
INSERT INTO `exp_security_hashes` VALUES(2405, 1366065421, '127.0.0.1', '02abe859470117819f2c8184bb5b94028632d895');
INSERT INTO `exp_security_hashes` VALUES(2406, 1366065421, '127.0.0.1', 'f9dc528d5754a5322b675d70d6be1ecc7ad3303d');
INSERT INTO `exp_security_hashes` VALUES(2407, 1366065421, '127.0.0.1', 'e5410cb9417fafbfa99e41e5610a344b3c2f59a4');
INSERT INTO `exp_security_hashes` VALUES(2408, 1366065422, '127.0.0.1', '3096f301d4a86665f72c85715de6985f5cc7b8a4');
INSERT INTO `exp_security_hashes` VALUES(2409, 1366065422, '127.0.0.1', 'f86daa05fcb1f546b893866ddac1f4ff9115b2cf');
INSERT INTO `exp_security_hashes` VALUES(2410, 1366065422, '127.0.0.1', 'a5fb571f5e2e8a2073362326358c6c084a58a37c');
INSERT INTO `exp_security_hashes` VALUES(2411, 1366065422, '127.0.0.1', 'f5ab1ebc746d7d9da2a140cca4cd2744b7ef99b1');
INSERT INTO `exp_security_hashes` VALUES(2412, 1366065422, '127.0.0.1', '3f00c06e28df219db70430335fdb7b1ed367f522');
INSERT INTO `exp_security_hashes` VALUES(2413, 1366065422, '127.0.0.1', 'cc1f5982e4f150f9e5b2bb6aa29de87978eeddbc');
INSERT INTO `exp_security_hashes` VALUES(2414, 1366065422, '127.0.0.1', 'f3331ac406c0cf8a2f0fb91880ce8cd6b974396f');
INSERT INTO `exp_security_hashes` VALUES(2415, 1366065423, '127.0.0.1', 'a19d4b75509b4878ac62a0474a88f3ca519fb887');
INSERT INTO `exp_security_hashes` VALUES(2416, 1366065423, '127.0.0.1', '69f79761ebca8cab4bc41b47bd533a9a21037c63');
INSERT INTO `exp_security_hashes` VALUES(2417, 1366065423, '127.0.0.1', 'af1b853524459b7be5ef5dabae89baf4fd2e0cf6');
INSERT INTO `exp_security_hashes` VALUES(2418, 1366065423, '127.0.0.1', '71def8f6cecbc42c36b0b859bd31771c8b48efcf');
INSERT INTO `exp_security_hashes` VALUES(2419, 1366065423, '127.0.0.1', '976fbee5cb13144de3e022bdf05d01ea1671cb5b');
INSERT INTO `exp_security_hashes` VALUES(2420, 1366065423, '127.0.0.1', 'bfef2b1e57d2350ad6983042055652ab6557413c');
INSERT INTO `exp_security_hashes` VALUES(2421, 1366065423, '127.0.0.1', '93a43f0cf5f97b240702e3c512fe5742a9de7f87');
INSERT INTO `exp_security_hashes` VALUES(2422, 1366065423, '127.0.0.1', '9ae82c47c4cefe0725d6d7d83a353b642a69e0ee');
INSERT INTO `exp_security_hashes` VALUES(2423, 1366065423, '127.0.0.1', '59ed221b06ee8d55a6caa404cb6211549b5a7e0c');
INSERT INTO `exp_security_hashes` VALUES(2424, 1366065423, '127.0.0.1', '53ccf0299836de65311ec255fc5a19cdd4fe3bf2');
INSERT INTO `exp_security_hashes` VALUES(2425, 1366065423, '127.0.0.1', '75714abc4d370fd782c115745e4880a4d2b07373');
INSERT INTO `exp_security_hashes` VALUES(2426, 1366065424, '127.0.0.1', '5c16adc278b5af4822f206bb0eb32a4bc51c49f1');
INSERT INTO `exp_security_hashes` VALUES(2427, 1366065424, '127.0.0.1', '3368eeda07657a720c06f4a6be439f4852bf2d31');
INSERT INTO `exp_security_hashes` VALUES(2428, 1366065424, '127.0.0.1', '056c055003c189fd91f5ea1b2b81942807c31066');
INSERT INTO `exp_security_hashes` VALUES(2429, 1366065424, '127.0.0.1', '649004e45f1ca0b7b3fd87a82734c767616e73b8');
INSERT INTO `exp_security_hashes` VALUES(2430, 1366065424, '127.0.0.1', '232da19ce3f509c5ed56fa0ae3d3b91f90f70cc1');
INSERT INTO `exp_security_hashes` VALUES(2431, 1366065425, '127.0.0.1', '6d45ece159b0c742716ce66e72b395f4b42dc5fe');
INSERT INTO `exp_security_hashes` VALUES(2432, 1366065425, '127.0.0.1', 'ac6604485540c532a2e081e1f579c0f442b40c8b');
INSERT INTO `exp_security_hashes` VALUES(2433, 1366065425, '127.0.0.1', 'efee688f10a25db1585cbd2bcd7948b7233cdb38');
INSERT INTO `exp_security_hashes` VALUES(2434, 1366065426, '127.0.0.1', '9e85dffea383163a4024ecd1a32e14bf4a844cc3');
INSERT INTO `exp_security_hashes` VALUES(2435, 1366065426, '127.0.0.1', 'e1ba1769f542870dda5cc23bae6bb5c68e0de42e');
INSERT INTO `exp_security_hashes` VALUES(2436, 1366065427, '127.0.0.1', 'f6b41a8a71d6d5499cbf422a878c5c4a3c95571f');
INSERT INTO `exp_security_hashes` VALUES(2437, 1366065427, '127.0.0.1', 'e9625c82cd6bfb50bbc2f21ea46907efe478159b');
INSERT INTO `exp_security_hashes` VALUES(2438, 1366065427, '127.0.0.1', '8e13d6e3eb0d74d0fe09c1cc39be1218716dd19f');
INSERT INTO `exp_security_hashes` VALUES(2439, 1366065427, '127.0.0.1', '179f7b726c9eb1a35087ba3e02a7239ef63d1499');
INSERT INTO `exp_security_hashes` VALUES(2440, 1366065428, '127.0.0.1', '2826cdc39826d16196e49e1cdcc7080d2e3b7437');
INSERT INTO `exp_security_hashes` VALUES(2441, 1366065428, '127.0.0.1', '4c7e3e6492adcf539fbfaba970a7da243d0fad63');
INSERT INTO `exp_security_hashes` VALUES(2442, 1366065428, '127.0.0.1', '61302dfcb0d6bcd35b8a6f1adedad73d69d2eec3');
INSERT INTO `exp_security_hashes` VALUES(2443, 1366065429, '127.0.0.1', '3c56c79831a98a3e0b2c95f8552a515730cc42bb');
INSERT INTO `exp_security_hashes` VALUES(2444, 1366065429, '127.0.0.1', '270c3b365e6ba0f1354f9f9f74aa5b3f88d0cf6e');
INSERT INTO `exp_security_hashes` VALUES(2445, 1366065430, '127.0.0.1', '7ad33fdc91adbdf54da119224be8aebe5fa1c5af');
INSERT INTO `exp_security_hashes` VALUES(2446, 1366065430, '127.0.0.1', 'e1015221ae26b3c225916a2896b187ba81e531fa');
INSERT INTO `exp_security_hashes` VALUES(2447, 1366065430, '127.0.0.1', 'a725295617734ad85041c0a684671ce75d9f4e85');
INSERT INTO `exp_security_hashes` VALUES(2448, 1366065430, '127.0.0.1', 'd950935a163e56f8ce6e07121c89d2f72c1a5a68');
INSERT INTO `exp_security_hashes` VALUES(2449, 1366065431, '127.0.0.1', '4410ae0dca5cbd3966c3b4c7eb8acae07ac03d75');
INSERT INTO `exp_security_hashes` VALUES(2450, 1366065431, '127.0.0.1', '41c9dfe57e93e9fee50cf8a4ac5353ee909cd4f9');
INSERT INTO `exp_security_hashes` VALUES(2451, 1366065431, '127.0.0.1', 'cff55edaac8b1df9a90c227055fbbf543c7e09be');
INSERT INTO `exp_security_hashes` VALUES(2452, 1366065432, '127.0.0.1', '47c24b1251dc1041a6b57ff863cad7e65892bfb5');
INSERT INTO `exp_security_hashes` VALUES(2453, 1366065432, '127.0.0.1', '9694d41a57f2c4bbcbe35ae9e2d8dd6f2594896a');
INSERT INTO `exp_security_hashes` VALUES(2454, 1366065432, '127.0.0.1', 'bbd4d12381601762def4aa5d5e96535a3ced1ba9');
INSERT INTO `exp_security_hashes` VALUES(2455, 1366065432, '127.0.0.1', 'e73c0d68db474c396be5af6fc2aaf163095261f5');
INSERT INTO `exp_security_hashes` VALUES(2456, 1366065433, '127.0.0.1', 'feaa544ada5dc461d00eba4aa761d0e6dd07da51');
INSERT INTO `exp_security_hashes` VALUES(2457, 1366065433, '127.0.0.1', '3ea828d87811bce65272870eee19137a0a6ae3cc');
INSERT INTO `exp_security_hashes` VALUES(2458, 1366065434, '127.0.0.1', '0817de2817c74cd85e39e7ab644e6e393526bfff');
INSERT INTO `exp_security_hashes` VALUES(2459, 1366065434, '127.0.0.1', '84dfc78e22e0c94d95bc8e5c2df0dfc106c61458');
INSERT INTO `exp_security_hashes` VALUES(2460, 1366065435, '127.0.0.1', 'cc471fb6b464cf49993b15f5650e80f38bd68810');
INSERT INTO `exp_security_hashes` VALUES(2461, 1366065435, '127.0.0.1', '7ca2208a0b66c0b4760261239edaa488eabf8672');
INSERT INTO `exp_security_hashes` VALUES(2462, 1366065435, '127.0.0.1', 'b0657beff632dd8aefab3436a975dd7109e55c81');
INSERT INTO `exp_security_hashes` VALUES(2463, 1366065437, '127.0.0.1', '15016b42a3379835ae29f69f360e1542772f97aa');
INSERT INTO `exp_security_hashes` VALUES(2464, 1366065437, '127.0.0.1', '66ae053da1f29dd07d20b43f411497118312218c');
INSERT INTO `exp_security_hashes` VALUES(2465, 1366065438, '127.0.0.1', 'c423ce3611ac9f863ddeefc074dd5dafd55a3f42');
INSERT INTO `exp_security_hashes` VALUES(2466, 1366065440, '127.0.0.1', '3d58af1fbe87a20ce274727dd9f83812cc3785cb');
INSERT INTO `exp_security_hashes` VALUES(2467, 1366065440, '127.0.0.1', '8fcac4cd8c0e26ff69b69079fc42a3915e8cbb33');
INSERT INTO `exp_security_hashes` VALUES(2468, 1366065441, '127.0.0.1', 'fcadaa20252db8cfd12c788c083835ce344269cd');
INSERT INTO `exp_security_hashes` VALUES(2469, 1366065442, '127.0.0.1', '985a9fa96faf1b878d0bbb3194072bce085e356a');
INSERT INTO `exp_security_hashes` VALUES(2470, 1366065442, '127.0.0.1', '230b5f02c6ec07aa7c939acfb6b0f94c22075605');
INSERT INTO `exp_security_hashes` VALUES(2471, 1366065443, '127.0.0.1', 'a670e5793dc2da1b1dd014041c7c790763210cbc');
INSERT INTO `exp_security_hashes` VALUES(2472, 1366065463, '127.0.0.1', '121e90d2e60d13ce0ac9ee9ef3e0566370042ef3');
INSERT INTO `exp_security_hashes` VALUES(2473, 1366065463, '127.0.0.1', '799247e7ebe0a11ed6c49ad6550b19f44d823326');
INSERT INTO `exp_security_hashes` VALUES(2474, 1366065463, '127.0.0.1', '55526c1e7f54d743904da02a74238bf071f06ad7');
INSERT INTO `exp_security_hashes` VALUES(2475, 1366065463, '127.0.0.1', 'fd01e00ecd57ff34e09321228377b6fa426de38b');
INSERT INTO `exp_security_hashes` VALUES(2476, 1366065463, '127.0.0.1', '13f5b2a2471a15266f003b833971678d39f1fbf1');
INSERT INTO `exp_security_hashes` VALUES(2477, 1366065464, '127.0.0.1', 'a26795e91fa4438ae7717094210729030fec46a7');
INSERT INTO `exp_security_hashes` VALUES(2478, 1366065464, '127.0.0.1', 'ada642878c6dc4f0e9ed3e97f8a778f1e1ee43b5');
INSERT INTO `exp_security_hashes` VALUES(2479, 1366065465, '127.0.0.1', '2026bd32a375f50da609215ab1ebbbaff91bff54');
INSERT INTO `exp_security_hashes` VALUES(2480, 1366065465, '127.0.0.1', 'ba69206df41668ba8667f6523560222567ffab11');
INSERT INTO `exp_security_hashes` VALUES(2481, 1366065473, '127.0.0.1', '4a05b8a3114011d8515fe4167529b56cc11d34a4');
INSERT INTO `exp_security_hashes` VALUES(2482, 1366065474, '127.0.0.1', 'b1c61d3b5be3d998af188781436dba01e333a2a4');
INSERT INTO `exp_security_hashes` VALUES(2483, 1366065474, '127.0.0.1', '641020e9be80a39d80aeeb283ac4aafe4c3662b3');
INSERT INTO `exp_security_hashes` VALUES(2484, 1366065477, '127.0.0.1', '45afac8d278a10d95ac6437a4aa6e99d16d62ea0');
INSERT INTO `exp_security_hashes` VALUES(2485, 1366065478, '127.0.0.1', 'f0b4900234a4b61f2565564a7d252ca59e31c091');
INSERT INTO `exp_security_hashes` VALUES(2486, 1366065478, '127.0.0.1', '2f30c42ecfd96c8d1a14a7ab18cf7c4729b25765');
INSERT INTO `exp_security_hashes` VALUES(2487, 1366065479, '127.0.0.1', 'cf60e489e9f9eea8abf2773a54c5a7f17802c287');
INSERT INTO `exp_security_hashes` VALUES(2488, 1366065480, '127.0.0.1', 'c28c090881ff12ee30bc6d2c1cdfc8786771442e');
INSERT INTO `exp_security_hashes` VALUES(2489, 1366065480, '127.0.0.1', '416e809911f962f2abdbe71a43fe0841655eca0e');
INSERT INTO `exp_security_hashes` VALUES(2490, 1366065480, '127.0.0.1', 'f80752dc0490c473875d2a611e320bd5f5e67399');
INSERT INTO `exp_security_hashes` VALUES(2491, 1366065480, '127.0.0.1', '6cbbd2b4c10707aed20ad804f9e09e00fbc2e20e');
INSERT INTO `exp_security_hashes` VALUES(2492, 1366065480, '127.0.0.1', 'aa5943bfa920dd01184dfc0028b4d3468d3176a0');
INSERT INTO `exp_security_hashes` VALUES(2493, 1366065481, '127.0.0.1', 'fe89a3260697a96cdf4a9b5f238278a96d07b58e');
INSERT INTO `exp_security_hashes` VALUES(2494, 1366065481, '127.0.0.1', 'dbca987cee26053d557160a3b3cc7eaa7d49707d');
INSERT INTO `exp_security_hashes` VALUES(2495, 1366065481, '127.0.0.1', '643518e65467414fdc28fceac459504dede9c8cc');
INSERT INTO `exp_security_hashes` VALUES(2496, 1366065491, '127.0.0.1', 'acf405507e201889bea85ca1f35d6045e67d8ce7');
INSERT INTO `exp_security_hashes` VALUES(2497, 1366065492, '127.0.0.1', '89b5a2df3470a8836bb27a9e3b6400e87febf66c');
INSERT INTO `exp_security_hashes` VALUES(2498, 1366065492, '127.0.0.1', '5e9a031a72413725aa3163a303d6a4ecc85b4743');
INSERT INTO `exp_security_hashes` VALUES(2499, 1366065493, '127.0.0.1', '143b08dde0c199259368c8b866d15249cba7aa78');
INSERT INTO `exp_security_hashes` VALUES(2500, 1366065493, '127.0.0.1', 'f015cb745d2f8353495d3df22e866eb965c6c1ca');
INSERT INTO `exp_security_hashes` VALUES(2501, 1366065494, '127.0.0.1', '0a79f819e73868aa5b971d659677d61df079df8d');
INSERT INTO `exp_security_hashes` VALUES(2502, 1366065540, '127.0.0.1', '918f06c1921e8b14675942b6ce0e31896449040e');
INSERT INTO `exp_security_hashes` VALUES(2503, 1366065541, '127.0.0.1', '94f1a743b1f1a64f3c0482cb016606615085a18a');
INSERT INTO `exp_security_hashes` VALUES(2504, 1366065642, '127.0.0.1', '00e6a87d1280208e62f945ac9d874e3924551141');
INSERT INTO `exp_security_hashes` VALUES(2505, 1366065643, '127.0.0.1', '05eea5312a8dfbc3559a6efbfdc5640b7932e8c2');
INSERT INTO `exp_security_hashes` VALUES(2506, 1366065643, '127.0.0.1', '2c51b2113aa2e17c4ac032cd8381af0271ace1ce');
INSERT INTO `exp_security_hashes` VALUES(2507, 1366065646, '127.0.0.1', '2e3f2df4bd0e0179cd484f0a2ca7ac032f3eaf85');
INSERT INTO `exp_security_hashes` VALUES(2508, 1366065647, '127.0.0.1', '3aad10e3ca87337b3ae7d1f20ac04ac8e1720152');
INSERT INTO `exp_security_hashes` VALUES(2509, 1366065712, '127.0.0.1', 'cc4985839c76d9c69795686e8535b3e11505fcea');
INSERT INTO `exp_security_hashes` VALUES(2510, 1366065712, '127.0.0.1', '8bf315bb7a17dd07c9327724db5c91e7b0e861fc');
INSERT INTO `exp_security_hashes` VALUES(2511, 1366065713, '127.0.0.1', '2fa3b23ae83c9eb713dc92ea0f89fc2c7e83a869');
INSERT INTO `exp_security_hashes` VALUES(2512, 1366065713, '127.0.0.1', '1fd6ae1db1d4ccb534f4be4f9931757c7d864d96');
INSERT INTO `exp_security_hashes` VALUES(2513, 1366065713, '127.0.0.1', '526d934e6d53aa93bdb45aa4ecf59e5a90415f2b');
INSERT INTO `exp_security_hashes` VALUES(2514, 1366065713, '127.0.0.1', 'b5e7086ef1e3a0b926461a8a7ad640561540d8ee');
INSERT INTO `exp_security_hashes` VALUES(2515, 1366065752, '127.0.0.1', '17fe76a3ca6668a288abbc7757b90c45097790e3');
INSERT INTO `exp_security_hashes` VALUES(2516, 1366065753, '127.0.0.1', '0ddd9f4f4fbabb904a47240f87b5ba5b7bbc2f7c');
INSERT INTO `exp_security_hashes` VALUES(2517, 1366065753, '127.0.0.1', '5109bd20767e64b0ca86d122e3eaa5b54db17646');
INSERT INTO `exp_security_hashes` VALUES(2518, 1366065797, '127.0.0.1', '0a62d45f7118cd32e88485ec0afebf06e53ac979');
INSERT INTO `exp_security_hashes` VALUES(2519, 1366065797, '127.0.0.1', 'fb127ca442d0e0cfa266db802747e565034cd02b');
INSERT INTO `exp_security_hashes` VALUES(2520, 1366065798, '127.0.0.1', '0da29d8e788a937462eabaf8baa6a10d1708611c');
INSERT INTO `exp_security_hashes` VALUES(2521, 1366065812, '127.0.0.1', '11d771495f4e58aa0a1df0b3a4ebfa4bbb28c0e9');
INSERT INTO `exp_security_hashes` VALUES(2522, 1366065812, '127.0.0.1', '623acb4eb942f5cea33798d5808b1ddbbcf52536');
INSERT INTO `exp_security_hashes` VALUES(2523, 1366065812, '127.0.0.1', 'ad86bea46f7e73f9703c92877dd6592ff45c18cd');
INSERT INTO `exp_security_hashes` VALUES(2524, 1366065819, '127.0.0.1', '12335f7534b6324fe96416e3b679796adf9815e0');
INSERT INTO `exp_security_hashes` VALUES(2525, 1366065878, '127.0.0.1', '79be4e1837000d16872f11593ef59e40fb514a36');
INSERT INTO `exp_security_hashes` VALUES(2526, 1366065998, '127.0.0.1', '3eda3ec008b28ba37f6516311db02bae7cf2f169');
INSERT INTO `exp_security_hashes` VALUES(2527, 1366065999, '127.0.0.1', 'c75d3fdb2e8411f245ba25dbd00a661ba25ca8f4');
INSERT INTO `exp_security_hashes` VALUES(2528, 1366065999, '127.0.0.1', 'aa594c3fb7606edaafcdf9e8aa8c57c188ae8d61');
INSERT INTO `exp_security_hashes` VALUES(2529, 1366066076, '127.0.0.1', 'e6cc16afb47dd5126e858f5a0b841b503a59ca43');
INSERT INTO `exp_security_hashes` VALUES(2530, 1366066077, '127.0.0.1', '0c2b25a50e071183defe8b72e5f75a9f05dcb999');
INSERT INTO `exp_security_hashes` VALUES(2531, 1366066077, '127.0.0.1', '333f1f002f2566b4a4762e4ae2032d6e8f4f4080');
INSERT INTO `exp_security_hashes` VALUES(2532, 1366066079, '127.0.0.1', '14e8c2f278340c6b52a75879dbb2db5800a42f40');
INSERT INTO `exp_security_hashes` VALUES(2533, 1366066080, '127.0.0.1', '75f309bd92d275493b8a42171fb8ddbaa01b4b58');
INSERT INTO `exp_security_hashes` VALUES(2534, 1366066080, '127.0.0.1', '89a7c9274f28b68d58789b498429f371aa690186');
INSERT INTO `exp_security_hashes` VALUES(2535, 1366066090, '127.0.0.1', '163aa0e388fb51645027f0422863468e4c4edff4');
INSERT INTO `exp_security_hashes` VALUES(2536, 1366066090, '127.0.0.1', '05a9c098096db0af58ed681f096a04c0555a27d0');
INSERT INTO `exp_security_hashes` VALUES(2537, 1366066091, '127.0.0.1', 'cd0d588d9b147749e798e53fdf1277a6397d23ad');
INSERT INTO `exp_security_hashes` VALUES(2538, 1366066098, '127.0.0.1', '654f423bc7d6aba0b44e63bf5e9daff53278cd43');
INSERT INTO `exp_security_hashes` VALUES(2539, 1366066098, '127.0.0.1', 'ff1d0b3aabba33a24f5fac47944ba3e68edc3977');
INSERT INTO `exp_security_hashes` VALUES(2540, 1366066099, '127.0.0.1', '7a4f7db9854218836e5ff4856f42331f0e3be752');
INSERT INTO `exp_security_hashes` VALUES(2541, 1366066119, '127.0.0.1', '3e7aa94a07499a9f4e4c9b86afcd88f5c0af9ff9');
INSERT INTO `exp_security_hashes` VALUES(2542, 1366066119, '127.0.0.1', '2e43c8c59d60f895946ae3a9d0728b4138460575');
INSERT INTO `exp_security_hashes` VALUES(2543, 1366066120, '127.0.0.1', '27a4b8b22d6e38ace65cc57a1882f87d874a2d7c');
INSERT INTO `exp_security_hashes` VALUES(2544, 1366066120, '127.0.0.1', '80a18a3f2eb087fa33eb61ce9f24c62c25364c5c');
INSERT INTO `exp_security_hashes` VALUES(2545, 1366066155, '127.0.0.1', 'a6b5b51f9d275dffd6ad193dd799155bb880996f');
INSERT INTO `exp_security_hashes` VALUES(2546, 1366066156, '127.0.0.1', '40c410f7366139e1d5e98b39d989492d5ac8c330');
INSERT INTO `exp_security_hashes` VALUES(2547, 1366066156, '127.0.0.1', '0fc5173e42e25115672d7c5a9901525763052180');
INSERT INTO `exp_security_hashes` VALUES(2548, 1366066173, '127.0.0.1', '92fad6bea5a6888aa50232285bd05810e6694df2');
INSERT INTO `exp_security_hashes` VALUES(2549, 1366066174, '127.0.0.1', '71be15b60ebe89a4e9781d5c3576d9f44fea1a84');
INSERT INTO `exp_security_hashes` VALUES(2550, 1366066174, '127.0.0.1', '0279ba68f4d5e4a42b7f1fbfc58cabc2e4c3c5e8');
INSERT INTO `exp_security_hashes` VALUES(2551, 1366066176, '127.0.0.1', '11ffaa4c11d569d1ee4bf457f6800d44e2bff901');
INSERT INTO `exp_security_hashes` VALUES(2552, 1366066351, '127.0.0.1', '0ad715e9b5637c3a52906fe3b40e2499d3e6d443');
INSERT INTO `exp_security_hashes` VALUES(2553, 1366066352, '127.0.0.1', '372515c29497a8cde028eb6b0e290b06db399dd6');
INSERT INTO `exp_security_hashes` VALUES(2554, 1366066353, '127.0.0.1', 'f24cf9f9c9f45c07a7b6d9577ff12895d3ce151d');
INSERT INTO `exp_security_hashes` VALUES(2555, 1366066353, '127.0.0.1', 'c6822f62130c21a56a3deecb9487f3c15b95182b');
INSERT INTO `exp_security_hashes` VALUES(2556, 1366066355, '127.0.0.1', '7012e9e6aa709eff8d145e7ca15943338ddd4c86');
INSERT INTO `exp_security_hashes` VALUES(2557, 1366066357, '127.0.0.1', 'fcdd08b1877857ed725b8156d5bb6723e25db71d');
INSERT INTO `exp_security_hashes` VALUES(2558, 1366066357, '127.0.0.1', 'b77b4019a9dddb2eb63a3a87b4976cbd575eebee');
INSERT INTO `exp_security_hashes` VALUES(2559, 1366066358, '127.0.0.1', '48d2591c429863894f6c384890bea19244c2a79c');
INSERT INTO `exp_security_hashes` VALUES(2560, 1366066358, '127.0.0.1', '5c47c4f7243fb68fe92c8646838ce99bf0d08bf2');
INSERT INTO `exp_security_hashes` VALUES(2561, 1366066365, '127.0.0.1', '71deec4d1e7f211fc22bf1f6ae04523883b9b9d8');
INSERT INTO `exp_security_hashes` VALUES(2562, 1366066365, '127.0.0.1', '770fdad3d572438626271018279d18d6b516e6a1');
INSERT INTO `exp_security_hashes` VALUES(2563, 1366066373, '127.0.0.1', '04ea71e75964fa98a98a753c36a1ebd3261acb98');
INSERT INTO `exp_security_hashes` VALUES(2564, 1366066373, '127.0.0.1', 'c6d7d75695986ed67f7a8aab82bb7e43627f5691');
INSERT INTO `exp_security_hashes` VALUES(2565, 1366066403, '127.0.0.1', 'fb54b1105a4b3cc2a469862804430dd6e9b05472');
INSERT INTO `exp_security_hashes` VALUES(2566, 1366066403, '127.0.0.1', '0072cbd273ee5b707834fcb497bf221900bb60ef');
INSERT INTO `exp_security_hashes` VALUES(2567, 1366066404, '127.0.0.1', '53ff43af55a9d16872c8657936c589d57b26ce01');
INSERT INTO `exp_security_hashes` VALUES(2568, 1366066408, '127.0.0.1', '47462d5d2a7dec2236637aba591a950ce044a24a');
INSERT INTO `exp_security_hashes` VALUES(2569, 1366066409, '127.0.0.1', '33ad87728e877602e5dd0051b4bf04150b43d4d1');
INSERT INTO `exp_security_hashes` VALUES(2570, 1366066415, '127.0.0.1', 'd386207d96fb509c35ae77b1f57cd077ec0ae2b9');
INSERT INTO `exp_security_hashes` VALUES(2571, 1366066416, '127.0.0.1', 'a18ba0bb2a2a192ca8d4b8f21c55fa01130e51d5');
INSERT INTO `exp_security_hashes` VALUES(2572, 1366066416, '127.0.0.1', 'c991669061d44e1a1df78fa607eba15c106b6b7e');
INSERT INTO `exp_security_hashes` VALUES(2573, 1366066416, '127.0.0.1', '88e8f1836a59aec106f8b712b28974377a6e2ab9');
INSERT INTO `exp_security_hashes` VALUES(2574, 1366066416, '127.0.0.1', 'bb8f27d3dcff1b633940ca7f731328ef4131ede9');
INSERT INTO `exp_security_hashes` VALUES(2575, 1366066416, '127.0.0.1', 'ba2d79c5c9208721356b6ddede868bdf8c007468');
INSERT INTO `exp_security_hashes` VALUES(2576, 1366066416, '127.0.0.1', '29e6a3a2cc3f095f7fcc5d7ca1bf40f4100388ca');
INSERT INTO `exp_security_hashes` VALUES(2577, 1366066416, '127.0.0.1', '5cd8b2ca1f99fbcc0db15f23d5067513dcd9b17d');
INSERT INTO `exp_security_hashes` VALUES(2578, 1366066417, '127.0.0.1', 'cd87f29bcc4a2d95ade71b28ac0fb7eeef41b909');
INSERT INTO `exp_security_hashes` VALUES(2579, 1366066417, '127.0.0.1', '592cf4f1a578c0ca29a95d78fce888298b7218e7');
INSERT INTO `exp_security_hashes` VALUES(2580, 1366066417, '127.0.0.1', 'bb271e8481579d67930980d6d4b0664e7f75c711');
INSERT INTO `exp_security_hashes` VALUES(2581, 1366066417, '127.0.0.1', 'ce39161735b55a71239fbb4f5eaf07983664af8f');
INSERT INTO `exp_security_hashes` VALUES(2582, 1366066417, '127.0.0.1', 'dcc43e507e24afb305482cf42723f36d21e6323f');
INSERT INTO `exp_security_hashes` VALUES(2583, 1366066417, '127.0.0.1', 'cea088b0559a028caea85b8da9516cfbb8242936');
INSERT INTO `exp_security_hashes` VALUES(2584, 1366066417, '127.0.0.1', 'c6ad8408882ec38863a99c02859b52739b13f164');
INSERT INTO `exp_security_hashes` VALUES(2585, 1366066417, '127.0.0.1', '5dab13c411413ee6a30dbbce73ccca139ae72c40');
INSERT INTO `exp_security_hashes` VALUES(2586, 1366066418, '127.0.0.1', '18ddb0c859a4570ed8c3ab3c9fab9bff7fc6bd13');
INSERT INTO `exp_security_hashes` VALUES(2587, 1366066418, '127.0.0.1', 'a5680a3c633a71bbfecac4049963f5a7507cc0fe');
INSERT INTO `exp_security_hashes` VALUES(2588, 1366066418, '127.0.0.1', '1e434c0b56d8095c17d8fc3aac9f3ee587fea711');
INSERT INTO `exp_security_hashes` VALUES(2589, 1366066418, '127.0.0.1', '76c899dec9f9df71ff3c6470ad0e91059a6ab089');
INSERT INTO `exp_security_hashes` VALUES(2590, 1366066418, '127.0.0.1', 'c0eb0b74d1e356d4dc06534e6c9bb406081e8233');
INSERT INTO `exp_security_hashes` VALUES(2591, 1366066418, '127.0.0.1', 'e779e94c4444eca99730155f03f55bd557ed0330');
INSERT INTO `exp_security_hashes` VALUES(2592, 1366066418, '127.0.0.1', '0b6f90a4a00db80ac6bef4d170e19dbb37b9bd94');
INSERT INTO `exp_security_hashes` VALUES(2593, 1366066418, '127.0.0.1', '73a7a37732f5265d88fa14647fe748e36a304e70');
INSERT INTO `exp_security_hashes` VALUES(2594, 1366066418, '127.0.0.1', 'a9d46661500d8c2b731e9e773bf5e33db4e4d444');
INSERT INTO `exp_security_hashes` VALUES(2595, 1366066418, '127.0.0.1', 'fa6a2c02d3dd0b75046332d559bb09add761be26');
INSERT INTO `exp_security_hashes` VALUES(2596, 1366066418, '127.0.0.1', 'ac4e8298363941402b9a90da2789cae7ab01ea84');
INSERT INTO `exp_security_hashes` VALUES(2597, 1366066419, '127.0.0.1', '5edef31ceac59509b05e6862001274c0dd194b54');
INSERT INTO `exp_security_hashes` VALUES(2598, 1366066419, '127.0.0.1', '780e00e8ea1a96777b8dc94b4471391aa0e0e04d');
INSERT INTO `exp_security_hashes` VALUES(2599, 1366066419, '127.0.0.1', '3f25b13f1fb8abefc25a6d074b934d999b596378');
INSERT INTO `exp_security_hashes` VALUES(2600, 1366066419, '127.0.0.1', '4346a942bfbde6001490929951f0c6a1a18c8bda');
INSERT INTO `exp_security_hashes` VALUES(2601, 1366066419, '127.0.0.1', 'd879ffd85aa5874e75fdb829598a47a584554e32');
INSERT INTO `exp_security_hashes` VALUES(2602, 1366066419, '127.0.0.1', '0114467d9f49b14ae5a951c175bd92834f3cd97f');
INSERT INTO `exp_security_hashes` VALUES(2603, 1366066419, '127.0.0.1', '859b4b9aa79c1d6506811ff20ffb5292f4439d70');
INSERT INTO `exp_security_hashes` VALUES(2604, 1366066419, '127.0.0.1', 'bb831b783571b673269dce744f3d4eb701c25488');
INSERT INTO `exp_security_hashes` VALUES(2605, 1366066419, '127.0.0.1', 'cc36e7a5059f98a5e93c33a9b87f8f0a130473eb');
INSERT INTO `exp_security_hashes` VALUES(2606, 1366066419, '127.0.0.1', '43a872fd68d26c3bcc0351ce2815fd8cbf7553c8');
INSERT INTO `exp_security_hashes` VALUES(2607, 1366066420, '127.0.0.1', '58c34368573c537404af640194f3e06e816a43f8');
INSERT INTO `exp_security_hashes` VALUES(2608, 1366066420, '127.0.0.1', '35e707960b31c66b2e5f515c96e3c747792f12b2');
INSERT INTO `exp_security_hashes` VALUES(2609, 1366066420, '127.0.0.1', '2012125d319bd8dcaad062f79056af78fee420e4');
INSERT INTO `exp_security_hashes` VALUES(2610, 1366066420, '127.0.0.1', '19a16b4cc864a8f6035fde1e05eb0fd57d9e0e9e');
INSERT INTO `exp_security_hashes` VALUES(2611, 1366066420, '127.0.0.1', '7c80faa4fe41e939a940bd0b9562069d13ecc23b');
INSERT INTO `exp_security_hashes` VALUES(2612, 1366066420, '127.0.0.1', '7e9e180e7275f18e4256a5f35685b4aeff0a4d05');
INSERT INTO `exp_security_hashes` VALUES(2613, 1366066421, '127.0.0.1', '5324012213d7cb3963b7ccf856a8e264b81a00b6');
INSERT INTO `exp_security_hashes` VALUES(2614, 1366066421, '127.0.0.1', '6f005a4808ae38f92e112b474ed694288f684d0f');
INSERT INTO `exp_security_hashes` VALUES(2615, 1366066422, '127.0.0.1', 'c712f3b8e5142a2b738b62a0d8890879010f6185');
INSERT INTO `exp_security_hashes` VALUES(2616, 1366066422, '127.0.0.1', '86716dd63ab7263cb4d8d9409fbd9d1685647053');
INSERT INTO `exp_security_hashes` VALUES(2617, 1366066422, '127.0.0.1', 'd9eb52ed77adf4f2de22d947f5643f747253010c');
INSERT INTO `exp_security_hashes` VALUES(2618, 1366066422, '127.0.0.1', 'aa3af15f9eb0212129781b9bc4fb83feb241cf29');
INSERT INTO `exp_security_hashes` VALUES(2619, 1366066423, '127.0.0.1', '4c449257b1eb721394063b1fef9a180faed4af06');
INSERT INTO `exp_security_hashes` VALUES(2620, 1366066423, '127.0.0.1', '25a62ddf853938273e61bc0add9c14664295bdd4');
INSERT INTO `exp_security_hashes` VALUES(2621, 1366066423, '127.0.0.1', '09ce979265a1aa1f35fcf64620f00fa753055a36');
INSERT INTO `exp_security_hashes` VALUES(2622, 1366066424, '127.0.0.1', 'c22275c72fd3db290c38ae812c8162678633ea3c');
INSERT INTO `exp_security_hashes` VALUES(2623, 1366066424, '127.0.0.1', '9b7076d878a6802c6797e9752a88a9188f51d81b');
INSERT INTO `exp_security_hashes` VALUES(2624, 1366066425, '127.0.0.1', '1c6c5d7d816c8bc85ff30358bae96d8d65cc7249');
INSERT INTO `exp_security_hashes` VALUES(2625, 1366066425, '127.0.0.1', '9dea49ac56a32bcc61e808cc508be08678c476a5');
INSERT INTO `exp_security_hashes` VALUES(2626, 1366066425, '127.0.0.1', '4c0243efa7272ce3d7444b02b5bfb522733b3f6e');
INSERT INTO `exp_security_hashes` VALUES(2627, 1366066425, '127.0.0.1', '8923a2d21b82b1215e41fe353c54e7d74b1e9d9e');
INSERT INTO `exp_security_hashes` VALUES(2628, 1366066426, '127.0.0.1', 'b2e9d65d31f8ca1371f96d8d09af2d6f72bc2493');
INSERT INTO `exp_security_hashes` VALUES(2629, 1366066426, '127.0.0.1', '8a3fafd29edd848ff1127d8fd44f5b4bca90ffa9');
INSERT INTO `exp_security_hashes` VALUES(2630, 1366066426, '127.0.0.1', '561b9c89b01056331c4b49553006135d2e48b99f');
INSERT INTO `exp_security_hashes` VALUES(2631, 1366066427, '127.0.0.1', '3594d33f5c3005ddd4f309f0948ae42a778c9e29');
INSERT INTO `exp_security_hashes` VALUES(2632, 1366066427, '127.0.0.1', '5f26edcdc490a5d92a39e7ff1f99fb982218bf96');
INSERT INTO `exp_security_hashes` VALUES(2633, 1366066428, '127.0.0.1', 'c092e0d5a34e962fa67606462f1631463fc62147');
INSERT INTO `exp_security_hashes` VALUES(2634, 1366066428, '127.0.0.1', '8209db3e6c13af302464d797926c2fc14f6c4044');
INSERT INTO `exp_security_hashes` VALUES(2635, 1366066428, '127.0.0.1', 'f4c47687a87ac07dbeaa4992979584304729d267');
INSERT INTO `exp_security_hashes` VALUES(2636, 1366066428, '127.0.0.1', '01c0dc20c24c911542fead734c1e1ccb76a2dba0');
INSERT INTO `exp_security_hashes` VALUES(2637, 1366066429, '127.0.0.1', 'd2b26a1dba5b570c88ee953c95f1dac5bf715c99');
INSERT INTO `exp_security_hashes` VALUES(2638, 1366066429, '127.0.0.1', '3af7e5992e8a1d80b4ba4683ce8dfe609d992719');
INSERT INTO `exp_security_hashes` VALUES(2639, 1366066429, '127.0.0.1', 'e5dc748fbc5853e4c1a35739a80a426d784bdae7');
INSERT INTO `exp_security_hashes` VALUES(2640, 1366066440, '127.0.0.1', '99ad353663a27f2e7e5f9fa6ccc8e17ec530da72');
INSERT INTO `exp_security_hashes` VALUES(2641, 1366066440, '127.0.0.1', '289e8c99c67d67cf4fec5a99ba4b67fe65e27b1f');
INSERT INTO `exp_security_hashes` VALUES(2642, 1366066440, '127.0.0.1', '44989ce240fa4d189342a12c4b08f8dec51bbd62');
INSERT INTO `exp_security_hashes` VALUES(2643, 1366066440, '127.0.0.1', '9fec841f1d9ab074006c7768062798c18c26d7dc');
INSERT INTO `exp_security_hashes` VALUES(2644, 1366066441, '127.0.0.1', '5ac503eb12d3f04f85c9aebc085d99aa7ac1e471');
INSERT INTO `exp_security_hashes` VALUES(2645, 1366066441, '127.0.0.1', 'ec264649fa8bc3110d6b880d6630ed95e7267280');
INSERT INTO `exp_security_hashes` VALUES(2646, 1366066442, '127.0.0.1', '0f9bfd798c14d0588155d529c6ea173d68837a3b');
INSERT INTO `exp_security_hashes` VALUES(2647, 1366066442, '127.0.0.1', '79800e295835340d2115191f2abcdcc9c54e721b');
INSERT INTO `exp_security_hashes` VALUES(2648, 1366066443, '127.0.0.1', 'd6f5c795f3f7091c6401874a244d7c5bf6a16718');
INSERT INTO `exp_security_hashes` VALUES(2649, 1366066444, '127.0.0.1', 'd9675bc61b09cbb730d831d80cf7be7438c913e7');
INSERT INTO `exp_security_hashes` VALUES(2650, 1366066444, '127.0.0.1', 'e0e9f738a5a903b5bc363eac6e39c3c3ba741cf7');
INSERT INTO `exp_security_hashes` VALUES(2651, 1366066445, '127.0.0.1', 'a908647265241eea3aaacee7f102d16d87d92f12');
INSERT INTO `exp_security_hashes` VALUES(2652, 1366066453, '127.0.0.1', '0e68ab17f5d752ff9512fe2e6daeffe5ac108621');
INSERT INTO `exp_security_hashes` VALUES(2653, 1366066453, '127.0.0.1', '44cb11a3410c43d6e6df9a63e983c816fa1a2451');
INSERT INTO `exp_security_hashes` VALUES(2654, 1366066454, '127.0.0.1', '08b292aeaad58e04122abcc16a9a87ad14e261fc');
INSERT INTO `exp_security_hashes` VALUES(2655, 1366066454, '127.0.0.1', 'f524f77a9220fbf1d02af869f605d85326df228d');
INSERT INTO `exp_security_hashes` VALUES(2656, 1366066454, '127.0.0.1', '41c785615dc6f83bc44ecf8f325952e830392786');
INSERT INTO `exp_security_hashes` VALUES(2657, 1366066454, '127.0.0.1', 'ae90202e311069677258af054d8319739ffbd929');
INSERT INTO `exp_security_hashes` VALUES(2658, 1366066454, '127.0.0.1', '9b8e39dea77acd1c97279f03e97de533900ccb46');
INSERT INTO `exp_security_hashes` VALUES(2659, 1366066455, '127.0.0.1', 'dd78e4e27c9db83401c40258f1767f89b6231efe');
INSERT INTO `exp_security_hashes` VALUES(2660, 1366066455, '127.0.0.1', '2ef648888a24b66798257305a4866fc45967218f');
INSERT INTO `exp_security_hashes` VALUES(2661, 1366066466, '127.0.0.1', '0ed7b165b9c13a64ef996cd2a129cecd56792f29');
INSERT INTO `exp_security_hashes` VALUES(2662, 1366066467, '127.0.0.1', 'd83d2718ee337ac464ee1f6393d29c68f249ecbb');
INSERT INTO `exp_security_hashes` VALUES(2663, 1366066467, '127.0.0.1', '7fc9c21d07d3fa551f4aa8e04175dd5746229572');
INSERT INTO `exp_security_hashes` VALUES(2664, 1366066471, '127.0.0.1', '29352cebaa85a32d0a0c9a1ac144eeda28c2993a');
INSERT INTO `exp_security_hashes` VALUES(2665, 1366066472, '127.0.0.1', '3f6023e908091569f29768ee11049516dac118ba');
INSERT INTO `exp_security_hashes` VALUES(2666, 1366066472, '127.0.0.1', '284357640ac7435a3eb90e493b08f8cac0a0626e');
INSERT INTO `exp_security_hashes` VALUES(2667, 1366066474, '127.0.0.1', '19a62955366499f5f2fb1e9fbe94d6c064699c28');
INSERT INTO `exp_security_hashes` VALUES(2668, 1366066474, '127.0.0.1', 'db9a071b02ee50e47efa55749a1a39d3fdee457d');
INSERT INTO `exp_security_hashes` VALUES(2669, 1366066474, '127.0.0.1', '9524d9248da294926c25cc3766323b0d607feaba');
INSERT INTO `exp_security_hashes` VALUES(2670, 1366066474, '127.0.0.1', '109d01edcd573f083def568b0fa6268bcedf60e8');
INSERT INTO `exp_security_hashes` VALUES(2671, 1366066474, '127.0.0.1', 'b1985363be90ae24724184ba1bb563f3ac040366');
INSERT INTO `exp_security_hashes` VALUES(2672, 1366066475, '127.0.0.1', 'a715c891324232bcce24f50d6a0ca79e56248632');
INSERT INTO `exp_security_hashes` VALUES(2673, 1366066475, '127.0.0.1', '406483c739187b8e529c0ee1d6c40b729bb759ab');
INSERT INTO `exp_security_hashes` VALUES(2674, 1366066475, '127.0.0.1', '34791e6ab8d4f29fef20cc5ef45c6acaa1835eb4');
INSERT INTO `exp_security_hashes` VALUES(2675, 1366066476, '127.0.0.1', 'f2fd42e52b793cc227bcfda9ea3e3c930022ba80');
INSERT INTO `exp_security_hashes` VALUES(2676, 1366066484, '127.0.0.1', '26e4aca667bfa1fbc40e13431cdb947043558217');
INSERT INTO `exp_security_hashes` VALUES(2677, 1366066485, '127.0.0.1', '08fe7a99cd9d64a6b4c528af2a95721e8946cc50');
INSERT INTO `exp_security_hashes` VALUES(2678, 1366066485, '127.0.0.1', '5a8846c64ab16b40bd1095cddd3099a6aedd14b7');
INSERT INTO `exp_security_hashes` VALUES(2679, 1366066489, '127.0.0.1', '6bf55a374ba10df57a719024e496a40a70e965c7');
INSERT INTO `exp_security_hashes` VALUES(2680, 1366066489, '127.0.0.1', '847fd263c833a7ccaf1c56f6dbee68f87765b26d');
INSERT INTO `exp_security_hashes` VALUES(2681, 1366066490, '127.0.0.1', '70c5f2e2fda11cbc7d249456f441fff9957efb26');
INSERT INTO `exp_security_hashes` VALUES(2682, 1366066500, '127.0.0.1', 'bf4916d0de8de6522ab0f4afffc3d2078f2a022e');
INSERT INTO `exp_security_hashes` VALUES(2683, 1366066500, '127.0.0.1', '399260f71f6e9939131fdc30c1b45689cc06d13b');
INSERT INTO `exp_security_hashes` VALUES(2684, 1366066500, '127.0.0.1', 'e952ec87ed5fd132838e75434cb6b72e2ae2bfd8');
INSERT INTO `exp_security_hashes` VALUES(2685, 1366066500, '127.0.0.1', '8c7ea061586ba158626b3d2e687b3d65a62e0a12');
INSERT INTO `exp_security_hashes` VALUES(2686, 1366066501, '127.0.0.1', 'b06981469102e0aff7277a0604eb04dbfb542002');
INSERT INTO `exp_security_hashes` VALUES(2687, 1366066501, '127.0.0.1', '0f0b3a65ce71529625a5f92e81f0ab0321fa623d');
INSERT INTO `exp_security_hashes` VALUES(2688, 1366066535, '127.0.0.1', '531d408044bf5f3de46ad226261222950fbaeeb5');
INSERT INTO `exp_security_hashes` VALUES(2689, 1366066535, '127.0.0.1', 'c0d8bb51fbf57dd0dbc4f1a795c1a5a9741e9b2a');
INSERT INTO `exp_security_hashes` VALUES(2690, 1366066538, '127.0.0.1', 'b82f92cc87afd1d7c6f0e3b9e3d92750e2fef4f2');
INSERT INTO `exp_security_hashes` VALUES(2691, 1366066541, '127.0.0.1', '478b211522bba79496b4cbe33003ddc06f0ee75c');
INSERT INTO `exp_security_hashes` VALUES(2692, 1366066542, '127.0.0.1', 'a251b4c1c2bf26cd5d8732501d7d7b3f8f1ab76e');
INSERT INTO `exp_security_hashes` VALUES(2693, 1366066544, '127.0.0.1', 'd5de94adc55e6b6b6e4dc368921a2c798e3e6dce');
INSERT INTO `exp_security_hashes` VALUES(2694, 1366066545, '127.0.0.1', '8d8078407c8e7cefe0b0695331de5997d9435365');
INSERT INTO `exp_security_hashes` VALUES(2695, 1366066545, '127.0.0.1', 'e15497b6024545e0e34df46cd6c88d68935e15da');
INSERT INTO `exp_security_hashes` VALUES(2696, 1366066545, '127.0.0.1', 'e757093b8bf3d03410f1483b0e0410aae3acb5e1');
INSERT INTO `exp_security_hashes` VALUES(2697, 1366066545, '127.0.0.1', '3cbd64e48b00a4deebd9e79c0acf087b08ce5c47');
INSERT INTO `exp_security_hashes` VALUES(2698, 1366066546, '127.0.0.1', '9d1ea58699327697767508255db92190ad22d805');
INSERT INTO `exp_security_hashes` VALUES(2699, 1366066589, '127.0.0.1', '13e3ea22a7b5bd2c697baf0c3ad0cf2163e4b01b');
INSERT INTO `exp_security_hashes` VALUES(2700, 1366066589, '127.0.0.1', '7410cd1f76d6b045c346ea8c11c4663514979387');
INSERT INTO `exp_security_hashes` VALUES(2701, 1366066591, '127.0.0.1', 'c170850335f165c76467ae791501de2e6c86b5f7');
INSERT INTO `exp_security_hashes` VALUES(2702, 1366066592, '127.0.0.1', '3aba57c96b60c6a7859707c63e39b6ce1e2fdce2');
INSERT INTO `exp_security_hashes` VALUES(2703, 1366066592, '127.0.0.1', '0e4d72dcba5a52792fe43869a956ee241e4db3f3');
INSERT INTO `exp_security_hashes` VALUES(2704, 1366066593, '127.0.0.1', '05a0c7b47bb6a1e9e10c16b059545ea3bca6c984');
INSERT INTO `exp_security_hashes` VALUES(2705, 1366066620, '127.0.0.1', '346d8247e1f0a7edee5eec790f2ca27d2895eedb');
INSERT INTO `exp_security_hashes` VALUES(2706, 1366066621, '127.0.0.1', '018b7ae7ca034d2e6b602c92c8871e9c656ae4d7');
INSERT INTO `exp_security_hashes` VALUES(2707, 1366066621, '127.0.0.1', '9012fde7eceae8f67282187d9ce7afbe00179305');
INSERT INTO `exp_security_hashes` VALUES(2708, 1366066626, '127.0.0.1', 'c920208c0af525923ba50de5aebf51d354e067e0');
INSERT INTO `exp_security_hashes` VALUES(2709, 1366066626, '127.0.0.1', '6f9a585d93285aa3aa8a4cb35b82f20832e51e5d');
INSERT INTO `exp_security_hashes` VALUES(2710, 1366066628, '127.0.0.1', '2dfebf4bac01b6591d40e3af94d484d9293c66b0');
INSERT INTO `exp_security_hashes` VALUES(2711, 1366066629, '127.0.0.1', 'd700c8c948487845b7601de34d71613895fdcd9c');
INSERT INTO `exp_security_hashes` VALUES(2712, 1366066629, '127.0.0.1', 'a9ed5ce2c098e0cbd55f5492931ea64632f63326');
INSERT INTO `exp_security_hashes` VALUES(2713, 1366066629, '127.0.0.1', '8cc8b127b0a1ff4add3a8d1333dddd5624a332f0');
INSERT INTO `exp_security_hashes` VALUES(2714, 1366066629, '127.0.0.1', 'fd87bfe33e19dddb1c8338a4324c1187e56e12a4');
INSERT INTO `exp_security_hashes` VALUES(2715, 1366066629, '127.0.0.1', '0e277e7cc758f82bb5a906463b8b91da08329fad');
INSERT INTO `exp_security_hashes` VALUES(2716, 1366066629, '127.0.0.1', '9542f3c8b78080d1550631fa79ad61e5662cb4e2');
INSERT INTO `exp_security_hashes` VALUES(2717, 1366066629, '127.0.0.1', '0a7ebb3d5821c206a5c62b97707e4682d04207e7');
INSERT INTO `exp_security_hashes` VALUES(2718, 1366066630, '127.0.0.1', '6eef6a58b46a7bb0ca124881e829080a13182c07');
INSERT INTO `exp_security_hashes` VALUES(2719, 1366066630, '127.0.0.1', 'f409edf12c8a2b3e039a6cd1cc3ba912ac6a1a14');
INSERT INTO `exp_security_hashes` VALUES(2720, 1366066630, '127.0.0.1', '429090073c46dcb40d1dde9fc75ab1e6ba605663');
INSERT INTO `exp_security_hashes` VALUES(2721, 1366066630, '127.0.0.1', 'ddaf1065f131e308a4f10e11cd9b69f571d5d287');
INSERT INTO `exp_security_hashes` VALUES(2722, 1366066630, '127.0.0.1', 'eaa0f828875b2131608ef264bd8f626cd5054b90');
INSERT INTO `exp_security_hashes` VALUES(2723, 1366066631, '127.0.0.1', '74bd821c806ba8d465d3b33f455943b766212a0e');
INSERT INTO `exp_security_hashes` VALUES(2724, 1366066631, '127.0.0.1', 'afd6d81a30410a61c1efae6a3ce861de9d5f74ea');
INSERT INTO `exp_security_hashes` VALUES(2725, 1366066631, '127.0.0.1', 'c5613c91ec6e657f78e2c5533b34a1da3507818d');
INSERT INTO `exp_security_hashes` VALUES(2726, 1366066632, '127.0.0.1', 'efc74896ae03675190f845beda28431c6001d898');
INSERT INTO `exp_security_hashes` VALUES(2727, 1366066632, '127.0.0.1', '8ca2929409e8ac10b112b70c237dbac155669b4c');
INSERT INTO `exp_security_hashes` VALUES(2728, 1366066632, '127.0.0.1', 'c6f4af55950b529a95377f25f1aa7cebe05fd814');
INSERT INTO `exp_security_hashes` VALUES(2729, 1366066632, '127.0.0.1', '4c4c444e5051da0aaa798aff54255284f49e0c72');
INSERT INTO `exp_security_hashes` VALUES(2730, 1366066632, '127.0.0.1', '6490004964dd9a1e26b2920adb47d05d29983869');
INSERT INTO `exp_security_hashes` VALUES(2731, 1366066632, '127.0.0.1', 'c74c0a843e4411b94d2a4073443539d65d6fc908');
INSERT INTO `exp_security_hashes` VALUES(2732, 1366066633, '127.0.0.1', 'ab061d02260f051e9879e5ba9cba894dd36adf03');
INSERT INTO `exp_security_hashes` VALUES(2733, 1366066633, '127.0.0.1', 'a4ecaf3cdbcfc71100eca768d8d64f4ca29fb295');
INSERT INTO `exp_security_hashes` VALUES(2734, 1366066633, '127.0.0.1', '68e3c149b8d61db5c7e559426dc402755025c851');
INSERT INTO `exp_security_hashes` VALUES(2735, 1366066633, '127.0.0.1', '9bd04cdafb62b641ac3c489debf12ab3b175b949');
INSERT INTO `exp_security_hashes` VALUES(2736, 1366066633, '127.0.0.1', 'e0a206cc2068959e92c4895ee3fd29d9c525561e');
INSERT INTO `exp_security_hashes` VALUES(2737, 1366066633, '127.0.0.1', '668a47a1ce75e2264c35a3a975be4796d3f3e295');
INSERT INTO `exp_security_hashes` VALUES(2738, 1366066633, '127.0.0.1', '1e362a60c7d75d2587224905e990296247cabd70');
INSERT INTO `exp_security_hashes` VALUES(2739, 1366066633, '127.0.0.1', 'f558a7106c59f40f2cd344ca5b0d132738bcf8ae');
INSERT INTO `exp_security_hashes` VALUES(2740, 1366066634, '127.0.0.1', 'e88229ad10a960e5181cf173dd114a0380dad13a');
INSERT INTO `exp_security_hashes` VALUES(2741, 1366066634, '127.0.0.1', '3c98193f52df2c8909c1e66bee2e1aa26b5ec831');
INSERT INTO `exp_security_hashes` VALUES(2742, 1366066635, '127.0.0.1', 'a2b6a3202ffacec29ac71aff18cbbd49870130af');
INSERT INTO `exp_security_hashes` VALUES(2743, 1366066635, '127.0.0.1', 'ecd8c51f81374f295ad642156cd2afe562ab8dbe');
INSERT INTO `exp_security_hashes` VALUES(2744, 1366066635, '127.0.0.1', '82b6a9698bbdf838ed3243a2ac033b82a7b7ca5c');
INSERT INTO `exp_security_hashes` VALUES(2745, 1366066636, '127.0.0.1', '63ae2864ac68b6121a70c186aaad871b4ea2136f');
INSERT INTO `exp_security_hashes` VALUES(2746, 1366066639, '127.0.0.1', '9b9707c8ce4bd2b9173f8685b1ce400067e2e8f0');
INSERT INTO `exp_security_hashes` VALUES(2747, 1366066639, '127.0.0.1', '7865da6b62a697dcc0b238e3c6f6b939799ff262');
INSERT INTO `exp_security_hashes` VALUES(2748, 1366066639, '127.0.0.1', '834f76d42acc603d8c17d62a036e408c6425c06e');
INSERT INTO `exp_security_hashes` VALUES(2749, 1366066639, '127.0.0.1', 'f588f85e22ee59842545bd64538c8fd3572c84ed');
INSERT INTO `exp_security_hashes` VALUES(2750, 1366066639, '127.0.0.1', '9f8ee658837b16a829bdbcc6b2ff1d08046c8a92');
INSERT INTO `exp_security_hashes` VALUES(2751, 1366066639, '127.0.0.1', 'ac8cd39ef01db96cbb48a8b849f9594e8814c3d1');
INSERT INTO `exp_security_hashes` VALUES(2752, 1366066640, '127.0.0.1', '931781377301db96c7dd3e913ec67f3add462e54');
INSERT INTO `exp_security_hashes` VALUES(2753, 1366066640, '127.0.0.1', 'e13d6ec8bb6c8ce92a97e70fefcd0db42f3308ee');
INSERT INTO `exp_security_hashes` VALUES(2754, 1366066640, '127.0.0.1', '096237a3f1f0bd9ee905e8be3de33dfc6440a64c');
INSERT INTO `exp_security_hashes` VALUES(2755, 1366066640, '127.0.0.1', '1190a1175f2b43e9bb80887ec616374806bbb257');
INSERT INTO `exp_security_hashes` VALUES(2756, 1366066640, '127.0.0.1', 'ccf31a8894f94c1d731e1119f7f25a5e32de40fe');
INSERT INTO `exp_security_hashes` VALUES(2757, 1366066640, '127.0.0.1', '7453f288f54af59288697a159eae227d45052f1e');
INSERT INTO `exp_security_hashes` VALUES(2758, 1366066640, '127.0.0.1', '92f4af3ad4cf97bb80a57678b5a0b7b55be27568');
INSERT INTO `exp_security_hashes` VALUES(2759, 1366066640, '127.0.0.1', 'c42b2914e53add43ea4fa592d0bce42fa368875b');
INSERT INTO `exp_security_hashes` VALUES(2760, 1366066640, '127.0.0.1', '1cd487b0c7334dc7e73e13973c1688f125f64d8e');
INSERT INTO `exp_security_hashes` VALUES(2761, 1366066641, '127.0.0.1', '9c9a26f16a64d16d99589b6ddda295db1151c9d2');
INSERT INTO `exp_security_hashes` VALUES(2762, 1366066641, '127.0.0.1', '545d2a910d47346d3420fdf8f5d3ee45b015458c');
INSERT INTO `exp_security_hashes` VALUES(2763, 1366066641, '127.0.0.1', '8a4d73dc2381c15fd3c99360b970ed4c1dda5ba1');
INSERT INTO `exp_security_hashes` VALUES(2764, 1366066641, '127.0.0.1', '9627c77f8943e1cfcc089c4e8ec408325f9a640f');
INSERT INTO `exp_security_hashes` VALUES(2765, 1366066641, '127.0.0.1', 'b3c06268e73e2c8493b4c35cd25dfe50dd107081');
INSERT INTO `exp_security_hashes` VALUES(2766, 1366066641, '127.0.0.1', '528793a30fd3adc52004629afce8e8eb6e9f988a');
INSERT INTO `exp_security_hashes` VALUES(2767, 1366066641, '127.0.0.1', 'ff1db8cbfc288c67329ca869514307693f3e230c');
INSERT INTO `exp_security_hashes` VALUES(2768, 1366066641, '127.0.0.1', '4d209e30af45967d9767811f588576613a272148');
INSERT INTO `exp_security_hashes` VALUES(2769, 1366066641, '127.0.0.1', 'ff5c47cc2d973c53fadefcc49d63bbca226947ec');
INSERT INTO `exp_security_hashes` VALUES(2770, 1366066642, '127.0.0.1', '0bdbdaa0775bcafaad8e5cf4f5548f9193fee73d');
INSERT INTO `exp_security_hashes` VALUES(2771, 1366066642, '127.0.0.1', '81d7ceffb0bdd09bd8b7b00ae6bc6bd4dcb718e1');
INSERT INTO `exp_security_hashes` VALUES(2772, 1366066642, '127.0.0.1', '4eb5b4e70724e0f33ca2f808a5fa4784603d1194');
INSERT INTO `exp_security_hashes` VALUES(2773, 1366066642, '127.0.0.1', '11cc1ea2fe2217e484b9f48f6cf920c2f41b0e37');
INSERT INTO `exp_security_hashes` VALUES(2774, 1366066642, '127.0.0.1', '532fbd1b4a5cca851cc80b67d59f05b2aaadcfd7');
INSERT INTO `exp_security_hashes` VALUES(2775, 1366066642, '127.0.0.1', '80c9d512f71195d70521671d3bc66dddcc3a725e');
INSERT INTO `exp_security_hashes` VALUES(2776, 1366066642, '127.0.0.1', '8964423045d381ce0acfc5cb28c66ca405e34b5e');
INSERT INTO `exp_security_hashes` VALUES(2777, 1366066642, '127.0.0.1', '6a345358edc7b4f37820ddf09144af732e2fc7f8');
INSERT INTO `exp_security_hashes` VALUES(2778, 1366066642, '127.0.0.1', 'e4a98466a29a64eb973b3ad9352862f7b9f16c97');
INSERT INTO `exp_security_hashes` VALUES(2779, 1366066642, '127.0.0.1', '3054f036e751508e3189c427e8fcc70321cd84d3');
INSERT INTO `exp_security_hashes` VALUES(2780, 1366066643, '127.0.0.1', '1c05b180a220b8d110951b696db796b85e13b2fd');
INSERT INTO `exp_security_hashes` VALUES(2781, 1366066643, '127.0.0.1', '583f84d77632ae3660405b72b1336011c015311e');
INSERT INTO `exp_security_hashes` VALUES(2782, 1366066643, '127.0.0.1', '51d1ee6b8a552cba353556cc6f68b7c70da152d1');
INSERT INTO `exp_security_hashes` VALUES(2783, 1366066643, '127.0.0.1', 'fdb7278f9b875ff29b90790b1e4e4855178edeeb');
INSERT INTO `exp_security_hashes` VALUES(2784, 1366066643, '127.0.0.1', '4ebe5b96d1c272e380e55a351809fddfb00eb23c');
INSERT INTO `exp_security_hashes` VALUES(2785, 1366066643, '127.0.0.1', 'd6ed97db6365f2cc0bac713b14a1f3968a80b396');
INSERT INTO `exp_security_hashes` VALUES(2786, 1366066643, '127.0.0.1', '8ded8d7da2676f077c6fb59a50132cd6776762c9');
INSERT INTO `exp_security_hashes` VALUES(2787, 1366066643, '127.0.0.1', 'bd3073d5b36024b4e79700331ae709dd95396119');
INSERT INTO `exp_security_hashes` VALUES(2788, 1366066643, '127.0.0.1', '124f19590c49d6bbdaf98960f0aed9907a5879d8');
INSERT INTO `exp_security_hashes` VALUES(2789, 1366066643, '127.0.0.1', 'b1dc59f397711c169642272b0c7e80bf78bd3dfd');
INSERT INTO `exp_security_hashes` VALUES(2790, 1366066644, '127.0.0.1', 'f492c67648583e793e96129ba69be1017be57ea9');
INSERT INTO `exp_security_hashes` VALUES(2791, 1366066644, '127.0.0.1', 'c846222e5c4e05f991013f5cf0b118cde5e3bc49');
INSERT INTO `exp_security_hashes` VALUES(2792, 1366066644, '127.0.0.1', 'e6ff0aaca36b7d7acf11f35cc01b2c273d54e116');
INSERT INTO `exp_security_hashes` VALUES(2793, 1366066644, '127.0.0.1', '79de58e418128acec3f6b95a0e5faf9616e118b7');
INSERT INTO `exp_security_hashes` VALUES(2794, 1366066644, '127.0.0.1', 'a134d5fb3d72d5cf2890557c18f8dc485d29ed9c');
INSERT INTO `exp_security_hashes` VALUES(2795, 1366066644, '127.0.0.1', '3f5cfe917a34ac9a256f20b956bc8be1e88330d6');
INSERT INTO `exp_security_hashes` VALUES(2796, 1366066644, '127.0.0.1', '18e46362bff6393d614041ca9ca8777398c3804b');
INSERT INTO `exp_security_hashes` VALUES(2797, 1366066644, '127.0.0.1', 'fc25f77f9b7024cc0567841c4b4fddac642e2925');
INSERT INTO `exp_security_hashes` VALUES(2798, 1366066644, '127.0.0.1', 'e10ca4b9bec4107a670baf7e57649d02141c9e8e');
INSERT INTO `exp_security_hashes` VALUES(2799, 1366066644, '127.0.0.1', 'c57fc85e4632da661bf73e5a8f605fb93e47ea06');
INSERT INTO `exp_security_hashes` VALUES(2800, 1366066645, '127.0.0.1', '1bb086518b8a114c95e1b6a12b33a87cb6878d89');
INSERT INTO `exp_security_hashes` VALUES(2801, 1366066645, '127.0.0.1', 'f7a024267332f08c5cf4b189598f0e6f3ed08ed8');
INSERT INTO `exp_security_hashes` VALUES(2802, 1366066646, '127.0.0.1', '7f496ab43b5b1af8a5d265e0c2b753cd345953f0');
INSERT INTO `exp_security_hashes` VALUES(2803, 1366066646, '127.0.0.1', '456cf29ef54b6a7434b8d58afa63dbc0b961755f');
INSERT INTO `exp_security_hashes` VALUES(2804, 1366066646, '127.0.0.1', 'd269e8b265771ede6e4ad109bf4bd912b2cd908a');
INSERT INTO `exp_security_hashes` VALUES(2805, 1366066647, '127.0.0.1', '93f29d09afd71f2b3b4be6ccb79f5ab6e227186c');
INSERT INTO `exp_security_hashes` VALUES(2806, 1366066647, '127.0.0.1', '3feca1bccc4ca19ffe7362f875e069ed559ef7c2');
INSERT INTO `exp_security_hashes` VALUES(2807, 1366066647, '127.0.0.1', 'c7ba6360e219a9a071877abe308fcd0505d57bc1');
INSERT INTO `exp_security_hashes` VALUES(2808, 1366066648, '127.0.0.1', 'd58f476a8481ee2f37e43466b65fba2519589623');
INSERT INTO `exp_security_hashes` VALUES(2809, 1366066648, '127.0.0.1', 'c47613be52aa184774891604a9aba6e2e20937fb');
INSERT INTO `exp_security_hashes` VALUES(2810, 1366066648, '127.0.0.1', 'af7a0422b9bc715c85f7840b8a423a580770e213');
INSERT INTO `exp_security_hashes` VALUES(2811, 1366066649, '127.0.0.1', 'ce0b656f60d7daa1213184c16afef626609950e1');
INSERT INTO `exp_security_hashes` VALUES(2812, 1366066649, '127.0.0.1', '68197e69a2f0c37e1f5fb8f80c4d0e7d27693004');
INSERT INTO `exp_security_hashes` VALUES(2813, 1366066649, '127.0.0.1', 'd82a3d08d65491210447945f61a768fc7dc6f1d5');
INSERT INTO `exp_security_hashes` VALUES(2814, 1366066650, '127.0.0.1', 'f4f7e788323d460b7be0f934b2dce70deb755eb1');
INSERT INTO `exp_security_hashes` VALUES(2815, 1366066650, '127.0.0.1', '1ea7ab80b2c1eaf3f95b59fac9af8c0245ac1b07');
INSERT INTO `exp_security_hashes` VALUES(2816, 1366066651, '127.0.0.1', '5e2da03ec5433854a169bbbeeeff339de2d1106c');
INSERT INTO `exp_security_hashes` VALUES(2817, 1366066651, '127.0.0.1', '6a3d7d3f4f7b1df489e94d1f5c953a02356688c0');
INSERT INTO `exp_security_hashes` VALUES(2818, 1366066651, '127.0.0.1', 'c4cfc6109a42abe570e8cf08cdfb49d61f48e01b');
INSERT INTO `exp_security_hashes` VALUES(2819, 1366066652, '127.0.0.1', '1e02ce2f6e39a609538ad65b8095b78a269573c2');
INSERT INTO `exp_security_hashes` VALUES(2820, 1366066652, '127.0.0.1', '58790534b2ff578a5f8707f263cd308d8c21ca16');
INSERT INTO `exp_security_hashes` VALUES(2821, 1366066653, '127.0.0.1', '91e40edaab4ff5bea65b4c3428b62aa805ce26f7');
INSERT INTO `exp_security_hashes` VALUES(2822, 1366066653, '127.0.0.1', 'ccb63906e828dce45ae6ce68871de37c0308e2eb');
INSERT INTO `exp_security_hashes` VALUES(2823, 1366066653, '127.0.0.1', '3cbcbc430056514ec2e92eb2f192f91942c8f901');
INSERT INTO `exp_security_hashes` VALUES(2824, 1366066654, '127.0.0.1', '9498cf759a47a1b0e441b85b1d7d4c39705573ee');
INSERT INTO `exp_security_hashes` VALUES(2825, 1366066654, '127.0.0.1', 'ef92ecadc3a5e1beee20181b95dbc446e7ef6953');
INSERT INTO `exp_security_hashes` VALUES(2826, 1366066655, '127.0.0.1', '0cab9fbe940a2037ac2632c36e41d2b479ed6a31');
INSERT INTO `exp_security_hashes` VALUES(2827, 1366066655, '127.0.0.1', '7851a8fab57ab027626b2d0e2ec69389483dc4d3');
INSERT INTO `exp_security_hashes` VALUES(2828, 1366066655, '127.0.0.1', '1d84e4e3be19ff73243e16dad343d5f2d6502ce8');
INSERT INTO `exp_security_hashes` VALUES(2829, 1366066656, '127.0.0.1', '581a00c4db07129e45e33518754d658a994b0a54');
INSERT INTO `exp_security_hashes` VALUES(2830, 1366066656, '127.0.0.1', 'dff3bc2f537a69c00eb07a3442336a0aba8d2ce0');
INSERT INTO `exp_security_hashes` VALUES(2831, 1366066657, '127.0.0.1', 'be3cac62b051596657ef36eaba0848c715a804ac');
INSERT INTO `exp_security_hashes` VALUES(2832, 1366066657, '127.0.0.1', '6aa03b968d6db8764533cb07c7d464111e177dda');
INSERT INTO `exp_security_hashes` VALUES(2833, 1366066658, '127.0.0.1', '30427f80c1e69e3f7e016601b5dd696036fe5bed');
INSERT INTO `exp_security_hashes` VALUES(2834, 1366066661, '127.0.0.1', '31a1f82f51b79b57b669cc90cf4f83307285511d');
INSERT INTO `exp_security_hashes` VALUES(2835, 1366066662, '127.0.0.1', '18a5e883e3fbe02d572771f76c6fa40d979d6645');
INSERT INTO `exp_security_hashes` VALUES(2836, 1366066675, '127.0.0.1', '6c9d5f4795a6aa79cc3e1340ea9546778e50a653');
INSERT INTO `exp_security_hashes` VALUES(2837, 1366066675, '127.0.0.1', '32cb8dbe73e73074f9b8cee3543b68af0a9e3a1f');
INSERT INTO `exp_security_hashes` VALUES(2838, 1366066676, '127.0.0.1', '42d33964d7d6b557ded8a7e18bef5ff13624e893');
INSERT INTO `exp_security_hashes` VALUES(2839, 1366066676, '127.0.0.1', '5997e500ffb91b7a0f549d08d33eca3efc7d079a');
INSERT INTO `exp_security_hashes` VALUES(2840, 1366066676, '127.0.0.1', '29200034ef1037f8a7838ca6f97d78c28ec8f8ea');
INSERT INTO `exp_security_hashes` VALUES(2841, 1366066677, '127.0.0.1', '0e3fffd75fe9ee2e7579162e1fb079b0aea2172d');
INSERT INTO `exp_security_hashes` VALUES(2842, 1366066681, '127.0.0.1', '76ffeebf69a9998e9bf80fd359cf09f3c6dcea63');
INSERT INTO `exp_security_hashes` VALUES(2843, 1366066681, '127.0.0.1', '3fe463580bfd1ec3cd22264371c04fe4848eef60');
INSERT INTO `exp_security_hashes` VALUES(2844, 1366066682, '127.0.0.1', '70f2b5336286dc44235c358a5e152c63508248df');
INSERT INTO `exp_security_hashes` VALUES(2845, 1366066691, '127.0.0.1', '32858500ad98b2a63ad1e3c32bacde63ed349f59');
INSERT INTO `exp_security_hashes` VALUES(2846, 1366066701, '127.0.0.1', '7b41f6c24f613755f0e01755c600be81f64ffdcb');
INSERT INTO `exp_security_hashes` VALUES(2847, 1366066701, '127.0.0.1', '0e20be3d4a5991702e852e84c3297bf3a85f89c2');
INSERT INTO `exp_security_hashes` VALUES(2848, 1366066703, '127.0.0.1', 'df86f262374cd1e176d1ab8ef09a0fb5b7cf2bca');
INSERT INTO `exp_security_hashes` VALUES(2849, 1366066703, '127.0.0.1', '97260d5df389ec653ea2c89d31daad208c15b75d');
INSERT INTO `exp_security_hashes` VALUES(2850, 1366066704, '127.0.0.1', '5053509faaaf254c3c027c1f0ba3dc156821247c');
INSERT INTO `exp_security_hashes` VALUES(2851, 1366066705, '127.0.0.1', '5eb6019a13c618ead056b6cff7d0c268080abf1a');
INSERT INTO `exp_security_hashes` VALUES(2852, 1366066705, '127.0.0.1', 'c37a201d383ab0ccfab5d0439d07d48b18895675');
INSERT INTO `exp_security_hashes` VALUES(2853, 1366066706, '127.0.0.1', 'b57633d972b6ece3a31878e1e3cd651f43777170');
INSERT INTO `exp_security_hashes` VALUES(2854, 1366066709, '127.0.0.1', 'a702b049e4a626ddd822835369418834727d52da');
INSERT INTO `exp_security_hashes` VALUES(2855, 1366066710, '127.0.0.1', '33e947e28ecbe09dd126eb7fb7b32370f90c6ac6');
INSERT INTO `exp_security_hashes` VALUES(2856, 1366066715, '127.0.0.1', 'b00fc90df722529eefe4f427985a455305953fa2');
INSERT INTO `exp_security_hashes` VALUES(2857, 1366066715, '127.0.0.1', 'c71613a307da9f57e878f09bc31132596f54805e');
INSERT INTO `exp_security_hashes` VALUES(2858, 1366066715, '127.0.0.1', '32cf2db35c8983dac6d02a632d5ef59d8dc21b7d');
INSERT INTO `exp_security_hashes` VALUES(2859, 1366066715, '127.0.0.1', '8e3ad5ef9aef33b92207659ced0ff70f162becc3');
INSERT INTO `exp_security_hashes` VALUES(2860, 1366066715, '127.0.0.1', '768ad84350823c0f24eb6a6bd53df843a9f518f7');
INSERT INTO `exp_security_hashes` VALUES(2861, 1366066716, '127.0.0.1', 'e77f33b1459521cd8ef6e3afb6cdda44c2d2fd50');
INSERT INTO `exp_security_hashes` VALUES(2862, 1366066716, '127.0.0.1', 'ef1a28e7f08fc97358d6d548a4cbfb1f4119153d');
INSERT INTO `exp_security_hashes` VALUES(2863, 1366066716, '127.0.0.1', '10bb383e68763f9117e5f4981d7e3f980afac84f');
INSERT INTO `exp_security_hashes` VALUES(2864, 1366066717, '127.0.0.1', '65b0802342378e6a256789ded63beef3d8f8f511');
INSERT INTO `exp_security_hashes` VALUES(2865, 1366066720, '127.0.0.1', '0f00b11333f7ed4572360224a68f5eb371bed23a');
INSERT INTO `exp_security_hashes` VALUES(2866, 1366066727, '127.0.0.1', 'b8db8cce1a371c12203341d2d457c00931c8ab9b');
INSERT INTO `exp_security_hashes` VALUES(2867, 1366066727, '127.0.0.1', '6218702e864d2beafa7ea1dbd12448b2416c08d3');
INSERT INTO `exp_security_hashes` VALUES(2868, 1366066729, '127.0.0.1', '9a1b4854a4b87ac286a9d23c389797b5806ef315');
INSERT INTO `exp_security_hashes` VALUES(2869, 1366066729, '127.0.0.1', '77ef2f37808c842d1f231b2bd9ec54f044d53888');
INSERT INTO `exp_security_hashes` VALUES(2870, 1366066729, '127.0.0.1', '1bd4e924019cd977c1a16e71bbd8534f4029ad21');
INSERT INTO `exp_security_hashes` VALUES(2871, 1366066733, '127.0.0.1', 'dc6b6fea899f5c70087a95c7303a8b49a56e5d9b');
INSERT INTO `exp_security_hashes` VALUES(2872, 1366066734, '127.0.0.1', '8aec24c51336344e248e249df47e4f46bf0946f9');
INSERT INTO `exp_security_hashes` VALUES(2873, 1366066734, '127.0.0.1', 'a4199863f60b92ea36fdf503138072eeacccd14d');
INSERT INTO `exp_security_hashes` VALUES(2874, 1366066737, '127.0.0.1', '6912d2c192f5a99225b4e100dcc213b8c31196b3');
INSERT INTO `exp_security_hashes` VALUES(2875, 1366066737, '127.0.0.1', 'cf2c52b1374a6b817e5ae913a3391b7003c79aaa');
INSERT INTO `exp_security_hashes` VALUES(2876, 1366066738, '127.0.0.1', '6fa927107941a7529b854d83cb588afb4d5c0d00');
INSERT INTO `exp_security_hashes` VALUES(2877, 1366066749, '127.0.0.1', 'f28c24655161bceca271c213a65843ef5ee942c2');
INSERT INTO `exp_security_hashes` VALUES(2878, 1366066749, '127.0.0.1', 'a14fc28ed6a5d85c7bd5986f2c0478c61c6e7e36');
INSERT INTO `exp_security_hashes` VALUES(2879, 1366066750, '127.0.0.1', '2e5770d87b2bcc953da86b70c8d29ebbf633b372');
INSERT INTO `exp_security_hashes` VALUES(2880, 1366066750, '127.0.0.1', 'ed97005dbd5211beaae84491de20683698e22f72');
INSERT INTO `exp_security_hashes` VALUES(2881, 1366066750, '127.0.0.1', '3cf2515aaea389652846b5d1f5d90abc6170a195');
INSERT INTO `exp_security_hashes` VALUES(2882, 1366066750, '127.0.0.1', 'bb314e0d920001c19ba17ad3172ac23cf34204bb');
INSERT INTO `exp_security_hashes` VALUES(2883, 1366066750, '127.0.0.1', '8c86ff5ccad40bbda6629360a0ce20e97c6ffc1f');
INSERT INTO `exp_security_hashes` VALUES(2884, 1366066751, '127.0.0.1', 'f29eb157ed112574a74b9c201d394df1cd12f514');
INSERT INTO `exp_security_hashes` VALUES(2885, 1366066751, '127.0.0.1', '750a1c4633afb2702438254d55d864021045f5d7');
INSERT INTO `exp_security_hashes` VALUES(2886, 1366066753, '127.0.0.1', '1514ebd5f3626b6efc8531460ce86cdf23dbe2a6');
INSERT INTO `exp_security_hashes` VALUES(2887, 1366066754, '127.0.0.1', '869ba15c64b1707c00b175f2f9518288aa3adaab');
INSERT INTO `exp_security_hashes` VALUES(2888, 1366066754, '127.0.0.1', 'e77a3f5c081afa250d2098d5ff0c27ddcf4bce68');
INSERT INTO `exp_security_hashes` VALUES(2889, 1366066755, '127.0.0.1', 'd0038d8c331b83d61a9bfa8ab65725a4b8ace3e7');
INSERT INTO `exp_security_hashes` VALUES(2890, 1366066756, '127.0.0.1', 'db810c601fca4097713047dd5d5385338151bb5a');
INSERT INTO `exp_security_hashes` VALUES(2891, 1366066756, '127.0.0.1', 'e1109d6747d331a2eb76827b6f8fc26b08f44983');
INSERT INTO `exp_security_hashes` VALUES(2892, 1366066762, '127.0.0.1', '9eefb3f4246c7d4af9c653d62db63beda5a43f74');
INSERT INTO `exp_security_hashes` VALUES(2893, 1366066763, '127.0.0.1', '2650f9adf7968af4b8d6b0cf67ec338f487c143c');
INSERT INTO `exp_security_hashes` VALUES(2894, 1366066763, '127.0.0.1', 'c0d2a06229a780403f9eb7dbc7975d552a6cfaf2');
INSERT INTO `exp_security_hashes` VALUES(2895, 1366066766, '127.0.0.1', '032cab64e619ebba676949cbee2c48fbf7a768a3');
INSERT INTO `exp_security_hashes` VALUES(2896, 1366066766, '127.0.0.1', 'f58da66150e04cf0502be8a8dbd096ff78f70807');
INSERT INTO `exp_security_hashes` VALUES(2897, 1366066767, '127.0.0.1', 'b63bba2995adadba2d67bb9c44922341ecd0cb33');
INSERT INTO `exp_security_hashes` VALUES(2898, 1366066770, '127.0.0.1', 'ee6dc4f7522636c45d644149bbca6fb8e07a7d16');
INSERT INTO `exp_security_hashes` VALUES(2899, 1366066771, '127.0.0.1', 'fa8b275b25c6a34d6fd4a577722d73124b235ce9');
INSERT INTO `exp_security_hashes` VALUES(2900, 1366066771, '127.0.0.1', '3edb4d4b68da02b77a3e91a0726a10135103edd3');
INSERT INTO `exp_security_hashes` VALUES(2901, 1366066773, '127.0.0.1', '33d0aa11a383da52b976c3b63aa01a6dccd2e1b1');
INSERT INTO `exp_security_hashes` VALUES(2902, 1366066773, '127.0.0.1', 'ca19c260b37a514e311feb709e8d8577b9fa0b80');
INSERT INTO `exp_security_hashes` VALUES(2903, 1366066773, '127.0.0.1', '0b2dada3a49e74a71973ef6d311c1dd03367ed61');
INSERT INTO `exp_security_hashes` VALUES(2904, 1366066777, '127.0.0.1', '8e10dc3f6173623c1c2925832d1fe43338abaec9');
INSERT INTO `exp_security_hashes` VALUES(2905, 1366066778, '127.0.0.1', '3b258786fa110db8d3a89f9e8086d7bc09fcb213');
INSERT INTO `exp_security_hashes` VALUES(2906, 1366066778, '127.0.0.1', '8baf6b0a16aadbae02c6aa5a489184f4f2f099d6');
INSERT INTO `exp_security_hashes` VALUES(2907, 1366066779, '127.0.0.1', '5a068d5064dba2e873830e411b5e32e66e7abbaf');
INSERT INTO `exp_security_hashes` VALUES(2908, 1366066779, '127.0.0.1', '9dec6a844aa0daff64fa4968d6f9c18685396bc1');
INSERT INTO `exp_security_hashes` VALUES(2909, 1366066780, '127.0.0.1', '87e3e2f7ed33a87e28c018a6199a8c910b663b95');
INSERT INTO `exp_security_hashes` VALUES(2910, 1366066783, '127.0.0.1', '9d0683238427a37a43399c06b19ab7f95d0fb53f');
INSERT INTO `exp_security_hashes` VALUES(2911, 1366066784, '127.0.0.1', '6a5fb978bc2a6dc677c14edb905cf1627cc63d86');
INSERT INTO `exp_security_hashes` VALUES(2912, 1366066784, '127.0.0.1', 'eb0eb86fce813a228da797000dc36f5def2a338b');
INSERT INTO `exp_security_hashes` VALUES(2913, 1366066786, '127.0.0.1', 'c81a39c68b293842835d09677faf9bfbc73fb9fa');
INSERT INTO `exp_security_hashes` VALUES(2914, 1366066786, '127.0.0.1', '11e65b06a6b21efdd3679ab5c0b22fed7ffdcf52');
INSERT INTO `exp_security_hashes` VALUES(2915, 1366066786, '127.0.0.1', '6e86bbdedd9389a89138bc716565d332c58afc77');
INSERT INTO `exp_security_hashes` VALUES(2916, 1366066790, '127.0.0.1', '9b30edd51d1291e58cace2e72d2b27ac10a38ba4');
INSERT INTO `exp_security_hashes` VALUES(2917, 1366066791, '127.0.0.1', '095f16a07cb4380908215903dc7a522b9f135c90');
INSERT INTO `exp_security_hashes` VALUES(2918, 1366066791, '127.0.0.1', '2a9271f9bcfe2b763436dcffe798168fd0468a60');
INSERT INTO `exp_security_hashes` VALUES(2919, 1366066793, '127.0.0.1', '1dd5dd3ab2553697bf6bba57705bfdbf7adb2ec3');
INSERT INTO `exp_security_hashes` VALUES(2920, 1366066793, '127.0.0.1', 'bcfb5c96d40a3bfaf583fb442bdfc860e33003cb');
INSERT INTO `exp_security_hashes` VALUES(2921, 1366066793, '127.0.0.1', '54de5ec9768f9f5490e513731d4a3884938b153b');
INSERT INTO `exp_security_hashes` VALUES(2922, 1366066799, '127.0.0.1', '3548af724390e7e04a8faf909cb1068cacb8fb00');
INSERT INTO `exp_security_hashes` VALUES(2923, 1366066800, '127.0.0.1', 'fb295ef3846010b8039a0a5aa3784702b5a102f5');
INSERT INTO `exp_security_hashes` VALUES(2924, 1366066800, '127.0.0.1', 'bdc4eef4d3a9a430f838b5ccb80da9d8a4a72936');
INSERT INTO `exp_security_hashes` VALUES(2925, 1366066802, '127.0.0.1', 'b3e32709271e6d996049b47267a6ed25021bfe5b');
INSERT INTO `exp_security_hashes` VALUES(2926, 1366066802, '127.0.0.1', '08ea7875de1f36955fcc4b2109e3683d0584c3ba');
INSERT INTO `exp_security_hashes` VALUES(2927, 1366066802, '127.0.0.1', 'a4f6e1bf61c5b7ec2a21a4bd6388ec8686679ee5');
INSERT INTO `exp_security_hashes` VALUES(2928, 1366066807, '127.0.0.1', '1768e80760f57c442861fed708d8ee75fa89027b');
INSERT INTO `exp_security_hashes` VALUES(2929, 1366066808, '127.0.0.1', 'f2298dd253bb3611f39da7a61979b98289f89cc6');
INSERT INTO `exp_security_hashes` VALUES(2930, 1366066808, '127.0.0.1', 'fcaa0cb98ce4cb4f0dc053d3624d64513825edb6');
INSERT INTO `exp_security_hashes` VALUES(2931, 1366066809, '127.0.0.1', 'c2eabf29d0f612e09705d4b9c97bba2106c03350');
INSERT INTO `exp_security_hashes` VALUES(2932, 1366066810, '127.0.0.1', '4543d502d79c4727b0244a53d1f617bbb6f21276');
INSERT INTO `exp_security_hashes` VALUES(2933, 1366066810, '127.0.0.1', '3226368d54a58ecf72d16b9ac37fe0739dbd5149');
INSERT INTO `exp_security_hashes` VALUES(2934, 1366066820, '127.0.0.1', 'bb5f6d82af9ed1a38eeca7ad6953e0a95589328f');
INSERT INTO `exp_security_hashes` VALUES(2935, 1366066820, '127.0.0.1', 'e6fcaa2db45b05e22ae2862bcdf5806808aeb3a7');
INSERT INTO `exp_security_hashes` VALUES(2936, 1366066821, '127.0.0.1', '22d9ca40fd6c9bf750a897fe490266c2d598106c');
INSERT INTO `exp_security_hashes` VALUES(2937, 1366066821, '127.0.0.1', '7885e8d87ba93dfce7343d6fceb76337e50d7562');
INSERT INTO `exp_security_hashes` VALUES(2938, 1366066825, '127.0.0.1', 'e9e30c5330b96da94eac5a8f4ea03e1ca6b99452');
INSERT INTO `exp_security_hashes` VALUES(2939, 1366066826, '127.0.0.1', 'd8d727290d4001355396a56e10858b1db037f9a0');
INSERT INTO `exp_security_hashes` VALUES(2940, 1366066826, '127.0.0.1', 'ba87c76ca557e2fbd6ada23b2a39361787e6af18');
INSERT INTO `exp_security_hashes` VALUES(2941, 1366066827, '127.0.0.1', '265f5756bf80dd30465db7d7364f144481405ade');
INSERT INTO `exp_security_hashes` VALUES(2942, 1366066828, '127.0.0.1', '4a8b7a6264ea5db9dcd88f1825356ee762fee147');
INSERT INTO `exp_security_hashes` VALUES(2943, 1366066828, '127.0.0.1', '9479f407ddc68e00377f1f583991f28ce7b70443');
INSERT INTO `exp_security_hashes` VALUES(2944, 1366066829, '127.0.0.1', 'd7cad23bfd120e90dc39ec0eeec8c72d0934fa80');
INSERT INTO `exp_security_hashes` VALUES(2945, 1366066829, '127.0.0.1', '5f7b78f1196e038a22b12bd6755802e250515059');
INSERT INTO `exp_security_hashes` VALUES(2946, 1366066830, '127.0.0.1', 'f3b127a241f55c1ea23e6527d199689ea0b8dec5');
INSERT INTO `exp_security_hashes` VALUES(2947, 1366066840, '127.0.0.1', 'f3e6b1b87c65f2e7e97cf389e936ae20953442d1');
INSERT INTO `exp_security_hashes` VALUES(2948, 1366066840, '127.0.0.1', '334edae958bcafaa10a6d893292a0ab186391f8a');
INSERT INTO `exp_security_hashes` VALUES(2949, 1366066842, '127.0.0.1', '2d47a5ec85f155f7751e05b95346bed95c36a3cc');
INSERT INTO `exp_security_hashes` VALUES(2950, 1366066843, '127.0.0.1', '067a4684fa79f65952e932a652450b95c8581181');
INSERT INTO `exp_security_hashes` VALUES(2951, 1366066843, '127.0.0.1', '322a07fe84410806335369d83877b0849f9ff05f');
INSERT INTO `exp_security_hashes` VALUES(2952, 1366066844, '127.0.0.1', '2fa149acbe4b0432ed54f91ffea2c9a20fbb1f21');
INSERT INTO `exp_security_hashes` VALUES(2953, 1366066844, '127.0.0.1', 'eca68c3fcbb59ef888ad7181518a1fe35c2a79ac');
INSERT INTO `exp_security_hashes` VALUES(2954, 1366066844, '127.0.0.1', 'ce7656f5d1e94b87193dfafa07d115916cd11fbe');
INSERT INTO `exp_security_hashes` VALUES(2955, 1366066845, '127.0.0.1', 'f8a61ab629ccabad6e0a6d3a4713c1dc203886bd');
INSERT INTO `exp_security_hashes` VALUES(2956, 1366066845, '127.0.0.1', '9a4c42a25b53ef0614f3a51622f1a7100cfd0be5');
INSERT INTO `exp_security_hashes` VALUES(2957, 1366066845, '127.0.0.1', 'e19ce20262cce337fee1cb248ba0989151d504ef');
INSERT INTO `exp_security_hashes` VALUES(2958, 1366066846, '127.0.0.1', 'c31578eaf6b67be3000d5d2b8dc9833e257cb2e8');
INSERT INTO `exp_security_hashes` VALUES(2959, 1366066846, '127.0.0.1', 'ae3890d755b19e31da1dfa49b888dcc16b75d70a');
INSERT INTO `exp_security_hashes` VALUES(2960, 1366066852, '127.0.0.1', '0b3145e8087405042b85a502bce7ad7e828b2c97');
INSERT INTO `exp_security_hashes` VALUES(2961, 1366066853, '127.0.0.1', 'dc6fb6fe1bcdac2f9929b80024d6e3acbbd5ebfe');
INSERT INTO `exp_security_hashes` VALUES(2962, 1366066853, '127.0.0.1', 'd2d7e301c23ef9d23ff2fc6931244e2966df24af');
INSERT INTO `exp_security_hashes` VALUES(2963, 1366066854, '127.0.0.1', '933e5ae44649f1fea25bd14b949af722e3057f15');
INSERT INTO `exp_security_hashes` VALUES(2964, 1366066854, '127.0.0.1', '94dc9579851e81286e26d3b585d1fbba3206fd5e');
INSERT INTO `exp_security_hashes` VALUES(2965, 1366066854, '127.0.0.1', 'e3b41ddc69af96cddb4313122e213e7046fcf075');
INSERT INTO `exp_security_hashes` VALUES(2966, 1366066854, '127.0.0.1', '8f40ad80f46c98c404190bea5dade42d75023a6b');
INSERT INTO `exp_security_hashes` VALUES(2967, 1366066854, '127.0.0.1', '4061b9bc55f969a35c1af285d3f1e727c035755f');
INSERT INTO `exp_security_hashes` VALUES(2968, 1366066854, '127.0.0.1', '4a94771b89af3f34484fd22e41e013c741f40139');
INSERT INTO `exp_security_hashes` VALUES(2969, 1366066854, '127.0.0.1', '506c01a9521737b6dd20bff812706c59ee3e5f26');
INSERT INTO `exp_security_hashes` VALUES(2970, 1366066855, '127.0.0.1', 'c4e67ab5d06a191257abbe3eac70e100933cf946');
INSERT INTO `exp_security_hashes` VALUES(2971, 1366066855, '127.0.0.1', '921f267ced7bae9d9abb4a15015f6b78d9267429');
INSERT INTO `exp_security_hashes` VALUES(2972, 1366066855, '127.0.0.1', '5b5d4fb1dfb19d84eb62400986939fb6c3bafb7b');
INSERT INTO `exp_security_hashes` VALUES(2973, 1366066855, '127.0.0.1', 'ca61052fba5d2694e46812064fe249b65b5ea812');
INSERT INTO `exp_security_hashes` VALUES(2974, 1366066855, '127.0.0.1', '39355b92469913f5f5382f15ffb560d8d7beac98');
INSERT INTO `exp_security_hashes` VALUES(2975, 1366066856, '127.0.0.1', 'bbee8850fa9f2265e9b9409cacae62a3397b06e7');
INSERT INTO `exp_security_hashes` VALUES(2976, 1366066856, '127.0.0.1', '726219d25cc18a0f4f766dcd158ef0109dc7924b');
INSERT INTO `exp_security_hashes` VALUES(2977, 1366066856, '127.0.0.1', 'c083ba1516b48d3bd8923e4e4b34ca627f4efc63');
INSERT INTO `exp_security_hashes` VALUES(2978, 1366066857, '127.0.0.1', '7d65cda6a29c58521f8e49b2b3e639de10a50c95');
INSERT INTO `exp_security_hashes` VALUES(2979, 1366066857, '127.0.0.1', '72d493b38f5cccda82e2b64f7f38bd73fa2c0682');
INSERT INTO `exp_security_hashes` VALUES(2980, 1366066857, '127.0.0.1', '41577882912516173f79c655098823c187694f9e');
INSERT INTO `exp_security_hashes` VALUES(2981, 1366066857, '127.0.0.1', '2687c50ef7acabe370d9d66d481edfbf60a30bd4');
INSERT INTO `exp_security_hashes` VALUES(2982, 1366066857, '127.0.0.1', 'bcc8af5f8af1386fd90f68e5c7333fbda29b3534');
INSERT INTO `exp_security_hashes` VALUES(2983, 1366066857, '127.0.0.1', '83d56ca6eedd8cbf9caa56e93bddf3db5834db8a');
INSERT INTO `exp_security_hashes` VALUES(2984, 1366066857, '127.0.0.1', 'bded1a249db9efabc524a731dbaa8a7803e41e1d');
INSERT INTO `exp_security_hashes` VALUES(2985, 1366066858, '127.0.0.1', '21d61f5ed696804fcdb7ff43a61f3296c4c4d3db');
INSERT INTO `exp_security_hashes` VALUES(2986, 1366066858, '127.0.0.1', 'f8dbd272acc04b456682e45680c27193ab943149');
INSERT INTO `exp_security_hashes` VALUES(2987, 1366066858, '127.0.0.1', '1f203839b6db03fe81b06a52110cf437eb74e938');
INSERT INTO `exp_security_hashes` VALUES(2988, 1366066858, '127.0.0.1', '2d83c21221b148ab238243b269302c9b43de3941');
INSERT INTO `exp_security_hashes` VALUES(2989, 1366066858, '127.0.0.1', 'de1d07327f0f6181d1ddd5e1e741a8f596449405');
INSERT INTO `exp_security_hashes` VALUES(2990, 1366066858, '127.0.0.1', '5758dead82548a4fec31c828684400e2bad4d8f7');
INSERT INTO `exp_security_hashes` VALUES(2991, 1366066858, '127.0.0.1', '410859f7f579905ee2657bca003ad9ae4a07fc88');
INSERT INTO `exp_security_hashes` VALUES(2992, 1366066859, '127.0.0.1', 'e15b9e84b09c77ae7540ae9ff22799e62ac24d82');
INSERT INTO `exp_security_hashes` VALUES(2993, 1366066859, '127.0.0.1', 'f89f9e9ed57648f2994b2b047e1c95be7b9a0140');
INSERT INTO `exp_security_hashes` VALUES(2994, 1366066859, '127.0.0.1', 'ba22e0fbdb2e9dea405434cc3116a65f594359c6');
INSERT INTO `exp_security_hashes` VALUES(2995, 1366066859, '127.0.0.1', 'ad79927e6c627e01e53123b28e52972a5689c144');
INSERT INTO `exp_security_hashes` VALUES(2996, 1366066859, '127.0.0.1', '6d727048a2cff99b58af1e8063e79f60c848813d');
INSERT INTO `exp_security_hashes` VALUES(2997, 1366066859, '127.0.0.1', 'f218fe96481e956900093f89f23e2fe645420e35');
INSERT INTO `exp_security_hashes` VALUES(2998, 1366066859, '127.0.0.1', '9c832c3ef328db0454fddd98cf3ddacf913154d7');
INSERT INTO `exp_security_hashes` VALUES(2999, 1366066859, '127.0.0.1', '1cf6e2c26cc52c060cb72c72c5633fd3210c4785');
INSERT INTO `exp_security_hashes` VALUES(3000, 1366066859, '127.0.0.1', 'c4d9e51e7965850339356b8e60fa4c58b4a6e225');
INSERT INTO `exp_security_hashes` VALUES(3001, 1366066860, '127.0.0.1', 'f69abc289b80fcb8bb5f456cd0a7d9fd9499d087');
INSERT INTO `exp_security_hashes` VALUES(3002, 1366066860, '127.0.0.1', '2aa3471117d67bb60b3a5a53e33f3a8d2e885d2f');
INSERT INTO `exp_security_hashes` VALUES(3003, 1366066860, '127.0.0.1', 'fd24a37e9b467edaaa7bb0dfee3892ac4c5af37b');
INSERT INTO `exp_security_hashes` VALUES(3004, 1366066860, '127.0.0.1', 'b7f5cc628defb3c4731ad329062651d622ce3a52');
INSERT INTO `exp_security_hashes` VALUES(3005, 1366066860, '127.0.0.1', '3e65070595ca44cfa7a4f73c54daf0569ec37a4d');
INSERT INTO `exp_security_hashes` VALUES(3006, 1366066860, '127.0.0.1', '5192e8a0ef63972c1f182efb7fa3c1d993465f78');
INSERT INTO `exp_security_hashes` VALUES(3007, 1366066860, '127.0.0.1', '3bd80f9b5962b318850b9798dc6d2e3f78c25e45');
INSERT INTO `exp_security_hashes` VALUES(3008, 1366066860, '127.0.0.1', '18c966f39dcb9f0940dc1f99c13575b32e46a894');
INSERT INTO `exp_security_hashes` VALUES(3009, 1366066860, '127.0.0.1', '052b72bdbfe69c859a401526c791ef96f11d4fa6');
INSERT INTO `exp_security_hashes` VALUES(3010, 1366066860, '127.0.0.1', 'd7e2db3d837d28898bbd575edc3af6a1a75c4dc4');
INSERT INTO `exp_security_hashes` VALUES(3011, 1366066861, '127.0.0.1', '9848f97bc26bcf935cbf4fa6c2cd7dc4f290a49c');
INSERT INTO `exp_security_hashes` VALUES(3012, 1366066861, '127.0.0.1', '387de90f4aa12482b8f30d7b959d9da48faf6a38');
INSERT INTO `exp_security_hashes` VALUES(3013, 1366066861, '127.0.0.1', 'd22ceadc7eb65a1e19164cbdf86c8592ea30467f');
INSERT INTO `exp_security_hashes` VALUES(3014, 1366066861, '127.0.0.1', '48e12c50447471ed316a8eb4cddea0f50bee0d0e');
INSERT INTO `exp_security_hashes` VALUES(3015, 1366066861, '127.0.0.1', '3ca3d027c639e324703576bcf08ac61e7e2d0c03');
INSERT INTO `exp_security_hashes` VALUES(3016, 1366066861, '127.0.0.1', '1aab73d5a698d4daefbd415072f94f964e2623e6');
INSERT INTO `exp_security_hashes` VALUES(3017, 1366066862, '127.0.0.1', '05dae60bd2144a0e4840c79a59a1bdcf8ab9d2fc');
INSERT INTO `exp_security_hashes` VALUES(3018, 1366066862, '127.0.0.1', 'c3688a5da7203a0e4789adc60923fd395b2389c6');
INSERT INTO `exp_security_hashes` VALUES(3019, 1366066863, '127.0.0.1', '9b1055f33652e0ca5c6d274714f298bd6208b882');
INSERT INTO `exp_security_hashes` VALUES(3020, 1366066863, '127.0.0.1', '2d26a9f6e9492970773c578b2d905b262226700c');
INSERT INTO `exp_security_hashes` VALUES(3021, 1366066863, '127.0.0.1', 'e89806d18bfea39a3c218ec1deaf477da21b41bc');
INSERT INTO `exp_security_hashes` VALUES(3022, 1366066864, '127.0.0.1', 'e193a76ab50522f14ce3abf2d76be11712857cca');
INSERT INTO `exp_security_hashes` VALUES(3023, 1366066864, '127.0.0.1', 'aae42194d5efbf6c31944042817cb9ef72ad2317');
INSERT INTO `exp_security_hashes` VALUES(3024, 1366066865, '127.0.0.1', 'fe8d6ccb6b63aebf67dbd7f4161563dc4d20146f');
INSERT INTO `exp_security_hashes` VALUES(3025, 1366066865, '127.0.0.1', '6d965deeb1540d25155d123049372645ce7adcb9');
INSERT INTO `exp_security_hashes` VALUES(3026, 1366066866, '127.0.0.1', '642e30a08e556b98ee613f41d9083c44f9d89792');
INSERT INTO `exp_security_hashes` VALUES(3027, 1366066866, '127.0.0.1', '2a358d8bd4e0728ceca027b8b214a143b0b28d72');
INSERT INTO `exp_security_hashes` VALUES(3028, 1366066866, '127.0.0.1', '471bf50e5111297fd71a5dda038d939bb1209032');
INSERT INTO `exp_security_hashes` VALUES(3029, 1366066867, '127.0.0.1', 'e2254b25dba09655e653b6cdd37633d49567a429');
INSERT INTO `exp_security_hashes` VALUES(3030, 1366066867, '127.0.0.1', 'e3a1218c56863677011b8fdff4273e64779c671e');
INSERT INTO `exp_security_hashes` VALUES(3031, 1366066868, '127.0.0.1', '4ba14d4714fd07c111ac758adb6156deabed58d6');
INSERT INTO `exp_security_hashes` VALUES(3032, 1366066868, '127.0.0.1', '616a37e862554743b8b21f030e702b8bb6fe8911');
INSERT INTO `exp_security_hashes` VALUES(3033, 1366066870, '127.0.0.1', 'dc09daf20b0b52920e0695b9a6e792b86d0831d2');
INSERT INTO `exp_security_hashes` VALUES(3034, 1366066871, '127.0.0.1', '2ef9af80620ea4772edca33437c518dff4cebe55');
INSERT INTO `exp_security_hashes` VALUES(3035, 1366066871, '127.0.0.1', '6560fb70baac8e93aca78eb9f79b05385f32e5df');
INSERT INTO `exp_security_hashes` VALUES(3036, 1366066873, '127.0.0.1', '109e1898f10478a13b6583a69266de3b0f44f222');
INSERT INTO `exp_security_hashes` VALUES(3037, 1366066873, '127.0.0.1', 'eda9c83848d845cf3a278b2c3eef8c59221615f7');
INSERT INTO `exp_security_hashes` VALUES(3038, 1366066873, '127.0.0.1', '6c4457fc3416dd057d9d6fa7e73f91aa48d23228');
INSERT INTO `exp_security_hashes` VALUES(3039, 1366066874, '127.0.0.1', 'f8bbe395bb94cb75deab8acb62b65b7e257c1a45');
INSERT INTO `exp_security_hashes` VALUES(3040, 1366066875, '127.0.0.1', '5f86a9466a585f0380210e75a5509553c7610459');
INSERT INTO `exp_security_hashes` VALUES(3041, 1366066875, '127.0.0.1', 'e5889a27439c00533026881fe7ab1a1f1e65abd3');
INSERT INTO `exp_security_hashes` VALUES(3042, 1366066875, '127.0.0.1', '3a4bdc3525ea769fc9e496e8d0debd07a7f270e6');
INSERT INTO `exp_security_hashes` VALUES(3043, 1366066875, '127.0.0.1', '0efe4b8306c86dd5e201ee32015027e551ff38d2');
INSERT INTO `exp_security_hashes` VALUES(3044, 1366066875, '127.0.0.1', '60b0c8e194d2e31cc445c8509c084caf13390929');
INSERT INTO `exp_security_hashes` VALUES(3045, 1366066876, '127.0.0.1', '2a716414ed3a8ae5741ef217dda41c3e5900e32a');
INSERT INTO `exp_security_hashes` VALUES(3046, 1366066876, '127.0.0.1', '69f7b38b66db257b752b921e15edce71c418adea');
INSERT INTO `exp_security_hashes` VALUES(3047, 1366066876, '127.0.0.1', 'f382506a3c078bd8e6fec716a2166a81770014e1');
INSERT INTO `exp_security_hashes` VALUES(3048, 1366066878, '127.0.0.1', '6e5fa38e08621622ba6e060dd1bccb997d481bb2');
INSERT INTO `exp_security_hashes` VALUES(3049, 1366066879, '127.0.0.1', '97a0f1fb918e85180b59a29e3d6878c7b469a436');
INSERT INTO `exp_security_hashes` VALUES(3050, 1366066879, '127.0.0.1', '941657f797f16db8eea95060b6246d1b9a9c1a05');
INSERT INTO `exp_security_hashes` VALUES(3051, 1366066880, '127.0.0.1', '6be7a5c25397dc8ad9f4e8f4c857fdfb54712a1c');
INSERT INTO `exp_security_hashes` VALUES(3052, 1366066880, '127.0.0.1', '95feca81a48c4cc79c5f6b0d308388886fa7aa16');
INSERT INTO `exp_security_hashes` VALUES(3053, 1366066881, '127.0.0.1', 'edb0e90d73ec878038b87925865b8bd6da0311c0');
INSERT INTO `exp_security_hashes` VALUES(3054, 1366066883, '127.0.0.1', 'd314e50e3200fe1b67c094ddfd021d54b15e9228');
INSERT INTO `exp_security_hashes` VALUES(3055, 1366066884, '127.0.0.1', '826f2642f31fe7cb24364a80a59449f9f83dad86');
INSERT INTO `exp_security_hashes` VALUES(3056, 1366066884, '127.0.0.1', '0fca2e4202c1db8c482e61c62f6b588038b89e0c');
INSERT INTO `exp_security_hashes` VALUES(3057, 1366066887, '127.0.0.1', '9072b4ebbbeadddb43aebf148cbfcbc60b7ed23a');
INSERT INTO `exp_security_hashes` VALUES(3058, 1366066887, '127.0.0.1', 'c3cc2edcd61dfb9fea7a4b1a86fc89be7ff80ebb');
INSERT INTO `exp_security_hashes` VALUES(3059, 1366066888, '127.0.0.1', '1858e507d50fcb5dc7aa2c41edac03f9d44a0782');
INSERT INTO `exp_security_hashes` VALUES(3060, 1366066894, '127.0.0.1', '616dd48233d58e790bd068897dd22c39bef7356d');
INSERT INTO `exp_security_hashes` VALUES(3061, 1366066895, '127.0.0.1', '70c636bfec67394cd8893feec04a560c1b134307');
INSERT INTO `exp_security_hashes` VALUES(3062, 1366066895, '127.0.0.1', 'ebe35b3818893e5fa996e539f1bf10f51ec7a50c');
INSERT INTO `exp_security_hashes` VALUES(3063, 1366066896, '127.0.0.1', '7ed56ee72aabbd0115bf5e2b3cd9c0a3694c19bc');
INSERT INTO `exp_security_hashes` VALUES(3064, 1366066896, '127.0.0.1', '66a82b5661f26d170161ee0af34ab476648a5305');
INSERT INTO `exp_security_hashes` VALUES(3065, 1366066897, '127.0.0.1', '1d0c777e8eaf02d6642e5a5dd21803cb6005f51d');
INSERT INTO `exp_security_hashes` VALUES(3066, 1366066900, '127.0.0.1', '24063c9484f42232ee26e46308d7b214807d6419');
INSERT INTO `exp_security_hashes` VALUES(3067, 1366066901, '127.0.0.1', '85c2e27e22e249e326aa5c55aeea12cf3b3918f8');
INSERT INTO `exp_security_hashes` VALUES(3068, 1366066901, '127.0.0.1', '4e650468df955378bb062616d4cf1bd61b177379');
INSERT INTO `exp_security_hashes` VALUES(3069, 1366066902, '127.0.0.1', '2b24dad8991658c065f2f629436ad2c83150357c');
INSERT INTO `exp_security_hashes` VALUES(3070, 1366066902, '127.0.0.1', '1ace7bf96a01e7c6df7a1a7894f36bd3a81af400');
INSERT INTO `exp_security_hashes` VALUES(3071, 1366066903, '127.0.0.1', '427fe3f7e21d880fd1e7797c89ed55a4a54793de');
INSERT INTO `exp_security_hashes` VALUES(3072, 1366066906, '127.0.0.1', '24e73a1c2d34e9c631a2e868907649ffea6a2b6d');
INSERT INTO `exp_security_hashes` VALUES(3073, 1366066907, '127.0.0.1', 'd922ffa65cf6fe116416e311cc9b9bf83c6410f5');
INSERT INTO `exp_security_hashes` VALUES(3074, 1366066907, '127.0.0.1', '116bcc7bca83dd06fe546a7d75c871c33e036a6d');
INSERT INTO `exp_security_hashes` VALUES(3075, 1366066908, '127.0.0.1', '3355e70d6ea6377173f29ff1fb8d59faa040218b');
INSERT INTO `exp_security_hashes` VALUES(3076, 1366066908, '127.0.0.1', '4135b1797187ca8f18996c4931550192a9c48e98');
INSERT INTO `exp_security_hashes` VALUES(3077, 1366066909, '127.0.0.1', '0e6cfbf47f9fda1560fd5c37c218e98a4a4d2d05');
INSERT INTO `exp_security_hashes` VALUES(3078, 1366066916, '127.0.0.1', '581c86d99a78100a507c1b67d7b4b88e6063b001');
INSERT INTO `exp_security_hashes` VALUES(3079, 1366066917, '127.0.0.1', 'ba8a1f8a1155644e8cc1e9d99792accec9df1c8b');
INSERT INTO `exp_security_hashes` VALUES(3080, 1366066917, '127.0.0.1', '718245d4fcc1f30a704f4f29d82a2eaed05073cb');
INSERT INTO `exp_security_hashes` VALUES(3081, 1366066918, '127.0.0.1', 'd38169f6f9d025087daf6c95774f6a8d5cf1c6c8');
INSERT INTO `exp_security_hashes` VALUES(3082, 1366066918, '127.0.0.1', 'c75c418ffcb849f971507e6b1ebe8d4ea495c5ca');
INSERT INTO `exp_security_hashes` VALUES(3083, 1366066919, '127.0.0.1', 'ce1968670351327b03d261bdcd4296f78736fa2e');
INSERT INTO `exp_security_hashes` VALUES(3084, 1366066922, '127.0.0.1', '8c1b35b7e67db48444bd4a55790aef5f77b08f09');
INSERT INTO `exp_security_hashes` VALUES(3085, 1366066923, '127.0.0.1', 'a4f770eca33101432f44ab3c41d9c7513a85d93d');
INSERT INTO `exp_security_hashes` VALUES(3086, 1366066923, '127.0.0.1', 'ab934e70fa2a2abe025e1beedd5c8eb87e967b36');
INSERT INTO `exp_security_hashes` VALUES(3087, 1366066924, '127.0.0.1', '28042776158f3ebae0db23ab8695ecbdc5811a8d');
INSERT INTO `exp_security_hashes` VALUES(3088, 1366066924, '127.0.0.1', '6731eda24ad9a909e97dee137a5957d44253146c');
INSERT INTO `exp_security_hashes` VALUES(3089, 1366066925, '127.0.0.1', '31bed1802ba4512b30a94bb7bf8205d0a6880e3d');
INSERT INTO `exp_security_hashes` VALUES(3090, 1366066929, '127.0.0.1', '7ed4369bbc8a6434348468101df778e8e4da5071');
INSERT INTO `exp_security_hashes` VALUES(3091, 1366066930, '127.0.0.1', '65733e9a1f4e0828efc76a4b043f6646487c1fdb');
INSERT INTO `exp_security_hashes` VALUES(3092, 1366066931, '127.0.0.1', 'abb5384bec973d75dd11aafa3e9caee09d2800ad');
INSERT INTO `exp_security_hashes` VALUES(3093, 1366066932, '127.0.0.1', '2f4d6b03d57324d6efa16077b8af1ef3e42f113f');
INSERT INTO `exp_security_hashes` VALUES(3094, 1366066932, '127.0.0.1', '098708cc72198c5e4d2864c4abcc53e8138dbf30');
INSERT INTO `exp_security_hashes` VALUES(3095, 1366066933, '127.0.0.1', 'c12890bd6b1b91edef034c3baca617210db7588f');
INSERT INTO `exp_security_hashes` VALUES(3096, 1366066936, '127.0.0.1', '0d193145785e1f6b8eaf8e36e3b7bc19422db501');
INSERT INTO `exp_security_hashes` VALUES(3097, 1366066937, '127.0.0.1', 'c15b581353fb0ba7ba6cfe2c360c2ec0cbabae3d');
INSERT INTO `exp_security_hashes` VALUES(3098, 1366066937, '127.0.0.1', '4ca1802632ef389a77499478b3fe435dcd355be6');
INSERT INTO `exp_security_hashes` VALUES(3099, 1366066939, '127.0.0.1', 'fc79296d62ed5aaf6b14378923a827b94e906c5c');
INSERT INTO `exp_security_hashes` VALUES(3100, 1366066939, '127.0.0.1', '9945196e4bc41efd06892ee8cc4d488184232282');
INSERT INTO `exp_security_hashes` VALUES(3101, 1366066939, '127.0.0.1', 'ff29aaed1ff0e7d4166601985c5f4caeca87b7cf');
INSERT INTO `exp_security_hashes` VALUES(3102, 1366066942, '127.0.0.1', '735619f082defaef9398573d7be6d2fdde40f702');
INSERT INTO `exp_security_hashes` VALUES(3103, 1366066943, '127.0.0.1', '656f665f7375c2e3d61173f9fb249b85d5077f66');
INSERT INTO `exp_security_hashes` VALUES(3104, 1366066943, '127.0.0.1', 'a0171076dcdb59e5c3d39d0d8f196a2c9c531008');
INSERT INTO `exp_security_hashes` VALUES(3105, 1366066945, '127.0.0.1', 'a01b43818a9a30f657d8732106167c6384d1ecb3');
INSERT INTO `exp_security_hashes` VALUES(3106, 1366066945, '127.0.0.1', 'bc2ebc56f2f5c453be5723494c639226be2e2ede');
INSERT INTO `exp_security_hashes` VALUES(3107, 1366066946, '127.0.0.1', 'f847f50f85103f95806e9244c79bdfd00eeb1c18');
INSERT INTO `exp_security_hashes` VALUES(3108, 1366066946, '127.0.0.1', 'fc221fb7dc5450d27938639edfc0b2c86ce96903');
INSERT INTO `exp_security_hashes` VALUES(3109, 1366066946, '127.0.0.1', '129a8b319b80a84aa748e3a63619c8f845801688');
INSERT INTO `exp_security_hashes` VALUES(3110, 1366066947, '127.0.0.1', 'aea80c48cdf0e64698eef6ec25b08c17a4e33e2e');
INSERT INTO `exp_security_hashes` VALUES(3111, 1366069070, '127.0.0.1', 'eae9f68d966009a3eab36588e0697786ae44ef83');
INSERT INTO `exp_security_hashes` VALUES(3112, 1366069070, '127.0.0.1', 'f6650d8cf935a9ac56cdbb01a811d6377b2cda2d');
INSERT INTO `exp_security_hashes` VALUES(3113, 1366069071, '127.0.0.1', 'fc25a099d23e032becff3e0cbf4a353a09fa270b');
INSERT INTO `exp_security_hashes` VALUES(3114, 1366069137, '127.0.0.1', '65c5defc0326429429ff0fb4666793218a4122c9');
INSERT INTO `exp_security_hashes` VALUES(3115, 1366069137, '127.0.0.1', '89354d1bda3a9c59f40d06c3393abfe0e2a987d3');
INSERT INTO `exp_security_hashes` VALUES(3116, 1366069138, '127.0.0.1', '89a4a628464b5c06e5ab86b7547619694cdb9546');
INSERT INTO `exp_security_hashes` VALUES(3117, 1366071295, '127.0.0.1', '71f87ba1b76306d7560f4d8f79e40d8950d0ae08');
INSERT INTO `exp_security_hashes` VALUES(3118, 1366071295, '127.0.0.1', '3e60bb9c41ca327b7ab3490962b180ec4f5e7cb0');
INSERT INTO `exp_security_hashes` VALUES(3119, 1366071296, '127.0.0.1', 'b3fffeb654628118481aadfa391d44d1ec630b1c');
INSERT INTO `exp_security_hashes` VALUES(3120, 1366071324, '127.0.0.1', '3b7ac98b97609292b088713a30a94806c4eb85a1');
INSERT INTO `exp_security_hashes` VALUES(3121, 1366071337, '127.0.0.1', 'b1ca8a083db90a4895ac18858d41379fefb2c6fe');
INSERT INTO `exp_security_hashes` VALUES(3122, 1366071378, '127.0.0.1', '452753507145f64823f49b615b361068b0da1552');
INSERT INTO `exp_security_hashes` VALUES(3123, 1366071379, '127.0.0.1', '0f0087d7b34795f5eef39411aa55ce1d2e8d8a1e');
INSERT INTO `exp_security_hashes` VALUES(3124, 1366071380, '127.0.0.1', 'e72e81d2fd3629cebe9057e82de28b6d9800d383');
INSERT INTO `exp_security_hashes` VALUES(3125, 1366071391, '127.0.0.1', '67af83ef2c8bb9ee6aad1a1c79ac62206ae756ec');
INSERT INTO `exp_security_hashes` VALUES(3126, 1366071392, '127.0.0.1', 'b86f260cb9207f39a04f13c409077a3dcb2ccffb');
INSERT INTO `exp_security_hashes` VALUES(3127, 1366071392, '127.0.0.1', '38b5864ebed814d25d08a8ef2835583bf5f1d039');
INSERT INTO `exp_security_hashes` VALUES(3128, 1366071396, '127.0.0.1', 'cee04ad1399fb398c91091207611096efe78a168');
INSERT INTO `exp_security_hashes` VALUES(3129, 1366071397, '127.0.0.1', '228e6c9e17a5ec0fd5c21f85015c6bede8c7a9f7');
INSERT INTO `exp_security_hashes` VALUES(3130, 1366071397, '127.0.0.1', '4f18705d2b441178eaa15e4c338451fa809cf6ad');
INSERT INTO `exp_security_hashes` VALUES(3131, 1366071429, '127.0.0.1', '61225a5c2bf8d506cee47e25a1a761468689621f');
INSERT INTO `exp_security_hashes` VALUES(3132, 1366071430, '127.0.0.1', '9fe75fc47cdbc5c6b09673257f6befe12068675d');
INSERT INTO `exp_security_hashes` VALUES(3133, 1366071430, '127.0.0.1', 'e448e5d67291113de1b56a718f93b14ea643868f');
INSERT INTO `exp_security_hashes` VALUES(3134, 1366071434, '127.0.0.1', '11c3086bc719a2c1aa766409f03c22f1f3bc9a09');
INSERT INTO `exp_security_hashes` VALUES(3135, 1366071434, '127.0.0.1', '405d7e228d6ca528b566e43f37106f7b462c21ce');
INSERT INTO `exp_security_hashes` VALUES(3136, 1366071435, '127.0.0.1', '0e219c38d1a303ef3de4c1950dc43a0999121245');

/* Table structure for table `exp_sessions` */
DROP TABLE IF EXISTS `exp_sessions`;

CREATE TABLE `exp_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `admin_sess` tinyint(1) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  KEY `member_id` (`member_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_sessions` */
INSERT INTO `exp_sessions` VALUES('7d15dcb52e10c9c90cc07b692e31959583ad4028', 1, 1, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31', 1366071434);

/* Table structure for table `exp_sites` */
DROP TABLE IF EXISTS `exp_sites`;

CREATE TABLE `exp_sites` (
  `site_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `site_label` varchar(100) NOT NULL DEFAULT '',
  `site_name` varchar(50) NOT NULL DEFAULT '',
  `site_description` text,
  `site_system_preferences` mediumtext NOT NULL,
  `site_mailinglist_preferences` text NOT NULL,
  `site_member_preferences` text NOT NULL,
  `site_template_preferences` text NOT NULL,
  `site_channel_preferences` text NOT NULL,
  `site_bootstrap_checksums` text NOT NULL,
  PRIMARY KEY (`site_id`),
  KEY `site_name` (`site_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_sites` */
INSERT INTO `exp_sites` VALUES(1, 'Synergy Positioning Systems (NZ)', 'synergy_nz', '', 'YTo5Mjp7czoxMDoic2l0ZV9pbmRleCI7czowOiIiO3M6ODoic2l0ZV91cmwiO3M6MjE6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsLyI7czoxNjoidGhlbWVfZm9sZGVyX3VybCI7czoyODoiaHR0cDovL3N5bmVyZ3kubG9jYWwvdGhlbWVzLyI7czoxNToid2VibWFzdGVyX2VtYWlsIjtzOjE5OiJqYW1lc0A5NmJsYWNrLmNvLm56IjtzOjE0OiJ3ZWJtYXN0ZXJfbmFtZSI7czowOiIiO3M6MjA6ImNoYW5uZWxfbm9tZW5jbGF0dXJlIjtzOjc6ImNoYW5uZWwiO3M6MTA6Im1heF9jYWNoZXMiO3M6MzoiMTUwIjtzOjExOiJjYXB0Y2hhX3VybCI7czozNzoiaHR0cDovL3N5bmVyZ3kubG9jYWwvaW1hZ2VzL2NhcHRjaGFzLyI7czoxMjoiY2FwdGNoYV9wYXRoIjtzOjU0OiIvVXNlcnMvamFtZXMvUHJvamVjdHMvOTZibGFjay9TeW5lcmd5L2ltYWdlcy9jYXB0Y2hhcy8iO3M6MTI6ImNhcHRjaGFfZm9udCI7czoxOiJ5IjtzOjEyOiJjYXB0Y2hhX3JhbmQiO3M6MToieSI7czoyMzoiY2FwdGNoYV9yZXF1aXJlX21lbWJlcnMiO3M6MToibiI7czoxNzoiZW5hYmxlX2RiX2NhY2hpbmciO3M6MToibiI7czoxODoiZW5hYmxlX3NxbF9jYWNoaW5nIjtzOjE6Im4iO3M6MTg6ImZvcmNlX3F1ZXJ5X3N0cmluZyI7czoxOiJuIjtzOjEzOiJzaG93X3Byb2ZpbGVyIjtzOjE6Im4iO3M6MTg6InRlbXBsYXRlX2RlYnVnZ2luZyI7czoxOiJuIjtzOjE1OiJpbmNsdWRlX3NlY29uZHMiO3M6MToibiI7czoxMzoiY29va2llX2RvbWFpbiI7czowOiIiO3M6MTE6ImNvb2tpZV9wYXRoIjtzOjA6IiI7czoxNzoidXNlcl9zZXNzaW9uX3R5cGUiO3M6MToiYyI7czoxODoiYWRtaW5fc2Vzc2lvbl90eXBlIjtzOjI6ImNzIjtzOjIxOiJhbGxvd191c2VybmFtZV9jaGFuZ2UiO3M6MToieSI7czoxODoiYWxsb3dfbXVsdGlfbG9naW5zIjtzOjE6InkiO3M6MTY6InBhc3N3b3JkX2xvY2tvdXQiO3M6MToieSI7czoyNToicGFzc3dvcmRfbG9ja291dF9pbnRlcnZhbCI7czoxOiIxIjtzOjIwOiJyZXF1aXJlX2lwX2Zvcl9sb2dpbiI7czoxOiJ5IjtzOjIyOiJyZXF1aXJlX2lwX2Zvcl9wb3N0aW5nIjtzOjE6InkiO3M6MjQ6InJlcXVpcmVfc2VjdXJlX3Bhc3N3b3JkcyI7czoxOiJuIjtzOjE5OiJhbGxvd19kaWN0aW9uYXJ5X3B3IjtzOjE6InkiO3M6MjM6Im5hbWVfb2ZfZGljdGlvbmFyeV9maWxlIjtzOjA6IiI7czoxNzoieHNzX2NsZWFuX3VwbG9hZHMiO3M6MToieSI7czoxNToicmVkaXJlY3RfbWV0aG9kIjtzOjg6InJlZGlyZWN0IjtzOjk6ImRlZnRfbGFuZyI7czo3OiJlbmdsaXNoIjtzOjg6InhtbF9sYW5nIjtzOjI6ImVuIjtzOjEyOiJzZW5kX2hlYWRlcnMiO3M6MToieSI7czoxMToiZ3ppcF9vdXRwdXQiO3M6MToibiI7czoxMzoibG9nX3JlZmVycmVycyI7czoxOiJuIjtzOjEzOiJtYXhfcmVmZXJyZXJzIjtzOjM6IjUwMCI7czoxMToidGltZV9mb3JtYXQiO3M6MjoidXMiO3M6MTU6InNlcnZlcl90aW1lem9uZSI7czo0OiJVUDEyIjtzOjEzOiJzZXJ2ZXJfb2Zmc2V0IjtzOjA6IiI7czoxNjoiZGF5bGlnaHRfc2F2aW5ncyI7czoxOiJ5IjtzOjIxOiJkZWZhdWx0X3NpdGVfdGltZXpvbmUiO3M6NDoiVVAxMiI7czoxNjoiZGVmYXVsdF9zaXRlX2RzdCI7czoxOiJ5IjtzOjE1OiJob25vcl9lbnRyeV9kc3QiO3M6MToieSI7czoxMzoibWFpbF9wcm90b2NvbCI7czo0OiJtYWlsIjtzOjExOiJzbXRwX3NlcnZlciI7czowOiIiO3M6MTM6InNtdHBfdXNlcm5hbWUiO3M6MDoiIjtzOjEzOiJzbXRwX3Bhc3N3b3JkIjtzOjA6IiI7czoxMToiZW1haWxfZGVidWciO3M6MToibiI7czoxMzoiZW1haWxfY2hhcnNldCI7czo1OiJ1dGYtOCI7czoxNToiZW1haWxfYmF0Y2htb2RlIjtzOjE6Im4iO3M6MTY6ImVtYWlsX2JhdGNoX3NpemUiO3M6MDoiIjtzOjExOiJtYWlsX2Zvcm1hdCI7czo1OiJwbGFpbiI7czo5OiJ3b3JkX3dyYXAiO3M6MToieSI7czoyMjoiZW1haWxfY29uc29sZV90aW1lbG9jayI7czoxOiI1IjtzOjIyOiJsb2dfZW1haWxfY29uc29sZV9tc2dzIjtzOjE6InkiO3M6ODoiY3BfdGhlbWUiO3M6NzoiZGVmYXVsdCI7czoyMToiZW1haWxfbW9kdWxlX2NhcHRjaGFzIjtzOjE6Im4iO3M6MTY6ImxvZ19zZWFyY2hfdGVybXMiO3M6MToieSI7czoxMjoic2VjdXJlX2Zvcm1zIjtzOjE6InkiO3M6MTk6ImRlbnlfZHVwbGljYXRlX2RhdGEiO3M6MToieSI7czoyNDoicmVkaXJlY3Rfc3VibWl0dGVkX2xpbmtzIjtzOjE6Im4iO3M6MTY6ImVuYWJsZV9jZW5zb3JpbmciO3M6MToibiI7czoxNDoiY2Vuc29yZWRfd29yZHMiO3M6MDoiIjtzOjE4OiJjZW5zb3JfcmVwbGFjZW1lbnQiO3M6MDoiIjtzOjEwOiJiYW5uZWRfaXBzIjtzOjA6IiI7czoxMzoiYmFubmVkX2VtYWlscyI7czowOiIiO3M6MTY6ImJhbm5lZF91c2VybmFtZXMiO3M6MDoiIjtzOjE5OiJiYW5uZWRfc2NyZWVuX25hbWVzIjtzOjA6IiI7czoxMDoiYmFuX2FjdGlvbiI7czo4OiJyZXN0cmljdCI7czoxMToiYmFuX21lc3NhZ2UiO3M6MzQ6IlRoaXMgc2l0ZSBpcyBjdXJyZW50bHkgdW5hdmFpbGFibGUiO3M6MTU6ImJhbl9kZXN0aW5hdGlvbiI7czoyMToiaHR0cDovL3d3dy55YWhvby5jb20vIjtzOjE2OiJlbmFibGVfZW1vdGljb25zIjtzOjE6InkiO3M6MTI6ImVtb3RpY29uX3VybCI7czozNjoiaHR0cDovL3N5bmVyZ3kubG9jYWwvaW1hZ2VzL3NtaWxleXMvIjtzOjE5OiJyZWNvdW50X2JhdGNoX3RvdGFsIjtzOjQ6IjEwMDAiO3M6MTc6Im5ld192ZXJzaW9uX2NoZWNrIjtzOjE6InkiO3M6MTc6ImVuYWJsZV90aHJvdHRsaW5nIjtzOjE6Im4iO3M6MTc6ImJhbmlzaF9tYXNrZWRfaXBzIjtzOjE6InkiO3M6MTQ6Im1heF9wYWdlX2xvYWRzIjtzOjI6IjEwIjtzOjEzOiJ0aW1lX2ludGVydmFsIjtzOjE6IjgiO3M6MTI6ImxvY2tvdXRfdGltZSI7czoyOiIzMCI7czoxNToiYmFuaXNobWVudF90eXBlIjtzOjc6Im1lc3NhZ2UiO3M6MTQ6ImJhbmlzaG1lbnRfdXJsIjtzOjA6IiI7czoxODoiYmFuaXNobWVudF9tZXNzYWdlIjtzOjUwOiJZb3UgaGF2ZSBleGNlZWRlZCB0aGUgYWxsb3dlZCBwYWdlIGxvYWQgZnJlcXVlbmN5LiI7czoxNzoiZW5hYmxlX3NlYXJjaF9sb2ciO3M6MToieSI7czoxOToibWF4X2xvZ2dlZF9zZWFyY2hlcyI7czozOiI1MDAiO3M6MTc6InRoZW1lX2ZvbGRlcl9wYXRoIjtzOjQ1OiIvVXNlcnMvamFtZXMvUHJvamVjdHMvOTZibGFjay9TeW5lcmd5L3RoZW1lcy8iO3M6MTA6ImlzX3NpdGVfb24iO3M6MToieSI7czoxMToicnRlX2VuYWJsZWQiO3M6MToieSI7czoyMjoicnRlX2RlZmF1bHRfdG9vbHNldF9pZCI7czoxOiIxIjt9', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6MzY6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9hdmF0YXJzLyI7czoxMToiYXZhdGFyX3BhdGgiO3M6NTM6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL2F2YXRhcnMvIjtzOjE2OiJhdmF0YXJfbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNzoiYXZhdGFyX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEzOiJhdmF0YXJfbWF4X2tiIjtzOjI6IjUwIjtzOjEzOiJlbmFibGVfcGhvdG9zIjtzOjE6Im4iO3M6OToicGhvdG9fdXJsIjtzOjQyOiJodHRwOi8vc3luZXJneS5sb2NhbC9pbWFnZXMvbWVtYmVyX3Bob3Rvcy8iO3M6MTA6InBob3RvX3BhdGgiO3M6NTk6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjE1OiJwaG90b19tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE2OiJwaG90b19tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMjoicGhvdG9fbWF4X2tiIjtzOjI6IjUwIjtzOjE2OiJhbGxvd19zaWduYXR1cmVzIjtzOjE6InkiO3M6MTM6InNpZ19tYXhsZW5ndGgiO3M6MzoiNTAwIjtzOjIxOiJzaWdfYWxsb3dfaW1nX2hvdGxpbmsiO3M6MToibiI7czoyMDoic2lnX2FsbG93X2ltZ191cGxvYWQiO3M6MToibiI7czoxMToic2lnX2ltZ191cmwiO3M6NTA6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9zaWduYXR1cmVfYXR0YWNobWVudHMvIjtzOjEyOiJzaWdfaW1nX3BhdGgiO3M6Njc6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo2MDoiL1VzZXJzL2phbWVzL1Byb2plY3RzLzk2YmxhY2svU3luZXJneS9pbWFnZXMvcG1fYXR0YWNobWVudHMvIjtzOjIzOiJwcnZfbXNnX21heF9hdHRhY2htZW50cyI7czoxOiIzIjtzOjIyOiJwcnZfbXNnX2F0dGFjaF9tYXhzaXplIjtzOjM6IjI1MCI7czoyMDoicHJ2X21zZ19hdHRhY2hfdG90YWwiO3M6MzoiMTAwIjtzOjE5OiJwcnZfbXNnX2h0bWxfZm9ybWF0IjtzOjQ6InNhZmUiO3M6MTg6InBydl9tc2dfYXV0b19saW5rcyI7czoxOiJ5IjtzOjE3OiJwcnZfbXNnX21heF9jaGFycyI7czo0OiI2MDAwIjtzOjE5OiJtZW1iZXJsaXN0X29yZGVyX2J5IjtzOjExOiJ0b3RhbF9wb3N0cyI7czoyMToibWVtYmVybGlzdF9zb3J0X29yZGVyIjtzOjQ6ImRlc2MiO3M6MjA6Im1lbWJlcmxpc3Rfcm93X2xpbWl0IjtzOjI6IjIwIjt9', 'YTo2OntzOjExOiJzdHJpY3RfdXJscyI7czoxOiJuIjtzOjg6InNpdGVfNDA0IjtzOjA6IiI7czoxOToic2F2ZV90bXBsX3JldmlzaW9ucyI7czoxOiJuIjtzOjE4OiJtYXhfdG1wbF9yZXZpc2lvbnMiO3M6MToiNSI7czoxNToic2F2ZV90bXBsX2ZpbGVzIjtzOjE6InkiO3M6MTg6InRtcGxfZmlsZV9iYXNlcGF0aCI7czo0NjoiL1VzZXJzL2phbWVzbWNmYWxsL1Byb2plY3RzL1N5bmVyZ3kvdGVtcGxhdGVzLyI7fQ==', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YToyOntzOjc6ImVtYWlsZWQiO2E6MDp7fXM6NDU6Ii9Vc2Vycy9qYW1lc21jZmFsbC9Qcm9qZWN0cy9TeW5lcmd5L2luZGV4LnBocCI7czozMjoiZjFiMTliOGE0NDU5NmM1MzQ2MTViZWJlMTMyNmYwNjgiO30=');
INSERT INTO `exp_sites` VALUES(2, 'Synergy Positioning Systems (AU)', 'synergy_au', '', 'YTo5MTp7czoxMDoiaXNfc2l0ZV9vbiI7czoxOiJ5IjtzOjEwOiJzaXRlX2luZGV4IjtzOjA6IiI7czo4OiJzaXRlX3VybCI7czoyMToiaHR0cDovL3N5bmVyZ3kubG9jYWwvIjtzOjE2OiJ0aGVtZV9mb2xkZXJfdXJsIjtzOjI4OiJodHRwOi8vc3luZXJneS5sb2NhbC90aGVtZXMvIjtzOjE3OiJ0aGVtZV9mb2xkZXJfcGF0aCI7czo0NToiL1VzZXJzL2phbWVzL1Byb2plY3RzLzk2YmxhY2svU3luZXJneS90aGVtZXMvIjtzOjE1OiJ3ZWJtYXN0ZXJfZW1haWwiO3M6MTk6ImphbWVzQDk2YmxhY2suY28ubnoiO3M6MTQ6IndlYm1hc3Rlcl9uYW1lIjtzOjA6IiI7czoyMDoiY2hhbm5lbF9ub21lbmNsYXR1cmUiO3M6NzoiY2hhbm5lbCI7czoxMDoibWF4X2NhY2hlcyI7czozOiIxNTAiO3M6MTE6ImNhcHRjaGFfdXJsIjtzOjM3OiJodHRwOi8vc3luZXJneS5sb2NhbC9pbWFnZXMvY2FwdGNoYXMvIjtzOjEyOiJjYXB0Y2hhX3BhdGgiO3M6NTQ6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL2NhcHRjaGFzLyI7czoxMjoiY2FwdGNoYV9mb250IjtzOjE6InkiO3M6MTI6ImNhcHRjaGFfcmFuZCI7czoxOiJ5IjtzOjIzOiJjYXB0Y2hhX3JlcXVpcmVfbWVtYmVycyI7czoxOiJuIjtzOjE3OiJlbmFibGVfZGJfY2FjaGluZyI7czoxOiJuIjtzOjE4OiJlbmFibGVfc3FsX2NhY2hpbmciO3M6MToibiI7czoxODoiZm9yY2VfcXVlcnlfc3RyaW5nIjtzOjE6Im4iO3M6MTM6InNob3dfcHJvZmlsZXIiO3M6MToibiI7czoxODoidGVtcGxhdGVfZGVidWdnaW5nIjtzOjE6Im4iO3M6MTU6ImluY2x1ZGVfc2Vjb25kcyI7czoxOiJuIjtzOjEzOiJjb29raWVfZG9tYWluIjtzOjA6IiI7czoxMToiY29va2llX3BhdGgiO3M6MDoiIjtzOjE3OiJ1c2VyX3Nlc3Npb25fdHlwZSI7czoxOiJjIjtzOjE4OiJhZG1pbl9zZXNzaW9uX3R5cGUiO3M6MjoiY3MiO3M6MjE6ImFsbG93X3VzZXJuYW1lX2NoYW5nZSI7czoxOiJ5IjtzOjE4OiJhbGxvd19tdWx0aV9sb2dpbnMiO3M6MToieSI7czoxNjoicGFzc3dvcmRfbG9ja291dCI7czoxOiJ5IjtzOjI1OiJwYXNzd29yZF9sb2Nrb3V0X2ludGVydmFsIjtzOjE6IjEiO3M6MjA6InJlcXVpcmVfaXBfZm9yX2xvZ2luIjtzOjE6InkiO3M6MjI6InJlcXVpcmVfaXBfZm9yX3Bvc3RpbmciO3M6MToieSI7czoyNDoicmVxdWlyZV9zZWN1cmVfcGFzc3dvcmRzIjtzOjE6Im4iO3M6MTk6ImFsbG93X2RpY3Rpb25hcnlfcHciO3M6MToieSI7czoyMzoibmFtZV9vZl9kaWN0aW9uYXJ5X2ZpbGUiO3M6MDoiIjtzOjE3OiJ4c3NfY2xlYW5fdXBsb2FkcyI7czoxOiJ5IjtzOjE1OiJyZWRpcmVjdF9tZXRob2QiO3M6ODoicmVkaXJlY3QiO3M6OToiZGVmdF9sYW5nIjtzOjc6ImVuZ2xpc2giO3M6ODoieG1sX2xhbmciO3M6MjoiZW4iO3M6MTI6InNlbmRfaGVhZGVycyI7czoxOiJ5IjtzOjExOiJnemlwX291dHB1dCI7czoxOiJuIjtzOjEzOiJsb2dfcmVmZXJyZXJzIjtzOjE6Im4iO3M6MTM6Im1heF9yZWZlcnJlcnMiO3M6MzoiNTAwIjtzOjExOiJ0aW1lX2Zvcm1hdCI7czoyOiJ1cyI7czoxNToic2VydmVyX3RpbWV6b25lIjtzOjQ6IlVQMTIiO3M6MTM6InNlcnZlcl9vZmZzZXQiO3M6MDoiIjtzOjE2OiJkYXlsaWdodF9zYXZpbmdzIjtzOjE6InkiO3M6MjE6ImRlZmF1bHRfc2l0ZV90aW1lem9uZSI7czo0OiJVUDEyIjtzOjE2OiJkZWZhdWx0X3NpdGVfZHN0IjtzOjE6InkiO3M6MTM6Im1haWxfcHJvdG9jb2wiO3M6NDoibWFpbCI7czoxMToic210cF9zZXJ2ZXIiO3M6MDoiIjtzOjEzOiJzbXRwX3VzZXJuYW1lIjtzOjA6IiI7czoxMzoic210cF9wYXNzd29yZCI7czowOiIiO3M6MTE6ImVtYWlsX2RlYnVnIjtzOjE6Im4iO3M6MTM6ImVtYWlsX2NoYXJzZXQiO3M6NToidXRmLTgiO3M6MTU6ImVtYWlsX2JhdGNobW9kZSI7czoxOiJuIjtzOjE2OiJlbWFpbF9iYXRjaF9zaXplIjtzOjA6IiI7czoxMToibWFpbF9mb3JtYXQiO3M6NToicGxhaW4iO3M6OToid29yZF93cmFwIjtzOjE6InkiO3M6MjI6ImVtYWlsX2NvbnNvbGVfdGltZWxvY2siO3M6MToiNSI7czoyMjoibG9nX2VtYWlsX2NvbnNvbGVfbXNncyI7czoxOiJ5IjtzOjg6ImNwX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MjE6ImVtYWlsX21vZHVsZV9jYXB0Y2hhcyI7czoxOiJuIjtzOjE2OiJsb2dfc2VhcmNoX3Rlcm1zIjtzOjE6InkiO3M6MTI6InNlY3VyZV9mb3JtcyI7czoxOiJ5IjtzOjE5OiJkZW55X2R1cGxpY2F0ZV9kYXRhIjtzOjE6InkiO3M6MjQ6InJlZGlyZWN0X3N1Ym1pdHRlZF9saW5rcyI7czoxOiJuIjtzOjE2OiJlbmFibGVfY2Vuc29yaW5nIjtzOjE6Im4iO3M6MTQ6ImNlbnNvcmVkX3dvcmRzIjtzOjA6IiI7czoxODoiY2Vuc29yX3JlcGxhY2VtZW50IjtzOjA6IiI7czoxMDoiYmFubmVkX2lwcyI7czowOiIiO3M6MTM6ImJhbm5lZF9lbWFpbHMiO3M6MDoiIjtzOjE2OiJiYW5uZWRfdXNlcm5hbWVzIjtzOjA6IiI7czoxOToiYmFubmVkX3NjcmVlbl9uYW1lcyI7czowOiIiO3M6MTA6ImJhbl9hY3Rpb24iO3M6ODoicmVzdHJpY3QiO3M6MTE6ImJhbl9tZXNzYWdlIjtzOjM0OiJUaGlzIHNpdGUgaXMgY3VycmVudGx5IHVuYXZhaWxhYmxlIjtzOjE1OiJiYW5fZGVzdGluYXRpb24iO3M6MjE6Imh0dHA6Ly93d3cueWFob28uY29tLyI7czoxNjoiZW5hYmxlX2Vtb3RpY29ucyI7czoxOiJ5IjtzOjEyOiJlbW90aWNvbl91cmwiO3M6MzY6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9zbWlsZXlzLyI7czoxOToicmVjb3VudF9iYXRjaF90b3RhbCI7czo0OiIxMDAwIjtzOjE3OiJuZXdfdmVyc2lvbl9jaGVjayI7czoxOiJ5IjtzOjE3OiJlbmFibGVfdGhyb3R0bGluZyI7czoxOiJuIjtzOjE3OiJiYW5pc2hfbWFza2VkX2lwcyI7czoxOiJ5IjtzOjE0OiJtYXhfcGFnZV9sb2FkcyI7czoyOiIxMCI7czoxMzoidGltZV9pbnRlcnZhbCI7czoxOiI4IjtzOjEyOiJsb2Nrb3V0X3RpbWUiO3M6MjoiMzAiO3M6MTU6ImJhbmlzaG1lbnRfdHlwZSI7czo3OiJtZXNzYWdlIjtzOjE0OiJiYW5pc2htZW50X3VybCI7czowOiIiO3M6MTg6ImJhbmlzaG1lbnRfbWVzc2FnZSI7czo1MDoiWW91IGhhdmUgZXhjZWVkZWQgdGhlIGFsbG93ZWQgcGFnZSBsb2FkIGZyZXF1ZW5jeS4iO3M6MTc6ImVuYWJsZV9zZWFyY2hfbG9nIjtzOjE6InkiO3M6MTk6Im1heF9sb2dnZWRfc2VhcmNoZXMiO3M6MzoiNTAwIjtzOjExOiJydGVfZW5hYmxlZCI7czoxOiJ5IjtzOjIyOiJydGVfZGVmYXVsdF90b29sc2V0X2lkIjtzOjE6IjEiO30=', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6MzY6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9hdmF0YXJzLyI7czoxMToiYXZhdGFyX3BhdGgiO3M6NTM6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL2F2YXRhcnMvIjtzOjE2OiJhdmF0YXJfbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNzoiYXZhdGFyX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEzOiJhdmF0YXJfbWF4X2tiIjtzOjI6IjUwIjtzOjEzOiJlbmFibGVfcGhvdG9zIjtzOjE6Im4iO3M6OToicGhvdG9fdXJsIjtzOjQyOiJodHRwOi8vc3luZXJneS5sb2NhbC9pbWFnZXMvbWVtYmVyX3Bob3Rvcy8iO3M6MTA6InBob3RvX3BhdGgiO3M6NTk6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjE1OiJwaG90b19tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE2OiJwaG90b19tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMjoicGhvdG9fbWF4X2tiIjtzOjI6IjUwIjtzOjE2OiJhbGxvd19zaWduYXR1cmVzIjtzOjE6InkiO3M6MTM6InNpZ19tYXhsZW5ndGgiO3M6MzoiNTAwIjtzOjIxOiJzaWdfYWxsb3dfaW1nX2hvdGxpbmsiO3M6MToibiI7czoyMDoic2lnX2FsbG93X2ltZ191cGxvYWQiO3M6MToibiI7czoxMToic2lnX2ltZ191cmwiO3M6NTA6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9zaWduYXR1cmVfYXR0YWNobWVudHMvIjtzOjEyOiJzaWdfaW1nX3BhdGgiO3M6Njc6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo2MDoiL1VzZXJzL2phbWVzL1Byb2plY3RzLzk2YmxhY2svU3luZXJneS9pbWFnZXMvcG1fYXR0YWNobWVudHMvIjtzOjIzOiJwcnZfbXNnX21heF9hdHRhY2htZW50cyI7czoxOiIzIjtzOjIyOiJwcnZfbXNnX2F0dGFjaF9tYXhzaXplIjtzOjM6IjI1MCI7czoyMDoicHJ2X21zZ19hdHRhY2hfdG90YWwiO3M6MzoiMTAwIjtzOjE5OiJwcnZfbXNnX2h0bWxfZm9ybWF0IjtzOjQ6InNhZmUiO3M6MTg6InBydl9tc2dfYXV0b19saW5rcyI7czoxOiJ5IjtzOjE3OiJwcnZfbXNnX21heF9jaGFycyI7czo0OiI2MDAwIjtzOjE5OiJtZW1iZXJsaXN0X29yZGVyX2J5IjtzOjExOiJ0b3RhbF9wb3N0cyI7czoyMToibWVtYmVybGlzdF9zb3J0X29yZGVyIjtzOjQ6ImRlc2MiO3M6MjA6Im1lbWJlcmxpc3Rfcm93X2xpbWl0IjtzOjI6IjIwIjt9', 'YTo2OntzOjE1OiJzYXZlX3RtcGxfZmlsZXMiO3M6MToieSI7czoxODoidG1wbF9maWxlX2Jhc2VwYXRoIjtzOjQ4OiIvVXNlcnMvamFtZXMvUHJvamVjdHMvOTZibGFjay9TeW5lcmd5L3RlbXBsYXRlcy8iO3M6ODoic2l0ZV80MDQiO3M6MDoiIjtzOjE5OiJzYXZlX3RtcGxfcmV2aXNpb25zIjtzOjE6Im4iO3M6MTg6Im1heF90bXBsX3JldmlzaW9ucyI7czoxOiI1IjtzOjExOiJzdHJpY3RfdXJscyI7czoxOiJuIjt9', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YToyOntzOjc6ImVtYWlsZWQiO2E6MDp7fXM6NDc6Ii9Vc2Vycy9qYW1lc21jZmFsbC9Qcm9qZWN0cy9TeW5lcmd5QVUvaW5kZXgucGhwIjtzOjMyOiJiZjViZGMzMGYwYWVmZDI4Mjc0MmU4MDUxNTAyMWY4MSI7fQ==');

/* Table structure for table `exp_snippets` */
DROP TABLE IF EXISTS `exp_snippets`;

CREATE TABLE `exp_snippets` (
  `snippet_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) NOT NULL,
  `snippet_name` varchar(75) NOT NULL,
  `snippet_contents` text,
  PRIMARY KEY (`snippet_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_specialty_templates` */
DROP TABLE IF EXISTS `exp_specialty_templates`;

CREATE TABLE `exp_specialty_templates` (
  `template_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_name` varchar(50) NOT NULL,
  `data_title` varchar(80) NOT NULL,
  `template_data` text NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_specialty_templates` */
INSERT INTO `exp_specialty_templates` VALUES(1, 1, 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>');
INSERT INTO `exp_specialty_templates` VALUES(2, 1, 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=\'content-type\' content=\'text/html; charset={charset}\' />\n\n{meta_refresh}\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>');
INSERT INTO `exp_specialty_templates` VALUES(3, 1, 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}');
INSERT INTO `exp_specialty_templates` VALUES(4, 1, 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n');
INSERT INTO `exp_specialty_templates` VALUES(5, 1, 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}');
INSERT INTO `exp_specialty_templates` VALUES(6, 1, 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}');
INSERT INTO `exp_specialty_templates` VALUES(7, 1, 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(8, 1, 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(9, 1, 'y', 'reset_password_notification', 'New Login Information', '{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(10, 1, 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(11, 1, 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe\'re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(12, 1, 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the \"{mailing_list}\" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}');
INSERT INTO `exp_specialty_templates` VALUES(13, 1, 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}');
INSERT INTO `exp_specialty_templates` VALUES(14, 1, 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}');
INSERT INTO `exp_specialty_templates` VALUES(15, 1, 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled �{message_subject}�.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(16, 1, 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(17, 2, 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>');
INSERT INTO `exp_specialty_templates` VALUES(18, 2, 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=\'content-type\' content=\'text/html; charset={charset}\' />\n\n{meta_refresh}\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>');
INSERT INTO `exp_specialty_templates` VALUES(19, 2, 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}');
INSERT INTO `exp_specialty_templates` VALUES(20, 2, 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n');
INSERT INTO `exp_specialty_templates` VALUES(21, 2, 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}');
INSERT INTO `exp_specialty_templates` VALUES(22, 2, 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}');
INSERT INTO `exp_specialty_templates` VALUES(23, 2, 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(24, 2, 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(25, 2, 'y', 'reset_password_notification', 'New Login Information', '{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(26, 2, 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(27, 2, 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe\'re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(28, 2, 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the \"{mailing_list}\" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}');
INSERT INTO `exp_specialty_templates` VALUES(29, 2, 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}');
INSERT INTO `exp_specialty_templates` VALUES(30, 2, 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}');
INSERT INTO `exp_specialty_templates` VALUES(31, 2, 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled �{message_subject}�.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(32, 2, 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}');

/* Table structure for table `exp_stats` */
DROP TABLE IF EXISTS `exp_stats`;

CREATE TABLE `exp_stats` (
  `stat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `total_members` mediumint(7) NOT NULL DEFAULT '0',
  `recent_member_id` int(10) NOT NULL DEFAULT '0',
  `recent_member` varchar(50) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `most_visitors` mediumint(7) NOT NULL DEFAULT '0',
  `most_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_cache_clear` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`stat_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_stats` */
INSERT INTO `exp_stats` VALUES(1, 1, 1, 1, '96black', 33, 0, 0, 0, 1366066488, 0, 0, 1366071435, 34, 1365643350, 1366073681);
INSERT INTO `exp_stats` VALUES(2, 2, 1, 1, '96black', 0, 0, 0, 0, 0, 0, 0, 1365848246, 6, 1365845962, 1366235389);

/* Table structure for table `exp_status_groups` */
DROP TABLE IF EXISTS `exp_status_groups`;

CREATE TABLE `exp_status_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_status_groups` */
INSERT INTO `exp_status_groups` VALUES(1, 1, 'Statuses');
INSERT INTO `exp_status_groups` VALUES(2, 2, 'Statuses');

/* Table structure for table `exp_status_no_access` */
DROP TABLE IF EXISTS `exp_status_no_access`;

CREATE TABLE `exp_status_no_access` (
  `status_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`status_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_statuses` */
DROP TABLE IF EXISTS `exp_statuses`;

CREATE TABLE `exp_statuses` (
  `status_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `status_order` int(3) unsigned NOT NULL,
  `highlight` varchar(30) NOT NULL,
  PRIMARY KEY (`status_id`),
  KEY `group_id` (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_statuses` */
INSERT INTO `exp_statuses` VALUES(1, 1, 1, 'open', 1, '009933');
INSERT INTO `exp_statuses` VALUES(2, 1, 1, 'closed', 2, '990000');
INSERT INTO `exp_statuses` VALUES(3, 2, 2, 'open', 1, '009933');
INSERT INTO `exp_statuses` VALUES(4, 2, 2, 'closed', 2, '990000');

/* Table structure for table `exp_template_groups` */
DROP TABLE IF EXISTS `exp_template_groups`;

CREATE TABLE `exp_template_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `group_order` int(3) unsigned NOT NULL,
  `is_site_default` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_template_groups` */
INSERT INTO `exp_template_groups` VALUES(1, 1, 'Routing', 1, 'y');
INSERT INTO `exp_template_groups` VALUES(5, 1, 'Home', 3, 'n');
INSERT INTO `exp_template_groups` VALUES(6, 1, 'Products', 4, 'n');
INSERT INTO `exp_template_groups` VALUES(8, 1, 'News', 6, 'n');
INSERT INTO `exp_template_groups` VALUES(9, 1, 'About-us', 7, 'n');
INSERT INTO `exp_template_groups` VALUES(10, 1, 'Contact-us', 8, 'n');
INSERT INTO `exp_template_groups` VALUES(11, 1, 'Services', 9, 'n');
INSERT INTO `exp_template_groups` VALUES(12, 1, 'Common', 10, 'n');
INSERT INTO `exp_template_groups` VALUES(13, 1, 'Featured-products', 10, 'n');
INSERT INTO `exp_template_groups` VALUES(14, 1, 'Case-studies', 11, 'n');
INSERT INTO `exp_template_groups` VALUES(15, 1, 'Search', 11, 'n');

/* Table structure for table `exp_template_member_groups` */
DROP TABLE IF EXISTS `exp_template_member_groups`;

CREATE TABLE `exp_template_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `template_group_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`template_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_template_no_access` */
DROP TABLE IF EXISTS `exp_template_no_access`;

CREATE TABLE `exp_template_no_access` (
  `template_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`template_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_templates` */
DROP TABLE IF EXISTS `exp_templates`;

CREATE TABLE `exp_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `template_name` varchar(50) NOT NULL,
  `save_template_file` char(1) NOT NULL DEFAULT 'n',
  `template_type` varchar(16) NOT NULL DEFAULT 'webpage',
  `template_data` mediumtext,
  `template_notes` text,
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `last_author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cache` char(1) NOT NULL DEFAULT 'n',
  `refresh` int(6) unsigned NOT NULL DEFAULT '0',
  `no_auth_bounce` varchar(50) NOT NULL DEFAULT '',
  `enable_http_auth` char(1) NOT NULL DEFAULT 'n',
  `allow_php` char(1) NOT NULL DEFAULT 'n',
  `php_parse_location` char(1) NOT NULL DEFAULT 'o',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_id`),
  KEY `group_id` (`group_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_templates` */
INSERT INTO `exp_templates` VALUES(1, 1, 1, 'index', 'y', 'webpage', '{embed=\"Common/header\"}\n\n{embed=\"Home/index\"}\n\n{embed=\"Common/footer\"}', '', 1365458557, 1, 'n', 0, '', 'n', 'y', 'o', 7146);
INSERT INTO `exp_templates` VALUES(4, 2, 1, 'monkey-butt', 'y', 'webpage', 'OH SNAP', '', 1355889015, 1, 'n', 0, '', 'n', 'n', 'o', 1);
INSERT INTO `exp_templates` VALUES(9, 1, 5, 'index', 'y', 'webpage', 'Home test', '', 1355945914, 1, 'n', 0, '', 'n', 'y', 'i', 4);
INSERT INTO `exp_templates` VALUES(10, 1, 6, 'index', 'y', 'webpage', '', '', 1355945909, 1, 'n', 0, '', 'n', 'y', 'o', 1385);
INSERT INTO `exp_templates` VALUES(12, 1, 8, 'index', 'y', 'webpage', '', '', 1355945899, 1, 'n', 0, '', 'n', 'n', 'o', 66);
INSERT INTO `exp_templates` VALUES(13, 1, 9, 'index', 'y', 'webpage', '', '', 1355945894, 1, 'n', 0, '', 'n', 'n', 'o', 72);
INSERT INTO `exp_templates` VALUES(14, 1, 10, 'index', 'y', 'webpage', '', '', 1355945889, 1, 'n', 0, '', 'n', 'y', 'i', 106);
INSERT INTO `exp_templates` VALUES(15, 1, 11, 'index', 'y', 'webpage', '', '', 1355945883, 1, 'n', 0, '', 'n', 'n', 'o', 155);
INSERT INTO `exp_templates` VALUES(16, 1, 12, 'index', 'y', 'webpage', '', '', 1355945878, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(17, 1, 12, 'header', 'y', 'webpage', '', '', 1362949938, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(18, 1, 12, 'footer', 'y', 'webpage', '', '', 1355946002, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(19, 1, 12, '_menu', 'y', 'webpage', '', '', 1362965438, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(20, 1, 6, 'product-page', 'y', 'webpage', '', '', 1358280171, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(22, 1, 6, 'category-listing', 'y', 'webpage', 'Category listing!', '', 1358280845, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(23, 1, 6, '_product-nav', 'y', 'webpage', '', '', 1358295203, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(24, 1, 6, 'category-landing', 'y', 'webpage', '', '', 1358365005, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(26, 1, 6, '_category-listing-products-blocks', 'y', 'webpage', '', '', 1362954501, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(27, 1, 8, 'article-list', 'y', 'webpage', '', '', 1362957827, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(28, 1, 8, 'article', 'y', 'webpage', '', '', 1362957835, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(29, 1, 10, '_contact-form', 'y', 'webpage', '', '', 1363570256, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(30, 1, 13, 'index', 'y', 'webpage', '', '', 1363828125, 1, 'n', 0, '', 'n', 'n', 'o', 1);
INSERT INTO `exp_templates` VALUES(31, 1, 13, 'page-intros', 'y', 'webpage', '{exp:channel:entries channel=\"products\" entry_id=\"{embed:product_id}\" limit=\"1\"}\n<div class=\"additionalBlocks\">\n    <ul>\n        {reverse_related_entries id=\"featured_product\"}\n        <li class=\"{switch=\'||third\'}\">\n            <h3><a href=\"{url_title}\">{title}</a></h3>\n            <p>{featured_content_intro}</p>\n            <a href=\"{url_title}\" class=\"readMore\">Read More</a>\n        </li>\n        {/reverse_related_entries}\n    </ul>\n    <div class=\"clear\"></div>\n</div>\n{/exp:channel:entries}', '', 1363827515, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(32, 1, 13, 'page', 'y', 'webpage', '', '', 1363823952, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(33, 1, 6, 'featured_subpage', 'y', 'webpage', '', '', 1363829194, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(34, 1, 14, 'index', 'y', 'webpage', '', '', 1363904378, 1, 'n', 0, '', 'n', 'n', 'o', 52);
INSERT INTO `exp_templates` VALUES(35, 1, 14, 'article-list', 'y', 'webpage', '', '', 1363904459, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(36, 1, 14, 'article', 'y', 'webpage', '', '', 1363904454, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(37, 1, 11, '_services-nav', 'y', 'webpage', '<h3>Services</h3>\n<ul id=\"sideMenu\">\n    <li><a href=\"#\">Roads &amp; Services</a></li>\n    <li><a href=\"#\">Mapping</a></li>\n    <li><a href=\"#\">Lorem Ipsum</a> </li>\n    <li><a href=\"#\">Lorem Ipsum</a> </li>\n</ul>', null, 1365479093, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(38, 1, 11, 'service-landing', 'y', 'webpage', '<div id=\"content\" class=\"std\">\n    <div class=\"middle\">\n        <div id=\"sideBar\">\n            {embed=\"Services/_services-nav\"}\n        </div>\n\n        <div id=\"copy\" class=\"services\">\n            <div class=\"inner\">\n\n                <ul class=\"breadCrumb\">\n                    <li><a href=\"#\">Home</a></li>\n                    <li class=\"current\"><a href=\"#\">Services</a></li>\n                </ul>\n\n                <div class=\"main\">\n                    <h1>Services</h1>\n                </div>\n                <ul class=\"servicesList\">\n                    <li><img src=\"/images/temp/services-temp.jpg\" alt=\"Roads &amp; Services\" />\n                        <h2><a href=\"#\">Roads and Services</a></h2>\n                        <div class=\"excerpt\"><p>One of our core services, Synergy Positioning are experts in asphalt paving technologies. We offer a complete solution for all your paving requirements. Using a host of suppliers including: Moba Matic, Topcon & Roadware, we have complete control of any surfacing project.</p></div>\n                        <a href=\"#\" class=\"readMore\">Read More</a>\n                    </li>\n                    <li class=\"second\"><img src=\"/images/temp/services-temp.jpg\" alt=\"Mapping\" />\n                        <h2><a href=\"#\">Mapping</a></h2>\n                        <div class=\"excerpt\"><p>One of our core services, Synergy Positioning are experts in asphalt paving technologies. We offer a complete solution for all your paving requirements. Using a host of suppliers including: Moba Matic, Topcon & Roadware, we have complete control of any surfacing project.</p></div>\n                        <a href=\"#\" class=\"readMore\">Read More</a>\n                    </li>\n                    <li><img src=\"/images/temp/services-temp.jpg\" alt=\"Lorem Ipsum Dolor\" />\n                        <h2><a href=\"#\">Lorem Ipsum Dolor</a></h2>\n                        <div class=\"excerpt\"><p>One of our core services, Synergy Positioning are experts in asphalt paving technologies. We offer a complete solution for all your paving requirements. Using a host of suppliers including: Moba Matic, Topcon & Roadware, we have complete control of any surfacing project.</p></div>\n                        <a href=\"#\" class=\"readMore\">Read More</a>\n                    </li>\n                    <li class=\"second\"><img src=\"/images/temp/services-temp.jpg\" alt=\"Lorem Ipsum Dolor\" />\n                        <h2><a href=\"#\">Lorem Ipsum Dolor</a></h2>\n                        <div class=\"excerpt\"><p>One of our core services, Synergy Positioning are experts in asphalt paving technologies. We offer a complete solution for all your paving requirements. Using a host of suppliers including: Moba Matic, Topcon & Roadware, we have complete control of any surfacing project.</p></div>\n                        <a href=\"#\" class=\"readMore\">Read More</a>\n                    </li>\n                </ul>	\n\n\n            </div>\n\n\n\n        </div>\n        <div class=\"clear\"></div>\n    </div>\n</div>', null, 1365479411, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(39, 1, 11, 'service-listing', 'y', 'webpage', '\n<div id=\"copy\" class=\"services servicesDetail\">\n    <div class=\"topPageImage\">\n        <img src=\"/images/temp/services-temp-top.jpg\" alt=\"Roads &amp; Services\" />\n    </div>\n\n    <div class=\"inner\">\n\n        <ul class=\"breadCrumb\">\n            <li><a href=\"#\">Home</a></li>\n            <li><a href=\"#\">Services</a></li>\n            <li><a href=\"#\">Roads &amp; Services</a></li>\n            <li class=\"current\"><a href=\"#\">Survey &amp; Design</a></li>\n\n        </ul>\n\n        <div class=\"main\">\n            <h1>Survey &amp; Design</h1>\n            <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus.</p>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. Aliquam eleifend enim eleifend lacus pretium ac pretium risus sodales. Aliquam mi nisi, suscipit in ornare in, tempus a justo. Vivamus porta, justo in sollicitudin vulputate, velit massa congue mi, a vehicula enim metus a lectus. Sed felis tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n            <hr />\n\n            <p><img src=\"/images/temp/services-temp-detail-1.jpg\" alt=\"Survey &amp; Design\" class=\"alignLeft\" /></p>\n\n            <h3>Lorem Ipsum Dolor</h3>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullam]is tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n            <h3>Pellentesque mattis ultrices dapibu uis ullamcorp</h3>\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit m]is tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n\n            <hr />\n            <iframe class=\"alignRight\" width=\"417\" height=\"267\" src=\"http://www.youtube.com/embed/39zYdKe6vLs\" frameborder=\"0\" allowfullscreen></iframe>\n            <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus.</h3>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper</p>\n        </div>\n    </div>\n\n    <div class=\"additionalBlocks\">\n        <ul>\n            <li>\n                <h3><a href=\"#\">Survey &amp; Design</a></h3>\n                <p>Synergy Positioning uses an in-house survey team with many years of experience in the asphalt and roading industries. Our in-house experts in CAD, manages the design and control of final surfacing levels to ensure a smooth and cost effective result, on time and within budget</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n            <li>\n                <h3><a href=\"#\">Paving Control</a></h3>\n                <p>Our paving managers are the best in the industry. Taking full control of the paving crew, we manage the project from start to finish.</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n            <li class=\"third\">\n                <h3><a href=\"#\">Additional Information</a></h3>\n                <p>Sed sagittis tincidunt lobortis. In non ligula a urna blandit tempor. Aenean a leo ut mauris posuere aliquet. Phasellus faucibus convallis aliquet. Proin velit mi, tempus quis varius eu, suscipit sit amet nisl.</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n        </ul>\n        <div class=\"clear\"></div>\n    </div>\n\n\n</div>\n', null, 1365481672, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(40, 1, 11, 'service-page', 'y', 'webpage', '<div id=\"copy\" class=\"services servicesDetail\">\n    <div class=\"topPageImage\">\n        <img src=\"/images/temp/services-temp-top.jpg\" alt=\"Roads &amp; Services\" />\n    </div>\n\n    <div class=\"inner\">\n\n        <ul class=\"breadCrumb\">\n            <li><a href=\"#\">Home</a></li>\n            <li><a href=\"#\">Services</a></li>\n            <li><a href=\"#\">Roads &amp; Services</a></li>\n            <li class=\"current\"><a href=\"#\">Survey &amp; Design</a></li>\n\n        </ul>\n\n        <div class=\"main\">\n            <h1>Survey &amp; Design</h1>\n            <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus.</p>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. Aliquam eleifend enim eleifend lacus pretium ac pretium risus sodales. Aliquam mi nisi, suscipit in ornare in, tempus a justo. Vivamus porta, justo in sollicitudin vulputate, velit massa congue mi, a vehicula enim metus a lectus. Sed felis tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n            <hr />\n\n            <p><img src=\"/images/temp/services-temp-detail-1.jpg\" alt=\"Survey &amp; Design\" class=\"alignLeft\" /></p>\n\n            <h3>Lorem Ipsum Dolor</h3>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullam]is tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n            <h3>Pellentesque mattis ultrices dapibu uis ullamcorp</h3>\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit m]is tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n\n            <hr />\n            <iframe class=\"alignRight\" width=\"417\" height=\"267\" src=\"http://www.youtube.com/embed/39zYdKe6vLs\" frameborder=\"0\" allowfullscreen></iframe>\n            <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus.</h3>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper</p>\n        </div>\n    </div>\n\n    <div class=\"additionalBlocks\">\n        <ul>\n            <li>\n                <h3><a href=\"#\">Survey &amp; Design</a></h3>\n                <p>Synergy Positioning uses an in-house survey team with many years of experience in the asphalt and roading industries. Our in-house experts in CAD, manages the design and control of final surfacing levels to ensure a smooth and cost effective result, on time and within budget</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n            <li>\n                <h3><a href=\"#\">Paving Control</a></h3>\n                <p>Our paving managers are the best in the industry. Taking full control of the paving crew, we manage the project from start to finish.</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n            <li class=\"third\">\n                <h3><a href=\"#\">Additional Information</a></h3>\n                <p>Sed sagittis tincidunt lobortis. In non ligula a urna blandit tempor. Aenean a leo ut mauris posuere aliquet. Phasellus faucibus convallis aliquet. Proin velit mi, tempus quis varius eu, suscipit sit amet nisl.</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n        </ul>\n        <div class=\"clear\"></div>\n    </div>\n\n\n</div>\n', null, 1365481672, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(41, 1, 9, 'page', 'y', 'webpage', '<div id=\"copy\" class=\"about\">\n            <div class=\"topPageImage\">\n                <img src=\"/images/temp/about-temp.jpg\" alt=\"About Synergy Positioning Systems\" />\n            </div>\n\n            <div class=\"inner\">\n\n                <ul class=\"breadCrumb\">\n                    <li><a href=\"#\">Home</a></li>\n                    <li class=\"current\"><a href=\"#\">About Us</a></li>\n\n                </ul>\n\n                <div class=\"main\">\n                    <h1>About Us</h1>\n\n                    <p class=\"intro\">Synergy Positioning Systems Ltd is 100% New Zealand owned and operated. It was formed when two of the industry�s most trusted companies merged in 2006 - Synergy Equipment and Systems Ltd and Geodetic Instruments Ltd. The result is a company that brings you over 30 years experience delivering the most advanced and innovative technologies combined with a commitment to outstanding customer service.</p>\n\n                    <p>Our aim is to make your job faster and easier, more accurate and cost efficient. We\'ll do this by giving you expert and honest advice and by offering you the best solution from our range of innovative positioning systems.</p>\n\n                    <p>Reliability is paramount - equipment breakages cost time and money. There are many low quality brands on the market that just don\'t stand up to the rigours of our construction industry. We only sell reliable brands we can trust and service.</p>\n\n                    <p>Our experienced team are always on hand for after sales support, product training and servicing, giving you the confidence and skills to get the best out of your equipment. Our product range is extensive and always growing.</p>\n\n                    <h2>Why choose synergy?</h2>\n\n\n                </div>\n            </div>\n\n            <div class=\"additionalBlocks\">\n                <ul>\n                    <li>\n                        <h3>Simple really....</h3>\n                        <p>You are dealing with an established company representing world renowned brands that have been tried and tested in increasing productivity and reducing costs.</p>\n\n                    </li>\n                    <li>\n                        <h3>Your profit...</h3>\n                        <p>With our equipment and systems, you gain efficiency &amp; reduce costs, saving you money on all aspects of the job including: fuel, wages, wear &amp; tear, materials &amp; processing. </p>\n\n                    </li>\n                    <li>\n                        <h3>Our Products</h3>\n                        <p>We only represent equipment &amp; brands that we trust. Topcon, world leaders in positioning equipment for over 80 years &amp; well established throughout the world, offers reliable &amp; robust equipment for any application.</p>\n\n                    </li>\n                    <li>\n                        <h3>Our Experience</h3>\n                        <p>Synergy Positioning Systems Ltd is 100% New Zealand owned and operated. Our industry exprience spans more than 30 years. </p>\n\n                    </li>\n                    <li>\n                        <h3>Our Service</h3>\n                        <p>We are commited to our customers. Your success is our reward. Our factory trained technicians and in-house industry experts ensure that what we sell is fully supported.</p>\n\n                    </li>\n                </ul>\n                <div class=\"clear\"></div>\n            </div>\n\n\n\n        </div>', null, 1365632661, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(42, 1, 9, 'management-page', 'y', 'webpage', '<div id=\"copy\" class=\"management\">\n\n    <div class=\"inner\">\n\n        <ul class=\"breadCrumb\">\n            <li><a href=\"#\">Home</a></li>\n            <li><a href=\"#\">About Us</a></li>\n            <li class=\"current\"><a href=\"#\">Management</a></li>\n\n        </ul>\n\n        <div class=\"main\">\n            <h1>Management</h1>\n\n            <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet.</p>\n\n            <hr />\n\n            <h2>New Zealand</h2>\n\n            <ul class=\"profileList\">\n                <li class=\"first\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 3 </strong>\n                        <span class=\"title\">Title</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n\n                <li class=\"second\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"third\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"fourth\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"fifth\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n            </ul>\n\n            <hr />\n\n            <h2>Australia</h2>\n\n            <ul class=\"profileList\">\n                <li class=\"first\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 3 </strong>\n                        <span class=\"title\">Title</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n\n                <li class=\"second\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"third\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"fourth\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"fifth\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n            </ul>\n        </div>\n    </div>\n\n\n\n\n\n</div>', '', 1365635326, 1, 'n', 0, '', 'n', 'n', 'o', 12);
INSERT INTO `exp_templates` VALUES(43, 1, 15, 'index', 'y', 'webpage', '{exp:search:simple_form channel=\"news\"}\n        <p>\n                <label for=\"keywords\">Search:</label><br>\n                <input type=\"text\" name=\"keywords\" id=\"keywords\" value=\"\" size=\"18\" maxlength=\"100\">\n        </p>\n        <p>\n                <a href=\"{path=\'search/index\'}\">Advanced Search</a>\n        </p>\n        <p>\n                <input type=\"submit\" value=\"submit\" class=\"submit\">\n        </p>\n{/exp:search:simple_form}', '', 1365964420, 1, 'n', 0, '', 'n', 'n', 'o', 5);
INSERT INTO `exp_templates` VALUES(44, 1, 15, 'results', 'y', 'webpage', '{exp:search:simple_form channel=\"products|services\" result_page=\"search/results\"}\n        <p>\n                <label for=\"keywords\">Search:</label><br>\n                <input type=\"text\" name=\"keywords\" id=\"keywords\" value=\"\" size=\"18\" maxlength=\"100\">\n        </p>\n        <p>\n                <a href=\"{path=\'search/index\'}\">Advanced Search</a>\n        </p>\n        <p>\n                <input type=\"submit\" value=\"submit\" class=\"submit\">\n        </p>\n{/exp:search:simple_form}\n\n<table border=\"0\" cellpadding=\"6\" cellspacing=\"1\" width=\"100%\">\n    <tr>\n        <th>{lang:title}</th>\n        <th>{lang:excerpt}</th>\n        <th>{lang:author}</th>\n        <th>{lang:date}</th>\n        <th>{lang:total_comments}</th>\n        <th>{lang:recent_comments}</th>\n    </tr>\n\n{exp:search:search_results switch=\"resultRowOne|resultRowTwo\"}\n\n    <tr class=\"{switch}\">\n        <td width=\"30%\" valign=\"top\"><b><a href=\"{auto_path}\">{title}</a></b></td>\n        <td width=\"30%\" valign=\"top\">{excerpt}</td>\n        <td width=\"10%\" valign=\"top\"><a href=\"{member_path=\'member/index\'}\">{author}</a></td>\n        <td width=\"10%\" valign=\"top\">{entry_date format=\"%m/%d/%y\"}</td>\n        <td width=\"10%\" valign=\"top\">{comment_total}</td>\n        <td width=\"10%\" valign=\"top\">{recent_comment_date format=\"%m/%d/%y\"}</td>\n    </tr>\n\n    {if count == total_results}\n        </table>\n    {/if}\n\n    {paginate}\n        <p>Page {current_page} of {total_pages} pages {pagination_links}</p>\n    {/paginate}\n\n{/exp:search:search_results}', '', 1365965029, 1, 'n', 0, '', 'n', 'n', 'o', 8);
INSERT INTO `exp_templates` VALUES(45, 1, 15, '_search-form', 'y', 'webpage', '{exp:search:simple_form channel=\"products|services\" result_page=\"search/_results\"}\n        <p>\n                <label for=\"keywords\">Search:</label><br>\n                <input type=\"text\" name=\"keywords\" id=\"keywords\" value=\"\" size=\"18\" maxlength=\"100\">\n        </p>\n        <p>\n                <input type=\"submit\" value=\"submit\" class=\"submit\">\n        </p>\n{/exp:search:simple_form}', '', 1365965186, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(46, 1, 6, '_enquiry-form', 'y', 'webpage', 'f', '', 1365966324, 1, 'n', 0, '', 'n', 'y', 'i', 0);

/* Table structure for table `exp_throttle` */
DROP TABLE IF EXISTS `exp_throttle`;

CREATE TABLE `exp_throttle` (
  `throttle_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL,
  `locked_out` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`throttle_id`),
  KEY `ip_address` (`ip_address`),
  KEY `last_activity` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_upload_no_access` */
DROP TABLE IF EXISTS `exp_upload_no_access`;

CREATE TABLE `exp_upload_no_access` (
  `upload_id` int(6) unsigned NOT NULL,
  `upload_loc` varchar(3) NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`upload_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_upload_prefs` */
DROP TABLE IF EXISTS `exp_upload_prefs`;

CREATE TABLE `exp_upload_prefs` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `server_path` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL,
  `allowed_types` varchar(3) NOT NULL DEFAULT 'img',
  `max_size` varchar(16) DEFAULT NULL,
  `max_height` varchar(6) DEFAULT NULL,
  `max_width` varchar(6) DEFAULT NULL,
  `properties` varchar(120) DEFAULT NULL,
  `pre_format` varchar(120) DEFAULT NULL,
  `post_format` varchar(120) DEFAULT NULL,
  `file_properties` varchar(120) DEFAULT NULL,
  `file_pre_format` varchar(120) DEFAULT NULL,
  `file_post_format` varchar(120) DEFAULT NULL,
  `cat_group` varchar(255) DEFAULT NULL,
  `batch_location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_upload_prefs` */
INSERT INTO `exp_upload_prefs` VALUES(1, 1, 'Product Images', '/Users/jamesmcfall/Projects/Synergy/uploads/product-images/', 'http://synergy.local/uploads/product-images/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(2, 1, 'Product Downloads', '/Users/jamesmcfall/Projects/Synergy/uploads/product-downloads/', 'http://synergy.local/uploads/product-downloads/', 'all', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(3, 1, 'Category Images', '/Users/jamesmcfall/Projects/Synergy/uploads/category-images/', 'http://synergy.local/uploads/category-images/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(4, 1, 'News Article Banner Image', '/Users/jamesmcfall/Projects/Synergy/uploads/news-images/', 'http://synergy.local/uploads/news-images/', 'img', '', '373', '866', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(5, 1, 'Case Study Banner Image', '/Users/jamesmcfall/Projects/Synergy/uploads/case-study-image/', 'http://synergy.local/uploads/case-study-image/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(6, 1, 'Home Page Banners', '/Users/jamesmcfall/Projects/Synergy/uploads/home-page-banners/', 'http://synergy.local/uploads/home-page-banners/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(7, 1, 'Services Banners', '/Users/jamesmcfall/Projects/Synergy/uploads/services-banners/', 'http://synergy.local/uploads/services-banners/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(8, 1, 'About Banners', '/Users/jamesmcfall/Projects/Synergy/uploads/about-banners/', 'http://synergy.local/uploads/about-banners/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(9, 1, 'Staff Pictures', '/Users/jamesmcfall/Projects/Synergy/uploads/staff-pictures/', 'http://synergy.local/uploads/staff-pictures/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(10, 1, 'Manufacturer Images', '/Users/jamesmcfall/Projects/Synergy/uploads/manufacturer-images/', 'http://synergy.local/uploads/manufacturer-images/', 'img', '', '', '', '', '', '', '', '', '', '', null);

/* Table structure for table `exp_wygwam_configs` */
DROP TABLE IF EXISTS `exp_wygwam_configs`;

CREATE TABLE `exp_wygwam_configs` (
  `config_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `config_name` varchar(32) DEFAULT NULL,
  `settings` text,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_wygwam_configs` */
INSERT INTO `exp_wygwam_configs` VALUES(1, 'Basic', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTA6e2k6MDtzOjQ6IkJvbGQiO2k6MTtzOjY6Ikl0YWxpYyI7aToyO3M6OToiVW5kZXJsaW5lIjtpOjM7czo2OiJTdHJpa2UiO2k6NDtzOjEyOiJOdW1iZXJlZExpc3QiO2k6NTtzOjEyOiJCdWxsZXRlZExpc3QiO2k6NjtzOjQ6IkxpbmsiO2k6NztzOjY6IlVubGluayI7aTo4O3M6NjoiQW5jaG9yIjtpOjk7czo1OiJBYm91dCI7fXM6NjoiaGVpZ2h0IjtzOjM6IjIwMCI7czoxNDoicmVzaXplX2VuYWJsZWQiO3M6MToieSI7czoxMToiY29udGVudHNDc3MiO2E6MDp7fXM6MTA6InVwbG9hZF9kaXIiO3M6MDoiIjt9');
INSERT INTO `exp_wygwam_configs` VALUES(2, 'Full', 'YTo1OntzOjc6InRvb2xiYXIiO2E6Njc6e2k6MDtzOjY6IlNvdXJjZSI7aToxO3M6NDoiU2F2ZSI7aToyO3M6NzoiTmV3UGFnZSI7aTozO3M6NzoiUHJldmlldyI7aTo0O3M6OToiVGVtcGxhdGVzIjtpOjU7czozOiJDdXQiO2k6NjtzOjQ6IkNvcHkiO2k6NztzOjU6IlBhc3RlIjtpOjg7czo5OiJQYXN0ZVRleHQiO2k6OTtzOjEzOiJQYXN0ZUZyb21Xb3JkIjtpOjEwO3M6NToiUHJpbnQiO2k6MTE7czoxMjoiU3BlbGxDaGVja2VyIjtpOjEyO3M6NToiU2NheXQiO2k6MTM7czo0OiJVbmRvIjtpOjE0O3M6NDoiUmVkbyI7aToxNTtzOjQ6IkZpbmQiO2k6MTY7czo3OiJSZXBsYWNlIjtpOjE3O3M6OToiU2VsZWN0QWxsIjtpOjE4O3M6MTI6IlJlbW92ZUZvcm1hdCI7aToxOTtzOjQ6IkZvcm0iO2k6MjA7czo4OiJDaGVja2JveCI7aToyMTtzOjU6IlJhZGlvIjtpOjIyO3M6OToiVGV4dEZpZWxkIjtpOjIzO3M6ODoiVGV4dGFyZWEiO2k6MjQ7czo2OiJTZWxlY3QiO2k6MjU7czo2OiJCdXR0b24iO2k6MjY7czoxMToiSW1hZ2VCdXR0b24iO2k6Mjc7czoxMToiSGlkZGVuRmllbGQiO2k6Mjg7czoxOiIvIjtpOjI5O3M6NDoiQm9sZCI7aTozMDtzOjY6Ikl0YWxpYyI7aTozMTtzOjk6IlVuZGVybGluZSI7aTozMjtzOjY6IlN0cmlrZSI7aTozMztzOjk6IlN1YnNjcmlwdCI7aTozNDtzOjExOiJTdXBlcnNjcmlwdCI7aTozNTtzOjEyOiJOdW1iZXJlZExpc3QiO2k6MzY7czoxMjoiQnVsbGV0ZWRMaXN0IjtpOjM3O3M6NzoiT3V0ZGVudCI7aTozODtzOjY6IkluZGVudCI7aTozOTtzOjEwOiJCbG9ja3F1b3RlIjtpOjQwO3M6OToiQ3JlYXRlRGl2IjtpOjQxO3M6MTE6Ikp1c3RpZnlMZWZ0IjtpOjQyO3M6MTM6Ikp1c3RpZnlDZW50ZXIiO2k6NDM7czoxMjoiSnVzdGlmeVJpZ2h0IjtpOjQ0O3M6MTI6Ikp1c3RpZnlCbG9jayI7aTo0NTtzOjQ6IkxpbmsiO2k6NDY7czo2OiJVbmxpbmsiO2k6NDc7czo2OiJBbmNob3IiO2k6NDg7czo1OiJJbWFnZSI7aTo0OTtzOjU6IkZsYXNoIjtpOjUwO3M6NToiVGFibGUiO2k6NTE7czoxNDoiSG9yaXpvbnRhbFJ1bGUiO2k6NTI7czo2OiJTbWlsZXkiO2k6NTM7czoxMToiU3BlY2lhbENoYXIiO2k6NTQ7czo5OiJQYWdlQnJlYWsiO2k6NTU7czo4OiJSZWFkTW9yZSI7aTo1NjtzOjEwOiJFbWJlZE1lZGlhIjtpOjU3O3M6MToiLyI7aTo1ODtzOjY6IlN0eWxlcyI7aTo1OTtzOjY6IkZvcm1hdCI7aTo2MDtzOjQ6IkZvbnQiO2k6NjE7czo4OiJGb250U2l6ZSI7aTo2MjtzOjk6IlRleHRDb2xvciI7aTo2MztzOjc6IkJHQ29sb3IiO2k6NjQ7czo4OiJNYXhpbWl6ZSI7aTo2NTtzOjEwOiJTaG93QmxvY2tzIjtpOjY2O3M6NToiQWJvdXQiO31zOjY6ImhlaWdodCI7czozOiIyMDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ==');
INSERT INTO `exp_wygwam_configs` VALUES(3, 'News', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTE6e2k6MDtzOjY6IkZvcm1hdCI7aToxO3M6NDoiQm9sZCI7aToyO3M6NjoiSXRhbGljIjtpOjM7czo5OiJVbmRlcmxpbmUiO2k6NDtzOjY6IlN0cmlrZSI7aTo1O3M6MTI6Ik51bWJlcmVkTGlzdCI7aTo2O3M6MTI6IkJ1bGxldGVkTGlzdCI7aTo3O3M6NDoiTGluayI7aTo4O3M6NjoiVW5saW5rIjtpOjk7czo2OiJBbmNob3IiO2k6MTA7czo1OiJBYm91dCI7fXM6NjoiaGVpZ2h0IjtzOjM6IjIwMCI7czoxNDoicmVzaXplX2VuYWJsZWQiO3M6MToieSI7czoxMToiY29udGVudHNDc3MiO2E6MDp7fXM6MTA6InVwbG9hZF9kaXIiO3M6MDoiIjt9');
INSERT INTO `exp_wygwam_configs` VALUES(4, 'Featured Products Content', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTg6e2k6MDtzOjY6IkZvcm1hdCI7aToxO3M6MTE6Ikp1c3RpZnlMZWZ0IjtpOjI7czoxMzoiSnVzdGlmeUNlbnRlciI7aTozO3M6MTI6Ikp1c3RpZnlSaWdodCI7aTo0O3M6MTI6Ikp1c3RpZnlCbG9jayI7aTo1O3M6NDoiQm9sZCI7aTo2O3M6NjoiSXRhbGljIjtpOjc7czo5OiJVbmRlcmxpbmUiO2k6ODtzOjY6IlN0cmlrZSI7aTo5O3M6MTI6Ik51bWJlcmVkTGlzdCI7aToxMDtzOjEyOiJCdWxsZXRlZExpc3QiO2k6MTE7czo1OiJJbWFnZSI7aToxMjtzOjU6IkZsYXNoIjtpOjEzO3M6MTA6IkVtYmVkTWVkaWEiO2k6MTQ7czo1OiJBYm91dCI7aToxNTtzOjQ6IkxpbmsiO2k6MTY7czo2OiJVbmxpbmsiO2k6MTc7czo2OiJBbmNob3IiO31zOjY6ImhlaWdodCI7czozOiIyMDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ==');
INSERT INTO `exp_wygwam_configs` VALUES(5, 'Synergy Default', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTk6e2k6MDtzOjY6IkZvcm1hdCI7aToxO3M6NDoiQm9sZCI7aToyO3M6NjoiSXRhbGljIjtpOjM7czo5OiJVbmRlcmxpbmUiO2k6NDtzOjY6IlN0cmlrZSI7aTo1O3M6MTE6Ikp1c3RpZnlMZWZ0IjtpOjY7czoxMzoiSnVzdGlmeUNlbnRlciI7aTo3O3M6MTI6Ikp1c3RpZnlSaWdodCI7aTo4O3M6MTI6Ikp1c3RpZnlCbG9jayI7aTo5O3M6MTI6Ik51bWJlcmVkTGlzdCI7aToxMDtzOjEyOiJCdWxsZXRlZExpc3QiO2k6MTE7czo0OiJMaW5rIjtpOjEyO3M6NjoiVW5saW5rIjtpOjEzO3M6NjoiQW5jaG9yIjtpOjE0O3M6NToiSW1hZ2UiO2k6MTU7czo1OiJGbGFzaCI7aToxNjtzOjE0OiJIb3Jpem9udGFsUnVsZSI7aToxNztzOjEwOiJFbWJlZE1lZGlhIjtpOjE4O3M6NToiQWJvdXQiO31zOjY6ImhlaWdodCI7czozOiI0MDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ==');


/* Table structure for table `exp_accessories` */
DROP TABLE IF EXISTS `exp_accessories`;

CREATE TABLE `exp_accessories` (
  `accessory_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(75) NOT NULL DEFAULT '',
  `member_groups` varchar(50) NOT NULL DEFAULT 'all',
  `controllers` text,
  `accessory_version` varchar(12) NOT NULL,
  PRIMARY KEY (`accessory_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_accessories` */
INSERT INTO `exp_accessories` VALUES(1, 'Expressionengine_info_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.0');
INSERT INTO `exp_accessories` VALUES(2, 'Mx_cloner_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|sites|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.0.3');

/* Table structure for table `exp_actions` */
DROP TABLE IF EXISTS `exp_actions`;

CREATE TABLE `exp_actions` (
  `action_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_actions` */
INSERT INTO `exp_actions` VALUES(1, 'Comment', 'insert_new_comment');
INSERT INTO `exp_actions` VALUES(2, 'Comment_mcp', 'delete_comment_notification');
INSERT INTO `exp_actions` VALUES(3, 'Comment', 'comment_subscribe');
INSERT INTO `exp_actions` VALUES(4, 'Comment', 'edit_comment');
INSERT INTO `exp_actions` VALUES(5, 'Email', 'send_email');
INSERT INTO `exp_actions` VALUES(6, 'Safecracker', 'submit_entry');
INSERT INTO `exp_actions` VALUES(7, 'Safecracker', 'combo_loader');
INSERT INTO `exp_actions` VALUES(8, 'Search', 'do_search');
INSERT INTO `exp_actions` VALUES(9, 'Channel', 'insert_new_entry');
INSERT INTO `exp_actions` VALUES(10, 'Channel', 'filemanager_endpoint');
INSERT INTO `exp_actions` VALUES(11, 'Channel', 'smiley_pop');
INSERT INTO `exp_actions` VALUES(12, 'Member', 'registration_form');
INSERT INTO `exp_actions` VALUES(13, 'Member', 'register_member');
INSERT INTO `exp_actions` VALUES(14, 'Member', 'activate_member');
INSERT INTO `exp_actions` VALUES(15, 'Member', 'member_login');
INSERT INTO `exp_actions` VALUES(16, 'Member', 'member_logout');
INSERT INTO `exp_actions` VALUES(17, 'Member', 'retrieve_password');
INSERT INTO `exp_actions` VALUES(18, 'Member', 'reset_password');
INSERT INTO `exp_actions` VALUES(19, 'Member', 'send_member_email');
INSERT INTO `exp_actions` VALUES(20, 'Member', 'update_un_pw');
INSERT INTO `exp_actions` VALUES(21, 'Member', 'member_search');
INSERT INTO `exp_actions` VALUES(22, 'Member', 'member_delete');
INSERT INTO `exp_actions` VALUES(23, 'Rte', 'get_js');
INSERT INTO `exp_actions` VALUES(24, 'Playa_mcp', 'filter_entries');

/* Table structure for table `exp_captcha` */
DROP TABLE IF EXISTS `exp_captcha`;

CREATE TABLE `exp_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_categories` */
DROP TABLE IF EXISTS `exp_categories`;

CREATE TABLE `exp_categories` (
  `cat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `parent_id` int(4) unsigned NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_url_title` varchar(75) NOT NULL,
  `cat_description` text,
  `cat_image` varchar(120) DEFAULT NULL,
  `cat_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `group_id` (`group_id`),
  KEY `cat_name` (`cat_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_categories` */
INSERT INTO `exp_categories` VALUES(1, 1, 1, 0, 'UAV', 'uav', '', '{filedir_3}cat-image.jpg', 3);
INSERT INTO `exp_categories` VALUES(2, 1, 1, 0, 'Optical', 'optical', 'Category description here', '{filedir_3}cat-image.jpg', 2);
INSERT INTO `exp_categories` VALUES(3, 1, 1, 2, 'Total Station', 'total-station', '', '{filedir_3}cat-image.jpg', 3);
INSERT INTO `exp_categories` VALUES(4, 1, 1, 3, 'Robotic', 'robotic', '', '{filedir_3}cat-image.jpg', 2);
INSERT INTO `exp_categories` VALUES(5, 1, 1, 3, 'Windows Based', 'windows-based', '', '{filedir_3}cat-image.jpg', 3);
INSERT INTO `exp_categories` VALUES(6, 1, 1, 3, 'Construction', 'construction', '', '{filedir_3}cat-image.jpg', 1);
INSERT INTO `exp_categories` VALUES(7, 1, 1, 0, 'GPS Equipment', 'gps-equipment', 'Lorem ipsum stuff', '{filedir_3}cat-image.jpg', 1);
INSERT INTO `exp_categories` VALUES(8, 1, 1, 2, 'Levels', 'levels', 'Test content', '{filedir_3}cat-image.jpg', 1);
INSERT INTO `exp_categories` VALUES(9, 1, 1, 2, 'Theyodolites', 'theyodolites', 'Test content', '{filedir_3}cat-image.jpg', 2);

/* Table structure for table `exp_category_field_data` */
DROP TABLE IF EXISTS `exp_category_field_data`;

CREATE TABLE `exp_category_field_data` (
  `cat_id` int(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_category_field_data` */
INSERT INTO `exp_category_field_data` VALUES(1, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(2, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(3, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(4, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(5, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(6, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(7, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(8, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(9, 1, 1);

/* Table structure for table `exp_category_fields` */
DROP TABLE IF EXISTS `exp_category_fields`;

CREATE TABLE `exp_category_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `field_label` varchar(50) NOT NULL DEFAULT '',
  `field_type` varchar(12) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_maxl` smallint(3) NOT NULL DEFAULT '128',
  `field_ta_rows` tinyint(2) NOT NULL DEFAULT '8',
  `field_default_fmt` varchar(40) NOT NULL DEFAULT 'none',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`field_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_category_groups` */
DROP TABLE IF EXISTS `exp_category_groups`;

CREATE TABLE `exp_category_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `sort_order` char(1) NOT NULL DEFAULT 'a',
  `exclude_group` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `can_edit_categories` text,
  `can_delete_categories` text,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_category_groups` */
INSERT INTO `exp_category_groups` VALUES(1, 1, 'Product Categories', 'a', 0, 'all', '', '');

/* Table structure for table `exp_category_posts` */
DROP TABLE IF EXISTS `exp_category_posts`;

CREATE TABLE `exp_category_posts` (
  `entry_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_category_posts` */
INSERT INTO `exp_category_posts` VALUES(1, 2);
INSERT INTO `exp_category_posts` VALUES(1, 3);
INSERT INTO `exp_category_posts` VALUES(1, 6);
INSERT INTO `exp_category_posts` VALUES(2, 2);
INSERT INTO `exp_category_posts` VALUES(2, 3);
INSERT INTO `exp_category_posts` VALUES(2, 6);
INSERT INTO `exp_category_posts` VALUES(3, 2);
INSERT INTO `exp_category_posts` VALUES(3, 3);
INSERT INTO `exp_category_posts` VALUES(3, 6);

/* Table structure for table `exp_channel_data` */
DROP TABLE IF EXISTS `exp_channel_data`;

CREATE TABLE `exp_channel_data` (
  `entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `field_id_1` text,
  `field_ft_1` tinytext,
  `field_id_2` text,
  `field_ft_2` tinytext,
  `field_id_3` text,
  `field_ft_3` tinytext,
  `field_id_4` text,
  `field_ft_4` tinytext,
  `field_id_5` text,
  `field_ft_5` tinytext,
  `field_id_6` text,
  `field_ft_6` tinytext,
  `field_id_7` text,
  `field_ft_7` tinytext,
  `field_id_8` text,
  `field_ft_8` tinytext,
  `field_id_9` text,
  `field_ft_9` tinytext,
  `field_id_10` text,
  `field_ft_10` tinytext,
  `field_id_11` text,
  `field_ft_11` tinytext,
  `field_id_12` text,
  `field_ft_12` tinytext,
  `field_id_13` text,
  `field_ft_13` tinytext,
  `field_id_14` text,
  `field_ft_14` tinytext,
  `field_id_15` text,
  `field_ft_15` tinytext,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_channel_data` */
INSERT INTO `exp_channel_data` VALUES(1, 1, 1, 'NZD\nAUD\n', 'none', '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '', 'none', '', 'xhtml', '1', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(2, 1, 1, 'NZD\nAUD\n', 'none', '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '', 'none', '', 'xhtml', '1', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(3, 1, 1, 'NZD\nAUD\n', 'none', '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '', 'none', '', 'xhtml', '1', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(4, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(5, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(6, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(7, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(8, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(9, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(10, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(11, 1, 4, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quam justo, egestas sed tincidunt id, feugiat eu urna. Suspendisse ornare, magna cursus tristique ullamcorper, massa velit ultricies metus, eu ultrices massa eros nec tortor. Sed auctor vehicula quam, ac condimentum mauris tristique non. Sed sed sodales nisi. Aliquam erat volutpat. Vivamus sit amet felis et urna lacinia ullamcorper eget et metus. Nunc laoreet eleifend molestie.</p>', 'none', '1', 'none');

/* Table structure for table `exp_channel_entries_autosave` */
DROP TABLE IF EXISTS `exp_channel_entries_autosave`;

CREATE TABLE `exp_channel_entries_autosave` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original_entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  `entry_data` text,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_channel_fields` */
DROP TABLE IF EXISTS `exp_channel_fields`;

CREATE TABLE `exp_channel_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL,
  `field_label` varchar(50) NOT NULL,
  `field_instructions` text,
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_pre_populate` char(1) NOT NULL DEFAULT 'n',
  `field_pre_channel_id` int(6) unsigned DEFAULT NULL,
  `field_pre_field_id` int(6) unsigned DEFAULT NULL,
  `field_related_to` varchar(12) NOT NULL DEFAULT 'channel',
  `field_related_id` int(6) unsigned NOT NULL DEFAULT '0',
  `field_related_orderby` varchar(12) NOT NULL DEFAULT 'date',
  `field_related_sort` varchar(4) NOT NULL DEFAULT 'desc',
  `field_related_max` smallint(4) NOT NULL DEFAULT '0',
  `field_ta_rows` tinyint(2) DEFAULT '8',
  `field_maxl` smallint(3) DEFAULT NULL,
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_search` char(1) NOT NULL DEFAULT 'n',
  `field_is_hidden` char(1) NOT NULL DEFAULT 'n',
  `field_fmt` varchar(40) NOT NULL DEFAULT 'xhtml',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_order` int(3) unsigned NOT NULL,
  `field_content_type` varchar(20) NOT NULL DEFAULT 'any',
  `field_settings` text,
  PRIMARY KEY (`field_id`),
  KEY `group_id` (`group_id`),
  KEY `field_type` (`field_type`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_channel_fields` */
INSERT INTO `exp_channel_fields` VALUES(1, 1, 1, 'product_prices', 'Product Prices', 'Enter the AU and NZ prices for this product in here and they will be dynamically output to the customer based on the website they are viewing.', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjIiO3M6ODoibWF4X3Jvd3MiO3M6MToiMiI7czo3OiJjb2xfaWRzIjthOjI6e2k6MDtzOjE6IjEiO2k6MTtzOjE6IjIiO319');
INSERT INTO `exp_channel_fields` VALUES(2, 1, 1, 'product_description', 'Product Description', 'On the product page, only the first couple of paragraphs will be shown until the user presses \"read more\"', 'wygwam', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIyIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(3, 1, 1, 'technical_specifications', 'Technical Specifications', 'Below you can create the technical specifications section for this product. Once a title is specified in the left column (i.e. Measurement Range) you can create a table in the right column of specifications relevant to this title. ', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MToiMyI7aToxO3M6MToiNCI7fX0=');
INSERT INTO `exp_channel_fields` VALUES(4, 1, 1, 'product_videos', 'Product Videos', 'Use this field to embed a Youtube video and give a title/description to the video. Any videos will appear under the \"Videos\" tab for this product.', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 4, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mzp7aTowO2k6NjtpOjE7aTo3O2k6MjtpOjg7fX0=');
INSERT INTO `exp_channel_fields` VALUES(5, 1, 1, 'rental_sections', 'Rental Sections', 'Add as many paragraphs to the rental section as you want.', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO2k6OTtpOjE7aToxMDt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(6, 1, 1, 'rental_pricing_nz', 'Rental Pricing (NZ)', 'This field controls the contents of the \"Availability & Costs\" section under the rental tab (for the NZ site).', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 6, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMTEiO2k6MTtzOjI6IjEyIjt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(7, 1, 1, 'rental_pricing_au', 'Rental Pricing (AU)', 'This field controls the contents of the \"Availability & Costs\" section under the rental tab (for the AU site).', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 7, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMTMiO2k6MTtzOjI6IjE0Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(8, 1, 1, 'featured_carousel_images', 'Featured Carousel Images', 'If this product is to be a featured product, you can upload many banner images to appear at the top of its featured page.\n\nThe banner will cycle between these images.', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 8, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6MTp7aTowO2k6MTU7fX0=');
INSERT INTO `exp_channel_fields` VALUES(9, 1, 1, 'is_featured_product', 'Is Featured Product?', 'If you want this product to be a featured product, tick this checkbox.\n\nPlease make sure you have created some featured product entries for this product first (in the publish menu).', 'pt_checkboxes', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'xhtml', 'n', 9, 'any', 'YTo3OntzOjc6Im9wdGlvbnMiO2E6MTp7czozOiJZZXMiO3M6MzoiWWVzIjt9czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(10, 1, 1, 'product_images', 'Product Images', '', 'matrix', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 10, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6MTp7aTowO3M6MjoiMTYiO319');
INSERT INTO `exp_channel_fields` VALUES(11, 1, 1, 'product_downloads', 'Product Downloads', 'Upload files to be available in the \"downloads\" tab', 'matrix', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 11, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMTciO2k6MTtzOjI6IjE4Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(12, 1, 3, 'article_content', 'Article Content', 'This is the main body content of the news item. Please note the first paragraph will be automatically made bold.', 'wygwam', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(13, 1, 3, 'article_banner_image', 'Article Banner Image', 'This is the image that appears at the top of the news article and on the news page. Please note the images should be exactly 866px by 373px and will have a thumbnail automatically cropped to 396px by 88px for the news page.', 'file', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiNCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(14, 1, 4, 'contact_introduction', 'Contact Introduction', 'The initial content before the branch information.', 'wygwam', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(15, 1, 4, 'branches', 'Branches', '', 'matrix', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6NTp7aTowO3M6MjoiMTkiO2k6MTtzOjI6IjIwIjtpOjI7czoyOiIyMSI7aTozO3M6MjoiMjIiO2k6NDtzOjI6IjIzIjt9fQ==');

/* Table structure for table `exp_channel_member_groups` */
DROP TABLE IF EXISTS `exp_channel_member_groups`;

CREATE TABLE `exp_channel_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `channel_id` int(6) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_channel_titles` */
DROP TABLE IF EXISTS `exp_channel_titles`;

CREATE TABLE `exp_channel_titles` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_channel_titles` */
INSERT INTO `exp_channel_titles` VALUES(1, 1, 1, 1, 0, null, '127.0.0.1', 'Topcon GTS-100N Construction', 'topcon-gts-100n-construction', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358293507, 'n', '2013', '01', '16', 0, 0, 20130310211708, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(2, 1, 1, 1, 0, null, '127.0.0.1', 'Topcon GTS-100N Robotic', 'topcon-gts-100n-robotic', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358293534, 'n', '2013', '01', '16', 0, 0, 20130116124534, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(3, 1, 1, 1, 0, null, '127.0.0.1', 'Topcon GTS-100N Windows Based', 'topcon-gts-100n-windows-based', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358293556, 'n', '2013', '01', '16', 0, 0, 20130116124556, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(4, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 1', 'news-article-1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961462, 'n', '2013', '03', '11', 0, 0, 20130311132422, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(5, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 2', 'news-article-2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961474, 'n', '2013', '03', '11', 0, 0, 20130311132434, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(6, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 6', 'news-article-6', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961462, 'n', '2013', '03', '11', 0, 0, 20130311003823, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(7, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 5', 'news-article-5', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961483, 'n', '2013', '03', '11', 0, 0, 20130311132443, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(8, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 4', 'news-article-4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961442, 'n', '2013', '03', '11', 0, 0, 20130311132402, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(9, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 3', 'news-article-3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961452, 'n', '2013', '03', '11', 0, 0, 20130311132412, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(10, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 7', 'news-article-7', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961463, 'n', '2013', '03', '11', 0, 0, 20130311132423, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(11, 1, 4, 1, 0, null, '127.0.0.1', 'Contact Us', 'contact-us', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362967322, 'n', '2013', '03', '11', 0, 0, 20130311150202, 0, 0);

/* Table structure for table `exp_channels` */
DROP TABLE IF EXISTS `exp_channels`;

CREATE TABLE `exp_channels` (
  `channel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_name` varchar(40) NOT NULL,
  `channel_title` varchar(100) NOT NULL,
  `channel_url` varchar(100) NOT NULL,
  `channel_description` varchar(225) DEFAULT NULL,
  `channel_lang` varchar(12) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `cat_group` varchar(255) DEFAULT NULL,
  `status_group` int(4) unsigned DEFAULT NULL,
  `deft_status` varchar(50) NOT NULL DEFAULT 'open',
  `field_group` int(4) unsigned DEFAULT NULL,
  `search_excerpt` int(4) unsigned DEFAULT NULL,
  `deft_category` varchar(60) DEFAULT NULL,
  `deft_comments` char(1) NOT NULL DEFAULT 'y',
  `channel_require_membership` char(1) NOT NULL DEFAULT 'y',
  `channel_max_chars` int(5) unsigned DEFAULT NULL,
  `channel_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `channel_allow_img_urls` char(1) NOT NULL DEFAULT 'y',
  `channel_auto_link_urls` char(1) NOT NULL DEFAULT 'n',
  `channel_notify` char(1) NOT NULL DEFAULT 'n',
  `channel_notify_emails` varchar(255) DEFAULT NULL,
  `comment_url` varchar(80) DEFAULT NULL,
  `comment_system_enabled` char(1) NOT NULL DEFAULT 'y',
  `comment_require_membership` char(1) NOT NULL DEFAULT 'n',
  `comment_use_captcha` char(1) NOT NULL DEFAULT 'n',
  `comment_moderate` char(1) NOT NULL DEFAULT 'n',
  `comment_max_chars` int(5) unsigned DEFAULT '5000',
  `comment_timelock` int(5) unsigned NOT NULL DEFAULT '0',
  `comment_require_email` char(1) NOT NULL DEFAULT 'y',
  `comment_text_formatting` char(5) NOT NULL DEFAULT 'xhtml',
  `comment_html_formatting` char(4) NOT NULL DEFAULT 'safe',
  `comment_allow_img_urls` char(1) NOT NULL DEFAULT 'n',
  `comment_auto_link_urls` char(1) NOT NULL DEFAULT 'y',
  `comment_notify` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_authors` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_emails` varchar(255) DEFAULT NULL,
  `comment_expiration` int(4) unsigned NOT NULL DEFAULT '0',
  `search_results_url` varchar(80) DEFAULT NULL,
  `ping_return_url` varchar(80) DEFAULT NULL,
  `show_button_cluster` char(1) NOT NULL DEFAULT 'y',
  `rss_url` varchar(80) DEFAULT NULL,
  `enable_versioning` char(1) NOT NULL DEFAULT 'n',
  `max_revisions` smallint(4) unsigned NOT NULL DEFAULT '10',
  `default_entry_title` varchar(100) DEFAULT NULL,
  `url_title_prefix` varchar(80) DEFAULT NULL,
  `live_look_template` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`),
  KEY `cat_group` (`cat_group`),
  KEY `status_group` (`status_group`),
  KEY `field_group` (`field_group`),
  KEY `channel_name` (`channel_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_channels` */
INSERT INTO `exp_channels` VALUES(1, 1, 'products', 'Products', 'http://synergy.local/', null, 'en', 3, 0, 1358293556, 0, '1', 1, 'open', 1, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(2, 1, 'featured_product_pages', 'Featured Product Pages', 'http://synergy.local/', null, 'en', 0, 0, 0, 0, '', 1, 'open', 2, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(3, 1, 'news', 'News', 'http://synergy.local/', null, 'en', 7, 0, 1362961483, 0, '', 1, 'open', 3, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(4, 1, 'contact_us', 'Contact Us', 'http://synergy.local/', null, 'en', 1, 0, 1362967322, 0, '', 1, 'open', 4, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);

/* Table structure for table `exp_comment_subscriptions` */
DROP TABLE IF EXISTS `exp_comment_subscriptions`;

CREATE TABLE `exp_comment_subscriptions` (
  `subscription_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `member_id` int(10) DEFAULT '0',
  `email` varchar(50) DEFAULT NULL,
  `subscription_date` varchar(10) DEFAULT NULL,
  `notification_sent` char(1) DEFAULT 'n',
  `hash` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`subscription_id`),
  KEY `entry_id` (`entry_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_comments` */
DROP TABLE IF EXISTS `exp_comments`;

CREATE TABLE `exp_comments` (
  `comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT '0',
  `channel_id` int(4) unsigned DEFAULT '1',
  `author_id` int(10) unsigned DEFAULT '0',
  `status` char(1) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `url` varchar(75) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `comment_date` int(10) DEFAULT NULL,
  `edit_date` int(10) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`comment_id`),
  KEY `entry_id` (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `status` (`status`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_cp_log` */
DROP TABLE IF EXISTS `exp_cp_log`;

CREATE TABLE `exp_cp_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `username` varchar(32) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `act_date` int(10) NOT NULL,
  `action` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_cp_log` */
INSERT INTO `exp_cp_log` VALUES(1, 1, 1, '96black', '127.0.0.1', 1355881673, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(2, 1, 1, '96black', '127.0.0.1', 1355883062, 'Site Updated&nbsp;&nbsp;Synergy Positioning Systems (NZ)');
INSERT INTO `exp_cp_log` VALUES(3, 1, 1, '96black', '127.0.0.1', 1355883097, 'Site Created&nbsp;&nbsp;Synergy Positioning Systems (AU)');
INSERT INTO `exp_cp_log` VALUES(4, 2, 1, '96black', '127.0.0.1', 1355943021, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(5, 2, 1, '96black', '127.0.0.1', 1355945651, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(6, 1, 1, '96black', '127.0.0.1', 1356034419, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(7, 2, 1, '96black', '127.0.0.1', 1356039515, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(8, 2, 1, '96black', '127.0.0.1', 1356039525, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(9, 1, 1, '96black', '127.0.0.1', 1358202174, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(10, 1, 1, '96black', '127.0.0.1', 1358202207, 'Channel Created:&nbsp;&nbsp;Products');
INSERT INTO `exp_cp_log` VALUES(11, 1, 1, '96black', '127.0.0.1', 1358202239, 'Field Group Created:&nbsp;Products');
INSERT INTO `exp_cp_log` VALUES(12, 1, 1, '96black', '127.0.0.1', 1358210877, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(13, 1, 1, '96black', '127.0.0.1', 1358218127, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(14, 1, 1, '96black', '127.0.0.1', 1358218180, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(15, 1, 1, '96black', '127.0.0.1', 1358218184, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(16, 1, 1, '96black', '127.0.0.1', 1358218208, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(17, 1, 1, '96black', '127.0.0.1', 1358220028, 'Channel Created:&nbsp;&nbsp;Featured Product Pages');
INSERT INTO `exp_cp_log` VALUES(18, 1, 1, '96black', '127.0.0.1', 1358220074, 'Field Group Created:&nbsp;Featured Product Pages');
INSERT INTO `exp_cp_log` VALUES(19, 1, 1, '96black', '127.0.0.1', 1358220742, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(20, 1, 1, '96black', '127.0.0.1', 1358274914, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(21, 1, 1, '96black', '127.0.0.1', 1358279266, 'Category Group Created:&nbsp;&nbsp;Product Categories');
INSERT INTO `exp_cp_log` VALUES(22, 1, 1, '96black', '127.0.0.1', 1358293233, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(23, 1, 1, '96black', '127.0.0.1', 1358362941, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(24, 1, 1, '96black', '127.0.0.1', 1359494506, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(25, 1, 1, '96black', '127.0.0.1', 1362948766, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(26, 1, 1, '96black', '127.0.0.1', 1362960488, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(27, 1, 1, '96black', '127.0.0.1', 1362960564, 'Channel Created:&nbsp;&nbsp;News');
INSERT INTO `exp_cp_log` VALUES(28, 1, 1, '96black', '127.0.0.1', 1362960570, 'Field Group Created:&nbsp;News');
INSERT INTO `exp_cp_log` VALUES(29, 1, 1, '96black', '127.0.0.1', 1362966730, 'Field Group Created:&nbsp;Contact Us');
INSERT INTO `exp_cp_log` VALUES(30, 1, 1, '96black', '127.0.0.1', 1362966734, 'Channel Created:&nbsp;&nbsp;Contact Us');

/* Table structure for table `exp_cp_search_index` */
DROP TABLE IF EXISTS `exp_cp_search_index`;

CREATE TABLE `exp_cp_search_index` (
  `search_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(20) DEFAULT NULL,
  `method` varchar(50) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `access` varchar(50) DEFAULT NULL,
  `keywords` text,
  PRIMARY KEY (`search_id`),
  FULLTEXT KEY `keywords` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


/* Table structure for table `exp_developer_log` */
DROP TABLE IF EXISTS `exp_developer_log`;

CREATE TABLE `exp_developer_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(10) unsigned NOT NULL,
  `viewed` char(1) NOT NULL DEFAULT 'n',
  `description` text,
  `function` varchar(100) DEFAULT NULL,
  `line` int(10) unsigned DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `deprecated_since` varchar(10) DEFAULT NULL,
  `use_instead` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_cache` */
DROP TABLE IF EXISTS `exp_email_cache`;

CREATE TABLE `exp_email_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `total_sent` int(6) unsigned NOT NULL,
  `from_name` varchar(70) NOT NULL,
  `from_email` varchar(70) NOT NULL,
  `recipient` text NOT NULL,
  `cc` text NOT NULL,
  `bcc` text NOT NULL,
  `recipient_array` mediumtext NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  `plaintext_alt` mediumtext NOT NULL,
  `mailinglist` char(1) NOT NULL DEFAULT 'n',
  `mailtype` varchar(6) NOT NULL,
  `text_fmt` varchar(40) NOT NULL,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `priority` char(1) NOT NULL DEFAULT '3',
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_cache_mg` */
DROP TABLE IF EXISTS `exp_email_cache_mg`;

CREATE TABLE `exp_email_cache_mg` (
  `cache_id` int(6) unsigned NOT NULL,
  `group_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_cache_ml` */
DROP TABLE IF EXISTS `exp_email_cache_ml`;

CREATE TABLE `exp_email_cache_ml` (
  `cache_id` int(6) unsigned NOT NULL,
  `list_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_console_cache` */
DROP TABLE IF EXISTS `exp_email_console_cache`;

CREATE TABLE `exp_email_console_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `member_id` int(10) unsigned NOT NULL,
  `member_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `recipient` varchar(70) NOT NULL,
  `recipient_name` varchar(50) NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_tracker` */
DROP TABLE IF EXISTS `exp_email_tracker`;

CREATE TABLE `exp_email_tracker` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_ip` varchar(16) NOT NULL,
  `sender_email` varchar(75) NOT NULL,
  `sender_username` varchar(50) NOT NULL,
  `number_recipients` int(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_entry_ping_status` */
DROP TABLE IF EXISTS `exp_entry_ping_status`;

CREATE TABLE `exp_entry_ping_status` (
  `entry_id` int(10) unsigned NOT NULL,
  `ping_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`ping_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_entry_versioning` */
DROP TABLE IF EXISTS `exp_entry_versioning`;

CREATE TABLE `exp_entry_versioning` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL,
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `version_date` int(10) NOT NULL,
  `version_data` mediumtext NOT NULL,
  PRIMARY KEY (`version_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_extensions` */
DROP TABLE IF EXISTS `exp_extensions`;

CREATE TABLE `exp_extensions` (
  `extension_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL DEFAULT '',
  `method` varchar(50) NOT NULL DEFAULT '',
  `hook` varchar(50) NOT NULL DEFAULT '',
  `settings` text NOT NULL,
  `priority` int(2) NOT NULL DEFAULT '10',
  `version` varchar(10) NOT NULL DEFAULT '',
  `enabled` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`extension_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_extensions` */
INSERT INTO `exp_extensions` VALUES(1, 'Safecracker_ext', 'form_declaration_modify_data', 'form_declaration_modify_data', '', 10, '2.1', 'y');
INSERT INTO `exp_extensions` VALUES(2, 'Rte_ext', 'myaccount_nav_setup', 'myaccount_nav_setup', '', 10, '1.0', 'y');
INSERT INTO `exp_extensions` VALUES(3, 'Rte_ext', 'cp_menu_array', 'cp_menu_array', '', 10, '1.0', 'y');
INSERT INTO `exp_extensions` VALUES(4, 'Rte_ext', 'publish_form_entry_data', 'publish_form_entry_data', '', 10, '1.0', 'y');
INSERT INTO `exp_extensions` VALUES(5, 'Matrix_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 10, '2.5.2', 'y');
INSERT INTO `exp_extensions` VALUES(6, 'Playa_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 9, '4.3.3', 'y');
INSERT INTO `exp_extensions` VALUES(7, 'Low_seg2cat_ext', 'sessions_end', 'sessions_end', 'a:3:{s:15:\"category_groups\";a:0:{}s:11:\"uri_pattern\";s:0:\"\";s:16:\"set_all_segments\";s:1:\"n\";}', 1, '2.6.3', 'y');
INSERT INTO `exp_extensions` VALUES(8, 'Mx_cloner_ext', 'publish_form_entry_data', 'publish_form_entry_data', 'a:1:{s:13:\"multilanguage\";s:1:\"n\";}', 10, '1.0.3', 'y');

/* Table structure for table `exp_field_formatting` */
DROP TABLE IF EXISTS `exp_field_formatting`;

CREATE TABLE `exp_field_formatting` (
  `formatting_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(10) unsigned NOT NULL,
  `field_fmt` varchar(40) NOT NULL,
  PRIMARY KEY (`formatting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_field_formatting` */
INSERT INTO `exp_field_formatting` VALUES(1, 1, 'none');
INSERT INTO `exp_field_formatting` VALUES(2, 1, 'br');
INSERT INTO `exp_field_formatting` VALUES(3, 1, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(4, 2, 'none');
INSERT INTO `exp_field_formatting` VALUES(5, 2, 'br');
INSERT INTO `exp_field_formatting` VALUES(6, 2, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(7, 3, 'none');
INSERT INTO `exp_field_formatting` VALUES(8, 3, 'br');
INSERT INTO `exp_field_formatting` VALUES(9, 3, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(10, 4, 'none');
INSERT INTO `exp_field_formatting` VALUES(11, 4, 'br');
INSERT INTO `exp_field_formatting` VALUES(12, 4, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(13, 5, 'none');
INSERT INTO `exp_field_formatting` VALUES(14, 5, 'br');
INSERT INTO `exp_field_formatting` VALUES(15, 5, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(16, 6, 'none');
INSERT INTO `exp_field_formatting` VALUES(17, 6, 'br');
INSERT INTO `exp_field_formatting` VALUES(18, 6, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(19, 7, 'none');
INSERT INTO `exp_field_formatting` VALUES(20, 7, 'br');
INSERT INTO `exp_field_formatting` VALUES(21, 7, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(22, 8, 'none');
INSERT INTO `exp_field_formatting` VALUES(23, 8, 'br');
INSERT INTO `exp_field_formatting` VALUES(24, 8, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(25, 9, 'none');
INSERT INTO `exp_field_formatting` VALUES(26, 9, 'br');
INSERT INTO `exp_field_formatting` VALUES(27, 9, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(28, 10, 'none');
INSERT INTO `exp_field_formatting` VALUES(29, 10, 'br');
INSERT INTO `exp_field_formatting` VALUES(30, 10, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(31, 11, 'none');
INSERT INTO `exp_field_formatting` VALUES(32, 11, 'br');
INSERT INTO `exp_field_formatting` VALUES(33, 11, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(34, 12, 'none');
INSERT INTO `exp_field_formatting` VALUES(35, 12, 'br');
INSERT INTO `exp_field_formatting` VALUES(36, 12, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(37, 13, 'none');
INSERT INTO `exp_field_formatting` VALUES(38, 13, 'br');
INSERT INTO `exp_field_formatting` VALUES(39, 13, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(40, 14, 'none');
INSERT INTO `exp_field_formatting` VALUES(41, 14, 'br');
INSERT INTO `exp_field_formatting` VALUES(42, 14, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(43, 15, 'none');
INSERT INTO `exp_field_formatting` VALUES(44, 15, 'br');
INSERT INTO `exp_field_formatting` VALUES(45, 15, 'xhtml');

/* Table structure for table `exp_field_groups` */
DROP TABLE IF EXISTS `exp_field_groups`;

CREATE TABLE `exp_field_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_field_groups` */
INSERT INTO `exp_field_groups` VALUES(1, 1, 'Products');
INSERT INTO `exp_field_groups` VALUES(2, 1, 'Featured Product Pages');
INSERT INTO `exp_field_groups` VALUES(3, 1, 'News');
INSERT INTO `exp_field_groups` VALUES(4, 1, 'Contact Us');

/* Table structure for table `exp_fieldtypes` */
DROP TABLE IF EXISTS `exp_fieldtypes`;

CREATE TABLE `exp_fieldtypes` (
  `fieldtype_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `version` varchar(12) NOT NULL,
  `settings` text,
  `has_global_settings` char(1) DEFAULT 'n',
  PRIMARY KEY (`fieldtype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_fieldtypes` */
INSERT INTO `exp_fieldtypes` VALUES(1, 'select', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(2, 'text', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(3, 'textarea', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(4, 'date', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(5, 'file', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(6, 'multi_select', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(7, 'checkboxes', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(8, 'radio', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(9, 'rel', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(10, 'rte', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(11, 'matrix', '2.5.2', 'YTowOnt9', 'y');
INSERT INTO `exp_fieldtypes` VALUES(12, 'playa', '4.3.3', 'YTowOnt9', 'y');
INSERT INTO `exp_fieldtypes` VALUES(13, 'wygwam', '2.6.3', 'YTowOnt9', 'y');
INSERT INTO `exp_fieldtypes` VALUES(14, 'pt_checkboxes', '1.0.3', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(15, 'pt_dropdown', '1.0.3', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(16, 'pt_multiselect', '1.0.3', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(17, 'pt_radio_buttons', '1.0.3', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(18, 'nolan', '1.0.3.1', 'YTowOnt9', 'n');

/* Table structure for table `exp_file_categories` */
DROP TABLE IF EXISTS `exp_file_categories`;

CREATE TABLE `exp_file_categories` (
  `file_id` int(10) unsigned DEFAULT NULL,
  `cat_id` int(10) unsigned DEFAULT NULL,
  `sort` int(10) unsigned DEFAULT '0',
  `is_cover` char(1) DEFAULT 'n',
  KEY `file_id` (`file_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_file_dimensions` */
DROP TABLE IF EXISTS `exp_file_dimensions`;

CREATE TABLE `exp_file_dimensions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `upload_location_id` int(4) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT '',
  `short_name` varchar(255) DEFAULT '',
  `resize_type` varchar(50) DEFAULT '',
  `width` int(10) DEFAULT '0',
  `height` int(10) DEFAULT '0',
  `watermark_id` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `upload_location_id` (`upload_location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_file_dimensions` */
INSERT INTO `exp_file_dimensions` VALUES(1, 1, 4, 'article-list-thumb', 'article-list-thumb', 'crop', 396, 88, 0);

/* Table structure for table `exp_file_watermarks` */
DROP TABLE IF EXISTS `exp_file_watermarks`;

CREATE TABLE `exp_file_watermarks` (
  `wm_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `wm_name` varchar(80) DEFAULT NULL,
  `wm_type` varchar(10) DEFAULT 'text',
  `wm_image_path` varchar(100) DEFAULT NULL,
  `wm_test_image_path` varchar(100) DEFAULT NULL,
  `wm_use_font` char(1) DEFAULT 'y',
  `wm_font` varchar(30) DEFAULT NULL,
  `wm_font_size` int(3) unsigned DEFAULT NULL,
  `wm_text` varchar(100) DEFAULT NULL,
  `wm_vrt_alignment` varchar(10) DEFAULT 'top',
  `wm_hor_alignment` varchar(10) DEFAULT 'left',
  `wm_padding` int(3) unsigned DEFAULT NULL,
  `wm_opacity` int(3) unsigned DEFAULT NULL,
  `wm_hor_offset` int(4) unsigned DEFAULT NULL,
  `wm_vrt_offset` int(4) unsigned DEFAULT NULL,
  `wm_x_transp` int(4) DEFAULT NULL,
  `wm_y_transp` int(4) DEFAULT NULL,
  `wm_font_color` varchar(7) DEFAULT NULL,
  `wm_use_drop_shadow` char(1) DEFAULT 'y',
  `wm_shadow_distance` int(3) unsigned DEFAULT NULL,
  `wm_shadow_color` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`wm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_files` */
DROP TABLE IF EXISTS `exp_files`;

CREATE TABLE `exp_files` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `title` varchar(255) DEFAULT NULL,
  `upload_location_id` int(4) unsigned DEFAULT '0',
  `rel_path` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_size` int(10) DEFAULT '0',
  `description` text,
  `credit` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `uploaded_by_member_id` int(10) unsigned DEFAULT '0',
  `upload_date` int(10) DEFAULT NULL,
  `modified_by_member_id` int(10) unsigned DEFAULT '0',
  `modified_date` int(10) DEFAULT NULL,
  `file_hw_original` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`file_id`),
  KEY `upload_location_id` (`upload_location_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_files` */
INSERT INTO `exp_files` VALUES(1, 1, 'large-product-image.jpg', 1, '/Users/james/Projects/96black/Synergy/uploads/product-images/large-product-image.jpg', 'image/jpeg', 'large-product-image.jpg', 82647, null, null, null, 1, 1358293635, 1, 1358293635, '485 352');
INSERT INTO `exp_files` VALUES(3, 1, 'large-product-image2.jpg', 2, '/Users/james/Projects/96black/Synergy/uploads/product-images/large-product-image2.jpg', 'image/jpeg', 'large-product-image2.jpg', 82647, null, null, null, 1, 1358294245, 1, 1358294248, '485 352');
INSERT INTO `exp_files` VALUES(4, 1, 'large-product-image3.jpg', 2, '/Users/james/Projects/96black/Synergy/uploads/product-images/large-product-image3.jpg', 'image/jpeg', 'large-product-image3.jpg', 82647, null, null, null, 1, 1358294270, 1, 1358294270, '485 352');
INSERT INTO `exp_files` VALUES(5, 1, 'large-product-image4.jpg', 2, '/Users/james/Projects/96black/Synergy/uploads/product-downloads/large-product-image4.jpg', 'image/jpeg', 'large-product-image4.jpg', 82647, null, null, null, 1, 1358294478, 1, 1358294478, '485 352');
INSERT INTO `exp_files` VALUES(6, 1, 'large-product-image6.jpg', 1, '/Users/james/Projects/96black/Synergy/uploads/product-images/large-product-image6.jpg', 'image/jpeg', 'large-product-image6.jpg', 82647, null, null, null, 1, 1358363557, 1, 1358363557, '485 352');
INSERT INTO `exp_files` VALUES(7, 1, 'cat-image.jpg', 3, '/Users/jamesmcfall/Projects/Synergy/uploads/category-images/cat-image.jpg', 'image/jpeg', 'cat-image.jpg', 8376, null, null, null, 1, 1362950904, 1, 1362950904, '211 153');
INSERT INTO `exp_files` VALUES(8, 1, 'news-temp-top.jpg', 4, '/Users/jamesmcfall/Projects/Synergy/uploads/news-images/news-temp-top.jpg', 'image/jpeg', 'news-temp-top.jpg', 56244, null, null, null, 1, 1362961635, 1, 1362961635, '373 866');

/* Table structure for table `exp_global_variables` */
DROP TABLE IF EXISTS `exp_global_variables`;

CREATE TABLE `exp_global_variables` (
  `variable_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `variable_name` varchar(50) NOT NULL,
  `variable_data` text NOT NULL,
  PRIMARY KEY (`variable_id`),
  KEY `variable_name` (`variable_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_html_buttons` */
DROP TABLE IF EXISTS `exp_html_buttons`;

CREATE TABLE `exp_html_buttons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `tag_name` varchar(32) NOT NULL,
  `tag_open` varchar(120) NOT NULL,
  `tag_close` varchar(120) NOT NULL,
  `accesskey` varchar(32) NOT NULL,
  `tag_order` int(3) unsigned NOT NULL,
  `tag_row` char(1) NOT NULL DEFAULT '1',
  `classname` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_html_buttons` */
INSERT INTO `exp_html_buttons` VALUES(1, 1, 0, 'b', '<strong>', '</strong>', 'b', 1, '1', 'btn_b');
INSERT INTO `exp_html_buttons` VALUES(2, 1, 0, 'i', '<em>', '</em>', 'i', 2, '1', 'btn_i');
INSERT INTO `exp_html_buttons` VALUES(3, 1, 0, 'blockquote', '<blockquote>', '</blockquote>', 'q', 3, '1', 'btn_blockquote');
INSERT INTO `exp_html_buttons` VALUES(4, 1, 0, 'a', '<a href=\"[![Link:!:http://]!]\"(!( title=\"[![Title]!]\")!)>', '</a>', 'a', 4, '1', 'btn_a');
INSERT INTO `exp_html_buttons` VALUES(5, 1, 0, 'img', '<img src=\"[![Link:!:http://]!]\" alt=\"[![Alternative text]!]\" />', '', '', 5, '1', 'btn_img');
INSERT INTO `exp_html_buttons` VALUES(6, 2, 0, 'b', '<strong>', '</strong>', 'b', 1, '1', 'btn_b');
INSERT INTO `exp_html_buttons` VALUES(7, 2, 0, 'i', '<em>', '</em>', 'i', 2, '1', 'btn_i');
INSERT INTO `exp_html_buttons` VALUES(8, 2, 0, 'blockquote', '<blockquote>', '</blockquote>', 'q', 3, '1', 'btn_blockquote');
INSERT INTO `exp_html_buttons` VALUES(9, 2, 0, 'a', '<a href=\"[![Link:!:http://]!]\"(!( title=\"[![Title]!]\")!)>', '</a>', 'a', 4, '1', 'btn_a');
INSERT INTO `exp_html_buttons` VALUES(10, 2, 0, 'img', '<img src=\"[![Link:!:http://]!]\" alt=\"[![Alternative text]!]\" />', '', '', 5, '1', 'btn_img');

/* Table structure for table `exp_layout_publish` */
DROP TABLE IF EXISTS `exp_layout_publish`;

CREATE TABLE `exp_layout_publish` (
  `layout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_group` int(4) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(4) unsigned NOT NULL DEFAULT '0',
  `field_layout` text,
  PRIMARY KEY (`layout_id`),
  KEY `site_id` (`site_id`),
  KEY `member_group` (`member_group`),
  KEY `channel_id` (`channel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_layout_publish` */
INSERT INTO `exp_layout_publish` VALUES(9, 1, 1, 1, 'a:9:{s:7:\"publish\";a:6:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:1;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:2;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:10;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:15:\"technical_specs\";a:2:{s:10:\"_tab_label\";s:15:\"Technical Specs\";i:3;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"downloads\";a:2:{s:10:\"_tab_label\";s:9:\"Downloads\";i:11;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:6:\"videos\";a:2:{s:10:\"_tab_label\";s:6:\"Videos\";i:4;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:14:\"rental_details\";a:4:{s:10:\"_tab_label\";s:14:\"Rental Details\";i:5;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:6;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:7;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:16:\"featured_details\";a:3:{s:10:\"_tab_label\";s:16:\"Featured Details\";i:9;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:8;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:10:\"Categories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}');

/* Table structure for table `exp_matrix_cols` */
DROP TABLE IF EXISTS `exp_matrix_cols`;

CREATE TABLE `exp_matrix_cols` (
  `col_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `col_name` varchar(32) DEFAULT NULL,
  `col_label` varchar(50) DEFAULT NULL,
  `col_instructions` text,
  `col_type` varchar(50) DEFAULT 'text',
  `col_required` char(1) DEFAULT 'n',
  `col_search` char(1) DEFAULT 'n',
  `col_order` int(3) unsigned DEFAULT NULL,
  `col_width` varchar(4) DEFAULT NULL,
  `col_settings` text,
  PRIMARY KEY (`col_id`),
  KEY `site_id` (`site_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_matrix_cols` */
INSERT INTO `exp_matrix_cols` VALUES(1, 1, 1, null, 'currency', 'Currency', '', 'pt_dropdown', 'n', 'y', 0, '33%', 'YToxOntzOjc6Im9wdGlvbnMiO2E6Mjp7czozOiJOWkQiO3M6MzoiTlpEIjtzOjM6IkFVRCI7czozOiJBVUQiO319');
INSERT INTO `exp_matrix_cols` VALUES(2, 1, 1, null, 'amount', 'Amount', 'Please enter the dollar value (without &#36; or any decimals as they will be output automatically)', 'number', 'n', 'n', 1, '', 'YTozOntzOjk6Im1pbl92YWx1ZSI7czowOiIiO3M6OToibWF4X3ZhbHVlIjtzOjA6IiI7czo4OiJkZWNpbWFscyI7czoxOiIwIjt9');
INSERT INTO `exp_matrix_cols` VALUES(3, 1, 3, null, 'section_title', 'Section Title', 'i.e. Dimensions', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(4, 1, 3, null, 'specification_details', 'Specification Details', 'This is where you enter i.e. Length, 200mm', 'nolan', 'n', 'n', 1, '', 'YToyOntzOjE2OiJub2xhbl9jb2xfbGFiZWxzIjtzOjQwOiJTcGVjaWZpY2F0aW9uIE5hbWUgfCBTcGVjaWZpY2F0aW9uIFZhbHVlIjtzOjE1OiJub2xhbl9jb2xfbmFtZXMiO3M6NDA6InNwZWNpZmljYXRpb25fbmFtZSB8IHNwZWNpZmljYXRpb25fdmFsdWUiO30=');
INSERT INTO `exp_matrix_cols` VALUES(6, 1, 4, null, 'video_heading', 'Video Heading', 'The heading to appear above the video', 'text', 'n', 'n', 0, '25%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(7, 1, 4, null, 'video_embed_url', 'Video Embed URL', 'The embed url from Youtube', 'text', 'n', 'n', 1, '25%', 'YTo0OntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(8, 1, 4, null, 'video_description', 'Video Description', 'This description appears beneath the video', 'wygwam', 'n', 'n', 2, '50%', 'YTozOntzOjc6ImNvbnZlcnQiO3M6MDoiIjtzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(9, 1, 5, null, 'rental_heading', 'Rental Heading', '', 'text', 'n', 'n', 0, '25%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(10, 1, 5, null, 'rental_content', 'Rental Content', '', 'wygwam', 'n', 'n', 1, '75%', 'YTozOntzOjc6ImNvbnZlcnQiO3M6MDoiIjtzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(11, 1, 6, null, 'rental_duration_nz', 'Rental Duration (NZ)', 'i.e. 1-3 days', 'text', 'n', 'n', 0, '20%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(12, 1, 6, null, 'rental_pricing_nz', 'Rental Pricing (NZ)', 'Please exclude the &#36; sign and decimal values.', 'number', 'n', 'n', 1, '', 'YTozOntzOjk6Im1pbl92YWx1ZSI7czowOiIiO3M6OToibWF4X3ZhbHVlIjtzOjA6IiI7czo4OiJkZWNpbWFscyI7czoxOiIwIjt9');
INSERT INTO `exp_matrix_cols` VALUES(13, 1, 7, null, 'rental_duration_au', 'Rental Duration (AU)', 'i.e. 1-3 days', 'text', 'n', 'n', 0, '20%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(14, 1, 7, null, 'rental_pricing_au', 'Rental Pricing (AU)', 'Please exclude the &#36; sign and decimal values.', 'number', 'n', 'n', 1, '', 'YTozOntzOjk6Im1pbl92YWx1ZSI7czowOiIiO3M6OToibWF4X3ZhbHVlIjtzOjA6IiI7czo4OiJkZWNpbWFscyI7czoxOiIwIjt9');
INSERT INTO `exp_matrix_cols` VALUES(15, 1, 8, null, 'featured_banner_image', 'Featured Banner Image', '', 'file', 'n', 'n', 0, '33%', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czozOiJhbGwiO3M6MTI6ImNvbnRlbnRfdHlwZSI7czo1OiJpbWFnZSI7fQ==');
INSERT INTO `exp_matrix_cols` VALUES(16, 1, 10, null, 'product_image', 'Product Image', '', 'file', 'n', 'n', 0, '33%', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiIxIjtzOjEyOiJjb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO30=');
INSERT INTO `exp_matrix_cols` VALUES(17, 1, 11, null, 'file_title', 'File Title', '', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(18, 1, 11, null, 'download_file', 'Files', '', 'file', 'n', 'n', 1, '', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiIyIjtzOjEyOiJjb250ZW50X3R5cGUiO3M6MzoiYWxsIjt9');
INSERT INTO `exp_matrix_cols` VALUES(19, 1, 15, null, 'branch_region', 'Branch Region', 'i.e. Auckland/Christchurch', 'text', 'y', 'n', 0, '20%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(20, 1, 15, null, 'contact_information', 'Contact Information', '', 'nolan', 'y', 'n', 1, '', 'YToyOntzOjE2OiJub2xhbl9jb2xfbGFiZWxzIjtzOjI0OiJDb250YWN0IE1ldGhvZCB8IERldGFpbHMiO3M6MTU6Im5vbGFuX2NvbF9uYW1lcyI7czozMjoiY29udGFjdF9tZXRob2QgfCBjb250YWN0X2RldGFpbHMiO30=');
INSERT INTO `exp_matrix_cols` VALUES(21, 1, 15, null, 'operating_hours', 'Operating Hours', 'i.e. 8:00am to 5:00pm', 'text', 'y', 'n', 2, '', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(22, 1, 15, null, 'physical_address', 'Physical Address', '', 'text', 'y', 'n', 3, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(23, 1, 15, null, 'postal_address', 'Postal Address', '', 'text', 'y', 'n', 4, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');

/* Table structure for table `exp_matrix_data` */
DROP TABLE IF EXISTS `exp_matrix_data`;

CREATE TABLE `exp_matrix_data` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `is_draft` tinyint(1) unsigned DEFAULT '0',
  `row_order` int(4) unsigned DEFAULT NULL,
  `col_id_1` text,
  `col_id_2` int(11) DEFAULT '0',
  `col_id_3` text,
  `col_id_4` text,
  `col_id_6` text,
  `col_id_7` text,
  `col_id_8` text,
  `col_id_9` text,
  `col_id_10` text,
  `col_id_11` text,
  `col_id_12` int(11) DEFAULT '0',
  `col_id_13` text,
  `col_id_14` int(11) DEFAULT '0',
  `col_id_15` text,
  `col_id_16` text,
  `col_id_17` text,
  `col_id_18` text,
  `col_id_19` text,
  `col_id_20` text,
  `col_id_21` text,
  `col_id_22` text,
  `col_id_23` text,
  PRIMARY KEY (`row_id`),
  KEY `site_id` (`site_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_matrix_data` */
INSERT INTO `exp_matrix_data` VALUES(1, 1, 1, 1, null, 0, 1, 'NZD', 45000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(2, 1, 1, 1, null, 0, 2, 'AUD', 40000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(3, 1, 1, 3, null, 0, 1, null, 0, 'Telescope', 'a:7:{i:0;a:2:{s:18:\"specification_name\";s:6:\"Length\";s:19:\"specification_value\";s:15:\"150 Millimeters\";}i:1;a:2:{s:18:\"specification_name\";s:23:\"Objective Lens Diameter\";s:19:\"specification_value\";s:15:\"45mm (EDM:50mm)\";}i:2;a:2:{s:18:\"specification_name\";s:13:\"Magnification\";s:19:\"specification_value\";s:3:\"30x\";}i:3;a:2:{s:18:\"specification_name\";s:5:\"Image\";s:19:\"specification_value\";s:5:\"Erect\";}i:4;a:2:{s:18:\"specification_name\";s:13:\"Field of View\";s:19:\"specification_value\";s:3:\"130\";}i:5;a:2:{s:18:\"specification_name\";s:13:\"Resolve Power\";s:19:\"specification_value\";s:2:\"3\"\";}i:6;a:2:{s:18:\"specification_name\";s:18:\"Min Focus Distance\";s:19:\"specification_value\";s:10:\"1.3 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(4, 1, 1, 3, null, 0, 2, null, 0, 'Measurement Range', 'a:2:{i:0;a:2:{s:18:\"specification_name\";s:8:\"1 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}i:1;a:2:{s:18:\"specification_name\";s:8:\"3 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(5, 1, 1, 10, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image.jpg', null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(6, 1, 1, 11, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, 'Test uploaded file', '{filedir_2}large-product-image4.jpg', null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(7, 1, 1, 4, null, 0, 1, null, 0, null, null, 'Test Video Of My 4x4', 'http://www.youtube.com/embed/HNIA-nesZEU', '<p>\n	Test vid of my 4x4.&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae gravida tortor. Maecenas turpis purus, imperdiet id elementum eget, feugiat in tellus. Duis molestie, nulla nec sollicitudin consectetur, magna nisl condimentum neque, at consequat magna enim a tellus. Maecenas porttitor mattis aliquet. Ut lobortis, tortor eget lacinia commodo, mi sapien molestie neque, quis mattis metus lorem posuere massa. Vivamus id pretium lacus. Aliquam ac mauris orci, vulputate ullamcorper elit. Donec eget eros eros. Etiam ac sapien a sem ullamcorper consequat. Suspendisse ut elementum arcu. Cras fringilla tortor pellentesque nibh pharetra pellentesque. Sed et mauris sit amet ligula scelerisque sodales sed nec lorem. In vel quam sit amet eros lobortis consectetur sit amet egestas nisl. Cras erat turpis, suscipit posuere mollis nec, dictum eget lectus.</p>', null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(8, 1, 1, 5, null, 0, 1, null, 0, null, null, null, null, null, 'Test heading', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(9, 1, 1, 5, null, 0, 2, null, 0, null, null, null, null, null, 'Test heading 2', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(10, 1, 1, 6, null, 0, 1, null, 0, null, null, null, null, null, null, null, '1-3 days', 100, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(11, 1, 1, 6, null, 0, 2, null, 0, null, null, null, null, null, null, null, '4-6 days', 180, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(12, 1, 1, 7, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, '1-3 days', 80, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(13, 1, 1, 7, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, '4-6 days', 160, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(14, 1, 1, 10, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image6.jpg', null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(15, 1, 2, 1, null, 0, 1, 'NZD', 45000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(16, 1, 2, 1, null, 0, 2, 'AUD', 40000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(17, 1, 2, 3, null, 0, 1, null, 0, 'Telescope', 'a:7:{i:0;a:2:{s:18:\"specification_name\";s:6:\"Length\";s:19:\"specification_value\";s:15:\"150 Millimeters\";}i:1;a:2:{s:18:\"specification_name\";s:23:\"Objective Lens Diameter\";s:19:\"specification_value\";s:15:\"45mm (EDM:50mm)\";}i:2;a:2:{s:18:\"specification_name\";s:13:\"Magnification\";s:19:\"specification_value\";s:3:\"30x\";}i:3;a:2:{s:18:\"specification_name\";s:5:\"Image\";s:19:\"specification_value\";s:5:\"Erect\";}i:4;a:2:{s:18:\"specification_name\";s:13:\"Field of View\";s:19:\"specification_value\";s:3:\"130\";}i:5;a:2:{s:18:\"specification_name\";s:13:\"Resolve Power\";s:19:\"specification_value\";s:2:\"3\"\";}i:6;a:2:{s:18:\"specification_name\";s:18:\"Min Focus Distance\";s:19:\"specification_value\";s:10:\"1.3 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(18, 1, 2, 3, null, 0, 2, null, 0, 'Measurement Range', 'a:2:{i:0;a:2:{s:18:\"specification_name\";s:8:\"1 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}i:1;a:2:{s:18:\"specification_name\";s:8:\"3 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(19, 1, 2, 4, null, 0, 1, null, 0, null, null, 'Test Video Of My 4x4', 'http://www.youtube.com/embed/HNIA-nesZEU', '<p>\n	Test vid of my 4x4.&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae gravida tortor. Maecenas turpis purus, imperdiet id elementum eget, feugiat in tellus. Duis molestie, nulla nec sollicitudin consectetur, magna nisl condimentum neque, at consequat magna enim a tellus. Maecenas porttitor mattis aliquet. Ut lobortis, tortor eget lacinia commodo, mi sapien molestie neque, quis mattis metus lorem posuere massa. Vivamus id pretium lacus. Aliquam ac mauris orci, vulputate ullamcorper elit. Donec eget eros eros. Etiam ac sapien a sem ullamcorper consequat. Suspendisse ut elementum arcu. Cras fringilla tortor pellentesque nibh pharetra pellentesque. Sed et mauris sit amet ligula scelerisque sodales sed nec lorem. In vel quam sit amet eros lobortis consectetur sit amet egestas nisl. Cras erat turpis, suscipit posuere mollis nec, dictum eget lectus.</p>', null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(20, 1, 2, 5, null, 0, 1, null, 0, null, null, null, null, null, 'Test heading', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(21, 1, 2, 5, null, 0, 2, null, 0, null, null, null, null, null, 'Test heading 2', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(22, 1, 2, 6, null, 0, 1, null, 0, null, null, null, null, null, null, null, '1-3 days', 100, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(23, 1, 2, 6, null, 0, 2, null, 0, null, null, null, null, null, null, null, '4-6 days', 180, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(24, 1, 2, 7, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, '1-3 days', 80, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(25, 1, 2, 7, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, '4-6 days', 160, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(26, 1, 2, 10, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image.jpg', null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(27, 1, 2, 10, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image6.jpg', null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(28, 1, 2, 11, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, 'Test uploaded file', '{filedir_2}large-product-image4.jpg', null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(29, 1, 3, 1, null, 0, 1, 'NZD', 45000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(30, 1, 3, 1, null, 0, 2, 'AUD', 40000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(31, 1, 3, 3, null, 0, 1, null, 0, 'Telescope', 'a:7:{i:0;a:2:{s:18:\"specification_name\";s:6:\"Length\";s:19:\"specification_value\";s:15:\"150 Millimeters\";}i:1;a:2:{s:18:\"specification_name\";s:23:\"Objective Lens Diameter\";s:19:\"specification_value\";s:15:\"45mm (EDM:50mm)\";}i:2;a:2:{s:18:\"specification_name\";s:13:\"Magnification\";s:19:\"specification_value\";s:3:\"30x\";}i:3;a:2:{s:18:\"specification_name\";s:5:\"Image\";s:19:\"specification_value\";s:5:\"Erect\";}i:4;a:2:{s:18:\"specification_name\";s:13:\"Field of View\";s:19:\"specification_value\";s:3:\"130\";}i:5;a:2:{s:18:\"specification_name\";s:13:\"Resolve Power\";s:19:\"specification_value\";s:2:\"3\"\";}i:6;a:2:{s:18:\"specification_name\";s:18:\"Min Focus Distance\";s:19:\"specification_value\";s:10:\"1.3 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(32, 1, 3, 3, null, 0, 2, null, 0, 'Measurement Range', 'a:2:{i:0;a:2:{s:18:\"specification_name\";s:8:\"1 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}i:1;a:2:{s:18:\"specification_name\";s:8:\"3 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(33, 1, 3, 4, null, 0, 1, null, 0, null, null, 'Test Video Of My 4x4', 'http://www.youtube.com/embed/HNIA-nesZEU', '<p>\n	Test vid of my 4x4.&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae gravida tortor. Maecenas turpis purus, imperdiet id elementum eget, feugiat in tellus. Duis molestie, nulla nec sollicitudin consectetur, magna nisl condimentum neque, at consequat magna enim a tellus. Maecenas porttitor mattis aliquet. Ut lobortis, tortor eget lacinia commodo, mi sapien molestie neque, quis mattis metus lorem posuere massa. Vivamus id pretium lacus. Aliquam ac mauris orci, vulputate ullamcorper elit. Donec eget eros eros. Etiam ac sapien a sem ullamcorper consequat. Suspendisse ut elementum arcu. Cras fringilla tortor pellentesque nibh pharetra pellentesque. Sed et mauris sit amet ligula scelerisque sodales sed nec lorem. In vel quam sit amet eros lobortis consectetur sit amet egestas nisl. Cras erat turpis, suscipit posuere mollis nec, dictum eget lectus.</p>', null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(34, 1, 3, 5, null, 0, 1, null, 0, null, null, null, null, null, 'Test heading', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(35, 1, 3, 5, null, 0, 2, null, 0, null, null, null, null, null, 'Test heading 2', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(36, 1, 3, 6, null, 0, 1, null, 0, null, null, null, null, null, null, null, '1-3 days', 100, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(37, 1, 3, 6, null, 0, 2, null, 0, null, null, null, null, null, null, null, '4-6 days', 180, null, 0, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(38, 1, 3, 7, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, '1-3 days', 80, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(39, 1, 3, 7, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, '4-6 days', 160, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(40, 1, 3, 10, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image.jpg', null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(41, 1, 3, 10, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image6.jpg', null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(42, 1, 3, 11, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, 'Test uploaded file', '{filedir_2}large-product-image4.jpg', null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(43, 1, 11, 15, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, 'Auckland', 'a:4:{i:0;a:2:{s:14:\"contact_method\";s:9:\"Free Call\";s:15:\"contact_details\";s:12:\"0800-867-266\";}i:1;a:2:{s:14:\"contact_method\";s:5:\"Phone\";s:15:\"contact_details\";s:14:\"+64-9-476-5151\";}i:2;a:2:{s:14:\"contact_method\";s:3:\"Fax\";s:15:\"contact_details\";s:14:\"+63-9-476-5140\";}i:3;a:2:{s:14:\"contact_method\";s:5:\"Email\";s:15:\"contact_details\";s:30:\" info@synergypositioning.co.nz\";}}', '8:00am to 5:00pm', '3/52 Arrenway Drive \nAlbany \nAuckland \nNew Zealand', 'P O Box 100450 \nNSMC \nAuckland \nNew Zealand \n0745');
INSERT INTO `exp_matrix_data` VALUES(44, 1, 11, 15, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, 'Christchurch', 'a:4:{i:0;a:2:{s:14:\"contact_method\";s:9:\"Free Call\";s:15:\"contact_details\";s:13:\" 0800-867-266\";}i:1;a:2:{s:14:\"contact_method\";s:5:\"Phone\";s:15:\"contact_details\";s:14:\"+64-9-476-5151\";}i:2;a:2:{s:14:\"contact_method\";s:3:\"Fax\";s:15:\"contact_details\";s:14:\"+63-9-476-5140\";}i:3;a:2:{s:14:\"contact_method\";s:5:\"Email\";s:15:\"contact_details\";s:30:\" info@synergypositioning.co.nz\";}}', '8:00am to 5:00pm', '3/52 Arrenway Drive \nAlbany \nAuckland \nNew Zealand', 'P O Box 100450 \nNSMC \nAuckland \nNew Zealand \n0745');

/* Table structure for table `exp_member_bulletin_board` */
DROP TABLE IF EXISTS `exp_member_bulletin_board`;

CREATE TABLE `exp_member_bulletin_board` (
  `bulletin_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `bulletin_group` int(8) unsigned NOT NULL,
  `bulletin_date` int(10) unsigned NOT NULL,
  `hash` varchar(10) NOT NULL DEFAULT '',
  `bulletin_expires` int(10) unsigned NOT NULL DEFAULT '0',
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_id`),
  KEY `sender_id` (`sender_id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_member_data` */
DROP TABLE IF EXISTS `exp_member_data`;

CREATE TABLE `exp_member_data` (
  `member_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_member_data` */
INSERT INTO `exp_member_data` VALUES(1);

/* Table structure for table `exp_member_fields` */
DROP TABLE IF EXISTS `exp_member_fields`;

CREATE TABLE `exp_member_fields` (
  `m_field_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `m_field_name` varchar(32) NOT NULL,
  `m_field_label` varchar(50) NOT NULL,
  `m_field_description` text NOT NULL,
  `m_field_type` varchar(12) NOT NULL DEFAULT 'text',
  `m_field_list_items` text NOT NULL,
  `m_field_ta_rows` tinyint(2) DEFAULT '8',
  `m_field_maxl` smallint(3) NOT NULL,
  `m_field_width` varchar(6) NOT NULL,
  `m_field_search` char(1) NOT NULL DEFAULT 'y',
  `m_field_required` char(1) NOT NULL DEFAULT 'n',
  `m_field_public` char(1) NOT NULL DEFAULT 'y',
  `m_field_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_cp_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_fmt` char(5) NOT NULL DEFAULT 'none',
  `m_field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`m_field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_member_groups` */
DROP TABLE IF EXISTS `exp_member_groups`;

CREATE TABLE `exp_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_title` varchar(100) NOT NULL,
  `group_description` text NOT NULL,
  `is_locked` char(1) NOT NULL DEFAULT 'y',
  `can_view_offline_system` char(1) NOT NULL DEFAULT 'n',
  `can_view_online_system` char(1) NOT NULL DEFAULT 'y',
  `can_access_cp` char(1) NOT NULL DEFAULT 'y',
  `can_access_content` char(1) NOT NULL DEFAULT 'n',
  `can_access_publish` char(1) NOT NULL DEFAULT 'n',
  `can_access_edit` char(1) NOT NULL DEFAULT 'n',
  `can_access_files` char(1) NOT NULL DEFAULT 'n',
  `can_access_fieldtypes` char(1) NOT NULL DEFAULT 'n',
  `can_access_design` char(1) NOT NULL DEFAULT 'n',
  `can_access_addons` char(1) NOT NULL DEFAULT 'n',
  `can_access_modules` char(1) NOT NULL DEFAULT 'n',
  `can_access_extensions` char(1) NOT NULL DEFAULT 'n',
  `can_access_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_access_plugins` char(1) NOT NULL DEFAULT 'n',
  `can_access_members` char(1) NOT NULL DEFAULT 'n',
  `can_access_admin` char(1) NOT NULL DEFAULT 'n',
  `can_access_sys_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_content_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_tools` char(1) NOT NULL DEFAULT 'n',
  `can_access_comm` char(1) NOT NULL DEFAULT 'n',
  `can_access_utilities` char(1) NOT NULL DEFAULT 'n',
  `can_access_data` char(1) NOT NULL DEFAULT 'n',
  `can_access_logs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_channels` char(1) NOT NULL DEFAULT 'n',
  `can_admin_upload_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_design` char(1) NOT NULL DEFAULT 'n',
  `can_admin_members` char(1) NOT NULL DEFAULT 'n',
  `can_delete_members` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_groups` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_templates` char(1) NOT NULL DEFAULT 'n',
  `can_ban_users` char(1) NOT NULL DEFAULT 'n',
  `can_admin_modules` char(1) NOT NULL DEFAULT 'n',
  `can_admin_templates` char(1) NOT NULL DEFAULT 'n',
  `can_admin_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_edit_categories` char(1) NOT NULL DEFAULT 'n',
  `can_delete_categories` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_edit_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_assign_post_authors` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self_entries` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_entries` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_moderate_comments` char(1) NOT NULL DEFAULT 'n',
  `can_send_email` char(1) NOT NULL DEFAULT 'n',
  `can_send_cached_email` char(1) NOT NULL DEFAULT 'n',
  `can_email_member_groups` char(1) NOT NULL DEFAULT 'n',
  `can_email_mailinglist` char(1) NOT NULL DEFAULT 'n',
  `can_email_from_profile` char(1) NOT NULL DEFAULT 'n',
  `can_view_profiles` char(1) NOT NULL DEFAULT 'n',
  `can_edit_html_buttons` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self` char(1) NOT NULL DEFAULT 'n',
  `mbr_delete_notify_emails` varchar(255) DEFAULT NULL,
  `can_post_comments` char(1) NOT NULL DEFAULT 'n',
  `exclude_from_moderation` char(1) NOT NULL DEFAULT 'n',
  `can_search` char(1) NOT NULL DEFAULT 'n',
  `search_flood_control` mediumint(5) unsigned NOT NULL,
  `can_send_private_messages` char(1) NOT NULL DEFAULT 'n',
  `prv_msg_send_limit` smallint(5) unsigned NOT NULL DEFAULT '20',
  `prv_msg_storage_limit` smallint(5) unsigned NOT NULL DEFAULT '60',
  `can_attach_in_private_messages` char(1) NOT NULL DEFAULT 'n',
  `can_send_bulletins` char(1) NOT NULL DEFAULT 'n',
  `include_in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `include_in_memberlist` char(1) NOT NULL DEFAULT 'y',
  `include_in_mailinglists` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`group_id`,`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_member_groups` */
INSERT INTO `exp_member_groups` VALUES(1, 1, 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y');
INSERT INTO `exp_member_groups` VALUES(1, 2, 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y');
INSERT INTO `exp_member_groups` VALUES(2, 1, 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', 60, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(2, 2, 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', 60, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(3, 1, 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(3, 2, 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(4, 1, 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(4, 2, 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(5, 1, 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', 10, 'y', 20, 60, 'y', 'n', 'n', 'y', 'y');
INSERT INTO `exp_member_groups` VALUES(5, 2, 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', 10, 'y', 20, 60, 'y', 'n', 'n', 'y', 'y');

/* Table structure for table `exp_member_homepage` */
DROP TABLE IF EXISTS `exp_member_homepage`;

CREATE TABLE `exp_member_homepage` (
  `member_id` int(10) unsigned NOT NULL,
  `recent_entries` char(1) NOT NULL DEFAULT 'l',
  `recent_entries_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_comments` char(1) NOT NULL DEFAULT 'l',
  `recent_comments_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_members` char(1) NOT NULL DEFAULT 'n',
  `recent_members_order` int(3) unsigned NOT NULL DEFAULT '0',
  `site_statistics` char(1) NOT NULL DEFAULT 'r',
  `site_statistics_order` int(3) unsigned NOT NULL DEFAULT '0',
  `member_search_form` char(1) NOT NULL DEFAULT 'n',
  `member_search_form_order` int(3) unsigned NOT NULL DEFAULT '0',
  `notepad` char(1) NOT NULL DEFAULT 'r',
  `notepad_order` int(3) unsigned NOT NULL DEFAULT '0',
  `bulletin_board` char(1) NOT NULL DEFAULT 'r',
  `bulletin_board_order` int(3) unsigned NOT NULL DEFAULT '0',
  `pmachine_news_feed` char(1) NOT NULL DEFAULT 'n',
  `pmachine_news_feed_order` int(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_member_homepage` */
INSERT INTO `exp_member_homepage` VALUES(1, 'l', 1, 'l', 2, 'n', 0, 'r', 1, 'n', 0, 'r', 2, 'r', 0, 'l', 0);

/* Table structure for table `exp_member_search` */
DROP TABLE IF EXISTS `exp_member_search`;

CREATE TABLE `exp_member_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `search_date` int(10) unsigned NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `fields` varchar(200) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(8) unsigned NOT NULL,
  `query` text NOT NULL,
  PRIMARY KEY (`search_id`),
  KEY `member_id` (`member_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_members` */
DROP TABLE IF EXISTS `exp_members`;

CREATE TABLE `exp_members` (
  `member_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` smallint(4) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL DEFAULT '',
  `unique_id` varchar(40) NOT NULL,
  `crypt_key` varchar(40) DEFAULT NULL,
  `authcode` varchar(10) DEFAULT NULL,
  `email` varchar(72) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `occupation` varchar(80) DEFAULT NULL,
  `interests` varchar(120) DEFAULT NULL,
  `bday_d` int(2) DEFAULT NULL,
  `bday_m` int(2) DEFAULT NULL,
  `bday_y` int(4) DEFAULT NULL,
  `aol_im` varchar(50) DEFAULT NULL,
  `yahoo_im` varchar(50) DEFAULT NULL,
  `msn_im` varchar(50) DEFAULT NULL,
  `icq` varchar(50) DEFAULT NULL,
  `bio` text,
  `signature` text,
  `avatar_filename` varchar(120) DEFAULT NULL,
  `avatar_width` int(4) unsigned DEFAULT NULL,
  `avatar_height` int(4) unsigned DEFAULT NULL,
  `photo_filename` varchar(120) DEFAULT NULL,
  `photo_width` int(4) unsigned DEFAULT NULL,
  `photo_height` int(4) unsigned DEFAULT NULL,
  `sig_img_filename` varchar(120) DEFAULT NULL,
  `sig_img_width` int(4) unsigned DEFAULT NULL,
  `sig_img_height` int(4) unsigned DEFAULT NULL,
  `ignore_list` text,
  `private_messages` int(4) unsigned NOT NULL DEFAULT '0',
  `accept_messages` char(1) NOT NULL DEFAULT 'y',
  `last_view_bulletins` int(10) NOT NULL DEFAULT '0',
  `last_bulletin_date` int(10) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `join_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visit` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `total_entries` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_comments` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `accept_admin_email` char(1) NOT NULL DEFAULT 'y',
  `accept_user_email` char(1) NOT NULL DEFAULT 'y',
  `notify_by_default` char(1) NOT NULL DEFAULT 'y',
  `notify_of_pm` char(1) NOT NULL DEFAULT 'y',
  `display_avatars` char(1) NOT NULL DEFAULT 'y',
  `display_signatures` char(1) NOT NULL DEFAULT 'y',
  `parse_smileys` char(1) NOT NULL DEFAULT 'y',
  `smart_notifications` char(1) NOT NULL DEFAULT 'y',
  `language` varchar(50) NOT NULL,
  `timezone` varchar(8) NOT NULL,
  `daylight_savings` char(1) NOT NULL DEFAULT 'n',
  `localization_is_site_default` char(1) NOT NULL DEFAULT 'n',
  `time_format` char(2) NOT NULL DEFAULT 'us',
  `cp_theme` varchar(32) DEFAULT NULL,
  `profile_theme` varchar(32) DEFAULT NULL,
  `forum_theme` varchar(32) DEFAULT NULL,
  `tracker` text,
  `template_size` varchar(2) NOT NULL DEFAULT '20',
  `notepad` text,
  `notepad_size` varchar(2) NOT NULL DEFAULT '18',
  `quick_links` text,
  `quick_tabs` text,
  `show_sidebar` char(1) NOT NULL DEFAULT 'n',
  `pmember_id` int(10) NOT NULL DEFAULT '0',
  `rte_enabled` char(1) NOT NULL DEFAULT 'y',
  `rte_toolset_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`),
  KEY `group_id` (`group_id`),
  KEY `unique_id` (`unique_id`),
  KEY `password` (`password`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_members` */
INSERT INTO `exp_members` VALUES(1, 1, '96black', '96black', '106cfaa54b9caa63de2a620edb474b900a68d8e76e79847360bbef4323d51b29ea822b5dfe7f5352be760a6ae6a6de7b8abb78f37ad429db302a103d864acad8', '=WDztY<`dk>S{94N7[sY$1\'WCM{:qPT;Z{P44_rlj\'GU8,WH]l$%3d27{+oz,-=(&Ed*nE>q)!+z4p=a;?`yYL|]l:.`/U.=P3lU@\"n*jX_-=GO>!E+?()Cqj<ai>Q=a', '2d3c4dab12f0973e5396766a007ac2efd798eb16', 'b9cbee325c2edd1db5f8b3ef603a2ad0bb317a5f', null, 'james@96black.co.nz', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 0, 'y', 0, 0, '127.0.0.1', 1355879065, 1359494506, 1362969236, 11, 0, 0, 0, 1362967503, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UP12', 'y', 'n', 'us', null, null, null, null, '20', null, '18', '', null, 'n', 0, 'y', 0);

/* Table structure for table `exp_message_attachments` */
DROP TABLE IF EXISTS `exp_message_attachments`;

CREATE TABLE `exp_message_attachments` (
  `attachment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_name` varchar(50) NOT NULL DEFAULT '',
  `attachment_hash` varchar(40) NOT NULL DEFAULT '',
  `attachment_extension` varchar(20) NOT NULL DEFAULT '',
  `attachment_location` varchar(150) NOT NULL DEFAULT '',
  `attachment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_size` int(10) unsigned NOT NULL DEFAULT '0',
  `is_temp` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`attachment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_message_copies` */
DROP TABLE IF EXISTS `exp_message_copies`;

CREATE TABLE `exp_message_copies` (
  `copy_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `recipient_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_received` char(1) NOT NULL DEFAULT 'n',
  `message_read` char(1) NOT NULL DEFAULT 'n',
  `message_time_read` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_downloaded` char(1) NOT NULL DEFAULT 'n',
  `message_folder` int(10) unsigned NOT NULL DEFAULT '1',
  `message_authcode` varchar(10) NOT NULL DEFAULT '',
  `message_deleted` char(1) NOT NULL DEFAULT 'n',
  `message_status` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`copy_id`),
  KEY `message_id` (`message_id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_message_data` */
DROP TABLE IF EXISTS `exp_message_data`;

CREATE TABLE `exp_message_data` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_date` int(10) unsigned NOT NULL DEFAULT '0',
  `message_subject` varchar(255) NOT NULL DEFAULT '',
  `message_body` text NOT NULL,
  `message_tracking` char(1) NOT NULL DEFAULT 'y',
  `message_attachments` char(1) NOT NULL DEFAULT 'n',
  `message_recipients` varchar(200) NOT NULL DEFAULT '',
  `message_cc` varchar(200) NOT NULL DEFAULT '',
  `message_hide_cc` char(1) NOT NULL DEFAULT 'n',
  `message_sent_copy` char(1) NOT NULL DEFAULT 'n',
  `total_recipients` int(5) unsigned NOT NULL DEFAULT '0',
  `message_status` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`message_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_message_folders` */
DROP TABLE IF EXISTS `exp_message_folders`;

CREATE TABLE `exp_message_folders` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `folder1_name` varchar(50) NOT NULL DEFAULT 'InBox',
  `folder2_name` varchar(50) NOT NULL DEFAULT 'Sent',
  `folder3_name` varchar(50) NOT NULL DEFAULT '',
  `folder4_name` varchar(50) NOT NULL DEFAULT '',
  `folder5_name` varchar(50) NOT NULL DEFAULT '',
  `folder6_name` varchar(50) NOT NULL DEFAULT '',
  `folder7_name` varchar(50) NOT NULL DEFAULT '',
  `folder8_name` varchar(50) NOT NULL DEFAULT '',
  `folder9_name` varchar(50) NOT NULL DEFAULT '',
  `folder10_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_message_listed` */
DROP TABLE IF EXISTS `exp_message_listed`;

CREATE TABLE `exp_message_listed` (
  `listed_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_member` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_description` varchar(100) NOT NULL DEFAULT '',
  `listed_type` varchar(10) NOT NULL DEFAULT 'blocked',
  PRIMARY KEY (`listed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_module_member_groups` */
DROP TABLE IF EXISTS `exp_module_member_groups`;

CREATE TABLE `exp_module_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `module_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_modules` */
DROP TABLE IF EXISTS `exp_modules`;

CREATE TABLE `exp_modules` (
  `module_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(50) NOT NULL,
  `module_version` varchar(12) NOT NULL,
  `has_cp_backend` char(1) NOT NULL DEFAULT 'n',
  `has_publish_fields` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_modules` */
INSERT INTO `exp_modules` VALUES(1, 'Comment', '2.3', 'y', 'n');
INSERT INTO `exp_modules` VALUES(2, 'Email', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(3, 'Emoticon', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(4, 'File', '1.0.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(5, 'Jquery', '1.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(6, 'Rss', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(7, 'Safecracker', '2.1', 'y', 'n');
INSERT INTO `exp_modules` VALUES(8, 'Search', '2.2', 'n', 'n');
INSERT INTO `exp_modules` VALUES(9, 'Channel', '2.0.1', 'n', 'n');
INSERT INTO `exp_modules` VALUES(10, 'Member', '2.1', 'n', 'n');
INSERT INTO `exp_modules` VALUES(11, 'Stats', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(12, 'Rte', '1.0', 'y', 'n');
INSERT INTO `exp_modules` VALUES(13, 'Playa', '4.3.3', 'n', 'n');
INSERT INTO `exp_modules` VALUES(14, 'Wygwam', '2.6.3', 'y', 'n');
INSERT INTO `exp_modules` VALUES(15, 'Query', '2.0', 'n', 'n');

/* Table structure for table `exp_online_users` */
DROP TABLE IF EXISTS `exp_online_users`;

CREATE TABLE `exp_online_users` (
  `online_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `in_forum` char(1) NOT NULL DEFAULT 'n',
  `name` varchar(50) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `anon` char(1) NOT NULL,
  PRIMARY KEY (`online_id`),
  KEY `date` (`date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_online_users` */
INSERT INTO `exp_online_users` VALUES(6, 2, 0, 'n', '', '127.0.0.1', 1355941646, '');
INSERT INTO `exp_online_users` VALUES(8, 2, 0, 'n', '', '127.0.0.1', 1355941646, '');
INSERT INTO `exp_online_users` VALUES(9, 2, 0, 'n', '', '127.0.0.1', 1355941646, '');
INSERT INTO `exp_online_users` VALUES(42, 1, 0, 'n', '', '127.0.0.1', 1362969393, '');
INSERT INTO `exp_online_users` VALUES(43, 1, 0, 'n', '', '127.0.0.1', 1362969393, '');
INSERT INTO `exp_online_users` VALUES(45, 1, 0, 'n', '', '127.0.0.1', 1362969393, '');
INSERT INTO `exp_online_users` VALUES(46, 1, 1, 'n', '96black', '127.0.0.1', 1362969386, '');
INSERT INTO `exp_online_users` VALUES(47, 1, 0, 'n', '', '127.0.0.1', 1362969393, '');
INSERT INTO `exp_online_users` VALUES(48, 1, 1, 'n', '96black', '127.0.0.1', 1362969386, '');
INSERT INTO `exp_online_users` VALUES(49, 1, 1, 'n', '96black', '127.0.0.1', 1362969386, '');
INSERT INTO `exp_online_users` VALUES(50, 1, 1, 'n', '96black', '127.0.0.1', 1362969386, '');

/* Table structure for table `exp_password_lockout` */
DROP TABLE IF EXISTS `exp_password_lockout`;

CREATE TABLE `exp_password_lockout` (
  `lockout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`lockout_id`),
  KEY `login_date` (`login_date`),
  KEY `ip_address` (`ip_address`),
  KEY `user_agent` (`user_agent`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_password_lockout` */
INSERT INTO `exp_password_lockout` VALUES(1, 1356039512, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.101 Safari/537.11', '96black');

/* Table structure for table `exp_ping_servers` */
DROP TABLE IF EXISTS `exp_ping_servers`;

CREATE TABLE `exp_ping_servers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `server_name` varchar(32) NOT NULL,
  `server_url` varchar(150) NOT NULL,
  `port` varchar(4) NOT NULL DEFAULT '80',
  `ping_protocol` varchar(12) NOT NULL DEFAULT 'xmlrpc',
  `is_default` char(1) NOT NULL DEFAULT 'y',
  `server_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_playa_relationships` */
DROP TABLE IF EXISTS `exp_playa_relationships`;

CREATE TABLE `exp_playa_relationships` (
  `rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_entry_id` int(10) unsigned DEFAULT NULL,
  `parent_field_id` int(6) unsigned DEFAULT NULL,
  `parent_col_id` int(6) unsigned DEFAULT NULL,
  `parent_row_id` int(10) unsigned DEFAULT NULL,
  `parent_var_id` int(6) unsigned DEFAULT NULL,
  `child_entry_id` int(10) unsigned DEFAULT NULL,
  `rel_order` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `parent_entry_id` (`parent_entry_id`),
  KEY `parent_field_id` (`parent_field_id`),
  KEY `parent_col_id` (`parent_col_id`),
  KEY `parent_row_id` (`parent_row_id`),
  KEY `parent_var_id` (`parent_var_id`),
  KEY `child_entry_id` (`child_entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_relationships` */
DROP TABLE IF EXISTS `exp_relationships`;

CREATE TABLE `exp_relationships` (
  `rel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `rel_parent_id` int(10) NOT NULL DEFAULT '0',
  `rel_child_id` int(10) NOT NULL DEFAULT '0',
  `rel_type` varchar(12) NOT NULL,
  `rel_data` mediumtext NOT NULL,
  `reverse_rel_data` mediumtext NOT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `rel_parent_id` (`rel_parent_id`),
  KEY `rel_child_id` (`rel_child_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_remember_me` */
DROP TABLE IF EXISTS `exp_remember_me`;

CREATE TABLE `exp_remember_me` (
  `remember_me_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) DEFAULT '0',
  `ip_address` varchar(45) DEFAULT '0',
  `user_agent` varchar(120) DEFAULT '',
  `admin_sess` tinyint(1) DEFAULT '0',
  `site_id` int(4) DEFAULT '1',
  `expiration` int(10) DEFAULT '0',
  `last_refresh` int(10) DEFAULT '0',
  PRIMARY KEY (`remember_me_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_reset_password` */
DROP TABLE IF EXISTS `exp_reset_password`;

CREATE TABLE `exp_reset_password` (
  `reset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL,
  `resetcode` varchar(12) NOT NULL,
  `date` int(10) NOT NULL,
  PRIMARY KEY (`reset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_revision_tracker` */
DROP TABLE IF EXISTS `exp_revision_tracker`;

CREATE TABLE `exp_revision_tracker` (
  `tracker_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `item_table` varchar(20) NOT NULL,
  `item_field` varchar(20) NOT NULL,
  `item_date` int(10) NOT NULL,
  `item_author_id` int(10) unsigned NOT NULL,
  `item_data` mediumtext NOT NULL,
  PRIMARY KEY (`tracker_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_rte_tools` */
DROP TABLE IF EXISTS `exp_rte_tools`;

CREATE TABLE `exp_rte_tools` (
  `tool_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(75) DEFAULT NULL,
  `class` varchar(75) DEFAULT NULL,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`tool_id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_rte_tools` */
INSERT INTO `exp_rte_tools` VALUES(1, 'Blockquote', 'Blockquote_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(2, 'Bold', 'Bold_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(3, 'Headings', 'Headings_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(4, 'Image', 'Image_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(5, 'Italic', 'Italic_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(6, 'Link', 'Link_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(7, 'Ordered List', 'Ordered_list_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(8, 'Underline', 'Underline_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(9, 'Unordered List', 'Unordered_list_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(10, 'View Source', 'View_source_rte', 'y');

/* Table structure for table `exp_rte_toolsets` */
DROP TABLE IF EXISTS `exp_rte_toolsets`;

CREATE TABLE `exp_rte_toolsets` (
  `toolset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `tools` text,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`toolset_id`),
  KEY `member_id` (`member_id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_rte_toolsets` */
INSERT INTO `exp_rte_toolsets` VALUES(1, 0, 'Default', '3|2|5|1|9|7|6|4|10', 'y');

/* Table structure for table `exp_search` */
DROP TABLE IF EXISTS `exp_search`;

CREATE TABLE `exp_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) NOT NULL DEFAULT '1',
  `search_date` int(10) NOT NULL,
  `keywords` varchar(60) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(6) NOT NULL,
  `per_page` tinyint(3) unsigned NOT NULL,
  `query` mediumtext,
  `custom_fields` mediumtext,
  `result_page` varchar(70) NOT NULL,
  PRIMARY KEY (`search_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


/* Table structure for table `exp_search_log` */
DROP TABLE IF EXISTS `exp_search_log`;

CREATE TABLE `exp_search_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `search_date` int(10) NOT NULL,
  `search_type` varchar(32) NOT NULL,
  `search_terms` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_security_hashes` */
DROP TABLE IF EXISTS `exp_security_hashes`;

CREATE TABLE `exp_security_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `hash` varchar(40) NOT NULL,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB AUTO_INCREMENT=1138 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_security_hashes` */
INSERT INTO `exp_security_hashes` VALUES(817, 1362955093, '127.0.0.1', 'd482213b6c9ae700aecf8ae9ff9e85ce5ddca1b3');
INSERT INTO `exp_security_hashes` VALUES(818, 1362957813, '127.0.0.1', 'c92f493d1a0ccd2e0f3b98832a305775f91bc1ce');
INSERT INTO `exp_security_hashes` VALUES(819, 1362957824, '127.0.0.1', 'ac14c53216ae9af1dd2749e0a4569899cc953262');
INSERT INTO `exp_security_hashes` VALUES(820, 1362957827, '127.0.0.1', 'f48ec1a98a717927179b3f167e5319a6abbfce7e');
INSERT INTO `exp_security_hashes` VALUES(821, 1362957827, '127.0.0.1', 'a19ddb1eae5aae58596945851c8b870602a9ed1c');
INSERT INTO `exp_security_hashes` VALUES(822, 1362957829, '127.0.0.1', 'b1de9961025bbf175e1f00fa9d765a1606b60032');
INSERT INTO `exp_security_hashes` VALUES(823, 1362957833, '127.0.0.1', 'c39b297cbea3d7cb3f38b963de36ac5db7f41027');
INSERT INTO `exp_security_hashes` VALUES(824, 1362957835, '127.0.0.1', '5eb360d42e808893184723fd494ea4558897ec5f');
INSERT INTO `exp_security_hashes` VALUES(825, 1362957836, '127.0.0.1', '39f6b3c0a241daedd61ef84db606030bb207c337');
INSERT INTO `exp_security_hashes` VALUES(826, 1362960488, '127.0.0.1', '54d074d531fcd61fb17b40606344f5d09bdf89c9');
INSERT INTO `exp_security_hashes` VALUES(827, 1362960556, '127.0.0.1', '1990656f8ace74f0dd1886b1fb01a84bfbd9e28c');
INSERT INTO `exp_security_hashes` VALUES(828, 1362960556, '127.0.0.1', '684635fc60d176974bf55c09ef60399601e10b25');
INSERT INTO `exp_security_hashes` VALUES(829, 1362960558, '127.0.0.1', 'cbad1f488cbfd104a8dbb39da633312f0bd59820');
INSERT INTO `exp_security_hashes` VALUES(830, 1362960564, '127.0.0.1', 'f9c051b4202d62d473713dc24f62e3fd3d409737');
INSERT INTO `exp_security_hashes` VALUES(831, 1362960564, '127.0.0.1', '682dc80ce427119c5362f718e313a0dbd46d5b9c');
INSERT INTO `exp_security_hashes` VALUES(832, 1362960567, '127.0.0.1', '92aca9b526ecf3e42367bcbad20db18a08e3ac90');
INSERT INTO `exp_security_hashes` VALUES(833, 1362960570, '127.0.0.1', '444f77a1cfd6d395190b49d11475f781c6a7a944');
INSERT INTO `exp_security_hashes` VALUES(834, 1362960570, '127.0.0.1', '77d085ea9bf30edbdc8f1ed3196b1c21bec9028d');
INSERT INTO `exp_security_hashes` VALUES(835, 1362960573, '127.0.0.1', '8a0769ce06e14b2632c286629253f69a6869293c');
INSERT INTO `exp_security_hashes` VALUES(836, 1362960578, '127.0.0.1', 'fa030057d906ed922e34715ce1dd6426bd34377a');
INSERT INTO `exp_security_hashes` VALUES(837, 1362960578, '127.0.0.1', '66d3bde20858cddb49fe4481a9138a75763d11be');
INSERT INTO `exp_security_hashes` VALUES(838, 1362960585, '127.0.0.1', 'a7ca57dc075f6623ce1f940ceeb325ff36780222');
INSERT INTO `exp_security_hashes` VALUES(839, 1362960587, '127.0.0.1', 'ad54f62b37cfeb4a337f94d5f51a0e9a6390768d');
INSERT INTO `exp_security_hashes` VALUES(840, 1362960832, '127.0.0.1', 'f0782663564aaa99dabca542c7b91e77e1d0aa62');
INSERT INTO `exp_security_hashes` VALUES(841, 1362960832, '127.0.0.1', 'a612b95578eb95a5d410c74037cad485c0e08cfd');
INSERT INTO `exp_security_hashes` VALUES(842, 1362960835, '127.0.0.1', 'c1f29bd5d48c5fec40909d6539305fab49d680b9');
INSERT INTO `exp_security_hashes` VALUES(843, 1362960840, '127.0.0.1', 'e3c1193c4d1d44ba533b869deeb4c866ea502991');
INSERT INTO `exp_security_hashes` VALUES(844, 1362960840, '127.0.0.1', 'fe6b43249608635fbfcd0e0056193cc5a1eee2ee');
INSERT INTO `exp_security_hashes` VALUES(845, 1362960843, '127.0.0.1', '68804ebf1ce87f9be9c93031f2fa749a1759f401');
INSERT INTO `exp_security_hashes` VALUES(846, 1362960858, '127.0.0.1', '474046bcd1e3f82605a0fdcc7f776584b119b021');
INSERT INTO `exp_security_hashes` VALUES(847, 1362960858, '127.0.0.1', '5d8aa5dad1638aeb2dd091a08e2b982308a06e6b');
INSERT INTO `exp_security_hashes` VALUES(848, 1362960961, '127.0.0.1', '70edda888ac8b8a17946ec1c8e84a8ff1751006b');
INSERT INTO `exp_security_hashes` VALUES(849, 1362960965, '127.0.0.1', '4ff3d033f481869515234daf0a265172263f0825');
INSERT INTO `exp_security_hashes` VALUES(850, 1362960968, '127.0.0.1', 'd7a2124f3b17202e109eef81f4058e11ae64ed2f');
INSERT INTO `exp_security_hashes` VALUES(851, 1362961088, '127.0.0.1', '8ad030baf9feecc04cd728e88c5b6d40125d7ddb');
INSERT INTO `exp_security_hashes` VALUES(852, 1362961102, '127.0.0.1', '939aff73e2121a37b8e9845c896e0960cb17698a');
INSERT INTO `exp_security_hashes` VALUES(853, 1362961102, '127.0.0.1', 'dd6d09fe5b9de0de35dc075cdc43f56ed2460f76');
INSERT INTO `exp_security_hashes` VALUES(854, 1362961106, '127.0.0.1', 'fab71a88b20509e1c71bb08798611c86844b8d2c');
INSERT INTO `exp_security_hashes` VALUES(855, 1362961106, '127.0.0.1', 'e9b848047e03e1a2fb5e989f266aff43465f2f22');
INSERT INTO `exp_security_hashes` VALUES(856, 1362961116, '127.0.0.1', '1a66d48a3d22440e4e5e95c792203d0a3baac530');
INSERT INTO `exp_security_hashes` VALUES(857, 1362961119, '127.0.0.1', '25b68df232e8073d247c4e230797acb5a01d1718');
INSERT INTO `exp_security_hashes` VALUES(858, 1362961137, '127.0.0.1', '94dfba43e4ca30609525c7d1bb06ef1f6192f5a2');
INSERT INTO `exp_security_hashes` VALUES(859, 1362961139, '127.0.0.1', 'f9137790723d85a9ea6929475b1e687c10ab7c8f');
INSERT INTO `exp_security_hashes` VALUES(860, 1362961140, '127.0.0.1', '17ad5dd814e9ba8732d0e808ab254885ac4e6510');
INSERT INTO `exp_security_hashes` VALUES(861, 1362961187, '127.0.0.1', 'a8353d5d958c9e4513896528c9d67116c159b08d');
INSERT INTO `exp_security_hashes` VALUES(862, 1362961189, '127.0.0.1', '0b6eb3d8d307ba37e24aa4dd4f6e06773f58952c');
INSERT INTO `exp_security_hashes` VALUES(863, 1362961263, '127.0.0.1', '7581631868437e1a23daeafb70230579993ee6ab');
INSERT INTO `exp_security_hashes` VALUES(864, 1362961263, '127.0.0.1', 'bd347cd62330768cbd121d19753d059e1a93543a');
INSERT INTO `exp_security_hashes` VALUES(865, 1362961278, '127.0.0.1', '77fb7a17fa18055c8cc9878c9d920f8e7713a309');
INSERT INTO `exp_security_hashes` VALUES(866, 1362961282, '127.0.0.1', '794530e27b61c92105306f42ae52baee13103dfc');
INSERT INTO `exp_security_hashes` VALUES(867, 1362961283, '127.0.0.1', 'ca4150556f6544f1742679d8627823f571607f2a');
INSERT INTO `exp_security_hashes` VALUES(868, 1362961294, '127.0.0.1', '96f6bc5faed396aa60f8310602665ad6cbc30bd8');
INSERT INTO `exp_security_hashes` VALUES(869, 1362961294, '127.0.0.1', 'a1d53d291ce668aa6ea58adc68b914fe3b9bf8c3');
INSERT INTO `exp_security_hashes` VALUES(870, 1362961295, '127.0.0.1', '6d0ca99f32b1d686d23b64d64cb2b8bbaa5ae956');
INSERT INTO `exp_security_hashes` VALUES(871, 1362961295, '127.0.0.1', 'fd95159857b67ba44bb387f3cfa7f8116dc5203b');
INSERT INTO `exp_security_hashes` VALUES(872, 1362961309, '127.0.0.1', 'f9f6ab8e72b398dc75dc9aefe61620a9d8ff24dc');
INSERT INTO `exp_security_hashes` VALUES(873, 1362961311, '127.0.0.1', '737552156ac3b28c42e8f1c100c65940bcfd9267');
INSERT INTO `exp_security_hashes` VALUES(874, 1362961313, '127.0.0.1', '166a06594588bec9dcba75fa10665380cb7cc762');
INSERT INTO `exp_security_hashes` VALUES(875, 1362961317, '127.0.0.1', '58db08d5f3d4945524018a7772c85dc93895d95d');
INSERT INTO `exp_security_hashes` VALUES(876, 1362961317, '127.0.0.1', '6ff37284996e5396be8dda7861fcd617426da09f');
INSERT INTO `exp_security_hashes` VALUES(877, 1362961320, '127.0.0.1', '8d2def319c9336d3176a31e9b21ffe0c9f8e08d4');
INSERT INTO `exp_security_hashes` VALUES(878, 1362961321, '127.0.0.1', 'f057963ade7c489aeb869165990bc641c632bca3');
INSERT INTO `exp_security_hashes` VALUES(879, 1362961321, '127.0.0.1', 'b7fbb41aceddad2427f59aad43dd672586f43cb7');
INSERT INTO `exp_security_hashes` VALUES(880, 1362961321, '127.0.0.1', '33fa9b030464e2efc6eb3cb99cbc55d0405a09df');
INSERT INTO `exp_security_hashes` VALUES(881, 1362961321, '127.0.0.1', 'cbd46cd05a89eb2ccecc09ce789004f249869d2c');
INSERT INTO `exp_security_hashes` VALUES(882, 1362961359, '127.0.0.1', '6f4fdc2ab8032d19453b545b3b13d95ae9598110');
INSERT INTO `exp_security_hashes` VALUES(883, 1362961361, '127.0.0.1', '0895d952584c087b9fe74f7c241c50cf26ba9d99');
INSERT INTO `exp_security_hashes` VALUES(884, 1362961363, '127.0.0.1', '8cdbb499c17968c8a16274442e42133f2be50261');
INSERT INTO `exp_security_hashes` VALUES(885, 1362961382, '127.0.0.1', '429b4be5ada7a02070b9e2cf3024412b5d253bed');
INSERT INTO `exp_security_hashes` VALUES(886, 1362961424, '127.0.0.1', '562b9040acb3156d7fb08ab9ce35677685201c94');
INSERT INTO `exp_security_hashes` VALUES(887, 1362961424, '127.0.0.1', '14b753549eaa5322b6d4b7616f2ca30b4b8ac07f');
INSERT INTO `exp_security_hashes` VALUES(888, 1362961428, '127.0.0.1', '0a95267e87bd715f85e37ca39fa7e1bc3b86ae0e');
INSERT INTO `exp_security_hashes` VALUES(889, 1362961429, '127.0.0.1', '945b867b09c7101f4d44e63cb64b5339975b5a08');
INSERT INTO `exp_security_hashes` VALUES(890, 1362961429, '127.0.0.1', 'a73bac58d363237f425a28b9752de7f837a98ac8');
INSERT INTO `exp_security_hashes` VALUES(891, 1362961429, '127.0.0.1', '1fc38a08d66b5d5331f7d965c45cc7b372295fc7');
INSERT INTO `exp_security_hashes` VALUES(892, 1362961429, '127.0.0.1', '3cd681d9c54c0e6988a0fdf954afa3ec5dbef3d5');
INSERT INTO `exp_security_hashes` VALUES(893, 1362961436, '127.0.0.1', '6b7ea59a1294355bd308716d3d46adde3a787690');
INSERT INTO `exp_security_hashes` VALUES(894, 1362961437, '127.0.0.1', '8314ebbe0027bb021ec43c8dc9493c2ea5c6b410');
INSERT INTO `exp_security_hashes` VALUES(895, 1362961438, '127.0.0.1', '9046c6e558d12350893ea28f6ffc2b63750e62e1');
INSERT INTO `exp_security_hashes` VALUES(896, 1362961446, '127.0.0.1', 'ac321e456d8dcc1e2cc34b6eef4ac177940e31df');
INSERT INTO `exp_security_hashes` VALUES(897, 1362961449, '127.0.0.1', '4cfc9b4e68dd9c642a23565043ffd63d583a1be5');
INSERT INTO `exp_security_hashes` VALUES(898, 1362961449, '127.0.0.1', '351a77ed56c516853a5daee15a44ea3f6f439083');
INSERT INTO `exp_security_hashes` VALUES(899, 1362961450, '127.0.0.1', 'e7c343b66e1b478d3f37109d1fb5f72e7deba686');
INSERT INTO `exp_security_hashes` VALUES(900, 1362961450, '127.0.0.1', '0760babd5829ebf576a4e09216ffb6719501bab6');
INSERT INTO `exp_security_hashes` VALUES(901, 1362961451, '127.0.0.1', '18e038463207fffc361408800a7689acd2077be1');
INSERT INTO `exp_security_hashes` VALUES(902, 1362961451, '127.0.0.1', 'f4c0a9b41cc350e6344f2d623b508db8adbc7566');
INSERT INTO `exp_security_hashes` VALUES(903, 1362961451, '127.0.0.1', '97d571c32b9a5870eb2fda4aa24b5f0059993207');
INSERT INTO `exp_security_hashes` VALUES(904, 1362961498, '127.0.0.1', 'abb3fb4e8e0edfbd56ced3e8933210552ca2c586');
INSERT INTO `exp_security_hashes` VALUES(905, 1362961500, '127.0.0.1', '9ab46ead06cd73f613b71a2d6c9e119fc9512143');
INSERT INTO `exp_security_hashes` VALUES(906, 1362961507, '127.0.0.1', 'ff1e124a4dabb775f83963a7fbff4458e94ce630');
INSERT INTO `exp_security_hashes` VALUES(907, 1362961511, '127.0.0.1', '357e26cd13c7e3521a01a917f9e43cd400f176e6');
INSERT INTO `exp_security_hashes` VALUES(908, 1362961520, '127.0.0.1', '50cfe2e06515d70071b163b30400632861a37394');
INSERT INTO `exp_security_hashes` VALUES(909, 1362961535, '127.0.0.1', 'ba3dd60b172c09c52e4e4763b3c7d3686661e25b');
INSERT INTO `exp_security_hashes` VALUES(910, 1362961607, '127.0.0.1', '96b7fbd105f7a8547daf2951cab010a7c891a0ae');
INSERT INTO `exp_security_hashes` VALUES(911, 1362961607, '127.0.0.1', '25b3ffe1c43e8cc281b69983dd510efa99d8b7b6');
INSERT INTO `exp_security_hashes` VALUES(912, 1362961610, '127.0.0.1', '4892aa80a00781e052df4ab6a3cbfa4daee29f74');
INSERT INTO `exp_security_hashes` VALUES(913, 1362961613, '127.0.0.1', 'ba21d225d14540eab439701499e2fc1c003249c5');
INSERT INTO `exp_security_hashes` VALUES(914, 1362961635, '127.0.0.1', '6945247f9a2d5e92496c0acda39635a869713331');
INSERT INTO `exp_security_hashes` VALUES(915, 1362961638, '127.0.0.1', '03adcea778151062d7e2fb33e0b98f4986a2e9fc');
INSERT INTO `exp_security_hashes` VALUES(916, 1362961638, '127.0.0.1', '102702c9c56dfc4dab64dc2924a666cb583e81ce');
INSERT INTO `exp_security_hashes` VALUES(917, 1362961638, '127.0.0.1', 'f83bade94605c0360487ba99fd474d083b5150c8');
INSERT INTO `exp_security_hashes` VALUES(918, 1362961639, '127.0.0.1', '8e3cfbc5c9fb68b9f1dc02fa1a1accd21e595538');
INSERT INTO `exp_security_hashes` VALUES(919, 1362961643, '127.0.0.1', 'c61e9b2d49c88f949e4325729d784f093254bb37');
INSERT INTO `exp_security_hashes` VALUES(920, 1362961643, '127.0.0.1', '76fa43015c8fbdb532d114cd8048404da17ab3ce');
INSERT INTO `exp_security_hashes` VALUES(921, 1362961648, '127.0.0.1', '4dd2748d9861ee4abe32a24428844e59ff3c147b');
INSERT INTO `exp_security_hashes` VALUES(922, 1362961650, '127.0.0.1', '27aca1fc17035b689ce1339eabb986864e2cfd35');
INSERT INTO `exp_security_hashes` VALUES(923, 1362961650, '127.0.0.1', '9e7ffc706aff074b1e1b103da090e6981e41cd82');
INSERT INTO `exp_security_hashes` VALUES(924, 1362961650, '127.0.0.1', '34d71ce126da83698bfe067bae7894d764d9c3f7');
INSERT INTO `exp_security_hashes` VALUES(925, 1362961651, '127.0.0.1', '6f98c05cbe4709cb7ef72fc15115c73ce1290567');
INSERT INTO `exp_security_hashes` VALUES(926, 1362961651, '127.0.0.1', 'd112aa4af788ebc12fa265db0b781c5555db131b');
INSERT INTO `exp_security_hashes` VALUES(927, 1362961656, '127.0.0.1', '17e140ef2fd94c015b8813110e1fdf4183c873a6');
INSERT INTO `exp_security_hashes` VALUES(928, 1362961656, '127.0.0.1', '27bb7aa1447f7b13f06e4b0cc99229d9684e0080');
INSERT INTO `exp_security_hashes` VALUES(929, 1362961657, '127.0.0.1', 'c858c76e5350b73e822be899ee98f09be388d48f');
INSERT INTO `exp_security_hashes` VALUES(930, 1362961658, '127.0.0.1', '80ebc28f6dbb8241a8aa67cc4ae00a6350825b8d');
INSERT INTO `exp_security_hashes` VALUES(931, 1362961658, '127.0.0.1', '3e58a0f00a0476e08b717dd593515ce4fa7c6f67');
INSERT INTO `exp_security_hashes` VALUES(932, 1362961658, '127.0.0.1', 'a42cd1683f9232d941361f0ce596539a88bf8a1a');
INSERT INTO `exp_security_hashes` VALUES(933, 1362961658, '127.0.0.1', '5876d20024b480f383d3575ebd64b06efc77e66d');
INSERT INTO `exp_security_hashes` VALUES(934, 1362961661, '127.0.0.1', '2f8978fc6781bc158fe02bb2f211bb3101a9750c');
INSERT INTO `exp_security_hashes` VALUES(935, 1362961663, '127.0.0.1', '06ad9ef043fe58cabe6f5614416069d7ca863d9e');
INSERT INTO `exp_security_hashes` VALUES(936, 1362961663, '127.0.0.1', '53a55198421f78ea054850f0900b3b0407504172');
INSERT INTO `exp_security_hashes` VALUES(937, 1362961663, '127.0.0.1', '338d17bd6f25356a3fbecb2ae148c914df31db93');
INSERT INTO `exp_security_hashes` VALUES(938, 1362961663, '127.0.0.1', '106a65d8a27e82686c1955d2bb23e768eea0126a');
INSERT INTO `exp_security_hashes` VALUES(939, 1362961663, '127.0.0.1', '38d7178c495350f25087979875da879bb38720db');
INSERT INTO `exp_security_hashes` VALUES(940, 1362961667, '127.0.0.1', 'ec05c7459f29568c829549eb1e350a2d51bf2cc7');
INSERT INTO `exp_security_hashes` VALUES(941, 1362961668, '127.0.0.1', 'e43f7a8bb2ccef288f7786848b6e2e005e4650d0');
INSERT INTO `exp_security_hashes` VALUES(942, 1362961810, '127.0.0.1', 'd53da6fd081d7f79f77bb12bd435d47a24fcf866');
INSERT INTO `exp_security_hashes` VALUES(943, 1362961813, '127.0.0.1', '227184703d631e92711b577f8e682ae6f9670732');
INSERT INTO `exp_security_hashes` VALUES(944, 1362961828, '127.0.0.1', '0027363992a03f26ee3ebc9f23b4896f338ea173');
INSERT INTO `exp_security_hashes` VALUES(945, 1362961830, '127.0.0.1', '55a1efaab721158878f329d343ec7743699d78a4');
INSERT INTO `exp_security_hashes` VALUES(946, 1362961941, '127.0.0.1', '717a2bf345db4aa8ddbc225c64ab5caae31aed5b');
INSERT INTO `exp_security_hashes` VALUES(947, 1362961957, '127.0.0.1', '54dce035b037b078378825c70a52433d0cc149fb');
INSERT INTO `exp_security_hashes` VALUES(948, 1362961962, '127.0.0.1', 'd38d457fef93412efb7c91d43f1e0f06ef9c5ec1');
INSERT INTO `exp_security_hashes` VALUES(949, 1362961967, '127.0.0.1', 'a35fe34313d6c2e2d4bb5b3e2872843e3d2a3ff0');
INSERT INTO `exp_security_hashes` VALUES(950, 1362961986, '127.0.0.1', '7496c1aaace9727b44ef08000e8aa2b20bdd4e57');
INSERT INTO `exp_security_hashes` VALUES(951, 1362961998, '127.0.0.1', '57e2a1354e1b7651070d978126d1ee8fa332d378');
INSERT INTO `exp_security_hashes` VALUES(952, 1362962035, '127.0.0.1', 'accea9a7a8ef2fd87ac2596118c0ea2eaa7a1534');
INSERT INTO `exp_security_hashes` VALUES(953, 1362962037, '127.0.0.1', '41485515d10551213cc6f9b583af7a1f7e1cd928');
INSERT INTO `exp_security_hashes` VALUES(954, 1362962242, '127.0.0.1', '08744c280092a70634d831ab8f967d766872c5d9');
INSERT INTO `exp_security_hashes` VALUES(955, 1362962244, '127.0.0.1', '745196c31a06afcf88806e9bb0d776b639e00b8d');
INSERT INTO `exp_security_hashes` VALUES(956, 1362962245, '127.0.0.1', '83eaf5f36478df48e99bd55b2f6fb446f1067879');
INSERT INTO `exp_security_hashes` VALUES(957, 1362962245, '127.0.0.1', '73fdd3e9d5b429ed6aa61dff489a52f0e8dd2d7c');
INSERT INTO `exp_security_hashes` VALUES(958, 1362962245, '127.0.0.1', 'e63489e0c1d5c1bb7acc3fb3deda0284c696aafa');
INSERT INTO `exp_security_hashes` VALUES(959, 1362962245, '127.0.0.1', '702bf5e2a5354d809c8180010dbf8141c7d7a32f');
INSERT INTO `exp_security_hashes` VALUES(960, 1362962245, '127.0.0.1', '6f0db6b1cf9d068000cf50d6a7448cf3485eeb10');
INSERT INTO `exp_security_hashes` VALUES(961, 1362962245, '127.0.0.1', '3d12b9e3c768b82def325c7df42aeca0e77895f1');
INSERT INTO `exp_security_hashes` VALUES(962, 1362962245, '127.0.0.1', '80af3be875122d5210faab0debb9048eff781733');
INSERT INTO `exp_security_hashes` VALUES(963, 1362962246, '127.0.0.1', 'b25dc0cc04394b02beebe21b7f6adbef0c77d3bb');
INSERT INTO `exp_security_hashes` VALUES(964, 1362962246, '127.0.0.1', '511289a9dbd6af8a140e9597bd4a50c274f760ca');
INSERT INTO `exp_security_hashes` VALUES(965, 1362962246, '127.0.0.1', 'b59672875dffee18740b6d3cb563aa5c28f4ce5c');
INSERT INTO `exp_security_hashes` VALUES(966, 1362962246, '127.0.0.1', '7e4bcc0123e5ab7335ffaf30a5e8d02c37ba1dd4');
INSERT INTO `exp_security_hashes` VALUES(967, 1362962246, '127.0.0.1', '490a89bf984a231c0dcdd9e60350ef2e010e6412');
INSERT INTO `exp_security_hashes` VALUES(968, 1362962246, '127.0.0.1', '7b813603503ac94b927fe99c7a6f8e293260789f');
INSERT INTO `exp_security_hashes` VALUES(969, 1362962246, '127.0.0.1', '83e1235e8cfcf2e12fe1f4ff3eb16d07430f5679');
INSERT INTO `exp_security_hashes` VALUES(970, 1362962246, '127.0.0.1', '7bbc78a2cca4d278dde9aa15c0edef1e4397c353');
INSERT INTO `exp_security_hashes` VALUES(971, 1362962246, '127.0.0.1', 'e4a429f91c73535936753728a82277a46f009ff2');
INSERT INTO `exp_security_hashes` VALUES(972, 1362962247, '127.0.0.1', 'c82d2863274470f0a031fbcea1ced6dee68f4ae5');
INSERT INTO `exp_security_hashes` VALUES(973, 1362962247, '127.0.0.1', '72c029d4d06fdb30daa873fd51080c0f5e52b787');
INSERT INTO `exp_security_hashes` VALUES(974, 1362962247, '127.0.0.1', '29bc12b7d131160ddf4d3c4b05a6f12060618e0a');
INSERT INTO `exp_security_hashes` VALUES(975, 1362962250, '127.0.0.1', '491f7d2ed1508fe976f31db10146e61949c35b05');
INSERT INTO `exp_security_hashes` VALUES(976, 1362962251, '127.0.0.1', 'e14ea69c3b988ad9f384bf1355866615764916f5');
INSERT INTO `exp_security_hashes` VALUES(977, 1362962255, '127.0.0.1', '227b2d13b26b9ea4584eabee22a267dda3a53daa');
INSERT INTO `exp_security_hashes` VALUES(978, 1362962255, '127.0.0.1', '61b52e604ec0411328850b2dcf796a69302daefd');
INSERT INTO `exp_security_hashes` VALUES(979, 1362962256, '127.0.0.1', 'e1afae2c533de2d285f8cc76b645c4c417216081');
INSERT INTO `exp_security_hashes` VALUES(980, 1362962256, '127.0.0.1', 'f6f74b2b60acfc517c26776db573e157fd71b722');
INSERT INTO `exp_security_hashes` VALUES(981, 1362962256, '127.0.0.1', 'd982272213cb8856f89add347e6440b73929b0aa');
INSERT INTO `exp_security_hashes` VALUES(982, 1362962258, '127.0.0.1', 'e56f2297f3b386aeda0a5de4f27174a2b8221da9');
INSERT INTO `exp_security_hashes` VALUES(983, 1362962258, '127.0.0.1', '0e05098a216948f4da771195e6a78b408e3d6d86');
INSERT INTO `exp_security_hashes` VALUES(984, 1362962262, '127.0.0.1', 'e71a0ccdccc6d0dd51b8f7964161c4469444c3c0');
INSERT INTO `exp_security_hashes` VALUES(985, 1362962262, '127.0.0.1', '66c73feb47a1cc76ac9b6d3c207f53ed840c98e6');
INSERT INTO `exp_security_hashes` VALUES(986, 1362962266, '127.0.0.1', '40683ac5dc08c9192a5619d981b9e5e224d004d3');
INSERT INTO `exp_security_hashes` VALUES(987, 1362962266, '127.0.0.1', '8e71da8cde9da35d9bfd3ca092b42cbb6d1f48c4');
INSERT INTO `exp_security_hashes` VALUES(988, 1362962293, '127.0.0.1', '74f24dc7398f0ae574b8046b798c6edbfb1e5903');
INSERT INTO `exp_security_hashes` VALUES(989, 1362962296, '127.0.0.1', '36028e063d55c1a1fd825bcaa15287b651f6d2b7');
INSERT INTO `exp_security_hashes` VALUES(990, 1362962297, '127.0.0.1', 'cef6713829f308bcd42137055429a10c1ff579f6');
INSERT INTO `exp_security_hashes` VALUES(991, 1362962297, '127.0.0.1', '8bca7ede301eb75cb317995ad4ffb219e5c9dbe4');
INSERT INTO `exp_security_hashes` VALUES(992, 1362962297, '127.0.0.1', '5768501e2d869fd248607bcac94ef0a8cc1f69e8');
INSERT INTO `exp_security_hashes` VALUES(993, 1362962297, '127.0.0.1', '32a3ff61976e5af6c61f6c28b198127fd80606ca');
INSERT INTO `exp_security_hashes` VALUES(994, 1362962303, '127.0.0.1', '3557a542d395bbe7bc2ec0241dd87deb08cc6ec5');
INSERT INTO `exp_security_hashes` VALUES(995, 1362962303, '127.0.0.1', 'de1297c34a4eb5ac3560c021d623ea1b9fcbbb3b');
INSERT INTO `exp_security_hashes` VALUES(996, 1362962306, '127.0.0.1', '5bd3950b9de1cde508d636b84174d10f0eb70273');
INSERT INTO `exp_security_hashes` VALUES(997, 1362962308, '127.0.0.1', '51fe99b0403da8c26da47c7ada75a4b67653f3f4');
INSERT INTO `exp_security_hashes` VALUES(998, 1362962308, '127.0.0.1', 'fe369423e382bee86ec6f39ae2ed4bbcea31136a');
INSERT INTO `exp_security_hashes` VALUES(999, 1362962308, '127.0.0.1', '8829522f3012a9bbb0729cae33fe674ab4ebc77d');
INSERT INTO `exp_security_hashes` VALUES(1000, 1362962308, '127.0.0.1', 'dcd07a3c868c628e7b6bf967734b9b0eab1248b7');
INSERT INTO `exp_security_hashes` VALUES(1001, 1362962308, '127.0.0.1', '05ce53041cc6731506eb957d52f00b75ecb0a08e');
INSERT INTO `exp_security_hashes` VALUES(1002, 1362962313, '127.0.0.1', 'da0427b895b35e7d2e5a329d574a88f740571e39');
INSERT INTO `exp_security_hashes` VALUES(1003, 1362962313, '127.0.0.1', '90e5953b8c9481afa38583bbc488cf65208e1b6d');
INSERT INTO `exp_security_hashes` VALUES(1004, 1362962314, '127.0.0.1', '99a3c8042032ee75c6aeb523a4b0ce9a4bdd0378');
INSERT INTO `exp_security_hashes` VALUES(1005, 1362962314, '127.0.0.1', '12429f3dea44a6768b28bd6651595d8fd3d6686a');
INSERT INTO `exp_security_hashes` VALUES(1006, 1362962314, '127.0.0.1', '8ef791d9e01460768313247fb22b70a10670cc3a');
INSERT INTO `exp_security_hashes` VALUES(1007, 1362962317, '127.0.0.1', 'fd1d9f8992895f10e26729f6acaef2f8abc0bad7');
INSERT INTO `exp_security_hashes` VALUES(1008, 1362962318, '127.0.0.1', '95d9749b3e25d7071e51c78b690eac8d4d43c3d0');
INSERT INTO `exp_security_hashes` VALUES(1009, 1362962318, '127.0.0.1', '3bc73ebdcaa3ef1ee4fec0cd03b1a9cca26140f3');
INSERT INTO `exp_security_hashes` VALUES(1010, 1362962318, '127.0.0.1', '8f5fdaaf514e8d4ddd0d1411c4d36bb7d6294c54');
INSERT INTO `exp_security_hashes` VALUES(1011, 1362962318, '127.0.0.1', 'd1db707bb1545e4249ef979b577743268177efd9');
INSERT INTO `exp_security_hashes` VALUES(1012, 1362962320, '127.0.0.1', '7858cb9e0b8625830c7ce26bfb4624cee2c3c446');
INSERT INTO `exp_security_hashes` VALUES(1013, 1362962320, '127.0.0.1', '3b60913099e0022e088701daab7baf458fc5a7ba');
INSERT INTO `exp_security_hashes` VALUES(1014, 1362962320, '127.0.0.1', '5aeebfac4aa0cb0e9e45f968d7a5e0cd4c42491a');
INSERT INTO `exp_security_hashes` VALUES(1015, 1362962320, '127.0.0.1', 'e996c6d56cd03f2272202e8e7eac8dfd3ca674ee');
INSERT INTO `exp_security_hashes` VALUES(1016, 1362962320, '127.0.0.1', '58b28fabdd264811d1cead24c03cd1464dd56781');
INSERT INTO `exp_security_hashes` VALUES(1017, 1362962324, '127.0.0.1', 'f9bcb2eebd0a3a53988299e5de69919da8f8088d');
INSERT INTO `exp_security_hashes` VALUES(1018, 1362962324, '127.0.0.1', 'b417b0d72518e5bf9dade8e2ceb13f562551cb69');
INSERT INTO `exp_security_hashes` VALUES(1019, 1362962326, '127.0.0.1', '89db2b291fd179c23e768ea15fa32d0f3caab69d');
INSERT INTO `exp_security_hashes` VALUES(1020, 1362962332, '127.0.0.1', '0b40a4803d4f1286cdf11d4f1800f3e4ecea2f66');
INSERT INTO `exp_security_hashes` VALUES(1021, 1362962332, '127.0.0.1', 'b057c1c548937cb7032010076c6ec7c238f9eb7f');
INSERT INTO `exp_security_hashes` VALUES(1022, 1362962332, '127.0.0.1', '3d747cc4b8062f2918c0682e2ad492b835d77c54');
INSERT INTO `exp_security_hashes` VALUES(1023, 1362962333, '127.0.0.1', '36754bf2d069a3da789667a53477f417edb173ef');
INSERT INTO `exp_security_hashes` VALUES(1024, 1362962333, '127.0.0.1', '8ee848e934561f00d686257003a30cbded26e325');
INSERT INTO `exp_security_hashes` VALUES(1025, 1362962344, '127.0.0.1', 'c0739ec1157816d16afa11ed2c665e8e53484603');
INSERT INTO `exp_security_hashes` VALUES(1026, 1362962344, '127.0.0.1', 'd9bbec3f2540c5d5b6ac4af223736b1ddd4d9827');
INSERT INTO `exp_security_hashes` VALUES(1027, 1362962345, '127.0.0.1', '574accb23c99ab642e75d8651f89537f675592c3');
INSERT INTO `exp_security_hashes` VALUES(1028, 1362962349, '127.0.0.1', '323805a85d564f2fe2e24cc5130c2c435a496467');
INSERT INTO `exp_security_hashes` VALUES(1029, 1362962349, '127.0.0.1', '27b733d2e63b26ed30382d1b1c3d1ab73a6f1e2c');
INSERT INTO `exp_security_hashes` VALUES(1030, 1362962349, '127.0.0.1', '13a077e663abd568257c786e6c94777824c0ddb3');
INSERT INTO `exp_security_hashes` VALUES(1031, 1362962349, '127.0.0.1', '428dfcf0d6e164f4e1bd0efd8759ef4f9ae1b5a3');
INSERT INTO `exp_security_hashes` VALUES(1032, 1362962349, '127.0.0.1', '1182625b83e75002c4d82fd571605ece736f7c17');
INSERT INTO `exp_security_hashes` VALUES(1033, 1362962353, '127.0.0.1', 'a59087dd2aeaaad851197c8fb87d84a4a0ba9fce');
INSERT INTO `exp_security_hashes` VALUES(1034, 1362962353, '127.0.0.1', '79fa09ce7242125a12047057a0235a77aae26520');
INSERT INTO `exp_security_hashes` VALUES(1035, 1362962355, '127.0.0.1', 'ffe549124c1528d59fd07ec1d2485a0657a37ba0');
INSERT INTO `exp_security_hashes` VALUES(1036, 1362962359, '127.0.0.1', '2520cd253f6a3361bc1cf0e5ea720a6215316bfc');
INSERT INTO `exp_security_hashes` VALUES(1037, 1362962359, '127.0.0.1', '0e69b73573de2bc55b6af057ee0ad8ecd328ccb1');
INSERT INTO `exp_security_hashes` VALUES(1038, 1362962360, '127.0.0.1', 'c145b0afbd11680082fb142305bb94331d3d0929');
INSERT INTO `exp_security_hashes` VALUES(1039, 1362962360, '127.0.0.1', 'a0322a6dee819742c8a6d6a38a9f5166cfa7dda0');
INSERT INTO `exp_security_hashes` VALUES(1040, 1362962360, '127.0.0.1', 'a956359f2bcf9924a6f8ba6022bbe22c6c6b0d88');
INSERT INTO `exp_security_hashes` VALUES(1041, 1362962364, '127.0.0.1', '74d0f0d429cf137cf484a8d3c75a87be6d679aea');
INSERT INTO `exp_security_hashes` VALUES(1042, 1362962365, '127.0.0.1', '9b05820114ec384b1eef83063542605c0a8ab6ca');
INSERT INTO `exp_security_hashes` VALUES(1043, 1362963971, '127.0.0.1', 'fcb5d30885f7c8d414508886d8fb38bc57bea654');
INSERT INTO `exp_security_hashes` VALUES(1044, 1362963988, '127.0.0.1', '2cea6543a4e4abb50e3cc748d5e3cbc96d015cf8');
INSERT INTO `exp_security_hashes` VALUES(1045, 1362964986, '127.0.0.1', '620a6404c11fea520dbede90584c400a8451ef20');
INSERT INTO `exp_security_hashes` VALUES(1046, 1362964988, '127.0.0.1', '22e37fcd7643268cf36875c90b5e04e7b1ca7813');
INSERT INTO `exp_security_hashes` VALUES(1047, 1362965086, '127.0.0.1', '059e035f68a46082ef2cfb6eb41c7b93a752c266');
INSERT INTO `exp_security_hashes` VALUES(1048, 1362965104, '127.0.0.1', '724e840187202e1b0cd383acc23cdc02338ba54c');
INSERT INTO `exp_security_hashes` VALUES(1049, 1362965113, '127.0.0.1', 'e6235b88a30801d13fab9848a54e42a54a940728');
INSERT INTO `exp_security_hashes` VALUES(1050, 1362965116, '127.0.0.1', '1b44ed7c5b0cfc2d67f2d5d44db52947caad73bd');
INSERT INTO `exp_security_hashes` VALUES(1051, 1362965117, '127.0.0.1', '6f3c8e28e93ce367b60b2048079a4cf10b447986');
INSERT INTO `exp_security_hashes` VALUES(1052, 1362965179, '127.0.0.1', 'cd881f9bf730f17e32617b8bdbdf1e88762d36f1');
INSERT INTO `exp_security_hashes` VALUES(1053, 1362965181, '127.0.0.1', '96744b85073688cd0e2fe1a61ae81ed9debf67f5');
INSERT INTO `exp_security_hashes` VALUES(1054, 1362965418, '127.0.0.1', 'bc4a10260525a259e0eeaf7e81097f7343609bcb');
INSERT INTO `exp_security_hashes` VALUES(1055, 1362965419, '127.0.0.1', '6d202e4cf1551417a08bddd659c1a00f0c4a8cb6');
INSERT INTO `exp_security_hashes` VALUES(1056, 1362965421, '127.0.0.1', '477ea96fb28fd9cb4f2280ada81d9f8472dac7be');
INSERT INTO `exp_security_hashes` VALUES(1057, 1362965432, '127.0.0.1', 'a53b7d76d600a4000e980b0216bc3a7a7c1266e3');
INSERT INTO `exp_security_hashes` VALUES(1058, 1362965433, '127.0.0.1', 'fa548d045f10eaf7cbc1bf0898e868b66881022d');
INSERT INTO `exp_security_hashes` VALUES(1059, 1362965438, '127.0.0.1', '7909a3b65bf142f589d4e871bbea56e92f558274');
INSERT INTO `exp_security_hashes` VALUES(1060, 1362965438, '127.0.0.1', 'eb6e909cfee9b1c1fe5f79ffc17a9dcad3dfa01a');
INSERT INTO `exp_security_hashes` VALUES(1061, 1362965439, '127.0.0.1', '40366a63e551b440e8e0356e46861a6a893fb4c7');
INSERT INTO `exp_security_hashes` VALUES(1062, 1362966682, '127.0.0.1', '8e7515b905ca7619fdacdda29bb032caa6a4fe44');
INSERT INTO `exp_security_hashes` VALUES(1063, 1362966683, '127.0.0.1', '44dac44f9a6f71417980605d7f000b3f74b2fab1');
INSERT INTO `exp_security_hashes` VALUES(1064, 1362966688, '127.0.0.1', '5eddae7b8bc0384e0d544d264a68bc348a1edf58');
INSERT INTO `exp_security_hashes` VALUES(1065, 1362966688, '127.0.0.1', '3d48c87cd21519cca81d0e677b318f36f7c39941');
INSERT INTO `exp_security_hashes` VALUES(1066, 1362966723, '127.0.0.1', 'cdb2421490fa1d7c10717efaf0f87e909bbf0b4a');
INSERT INTO `exp_security_hashes` VALUES(1067, 1362966723, '127.0.0.1', '5af2a91a03d573d7d44c70969ff66787667c7678');
INSERT INTO `exp_security_hashes` VALUES(1068, 1362966726, '127.0.0.1', '4b24035f509e6b8bf804f4723deefcd7cf83764b');
INSERT INTO `exp_security_hashes` VALUES(1069, 1362966730, '127.0.0.1', '2b6390913fa4bc7cd606df7206b68eeab54069c9');
INSERT INTO `exp_security_hashes` VALUES(1070, 1362966730, '127.0.0.1', '901c9f5b32f84cac94cdf0c35ba832c31b0cc042');
INSERT INTO `exp_security_hashes` VALUES(1071, 1362966732, '127.0.0.1', 'd81161d0b276cd747e82c43e203ee4ee1463097a');
INSERT INTO `exp_security_hashes` VALUES(1072, 1362966734, '127.0.0.1', 'e7b2fd1f7abd82decf62def32c1634db7f4258c1');
INSERT INTO `exp_security_hashes` VALUES(1073, 1362966735, '127.0.0.1', '9b668a937129dcb94b8f49f4971b02b68f99aae3');
INSERT INTO `exp_security_hashes` VALUES(1074, 1362966737, '127.0.0.1', '9374f7d8d8f3f2526f2e5e03edfed907b0b62d05');
INSERT INTO `exp_security_hashes` VALUES(1075, 1362966739, '127.0.0.1', '3a2c07cb49cd3df028bd962325a9d3f639001175');
INSERT INTO `exp_security_hashes` VALUES(1076, 1362966744, '127.0.0.1', '79f0da38a26fc6e1a6844520dd672a5820d554d5');
INSERT INTO `exp_security_hashes` VALUES(1077, 1362966744, '127.0.0.1', 'c270244868423cb713aede1ac0f82a048e34ea60');
INSERT INTO `exp_security_hashes` VALUES(1078, 1362966747, '127.0.0.1', '2370007319b6ded5fb78f0155a29697db5ea1fb2');
INSERT INTO `exp_security_hashes` VALUES(1079, 1362966759, '127.0.0.1', '1f0c9b2f5effa4e0663d676800194a87d4059832');
INSERT INTO `exp_security_hashes` VALUES(1080, 1362966797, '127.0.0.1', '7f3cd3c59432e9d3d50aac9fd23278933ab8a6fd');
INSERT INTO `exp_security_hashes` VALUES(1081, 1362966797, '127.0.0.1', '6a3cd28ada1c58c6c10e5e7f2a952e79695f7174');
INSERT INTO `exp_security_hashes` VALUES(1082, 1362966799, '127.0.0.1', '21673a0abdc1f3413f16811dcfe4c7ce526bac63');
INSERT INTO `exp_security_hashes` VALUES(1083, 1362967158, '127.0.0.1', 'f69253f21f423ee379dbc2148cea78d75fc86ecc');
INSERT INTO `exp_security_hashes` VALUES(1084, 1362967158, '127.0.0.1', '31709da2f8d81d4fafc3a0c631754855030e0912');
INSERT INTO `exp_security_hashes` VALUES(1085, 1362967159, '127.0.0.1', '3f202e51f95b8a9ddae6d65adf2a86cc9b1873f9');
INSERT INTO `exp_security_hashes` VALUES(1086, 1362967160, '127.0.0.1', '932b5df786434a8e5b87eeae2a2ca23ff702d046');
INSERT INTO `exp_security_hashes` VALUES(1087, 1362967160, '127.0.0.1', 'cdca6cfa757926aa5ad8fb16847a6952faa22029');
INSERT INTO `exp_security_hashes` VALUES(1088, 1362967160, '127.0.0.1', '844773d978585787a0374049ecd12b554175c29f');
INSERT INTO `exp_security_hashes` VALUES(1089, 1362967160, '127.0.0.1', 'e56cb58820405aef755f3b2fd5f082b3743d7637');
INSERT INTO `exp_security_hashes` VALUES(1090, 1362967188, '127.0.0.1', 'cd90024758f5312cc862c5186876680c5992b6f0');
INSERT INTO `exp_security_hashes` VALUES(1091, 1362967205, '127.0.0.1', '46a9c5040658cb354b8f02e372605327ab833553');
INSERT INTO `exp_security_hashes` VALUES(1092, 1362967206, '127.0.0.1', '0bbc23b9b242ec1095b26b2e92095d742f1cd1f3');
INSERT INTO `exp_security_hashes` VALUES(1093, 1362967208, '127.0.0.1', 'de73933cceb5859cc1da81383b030507c45ee013');
INSERT INTO `exp_security_hashes` VALUES(1094, 1362967219, '127.0.0.1', '132adba916ae5b055a3320494bfdb524c25d3542');
INSERT INTO `exp_security_hashes` VALUES(1095, 1362967220, '127.0.0.1', '0f2bfe2b1e056082c1dbc013ac0e72612370ca73');
INSERT INTO `exp_security_hashes` VALUES(1096, 1362967220, '127.0.0.1', '3f64a5d21c693164aa1c284eec74886570924462');
INSERT INTO `exp_security_hashes` VALUES(1097, 1362967221, '127.0.0.1', 'cc125a6a1e5034b0473a43cdc7b4a30324d24c08');
INSERT INTO `exp_security_hashes` VALUES(1098, 1362967221, '127.0.0.1', '63781cc1f8e90381efa146867f201f4fd894212f');
INSERT INTO `exp_security_hashes` VALUES(1099, 1362967221, '127.0.0.1', '61c9003ad03abcd6a490daa251a142a151e7e4ba');
INSERT INTO `exp_security_hashes` VALUES(1100, 1362967221, '127.0.0.1', 'de80d9514516a2881c7e42298ced527da115785e');
INSERT INTO `exp_security_hashes` VALUES(1101, 1362967270, '127.0.0.1', '6cdb0bd827fee81114d015d39ad5dbf069645dfd');
INSERT INTO `exp_security_hashes` VALUES(1102, 1362967317, '127.0.0.1', 'd99faf4887cbebef5e4220390cf1cdf973cd03d4');
INSERT INTO `exp_security_hashes` VALUES(1103, 1362967318, '127.0.0.1', '46b51a66c53ab58ab630619713007580f8a94fb2');
INSERT INTO `exp_security_hashes` VALUES(1104, 1362967325, '127.0.0.1', '8639a0f0fafcda977f3eb835815edb6436b4a133');
INSERT INTO `exp_security_hashes` VALUES(1105, 1362967325, '127.0.0.1', '8c9858c69253e83a93d5303f5cbd26fd810855d4');
INSERT INTO `exp_security_hashes` VALUES(1106, 1362967326, '127.0.0.1', '20e724aa8e4bc03de5ef0d130496d913b8bf9633');
INSERT INTO `exp_security_hashes` VALUES(1107, 1362967326, '127.0.0.1', '040c5a5d4fd7f57cd3d02c676fa65ff336809463');
INSERT INTO `exp_security_hashes` VALUES(1108, 1362967326, '127.0.0.1', '0eec9685851e5d3adbb68c49d367ff03e09cdac5');
INSERT INTO `exp_security_hashes` VALUES(1109, 1362967336, '127.0.0.1', 'f45b54ab526d4112af3f061981d2c0330c065c08');
INSERT INTO `exp_security_hashes` VALUES(1110, 1362967372, '127.0.0.1', '26467de1e3cab11b7af24cc72a4eb89551bcb86b');
INSERT INTO `exp_security_hashes` VALUES(1111, 1362967373, '127.0.0.1', 'c59e8b1bc37746af592cad401adf8e0403a6283c');
INSERT INTO `exp_security_hashes` VALUES(1112, 1362967375, '127.0.0.1', '7886608667a25cc272fefc58cabb6689e023132e');
INSERT INTO `exp_security_hashes` VALUES(1113, 1362967375, '127.0.0.1', '4875368791316212f8552060964088f38404dca2');
INSERT INTO `exp_security_hashes` VALUES(1114, 1362967375, '127.0.0.1', '4a842de775a2865fdd7d1d4a3953986a4c3bd5bc');
INSERT INTO `exp_security_hashes` VALUES(1115, 1362967376, '127.0.0.1', 'f4a8716ef5b1e91a3e26f85fa47c4e7b1c2fb1d4');
INSERT INTO `exp_security_hashes` VALUES(1116, 1362967376, '127.0.0.1', '53dad93ccd61a07599df5e0a816bc2990426597a');
INSERT INTO `exp_security_hashes` VALUES(1117, 1362967441, '127.0.0.1', '6156041b345ee1f1abad41185049393b310f437a');
INSERT INTO `exp_security_hashes` VALUES(1118, 1362967503, '127.0.0.1', '65c0502d3a655d216e79413629e8c20c67584442');
INSERT INTO `exp_security_hashes` VALUES(1119, 1362967503, '127.0.0.1', '818991329a953f7f45f47c597463b86abb5e3ce6');
INSERT INTO `exp_security_hashes` VALUES(1120, 1362967616, '127.0.0.1', '04a4ca2613f7ae1c996c27ea7978bf84d8fa0a23');
INSERT INTO `exp_security_hashes` VALUES(1121, 1362967630, '127.0.0.1', 'eabecaac514a32ae63d2f42611ce81180346cd31');
INSERT INTO `exp_security_hashes` VALUES(1122, 1362967632, '127.0.0.1', '13619641a2b74a591471869487d38cdf52eee42f');
INSERT INTO `exp_security_hashes` VALUES(1123, 1362967632, '127.0.0.1', '524ccc055b316768fa00c8ede0217f3cee96ea75');
INSERT INTO `exp_security_hashes` VALUES(1124, 1362967633, '127.0.0.1', 'd63cddb1611a1c48b026b2f54d8cb58eb7d44c09');
INSERT INTO `exp_security_hashes` VALUES(1125, 1362967633, '127.0.0.1', '370b255846be1f2ea1ac40c1009d847093ee1b9d');
INSERT INTO `exp_security_hashes` VALUES(1126, 1362967633, '127.0.0.1', '2358ccfc4b3d5284ba2a13176661ee6b97a3cc66');
INSERT INTO `exp_security_hashes` VALUES(1127, 1362967702, '127.0.0.1', '4df073767d76db05db20bac23b4b05cc1c795f80');
INSERT INTO `exp_security_hashes` VALUES(1128, 1362967703, '127.0.0.1', '7ce98f00a7a79ec9513031766b15eac440b56706');
INSERT INTO `exp_security_hashes` VALUES(1129, 1362967807, '127.0.0.1', '5e5e0138163c3439979be5ef9e3ac0ca550a589d');
INSERT INTO `exp_security_hashes` VALUES(1130, 1362967881, '127.0.0.1', '27011203e2c9a80b1ae3bd7852d7cca42f1482f0');
INSERT INTO `exp_security_hashes` VALUES(1131, 1362967979, '127.0.0.1', '2204f6f41923366c0361db2fa090a26c237af0b7');
INSERT INTO `exp_security_hashes` VALUES(1132, 1362968933, '127.0.0.1', '2981ede67703ee67c34e0b34ba271f068978444e');
INSERT INTO `exp_security_hashes` VALUES(1133, 1362969352, '127.0.0.1', '004c196ff838a34ffd5e4540838d3e0a78e2bb74');
INSERT INTO `exp_security_hashes` VALUES(1134, 1362969354, '127.0.0.1', 'fa86855a844a9ab5b6563d0b92936e933fe41da3');
INSERT INTO `exp_security_hashes` VALUES(1135, 1362969356, '127.0.0.1', 'ff88f2fbb469158a3a7b2ff077c2e762fe63a242');
INSERT INTO `exp_security_hashes` VALUES(1136, 1362969360, '127.0.0.1', 'cb5cd7dea1543557be83c3390b010ab407699f54');
INSERT INTO `exp_security_hashes` VALUES(1137, 1362969360, '127.0.0.1', 'da4c087a633b81ee6eebeb580e65898fa5253567');

/* Table structure for table `exp_sessions` */
DROP TABLE IF EXISTS `exp_sessions`;

CREATE TABLE `exp_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `admin_sess` tinyint(1) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  KEY `member_id` (`member_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_sessions` */
INSERT INTO `exp_sessions` VALUES('757654ff8dc446e4dd77515f3a507eeb5e9d40d0', 1, 1, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.152 Safari/537.22', 1362960488);
INSERT INTO `exp_sessions` VALUES('da5f562da919a56ebada7f2970128d867d18c044', 1, 1, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.152 Safari/537.22', 1362969386);

/* Table structure for table `exp_sites` */
DROP TABLE IF EXISTS `exp_sites`;

CREATE TABLE `exp_sites` (
  `site_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `site_label` varchar(100) NOT NULL DEFAULT '',
  `site_name` varchar(50) NOT NULL DEFAULT '',
  `site_description` text,
  `site_system_preferences` mediumtext NOT NULL,
  `site_mailinglist_preferences` text NOT NULL,
  `site_member_preferences` text NOT NULL,
  `site_template_preferences` text NOT NULL,
  `site_channel_preferences` text NOT NULL,
  `site_bootstrap_checksums` text NOT NULL,
  PRIMARY KEY (`site_id`),
  KEY `site_name` (`site_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_sites` */
INSERT INTO `exp_sites` VALUES(1, 'Synergy Positioning Systems (NZ)', 'synergy_nz', '', 'YTo5Mjp7czoxMDoic2l0ZV9pbmRleCI7czowOiIiO3M6ODoic2l0ZV91cmwiO3M6MjE6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsLyI7czoxNjoidGhlbWVfZm9sZGVyX3VybCI7czoyODoiaHR0cDovL3N5bmVyZ3kubG9jYWwvdGhlbWVzLyI7czoxNToid2VibWFzdGVyX2VtYWlsIjtzOjE5OiJqYW1lc0A5NmJsYWNrLmNvLm56IjtzOjE0OiJ3ZWJtYXN0ZXJfbmFtZSI7czowOiIiO3M6MjA6ImNoYW5uZWxfbm9tZW5jbGF0dXJlIjtzOjc6ImNoYW5uZWwiO3M6MTA6Im1heF9jYWNoZXMiO3M6MzoiMTUwIjtzOjExOiJjYXB0Y2hhX3VybCI7czozNzoiaHR0cDovL3N5bmVyZ3kubG9jYWwvaW1hZ2VzL2NhcHRjaGFzLyI7czoxMjoiY2FwdGNoYV9wYXRoIjtzOjU0OiIvVXNlcnMvamFtZXMvUHJvamVjdHMvOTZibGFjay9TeW5lcmd5L2ltYWdlcy9jYXB0Y2hhcy8iO3M6MTI6ImNhcHRjaGFfZm9udCI7czoxOiJ5IjtzOjEyOiJjYXB0Y2hhX3JhbmQiO3M6MToieSI7czoyMzoiY2FwdGNoYV9yZXF1aXJlX21lbWJlcnMiO3M6MToibiI7czoxNzoiZW5hYmxlX2RiX2NhY2hpbmciO3M6MToibiI7czoxODoiZW5hYmxlX3NxbF9jYWNoaW5nIjtzOjE6Im4iO3M6MTg6ImZvcmNlX3F1ZXJ5X3N0cmluZyI7czoxOiJuIjtzOjEzOiJzaG93X3Byb2ZpbGVyIjtzOjE6Im4iO3M6MTg6InRlbXBsYXRlX2RlYnVnZ2luZyI7czoxOiJuIjtzOjE1OiJpbmNsdWRlX3NlY29uZHMiO3M6MToibiI7czoxMzoiY29va2llX2RvbWFpbiI7czowOiIiO3M6MTE6ImNvb2tpZV9wYXRoIjtzOjA6IiI7czoxNzoidXNlcl9zZXNzaW9uX3R5cGUiO3M6MToiYyI7czoxODoiYWRtaW5fc2Vzc2lvbl90eXBlIjtzOjI6ImNzIjtzOjIxOiJhbGxvd191c2VybmFtZV9jaGFuZ2UiO3M6MToieSI7czoxODoiYWxsb3dfbXVsdGlfbG9naW5zIjtzOjE6InkiO3M6MTY6InBhc3N3b3JkX2xvY2tvdXQiO3M6MToieSI7czoyNToicGFzc3dvcmRfbG9ja291dF9pbnRlcnZhbCI7czoxOiIxIjtzOjIwOiJyZXF1aXJlX2lwX2Zvcl9sb2dpbiI7czoxOiJ5IjtzOjIyOiJyZXF1aXJlX2lwX2Zvcl9wb3N0aW5nIjtzOjE6InkiO3M6MjQ6InJlcXVpcmVfc2VjdXJlX3Bhc3N3b3JkcyI7czoxOiJuIjtzOjE5OiJhbGxvd19kaWN0aW9uYXJ5X3B3IjtzOjE6InkiO3M6MjM6Im5hbWVfb2ZfZGljdGlvbmFyeV9maWxlIjtzOjA6IiI7czoxNzoieHNzX2NsZWFuX3VwbG9hZHMiO3M6MToieSI7czoxNToicmVkaXJlY3RfbWV0aG9kIjtzOjg6InJlZGlyZWN0IjtzOjk6ImRlZnRfbGFuZyI7czo3OiJlbmdsaXNoIjtzOjg6InhtbF9sYW5nIjtzOjI6ImVuIjtzOjEyOiJzZW5kX2hlYWRlcnMiO3M6MToieSI7czoxMToiZ3ppcF9vdXRwdXQiO3M6MToibiI7czoxMzoibG9nX3JlZmVycmVycyI7czoxOiJuIjtzOjEzOiJtYXhfcmVmZXJyZXJzIjtzOjM6IjUwMCI7czoxMToidGltZV9mb3JtYXQiO3M6MjoidXMiO3M6MTU6InNlcnZlcl90aW1lem9uZSI7czo0OiJVUDEyIjtzOjEzOiJzZXJ2ZXJfb2Zmc2V0IjtzOjA6IiI7czoxNjoiZGF5bGlnaHRfc2F2aW5ncyI7czoxOiJ5IjtzOjIxOiJkZWZhdWx0X3NpdGVfdGltZXpvbmUiO3M6NDoiVVAxMiI7czoxNjoiZGVmYXVsdF9zaXRlX2RzdCI7czoxOiJ5IjtzOjE1OiJob25vcl9lbnRyeV9kc3QiO3M6MToieSI7czoxMzoibWFpbF9wcm90b2NvbCI7czo0OiJtYWlsIjtzOjExOiJzbXRwX3NlcnZlciI7czowOiIiO3M6MTM6InNtdHBfdXNlcm5hbWUiO3M6MDoiIjtzOjEzOiJzbXRwX3Bhc3N3b3JkIjtzOjA6IiI7czoxMToiZW1haWxfZGVidWciO3M6MToibiI7czoxMzoiZW1haWxfY2hhcnNldCI7czo1OiJ1dGYtOCI7czoxNToiZW1haWxfYmF0Y2htb2RlIjtzOjE6Im4iO3M6MTY6ImVtYWlsX2JhdGNoX3NpemUiO3M6MDoiIjtzOjExOiJtYWlsX2Zvcm1hdCI7czo1OiJwbGFpbiI7czo5OiJ3b3JkX3dyYXAiO3M6MToieSI7czoyMjoiZW1haWxfY29uc29sZV90aW1lbG9jayI7czoxOiI1IjtzOjIyOiJsb2dfZW1haWxfY29uc29sZV9tc2dzIjtzOjE6InkiO3M6ODoiY3BfdGhlbWUiO3M6NzoiZGVmYXVsdCI7czoyMToiZW1haWxfbW9kdWxlX2NhcHRjaGFzIjtzOjE6Im4iO3M6MTY6ImxvZ19zZWFyY2hfdGVybXMiO3M6MToieSI7czoxMjoic2VjdXJlX2Zvcm1zIjtzOjE6InkiO3M6MTk6ImRlbnlfZHVwbGljYXRlX2RhdGEiO3M6MToieSI7czoyNDoicmVkaXJlY3Rfc3VibWl0dGVkX2xpbmtzIjtzOjE6Im4iO3M6MTY6ImVuYWJsZV9jZW5zb3JpbmciO3M6MToibiI7czoxNDoiY2Vuc29yZWRfd29yZHMiO3M6MDoiIjtzOjE4OiJjZW5zb3JfcmVwbGFjZW1lbnQiO3M6MDoiIjtzOjEwOiJiYW5uZWRfaXBzIjtzOjA6IiI7czoxMzoiYmFubmVkX2VtYWlscyI7czowOiIiO3M6MTY6ImJhbm5lZF91c2VybmFtZXMiO3M6MDoiIjtzOjE5OiJiYW5uZWRfc2NyZWVuX25hbWVzIjtzOjA6IiI7czoxMDoiYmFuX2FjdGlvbiI7czo4OiJyZXN0cmljdCI7czoxMToiYmFuX21lc3NhZ2UiO3M6MzQ6IlRoaXMgc2l0ZSBpcyBjdXJyZW50bHkgdW5hdmFpbGFibGUiO3M6MTU6ImJhbl9kZXN0aW5hdGlvbiI7czoyMToiaHR0cDovL3d3dy55YWhvby5jb20vIjtzOjE2OiJlbmFibGVfZW1vdGljb25zIjtzOjE6InkiO3M6MTI6ImVtb3RpY29uX3VybCI7czozNjoiaHR0cDovL3N5bmVyZ3kubG9jYWwvaW1hZ2VzL3NtaWxleXMvIjtzOjE5OiJyZWNvdW50X2JhdGNoX3RvdGFsIjtzOjQ6IjEwMDAiO3M6MTc6Im5ld192ZXJzaW9uX2NoZWNrIjtzOjE6InkiO3M6MTc6ImVuYWJsZV90aHJvdHRsaW5nIjtzOjE6Im4iO3M6MTc6ImJhbmlzaF9tYXNrZWRfaXBzIjtzOjE6InkiO3M6MTQ6Im1heF9wYWdlX2xvYWRzIjtzOjI6IjEwIjtzOjEzOiJ0aW1lX2ludGVydmFsIjtzOjE6IjgiO3M6MTI6ImxvY2tvdXRfdGltZSI7czoyOiIzMCI7czoxNToiYmFuaXNobWVudF90eXBlIjtzOjc6Im1lc3NhZ2UiO3M6MTQ6ImJhbmlzaG1lbnRfdXJsIjtzOjA6IiI7czoxODoiYmFuaXNobWVudF9tZXNzYWdlIjtzOjUwOiJZb3UgaGF2ZSBleGNlZWRlZCB0aGUgYWxsb3dlZCBwYWdlIGxvYWQgZnJlcXVlbmN5LiI7czoxNzoiZW5hYmxlX3NlYXJjaF9sb2ciO3M6MToieSI7czoxOToibWF4X2xvZ2dlZF9zZWFyY2hlcyI7czozOiI1MDAiO3M6MTc6InRoZW1lX2ZvbGRlcl9wYXRoIjtzOjQ1OiIvVXNlcnMvamFtZXMvUHJvamVjdHMvOTZibGFjay9TeW5lcmd5L3RoZW1lcy8iO3M6MTA6ImlzX3NpdGVfb24iO3M6MToieSI7czoxMToicnRlX2VuYWJsZWQiO3M6MToieSI7czoyMjoicnRlX2RlZmF1bHRfdG9vbHNldF9pZCI7czoxOiIxIjt9', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6MzY6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9hdmF0YXJzLyI7czoxMToiYXZhdGFyX3BhdGgiO3M6NTM6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL2F2YXRhcnMvIjtzOjE2OiJhdmF0YXJfbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNzoiYXZhdGFyX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEzOiJhdmF0YXJfbWF4X2tiIjtzOjI6IjUwIjtzOjEzOiJlbmFibGVfcGhvdG9zIjtzOjE6Im4iO3M6OToicGhvdG9fdXJsIjtzOjQyOiJodHRwOi8vc3luZXJneS5sb2NhbC9pbWFnZXMvbWVtYmVyX3Bob3Rvcy8iO3M6MTA6InBob3RvX3BhdGgiO3M6NTk6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjE1OiJwaG90b19tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE2OiJwaG90b19tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMjoicGhvdG9fbWF4X2tiIjtzOjI6IjUwIjtzOjE2OiJhbGxvd19zaWduYXR1cmVzIjtzOjE6InkiO3M6MTM6InNpZ19tYXhsZW5ndGgiO3M6MzoiNTAwIjtzOjIxOiJzaWdfYWxsb3dfaW1nX2hvdGxpbmsiO3M6MToibiI7czoyMDoic2lnX2FsbG93X2ltZ191cGxvYWQiO3M6MToibiI7czoxMToic2lnX2ltZ191cmwiO3M6NTA6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9zaWduYXR1cmVfYXR0YWNobWVudHMvIjtzOjEyOiJzaWdfaW1nX3BhdGgiO3M6Njc6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo2MDoiL1VzZXJzL2phbWVzL1Byb2plY3RzLzk2YmxhY2svU3luZXJneS9pbWFnZXMvcG1fYXR0YWNobWVudHMvIjtzOjIzOiJwcnZfbXNnX21heF9hdHRhY2htZW50cyI7czoxOiIzIjtzOjIyOiJwcnZfbXNnX2F0dGFjaF9tYXhzaXplIjtzOjM6IjI1MCI7czoyMDoicHJ2X21zZ19hdHRhY2hfdG90YWwiO3M6MzoiMTAwIjtzOjE5OiJwcnZfbXNnX2h0bWxfZm9ybWF0IjtzOjQ6InNhZmUiO3M6MTg6InBydl9tc2dfYXV0b19saW5rcyI7czoxOiJ5IjtzOjE3OiJwcnZfbXNnX21heF9jaGFycyI7czo0OiI2MDAwIjtzOjE5OiJtZW1iZXJsaXN0X29yZGVyX2J5IjtzOjExOiJ0b3RhbF9wb3N0cyI7czoyMToibWVtYmVybGlzdF9zb3J0X29yZGVyIjtzOjQ6ImRlc2MiO3M6MjA6Im1lbWJlcmxpc3Rfcm93X2xpbWl0IjtzOjI6IjIwIjt9', 'YTo2OntzOjExOiJzdHJpY3RfdXJscyI7czoxOiJuIjtzOjg6InNpdGVfNDA0IjtzOjA6IiI7czoxOToic2F2ZV90bXBsX3JldmlzaW9ucyI7czoxOiJuIjtzOjE4OiJtYXhfdG1wbF9yZXZpc2lvbnMiO3M6MToiNSI7czoxNToic2F2ZV90bXBsX2ZpbGVzIjtzOjE6InkiO3M6MTg6InRtcGxfZmlsZV9iYXNlcGF0aCI7czo0NjoiL1VzZXJzL2phbWVzbWNmYWxsL1Byb2plY3RzL1N5bmVyZ3kvdGVtcGxhdGVzLyI7fQ==', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YToyOntzOjc6ImVtYWlsZWQiO2E6MDp7fXM6NDU6Ii9Vc2Vycy9qYW1lc21jZmFsbC9Qcm9qZWN0cy9TeW5lcmd5L2luZGV4LnBocCI7czozMjoiZjFiMTliOGE0NDU5NmM1MzQ2MTViZWJlMTMyNmYwNjgiO30=');
INSERT INTO `exp_sites` VALUES(2, 'Synergy Positioning Systems (AU)', 'synergy_au', '', 'YTo5MTp7czoxMDoiaXNfc2l0ZV9vbiI7czoxOiJ5IjtzOjEwOiJzaXRlX2luZGV4IjtzOjA6IiI7czo4OiJzaXRlX3VybCI7czoyMToiaHR0cDovL3N5bmVyZ3kubG9jYWwvIjtzOjE2OiJ0aGVtZV9mb2xkZXJfdXJsIjtzOjI4OiJodHRwOi8vc3luZXJneS5sb2NhbC90aGVtZXMvIjtzOjE3OiJ0aGVtZV9mb2xkZXJfcGF0aCI7czo0NToiL1VzZXJzL2phbWVzL1Byb2plY3RzLzk2YmxhY2svU3luZXJneS90aGVtZXMvIjtzOjE1OiJ3ZWJtYXN0ZXJfZW1haWwiO3M6MTk6ImphbWVzQDk2YmxhY2suY28ubnoiO3M6MTQ6IndlYm1hc3Rlcl9uYW1lIjtzOjA6IiI7czoyMDoiY2hhbm5lbF9ub21lbmNsYXR1cmUiO3M6NzoiY2hhbm5lbCI7czoxMDoibWF4X2NhY2hlcyI7czozOiIxNTAiO3M6MTE6ImNhcHRjaGFfdXJsIjtzOjM3OiJodHRwOi8vc3luZXJneS5sb2NhbC9pbWFnZXMvY2FwdGNoYXMvIjtzOjEyOiJjYXB0Y2hhX3BhdGgiO3M6NTQ6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL2NhcHRjaGFzLyI7czoxMjoiY2FwdGNoYV9mb250IjtzOjE6InkiO3M6MTI6ImNhcHRjaGFfcmFuZCI7czoxOiJ5IjtzOjIzOiJjYXB0Y2hhX3JlcXVpcmVfbWVtYmVycyI7czoxOiJuIjtzOjE3OiJlbmFibGVfZGJfY2FjaGluZyI7czoxOiJuIjtzOjE4OiJlbmFibGVfc3FsX2NhY2hpbmciO3M6MToibiI7czoxODoiZm9yY2VfcXVlcnlfc3RyaW5nIjtzOjE6Im4iO3M6MTM6InNob3dfcHJvZmlsZXIiO3M6MToibiI7czoxODoidGVtcGxhdGVfZGVidWdnaW5nIjtzOjE6Im4iO3M6MTU6ImluY2x1ZGVfc2Vjb25kcyI7czoxOiJuIjtzOjEzOiJjb29raWVfZG9tYWluIjtzOjA6IiI7czoxMToiY29va2llX3BhdGgiO3M6MDoiIjtzOjE3OiJ1c2VyX3Nlc3Npb25fdHlwZSI7czoxOiJjIjtzOjE4OiJhZG1pbl9zZXNzaW9uX3R5cGUiO3M6MjoiY3MiO3M6MjE6ImFsbG93X3VzZXJuYW1lX2NoYW5nZSI7czoxOiJ5IjtzOjE4OiJhbGxvd19tdWx0aV9sb2dpbnMiO3M6MToieSI7czoxNjoicGFzc3dvcmRfbG9ja291dCI7czoxOiJ5IjtzOjI1OiJwYXNzd29yZF9sb2Nrb3V0X2ludGVydmFsIjtzOjE6IjEiO3M6MjA6InJlcXVpcmVfaXBfZm9yX2xvZ2luIjtzOjE6InkiO3M6MjI6InJlcXVpcmVfaXBfZm9yX3Bvc3RpbmciO3M6MToieSI7czoyNDoicmVxdWlyZV9zZWN1cmVfcGFzc3dvcmRzIjtzOjE6Im4iO3M6MTk6ImFsbG93X2RpY3Rpb25hcnlfcHciO3M6MToieSI7czoyMzoibmFtZV9vZl9kaWN0aW9uYXJ5X2ZpbGUiO3M6MDoiIjtzOjE3OiJ4c3NfY2xlYW5fdXBsb2FkcyI7czoxOiJ5IjtzOjE1OiJyZWRpcmVjdF9tZXRob2QiO3M6ODoicmVkaXJlY3QiO3M6OToiZGVmdF9sYW5nIjtzOjc6ImVuZ2xpc2giO3M6ODoieG1sX2xhbmciO3M6MjoiZW4iO3M6MTI6InNlbmRfaGVhZGVycyI7czoxOiJ5IjtzOjExOiJnemlwX291dHB1dCI7czoxOiJuIjtzOjEzOiJsb2dfcmVmZXJyZXJzIjtzOjE6Im4iO3M6MTM6Im1heF9yZWZlcnJlcnMiO3M6MzoiNTAwIjtzOjExOiJ0aW1lX2Zvcm1hdCI7czoyOiJ1cyI7czoxNToic2VydmVyX3RpbWV6b25lIjtzOjQ6IlVQMTIiO3M6MTM6InNlcnZlcl9vZmZzZXQiO3M6MDoiIjtzOjE2OiJkYXlsaWdodF9zYXZpbmdzIjtzOjE6InkiO3M6MjE6ImRlZmF1bHRfc2l0ZV90aW1lem9uZSI7czo0OiJVUDEyIjtzOjE2OiJkZWZhdWx0X3NpdGVfZHN0IjtzOjE6InkiO3M6MTM6Im1haWxfcHJvdG9jb2wiO3M6NDoibWFpbCI7czoxMToic210cF9zZXJ2ZXIiO3M6MDoiIjtzOjEzOiJzbXRwX3VzZXJuYW1lIjtzOjA6IiI7czoxMzoic210cF9wYXNzd29yZCI7czowOiIiO3M6MTE6ImVtYWlsX2RlYnVnIjtzOjE6Im4iO3M6MTM6ImVtYWlsX2NoYXJzZXQiO3M6NToidXRmLTgiO3M6MTU6ImVtYWlsX2JhdGNobW9kZSI7czoxOiJuIjtzOjE2OiJlbWFpbF9iYXRjaF9zaXplIjtzOjA6IiI7czoxMToibWFpbF9mb3JtYXQiO3M6NToicGxhaW4iO3M6OToid29yZF93cmFwIjtzOjE6InkiO3M6MjI6ImVtYWlsX2NvbnNvbGVfdGltZWxvY2siO3M6MToiNSI7czoyMjoibG9nX2VtYWlsX2NvbnNvbGVfbXNncyI7czoxOiJ5IjtzOjg6ImNwX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MjE6ImVtYWlsX21vZHVsZV9jYXB0Y2hhcyI7czoxOiJuIjtzOjE2OiJsb2dfc2VhcmNoX3Rlcm1zIjtzOjE6InkiO3M6MTI6InNlY3VyZV9mb3JtcyI7czoxOiJ5IjtzOjE5OiJkZW55X2R1cGxpY2F0ZV9kYXRhIjtzOjE6InkiO3M6MjQ6InJlZGlyZWN0X3N1Ym1pdHRlZF9saW5rcyI7czoxOiJuIjtzOjE2OiJlbmFibGVfY2Vuc29yaW5nIjtzOjE6Im4iO3M6MTQ6ImNlbnNvcmVkX3dvcmRzIjtzOjA6IiI7czoxODoiY2Vuc29yX3JlcGxhY2VtZW50IjtzOjA6IiI7czoxMDoiYmFubmVkX2lwcyI7czowOiIiO3M6MTM6ImJhbm5lZF9lbWFpbHMiO3M6MDoiIjtzOjE2OiJiYW5uZWRfdXNlcm5hbWVzIjtzOjA6IiI7czoxOToiYmFubmVkX3NjcmVlbl9uYW1lcyI7czowOiIiO3M6MTA6ImJhbl9hY3Rpb24iO3M6ODoicmVzdHJpY3QiO3M6MTE6ImJhbl9tZXNzYWdlIjtzOjM0OiJUaGlzIHNpdGUgaXMgY3VycmVudGx5IHVuYXZhaWxhYmxlIjtzOjE1OiJiYW5fZGVzdGluYXRpb24iO3M6MjE6Imh0dHA6Ly93d3cueWFob28uY29tLyI7czoxNjoiZW5hYmxlX2Vtb3RpY29ucyI7czoxOiJ5IjtzOjEyOiJlbW90aWNvbl91cmwiO3M6MzY6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9zbWlsZXlzLyI7czoxOToicmVjb3VudF9iYXRjaF90b3RhbCI7czo0OiIxMDAwIjtzOjE3OiJuZXdfdmVyc2lvbl9jaGVjayI7czoxOiJ5IjtzOjE3OiJlbmFibGVfdGhyb3R0bGluZyI7czoxOiJuIjtzOjE3OiJiYW5pc2hfbWFza2VkX2lwcyI7czoxOiJ5IjtzOjE0OiJtYXhfcGFnZV9sb2FkcyI7czoyOiIxMCI7czoxMzoidGltZV9pbnRlcnZhbCI7czoxOiI4IjtzOjEyOiJsb2Nrb3V0X3RpbWUiO3M6MjoiMzAiO3M6MTU6ImJhbmlzaG1lbnRfdHlwZSI7czo3OiJtZXNzYWdlIjtzOjE0OiJiYW5pc2htZW50X3VybCI7czowOiIiO3M6MTg6ImJhbmlzaG1lbnRfbWVzc2FnZSI7czo1MDoiWW91IGhhdmUgZXhjZWVkZWQgdGhlIGFsbG93ZWQgcGFnZSBsb2FkIGZyZXF1ZW5jeS4iO3M6MTc6ImVuYWJsZV9zZWFyY2hfbG9nIjtzOjE6InkiO3M6MTk6Im1heF9sb2dnZWRfc2VhcmNoZXMiO3M6MzoiNTAwIjtzOjExOiJydGVfZW5hYmxlZCI7czoxOiJ5IjtzOjIyOiJydGVfZGVmYXVsdF90b29sc2V0X2lkIjtzOjE6IjEiO30=', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6MzY6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9hdmF0YXJzLyI7czoxMToiYXZhdGFyX3BhdGgiO3M6NTM6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL2F2YXRhcnMvIjtzOjE2OiJhdmF0YXJfbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNzoiYXZhdGFyX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEzOiJhdmF0YXJfbWF4X2tiIjtzOjI6IjUwIjtzOjEzOiJlbmFibGVfcGhvdG9zIjtzOjE6Im4iO3M6OToicGhvdG9fdXJsIjtzOjQyOiJodHRwOi8vc3luZXJneS5sb2NhbC9pbWFnZXMvbWVtYmVyX3Bob3Rvcy8iO3M6MTA6InBob3RvX3BhdGgiO3M6NTk6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjE1OiJwaG90b19tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE2OiJwaG90b19tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMjoicGhvdG9fbWF4X2tiIjtzOjI6IjUwIjtzOjE2OiJhbGxvd19zaWduYXR1cmVzIjtzOjE6InkiO3M6MTM6InNpZ19tYXhsZW5ndGgiO3M6MzoiNTAwIjtzOjIxOiJzaWdfYWxsb3dfaW1nX2hvdGxpbmsiO3M6MToibiI7czoyMDoic2lnX2FsbG93X2ltZ191cGxvYWQiO3M6MToibiI7czoxMToic2lnX2ltZ191cmwiO3M6NTA6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9zaWduYXR1cmVfYXR0YWNobWVudHMvIjtzOjEyOiJzaWdfaW1nX3BhdGgiO3M6Njc6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo2MDoiL1VzZXJzL2phbWVzL1Byb2plY3RzLzk2YmxhY2svU3luZXJneS9pbWFnZXMvcG1fYXR0YWNobWVudHMvIjtzOjIzOiJwcnZfbXNnX21heF9hdHRhY2htZW50cyI7czoxOiIzIjtzOjIyOiJwcnZfbXNnX2F0dGFjaF9tYXhzaXplIjtzOjM6IjI1MCI7czoyMDoicHJ2X21zZ19hdHRhY2hfdG90YWwiO3M6MzoiMTAwIjtzOjE5OiJwcnZfbXNnX2h0bWxfZm9ybWF0IjtzOjQ6InNhZmUiO3M6MTg6InBydl9tc2dfYXV0b19saW5rcyI7czoxOiJ5IjtzOjE3OiJwcnZfbXNnX21heF9jaGFycyI7czo0OiI2MDAwIjtzOjE5OiJtZW1iZXJsaXN0X29yZGVyX2J5IjtzOjExOiJ0b3RhbF9wb3N0cyI7czoyMToibWVtYmVybGlzdF9zb3J0X29yZGVyIjtzOjQ6ImRlc2MiO3M6MjA6Im1lbWJlcmxpc3Rfcm93X2xpbWl0IjtzOjI6IjIwIjt9', 'YTo2OntzOjE1OiJzYXZlX3RtcGxfZmlsZXMiO3M6MToieSI7czoxODoidG1wbF9maWxlX2Jhc2VwYXRoIjtzOjQ4OiIvVXNlcnMvamFtZXMvUHJvamVjdHMvOTZibGFjay9TeW5lcmd5L3RlbXBsYXRlcy8iO3M6ODoic2l0ZV80MDQiO3M6MDoiIjtzOjE5OiJzYXZlX3RtcGxfcmV2aXNpb25zIjtzOjE6Im4iO3M6MTg6Im1heF90bXBsX3JldmlzaW9ucyI7czoxOiI1IjtzOjExOiJzdHJpY3RfdXJscyI7czoxOiJuIjt9', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YToxOntzOjc6ImVtYWlsZWQiO2E6MDp7fX0=');

/* Table structure for table `exp_snippets` */
DROP TABLE IF EXISTS `exp_snippets`;

CREATE TABLE `exp_snippets` (
  `snippet_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) NOT NULL,
  `snippet_name` varchar(75) NOT NULL,
  `snippet_contents` text,
  PRIMARY KEY (`snippet_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_specialty_templates` */
DROP TABLE IF EXISTS `exp_specialty_templates`;

CREATE TABLE `exp_specialty_templates` (
  `template_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_name` varchar(50) NOT NULL,
  `data_title` varchar(80) NOT NULL,
  `template_data` text NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_specialty_templates` */
INSERT INTO `exp_specialty_templates` VALUES(1, 1, 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>');
INSERT INTO `exp_specialty_templates` VALUES(2, 1, 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=\'content-type\' content=\'text/html; charset={charset}\' />\n\n{meta_refresh}\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>');
INSERT INTO `exp_specialty_templates` VALUES(3, 1, 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}');
INSERT INTO `exp_specialty_templates` VALUES(4, 1, 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n');
INSERT INTO `exp_specialty_templates` VALUES(5, 1, 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}');
INSERT INTO `exp_specialty_templates` VALUES(6, 1, 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}');
INSERT INTO `exp_specialty_templates` VALUES(7, 1, 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(8, 1, 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(9, 1, 'y', 'reset_password_notification', 'New Login Information', '{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(10, 1, 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(11, 1, 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe\'re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(12, 1, 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the \"{mailing_list}\" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}');
INSERT INTO `exp_specialty_templates` VALUES(13, 1, 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}');
INSERT INTO `exp_specialty_templates` VALUES(14, 1, 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}');
INSERT INTO `exp_specialty_templates` VALUES(15, 1, 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled �{message_subject}�.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(16, 1, 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(17, 2, 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>');
INSERT INTO `exp_specialty_templates` VALUES(18, 2, 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=\'content-type\' content=\'text/html; charset={charset}\' />\n\n{meta_refresh}\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>');
INSERT INTO `exp_specialty_templates` VALUES(19, 2, 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}');
INSERT INTO `exp_specialty_templates` VALUES(20, 2, 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n');
INSERT INTO `exp_specialty_templates` VALUES(21, 2, 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}');
INSERT INTO `exp_specialty_templates` VALUES(22, 2, 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}');
INSERT INTO `exp_specialty_templates` VALUES(23, 2, 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(24, 2, 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(25, 2, 'y', 'reset_password_notification', 'New Login Information', '{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(26, 2, 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(27, 2, 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe\'re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(28, 2, 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the \"{mailing_list}\" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}');
INSERT INTO `exp_specialty_templates` VALUES(29, 2, 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}');
INSERT INTO `exp_specialty_templates` VALUES(30, 2, 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}');
INSERT INTO `exp_specialty_templates` VALUES(31, 2, 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled �{message_subject}�.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(32, 2, 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}');

/* Table structure for table `exp_stats` */
DROP TABLE IF EXISTS `exp_stats`;

CREATE TABLE `exp_stats` (
  `stat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `total_members` mediumint(7) NOT NULL DEFAULT '0',
  `recent_member_id` int(10) NOT NULL DEFAULT '0',
  `recent_member` varchar(50) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `most_visitors` mediumint(7) NOT NULL DEFAULT '0',
  `most_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_cache_clear` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`stat_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_stats` */
INSERT INTO `exp_stats` VALUES(1, 1, 1, 1, '96black', 11, 0, 0, 0, 1362967322, 0, 0, 1362969393, 13, 1358210902, 1363553562);
INSERT INTO `exp_stats` VALUES(2, 2, 1, 1, '96black', 0, 0, 0, 0, 0, 0, 0, 1355941646, 2, 1355881675, 1356492669);

/* Table structure for table `exp_status_groups` */
DROP TABLE IF EXISTS `exp_status_groups`;

CREATE TABLE `exp_status_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_status_groups` */
INSERT INTO `exp_status_groups` VALUES(1, 1, 'Statuses');
INSERT INTO `exp_status_groups` VALUES(2, 2, 'Statuses');

/* Table structure for table `exp_status_no_access` */
DROP TABLE IF EXISTS `exp_status_no_access`;

CREATE TABLE `exp_status_no_access` (
  `status_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`status_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_statuses` */
DROP TABLE IF EXISTS `exp_statuses`;

CREATE TABLE `exp_statuses` (
  `status_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `status_order` int(3) unsigned NOT NULL,
  `highlight` varchar(30) NOT NULL,
  PRIMARY KEY (`status_id`),
  KEY `group_id` (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_statuses` */
INSERT INTO `exp_statuses` VALUES(1, 1, 1, 'open', 1, '009933');
INSERT INTO `exp_statuses` VALUES(2, 1, 1, 'closed', 2, '990000');
INSERT INTO `exp_statuses` VALUES(3, 2, 2, 'open', 1, '009933');
INSERT INTO `exp_statuses` VALUES(4, 2, 2, 'closed', 2, '990000');

/* Table structure for table `exp_template_groups` */
DROP TABLE IF EXISTS `exp_template_groups`;

CREATE TABLE `exp_template_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `group_order` int(3) unsigned NOT NULL,
  `is_site_default` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_template_groups` */
INSERT INTO `exp_template_groups` VALUES(1, 1, 'Routing', 1, 'y');
INSERT INTO `exp_template_groups` VALUES(2, 2, 'Routing', 2, 'y');
INSERT INTO `exp_template_groups` VALUES(5, 1, 'Home', 3, 'n');
INSERT INTO `exp_template_groups` VALUES(6, 1, 'Products', 4, 'n');
INSERT INTO `exp_template_groups` VALUES(8, 1, 'News', 6, 'n');
INSERT INTO `exp_template_groups` VALUES(9, 1, 'About-us', 7, 'n');
INSERT INTO `exp_template_groups` VALUES(10, 1, 'Contact-us', 8, 'n');
INSERT INTO `exp_template_groups` VALUES(11, 1, 'Services', 9, 'n');
INSERT INTO `exp_template_groups` VALUES(12, 1, 'Common', 10, 'n');

/* Table structure for table `exp_template_member_groups` */
DROP TABLE IF EXISTS `exp_template_member_groups`;

CREATE TABLE `exp_template_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `template_group_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`template_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_template_no_access` */
DROP TABLE IF EXISTS `exp_template_no_access`;

CREATE TABLE `exp_template_no_access` (
  `template_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`template_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_templates` */
DROP TABLE IF EXISTS `exp_templates`;

CREATE TABLE `exp_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `template_name` varchar(50) NOT NULL,
  `save_template_file` char(1) NOT NULL DEFAULT 'n',
  `template_type` varchar(16) NOT NULL DEFAULT 'webpage',
  `template_data` mediumtext,
  `template_notes` text,
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `last_author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cache` char(1) NOT NULL DEFAULT 'n',
  `refresh` int(6) unsigned NOT NULL DEFAULT '0',
  `no_auth_bounce` varchar(50) NOT NULL DEFAULT '',
  `enable_http_auth` char(1) NOT NULL DEFAULT 'n',
  `allow_php` char(1) NOT NULL DEFAULT 'n',
  `php_parse_location` char(1) NOT NULL DEFAULT 'o',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_id`),
  KEY `group_id` (`group_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_templates` */
INSERT INTO `exp_templates` VALUES(1, 1, 1, 'index', 'y', 'webpage', 'Routing NZ', '', 1355885942, 1, 'n', 0, '', 'n', 'y', 'o', 1769);
INSERT INTO `exp_templates` VALUES(2, 2, 2, 'index', 'y', 'webpage', 'AU routing file\n\n{embed=\"synergy_nz:home/index\"}', '', 1355889914, 1, 'n', 0, '', 'n', 'y', 'o', 35);
INSERT INTO `exp_templates` VALUES(4, 2, 1, 'monkey-butt', 'y', 'webpage', 'OH SNAP', '', 1355889015, 1, 'n', 0, '', 'n', 'n', 'o', 1);
INSERT INTO `exp_templates` VALUES(9, 1, 5, 'index', 'y', 'webpage', 'Home test', '', 1355945914, 1, 'n', 0, '', 'n', 'n', 'o', 4);
INSERT INTO `exp_templates` VALUES(10, 1, 6, 'index', 'y', 'webpage', '', '', 1355945909, 1, 'n', 0, '', 'n', 'n', 'o', 282);
INSERT INTO `exp_templates` VALUES(12, 1, 8, 'index', 'y', 'webpage', '', '', 1355945899, 1, 'n', 0, '', 'n', 'n', 'o', 41);
INSERT INTO `exp_templates` VALUES(13, 1, 9, 'index', 'y', 'webpage', '', '', 1355945894, 1, 'n', 0, '', 'n', 'n', 'o', 4);
INSERT INTO `exp_templates` VALUES(14, 1, 10, 'index', 'y', 'webpage', '', '', 1355945889, 1, 'n', 0, '', 'n', 'n', 'o', 12);
INSERT INTO `exp_templates` VALUES(15, 1, 11, 'index', 'y', 'webpage', '', '', 1355945883, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(16, 1, 12, 'index', 'y', 'webpage', '', '', 1355945878, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(17, 1, 12, 'header', 'y', 'webpage', '', '', 1362949938, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(18, 1, 12, 'footer', 'y', 'webpage', '', '', 1355946002, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(19, 1, 12, '_menu', 'y', 'webpage', '', '', 1362965438, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(20, 1, 6, 'product-page', 'y', 'webpage', '', '', 1358280171, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(22, 1, 6, 'category-listing', 'y', 'webpage', 'Category listing!', '', 1358280845, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(23, 1, 6, '_product-nav', 'y', 'webpage', '', '', 1358295203, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(24, 1, 6, 'category-landing', 'y', 'webpage', '', '', 1358365005, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(25, 1, 6, '_category-listing-top-nav-subcats', 'y', 'webpage', '', '', 1358369422, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(26, 1, 6, '_category-listing-products-blocks', 'y', 'webpage', '', '', 1362954501, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(27, 1, 8, 'article-list', 'y', 'webpage', '', '', 1362957827, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(28, 1, 8, 'article', 'y', 'webpage', '', '', 1362957835, 1, 'n', 0, '', 'n', 'n', 'o', 0);

/* Table structure for table `exp_throttle` */
DROP TABLE IF EXISTS `exp_throttle`;

CREATE TABLE `exp_throttle` (
  `throttle_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL,
  `locked_out` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`throttle_id`),
  KEY `ip_address` (`ip_address`),
  KEY `last_activity` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_upload_no_access` */
DROP TABLE IF EXISTS `exp_upload_no_access`;

CREATE TABLE `exp_upload_no_access` (
  `upload_id` int(6) unsigned NOT NULL,
  `upload_loc` varchar(3) NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`upload_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_upload_prefs` */
DROP TABLE IF EXISTS `exp_upload_prefs`;

CREATE TABLE `exp_upload_prefs` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `server_path` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL,
  `allowed_types` varchar(3) NOT NULL DEFAULT 'img',
  `max_size` varchar(16) DEFAULT NULL,
  `max_height` varchar(6) DEFAULT NULL,
  `max_width` varchar(6) DEFAULT NULL,
  `properties` varchar(120) DEFAULT NULL,
  `pre_format` varchar(120) DEFAULT NULL,
  `post_format` varchar(120) DEFAULT NULL,
  `file_properties` varchar(120) DEFAULT NULL,
  `file_pre_format` varchar(120) DEFAULT NULL,
  `file_post_format` varchar(120) DEFAULT NULL,
  `cat_group` varchar(255) DEFAULT NULL,
  `batch_location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_upload_prefs` */
INSERT INTO `exp_upload_prefs` VALUES(1, 1, 'Product Images', '/Users/jamesmcfall/Projects/Synergy/uploads/product-images/', 'http://synergy.local/uploads/product-images/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(2, 1, 'Product Downloads', '/Users/jamesmcfall/Projects/Synergy/uploads/product-downloads/', 'http://synergy.local/uploads/product-downloads/', 'all', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(3, 1, 'Category Images', '/Users/jamesmcfall/Projects/Synergy/uploads/category-images/', 'http://synergy.local/uploads/category-images/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(4, 1, 'News Article Banner Image', '/Users/jamesmcfall/Projects/Synergy/uploads/news-images/', 'http://synergy.local/uploads/news-images/', 'img', '', '373', '866', '', '', '', '', '', '', '', null);

/* Table structure for table `exp_wygwam_configs` */
DROP TABLE IF EXISTS `exp_wygwam_configs`;

CREATE TABLE `exp_wygwam_configs` (
  `config_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `config_name` varchar(32) DEFAULT NULL,
  `settings` text,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_wygwam_configs` */
INSERT INTO `exp_wygwam_configs` VALUES(1, 'Basic', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTA6e2k6MDtzOjQ6IkJvbGQiO2k6MTtzOjY6Ikl0YWxpYyI7aToyO3M6OToiVW5kZXJsaW5lIjtpOjM7czo2OiJTdHJpa2UiO2k6NDtzOjEyOiJOdW1iZXJlZExpc3QiO2k6NTtzOjEyOiJCdWxsZXRlZExpc3QiO2k6NjtzOjQ6IkxpbmsiO2k6NztzOjY6IlVubGluayI7aTo4O3M6NjoiQW5jaG9yIjtpOjk7czo1OiJBYm91dCI7fXM6NjoiaGVpZ2h0IjtzOjM6IjIwMCI7czoxNDoicmVzaXplX2VuYWJsZWQiO3M6MToieSI7czoxMToiY29udGVudHNDc3MiO2E6MDp7fXM6MTA6InVwbG9hZF9kaXIiO3M6MDoiIjt9');
INSERT INTO `exp_wygwam_configs` VALUES(2, 'Full', 'YTo1OntzOjc6InRvb2xiYXIiO2E6Njc6e2k6MDtzOjY6IlNvdXJjZSI7aToxO3M6NDoiU2F2ZSI7aToyO3M6NzoiTmV3UGFnZSI7aTozO3M6NzoiUHJldmlldyI7aTo0O3M6OToiVGVtcGxhdGVzIjtpOjU7czozOiJDdXQiO2k6NjtzOjQ6IkNvcHkiO2k6NztzOjU6IlBhc3RlIjtpOjg7czo5OiJQYXN0ZVRleHQiO2k6OTtzOjEzOiJQYXN0ZUZyb21Xb3JkIjtpOjEwO3M6NToiUHJpbnQiO2k6MTE7czoxMjoiU3BlbGxDaGVja2VyIjtpOjEyO3M6NToiU2NheXQiO2k6MTM7czo0OiJVbmRvIjtpOjE0O3M6NDoiUmVkbyI7aToxNTtzOjQ6IkZpbmQiO2k6MTY7czo3OiJSZXBsYWNlIjtpOjE3O3M6OToiU2VsZWN0QWxsIjtpOjE4O3M6MTI6IlJlbW92ZUZvcm1hdCI7aToxOTtzOjQ6IkZvcm0iO2k6MjA7czo4OiJDaGVja2JveCI7aToyMTtzOjU6IlJhZGlvIjtpOjIyO3M6OToiVGV4dEZpZWxkIjtpOjIzO3M6ODoiVGV4dGFyZWEiO2k6MjQ7czo2OiJTZWxlY3QiO2k6MjU7czo2OiJCdXR0b24iO2k6MjY7czoxMToiSW1hZ2VCdXR0b24iO2k6Mjc7czoxMToiSGlkZGVuRmllbGQiO2k6Mjg7czoxOiIvIjtpOjI5O3M6NDoiQm9sZCI7aTozMDtzOjY6Ikl0YWxpYyI7aTozMTtzOjk6IlVuZGVybGluZSI7aTozMjtzOjY6IlN0cmlrZSI7aTozMztzOjk6IlN1YnNjcmlwdCI7aTozNDtzOjExOiJTdXBlcnNjcmlwdCI7aTozNTtzOjEyOiJOdW1iZXJlZExpc3QiO2k6MzY7czoxMjoiQnVsbGV0ZWRMaXN0IjtpOjM3O3M6NzoiT3V0ZGVudCI7aTozODtzOjY6IkluZGVudCI7aTozOTtzOjEwOiJCbG9ja3F1b3RlIjtpOjQwO3M6OToiQ3JlYXRlRGl2IjtpOjQxO3M6MTE6Ikp1c3RpZnlMZWZ0IjtpOjQyO3M6MTM6Ikp1c3RpZnlDZW50ZXIiO2k6NDM7czoxMjoiSnVzdGlmeVJpZ2h0IjtpOjQ0O3M6MTI6Ikp1c3RpZnlCbG9jayI7aTo0NTtzOjQ6IkxpbmsiO2k6NDY7czo2OiJVbmxpbmsiO2k6NDc7czo2OiJBbmNob3IiO2k6NDg7czo1OiJJbWFnZSI7aTo0OTtzOjU6IkZsYXNoIjtpOjUwO3M6NToiVGFibGUiO2k6NTE7czoxNDoiSG9yaXpvbnRhbFJ1bGUiO2k6NTI7czo2OiJTbWlsZXkiO2k6NTM7czoxMToiU3BlY2lhbENoYXIiO2k6NTQ7czo5OiJQYWdlQnJlYWsiO2k6NTU7czo4OiJSZWFkTW9yZSI7aTo1NjtzOjEwOiJFbWJlZE1lZGlhIjtpOjU3O3M6MToiLyI7aTo1ODtzOjY6IlN0eWxlcyI7aTo1OTtzOjY6IkZvcm1hdCI7aTo2MDtzOjQ6IkZvbnQiO2k6NjE7czo4OiJGb250U2l6ZSI7aTo2MjtzOjk6IlRleHRDb2xvciI7aTo2MztzOjc6IkJHQ29sb3IiO2k6NjQ7czo4OiJNYXhpbWl6ZSI7aTo2NTtzOjEwOiJTaG93QmxvY2tzIjtpOjY2O3M6NToiQWJvdXQiO31zOjY6ImhlaWdodCI7czozOiIyMDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ==');
INSERT INTO `exp_wygwam_configs` VALUES(3, 'News', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTE6e2k6MDtzOjY6IkZvcm1hdCI7aToxO3M6NDoiQm9sZCI7aToyO3M6NjoiSXRhbGljIjtpOjM7czo5OiJVbmRlcmxpbmUiO2k6NDtzOjY6IlN0cmlrZSI7aTo1O3M6MTI6Ik51bWJlcmVkTGlzdCI7aTo2O3M6MTI6IkJ1bGxldGVkTGlzdCI7aTo3O3M6NDoiTGluayI7aTo4O3M6NjoiVW5saW5rIjtpOjk7czo2OiJBbmNob3IiO2k6MTA7czo1OiJBYm91dCI7fXM6NjoiaGVpZ2h0IjtzOjM6IjIwMCI7czoxNDoicmVzaXplX2VuYWJsZWQiO3M6MToieSI7czoxMToiY29udGVudHNDc3MiO2E6MDp7fXM6MTA6InVwbG9hZF9kaXIiO3M6MDoiIjt9');


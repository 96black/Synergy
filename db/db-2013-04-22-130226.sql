/* Table structure for table `exp_accessories` */
DROP TABLE IF EXISTS `exp_accessories`;

CREATE TABLE `exp_accessories` (
  `accessory_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(75) NOT NULL DEFAULT '',
  `member_groups` varchar(50) NOT NULL DEFAULT 'all',
  `controllers` text,
  `accessory_version` varchar(12) NOT NULL,
  PRIMARY KEY (`accessory_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_accessories` */
INSERT INTO `exp_accessories` VALUES(1, 'Expressionengine_info_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.0');
INSERT INTO `exp_accessories` VALUES(2, 'Mx_cloner_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|sites|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.0.3');

/* Table structure for table `exp_actions` */
DROP TABLE IF EXISTS `exp_actions`;

CREATE TABLE `exp_actions` (
  `action_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_actions` */
INSERT INTO `exp_actions` VALUES(1, 'Comment', 'insert_new_comment');
INSERT INTO `exp_actions` VALUES(2, 'Comment_mcp', 'delete_comment_notification');
INSERT INTO `exp_actions` VALUES(3, 'Comment', 'comment_subscribe');
INSERT INTO `exp_actions` VALUES(4, 'Comment', 'edit_comment');
INSERT INTO `exp_actions` VALUES(5, 'Email', 'send_email');
INSERT INTO `exp_actions` VALUES(6, 'Safecracker', 'submit_entry');
INSERT INTO `exp_actions` VALUES(7, 'Safecracker', 'combo_loader');
INSERT INTO `exp_actions` VALUES(8, 'Search', 'do_search');
INSERT INTO `exp_actions` VALUES(9, 'Channel', 'insert_new_entry');
INSERT INTO `exp_actions` VALUES(10, 'Channel', 'filemanager_endpoint');
INSERT INTO `exp_actions` VALUES(11, 'Channel', 'smiley_pop');
INSERT INTO `exp_actions` VALUES(12, 'Member', 'registration_form');
INSERT INTO `exp_actions` VALUES(13, 'Member', 'register_member');
INSERT INTO `exp_actions` VALUES(14, 'Member', 'activate_member');
INSERT INTO `exp_actions` VALUES(15, 'Member', 'member_login');
INSERT INTO `exp_actions` VALUES(16, 'Member', 'member_logout');
INSERT INTO `exp_actions` VALUES(17, 'Member', 'retrieve_password');
INSERT INTO `exp_actions` VALUES(18, 'Member', 'reset_password');
INSERT INTO `exp_actions` VALUES(19, 'Member', 'send_member_email');
INSERT INTO `exp_actions` VALUES(20, 'Member', 'update_un_pw');
INSERT INTO `exp_actions` VALUES(21, 'Member', 'member_search');
INSERT INTO `exp_actions` VALUES(22, 'Member', 'member_delete');
INSERT INTO `exp_actions` VALUES(23, 'Rte', 'get_js');
INSERT INTO `exp_actions` VALUES(24, 'Playa_mcp', 'filter_entries');
INSERT INTO `exp_actions` VALUES(25, 'Freeform', 'save_form');

/* Table structure for table `exp_captcha` */
DROP TABLE IF EXISTS `exp_captcha`;

CREATE TABLE `exp_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_categories` */
DROP TABLE IF EXISTS `exp_categories`;

CREATE TABLE `exp_categories` (
  `cat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `parent_id` int(4) unsigned NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_url_title` varchar(75) NOT NULL,
  `cat_description` text,
  `cat_image` varchar(120) DEFAULT NULL,
  `cat_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `group_id` (`group_id`),
  KEY `cat_name` (`cat_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_categories` */
INSERT INTO `exp_categories` VALUES(1, 1, 1, 0, 'UAV', 'uav', '', '{filedir_3}cat-image.jpg', 3);
INSERT INTO `exp_categories` VALUES(2, 1, 1, 0, 'Optical', 'optical', 'Category description here', '{filedir_3}cat-image.jpg', 2);
INSERT INTO `exp_categories` VALUES(3, 1, 1, 2, 'Total Station', 'total-station', '', '{filedir_3}cat-image.jpg', 3);
INSERT INTO `exp_categories` VALUES(4, 1, 1, 3, 'Robotic', 'robotic', '', '{filedir_3}cat-image.jpg', 2);
INSERT INTO `exp_categories` VALUES(5, 1, 1, 3, 'Windows Based', 'windows-based', '', '{filedir_3}cat-image.jpg', 3);
INSERT INTO `exp_categories` VALUES(6, 1, 1, 3, 'Construction', 'construction', '', '{filedir_3}cat-image.jpg', 1);
INSERT INTO `exp_categories` VALUES(7, 1, 1, 0, 'GPS Equipment', 'gps-equipment', 'Lorem ipsum stuff', '{filedir_3}cat-image.jpg', 1);
INSERT INTO `exp_categories` VALUES(8, 1, 1, 2, 'Levels', 'levels', 'Test content', '{filedir_3}cat-image.jpg', 1);
INSERT INTO `exp_categories` VALUES(9, 1, 1, 2, 'Theyodolites', 'theyodolites', 'Test content', '{filedir_3}cat-image.jpg', 2);
INSERT INTO `exp_categories` VALUES(10, 1, 2, 0, 'Roads And Surfaces', 'roads-and-surfaces', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim. ', '{filedir_3}services-temp-top.jpg', 3);
INSERT INTO `exp_categories` VALUES(11, 1, 2, 0, 'Mapping', 'mapping', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim. ', '{filedir_3}services-temp-top.jpg', 2);
INSERT INTO `exp_categories` VALUES(12, 1, 2, 0, 'Lorem ipsum', 'lorem-ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim. ', '{filedir_3}services-temp-top.jpg', 1);

/* Table structure for table `exp_category_field_data` */
DROP TABLE IF EXISTS `exp_category_field_data`;

CREATE TABLE `exp_category_field_data` (
  `cat_id` int(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_category_field_data` */
INSERT INTO `exp_category_field_data` VALUES(1, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(2, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(3, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(4, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(5, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(6, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(7, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(8, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(9, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(10, 1, 2);
INSERT INTO `exp_category_field_data` VALUES(11, 1, 2);
INSERT INTO `exp_category_field_data` VALUES(12, 1, 2);

/* Table structure for table `exp_category_fields` */
DROP TABLE IF EXISTS `exp_category_fields`;

CREATE TABLE `exp_category_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `field_label` varchar(50) NOT NULL DEFAULT '',
  `field_type` varchar(12) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_maxl` smallint(3) NOT NULL DEFAULT '128',
  `field_ta_rows` tinyint(2) NOT NULL DEFAULT '8',
  `field_default_fmt` varchar(40) NOT NULL DEFAULT 'none',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`field_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_category_groups` */
DROP TABLE IF EXISTS `exp_category_groups`;

CREATE TABLE `exp_category_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `sort_order` char(1) NOT NULL DEFAULT 'a',
  `exclude_group` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `can_edit_categories` text,
  `can_delete_categories` text,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_category_groups` */
INSERT INTO `exp_category_groups` VALUES(1, 1, 'Product Categories', 'a', 0, 'all', '', '');
INSERT INTO `exp_category_groups` VALUES(2, 1, 'Service Categories', 'a', 0, 'all', '', '');

/* Table structure for table `exp_category_posts` */
DROP TABLE IF EXISTS `exp_category_posts`;

CREATE TABLE `exp_category_posts` (
  `entry_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_category_posts` */
INSERT INTO `exp_category_posts` VALUES(1, 2);
INSERT INTO `exp_category_posts` VALUES(1, 3);
INSERT INTO `exp_category_posts` VALUES(1, 6);
INSERT INTO `exp_category_posts` VALUES(2, 2);
INSERT INTO `exp_category_posts` VALUES(2, 3);
INSERT INTO `exp_category_posts` VALUES(2, 6);
INSERT INTO `exp_category_posts` VALUES(3, 2);
INSERT INTO `exp_category_posts` VALUES(3, 3);
INSERT INTO `exp_category_posts` VALUES(3, 6);
INSERT INTO `exp_category_posts` VALUES(26, 10);
INSERT INTO `exp_category_posts` VALUES(35, 11);

/* Table structure for table `exp_channel_data` */
DROP TABLE IF EXISTS `exp_channel_data`;

CREATE TABLE `exp_channel_data` (
  `entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `field_id_1` text,
  `field_ft_1` tinytext,
  `field_id_2` text,
  `field_ft_2` tinytext,
  `field_id_3` text,
  `field_ft_3` tinytext,
  `field_id_4` text,
  `field_ft_4` tinytext,
  `field_id_5` text,
  `field_ft_5` tinytext,
  `field_id_6` text,
  `field_ft_6` tinytext,
  `field_id_7` text,
  `field_ft_7` tinytext,
  `field_id_8` text,
  `field_ft_8` tinytext,
  `field_id_9` text,
  `field_ft_9` tinytext,
  `field_id_10` text,
  `field_ft_10` tinytext,
  `field_id_11` text,
  `field_ft_11` tinytext,
  `field_id_12` text,
  `field_ft_12` tinytext,
  `field_id_13` text,
  `field_ft_13` tinytext,
  `field_id_14` text,
  `field_ft_14` tinytext,
  `field_id_15` text,
  `field_ft_15` tinytext,
  `field_id_16` int(10) DEFAULT '0',
  `field_ft_16` tinytext,
  `field_id_17` text,
  `field_ft_17` tinytext,
  `field_id_18` text,
  `field_ft_18` tinytext,
  `field_id_19` text,
  `field_ft_19` tinytext,
  `field_id_20` text,
  `field_ft_20` tinytext,
  `field_id_22` text,
  `field_ft_22` tinytext,
  `field_id_23` text,
  `field_ft_23` tinytext,
  `field_id_24` text,
  `field_ft_24` tinytext,
  `field_id_25` text,
  `field_ft_25` tinytext,
  `field_id_26` text,
  `field_ft_26` tinytext,
  `field_id_27` text,
  `field_ft_27` tinytext,
  `field_id_28` text,
  `field_ft_28` tinytext,
  `field_id_29` text,
  `field_ft_29` tinytext,
  `field_id_30` text,
  `field_ft_30` tinytext,
  `field_id_31` text,
  `field_ft_31` tinytext,
  `field_id_32` text,
  `field_ft_32` tinytext,
  `field_id_33` text,
  `field_ft_33` tinytext,
  `field_id_34` text,
  `field_ft_34` tinytext,
  `field_id_35` text,
  `field_ft_35` tinytext,
  `field_id_36` text,
  `field_ft_36` tinytext,
  `field_id_37` int(10) DEFAULT '0',
  `field_ft_37` tinytext,
  `field_id_38` text,
  `field_ft_38` tinytext,
  `field_id_39` text,
  `field_ft_39` tinytext,
  `field_id_40` text,
  `field_ft_40` tinytext,
  `field_id_42` text,
  `field_ft_42` tinytext,
  `field_id_43` text,
  `field_ft_43` tinytext,
  `field_id_44` text,
  `field_ft_44` tinytext,
  `field_id_45` text,
  `field_ft_45` tinytext,
  `field_id_46` text,
  `field_ft_46` tinytext,
  `field_id_47` text,
  `field_ft_47` tinytext,
  `field_id_48` text,
  `field_ft_48` tinytext,
  `field_id_49` text,
  `field_ft_49` tinytext,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_channel_data` */
INSERT INTO `exp_channel_data` VALUES(1, 1, 1, 'NZD\nAUD\n', 'none', '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', 'Yes', 'none', '1', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(2, 1, 1, 'NZD\nAUD\n', 'none', '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', 'Yes', 'none', '1', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '{filedir_1}mega-menu-feature-product-image_(1).jpg', 'none', '', 'none', '', 'none', '', 'none', 5, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(3, 1, 1, 'NZD\nAUD\n', 'none', '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '', 'none', '', 'none', '1', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(4, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'All', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(5, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'All', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(6, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'All', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(7, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'All', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(8, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'All', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(9, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'All', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(10, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'All', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(11, 1, 4, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quam justo, egestas sed tincidunt id, feugiat eu urna. Suspendisse ornare, magna cursus tristique ullamcorper, massa velit ultricies metus, eu ultrices massa eros nec tortor. Sed auctor vehicula quam, ac condimentum mauris tristique non. Sed sed sodales nisi. Aliquam erat volutpat. Vivamus sit amet felis et urna lacinia ullamcorper eget et metus. Nunc laoreet eleifend molestie.</p>', 'none', '1', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', 'NZ\nAU\n', 'none', '1', 'none', '<p>\n	G&#39;day Mate.</p>\n<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quam justo, egestas sed tincidunt id, feugiat eu urna. Suspendisse ornare, magna cursus tristique ullamcorper, massa velit ultricies metus, eu ultrices massa eros nec tortor. Sed auctor vehicula quam, ac condimentum mauris tristique non. Sed sed sodales nisi. Aliquam erat volutpat. Vivamus sit amet felis et urna lacinia ullamcorper eget et metus. Nunc laoreet eleifend molestie.</p>', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '<p>\n	Test lower content</p>', 'none', '<p>\n	Test lower content</p>', 'none');
INSERT INTO `exp_channel_data` VALUES(12, 1, 2, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 1, 'none', '1', 'none', 'The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(13, 1, 2, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 2, 'none', '1', 'none', 'The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(14, 1, 2, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 3, 'none', '1', 'none', 'The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(15, 1, 2, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 4, 'none', '1', 'none', 'The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(16, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', 'All', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(17, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', 'All', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(18, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', 'All', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(19, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', 'All', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(20, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', 'All', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(21, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', 'All', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(22, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', 'All', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(23, 1, 8, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '1', 'none', '1', 'none', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non felis dignissim leo faucibus commodo. Aliquam urna velit, sollicitudin eget mollis ac, condimentum quis sapien. Aliquam aliquam vestibulum libero. Donec scelerisque lorem sollicitudin leo rhoncus eget dignissim felis blandit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ', 'none', '{filedir_6}services.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', 'All', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(24, 1, 8, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '1', 'none', '1', 'none', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non felis dignissim leo faucibus commodo. Aliquam urna velit, sollicitudin eget mollis ac, condimentum quis sapien. Aliquam aliquam vestibulum libero. Donec scelerisque lorem sollicitudin leo rhoncus eget dignissim felis blandit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ', 'none', '{filedir_6}products.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', 'All', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(25, 1, 8, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '1', 'none', '1', 'none', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non felis dignissim leo faucibus commodo. Aliquam urna velit, sollicitudin eget mollis ac, condimentum quis sapien. Aliquam aliquam vestibulum libero. Donec scelerisque lorem sollicitudin leo rhoncus eget dignissim felis blandit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ', 'none', '{filedir_6}about.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', 'NZ', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(26, 1, 7, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '1', 'none', '{filedir_7}services-temp-top.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', 'NZ', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(27, 1, 10, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<h1>\n	<strong style=\"font-size: 12px;\">Synergy Positioning Systems Ltd is 100% New Zealand owned and operated. It was formed when two of the industry&rsquo;s most trusted companies merged in 2006 - Synergy Equipment and Systems Ltd and Geodetic Instruments Ltd. The result is a company that brings you over 30 years experience delivering the most advanced and innovative technologies combined with a commitment to outstanding customer service.</strong></h1>\n<p>\n	Our aim is to make your job faster and easier, more accurate and cost efficient. We&#39;ll do this by giving you expert and honest advice and by offering you the best solution from our range of innovative positioning systems.<br />\n	Reliability is paramount - equipment breakages cost time and money. There are many low quality brands on the market that just don&#39;t stand up to the rigours of our construction industry. We only sell reliable brands we can trust and service.</p>\n<p>\n	Our experienced team are always on hand for after sales support, product training and servicing, giving you the confidence and skills to get the best out of your equipment. Our product range is extensive and always growing.</p>\n<h2>\n	Why choose synergy?</h2>', 'none', '1', 'none', '{filedir_8}about-temp.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '1', 'none', '<p>\n	<strong>AUS CONTENT Synergy Positioning Systems Ltd is 100% New Zealand owned and operated. It was formed when two of the industry&rsquo;s most trusted companies merged in 2006 - Synergy Equipment and Systems Ltd and Geodetic Instruments Ltd. The result is a company that brings you over 30 years experience delivering the most advanced and innovative technologies combined with a commitment to outstanding customer service.</strong></p>\n<p>\n	Our aim is to make your job faster and easier, more accurate and cost efficient. We&#39;ll do this by giving you expert and honest advice and by offering you the best solution from our range of innovative positioning systems.<br />\n	Reliability is paramount - equipment breakages cost time and money. There are many low quality brands on the market that just don&#39;t stand up to the rigours of our construction industry. We only sell reliable brands we can trust and service.</p>\n<p>\n	Our experienced team are always on hand for after sales support, product training and servicing, giving you the confidence and skills to get the best out of your equipment. Our product range is extensive and always growing.</p>\n<h2>\n	Why choose synergy?</h2>', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(28, 1, 9, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<p>\n	<strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</strong></p>\n<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet.</p>', 'none', '', null, '', null, '', null, 'NZ\nAU\n', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(29, 1, 11, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '{filedir_10}footer-topcon.gif', 'none', '{filedir_10}topcon-logo.gif', 'none', '{filedir_10}mega-menu-feature-product-logo.jpg', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(30, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<p>\n	Test NZ article</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, 'NZ', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(31, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<p>\n	AU only!</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, 'AU', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(32, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	NZ only case study</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, 'NZ', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(33, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	AU only.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, 'AU', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(34, 1, 8, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '1', 'none', '1', 'none', 'Australia lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non felis dignissim leo faucibus commodo. Aliquam urna velit, sollicitudin eget mollis ac, condimentum quis sapien. Aliquam aliquam vestibulum libero. Donec scelerisque lorem sollicitudin leo rhoncus eget dignissim felis blandit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ', 'none', '{filedir_6}about.jpg', 'none', '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 'AU', 'xhtml', '', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(35, 1, 7, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '1', 'none', '{filedir_7}services-temp-top.jpg', 'none', '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 'All', 'xhtml', '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(36, 1, 10, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<p>\n	Test NZ content</p>', 'none', '', 'none', '{filedir_8}about-temp.jpg', 'none', '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', 'none', '<p>\n	Test AU content</p>', 'none', '', null, '', null, '', null, '', null, '', 'none', '', 'none');
INSERT INTO `exp_channel_data` VALUES(37, 1, 10, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<p>\n	Test content NZ</p>', 'none', '1', 'none', '{filedir_8}about-temp.jpg', 'none', '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', 'none', '<p>\n	Test content AU</p>', 'none', '', null, '', null, '', null, '', null, '', 'none', '', 'none');

/* Table structure for table `exp_channel_entries_autosave` */
DROP TABLE IF EXISTS `exp_channel_entries_autosave`;

CREATE TABLE `exp_channel_entries_autosave` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original_entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  `entry_data` text,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_channel_fields` */
DROP TABLE IF EXISTS `exp_channel_fields`;

CREATE TABLE `exp_channel_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL,
  `field_label` varchar(50) NOT NULL,
  `field_instructions` text,
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_pre_populate` char(1) NOT NULL DEFAULT 'n',
  `field_pre_channel_id` int(6) unsigned DEFAULT NULL,
  `field_pre_field_id` int(6) unsigned DEFAULT NULL,
  `field_related_to` varchar(12) NOT NULL DEFAULT 'channel',
  `field_related_id` int(6) unsigned NOT NULL DEFAULT '0',
  `field_related_orderby` varchar(12) NOT NULL DEFAULT 'date',
  `field_related_sort` varchar(4) NOT NULL DEFAULT 'desc',
  `field_related_max` smallint(4) NOT NULL DEFAULT '0',
  `field_ta_rows` tinyint(2) DEFAULT '8',
  `field_maxl` smallint(3) DEFAULT NULL,
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_search` char(1) NOT NULL DEFAULT 'n',
  `field_is_hidden` char(1) NOT NULL DEFAULT 'n',
  `field_fmt` varchar(40) NOT NULL DEFAULT 'xhtml',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_order` int(3) unsigned NOT NULL,
  `field_content_type` varchar(20) NOT NULL DEFAULT 'any',
  `field_settings` text,
  PRIMARY KEY (`field_id`),
  KEY `group_id` (`group_id`),
  KEY `field_type` (`field_type`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_channel_fields` */
INSERT INTO `exp_channel_fields` VALUES(1, 1, 1, 'product_prices', 'Product Prices', 'Enter the AU and NZ prices for this product in here and they will be dynamically output to the customer based on the website they are viewing.', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjIiO3M6ODoibWF4X3Jvd3MiO3M6MToiMiI7czo3OiJjb2xfaWRzIjthOjI6e2k6MDtzOjE6IjEiO2k6MTtzOjE6IjIiO319');
INSERT INTO `exp_channel_fields` VALUES(2, 1, 1, 'product_description', 'Product Description', 'On the product page, only the first couple of paragraphs will be shown until the user presses \"read more\"', 'wygwam', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIyIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(3, 1, 1, 'technical_specifications', 'Technical Specifications', 'Below you can create the technical specifications section for this product. Once a title is specified in the left column (i.e. Measurement Range) you can create a table in the right column of specifications relevant to this title. ', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MToiMyI7aToxO3M6MToiNCI7fX0=');
INSERT INTO `exp_channel_fields` VALUES(4, 1, 1, 'product_videos', 'Product Videos', 'Use this field to embed a Youtube video and give a title/description to the video. Any videos will appear under the \"Videos\" tab for this product.', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 4, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mzp7aTowO2k6NjtpOjE7aTo3O2k6MjtpOjg7fX0=');
INSERT INTO `exp_channel_fields` VALUES(5, 1, 1, 'rental_sections', 'Rental Sections', 'Add as many paragraphs to the rental section as you want.', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO2k6OTtpOjE7aToxMDt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(6, 1, 1, 'rental_pricing_nz', 'Rental Pricing (NZ)', 'This field controls the contents of the \"Availability & Costs\" section under the rental tab (for the NZ site).', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 6, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMTEiO2k6MTtzOjI6IjEyIjt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(7, 1, 1, 'rental_pricing_au', 'Rental Pricing (AU)', 'This field controls the contents of the \"Availability & Costs\" section under the rental tab (for the AU site).', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 7, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMTMiO2k6MTtzOjI6IjE0Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(8, 1, 1, 'featured_carousel_images', 'Featured Carousel Images', 'If this product is to be a featured product, you can upload many banner images to appear at the top of its featured page.\n\nThe banner will cycle between these images.', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 8, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMTUiO2k6MTtzOjI6IjI0Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(9, 1, 1, 'is_featured_product', 'Is Featured Product?', 'If you want this product to be a featured product, tick this checkbox.\n\nPlease make sure you have created some featured product entries for this product first (in the publish menu).', 'checkboxes', 'Yes', 'n', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 9, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');
INSERT INTO `exp_channel_fields` VALUES(10, 1, 1, 'product_images', 'Product Images', '', 'matrix', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 10, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6MTp7aTowO3M6MjoiMTYiO319');
INSERT INTO `exp_channel_fields` VALUES(11, 1, 1, 'product_downloads', 'Product Downloads', 'Upload files to be available in the \"downloads\" tab', 'matrix', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 11, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMTciO2k6MTtzOjI6IjE4Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(12, 1, 3, 'article_content', 'Article Content', 'This is the main body content of the news item. Please note the first paragraph will be automatically made bold.', 'wygwam', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(13, 1, 3, 'article_banner_image', 'Article Banner Image', 'This is the image that appears at the top of the news article and on the news page. Please note the images should be exactly 866px by 373px and will have a thumbnail automatically cropped to 396px by 88px for the news page.', 'file', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiNCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(14, 1, 4, 'contact_introduction', 'Contact Introduction', 'The initial content before the branch information.', 'wygwam', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(15, 1, 4, 'branches', 'Branches', 'Please note the first branch entered here will be shown on the home page for the New Zealand site.', 'matrix', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'y', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Njp7aTowO3M6MjoiMTkiO2k6MTtzOjI6IjIwIjtpOjI7czoyOiIyMSI7aTozO3M6MjoiMjIiO2k6NDtzOjI6IjIzIjtpOjU7czoyOiI0NyI7fX0=');
INSERT INTO `exp_channel_fields` VALUES(16, 1, 2, 'featured_product', 'Featured Product', 'Pick the featured product this sub page is related to.', 'rel', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'y', 'n', 'none', 'n', 1, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');
INSERT INTO `exp_channel_fields` VALUES(17, 1, 2, 'featured_content', 'Featured Content', 'Each block of content you add will create a new block separated by a horizontal rule.', 'matrix', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6MTp7aTowO3M6MjoiMjUiO319');
INSERT INTO `exp_channel_fields` VALUES(18, 1, 2, 'featured_content_intro', 'Featured Content Intro', 'This is shown on the featured product page as the preview for this page.', 'textarea', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');
INSERT INTO `exp_channel_fields` VALUES(19, 1, 5, 'study_content', 'Study Content', 'This is the main body content of the news item. Please note the first paragraph will be automatically made bold.', 'wygwam', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(20, 1, 5, 'study_banner_image', 'Study Banner Image', '', 'file', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiNSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(22, 1, 7, 'home_marketing_message', 'Home Marketing Message', 'This is the large and smaller text that appears over the top of the banner image.', 'matrix', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO2k6MjY7aToxO2k6Mjc7fX0=');
INSERT INTO `exp_channel_fields` VALUES(23, 1, 7, 'home_banner_links', 'Home Banner Links', 'This field controls the content and location of the home page banner links (both over the banner and below in the tabs).', 'matrix', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 0, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMjgiO2k6MTtzOjI6IjI5Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(24, 1, 7, 'home_banner_tab_content', 'Home Banner Tab Content', 'The content of the tabs below the banners. A limit of 190 characters will be shown from this box.', 'textarea', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 3, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 4, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');
INSERT INTO `exp_channel_fields` VALUES(25, 1, 7, 'home_banner_image', 'Home Banner Image', 'This is the large background image shown in the banner.', 'file', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiNiI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(26, 1, 6, 'service_content', 'Service Content', 'Each block of content you add will create a new block separated by a horizontal rule.', 'matrix', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6MTp7aTowO2k6MzA7fX0=');
INSERT INTO `exp_channel_fields` VALUES(27, 1, 6, 'services_banner', 'Services Banner', 'An image with dimensions of 866px - 305px is best.', 'file', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiNyI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(28, 1, 8, 'management_page_content', 'Page Content', 'This is the content at the top of the management page.', 'wygwam', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo5OntzOjc6ImNvbnZlcnQiO3M6MDoiIjtzOjY6ImNvbmZpZyI7czoxOiI1IjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(29, 1, 9, 'about_page_content', 'Page Content', '', 'wygwam', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiI1IjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(30, 1, 9, 'about_us_grid', 'About Us Grid', 'Add as many blocks as you want to the bottom of the about us page', 'matrix', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMzIiO2k6MTtzOjI6IjMxIjt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(31, 1, 9, 'about_page_banner', 'About Page Banner', 'This should be a banner image ideally 866px x 305px, however it may be larger. The image will be constricted width-wise to 866px.', 'file', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiOCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(32, 1, 8, 'staff_members', 'Staff Members', 'Enter a row for each staff member here.', 'matrix', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6NTp7aTowO3M6MjoiMzMiO2k6MTtzOjI6IjM0IjtpOjI7czoyOiIzNSI7aTozO3M6MjoiNDYiO2k6NDtzOjI6IjQ5Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(33, 1, 1, 'featured_megamenu_icon', 'Featured Megamenu Icon', 'This is the small image that appears in the mega menu for features products. This needs to be an image with the dimensions 302px x 89px and will be constrained to this when displayed. Please note if this image is not supplied on a featured product, it will not be displayed in the featured section of the products megamenu.', 'file', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 12, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiMSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(34, 1, 10, 'manufacturer_footer_image', 'Footer Image', 'This image is output in the footer. It should be a grey/white text on a background colour of #3c3c3c.\nIt must be 30px in height and no wider than 225px (it can be smaller).', 'file', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MjoiMTAiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(35, 1, 10, 'manufacturer_product_page_image', 'Product Page Image', 'This image is used on the product page when you select this manufacturer for the given product. The logo in the design is 140px wide and 24px high (so we would recommend something similar), however you can upload different sizes as long as they fit in your page.', 'file', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MjoiMTAiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(36, 1, 10, 'featured_product_megamenu_image', 'Featured Product Megamenu Image', 'This image is only ever output in the product mega menu if a product with this manufacturer is a featured product. This is a fixed 166px by 49px image and will be constrained to this. Please note if this is not set and this product is featured, the product will not show in the featured megamenu block.', 'file', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 3, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MjoiMTAiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(37, 1, 1, 'manufacturer', 'Manufacturer', 'This field is used to display the manufacturer logo on the product page.', 'rel', '', '0', 0, 0, 'channel', 11, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 13, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');
INSERT INTO `exp_channel_fields` VALUES(38, 1, 4, 'header_contact_details', 'Header Contact Details', 'This is the contact information that is shown in the header of the website.', 'matrix', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjIiO3M6ODoibWF4X3Jvd3MiO3M6MToiMiI7czo3OiJjb2xfaWRzIjthOjM6e2k6MDtzOjI6IjM4IjtpOjE7czoyOiIzNiI7aToyO3M6MjoiMzciO319');
INSERT INTO `exp_channel_fields` VALUES(39, 1, 4, 'branches_au', 'Branches', 'Please note the first branch entered here will be shown on the home page for the Australian site.', 'matrix', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'y', 'n', 'none', 'n', 4, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Njp7aTowO3M6MjoiMzkiO2k6MTtzOjI6IjQwIjtpOjI7czoyOiI0MSI7aTozO3M6MjoiNDIiO2k6NDtzOjI6IjQzIjtpOjU7aTo0ODt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(40, 1, 4, 'contact_introduction_au', 'Contact Introduction', 'The initial content before the branch information.', 'wygwam', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(42, 1, 9, 'about_us_grid_au', 'About Us Grid', 'Add as many blocks as you want to the bottom of the about us page', 'matrix', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiNDQiO2k6MTtzOjI6IjQ1Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(43, 1, 9, 'about_page_content_au', 'Page Content', '', 'wygwam', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiI1IjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(44, 1, 3, 'article_country', 'Article Country', 'Use this to select whether the article should only be available on NZ, AU or both countries.', 'pt_dropdown', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'y', 'n', 'xhtml', 'n', 3, 'any', 'YTo3OntzOjc6Im9wdGlvbnMiO2E6Mzp7czozOiJBbGwiO3M6MzoiQWxsIjtzOjI6Ik5aIjtzOjI6Ik5aIjtzOjI6IkFVIjtzOjI6IkFVIjt9czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(45, 1, 5, 'study_country', 'Study Country', 'Use this to select whether the case study should only be available on NZ, AU or both countries.', 'pt_dropdown', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'y', 'n', 'xhtml', 'n', 3, 'any', 'YTo3OntzOjc6Im9wdGlvbnMiO2E6Mzp7czozOiJBbGwiO3M6MzoiQWxsIjtzOjI6Ik5aIjtzOjI6Ik5aIjtzOjI6IkFVIjtzOjI6IkFVIjt9czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(46, 1, 7, 'home_banner_country', 'Home Banner Country', 'Whether this banner is to be used for the NZ, AU or both home pages.', 'pt_dropdown', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'y', 'n', 'xhtml', 'n', 5, 'any', 'YTo3OntzOjc6Im9wdGlvbnMiO2E6Mzp7czozOiJBbGwiO3M6MzoiQWxsIjtzOjI6Ik5aIjtzOjI6Ik5aIjtzOjI6IkFVIjtzOjI6IkFVIjt9czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(47, 1, 6, 'services_country', 'Services Country', 'Use this field to make services available in specific countries.', 'pt_dropdown', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'y', 'n', 'xhtml', 'n', 3, 'any', 'YTo3OntzOjc6Im9wdGlvbnMiO2E6Mzp7czozOiJBbGwiO3M6MzoiQWxsIjtzOjI6Ik5aIjtzOjI6Ik5aIjtzOjI6IkFVIjtzOjI6IkFVIjt9czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(48, 1, 4, 'contact_lower_content', 'Contact Lower Content', 'Optional content to be output below the branches.', 'wygwam', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 6, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiI1IjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(49, 1, 4, 'contact_lower_content_au', 'Contact Lower Content (AU)', '', 'wygwam', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 7, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiI1IjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');

/* Table structure for table `exp_channel_member_groups` */
DROP TABLE IF EXISTS `exp_channel_member_groups`;

CREATE TABLE `exp_channel_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `channel_id` int(6) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_channel_titles` */
DROP TABLE IF EXISTS `exp_channel_titles`;

CREATE TABLE `exp_channel_titles` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_channel_titles` */
INSERT INTO `exp_channel_titles` VALUES(1, 1, 1, 1, 0, null, '127.0.0.1', 'Topcon GTS-100N Construction', 'topcon-gts-100n-construction', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358293518, 'n', '2013', '01', '16', 0, 0, 20130411225519, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(2, 1, 1, 1, 0, null, '127.0.0.1', 'Topcon GTS-100N Robotic', 'topcon-gts-100n-robotic', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358293544, 'n', '2013', '01', '16', 0, 0, 20130412020645, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(3, 1, 1, 1, 0, null, '127.0.0.1', 'Topcon GTS-100N Windows Based', 'topcon-gts-100n-windows-based', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358293500, 'n', '2013', '01', '16', 0, 0, 20130313211401, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(4, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 1', 'news-article-1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961454, 'n', '2013', '03', '11', 0, 0, 20130415223715, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(5, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 2', 'news-article-2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961449, 'n', '2013', '03', '11', 0, 0, 20130415223710, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(6, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 6', 'news-article-6', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961456, 'n', '2013', '03', '11', 0, 0, 20130415223717, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(7, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 5', 'news-article-5', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961446, 'n', '2013', '03', '11', 0, 0, 20130415223707, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(8, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 4', 'news-article-4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961461, 'n', '2013', '03', '11', 0, 0, 20130415223722, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(9, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 3', 'news-article-3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961459, 'n', '2013', '03', '11', 0, 0, 20130415223720, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(10, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 7', 'news-article-7', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961451, 'n', '2013', '03', '11', 0, 0, 20130415223712, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(11, 1, 4, 1, 0, null, '127.0.0.1', 'Contact Us', 'contact-us', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362967284, 'n', '2013', '03', '11', 0, 0, 20130419020525, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(12, 1, 2, 1, 0, null, '127.0.0.1', 'BRAMOR Orthophoto', 'bramor-orthophoto', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363822454, 'n', '2013', '03', '21', 0, 0, 20130320235515, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(13, 1, 2, 1, 0, null, '127.0.0.1', 'BRAMOR Orthophoto 2', 'bramor-orthophoto-2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363822493, 'n', '2013', '03', '21', 0, 0, 20130321123453, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(14, 1, 2, 1, 0, null, '127.0.0.1', 'BRAMOR Orthophoto 3', 'bramor-orthophoto-3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363822446, 'n', '2013', '03', '21', 0, 0, 20130321123406, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(15, 1, 2, 1, 0, null, '127.0.0.1', 'BRAMOR Orthophoto 4', 'bramor-orthophoto-4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363822463, 'n', '2013', '03', '21', 0, 0, 20130321123423, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(16, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study', 'test-case-study', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903639, 'n', '2013', '03', '22', 0, 0, 20130415230120, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(17, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 1', 'test-case-study-1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903727, 'n', '2013', '03', '22', 0, 0, 20130415230148, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(18, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 2', 'test-case-study-2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903691, 'n', '2013', '03', '22', 0, 0, 20130415230212, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(19, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 3', 'test-case-study-3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903655, 'n', '2013', '03', '22', 0, 0, 20130415230136, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(20, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 4', 'test-case-study-4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903737, 'n', '2013', '03', '22', 0, 0, 20130415230158, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(21, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 5', 'test-case-study-5', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903721, 'n', '2013', '03', '22', 0, 0, 20130415230142, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(22, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 6', 'test-case-study-6', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903698, 'n', '2013', '03', '22', 0, 0, 20130415230219, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(23, 1, 8, 1, 0, null, '127.0.0.1', 'Services', 'services', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365459710, 'n', '2013', '04', '09', 0, 0, 20130416010651, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(24, 1, 8, 1, 0, null, '127.0.0.1', 'Products', 'products', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365460066, 'n', '2013', '04', '09', 0, 0, 20130416010647, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(25, 1, 8, 1, 0, null, '127.0.0.1', 'About Us NZ', 'about-us', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365460126, 'n', '2013', '04', '09', 0, 0, 20130416010947, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(26, 1, 7, 1, 0, null, '127.0.0.1', 'Survey & Design', 'survey-design', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365483884, 'n', '2013', '04', '09', 0, 0, 20130416083245, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(27, 1, 10, 1, 0, null, '127.0.0.1', 'About Us', 'about-us', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365633138, 'n', '2013', '04', '11', 0, 0, 20130415002319, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(28, 1, 9, 1, 0, null, '127.0.0.1', 'Management', 'management', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365635704, 'n', '2013', '04', '11', 0, 0, 20130421225705, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(29, 1, 11, 1, 0, null, '127.0.0.1', 'Topcon', 'topcon', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365731435, 'n', '2013', '04', '12', 0, 0, 20130412145035, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(30, 1, 3, 1, 0, null, '127.0.0.1', 'Test NZ only article', 'test-nz-only-article', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1366065476, 'n', '2013', '04', '16', 0, 0, 20130416113756, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(31, 1, 3, 1, 0, null, '127.0.0.1', 'Test AU only article', 'test-au-only-article', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1366065432, 'n', '2013', '04', '16', 0, 0, 20130416113712, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(32, 1, 5, 1, 0, null, '127.0.0.1', 'NZ Only Case Study', 'nz-only-case-study', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1366066443, 'n', '2013', '04', '16', 0, 0, 20130415230204, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(33, 1, 5, 1, 0, null, '127.0.0.1', 'AU Only Case Study', 'au-only-case-study', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1366066488, 'n', '2013', '04', '16', 0, 0, 20130415225849, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(34, 1, 8, 1, 0, null, '127.0.0.1', 'About Us AU', 'about-us-au', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365460128, 'n', '2013', '04', '09', 0, 0, 20130416010949, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(35, 1, 7, 1, 0, null, '127.0.0.1', 'Other test service', 'other-test-service', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365483870, 'n', '2013', '04', '09', 0, 0, 20130416092931, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(36, 1, 10, 1, 0, null, '127.0.0.1', 'History', 'history', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1366105507, 'n', '2013', '04', '16', 0, 0, 20130416224507, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(37, 1, 10, 1, 0, null, '127.0.0.1', 'Test about us page', 'test-about-us-page', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1366153158, 'n', '2013', '04', '17', 0, 0, 20130417115918, 0, 0);

/* Table structure for table `exp_channels` */
DROP TABLE IF EXISTS `exp_channels`;

CREATE TABLE `exp_channels` (
  `channel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_name` varchar(40) NOT NULL,
  `channel_title` varchar(100) NOT NULL,
  `channel_url` varchar(100) NOT NULL,
  `channel_description` varchar(225) DEFAULT NULL,
  `channel_lang` varchar(12) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `cat_group` varchar(255) DEFAULT NULL,
  `status_group` int(4) unsigned DEFAULT NULL,
  `deft_status` varchar(50) NOT NULL DEFAULT 'open',
  `field_group` int(4) unsigned DEFAULT NULL,
  `search_excerpt` int(4) unsigned DEFAULT NULL,
  `deft_category` varchar(60) DEFAULT NULL,
  `deft_comments` char(1) NOT NULL DEFAULT 'y',
  `channel_require_membership` char(1) NOT NULL DEFAULT 'y',
  `channel_max_chars` int(5) unsigned DEFAULT NULL,
  `channel_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `channel_allow_img_urls` char(1) NOT NULL DEFAULT 'y',
  `channel_auto_link_urls` char(1) NOT NULL DEFAULT 'n',
  `channel_notify` char(1) NOT NULL DEFAULT 'n',
  `channel_notify_emails` varchar(255) DEFAULT NULL,
  `comment_url` varchar(80) DEFAULT NULL,
  `comment_system_enabled` char(1) NOT NULL DEFAULT 'y',
  `comment_require_membership` char(1) NOT NULL DEFAULT 'n',
  `comment_use_captcha` char(1) NOT NULL DEFAULT 'n',
  `comment_moderate` char(1) NOT NULL DEFAULT 'n',
  `comment_max_chars` int(5) unsigned DEFAULT '5000',
  `comment_timelock` int(5) unsigned NOT NULL DEFAULT '0',
  `comment_require_email` char(1) NOT NULL DEFAULT 'y',
  `comment_text_formatting` char(5) NOT NULL DEFAULT 'xhtml',
  `comment_html_formatting` char(4) NOT NULL DEFAULT 'safe',
  `comment_allow_img_urls` char(1) NOT NULL DEFAULT 'n',
  `comment_auto_link_urls` char(1) NOT NULL DEFAULT 'y',
  `comment_notify` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_authors` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_emails` varchar(255) DEFAULT NULL,
  `comment_expiration` int(4) unsigned NOT NULL DEFAULT '0',
  `search_results_url` varchar(80) DEFAULT NULL,
  `ping_return_url` varchar(80) DEFAULT NULL,
  `show_button_cluster` char(1) NOT NULL DEFAULT 'y',
  `rss_url` varchar(80) DEFAULT NULL,
  `enable_versioning` char(1) NOT NULL DEFAULT 'n',
  `max_revisions` smallint(4) unsigned NOT NULL DEFAULT '10',
  `default_entry_title` varchar(100) DEFAULT NULL,
  `url_title_prefix` varchar(80) DEFAULT NULL,
  `live_look_template` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`),
  KEY `cat_group` (`cat_group`),
  KEY `status_group` (`status_group`),
  KEY `field_group` (`field_group`),
  KEY `channel_name` (`channel_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_channels` */
INSERT INTO `exp_channels` VALUES(1, 1, 'products', 'Products', 'http://synergy.local/', null, 'en', 3, 0, 1358293544, 0, '1', 1, 'open', 1, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(2, 1, 'featured_product_pages', 'Featured Product Pages', 'http://synergy.local/', null, 'en', 4, 0, 1363822493, 0, '', 1, 'open', 2, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(3, 1, 'news', 'News', 'http://synergy.local/', null, 'en', 9, 0, 1366065476, 0, '', 1, 'open', 3, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(4, 1, 'contact_us', 'Contact Us', 'http://synergy.local/', null, 'en', 1, 0, 1362967284, 0, '', 1, 'open', 4, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(5, 1, 'case_studies', 'Case Studies', 'http://synergy.local/', null, 'en', 9, 0, 1366066488, 0, '', 1, 'open', 5, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(7, 1, 'services', 'Services', 'http://synergy.local/', null, 'en', 2, 0, 1365483884, 0, '2', 1, 'open', 6, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(8, 1, 'home_page_banners', 'Home Page Banners', 'http://synergy.local/', null, 'en', 4, 0, 1365460128, 0, '', 1, 'open', 7, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(9, 1, 'management', 'Management', 'http://synergy.local/', null, 'en', 1, 0, 1365635704, 0, '', 1, 'open', 8, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(10, 1, 'about_us', 'About Us', 'http://synergy.local/', null, 'en', 3, 0, 1366153158, 0, '', 1, 'open', 9, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(11, 1, 'manufacturers', 'Manufacturers', 'http://synergy.local/', null, 'en', 1, 0, 1365731435, 0, '', 1, 'open', 10, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);

/* Table structure for table `exp_comment_subscriptions` */
DROP TABLE IF EXISTS `exp_comment_subscriptions`;

CREATE TABLE `exp_comment_subscriptions` (
  `subscription_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `member_id` int(10) DEFAULT '0',
  `email` varchar(50) DEFAULT NULL,
  `subscription_date` varchar(10) DEFAULT NULL,
  `notification_sent` char(1) DEFAULT 'n',
  `hash` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`subscription_id`),
  KEY `entry_id` (`entry_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_comments` */
DROP TABLE IF EXISTS `exp_comments`;

CREATE TABLE `exp_comments` (
  `comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT '0',
  `channel_id` int(4) unsigned DEFAULT '1',
  `author_id` int(10) unsigned DEFAULT '0',
  `status` char(1) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `url` varchar(75) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `comment_date` int(10) DEFAULT NULL,
  `edit_date` int(10) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`comment_id`),
  KEY `entry_id` (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `status` (`status`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_cp_log` */
DROP TABLE IF EXISTS `exp_cp_log`;

CREATE TABLE `exp_cp_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `username` varchar(32) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `act_date` int(10) NOT NULL,
  `action` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_cp_log` */
INSERT INTO `exp_cp_log` VALUES(1, 1, 1, '96black', '127.0.0.1', 1355881673, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(2, 1, 1, '96black', '127.0.0.1', 1355883062, 'Site Updated&nbsp;&nbsp;Synergy Positioning Systems (NZ)');
INSERT INTO `exp_cp_log` VALUES(3, 1, 1, '96black', '127.0.0.1', 1355883097, 'Site Created&nbsp;&nbsp;Synergy Positioning Systems (AU)');
INSERT INTO `exp_cp_log` VALUES(4, 2, 1, '96black', '127.0.0.1', 1355943021, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(5, 2, 1, '96black', '127.0.0.1', 1355945651, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(6, 1, 1, '96black', '127.0.0.1', 1356034419, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(7, 2, 1, '96black', '127.0.0.1', 1356039515, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(8, 2, 1, '96black', '127.0.0.1', 1356039525, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(9, 1, 1, '96black', '127.0.0.1', 1358202174, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(10, 1, 1, '96black', '127.0.0.1', 1358202207, 'Channel Created:&nbsp;&nbsp;Products');
INSERT INTO `exp_cp_log` VALUES(11, 1, 1, '96black', '127.0.0.1', 1358202239, 'Field Group Created:&nbsp;Products');
INSERT INTO `exp_cp_log` VALUES(12, 1, 1, '96black', '127.0.0.1', 1358210877, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(13, 1, 1, '96black', '127.0.0.1', 1358218127, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(14, 1, 1, '96black', '127.0.0.1', 1358218180, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(15, 1, 1, '96black', '127.0.0.1', 1358218184, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(16, 1, 1, '96black', '127.0.0.1', 1358218208, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(17, 1, 1, '96black', '127.0.0.1', 1358220028, 'Channel Created:&nbsp;&nbsp;Featured Product Pages');
INSERT INTO `exp_cp_log` VALUES(18, 1, 1, '96black', '127.0.0.1', 1358220074, 'Field Group Created:&nbsp;Featured Product Pages');
INSERT INTO `exp_cp_log` VALUES(19, 1, 1, '96black', '127.0.0.1', 1358220742, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(20, 1, 1, '96black', '127.0.0.1', 1358274914, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(21, 1, 1, '96black', '127.0.0.1', 1358279266, 'Category Group Created:&nbsp;&nbsp;Product Categories');
INSERT INTO `exp_cp_log` VALUES(22, 1, 1, '96black', '127.0.0.1', 1358293233, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(23, 1, 1, '96black', '127.0.0.1', 1358362941, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(24, 1, 1, '96black', '127.0.0.1', 1359494506, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(25, 1, 1, '96black', '127.0.0.1', 1362948766, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(26, 1, 1, '96black', '127.0.0.1', 1362960488, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(27, 1, 1, '96black', '127.0.0.1', 1362960564, 'Channel Created:&nbsp;&nbsp;News');
INSERT INTO `exp_cp_log` VALUES(28, 1, 1, '96black', '127.0.0.1', 1362960570, 'Field Group Created:&nbsp;News');
INSERT INTO `exp_cp_log` VALUES(29, 1, 1, '96black', '127.0.0.1', 1362966730, 'Field Group Created:&nbsp;Contact Us');
INSERT INTO `exp_cp_log` VALUES(30, 1, 1, '96black', '127.0.0.1', 1362966734, 'Channel Created:&nbsp;&nbsp;Contact Us');
INSERT INTO `exp_cp_log` VALUES(31, 1, 1, '96black', '127.0.0.1', 1362974544, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(32, 1, 1, '96black', '127.0.0.1', 1362976053, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(33, 1, 1, '96black', '127.0.0.1', 1362976069, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(34, 1, 1, '96black', '127.0.0.1', 1362976093, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(35, 1, 1, '96black', '127.0.0.1', 1362992592, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(36, 1, 1, '96black', '127.0.0.1', 1363143606, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(37, 1, 1, '96black', '127.0.0.1', 1363209017, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(38, 1, 1, '96black', '127.0.0.1', 1363320699, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(39, 1, 1, '96black', '127.0.0.1', 1363567001, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(40, 1, 1, '96black', '127.0.0.1', 1363577373, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(41, 1, 1, '96black', '127.0.0.1', 1363578978, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(42, 1, 1, '96black', '127.0.0.1', 1363579481, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(43, 1, 1, '96black', '127.0.0.1', 1363660090, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(44, 1, 1, '96black', '127.0.0.1', 1363729623, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(45, 1, 1, '96black', '127.0.0.1', 1363736810, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(46, 1, 1, '96black', '127.0.0.1', 1363739939, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(47, 1, 1, '96black', '127.0.0.1', 1363739963, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(48, 1, 1, '96black', '127.0.0.1', 1363747179, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(49, 1, 1, '96black', '127.0.0.1', 1363747203, 'Category Group Created:&nbsp;&nbsp;Service Categories');
INSERT INTO `exp_cp_log` VALUES(50, 1, 1, '96black', '127.0.0.1', 1363820443, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(51, 1, 1, '96black', '127.0.0.1', 1363829155, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(52, 1, 1, '96black', '127.0.0.1', 1363833477, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(53, 1, 1, '96black', '127.0.0.1', 1363840598, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(54, 1, 1, '96black', '127.0.0.1', 1363902966, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(55, 1, 1, '96black', '127.0.0.1', 1363903295, 'Channel Created:&nbsp;&nbsp;Case Studies');
INSERT INTO `exp_cp_log` VALUES(56, 1, 1, '96black', '127.0.0.1', 1363903335, 'Field Group Created:&nbsp;Case Studies');
INSERT INTO `exp_cp_log` VALUES(57, 1, 1, '96black', '127.0.0.1', 1364159867, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(58, 1, 1, '96black', '127.0.0.1', 1364160031, 'Channel Created:&nbsp;&nbsp;Servoces');
INSERT INTO `exp_cp_log` VALUES(59, 1, 1, '96black', '127.0.0.1', 1364160036, 'Field Group Created:&nbsp;Services');
INSERT INTO `exp_cp_log` VALUES(60, 1, 1, '96black', '127.0.0.1', 1364160044, 'Channel Deleted:&nbsp;&nbsp;Servoces');
INSERT INTO `exp_cp_log` VALUES(61, 1, 1, '96black', '127.0.0.1', 1364160059, 'Channel Created:&nbsp;&nbsp;Services');
INSERT INTO `exp_cp_log` VALUES(62, 1, 1, '96black', '127.0.0.1', 1364160673, 'Channel Created:&nbsp;&nbsp;Home Page Banners');
INSERT INTO `exp_cp_log` VALUES(63, 1, 1, '96black', '127.0.0.1', 1364160678, 'Field Group Created:&nbsp;Home Page Banners');
INSERT INTO `exp_cp_log` VALUES(64, 1, 1, '96black', '127.0.0.1', 1365458478, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(65, 1, 1, '96black', '127.0.0.1', 1365459691, 'Custom Field Deleted:&nbsp;Home Main Heading');
INSERT INTO `exp_cp_log` VALUES(66, 1, 1, '96black', '127.0.0.1', 1365466907, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(67, 1, 1, '96black', '127.0.0.1', 1365467178, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(68, 1, 1, '96black', '127.0.0.1', 1365467422, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(69, 1, 1, '96black', '127.0.0.1', 1365467428, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(70, 1, 1, '96black', '127.0.0.1', 1365467460, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(71, 1, 1, '96black', '127.0.0.1', 1365467784, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(72, 1, 1, '96black', '127.0.0.1', 1365467819, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(73, 1, 1, '96black', '127.0.0.1', 1365467842, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(74, 1, 1, '96black', '127.0.0.1', 1365471782, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(75, 1, 1, '96black', '127.0.0.1', 1365472101, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(76, 1, 1, '96black', '127.0.0.1', 1365479239, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(77, 1, 1, '96black', '127.0.0.1', 1365479505, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(78, 1, 1, '96black', '127.0.0.1', 1365479533, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(79, 1, 1, '96black', '127.0.0.1', 1365558230, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(80, 1, 1, '96black', '127.0.0.1', 1365630778, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(81, 1, 1, '96black', '127.0.0.1', 1365630802, 'Channel Created:&nbsp;&nbsp;Management');
INSERT INTO `exp_cp_log` VALUES(82, 1, 1, '96black', '127.0.0.1', 1365630809, 'Field Group Created:&nbsp;Management');
INSERT INTO `exp_cp_log` VALUES(83, 1, 1, '96black', '127.0.0.1', 1365632300, 'Channel Created:&nbsp;&nbsp;About Us');
INSERT INTO `exp_cp_log` VALUES(84, 1, 1, '96black', '127.0.0.1', 1365632892, 'Field Group Created:&nbsp;About Us');
INSERT INTO `exp_cp_log` VALUES(85, 1, 1, '96black', '127.0.0.1', 1365641498, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(86, 1, 1, '96black', '127.0.0.1', 1365644175, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(87, 1, 1, '96black', '127.0.0.1', 1365653399, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(88, 1, 1, '96black', '127.0.0.1', 1365708737, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(89, 1, 1, '96black', '127.0.0.1', 1365725809, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(90, 1, 1, '96black', '127.0.0.1', 1365728123, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(91, 1, 1, '96black', '127.0.0.1', 1365729769, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(92, 1, 1, '96black', '127.0.0.1', 1365729854, 'Channel Created:&nbsp;&nbsp;Manufacturers');
INSERT INTO `exp_cp_log` VALUES(93, 1, 1, '96black', '127.0.0.1', 1365729858, 'Field Group Created:&nbsp;Manufacturers');
INSERT INTO `exp_cp_log` VALUES(94, 1, 1, '96black', '127.0.0.1', 1365737079, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(95, 1, 1, '96black', '127.0.0.1', 1365741053, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(96, 1, 1, '96black', '127.0.0.1', 1365741080, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(97, 1, 1, '96black', '127.0.0.1', 1365845178, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(98, 2, 1, '96black', '127.0.0.1', 1365848014, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(99, 2, 1, '96black', '127.0.0.1', 1365848029, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(100, 1, 1, '96black', '127.0.0.1', 1365848058, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(101, 2, 1, '96black', '127.0.0.1', 1365929769, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(102, 1, 1, '96black', '127.0.0.1', 1365964296, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(103, 1, 1, '96black', '127.0.0.1', 1365970813, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(104, 1, 1, '96black', '127.0.0.1', 1365973435, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(105, 1, 1, '96black', '127.0.0.1', 1365981775, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(106, 1, 1, '96black', '127.0.0.1', 1365985288, 'Custom Field Deleted:&nbsp;Page Content AU');
INSERT INTO `exp_cp_log` VALUES(107, 1, 1, '96black', '127.0.0.1', 1366065334, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(108, 1, 1, '96black', '127.0.0.1', 1366073562, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(109, 1, 1, '96black', '127.0.0.1', 1366088721, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(110, 1, 1, '96black', '127.0.0.1', 1366089583, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(111, 1, 1, '96black', '127.0.0.1', 1366090168, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(112, 1, 1, '96black', '127.0.0.1', 1366096141, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(113, 1, 1, '96black', '127.0.0.1', 1366096159, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(114, 1, 1, '96black', '127.0.0.1', 1366151611, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(115, 1, 1, '96black', '127.0.0.1', 1366248661, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(116, 1, 1, '96black', '127.0.0.1', 1366327022, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(117, 1, 1, '96black', '127.0.0.1', 1366330568, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(118, 1, 1, '96black', '127.0.0.1', 1366582123, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(119, 1, 1, '96black', '127.0.0.1', 1366592136, 'Logged out');

/* Table structure for table `exp_cp_search_index` */
DROP TABLE IF EXISTS `exp_cp_search_index`;

CREATE TABLE `exp_cp_search_index` (
  `search_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(20) DEFAULT NULL,
  `method` varchar(50) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `access` varchar(50) DEFAULT NULL,
  `keywords` text,
  PRIMARY KEY (`search_id`),
  FULLTEXT KEY `keywords` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


/* Table structure for table `exp_developer_log` */
DROP TABLE IF EXISTS `exp_developer_log`;

CREATE TABLE `exp_developer_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(10) unsigned NOT NULL,
  `viewed` char(1) NOT NULL DEFAULT 'n',
  `description` text,
  `function` varchar(100) DEFAULT NULL,
  `line` int(10) unsigned DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `deprecated_since` varchar(10) DEFAULT NULL,
  `use_instead` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_cache` */
DROP TABLE IF EXISTS `exp_email_cache`;

CREATE TABLE `exp_email_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `total_sent` int(6) unsigned NOT NULL,
  `from_name` varchar(70) NOT NULL,
  `from_email` varchar(70) NOT NULL,
  `recipient` text NOT NULL,
  `cc` text NOT NULL,
  `bcc` text NOT NULL,
  `recipient_array` mediumtext NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  `plaintext_alt` mediumtext NOT NULL,
  `mailinglist` char(1) NOT NULL DEFAULT 'n',
  `mailtype` varchar(6) NOT NULL,
  `text_fmt` varchar(40) NOT NULL,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `priority` char(1) NOT NULL DEFAULT '3',
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_cache_mg` */
DROP TABLE IF EXISTS `exp_email_cache_mg`;

CREATE TABLE `exp_email_cache_mg` (
  `cache_id` int(6) unsigned NOT NULL,
  `group_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_cache_ml` */
DROP TABLE IF EXISTS `exp_email_cache_ml`;

CREATE TABLE `exp_email_cache_ml` (
  `cache_id` int(6) unsigned NOT NULL,
  `list_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_console_cache` */
DROP TABLE IF EXISTS `exp_email_console_cache`;

CREATE TABLE `exp_email_console_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `member_id` int(10) unsigned NOT NULL,
  `member_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `recipient` varchar(70) NOT NULL,
  `recipient_name` varchar(50) NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_tracker` */
DROP TABLE IF EXISTS `exp_email_tracker`;

CREATE TABLE `exp_email_tracker` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_ip` varchar(16) NOT NULL,
  `sender_email` varchar(75) NOT NULL,
  `sender_username` varchar(50) NOT NULL,
  `number_recipients` int(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_entry_ping_status` */
DROP TABLE IF EXISTS `exp_entry_ping_status`;

CREATE TABLE `exp_entry_ping_status` (
  `entry_id` int(10) unsigned NOT NULL,
  `ping_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`ping_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_entry_versioning` */
DROP TABLE IF EXISTS `exp_entry_versioning`;

CREATE TABLE `exp_entry_versioning` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL,
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `version_date` int(10) NOT NULL,
  `version_data` mediumtext NOT NULL,
  PRIMARY KEY (`version_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_extensions` */
DROP TABLE IF EXISTS `exp_extensions`;

CREATE TABLE `exp_extensions` (
  `extension_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL DEFAULT '',
  `method` varchar(50) NOT NULL DEFAULT '',
  `hook` varchar(50) NOT NULL DEFAULT '',
  `settings` text NOT NULL,
  `priority` int(2) NOT NULL DEFAULT '10',
  `version` varchar(10) NOT NULL DEFAULT '',
  `enabled` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`extension_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_extensions` */
INSERT INTO `exp_extensions` VALUES(1, 'Safecracker_ext', 'form_declaration_modify_data', 'form_declaration_modify_data', '', 10, '2.1', 'y');
INSERT INTO `exp_extensions` VALUES(2, 'Rte_ext', 'myaccount_nav_setup', 'myaccount_nav_setup', '', 10, '1.0', 'y');
INSERT INTO `exp_extensions` VALUES(3, 'Rte_ext', 'cp_menu_array', 'cp_menu_array', '', 10, '1.0', 'y');
INSERT INTO `exp_extensions` VALUES(4, 'Rte_ext', 'publish_form_entry_data', 'publish_form_entry_data', '', 10, '1.0', 'y');
INSERT INTO `exp_extensions` VALUES(5, 'Matrix_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 10, '2.5.2', 'y');
INSERT INTO `exp_extensions` VALUES(6, 'Playa_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 9, '4.3.3', 'y');
INSERT INTO `exp_extensions` VALUES(7, 'Low_seg2cat_ext', 'sessions_end', 'sessions_end', 'a:3:{s:15:\"category_groups\";a:0:{}s:11:\"uri_pattern\";s:0:\"\";s:16:\"set_all_segments\";s:1:\"n\";}', 1, '2.6.3', 'y');
INSERT INTO `exp_extensions` VALUES(8, 'Mx_cloner_ext', 'publish_form_entry_data', 'publish_form_entry_data', 'a:1:{s:13:\"multilanguage\";s:1:\"n\";}', 10, '1.0.3', 'y');

/* Table structure for table `exp_field_formatting` */
DROP TABLE IF EXISTS `exp_field_formatting`;

CREATE TABLE `exp_field_formatting` (
  `formatting_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(10) unsigned NOT NULL,
  `field_fmt` varchar(40) NOT NULL,
  PRIMARY KEY (`formatting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=148 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_field_formatting` */
INSERT INTO `exp_field_formatting` VALUES(1, 1, 'none');
INSERT INTO `exp_field_formatting` VALUES(2, 1, 'br');
INSERT INTO `exp_field_formatting` VALUES(3, 1, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(4, 2, 'none');
INSERT INTO `exp_field_formatting` VALUES(5, 2, 'br');
INSERT INTO `exp_field_formatting` VALUES(6, 2, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(7, 3, 'none');
INSERT INTO `exp_field_formatting` VALUES(8, 3, 'br');
INSERT INTO `exp_field_formatting` VALUES(9, 3, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(10, 4, 'none');
INSERT INTO `exp_field_formatting` VALUES(11, 4, 'br');
INSERT INTO `exp_field_formatting` VALUES(12, 4, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(13, 5, 'none');
INSERT INTO `exp_field_formatting` VALUES(14, 5, 'br');
INSERT INTO `exp_field_formatting` VALUES(15, 5, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(16, 6, 'none');
INSERT INTO `exp_field_formatting` VALUES(17, 6, 'br');
INSERT INTO `exp_field_formatting` VALUES(18, 6, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(19, 7, 'none');
INSERT INTO `exp_field_formatting` VALUES(20, 7, 'br');
INSERT INTO `exp_field_formatting` VALUES(21, 7, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(22, 8, 'none');
INSERT INTO `exp_field_formatting` VALUES(23, 8, 'br');
INSERT INTO `exp_field_formatting` VALUES(24, 8, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(25, 9, 'none');
INSERT INTO `exp_field_formatting` VALUES(26, 9, 'br');
INSERT INTO `exp_field_formatting` VALUES(27, 9, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(28, 10, 'none');
INSERT INTO `exp_field_formatting` VALUES(29, 10, 'br');
INSERT INTO `exp_field_formatting` VALUES(30, 10, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(31, 11, 'none');
INSERT INTO `exp_field_formatting` VALUES(32, 11, 'br');
INSERT INTO `exp_field_formatting` VALUES(33, 11, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(34, 12, 'none');
INSERT INTO `exp_field_formatting` VALUES(35, 12, 'br');
INSERT INTO `exp_field_formatting` VALUES(36, 12, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(37, 13, 'none');
INSERT INTO `exp_field_formatting` VALUES(38, 13, 'br');
INSERT INTO `exp_field_formatting` VALUES(39, 13, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(40, 14, 'none');
INSERT INTO `exp_field_formatting` VALUES(41, 14, 'br');
INSERT INTO `exp_field_formatting` VALUES(42, 14, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(43, 15, 'none');
INSERT INTO `exp_field_formatting` VALUES(44, 15, 'br');
INSERT INTO `exp_field_formatting` VALUES(45, 15, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(46, 16, 'none');
INSERT INTO `exp_field_formatting` VALUES(47, 16, 'br');
INSERT INTO `exp_field_formatting` VALUES(48, 16, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(49, 17, 'none');
INSERT INTO `exp_field_formatting` VALUES(50, 17, 'br');
INSERT INTO `exp_field_formatting` VALUES(51, 17, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(52, 18, 'none');
INSERT INTO `exp_field_formatting` VALUES(53, 18, 'br');
INSERT INTO `exp_field_formatting` VALUES(54, 18, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(55, 19, 'none');
INSERT INTO `exp_field_formatting` VALUES(56, 19, 'br');
INSERT INTO `exp_field_formatting` VALUES(57, 19, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(58, 20, 'none');
INSERT INTO `exp_field_formatting` VALUES(59, 20, 'br');
INSERT INTO `exp_field_formatting` VALUES(60, 20, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(64, 22, 'none');
INSERT INTO `exp_field_formatting` VALUES(65, 22, 'br');
INSERT INTO `exp_field_formatting` VALUES(66, 22, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(67, 23, 'none');
INSERT INTO `exp_field_formatting` VALUES(68, 23, 'br');
INSERT INTO `exp_field_formatting` VALUES(69, 23, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(70, 24, 'none');
INSERT INTO `exp_field_formatting` VALUES(71, 24, 'br');
INSERT INTO `exp_field_formatting` VALUES(72, 24, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(73, 25, 'none');
INSERT INTO `exp_field_formatting` VALUES(74, 25, 'br');
INSERT INTO `exp_field_formatting` VALUES(75, 25, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(76, 26, 'none');
INSERT INTO `exp_field_formatting` VALUES(77, 26, 'br');
INSERT INTO `exp_field_formatting` VALUES(78, 26, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(79, 27, 'none');
INSERT INTO `exp_field_formatting` VALUES(80, 27, 'br');
INSERT INTO `exp_field_formatting` VALUES(81, 27, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(82, 28, 'none');
INSERT INTO `exp_field_formatting` VALUES(83, 28, 'br');
INSERT INTO `exp_field_formatting` VALUES(84, 28, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(85, 29, 'none');
INSERT INTO `exp_field_formatting` VALUES(86, 29, 'br');
INSERT INTO `exp_field_formatting` VALUES(87, 29, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(88, 30, 'none');
INSERT INTO `exp_field_formatting` VALUES(89, 30, 'br');
INSERT INTO `exp_field_formatting` VALUES(90, 30, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(91, 31, 'none');
INSERT INTO `exp_field_formatting` VALUES(92, 31, 'br');
INSERT INTO `exp_field_formatting` VALUES(93, 31, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(94, 32, 'none');
INSERT INTO `exp_field_formatting` VALUES(95, 32, 'br');
INSERT INTO `exp_field_formatting` VALUES(96, 32, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(97, 33, 'none');
INSERT INTO `exp_field_formatting` VALUES(98, 33, 'br');
INSERT INTO `exp_field_formatting` VALUES(99, 33, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(100, 34, 'none');
INSERT INTO `exp_field_formatting` VALUES(101, 34, 'br');
INSERT INTO `exp_field_formatting` VALUES(102, 34, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(103, 35, 'none');
INSERT INTO `exp_field_formatting` VALUES(104, 35, 'br');
INSERT INTO `exp_field_formatting` VALUES(105, 35, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(106, 36, 'none');
INSERT INTO `exp_field_formatting` VALUES(107, 36, 'br');
INSERT INTO `exp_field_formatting` VALUES(108, 36, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(109, 37, 'none');
INSERT INTO `exp_field_formatting` VALUES(110, 37, 'br');
INSERT INTO `exp_field_formatting` VALUES(111, 37, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(112, 38, 'none');
INSERT INTO `exp_field_formatting` VALUES(113, 38, 'br');
INSERT INTO `exp_field_formatting` VALUES(114, 38, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(115, 39, 'none');
INSERT INTO `exp_field_formatting` VALUES(116, 39, 'br');
INSERT INTO `exp_field_formatting` VALUES(117, 39, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(118, 40, 'none');
INSERT INTO `exp_field_formatting` VALUES(119, 40, 'br');
INSERT INTO `exp_field_formatting` VALUES(120, 40, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(124, 42, 'none');
INSERT INTO `exp_field_formatting` VALUES(125, 42, 'br');
INSERT INTO `exp_field_formatting` VALUES(126, 42, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(127, 43, 'none');
INSERT INTO `exp_field_formatting` VALUES(128, 43, 'br');
INSERT INTO `exp_field_formatting` VALUES(129, 43, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(130, 44, 'none');
INSERT INTO `exp_field_formatting` VALUES(131, 44, 'br');
INSERT INTO `exp_field_formatting` VALUES(132, 44, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(133, 45, 'none');
INSERT INTO `exp_field_formatting` VALUES(134, 45, 'br');
INSERT INTO `exp_field_formatting` VALUES(135, 45, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(136, 46, 'none');
INSERT INTO `exp_field_formatting` VALUES(137, 46, 'br');
INSERT INTO `exp_field_formatting` VALUES(138, 46, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(139, 47, 'none');
INSERT INTO `exp_field_formatting` VALUES(140, 47, 'br');
INSERT INTO `exp_field_formatting` VALUES(141, 47, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(142, 48, 'none');
INSERT INTO `exp_field_formatting` VALUES(143, 48, 'br');
INSERT INTO `exp_field_formatting` VALUES(144, 48, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(145, 49, 'none');
INSERT INTO `exp_field_formatting` VALUES(146, 49, 'br');
INSERT INTO `exp_field_formatting` VALUES(147, 49, 'xhtml');

/* Table structure for table `exp_field_groups` */
DROP TABLE IF EXISTS `exp_field_groups`;

CREATE TABLE `exp_field_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_field_groups` */
INSERT INTO `exp_field_groups` VALUES(1, 1, 'Products');
INSERT INTO `exp_field_groups` VALUES(2, 1, 'Featured Product Pages');
INSERT INTO `exp_field_groups` VALUES(3, 1, 'News');
INSERT INTO `exp_field_groups` VALUES(4, 1, 'Contact Us');
INSERT INTO `exp_field_groups` VALUES(5, 1, 'Case Studies');
INSERT INTO `exp_field_groups` VALUES(6, 1, 'Services');
INSERT INTO `exp_field_groups` VALUES(7, 1, 'Home Page Banners');
INSERT INTO `exp_field_groups` VALUES(8, 1, 'Management');
INSERT INTO `exp_field_groups` VALUES(9, 1, 'About Us');
INSERT INTO `exp_field_groups` VALUES(10, 1, 'Manufacturers');

/* Table structure for table `exp_fieldtypes` */
DROP TABLE IF EXISTS `exp_fieldtypes`;

CREATE TABLE `exp_fieldtypes` (
  `fieldtype_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `version` varchar(12) NOT NULL,
  `settings` text,
  `has_global_settings` char(1) DEFAULT 'n',
  PRIMARY KEY (`fieldtype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_fieldtypes` */
INSERT INTO `exp_fieldtypes` VALUES(1, 'select', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(2, 'text', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(3, 'textarea', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(4, 'date', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(5, 'file', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(6, 'multi_select', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(7, 'checkboxes', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(8, 'radio', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(9, 'rel', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(10, 'rte', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(11, 'matrix', '2.5.2', 'YTowOnt9', 'y');
INSERT INTO `exp_fieldtypes` VALUES(12, 'playa', '4.3.3', 'YTowOnt9', 'y');
INSERT INTO `exp_fieldtypes` VALUES(13, 'wygwam', '2.6.3', 'YTowOnt9', 'y');
INSERT INTO `exp_fieldtypes` VALUES(14, 'pt_checkboxes', '1.0.3', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(15, 'pt_dropdown', '1.0.3', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(16, 'pt_multiselect', '1.0.3', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(17, 'pt_radio_buttons', '1.0.3', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(18, 'nolan', '1.0.3.1', 'YTowOnt9', 'n');

/* Table structure for table `exp_file_categories` */
DROP TABLE IF EXISTS `exp_file_categories`;

CREATE TABLE `exp_file_categories` (
  `file_id` int(10) unsigned DEFAULT NULL,
  `cat_id` int(10) unsigned DEFAULT NULL,
  `sort` int(10) unsigned DEFAULT '0',
  `is_cover` char(1) DEFAULT 'n',
  KEY `file_id` (`file_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_file_dimensions` */
DROP TABLE IF EXISTS `exp_file_dimensions`;

CREATE TABLE `exp_file_dimensions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `upload_location_id` int(4) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT '',
  `short_name` varchar(255) DEFAULT '',
  `resize_type` varchar(50) DEFAULT '',
  `width` int(10) DEFAULT '0',
  `height` int(10) DEFAULT '0',
  `watermark_id` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `upload_location_id` (`upload_location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_file_dimensions` */
INSERT INTO `exp_file_dimensions` VALUES(1, 1, 4, 'article-list-thumb', 'article-list-thumb', 'crop', 396, 88, 0);
INSERT INTO `exp_file_dimensions` VALUES(2, 1, 5, 'case-study-list-thumb', 'case-study-list-thumb', 'crop', 396, 88, 0);

/* Table structure for table `exp_file_watermarks` */
DROP TABLE IF EXISTS `exp_file_watermarks`;

CREATE TABLE `exp_file_watermarks` (
  `wm_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `wm_name` varchar(80) DEFAULT NULL,
  `wm_type` varchar(10) DEFAULT 'text',
  `wm_image_path` varchar(100) DEFAULT NULL,
  `wm_test_image_path` varchar(100) DEFAULT NULL,
  `wm_use_font` char(1) DEFAULT 'y',
  `wm_font` varchar(30) DEFAULT NULL,
  `wm_font_size` int(3) unsigned DEFAULT NULL,
  `wm_text` varchar(100) DEFAULT NULL,
  `wm_vrt_alignment` varchar(10) DEFAULT 'top',
  `wm_hor_alignment` varchar(10) DEFAULT 'left',
  `wm_padding` int(3) unsigned DEFAULT NULL,
  `wm_opacity` int(3) unsigned DEFAULT NULL,
  `wm_hor_offset` int(4) unsigned DEFAULT NULL,
  `wm_vrt_offset` int(4) unsigned DEFAULT NULL,
  `wm_x_transp` int(4) DEFAULT NULL,
  `wm_y_transp` int(4) DEFAULT NULL,
  `wm_font_color` varchar(7) DEFAULT NULL,
  `wm_use_drop_shadow` char(1) DEFAULT 'y',
  `wm_shadow_distance` int(3) unsigned DEFAULT NULL,
  `wm_shadow_color` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`wm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_files` */
DROP TABLE IF EXISTS `exp_files`;

CREATE TABLE `exp_files` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `title` varchar(255) DEFAULT NULL,
  `upload_location_id` int(4) unsigned DEFAULT '0',
  `rel_path` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_size` int(10) DEFAULT '0',
  `description` text,
  `credit` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `uploaded_by_member_id` int(10) unsigned DEFAULT '0',
  `upload_date` int(10) DEFAULT NULL,
  `modified_by_member_id` int(10) unsigned DEFAULT '0',
  `modified_date` int(10) DEFAULT NULL,
  `file_hw_original` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`file_id`),
  KEY `upload_location_id` (`upload_location_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_files` */
INSERT INTO `exp_files` VALUES(1, 1, 'large-product-image.jpg', 1, '/Users/james/Projects/96black/Synergy/uploads/product-images/large-product-image.jpg', 'image/jpeg', 'large-product-image.jpg', 82647, null, null, null, 1, 1358293635, 1, 1358293635, '485 352');
INSERT INTO `exp_files` VALUES(3, 1, 'large-product-image2.jpg', 2, '/Users/james/Projects/96black/Synergy/uploads/product-images/large-product-image2.jpg', 'image/jpeg', 'large-product-image2.jpg', 82647, null, null, null, 1, 1358294245, 1, 1358294248, '485 352');
INSERT INTO `exp_files` VALUES(4, 1, 'large-product-image3.jpg', 2, '/Users/james/Projects/96black/Synergy/uploads/product-images/large-product-image3.jpg', 'image/jpeg', 'large-product-image3.jpg', 82647, null, null, null, 1, 1358294270, 1, 1358294270, '485 352');
INSERT INTO `exp_files` VALUES(5, 1, 'large-product-image4.jpg', 2, '/Users/james/Projects/96black/Synergy/uploads/product-downloads/large-product-image4.jpg', 'image/jpeg', 'large-product-image4.jpg', 82647, null, null, null, 1, 1358294478, 1, 1358294478, '485 352');
INSERT INTO `exp_files` VALUES(6, 1, 'large-product-image6.jpg', 1, '/Users/james/Projects/96black/Synergy/uploads/product-images/large-product-image6.jpg', 'image/jpeg', 'large-product-image6.jpg', 82647, null, null, null, 1, 1358363557, 1, 1358363557, '485 352');
INSERT INTO `exp_files` VALUES(7, 1, 'cat-image.jpg', 3, '/Users/jamesmcfall/Projects/Synergy/uploads/category-images/cat-image.jpg', 'image/jpeg', 'cat-image.jpg', 8376, null, null, null, 1, 1362950904, 1, 1362950904, '211 153');
INSERT INTO `exp_files` VALUES(8, 1, 'news-temp-top.jpg', 4, '/Users/jamesmcfall/Projects/Synergy/uploads/news-images/news-temp-top.jpg', 'image/jpeg', 'news-temp-top.jpg', 56244, null, null, null, 1, 1362961635, 1, 1362961635, '373 866');
INSERT INTO `exp_files` VALUES(9, 1, 'cat-feature-slide1.jpg', 1, '/Users/jamesmcfall/Projects/Synergy/uploads/product-images/cat-feature-slide1.jpg', 'image/jpeg', 'cat-feature-slide1.jpg', 19917, null, null, null, 1, 1363820695, 1, 1363820695, '411 866');
INSERT INTO `exp_files` VALUES(10, 1, 'cat-feature-slide3.jpg', 1, '/Users/jamesmcfall/Projects/Synergy/uploads/product-images/cat-feature-slide3.jpg', 'image/jpeg', 'cat-feature-slide3.jpg', 20183, null, null, null, 1, 1363820736, 1, 1363820736, '411 866');
INSERT INTO `exp_files` VALUES(11, 1, 'news-temp-top_(1).jpg', 5, '/Users/jamesmcfall/Projects/Synergy/uploads/case-study-image/news-temp-top_(1).jpg', 'image/jpeg', 'news-temp-top_(1).jpg', 56300, null, null, null, 1, 1363903862, 1, 1363903862, '373 866');
INSERT INTO `exp_files` VALUES(12, 1, 'news-temp-top_(2).jpg', 5, '/Users/jamesmcfall/Projects/Synergy/uploads/case-study-image/news-temp-top_(2).jpg', 'image/jpeg', 'news-temp-top_(2).jpg', 56300, null, null, null, 1, 1363904837, 1, 1363904841, '373 866');
INSERT INTO `exp_files` VALUES(13, 1, 'news-temp-top_(3).jpg', 5, '/Users/jamesmcfall/Projects/Synergy/uploads/case-study-image/news-temp-top_(3).jpg', 'image/jpeg', 'news-temp-top_(3).jpg', 56300, null, null, null, 1, 1363904916, 1, 1363904920, '373 866');
INSERT INTO `exp_files` VALUES(14, 1, 'services.jpg', 6, '/Users/jamesmcfall/Projects/Synergy/uploads/home-page-banners/services.jpg', 'image/jpeg', 'services.jpg', 88125, null, null, null, 1, 1365459810, 1, 1365459810, '714 1600');
INSERT INTO `exp_files` VALUES(15, 1, 'products.jpg', 6, '/Users/jamesmcfall/Projects/Synergy/uploads/home-page-banners/products.jpg', 'image/jpeg', 'products.jpg', 85228, null, null, null, 1, 1365460093, 1, 1365460093, '714 1600');
INSERT INTO `exp_files` VALUES(16, 1, 'about.jpg', 6, '/Users/jamesmcfall/Projects/Synergy/uploads/home-page-banners/about.jpg', 'image/jpeg', 'about.jpg', 111165, null, null, null, 1, 1365460222, 1, 1365460222, '714 1600');
INSERT INTO `exp_files` VALUES(17, 1, 'services-temp-top.jpg', 3, '/Users/jamesmcfall/Projects/Synergy/uploads/category-images/services-temp-top.jpg', 'image/jpeg', 'services-temp-top.jpg', 94945, null, null, null, 1, 1365480417, 1, 1365480417, '305 866');
INSERT INTO `exp_files` VALUES(18, 1, 'services-temp-top.jpg', 7, '/Users/jamesmcfall/Projects/Synergy/uploads/services-banners/services-temp-top.jpg', 'image/jpeg', 'services-temp-top.jpg', 94945, null, null, null, 1, 1365562517, 1, 1365562517, '305 866');
INSERT INTO `exp_files` VALUES(19, 1, 'about-temp.jpg', 8, '/Users/jamesmcfall/Projects/Synergy/uploads/about-banners/about-temp.jpg', 'image/jpeg', 'about-temp.jpg', 55081, null, null, null, 1, 1365633903, 1, 1365633903, '305 867');
INSERT INTO `exp_files` VALUES(20, 1, 'james.jpg', 9, '/Users/jamesmcfall/Projects/Synergy/uploads/staff-pictures/james.jpg', 'image/jpeg', 'james.jpg', 8591, null, null, null, 1, 1365636639, 1, 1365636639, '211 153');
INSERT INTO `exp_files` VALUES(21, 1, 'mega-menu-feature-product-image_(1).jpg', 1, '/Users/jamesmcfall/Projects/Synergy/uploads/product-images/mega-menu-feature-product-image_(1).jpg', 'image/jpeg', 'mega-menu-feature-product-image_(1).jpg', 10854, null, null, null, 1, 1365720765, 1, 1365720765, '89 302');
INSERT INTO `exp_files` VALUES(22, 1, 'footer-topcon.gif', 10, '/Users/jamesmcfall/Projects/Synergy/uploads/manufacturer-images/footer-topcon.gif', 'image/gif', 'footer-topcon.gif', 3881, null, null, null, 1, 1365731455, 1, 1365731455, '28 173');
INSERT INTO `exp_files` VALUES(23, 1, 'topcon-logo.gif', 10, '/Users/jamesmcfall/Projects/Synergy/uploads/manufacturer-images/topcon-logo.gif', 'image/gif', 'topcon-logo.gif', 3820, null, null, null, 1, 1365731467, 1, 1365731467, '24 140');
INSERT INTO `exp_files` VALUES(24, 1, 'mega-menu-feature-product-logo.jpg', 10, '/Users/jamesmcfall/Projects/Synergy/uploads/manufacturer-images/mega-menu-feature-product-logo.jpg', 'image/jpeg', 'mega-menu-feature-product-logo.jpg', 2918, null, null, null, 1, 1365731492, 1, 1365731492, '48 166');
INSERT INTO `exp_files` VALUES(25, 1, 'map1.jpg', 11, '/Users/jamesmcfall/Projects/Synergy/uploads/contact-maps/map1.jpg', 'image/jpeg', 'map1.jpg', 51845, null, null, null, 1, 1366081553, 1, 1366081553, '305 866');
INSERT INTO `exp_files` VALUES(26, 1, 'map2.jpg', 11, '/Users/jamesmcfall/Projects/Synergy/uploads/contact-maps/map2.jpg', 'image/jpeg', 'map2.jpg', 51610, null, null, null, 1, 1366081562, 1, 1366081562, '305 866');

/* Table structure for table `exp_freeform_composer_layouts` */
DROP TABLE IF EXISTS `exp_freeform_composer_layouts`;

CREATE TABLE `exp_freeform_composer_layouts` (
  `composer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `composer_data` text,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `preview` char(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`composer_id`),
  KEY `preview` (`preview`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_freeform_composer_templates` */
DROP TABLE IF EXISTS `exp_freeform_composer_templates`;

CREATE TABLE `exp_freeform_composer_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `template_name` varchar(150) NOT NULL DEFAULT 'default',
  `template_label` varchar(150) NOT NULL DEFAULT 'default',
  `template_description` text,
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_data` text,
  `param_data` text,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_freeform_fields` */
DROP TABLE IF EXISTS `exp_freeform_fields`;

CREATE TABLE `exp_freeform_fields` (
  `field_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `field_name` varchar(150) NOT NULL DEFAULT 'default',
  `field_label` varchar(150) NOT NULL DEFAULT 'default',
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `settings` text,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `required` char(1) NOT NULL DEFAULT 'n',
  `submissions_page` char(1) NOT NULL DEFAULT 'y',
  `moderation_page` char(1) NOT NULL DEFAULT 'y',
  `composer_use` char(1) NOT NULL DEFAULT 'y',
  `field_description` text,
  PRIMARY KEY (`field_id`),
  KEY `field_name` (`field_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_fields` */
INSERT INTO `exp_freeform_fields` VALUES(1, 1, 'first_name', 'First Name', 'text', '{\"field_length\":150,\"field_content_type\":\"any\"}', 1, 1365716048, 0, 'n', 'y', 'y', 'y', 'This field contains the user\'s first name.');
INSERT INTO `exp_freeform_fields` VALUES(2, 1, 'last_name', 'Last Name', 'text', '{\"field_length\":150,\"field_content_type\":\"any\"}', 1, 1365716048, 0, 'n', 'y', 'y', 'y', 'This field contains the user\'s last name.');
INSERT INTO `exp_freeform_fields` VALUES(3, 1, 'email', 'Email', 'text', '{\"field_length\":150,\"field_content_type\":\"email\"}', 1, 1365716048, 0, 'n', 'y', 'y', 'y', 'A basic email field for collecting stuff like an email address.');
INSERT INTO `exp_freeform_fields` VALUES(4, 1, 'user_message', 'Message', 'textarea', '{\"field_ta_rows\":6}', 1, 1365716048, 0, 'n', 'y', 'y', 'y', 'This field contains the user\'s message.');
INSERT INTO `exp_freeform_fields` VALUES(5, 1, 'company_name', 'Company Name', 'text', '{\"field_length\":\"255\",\"field_content_type\":\"any\",\"disallow_html_rendering\":\"y\"}', 1, 1365716329, 0, 'n', 'y', 'y', 'y', 'This field contains the company name.');
INSERT INTO `exp_freeform_fields` VALUES(6, 1, 'full_name', 'Full Name', 'text', '{\"field_length\":\"150\",\"field_content_type\":\"any\",\"disallow_html_rendering\":\"y\"}', 1, 1365716586, 0, 'n', 'y', 'y', 'y', 'This contains the full name.');
INSERT INTO `exp_freeform_fields` VALUES(7, 1, 'phone_number', 'Phone Number', 'text', '{\"field_length\":\"150\",\"field_content_type\":\"number\",\"disallow_html_rendering\":\"y\"}', 1, 1365716640, 0, 'n', 'y', 'y', 'y', '');

/* Table structure for table `exp_freeform_fieldtypes` */
DROP TABLE IF EXISTS `exp_freeform_fieldtypes`;

CREATE TABLE `exp_freeform_fieldtypes` (
  `fieldtype_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fieldtype_name` varchar(250) DEFAULT NULL,
  `settings` text,
  `default_field` char(1) NOT NULL DEFAULT 'n',
  `version` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`fieldtype_id`),
  KEY `fieldtype_name` (`fieldtype_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_fieldtypes` */
INSERT INTO `exp_freeform_fieldtypes` VALUES(1, 'file_upload', '[]', 'n', '4.0.11');
INSERT INTO `exp_freeform_fieldtypes` VALUES(2, 'mailinglist', '[]', 'n', '4.0.11');
INSERT INTO `exp_freeform_fieldtypes` VALUES(3, 'text', '[]', 'n', '4.0.11');
INSERT INTO `exp_freeform_fieldtypes` VALUES(4, 'textarea', '[]', 'n', '4.0.11');

/* Table structure for table `exp_freeform_file_uploads` */
DROP TABLE IF EXISTS `exp_freeform_file_uploads`;

CREATE TABLE `exp_freeform_file_uploads` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `field_id` int(10) unsigned NOT NULL DEFAULT '0',
  `server_path` varchar(750) DEFAULT NULL,
  `filename` varchar(250) DEFAULT NULL,
  `extension` varchar(20) DEFAULT NULL,
  `filesize` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`file_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `extension` (`extension`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_freeform_form_entries_1` */
DROP TABLE IF EXISTS `exp_freeform_form_entries_1`;

CREATE TABLE `exp_freeform_form_entries_1` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `complete` varchar(1) NOT NULL DEFAULT 'y',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(50) DEFAULT NULL,
  `form_field_3` text,
  `form_field_4` text,
  `form_field_6` text,
  `form_field_7` text,
  `form_field_5` text,
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_form_entries_1` */
INSERT INTO `exp_freeform_form_entries_1` VALUES(1, 1, 0, 'y', '127.0.0.1', 1365716048, 0, 'pending', 'support@solspace.com', 'Welcome to Freeform. We hope that you will enjoy Solspace software.', null, null, null);
INSERT INTO `exp_freeform_form_entries_1` VALUES(2, 1, 1, 'y', '127.0.0.1', 1365717771, 0, 'pending', '', '', '', '', '');
INSERT INTO `exp_freeform_form_entries_1` VALUES(3, 1, 1, 'y', '127.0.0.1', 1365718729, 0, 'pending', 'james@96black.co.nz', 'Test message submission.', 'James McFall', '093603493', '96black');

/* Table structure for table `exp_freeform_form_entries_2` */
DROP TABLE IF EXISTS `exp_freeform_form_entries_2`;

CREATE TABLE `exp_freeform_form_entries_2` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `complete` varchar(1) NOT NULL DEFAULT 'y',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(50) DEFAULT NULL,
  `form_field_5` text,
  `form_field_6` text,
  `form_field_7` text,
  `form_field_3` text,
  `form_field_4` text,
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_form_entries_2` */
INSERT INTO `exp_freeform_form_entries_2` VALUES(1, 1, 1, 'y', '127.0.0.1', 1365971016, 0, 'pending', '', '', '', '', 'fgsdfgdsf');
INSERT INTO `exp_freeform_form_entries_2` VALUES(2, 1, 1, 'y', '127.0.0.1', 1365971102, 0, 'pending', '', 'sdfsdfds', '', '', '');
INSERT INTO `exp_freeform_form_entries_2` VALUES(3, 1, 1, 'y', '127.0.0.1', 1365972061, 0, 'pending', 'Test', 'James', '12345', 'james@96black.co.nz', 'asfsaf');
INSERT INTO `exp_freeform_form_entries_2` VALUES(4, 1, 0, 'y', '127.0.0.1', 1365973613, 0, 'pending', 'asfadsf', 'asfdsa', '13213', 'sdf@sfsdfd.com', 'safasdfasdf');

/* Table structure for table `exp_freeform_form_entries_3` */
DROP TABLE IF EXISTS `exp_freeform_form_entries_3`;

CREATE TABLE `exp_freeform_form_entries_3` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `complete` varchar(1) NOT NULL DEFAULT 'y',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(50) DEFAULT NULL,
  `form_field_3` text,
  `form_field_4` text,
  `form_field_5` text,
  `form_field_6` text,
  `form_field_7` text,
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_form_entries_3` */
INSERT INTO `exp_freeform_form_entries_3` VALUES(1, 1, 1, 'y', '127.0.0.1', 1366083791, 0, 'pending', 'test@test.com', 'test', 'test', 'test', '123');

/* Table structure for table `exp_freeform_forms` */
DROP TABLE IF EXISTS `exp_freeform_forms`;

CREATE TABLE `exp_freeform_forms` (
  `form_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_name` varchar(150) NOT NULL DEFAULT 'default',
  `form_label` varchar(150) NOT NULL DEFAULT 'default',
  `default_status` varchar(150) NOT NULL DEFAULT 'default',
  `notify_user` char(1) NOT NULL DEFAULT 'n',
  `notify_admin` char(1) NOT NULL DEFAULT 'n',
  `user_email_field` varchar(150) NOT NULL DEFAULT '',
  `user_notification_id` int(10) unsigned NOT NULL DEFAULT '0',
  `admin_notification_id` int(10) unsigned NOT NULL DEFAULT '0',
  `admin_notification_email` text,
  `form_description` text,
  `field_ids` text,
  `field_order` text,
  `template_id` int(10) unsigned NOT NULL DEFAULT '0',
  `composer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `settings` text,
  PRIMARY KEY (`form_id`),
  KEY `form_name` (`form_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_forms` */
INSERT INTO `exp_freeform_forms` VALUES(1, 1, 'contact', 'Contact', 'pending', 'n', 'y', '', 0, 0, 'james@96black.co.nz', 'This is a basic contact form.', '3|4|5|6|7', '6|5|3|7|4', 0, 0, 1, 1365716048, 1365716871, null);
INSERT INTO `exp_freeform_forms` VALUES(2, 1, 'product_enquiry_form', 'Product Enquiry Form', 'pending', 'n', 'y', '', 0, 0, 'james@96black.co.nz', 'This form handles product enquiries.', '3|4|5|6|7', '6|5|7|3|4', 0, 0, 1, 1365716187, 1366084109, null);
INSERT INTO `exp_freeform_forms` VALUES(3, 1, 'product_rental_form', 'Product Rental Form', 'pending', 'n', 'n', '', 0, 0, 'james@96black.co.nz', 'This form handles product rental enquiries.', '3|4|5|6|7', '6|5|7|3|4', 0, 0, 1, 1366083126, 0, null);

/* Table structure for table `exp_freeform_multipage_hashes` */
DROP TABLE IF EXISTS `exp_freeform_multipage_hashes`;

CREATE TABLE `exp_freeform_multipage_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `hash` varchar(32) NOT NULL DEFAULT '',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit` char(1) NOT NULL DEFAULT 'n',
  `data` text,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`),
  KEY `ip_address` (`ip_address`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_freeform_notification_templates` */
DROP TABLE IF EXISTS `exp_freeform_notification_templates`;

CREATE TABLE `exp_freeform_notification_templates` (
  `notification_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `notification_name` varchar(150) NOT NULL DEFAULT 'default',
  `notification_label` varchar(150) NOT NULL DEFAULT 'default',
  `notification_description` text,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `allow_html` char(1) NOT NULL DEFAULT 'n',
  `from_name` varchar(150) NOT NULL DEFAULT '',
  `from_email` varchar(250) NOT NULL DEFAULT '',
  `reply_to_email` varchar(250) NOT NULL DEFAULT '',
  `email_subject` varchar(128) NOT NULL DEFAULT 'default',
  `include_attachments` char(1) NOT NULL DEFAULT 'n',
  `template_data` text,
  PRIMARY KEY (`notification_id`),
  KEY `notification_name` (`notification_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_freeform_params` */
DROP TABLE IF EXISTS `exp_freeform_params`;

CREATE TABLE `exp_freeform_params` (
  `params_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text,
  PRIMARY KEY (`params_id`)
) ENGINE=InnoDB AUTO_INCREMENT=858 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_params` */
INSERT INTO `exp_freeform_params` VALUES(831, 1366585307, '{\"form_id\":\"2\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/products\\/optical\\/total-station\",\"inline_error_return\":\"products\\/optical\\/total-station\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(832, 1366585307, '{\"form_id\":\"3\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/products\\/optical\\/total-station\",\"inline_error_return\":\"products\\/optical\\/total-station\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":false,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(833, 1366585309, '{\"form_id\":\"2\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/products\\/optical\\/total-station\\/construction\\/topcon-gts-100n-construction\",\"inline_error_return\":\"products\\/optical\\/total-station\\/construction\\/topcon-gts-100n-construction\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(834, 1366585309, '{\"form_id\":\"3\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/products\\/optical\\/total-station\\/construction\\/topcon-gts-100n-construction\",\"inline_error_return\":\"products\\/optical\\/total-station\\/construction\\/topcon-gts-100n-construction\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":false,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(835, 1366586285, '{\"form_id\":\"2\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/products\\/optical\\/total-station\\/construction\\/topcon-gts-100n-construction\",\"inline_error_return\":\"products\\/optical\\/total-station\\/construction\\/topcon-gts-100n-construction\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(836, 1366586285, '{\"form_id\":\"3\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/products\\/optical\\/total-station\\/construction\\/topcon-gts-100n-construction\",\"inline_error_return\":\"products\\/optical\\/total-station\\/construction\\/topcon-gts-100n-construction\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":false,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(837, 1366586306, '{\"form_id\":\"2\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/products\\/optical\\/total-station\\/construction\\/topcon-gts-100n-construction\",\"inline_error_return\":\"products\\/optical\\/total-station\\/construction\\/topcon-gts-100n-construction\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(838, 1366586306, '{\"form_id\":\"3\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/products\\/optical\\/total-station\\/construction\\/topcon-gts-100n-construction\",\"inline_error_return\":\"products\\/optical\\/total-station\\/construction\\/topcon-gts-100n-construction\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":false,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(839, 1366588221, '{\"form_id\":\"2\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/products\",\"inline_error_return\":\"products\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(840, 1366588221, '{\"form_id\":\"3\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/products\",\"inline_error_return\":\"products\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":false,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(841, 1366592063, '{\"form_id\":\"2\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/products\",\"inline_error_return\":\"products\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(842, 1366592063, '{\"form_id\":\"3\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/products\",\"inline_error_return\":\"products\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":false,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(843, 1366592134, '{\"form_id\":\"2\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/products\",\"inline_error_return\":\"products\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(844, 1366592134, '{\"form_id\":\"3\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/products\",\"inline_error_return\":\"products\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":false,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(845, 1366592135, '{\"form_id\":\"2\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/products\",\"inline_error_return\":\"products\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(846, 1366592135, '{\"form_id\":\"3\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/products\",\"inline_error_return\":\"products\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":false,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(847, 1366592151, '{\"form_id\":\"2\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/products\",\"inline_error_return\":\"products\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(848, 1366592151, '{\"form_id\":\"3\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/products\",\"inline_error_return\":\"products\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":false,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(849, 1366592154, '{\"form_id\":\"2\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/products\\/topcon-gts-100n-robotic\",\"inline_error_return\":\"products\\/topcon-gts-100n-robotic\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(850, 1366592154, '{\"form_id\":\"3\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/products\\/topcon-gts-100n-robotic\",\"inline_error_return\":\"products\\/topcon-gts-100n-robotic\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":false,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(851, 1366592166, '{\"form_id\":\"2\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/search\\/results\\/aa85b41a2610678266276ea6b01bc80a\\/\",\"inline_error_return\":\"search\\/results\\/aa85b41a2610678266276ea6b01bc80a\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(852, 1366592189, '{\"form_id\":\"2\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/search\\/results\\/aa85b41a2610678266276ea6b01bc80a\\/\",\"inline_error_return\":\"search\\/results\\/aa85b41a2610678266276ea6b01bc80a\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(853, 1366592212, '{\"form_id\":\"2\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/search\\/results\\/aa85b41a2610678266276ea6b01bc80a\\/\",\"inline_error_return\":\"search\\/results\\/aa85b41a2610678266276ea6b01bc80a\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(854, 1366592347, '{\"form_id\":\"2\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/search\\/results\\/aa85b41a2610678266276ea6b01bc80a\\/\",\"inline_error_return\":\"search\\/results\\/aa85b41a2610678266276ea6b01bc80a\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(855, 1366592354, '{\"form_id\":\"2\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/search\\/results\\/aa85b41a2610678266276ea6b01bc80a\\/\",\"inline_error_return\":\"search\\/results\\/aa85b41a2610678266276ea6b01bc80a\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(856, 1366592396, '{\"form_id\":\"2\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/search\\/results\\/aa85b41a2610678266276ea6b01bc80a\\/\",\"inline_error_return\":\"search\\/results\\/aa85b41a2610678266276ea6b01bc80a\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(857, 1366592410, '{\"form_id\":\"2\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/search\\/results\\/aa85b41a2610678266276ea6b01bc80a\\/\",\"inline_error_return\":\"search\\/results\\/aa85b41a2610678266276ea6b01bc80a\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');

/* Table structure for table `exp_freeform_preferences` */
DROP TABLE IF EXISTS `exp_freeform_preferences`;

CREATE TABLE `exp_freeform_preferences` (
  `preference_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `preference_name` varchar(80) DEFAULT NULL,
  `preference_value` text,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`preference_id`),
  KEY `preference_name` (`preference_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_preferences` */
INSERT INTO `exp_freeform_preferences` VALUES(1, 'ffp', 'n', 0);
INSERT INTO `exp_freeform_preferences` VALUES(2, 'field_layout_prefs', '{\"entry_layout_prefs\":{\"member\":{\"1\":{\"visible\":[\"6\",\"5\",\"7\",\"3\",\"4\",\"author\",\"ip_address\",\"entry_date\",\"edit_date\",\"status\"],\"hidden\":[\"complete\",\"entry_id\",\"site_id\"]}}}}', 1);

/* Table structure for table `exp_freeform_user_email` */
DROP TABLE IF EXISTS `exp_freeform_user_email`;

CREATE TABLE `exp_freeform_user_email` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `email_count` int(10) unsigned NOT NULL DEFAULT '0',
  `email_addresses` text,
  PRIMARY KEY (`email_id`),
  KEY `ip_address` (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_global_variables` */
DROP TABLE IF EXISTS `exp_global_variables`;

CREATE TABLE `exp_global_variables` (
  `variable_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `variable_name` varchar(50) NOT NULL,
  `variable_data` text NOT NULL,
  PRIMARY KEY (`variable_id`),
  KEY `variable_name` (`variable_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_global_variables` */
INSERT INTO `exp_global_variables` VALUES(1, 1, 'nz_gst_message', '* Please note all prices exclude GST');
INSERT INTO `exp_global_variables` VALUES(2, 1, 'au_gst_message', '* Please note all prices exclude GST');

/* Table structure for table `exp_html_buttons` */
DROP TABLE IF EXISTS `exp_html_buttons`;

CREATE TABLE `exp_html_buttons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `tag_name` varchar(32) NOT NULL,
  `tag_open` varchar(120) NOT NULL,
  `tag_close` varchar(120) NOT NULL,
  `accesskey` varchar(32) NOT NULL,
  `tag_order` int(3) unsigned NOT NULL,
  `tag_row` char(1) NOT NULL DEFAULT '1',
  `classname` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_html_buttons` */
INSERT INTO `exp_html_buttons` VALUES(1, 1, 0, 'b', '<strong>', '</strong>', 'b', 1, '1', 'btn_b');
INSERT INTO `exp_html_buttons` VALUES(2, 1, 0, 'i', '<em>', '</em>', 'i', 2, '1', 'btn_i');
INSERT INTO `exp_html_buttons` VALUES(3, 1, 0, 'blockquote', '<blockquote>', '</blockquote>', 'q', 3, '1', 'btn_blockquote');
INSERT INTO `exp_html_buttons` VALUES(4, 1, 0, 'a', '<a href=\"[![Link:!:http://]!]\"(!( title=\"[![Title]!]\")!)>', '</a>', 'a', 4, '1', 'btn_a');
INSERT INTO `exp_html_buttons` VALUES(5, 1, 0, 'img', '<img src=\"[![Link:!:http://]!]\" alt=\"[![Alternative text]!]\" />', '', '', 5, '1', 'btn_img');
INSERT INTO `exp_html_buttons` VALUES(6, 2, 0, 'b', '<strong>', '</strong>', 'b', 1, '1', 'btn_b');
INSERT INTO `exp_html_buttons` VALUES(7, 2, 0, 'i', '<em>', '</em>', 'i', 2, '1', 'btn_i');
INSERT INTO `exp_html_buttons` VALUES(8, 2, 0, 'blockquote', '<blockquote>', '</blockquote>', 'q', 3, '1', 'btn_blockquote');
INSERT INTO `exp_html_buttons` VALUES(9, 2, 0, 'a', '<a href=\"[![Link:!:http://]!]\"(!( title=\"[![Title]!]\")!)>', '</a>', 'a', 4, '1', 'btn_a');
INSERT INTO `exp_html_buttons` VALUES(10, 2, 0, 'img', '<img src=\"[![Link:!:http://]!]\" alt=\"[![Alternative text]!]\" />', '', '', 5, '1', 'btn_img');

/* Table structure for table `exp_layout_publish` */
DROP TABLE IF EXISTS `exp_layout_publish`;

CREATE TABLE `exp_layout_publish` (
  `layout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_group` int(4) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(4) unsigned NOT NULL DEFAULT '0',
  `field_layout` text,
  PRIMARY KEY (`layout_id`),
  KEY `site_id` (`site_id`),
  KEY `member_group` (`member_group`),
  KEY `channel_id` (`channel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_layout_publish` */
INSERT INTO `exp_layout_publish` VALUES(10, 1, 1, 2, 'a:4:{s:7:\"publish\";a:6:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:16;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:18;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:17;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:10:\"Categories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}');
INSERT INTO `exp_layout_publish` VALUES(12, 1, 1, 1, 'a:9:{s:7:\"publish\";a:7:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:37;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:1;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:2;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:10;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:15:\"technical_specs\";a:2:{s:10:\"_tab_label\";s:15:\"Technical Specs\";i:3;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"downloads\";a:2:{s:10:\"_tab_label\";s:9:\"Downloads\";i:11;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:6:\"videos\";a:2:{s:10:\"_tab_label\";s:6:\"Videos\";i:4;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:14:\"rental_details\";a:4:{s:10:\"_tab_label\";s:14:\"Rental Details\";i:5;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:6;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:7;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:16:\"featured_details\";a:4:{s:10:\"_tab_label\";s:16:\"Featured Details\";i:9;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:33;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:8;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:10:\"Categories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}');
INSERT INTO `exp_layout_publish` VALUES(20, 1, 1, 10, 'a:4:{s:7:\"publish\";a:7:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:31;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:19:\"new_zealand_content\";a:3:{s:10:\"_tab_label\";s:19:\"New Zealand Content\";i:29;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:30;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:18:\"australian_content\";a:3:{s:10:\"_tab_label\";s:18:\"Australian Content\";i:43;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:42;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:7:{s:10:\"_tab_label\";s:7:\"Options\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}');
INSERT INTO `exp_layout_publish` VALUES(21, 1, 1, 4, 'a:6:{s:7:\"publish\";a:4:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:38;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:19:\"new_zealand_content\";a:4:{s:10:\"_tab_label\";s:19:\"New Zealand Content\";i:14;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:15;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:48;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:18:\"australian_content\";a:4:{s:10:\"_tab_label\";s:18:\"Australian Content\";i:40;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:39;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:49;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:10:\"Categories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}');

/* Table structure for table `exp_matrix_cols` */
DROP TABLE IF EXISTS `exp_matrix_cols`;

CREATE TABLE `exp_matrix_cols` (
  `col_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `col_name` varchar(32) DEFAULT NULL,
  `col_label` varchar(50) DEFAULT NULL,
  `col_instructions` text,
  `col_type` varchar(50) DEFAULT 'text',
  `col_required` char(1) DEFAULT 'n',
  `col_search` char(1) DEFAULT 'n',
  `col_order` int(3) unsigned DEFAULT NULL,
  `col_width` varchar(4) DEFAULT NULL,
  `col_settings` text,
  PRIMARY KEY (`col_id`),
  KEY `site_id` (`site_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_matrix_cols` */
INSERT INTO `exp_matrix_cols` VALUES(1, 1, 1, null, 'currency', 'Currency', '', 'pt_dropdown', 'n', 'y', 0, '33%', 'YToxOntzOjc6Im9wdGlvbnMiO2E6Mjp7czozOiJOWkQiO3M6MzoiTlpEIjtzOjM6IkFVRCI7czozOiJBVUQiO319');
INSERT INTO `exp_matrix_cols` VALUES(2, 1, 1, null, 'amount', 'Amount', 'Please enter the dollar value (without &#36; or any decimals as they will be output automatically)', 'number', 'n', 'n', 1, '', 'YTozOntzOjk6Im1pbl92YWx1ZSI7czowOiIiO3M6OToibWF4X3ZhbHVlIjtzOjA6IiI7czo4OiJkZWNpbWFscyI7czoxOiIwIjt9');
INSERT INTO `exp_matrix_cols` VALUES(3, 1, 3, null, 'section_title', 'Section Title', 'i.e. Dimensions', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(4, 1, 3, null, 'specification_details', 'Specification Details', 'This is where you enter i.e. Length, 200mm', 'nolan', 'n', 'n', 1, '', 'YToyOntzOjE2OiJub2xhbl9jb2xfbGFiZWxzIjtzOjQwOiJTcGVjaWZpY2F0aW9uIE5hbWUgfCBTcGVjaWZpY2F0aW9uIFZhbHVlIjtzOjE1OiJub2xhbl9jb2xfbmFtZXMiO3M6NDA6InNwZWNpZmljYXRpb25fbmFtZSB8IHNwZWNpZmljYXRpb25fdmFsdWUiO30=');
INSERT INTO `exp_matrix_cols` VALUES(6, 1, 4, null, 'video_heading', 'Video Heading', 'The heading to appear above the video', 'text', 'n', 'n', 0, '25%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(7, 1, 4, null, 'video_embed_url', 'Video Embed URL', 'The embed url from Youtube', 'text', 'n', 'n', 1, '25%', 'YTo0OntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(8, 1, 4, null, 'video_description', 'Video Description', 'This description appears beneath the video', 'wygwam', 'n', 'n', 2, '50%', 'YTozOntzOjc6ImNvbnZlcnQiO3M6MDoiIjtzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(9, 1, 5, null, 'rental_heading', 'Rental Heading', '', 'text', 'n', 'n', 0, '25%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(10, 1, 5, null, 'rental_content', 'Rental Content', '', 'wygwam', 'n', 'n', 1, '75%', 'YTozOntzOjc6ImNvbnZlcnQiO3M6MDoiIjtzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(11, 1, 6, null, 'rental_duration_nz', 'Rental Duration (NZ)', 'i.e. 1-3 days', 'text', 'n', 'n', 0, '20%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(12, 1, 6, null, 'rental_pricing_nz', 'Rental Pricing (NZ)', 'Please exclude the &#36; sign and decimal values.', 'number', 'n', 'n', 1, '', 'YTozOntzOjk6Im1pbl92YWx1ZSI7czowOiIiO3M6OToibWF4X3ZhbHVlIjtzOjA6IiI7czo4OiJkZWNpbWFscyI7czoxOiIwIjt9');
INSERT INTO `exp_matrix_cols` VALUES(13, 1, 7, null, 'rental_duration_au', 'Rental Duration (AU)', 'i.e. 1-3 days', 'text', 'n', 'n', 0, '20%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(14, 1, 7, null, 'rental_pricing_au', 'Rental Pricing (AU)', 'Please exclude the &#36; sign and decimal values.', 'number', 'n', 'n', 1, '', 'YTozOntzOjk6Im1pbl92YWx1ZSI7czowOiIiO3M6OToibWF4X3ZhbHVlIjtzOjA6IiI7czo4OiJkZWNpbWFscyI7czoxOiIwIjt9');
INSERT INTO `exp_matrix_cols` VALUES(15, 1, 8, null, 'featured_banner_image', 'Featured Banner Image', '', 'file', 'n', 'n', 0, '33%', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiIxIjtzOjEyOiJjb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO30=');
INSERT INTO `exp_matrix_cols` VALUES(16, 1, 10, null, 'product_image', 'Product Image', '', 'file', 'n', 'n', 0, '33%', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiIxIjtzOjEyOiJjb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO30=');
INSERT INTO `exp_matrix_cols` VALUES(17, 1, 11, null, 'file_title', 'File Title', '', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(18, 1, 11, null, 'download_file', 'Files', '', 'file', 'n', 'n', 1, '', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiIyIjtzOjEyOiJjb250ZW50X3R5cGUiO3M6MzoiYWxsIjt9');
INSERT INTO `exp_matrix_cols` VALUES(19, 1, 15, null, 'branch_region', 'Branch Region', 'i.e. Auckland/Christchurch', 'text', 'y', 'n', 0, '20%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(20, 1, 15, null, 'contact_information', 'Contact Information', '', 'nolan', 'y', 'n', 1, '', 'YToyOntzOjE2OiJub2xhbl9jb2xfbGFiZWxzIjtzOjI0OiJDb250YWN0IE1ldGhvZCB8IERldGFpbHMiO3M6MTU6Im5vbGFuX2NvbF9uYW1lcyI7czozMjoiY29udGFjdF9tZXRob2QgfCBjb250YWN0X2RldGFpbHMiO30=');
INSERT INTO `exp_matrix_cols` VALUES(21, 1, 15, null, 'operating_hours', 'Operating Hours', 'i.e. 8:00am to 5:00pm', 'text', 'y', 'n', 2, '', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(22, 1, 15, null, 'physical_address', 'Physical Address', '', 'text', 'y', 'n', 3, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czoyOiJiciI7czozOiJkaXIiO3M6MzoibHRyIjt9');
INSERT INTO `exp_matrix_cols` VALUES(23, 1, 15, null, 'postal_address', 'Postal Address', '', 'text', 'y', 'n', 4, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czoyOiJiciI7czozOiJkaXIiO3M6MzoibHRyIjt9');
INSERT INTO `exp_matrix_cols` VALUES(24, 1, 8, null, 'featured_banner_description', 'Featured Banner Description', '', 'text', 'n', 'n', 1, '', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(25, 1, 17, null, 'content', 'Content', '', 'wygwam', 'y', 'n', 0, '', 'YToyOntzOjY6ImNvbmZpZyI7czoxOiI0IjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(26, 1, 22, null, 'banner_large_text', 'Banner Large Text', '', 'text', 'y', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(27, 1, 22, null, 'banner_small_text', 'Banner Small Text', '', 'text', 'n', 'n', 1, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(28, 1, 23, null, 'link_text', 'Link Text', '', 'text', 'y', 'n', 0, '50%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(29, 1, 23, null, 'link_location', 'Link Location', 'The page you want the link to point to. It must be prefixed with \"/\". For example \"/services\"', 'text', 'y', 'n', 1, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(30, 1, 26, null, 'content', 'Content', '', 'wygwam', 'n', 'n', 0, '', 'YTozOntzOjc6ImNvbnZlcnQiO3M6MDoiIjtzOjY6ImNvbmZpZyI7czoxOiI0IjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(31, 1, 30, null, 'grid_content', 'Grid Content', '', 'wygwam', 'n', 'n', 1, '', 'YToyOntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(32, 1, 30, null, 'grid_title', 'Grid Title', '', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(33, 1, 32, null, 'name', 'Name', '', 'text', 'y', 'n', 0, '', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(34, 1, 32, null, 'position', 'Position', '', 'text', 'y', 'n', 1, '', 'YTozOntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(35, 1, 32, null, 'image', 'Image', 'Image dimensions: 153px x 211px. If not supplied a default image is used.', 'file', 'n', 'n', 2, '10%', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiI5IjtzOjEyOiJjb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO30=');
INSERT INTO `exp_matrix_cols` VALUES(36, 1, 38, null, 'contact_number', 'Contact Number', '', 'text', 'y', 'n', 1, '40%', 'YTozOntzOjQ6Im1heGwiO3M6MzoiMTAwIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(37, 1, 38, null, 'email_address', 'Email Address', '', 'text', 'y', 'n', 2, '40%', 'YTozOntzOjQ6Im1heGwiO3M6MzoiMTAwIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(38, 1, 38, null, 'country', 'Country', '\"AU\" or \"NZ\"', 'pt_dropdown', 'y', 'y', 0, '20%', 'YToxOntzOjc6Im9wdGlvbnMiO2E6Mjp7czoyOiJBVSI7czoyOiJBVSI7czoyOiJOWiI7czoyOiJOWiI7fX0=');
INSERT INTO `exp_matrix_cols` VALUES(39, 1, 39, null, 'branch_region', 'Branch Region', 'i.e. Auckland/Christchurch', 'text', 'y', 'n', 0, '20%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(40, 1, 39, null, 'contact_information', 'Contact Information', '', 'nolan', 'y', 'n', 1, '', 'YToyOntzOjE2OiJub2xhbl9jb2xfbGFiZWxzIjtzOjI0OiJDb250YWN0IE1ldGhvZCB8IERldGFpbHMiO3M6MTU6Im5vbGFuX2NvbF9uYW1lcyI7czozMjoiY29udGFjdF9tZXRob2QgfCBjb250YWN0X2RldGFpbHMiO30=');
INSERT INTO `exp_matrix_cols` VALUES(41, 1, 39, null, 'operating_hours', 'Operating Hours', 'i.e. 8:00am to 5:00pm', 'text', 'y', 'n', 2, '', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(42, 1, 39, null, 'physical_address', 'Physical Address', '', 'text', 'y', 'n', 3, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czoyOiJiciI7czozOiJkaXIiO3M6MzoibHRyIjt9');
INSERT INTO `exp_matrix_cols` VALUES(43, 1, 39, null, 'postal_address', 'Postal Address', '', 'text', 'y', 'n', 4, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czoyOiJiciI7czozOiJkaXIiO3M6MzoibHRyIjt9');
INSERT INTO `exp_matrix_cols` VALUES(44, 1, 42, null, 'grid_title', 'Grid Title', '', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(45, 1, 42, null, 'grid_content', 'Grid Content', '', 'wygwam', 'n', 'n', 1, '', 'YToyOntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(46, 1, 32, null, 'country', 'Staff Country', '', 'pt_dropdown', 'y', 'y', 3, '', 'YToxOntzOjc6Im9wdGlvbnMiO2E6Mjp7czoyOiJOWiI7czoyOiJOWiI7czoyOiJBVSI7czoyOiJBVSI7fX0=');
INSERT INTO `exp_matrix_cols` VALUES(47, 1, 15, null, 'map_image', 'Map Image', 'An image with the dimensions 866px x 305 px', 'file', 'y', 'n', 5, '', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoyOiIxMSI7czoxMjoiY29udGVudF90eXBlIjtzOjU6ImltYWdlIjt9');
INSERT INTO `exp_matrix_cols` VALUES(48, 1, 39, null, 'map_image', 'Map Image', 'An image with the dimensions 866px x 305 px', 'file', 'y', 'n', 5, '', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoyOiIxMSI7czoxMjoiY29udGVudF90eXBlIjtzOjU6ImltYWdlIjt9');
INSERT INTO `exp_matrix_cols` VALUES(49, 1, 32, null, 'profile_contents', 'Profile Contents', '', 'wygwam', 'y', 'n', 4, '', 'YToyOntzOjY6ImNvbmZpZyI7czoxOiI1IjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');

/* Table structure for table `exp_matrix_data` */
DROP TABLE IF EXISTS `exp_matrix_data`;

CREATE TABLE `exp_matrix_data` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `is_draft` tinyint(1) unsigned DEFAULT '0',
  `row_order` int(4) unsigned DEFAULT NULL,
  `col_id_1` text,
  `col_id_2` int(11) DEFAULT '0',
  `col_id_3` text,
  `col_id_4` text,
  `col_id_6` text,
  `col_id_7` text,
  `col_id_8` text,
  `col_id_9` text,
  `col_id_10` text,
  `col_id_11` text,
  `col_id_12` int(11) DEFAULT '0',
  `col_id_13` text,
  `col_id_14` int(11) DEFAULT '0',
  `col_id_15` text,
  `col_id_16` text,
  `col_id_17` text,
  `col_id_18` text,
  `col_id_19` text,
  `col_id_20` text,
  `col_id_21` text,
  `col_id_22` text,
  `col_id_23` text,
  `col_id_24` text,
  `col_id_25` text,
  `col_id_26` text,
  `col_id_27` text,
  `col_id_28` text,
  `col_id_29` text,
  `col_id_30` text,
  `col_id_31` text,
  `col_id_32` text,
  `col_id_33` text,
  `col_id_34` text,
  `col_id_35` text,
  `col_id_36` text,
  `col_id_37` text,
  `col_id_38` text,
  `col_id_39` text,
  `col_id_40` text,
  `col_id_41` text,
  `col_id_42` text,
  `col_id_43` text,
  `col_id_44` text,
  `col_id_45` text,
  `col_id_46` text,
  `col_id_47` text,
  `col_id_48` text,
  `col_id_49` text,
  PRIMARY KEY (`row_id`),
  KEY `site_id` (`site_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_matrix_data` */
INSERT INTO `exp_matrix_data` VALUES(1, 1, 1, 1, null, 0, 1, 'NZD', 45000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(2, 1, 1, 1, null, 0, 2, 'AUD', 40000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(3, 1, 1, 3, null, 0, 1, null, 0, 'Telescope', 'a:7:{i:0;a:2:{s:18:\"specification_name\";s:6:\"Length\";s:19:\"specification_value\";s:15:\"150 Millimeters\";}i:1;a:2:{s:18:\"specification_name\";s:23:\"Objective Lens Diameter\";s:19:\"specification_value\";s:15:\"45mm (EDM:50mm)\";}i:2;a:2:{s:18:\"specification_name\";s:13:\"Magnification\";s:19:\"specification_value\";s:3:\"30x\";}i:3;a:2:{s:18:\"specification_name\";s:5:\"Image\";s:19:\"specification_value\";s:5:\"Erect\";}i:4;a:2:{s:18:\"specification_name\";s:13:\"Field of View\";s:19:\"specification_value\";s:3:\"130\";}i:5;a:2:{s:18:\"specification_name\";s:13:\"Resolve Power\";s:19:\"specification_value\";s:2:\"3\"\";}i:6;a:2:{s:18:\"specification_name\";s:18:\"Min Focus Distance\";s:19:\"specification_value\";s:10:\"1.3 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(4, 1, 1, 3, null, 0, 2, null, 0, 'Measurement Range', 'a:2:{i:0;a:2:{s:18:\"specification_name\";s:8:\"1 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}i:1;a:2:{s:18:\"specification_name\";s:8:\"3 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(5, 1, 1, 10, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(6, 1, 1, 11, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, 'Test uploaded file', '{filedir_2}large-product-image4.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(7, 1, 1, 4, null, 0, 1, null, 0, null, null, 'Test Video Of My 4x4', 'http://www.youtube.com/embed/HNIA-nesZEU', '<p>\n	Test vid of my 4x4.&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae gravida tortor. Maecenas turpis purus, imperdiet id elementum eget, feugiat in tellus. Duis molestie, nulla nec sollicitudin consectetur, magna nisl condimentum neque, at consequat magna enim a tellus. Maecenas porttitor mattis aliquet. Ut lobortis, tortor eget lacinia commodo, mi sapien molestie neque, quis mattis metus lorem posuere massa. Vivamus id pretium lacus. Aliquam ac mauris orci, vulputate ullamcorper elit. Donec eget eros eros. Etiam ac sapien a sem ullamcorper consequat. Suspendisse ut elementum arcu. Cras fringilla tortor pellentesque nibh pharetra pellentesque. Sed et mauris sit amet ligula scelerisque sodales sed nec lorem. In vel quam sit amet eros lobortis consectetur sit amet egestas nisl. Cras erat turpis, suscipit posuere mollis nec, dictum eget lectus.</p>', null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(8, 1, 1, 5, null, 0, 1, null, 0, null, null, null, null, null, 'Test heading', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(9, 1, 1, 5, null, 0, 2, null, 0, null, null, null, null, null, 'Test heading 2', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(10, 1, 1, 6, null, 0, 1, null, 0, null, null, null, null, null, null, null, '1-3 days', 100, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(11, 1, 1, 6, null, 0, 2, null, 0, null, null, null, null, null, null, null, '4-6 days', 180, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(12, 1, 1, 7, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, '1-3 days', 80, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(13, 1, 1, 7, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, '4-6 days', 160, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(14, 1, 1, 10, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image6.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(15, 1, 2, 1, null, 0, 1, 'NZD', 45000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(16, 1, 2, 1, null, 0, 2, 'AUD', 40000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(17, 1, 2, 3, null, 0, 1, null, 0, 'Telescope', 'a:7:{i:0;a:2:{s:18:\"specification_name\";s:6:\"Length\";s:19:\"specification_value\";s:15:\"150 Millimeters\";}i:1;a:2:{s:18:\"specification_name\";s:23:\"Objective Lens Diameter\";s:19:\"specification_value\";s:15:\"45mm (EDM:50mm)\";}i:2;a:2:{s:18:\"specification_name\";s:13:\"Magnification\";s:19:\"specification_value\";s:3:\"30x\";}i:3;a:2:{s:18:\"specification_name\";s:5:\"Image\";s:19:\"specification_value\";s:5:\"Erect\";}i:4;a:2:{s:18:\"specification_name\";s:13:\"Field of View\";s:19:\"specification_value\";s:3:\"130\";}i:5;a:2:{s:18:\"specification_name\";s:13:\"Resolve Power\";s:19:\"specification_value\";s:2:\"3\"\";}i:6;a:2:{s:18:\"specification_name\";s:18:\"Min Focus Distance\";s:19:\"specification_value\";s:10:\"1.3 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(18, 1, 2, 3, null, 0, 2, null, 0, 'Measurement Range', 'a:2:{i:0;a:2:{s:18:\"specification_name\";s:8:\"1 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}i:1;a:2:{s:18:\"specification_name\";s:8:\"3 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(19, 1, 2, 4, null, 0, 1, null, 0, null, null, 'Test Video Of My 4x4', 'http://www.youtube.com/embed/HNIA-nesZEU', '<p>\n	Test vid of my 4x4.&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae gravida tortor. Maecenas turpis purus, imperdiet id elementum eget, feugiat in tellus. Duis molestie, nulla nec sollicitudin consectetur, magna nisl condimentum neque, at consequat magna enim a tellus. Maecenas porttitor mattis aliquet. Ut lobortis, tortor eget lacinia commodo, mi sapien molestie neque, quis mattis metus lorem posuere massa. Vivamus id pretium lacus. Aliquam ac mauris orci, vulputate ullamcorper elit. Donec eget eros eros. Etiam ac sapien a sem ullamcorper consequat. Suspendisse ut elementum arcu. Cras fringilla tortor pellentesque nibh pharetra pellentesque. Sed et mauris sit amet ligula scelerisque sodales sed nec lorem. In vel quam sit amet eros lobortis consectetur sit amet egestas nisl. Cras erat turpis, suscipit posuere mollis nec, dictum eget lectus.</p>', null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(20, 1, 2, 5, null, 0, 1, null, 0, null, null, null, null, null, 'Test heading', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(21, 1, 2, 5, null, 0, 2, null, 0, null, null, null, null, null, 'Test heading 2', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(22, 1, 2, 6, null, 0, 1, null, 0, null, null, null, null, null, null, null, '1-3 days', 100, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(23, 1, 2, 6, null, 0, 2, null, 0, null, null, null, null, null, null, null, '4-6 days', 180, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(24, 1, 2, 7, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, '1-3 days', 80, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(25, 1, 2, 7, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, '4-6 days', 160, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(26, 1, 2, 10, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(27, 1, 2, 10, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image6.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(28, 1, 2, 11, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, 'Test uploaded file', '{filedir_2}large-product-image4.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(29, 1, 3, 1, null, 0, 1, 'NZD', 45000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(30, 1, 3, 1, null, 0, 2, 'AUD', 40000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(31, 1, 3, 3, null, 0, 1, null, 0, 'Telescope', 'a:7:{i:0;a:2:{s:18:\"specification_name\";s:6:\"Length\";s:19:\"specification_value\";s:15:\"150 Millimeters\";}i:1;a:2:{s:18:\"specification_name\";s:23:\"Objective Lens Diameter\";s:19:\"specification_value\";s:15:\"45mm (EDM:50mm)\";}i:2;a:2:{s:18:\"specification_name\";s:13:\"Magnification\";s:19:\"specification_value\";s:3:\"30x\";}i:3;a:2:{s:18:\"specification_name\";s:5:\"Image\";s:19:\"specification_value\";s:5:\"Erect\";}i:4;a:2:{s:18:\"specification_name\";s:13:\"Field of View\";s:19:\"specification_value\";s:3:\"130\";}i:5;a:2:{s:18:\"specification_name\";s:13:\"Resolve Power\";s:19:\"specification_value\";s:2:\"3\"\";}i:6;a:2:{s:18:\"specification_name\";s:18:\"Min Focus Distance\";s:19:\"specification_value\";s:10:\"1.3 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(32, 1, 3, 3, null, 0, 2, null, 0, 'Measurement Range', 'a:2:{i:0;a:2:{s:18:\"specification_name\";s:8:\"1 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}i:1;a:2:{s:18:\"specification_name\";s:8:\"3 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(33, 1, 3, 4, null, 0, 1, null, 0, null, null, 'Test Video Of My 4x4', 'http://www.youtube.com/embed/HNIA-nesZEU', '<p>\n	Test vid of my 4x4.&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae gravida tortor. Maecenas turpis purus, imperdiet id elementum eget, feugiat in tellus. Duis molestie, nulla nec sollicitudin consectetur, magna nisl condimentum neque, at consequat magna enim a tellus. Maecenas porttitor mattis aliquet. Ut lobortis, tortor eget lacinia commodo, mi sapien molestie neque, quis mattis metus lorem posuere massa. Vivamus id pretium lacus. Aliquam ac mauris orci, vulputate ullamcorper elit. Donec eget eros eros. Etiam ac sapien a sem ullamcorper consequat. Suspendisse ut elementum arcu. Cras fringilla tortor pellentesque nibh pharetra pellentesque. Sed et mauris sit amet ligula scelerisque sodales sed nec lorem. In vel quam sit amet eros lobortis consectetur sit amet egestas nisl. Cras erat turpis, suscipit posuere mollis nec, dictum eget lectus.</p>', null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(34, 1, 3, 5, null, 0, 1, null, 0, null, null, null, null, null, 'Test heading', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(35, 1, 3, 5, null, 0, 2, null, 0, null, null, null, null, null, 'Test heading 2', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(36, 1, 3, 6, null, 0, 1, null, 0, null, null, null, null, null, null, null, '1-3 days', 100, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(37, 1, 3, 6, null, 0, 2, null, 0, null, null, null, null, null, null, null, '4-6 days', 180, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(38, 1, 3, 7, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, '1-3 days', 80, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(39, 1, 3, 7, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, '4-6 days', 160, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(40, 1, 3, 10, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(41, 1, 3, 10, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image6.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(42, 1, 3, 11, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, 'Test uploaded file', '{filedir_2}large-product-image4.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(43, 1, 11, 15, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, 'Auckland', 'a:4:{i:0;a:2:{s:14:\"contact_method\";s:9:\"Free Call\";s:15:\"contact_details\";s:12:\"0800-867-266\";}i:1;a:2:{s:14:\"contact_method\";s:5:\"Phone\";s:15:\"contact_details\";s:14:\"+64-9-476-5151\";}i:2;a:2:{s:14:\"contact_method\";s:3:\"Fax\";s:15:\"contact_details\";s:14:\"+63-9-476-5140\";}i:3;a:2:{s:14:\"contact_method\";s:5:\"Email\";s:15:\"contact_details\";s:30:\" info@synergypositioning.co.nz\";}}', '8:00am to 5:00pm', '3/52 Arrenway Drive \nAlbany \nAuckland \nNew Zealand', 'P O Box 100450 \nNSMC \nAuckland \nNew Zealand \n0745', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '{filedir_11}map1.jpg', null, null);
INSERT INTO `exp_matrix_data` VALUES(44, 1, 11, 15, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, 'Christchurch', 'a:4:{i:0;a:2:{s:14:\"contact_method\";s:9:\"Free Call\";s:15:\"contact_details\";s:13:\" 0800-867-266\";}i:1;a:2:{s:14:\"contact_method\";s:5:\"Phone\";s:15:\"contact_details\";s:14:\"+64-9-476-5151\";}i:2;a:2:{s:14:\"contact_method\";s:3:\"Fax\";s:15:\"contact_details\";s:14:\"+63-9-476-5140\";}i:3;a:2:{s:14:\"contact_method\";s:5:\"Email\";s:15:\"contact_details\";s:30:\" info@synergypositioning.co.nz\";}}', '8:00am to 5:00pm', '3/52 Arrenway Drive \nAlbany \nAuckland \nNew Zealand', 'P O Box 100450 \nNSMC \nAuckland \nNew Zealand \n0745', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '{filedir_11}map2.jpg', null, null);
INSERT INTO `exp_matrix_data` VALUES(45, 1, 3, 10, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image6.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(46, 1, 2, 8, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, '{filedir_1}cat-feature-slide1.jpg', null, null, null, null, null, null, null, null, 'Test description 1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(47, 1, 2, 8, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, '{filedir_1}cat-feature-slide3.jpg', null, null, null, null, null, null, null, null, 'Test description 2', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(48, 1, 12, 17, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	BRAMOR Orthophoto</h2>\n<p>\n	The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability, ergonomic and user friendly ground control station, durability and mobility. The airframe is manufactured with CAD/CAM technology from advanced composite materials (Kevlar/Carbon/Vectran) that provide a high level of survivability. A high visibility color scheme and strobe and NAV lights are optional. Embedded autonomous flight procedures include an array of failsafe options based on man-rated standards.</p>\n<p>\n	The system consists of the air vehicle with a 25 megapixel DSLR E/O sensor, a foldable portable take off ramp and rugged ground control station. The whole system fits into one rugged transport case. The system is flight ready in under five minutes. The system is safely operated by one operator/pilot in command.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(49, 1, 12, 17, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>\n<p>\n	Solutpat metus, nec auctor mi metus eu nibh. Integer id nibh natis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malsapien, et vulputate libero. Praesent interdum risus a libero consequat dapibus. Nulla facilisi. Aliquam erat nisi, placerat id laoreet quis, fringilla ac dolor. Nam interdum tristique augue.</p>\n<p>\n	Nullam accumsan suscipit magna, non vestibulum tortor eleifend eget. Fusce mollis venenatis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(50, 1, 12, 17, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel.</p>\n<p>\n	Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(51, 1, 13, 17, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	BRAMOR Orthophoto</h2>\n<p>\n	The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability, ergonomic and user friendly ground control station, durability and mobility. The airframe is manufactured with CAD/CAM technology from advanced composite materials (Kevlar/Carbon/Vectran) that provide a high level of survivability. A high visibility color scheme and strobe and NAV lights are optional. Embedded autonomous flight procedures include an array of failsafe options based on man-rated standards.</p>\n<p>\n	The system consists of the air vehicle with a 25 megapixel DSLR E/O sensor, a foldable portable take off ramp and rugged ground control station. The whole system fits into one rugged transport case. The system is flight ready in under five minutes. The system is safely operated by one operator/pilot in command.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(52, 1, 13, 17, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>\n<p>\n	Solutpat metus, nec auctor mi metus eu nibh. Integer id nibh natis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malsapien, et vulputate libero. Praesent interdum risus a libero consequat dapibus. Nulla facilisi. Aliquam erat nisi, placerat id laoreet quis, fringilla ac dolor. Nam interdum tristique augue.</p>\n<p>\n	Nullam accumsan suscipit magna, non vestibulum tortor eleifend eget. Fusce mollis venenatis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(53, 1, 13, 17, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel.</p>\n<p>\n	Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(54, 1, 14, 17, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	BRAMOR Orthophoto</h2>\n<p>\n	The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability, ergonomic and user friendly ground control station, durability and mobility. The airframe is manufactured with CAD/CAM technology from advanced composite materials (Kevlar/Carbon/Vectran) that provide a high level of survivability. A high visibility color scheme and strobe and NAV lights are optional. Embedded autonomous flight procedures include an array of failsafe options based on man-rated standards.</p>\n<p>\n	The system consists of the air vehicle with a 25 megapixel DSLR E/O sensor, a foldable portable take off ramp and rugged ground control station. The whole system fits into one rugged transport case. The system is flight ready in under five minutes. The system is safely operated by one operator/pilot in command.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(55, 1, 14, 17, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>\n<p>\n	Solutpat metus, nec auctor mi metus eu nibh. Integer id nibh natis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malsapien, et vulputate libero. Praesent interdum risus a libero consequat dapibus. Nulla facilisi. Aliquam erat nisi, placerat id laoreet quis, fringilla ac dolor. Nam interdum tristique augue.</p>\n<p>\n	Nullam accumsan suscipit magna, non vestibulum tortor eleifend eget. Fusce mollis venenatis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(56, 1, 14, 17, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel.</p>\n<p>\n	Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(57, 1, 15, 17, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	BRAMOR Orthophoto</h2>\n<p>\n	The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability, ergonomic and user friendly ground control station, durability and mobility. The airframe is manufactured with CAD/CAM technology from advanced composite materials (Kevlar/Carbon/Vectran) that provide a high level of survivability. A high visibility color scheme and strobe and NAV lights are optional. Embedded autonomous flight procedures include an array of failsafe options based on man-rated standards.</p>\n<p>\n	The system consists of the air vehicle with a 25 megapixel DSLR E/O sensor, a foldable portable take off ramp and rugged ground control station. The whole system fits into one rugged transport case. The system is flight ready in under five minutes. The system is safely operated by one operator/pilot in command.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(58, 1, 15, 17, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>\n<p>\n	Solutpat metus, nec auctor mi metus eu nibh. Integer id nibh natis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malsapien, et vulputate libero. Praesent interdum risus a libero consequat dapibus. Nulla facilisi. Aliquam erat nisi, placerat id laoreet quis, fringilla ac dolor. Nam interdum tristique augue.</p>\n<p>\n	Nullam accumsan suscipit magna, non vestibulum tortor eleifend eget. Fusce mollis venenatis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(59, 1, 15, 17, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel.</p>\n<p>\n	Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(60, 1, 23, 22, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, 'Services.', 'Marketing message here', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(61, 1, 23, 23, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, 'Read more about services', '/services', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(62, 1, 24, 22, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, 'Trusted precision and control.', 'Marketing message here', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(63, 1, 24, 23, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, 'Browse, enquire & hire from our product range', '/products', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(64, 1, 25, 22, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, 'Synergy Positioning Systems', 'Marketing message here.', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(65, 1, 25, 23, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, 'Read more about Synergy', '/about-us', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(66, 1, 26, 26, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(67, 1, 26, 26, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	<img alt=\"\" src=\"{filedir_5}news-temp-top_(1).jpg\" style=\"width: 200px; height: 86px; float: right;\" /></p>\n<p>\n	L</p>\n<p>\n	orem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odi</p>\n<p>\n	o, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(68, 1, 27, 30, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	You are dealing with an established company representing world renowned brands that have been tried and tested in increasing productivity and reducing costs.</p>', 'Simple really....', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(69, 1, 27, 30, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	With our equipment and systems, you gain efficiency &amp; reduce costs, saving you money on all aspects of the job including: fuel, wages, wear &amp; tear, materials &amp; processing.</p>', 'Your profit...\n', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(70, 1, 27, 30, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	We only represent equipment &amp; brands that we trust. Topcon, world leaders in positioning equipment for over 80 years &amp; well established throughout the world, offers reliable &amp; robust equipment for any application.</p>', 'Our Products\n', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(71, 1, 27, 30, null, 0, 4, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	Synergy Positioning Systems Ltd is 100% New Zealand owned and operated. Our industry exprience spans more than 30 years.</p>', 'Our Experience\n', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(72, 1, 27, 30, null, 0, 5, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	We are commited to our customers. Your success is our reward. Our factory trained technicians and in-house industry experts ensure that what we sell is fully supported.</p>', 'Our Service\n', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(73, 1, 28, 32, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'James McFall', 'The man', '{filedir_9}james.jpg', null, null, null, null, null, null, null, null, null, null, 'NZ', null, null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non faucibus mi. Ut mattis congue neque, sollicitudin sagittis urna tempor a. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis dolor diam, consequat nec dictum quis, volutpat eget odio. Nunc eu diam odio, in commodo sapien. Pellentesque id nibh eget felis sagittis ultrices ultrices et est. Quisque ut felis non lectus volutpat iaculis et in nulla.</p>\n<p>\n	Maecenas nisl metus, ultricies quis convallis ac, faucibus aliquet quam. Praesent dignissim, nunc sit amet elementum lobortis, neque mi adipiscing urna, gravida viverra magna dui in quam. Suspendisse potenti. Quisque nec neque ac massa tincidunt convallis. Donec aliquam pulvinar purus eget viverra. Nullam quis dolor neque. Praesent imperdiet turpis est.</p>\n<p>\n	Praesent nulla sem, pharetra eu posuere id, ornare vel ligula. Duis vulputate sapien id odio bibendum vitae consectetur mauris fermentum. Donec porta ultricies ultricies. Nam tortor nulla, aliquam nec dignissim eleifend, imperdiet quis neque. Sed auctor laoreet mollis. Nulla eu nisi in mi dapibus cursus ac id massa. Cras molestie tincidunt fringilla. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>');
INSERT INTO `exp_matrix_data` VALUES(74, 1, 28, 32, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'John Smith', 'Unknown', null, null, null, null, null, null, null, null, null, null, null, 'AU', null, null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non faucibus mi. Ut mattis congue neque, sollicitudin sagittis urna tempor a. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis dolor diam, consequat nec dictum quis, volutpat eget odio. Nunc eu diam odio, in commodo sapien. Pellentesque id nibh eget felis sagittis ultrices ultrices et est. Quisque ut felis non lectus volutpat iaculis et in nulla.<br />\n	Maecenas nisl metus, ultricies quis convallis ac, faucibus aliquet quam. Praesent dignissim, nunc sit amet elementum lobortis, neque mi adipiscing urna, gravida viverra magna dui in quam. Suspendisse potenti. Quisque nec neque ac massa tincidunt convallis. Donec aliquam pulvinar purus eget viverra. Nullam quis dolor neque. Praesent imperdiet turpis est.</p>\n<p>\n	Praesent nulla sem, pharetra eu posuere id, ornare vel ligula. Duis vulputate sapien id odio bibendum vitae consectetur mauris fermentum. Donec porta ultricies ultricies. Nam tortor nulla, aliquam nec dignissim eleifend, imperdiet quis neque. Sed auctor laoreet mollis. Nulla eu nisi in mi dapibus cursus ac id massa. Cras molestie tincidunt fringilla. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>');
INSERT INTO `exp_matrix_data` VALUES(75, 1, 1, 8, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, '{filedir_1}cat-feature-slide3.jpg', null, null, null, null, null, null, null, null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(76, 1, 11, 38, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0800-867-266', 'info@96black.co.nz', 'NZ', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(77, 1, 11, 38, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0900-123-456', 'info@96black.co.nz', 'AU', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(78, 1, 11, 39, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'Sydney', 'a:4:{i:0;a:2:{s:14:\"contact_method\";s:9:\"Free Call\";s:15:\"contact_details\";s:13:\"0900-123-4567\";}i:1;a:2:{s:14:\"contact_method\";s:5:\"Phone\";s:15:\"contact_details\";s:6:\"123456\";}i:2;a:2:{s:14:\"contact_method\";s:3:\"Fax\";s:15:\"contact_details\";s:6:\"123456\";}i:3;a:2:{s:14:\"contact_method\";s:5:\"Email\";s:15:\"contact_details\";s:31:\" info@synergypositioning.com.au\";}}', '8:00am to 5:00pm', '123 Some Steet', 'Some PO Box', null, null, null, null, '{filedir_11}map1.jpg', null);
INSERT INTO `exp_matrix_data` VALUES(79, 1, 27, 42, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'Test grid item', '<p>\n	This is a test grid item</p>', null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(80, 1, 34, 22, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, 'Synergy Positioning Systems', 'Marketing message here.', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(81, 1, 34, 23, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, 'Read more about Synergy', '/about-us', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(82, 1, 35, 26, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(83, 1, 35, 26, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	<img alt=\"\" src=\"{filedir_5}news-temp-top_(1).jpg\" style=\"width: 200px; height: 86px; float: left;\" /></p>\n<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a.</p>\n<p>\n	o, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(84, 1, 37, 30, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	Some content</p>', 'Test grid item', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

/* Table structure for table `exp_member_bulletin_board` */
DROP TABLE IF EXISTS `exp_member_bulletin_board`;

CREATE TABLE `exp_member_bulletin_board` (
  `bulletin_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `bulletin_group` int(8) unsigned NOT NULL,
  `bulletin_date` int(10) unsigned NOT NULL,
  `hash` varchar(10) NOT NULL DEFAULT '',
  `bulletin_expires` int(10) unsigned NOT NULL DEFAULT '0',
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_id`),
  KEY `sender_id` (`sender_id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_member_data` */
DROP TABLE IF EXISTS `exp_member_data`;

CREATE TABLE `exp_member_data` (
  `member_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_member_data` */
INSERT INTO `exp_member_data` VALUES(1);

/* Table structure for table `exp_member_fields` */
DROP TABLE IF EXISTS `exp_member_fields`;

CREATE TABLE `exp_member_fields` (
  `m_field_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `m_field_name` varchar(32) NOT NULL,
  `m_field_label` varchar(50) NOT NULL,
  `m_field_description` text NOT NULL,
  `m_field_type` varchar(12) NOT NULL DEFAULT 'text',
  `m_field_list_items` text NOT NULL,
  `m_field_ta_rows` tinyint(2) DEFAULT '8',
  `m_field_maxl` smallint(3) NOT NULL,
  `m_field_width` varchar(6) NOT NULL,
  `m_field_search` char(1) NOT NULL DEFAULT 'y',
  `m_field_required` char(1) NOT NULL DEFAULT 'n',
  `m_field_public` char(1) NOT NULL DEFAULT 'y',
  `m_field_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_cp_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_fmt` char(5) NOT NULL DEFAULT 'none',
  `m_field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`m_field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_member_groups` */
DROP TABLE IF EXISTS `exp_member_groups`;

CREATE TABLE `exp_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_title` varchar(100) NOT NULL,
  `group_description` text NOT NULL,
  `is_locked` char(1) NOT NULL DEFAULT 'y',
  `can_view_offline_system` char(1) NOT NULL DEFAULT 'n',
  `can_view_online_system` char(1) NOT NULL DEFAULT 'y',
  `can_access_cp` char(1) NOT NULL DEFAULT 'y',
  `can_access_content` char(1) NOT NULL DEFAULT 'n',
  `can_access_publish` char(1) NOT NULL DEFAULT 'n',
  `can_access_edit` char(1) NOT NULL DEFAULT 'n',
  `can_access_files` char(1) NOT NULL DEFAULT 'n',
  `can_access_fieldtypes` char(1) NOT NULL DEFAULT 'n',
  `can_access_design` char(1) NOT NULL DEFAULT 'n',
  `can_access_addons` char(1) NOT NULL DEFAULT 'n',
  `can_access_modules` char(1) NOT NULL DEFAULT 'n',
  `can_access_extensions` char(1) NOT NULL DEFAULT 'n',
  `can_access_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_access_plugins` char(1) NOT NULL DEFAULT 'n',
  `can_access_members` char(1) NOT NULL DEFAULT 'n',
  `can_access_admin` char(1) NOT NULL DEFAULT 'n',
  `can_access_sys_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_content_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_tools` char(1) NOT NULL DEFAULT 'n',
  `can_access_comm` char(1) NOT NULL DEFAULT 'n',
  `can_access_utilities` char(1) NOT NULL DEFAULT 'n',
  `can_access_data` char(1) NOT NULL DEFAULT 'n',
  `can_access_logs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_channels` char(1) NOT NULL DEFAULT 'n',
  `can_admin_upload_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_design` char(1) NOT NULL DEFAULT 'n',
  `can_admin_members` char(1) NOT NULL DEFAULT 'n',
  `can_delete_members` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_groups` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_templates` char(1) NOT NULL DEFAULT 'n',
  `can_ban_users` char(1) NOT NULL DEFAULT 'n',
  `can_admin_modules` char(1) NOT NULL DEFAULT 'n',
  `can_admin_templates` char(1) NOT NULL DEFAULT 'n',
  `can_admin_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_edit_categories` char(1) NOT NULL DEFAULT 'n',
  `can_delete_categories` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_edit_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_assign_post_authors` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self_entries` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_entries` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_moderate_comments` char(1) NOT NULL DEFAULT 'n',
  `can_send_email` char(1) NOT NULL DEFAULT 'n',
  `can_send_cached_email` char(1) NOT NULL DEFAULT 'n',
  `can_email_member_groups` char(1) NOT NULL DEFAULT 'n',
  `can_email_mailinglist` char(1) NOT NULL DEFAULT 'n',
  `can_email_from_profile` char(1) NOT NULL DEFAULT 'n',
  `can_view_profiles` char(1) NOT NULL DEFAULT 'n',
  `can_edit_html_buttons` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self` char(1) NOT NULL DEFAULT 'n',
  `mbr_delete_notify_emails` varchar(255) DEFAULT NULL,
  `can_post_comments` char(1) NOT NULL DEFAULT 'n',
  `exclude_from_moderation` char(1) NOT NULL DEFAULT 'n',
  `can_search` char(1) NOT NULL DEFAULT 'n',
  `search_flood_control` mediumint(5) unsigned NOT NULL,
  `can_send_private_messages` char(1) NOT NULL DEFAULT 'n',
  `prv_msg_send_limit` smallint(5) unsigned NOT NULL DEFAULT '20',
  `prv_msg_storage_limit` smallint(5) unsigned NOT NULL DEFAULT '60',
  `can_attach_in_private_messages` char(1) NOT NULL DEFAULT 'n',
  `can_send_bulletins` char(1) NOT NULL DEFAULT 'n',
  `include_in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `include_in_memberlist` char(1) NOT NULL DEFAULT 'y',
  `include_in_mailinglists` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`group_id`,`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_member_groups` */
INSERT INTO `exp_member_groups` VALUES(1, 1, 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y');
INSERT INTO `exp_member_groups` VALUES(1, 2, 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y');
INSERT INTO `exp_member_groups` VALUES(2, 1, 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', 60, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(2, 2, 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', 60, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(3, 1, 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(3, 2, 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(4, 1, 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(4, 2, 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(5, 1, 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', 10, 'y', 20, 60, 'y', 'n', 'n', 'y', 'y');
INSERT INTO `exp_member_groups` VALUES(5, 2, 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', 10, 'y', 20, 60, 'y', 'n', 'n', 'y', 'y');

/* Table structure for table `exp_member_homepage` */
DROP TABLE IF EXISTS `exp_member_homepage`;

CREATE TABLE `exp_member_homepage` (
  `member_id` int(10) unsigned NOT NULL,
  `recent_entries` char(1) NOT NULL DEFAULT 'l',
  `recent_entries_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_comments` char(1) NOT NULL DEFAULT 'l',
  `recent_comments_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_members` char(1) NOT NULL DEFAULT 'n',
  `recent_members_order` int(3) unsigned NOT NULL DEFAULT '0',
  `site_statistics` char(1) NOT NULL DEFAULT 'r',
  `site_statistics_order` int(3) unsigned NOT NULL DEFAULT '0',
  `member_search_form` char(1) NOT NULL DEFAULT 'n',
  `member_search_form_order` int(3) unsigned NOT NULL DEFAULT '0',
  `notepad` char(1) NOT NULL DEFAULT 'r',
  `notepad_order` int(3) unsigned NOT NULL DEFAULT '0',
  `bulletin_board` char(1) NOT NULL DEFAULT 'r',
  `bulletin_board_order` int(3) unsigned NOT NULL DEFAULT '0',
  `pmachine_news_feed` char(1) NOT NULL DEFAULT 'n',
  `pmachine_news_feed_order` int(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_member_homepage` */
INSERT INTO `exp_member_homepage` VALUES(1, 'l', 1, 'l', 2, 'n', 0, 'r', 1, 'n', 0, 'r', 2, 'r', 0, 'l', 0);

/* Table structure for table `exp_member_search` */
DROP TABLE IF EXISTS `exp_member_search`;

CREATE TABLE `exp_member_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `search_date` int(10) unsigned NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `fields` varchar(200) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(8) unsigned NOT NULL,
  `query` text NOT NULL,
  PRIMARY KEY (`search_id`),
  KEY `member_id` (`member_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_members` */
DROP TABLE IF EXISTS `exp_members`;

CREATE TABLE `exp_members` (
  `member_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` smallint(4) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL DEFAULT '',
  `unique_id` varchar(40) NOT NULL,
  `crypt_key` varchar(40) DEFAULT NULL,
  `authcode` varchar(10) DEFAULT NULL,
  `email` varchar(72) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `occupation` varchar(80) DEFAULT NULL,
  `interests` varchar(120) DEFAULT NULL,
  `bday_d` int(2) DEFAULT NULL,
  `bday_m` int(2) DEFAULT NULL,
  `bday_y` int(4) DEFAULT NULL,
  `aol_im` varchar(50) DEFAULT NULL,
  `yahoo_im` varchar(50) DEFAULT NULL,
  `msn_im` varchar(50) DEFAULT NULL,
  `icq` varchar(50) DEFAULT NULL,
  `bio` text,
  `signature` text,
  `avatar_filename` varchar(120) DEFAULT NULL,
  `avatar_width` int(4) unsigned DEFAULT NULL,
  `avatar_height` int(4) unsigned DEFAULT NULL,
  `photo_filename` varchar(120) DEFAULT NULL,
  `photo_width` int(4) unsigned DEFAULT NULL,
  `photo_height` int(4) unsigned DEFAULT NULL,
  `sig_img_filename` varchar(120) DEFAULT NULL,
  `sig_img_width` int(4) unsigned DEFAULT NULL,
  `sig_img_height` int(4) unsigned DEFAULT NULL,
  `ignore_list` text,
  `private_messages` int(4) unsigned NOT NULL DEFAULT '0',
  `accept_messages` char(1) NOT NULL DEFAULT 'y',
  `last_view_bulletins` int(10) NOT NULL DEFAULT '0',
  `last_bulletin_date` int(10) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `join_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visit` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `total_entries` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_comments` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `accept_admin_email` char(1) NOT NULL DEFAULT 'y',
  `accept_user_email` char(1) NOT NULL DEFAULT 'y',
  `notify_by_default` char(1) NOT NULL DEFAULT 'y',
  `notify_of_pm` char(1) NOT NULL DEFAULT 'y',
  `display_avatars` char(1) NOT NULL DEFAULT 'y',
  `display_signatures` char(1) NOT NULL DEFAULT 'y',
  `parse_smileys` char(1) NOT NULL DEFAULT 'y',
  `smart_notifications` char(1) NOT NULL DEFAULT 'y',
  `language` varchar(50) NOT NULL,
  `timezone` varchar(8) NOT NULL,
  `daylight_savings` char(1) NOT NULL DEFAULT 'n',
  `localization_is_site_default` char(1) NOT NULL DEFAULT 'n',
  `time_format` char(2) NOT NULL DEFAULT 'us',
  `cp_theme` varchar(32) DEFAULT NULL,
  `profile_theme` varchar(32) DEFAULT NULL,
  `forum_theme` varchar(32) DEFAULT NULL,
  `tracker` text,
  `template_size` varchar(2) NOT NULL DEFAULT '20',
  `notepad` text,
  `notepad_size` varchar(2) NOT NULL DEFAULT '18',
  `quick_links` text,
  `quick_tabs` text,
  `show_sidebar` char(1) NOT NULL DEFAULT 'n',
  `pmember_id` int(10) NOT NULL DEFAULT '0',
  `rte_enabled` char(1) NOT NULL DEFAULT 'y',
  `rte_toolset_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`),
  KEY `group_id` (`group_id`),
  KEY `unique_id` (`unique_id`),
  KEY `password` (`password`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_members` */
INSERT INTO `exp_members` VALUES(1, 1, '96black', '96black', '106cfaa54b9caa63de2a620edb474b900a68d8e76e79847360bbef4323d51b29ea822b5dfe7f5352be760a6ae6a6de7b8abb78f37ad429db302a103d864acad8', '=WDztY<`dk>S{94N7[sY$1\'WCM{:qPT;Z{P44_rlj\'GU8,WH]l$%3d27{+oz,-=(&Ed*nE>q)!+z4p=a;?`yYL|]l:.`/U.=P3lU@\"n*jX_-=GO>!E+?()Cqj<ai>Q=a', '2d3c4dab12f0973e5396766a007ac2efd798eb16', 'b9cbee325c2edd1db5f8b3ef603a2ad0bb317a5f', null, 'james@96black.co.nz', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 0, 'y', 0, 0, '127.0.0.1', 1355879065, 1366347398, 1366592063, 37, 0, 0, 0, 1366153219, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UP12', 'y', 'n', 'us', null, null, null, null, '20', null, '18', '', 'Template Manager|C=design&M=manager|1', 'n', 0, 'y', 0);

/* Table structure for table `exp_message_attachments` */
DROP TABLE IF EXISTS `exp_message_attachments`;

CREATE TABLE `exp_message_attachments` (
  `attachment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_name` varchar(50) NOT NULL DEFAULT '',
  `attachment_hash` varchar(40) NOT NULL DEFAULT '',
  `attachment_extension` varchar(20) NOT NULL DEFAULT '',
  `attachment_location` varchar(150) NOT NULL DEFAULT '',
  `attachment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_size` int(10) unsigned NOT NULL DEFAULT '0',
  `is_temp` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`attachment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_message_copies` */
DROP TABLE IF EXISTS `exp_message_copies`;

CREATE TABLE `exp_message_copies` (
  `copy_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `recipient_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_received` char(1) NOT NULL DEFAULT 'n',
  `message_read` char(1) NOT NULL DEFAULT 'n',
  `message_time_read` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_downloaded` char(1) NOT NULL DEFAULT 'n',
  `message_folder` int(10) unsigned NOT NULL DEFAULT '1',
  `message_authcode` varchar(10) NOT NULL DEFAULT '',
  `message_deleted` char(1) NOT NULL DEFAULT 'n',
  `message_status` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`copy_id`),
  KEY `message_id` (`message_id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_message_data` */
DROP TABLE IF EXISTS `exp_message_data`;

CREATE TABLE `exp_message_data` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_date` int(10) unsigned NOT NULL DEFAULT '0',
  `message_subject` varchar(255) NOT NULL DEFAULT '',
  `message_body` text NOT NULL,
  `message_tracking` char(1) NOT NULL DEFAULT 'y',
  `message_attachments` char(1) NOT NULL DEFAULT 'n',
  `message_recipients` varchar(200) NOT NULL DEFAULT '',
  `message_cc` varchar(200) NOT NULL DEFAULT '',
  `message_hide_cc` char(1) NOT NULL DEFAULT 'n',
  `message_sent_copy` char(1) NOT NULL DEFAULT 'n',
  `total_recipients` int(5) unsigned NOT NULL DEFAULT '0',
  `message_status` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`message_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_message_folders` */
DROP TABLE IF EXISTS `exp_message_folders`;

CREATE TABLE `exp_message_folders` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `folder1_name` varchar(50) NOT NULL DEFAULT 'InBox',
  `folder2_name` varchar(50) NOT NULL DEFAULT 'Sent',
  `folder3_name` varchar(50) NOT NULL DEFAULT '',
  `folder4_name` varchar(50) NOT NULL DEFAULT '',
  `folder5_name` varchar(50) NOT NULL DEFAULT '',
  `folder6_name` varchar(50) NOT NULL DEFAULT '',
  `folder7_name` varchar(50) NOT NULL DEFAULT '',
  `folder8_name` varchar(50) NOT NULL DEFAULT '',
  `folder9_name` varchar(50) NOT NULL DEFAULT '',
  `folder10_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_message_folders` */
INSERT INTO `exp_message_folders` VALUES(1, 'InBox', 'Sent', '', '', '', '', '', '', '', '');

/* Table structure for table `exp_message_listed` */
DROP TABLE IF EXISTS `exp_message_listed`;

CREATE TABLE `exp_message_listed` (
  `listed_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_member` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_description` varchar(100) NOT NULL DEFAULT '',
  `listed_type` varchar(10) NOT NULL DEFAULT 'blocked',
  PRIMARY KEY (`listed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_module_member_groups` */
DROP TABLE IF EXISTS `exp_module_member_groups`;

CREATE TABLE `exp_module_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `module_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_modules` */
DROP TABLE IF EXISTS `exp_modules`;

CREATE TABLE `exp_modules` (
  `module_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(50) NOT NULL,
  `module_version` varchar(12) NOT NULL,
  `has_cp_backend` char(1) NOT NULL DEFAULT 'n',
  `has_publish_fields` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_modules` */
INSERT INTO `exp_modules` VALUES(1, 'Comment', '2.3', 'y', 'n');
INSERT INTO `exp_modules` VALUES(2, 'Email', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(3, 'Emoticon', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(4, 'File', '1.0.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(5, 'Jquery', '1.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(6, 'Rss', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(7, 'Safecracker', '2.1', 'y', 'n');
INSERT INTO `exp_modules` VALUES(8, 'Search', '2.2', 'n', 'n');
INSERT INTO `exp_modules` VALUES(9, 'Channel', '2.0.1', 'n', 'n');
INSERT INTO `exp_modules` VALUES(10, 'Member', '2.1', 'n', 'n');
INSERT INTO `exp_modules` VALUES(11, 'Stats', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(12, 'Rte', '1.0', 'y', 'n');
INSERT INTO `exp_modules` VALUES(13, 'Playa', '4.3.3', 'n', 'n');
INSERT INTO `exp_modules` VALUES(14, 'Wygwam', '2.6.3', 'y', 'n');
INSERT INTO `exp_modules` VALUES(15, 'Query', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(16, 'Freeform', '4.0.11', 'y', 'n');

/* Table structure for table `exp_online_users` */
DROP TABLE IF EXISTS `exp_online_users`;

CREATE TABLE `exp_online_users` (
  `online_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `in_forum` char(1) NOT NULL DEFAULT 'n',
  `name` varchar(50) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `anon` char(1) NOT NULL,
  PRIMARY KEY (`online_id`),
  KEY `date` (`date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=228 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_online_users` */
INSERT INTO `exp_online_users` VALUES(6, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(8, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(9, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(129, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(140, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(154, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(156, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(199, 1, 0, 'n', '', '127.0.0.1', 1366592411, '');
INSERT INTO `exp_online_users` VALUES(201, 1, 0, 'n', '', '127.0.0.1', 1366592411, '');
INSERT INTO `exp_online_users` VALUES(204, 1, 0, 'n', '', '127.0.0.1', 1366592411, '');
INSERT INTO `exp_online_users` VALUES(205, 1, 0, 'n', '', '127.0.0.1', 1366592411, '');
INSERT INTO `exp_online_users` VALUES(206, 1, 0, 'n', '', '127.0.0.1', 1366592411, '');
INSERT INTO `exp_online_users` VALUES(209, 1, 0, 'n', '', '127.0.0.1', 1366592411, '');
INSERT INTO `exp_online_users` VALUES(211, 1, 0, 'n', '', '127.0.0.1', 1366592411, '');
INSERT INTO `exp_online_users` VALUES(213, 1, 0, 'n', '', '127.0.0.1', 1366592411, '');
INSERT INTO `exp_online_users` VALUES(215, 1, 0, 'n', '', '127.0.0.1', 1366592411, '');
INSERT INTO `exp_online_users` VALUES(216, 1, 0, 'n', '', '127.0.0.1', 1366592411, '');
INSERT INTO `exp_online_users` VALUES(217, 1, 0, 'n', '', '127.0.0.1', 1366592411, '');
INSERT INTO `exp_online_users` VALUES(218, 1, 0, 'n', '', '127.0.0.1', 1366592411, '');
INSERT INTO `exp_online_users` VALUES(220, 1, 0, 'n', '', '127.0.0.1', 1366592411, '');
INSERT INTO `exp_online_users` VALUES(223, 1, 0, 'n', '', '127.0.0.1', 1366592411, '');
INSERT INTO `exp_online_users` VALUES(225, 1, 0, 'n', '', '127.0.0.1', 1366592411, '');
INSERT INTO `exp_online_users` VALUES(227, 1, 0, 'n', '', '127.0.0.1', 1366592411, '');

/* Table structure for table `exp_password_lockout` */
DROP TABLE IF EXISTS `exp_password_lockout`;

CREATE TABLE `exp_password_lockout` (
  `lockout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`lockout_id`),
  KEY `login_date` (`login_date`),
  KEY `ip_address` (`ip_address`),
  KEY `user_agent` (`user_agent`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;


/* Table structure for table `exp_ping_servers` */
DROP TABLE IF EXISTS `exp_ping_servers`;

CREATE TABLE `exp_ping_servers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `server_name` varchar(32) NOT NULL,
  `server_url` varchar(150) NOT NULL,
  `port` varchar(4) NOT NULL DEFAULT '80',
  `ping_protocol` varchar(12) NOT NULL DEFAULT 'xmlrpc',
  `is_default` char(1) NOT NULL DEFAULT 'y',
  `server_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_playa_relationships` */
DROP TABLE IF EXISTS `exp_playa_relationships`;

CREATE TABLE `exp_playa_relationships` (
  `rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_entry_id` int(10) unsigned DEFAULT NULL,
  `parent_field_id` int(6) unsigned DEFAULT NULL,
  `parent_col_id` int(6) unsigned DEFAULT NULL,
  `parent_row_id` int(10) unsigned DEFAULT NULL,
  `parent_var_id` int(6) unsigned DEFAULT NULL,
  `child_entry_id` int(10) unsigned DEFAULT NULL,
  `rel_order` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `parent_entry_id` (`parent_entry_id`),
  KEY `parent_field_id` (`parent_field_id`),
  KEY `parent_col_id` (`parent_col_id`),
  KEY `parent_row_id` (`parent_row_id`),
  KEY `parent_var_id` (`parent_var_id`),
  KEY `child_entry_id` (`child_entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_relationships` */
DROP TABLE IF EXISTS `exp_relationships`;

CREATE TABLE `exp_relationships` (
  `rel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `rel_parent_id` int(10) NOT NULL DEFAULT '0',
  `rel_child_id` int(10) NOT NULL DEFAULT '0',
  `rel_type` varchar(12) NOT NULL,
  `rel_data` mediumtext NOT NULL,
  `reverse_rel_data` mediumtext NOT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `rel_parent_id` (`rel_parent_id`),
  KEY `rel_child_id` (`rel_child_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_relationships` */
INSERT INTO `exp_relationships` VALUES(1, 12, 2, 'channel', '', 'a:3:{s:5:\"query\";O:18:\"CI_DB_mysql_result\":7:{s:7:\"conn_id\";i:0;s:9:\"result_id\";i:0;s:12:\"result_array\";a:1:{i:0;a:155:{s:8:\"entry_id\";s:2:\"12\";s:10:\"channel_id\";s:1:\"2\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:17:\"BRAMOR Orthophoto\";s:9:\"url_title\";s:17:\"bramor-orthophoto\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1363822454\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"03\";s:3:\"day\";s:2:\"21\";s:9:\"edit_date\";s:14:\"20130320235515\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:22:\"Featured Product Pages\";s:12:\"channel_name\";s:22:\"featured_product_pages\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"1\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:1:\"1\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:335:\"The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";s:11:\"field_id_38\";s:0:\"\";s:11:\"field_ft_38\";s:4:\"none\";s:11:\"field_id_39\";s:0:\"\";s:11:\"field_ft_39\";s:4:\"none\";s:11:\"field_id_40\";s:0:\"\";s:11:\"field_ft_40\";s:4:\"none\";s:11:\"field_id_42\";s:0:\"\";s:11:\"field_ft_42\";s:4:\"none\";s:11:\"field_id_43\";s:0:\"\";s:11:\"field_ft_43\";s:4:\"none\";s:11:\"field_id_44\";s:0:\"\";s:11:\"field_ft_44\";s:5:\"xhtml\";s:11:\"field_id_45\";s:0:\"\";s:11:\"field_ft_45\";s:5:\"xhtml\";s:11:\"field_id_46\";s:0:\"\";s:11:\"field_ft_46\";s:5:\"xhtml\";s:11:\"field_id_47\";s:0:\"\";s:11:\"field_ft_47\";s:5:\"xhtml\";s:11:\"field_id_48\";s:0:\"\";s:11:\"field_ft_48\";s:4:\"none\";s:11:\"field_id_49\";s:0:\"\";s:11:\"field_ft_49\";s:4:\"none\";}}s:13:\"result_object\";a:0:{}s:11:\"current_row\";i:0;s:8:\"num_rows\";i:1;s:8:\"row_data\";a:155:{s:8:\"entry_id\";s:2:\"12\";s:10:\"channel_id\";s:1:\"2\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:17:\"BRAMOR Orthophoto\";s:9:\"url_title\";s:17:\"bramor-orthophoto\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1363822454\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"03\";s:3:\"day\";s:2:\"21\";s:9:\"edit_date\";s:14:\"20130320235515\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:22:\"Featured Product Pages\";s:12:\"channel_name\";s:22:\"featured_product_pages\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"1\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:1:\"1\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:335:\"The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";s:11:\"field_id_38\";s:0:\"\";s:11:\"field_ft_38\";s:4:\"none\";s:11:\"field_id_39\";s:0:\"\";s:11:\"field_ft_39\";s:4:\"none\";s:11:\"field_id_40\";s:0:\"\";s:11:\"field_ft_40\";s:4:\"none\";s:11:\"field_id_42\";s:0:\"\";s:11:\"field_ft_42\";s:4:\"none\";s:11:\"field_id_43\";s:0:\"\";s:11:\"field_ft_43\";s:4:\"none\";s:11:\"field_id_44\";s:0:\"\";s:11:\"field_ft_44\";s:5:\"xhtml\";s:11:\"field_id_45\";s:0:\"\";s:11:\"field_ft_45\";s:5:\"xhtml\";s:11:\"field_id_46\";s:0:\"\";s:11:\"field_ft_46\";s:5:\"xhtml\";s:11:\"field_id_47\";s:0:\"\";s:11:\"field_ft_47\";s:5:\"xhtml\";s:11:\"field_id_48\";s:0:\"\";s:11:\"field_ft_48\";s:4:\"none\";s:11:\"field_id_49\";s:0:\"\";s:11:\"field_ft_49\";s:4:\"none\";}}s:10:\"cats_fixed\";s:1:\"1\";s:10:\"categories\";a:0:{}}');
INSERT INTO `exp_relationships` VALUES(2, 13, 2, 'channel', '', 'a:3:{s:5:\"query\";O:18:\"CI_DB_mysql_result\":7:{s:7:\"conn_id\";i:0;s:9:\"result_id\";i:0;s:12:\"result_array\";a:1:{i:0;a:155:{s:8:\"entry_id\";s:2:\"13\";s:10:\"channel_id\";s:1:\"2\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:19:\"BRAMOR Orthophoto 2\";s:9:\"url_title\";s:19:\"bramor-orthophoto-2\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1363822493\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"03\";s:3:\"day\";s:2:\"21\";s:9:\"edit_date\";s:14:\"20130321123453\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:22:\"Featured Product Pages\";s:12:\"channel_name\";s:22:\"featured_product_pages\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"2\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:1:\"1\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:335:\"The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";s:11:\"field_id_38\";s:0:\"\";s:11:\"field_ft_38\";s:4:\"none\";s:11:\"field_id_39\";s:0:\"\";s:11:\"field_ft_39\";s:4:\"none\";s:11:\"field_id_40\";s:0:\"\";s:11:\"field_ft_40\";s:4:\"none\";s:11:\"field_id_42\";s:0:\"\";s:11:\"field_ft_42\";s:4:\"none\";s:11:\"field_id_43\";s:0:\"\";s:11:\"field_ft_43\";s:4:\"none\";s:11:\"field_id_44\";s:0:\"\";s:11:\"field_ft_44\";s:5:\"xhtml\";s:11:\"field_id_45\";s:0:\"\";s:11:\"field_ft_45\";s:5:\"xhtml\";s:11:\"field_id_46\";s:0:\"\";s:11:\"field_ft_46\";s:5:\"xhtml\";s:11:\"field_id_47\";s:0:\"\";s:11:\"field_ft_47\";s:5:\"xhtml\";s:11:\"field_id_48\";s:0:\"\";s:11:\"field_ft_48\";s:4:\"none\";s:11:\"field_id_49\";s:0:\"\";s:11:\"field_ft_49\";s:4:\"none\";}}s:13:\"result_object\";a:0:{}s:11:\"current_row\";i:0;s:8:\"num_rows\";i:1;s:8:\"row_data\";a:155:{s:8:\"entry_id\";s:2:\"13\";s:10:\"channel_id\";s:1:\"2\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:19:\"BRAMOR Orthophoto 2\";s:9:\"url_title\";s:19:\"bramor-orthophoto-2\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1363822493\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"03\";s:3:\"day\";s:2:\"21\";s:9:\"edit_date\";s:14:\"20130321123453\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:22:\"Featured Product Pages\";s:12:\"channel_name\";s:22:\"featured_product_pages\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"2\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:1:\"1\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:335:\"The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";s:11:\"field_id_38\";s:0:\"\";s:11:\"field_ft_38\";s:4:\"none\";s:11:\"field_id_39\";s:0:\"\";s:11:\"field_ft_39\";s:4:\"none\";s:11:\"field_id_40\";s:0:\"\";s:11:\"field_ft_40\";s:4:\"none\";s:11:\"field_id_42\";s:0:\"\";s:11:\"field_ft_42\";s:4:\"none\";s:11:\"field_id_43\";s:0:\"\";s:11:\"field_ft_43\";s:4:\"none\";s:11:\"field_id_44\";s:0:\"\";s:11:\"field_ft_44\";s:5:\"xhtml\";s:11:\"field_id_45\";s:0:\"\";s:11:\"field_ft_45\";s:5:\"xhtml\";s:11:\"field_id_46\";s:0:\"\";s:11:\"field_ft_46\";s:5:\"xhtml\";s:11:\"field_id_47\";s:0:\"\";s:11:\"field_ft_47\";s:5:\"xhtml\";s:11:\"field_id_48\";s:0:\"\";s:11:\"field_ft_48\";s:4:\"none\";s:11:\"field_id_49\";s:0:\"\";s:11:\"field_ft_49\";s:4:\"none\";}}s:10:\"cats_fixed\";s:1:\"1\";s:10:\"categories\";a:0:{}}');
INSERT INTO `exp_relationships` VALUES(3, 14, 2, 'channel', '', 'a:3:{s:5:\"query\";O:18:\"CI_DB_mysql_result\":7:{s:7:\"conn_id\";i:0;s:9:\"result_id\";i:0;s:12:\"result_array\";a:1:{i:0;a:155:{s:8:\"entry_id\";s:2:\"14\";s:10:\"channel_id\";s:1:\"2\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:19:\"BRAMOR Orthophoto 3\";s:9:\"url_title\";s:19:\"bramor-orthophoto-3\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1363822446\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"03\";s:3:\"day\";s:2:\"21\";s:9:\"edit_date\";s:14:\"20130321123406\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:22:\"Featured Product Pages\";s:12:\"channel_name\";s:22:\"featured_product_pages\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"3\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:1:\"1\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:335:\"The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";s:11:\"field_id_38\";s:0:\"\";s:11:\"field_ft_38\";s:4:\"none\";s:11:\"field_id_39\";s:0:\"\";s:11:\"field_ft_39\";s:4:\"none\";s:11:\"field_id_40\";s:0:\"\";s:11:\"field_ft_40\";s:4:\"none\";s:11:\"field_id_42\";s:0:\"\";s:11:\"field_ft_42\";s:4:\"none\";s:11:\"field_id_43\";s:0:\"\";s:11:\"field_ft_43\";s:4:\"none\";s:11:\"field_id_44\";s:0:\"\";s:11:\"field_ft_44\";s:5:\"xhtml\";s:11:\"field_id_45\";s:0:\"\";s:11:\"field_ft_45\";s:5:\"xhtml\";s:11:\"field_id_46\";s:0:\"\";s:11:\"field_ft_46\";s:5:\"xhtml\";s:11:\"field_id_47\";s:0:\"\";s:11:\"field_ft_47\";s:5:\"xhtml\";s:11:\"field_id_48\";s:0:\"\";s:11:\"field_ft_48\";s:4:\"none\";s:11:\"field_id_49\";s:0:\"\";s:11:\"field_ft_49\";s:4:\"none\";}}s:13:\"result_object\";a:0:{}s:11:\"current_row\";i:0;s:8:\"num_rows\";i:1;s:8:\"row_data\";a:155:{s:8:\"entry_id\";s:2:\"14\";s:10:\"channel_id\";s:1:\"2\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:19:\"BRAMOR Orthophoto 3\";s:9:\"url_title\";s:19:\"bramor-orthophoto-3\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1363822446\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"03\";s:3:\"day\";s:2:\"21\";s:9:\"edit_date\";s:14:\"20130321123406\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:22:\"Featured Product Pages\";s:12:\"channel_name\";s:22:\"featured_product_pages\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"3\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:1:\"1\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:335:\"The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";s:11:\"field_id_38\";s:0:\"\";s:11:\"field_ft_38\";s:4:\"none\";s:11:\"field_id_39\";s:0:\"\";s:11:\"field_ft_39\";s:4:\"none\";s:11:\"field_id_40\";s:0:\"\";s:11:\"field_ft_40\";s:4:\"none\";s:11:\"field_id_42\";s:0:\"\";s:11:\"field_ft_42\";s:4:\"none\";s:11:\"field_id_43\";s:0:\"\";s:11:\"field_ft_43\";s:4:\"none\";s:11:\"field_id_44\";s:0:\"\";s:11:\"field_ft_44\";s:5:\"xhtml\";s:11:\"field_id_45\";s:0:\"\";s:11:\"field_ft_45\";s:5:\"xhtml\";s:11:\"field_id_46\";s:0:\"\";s:11:\"field_ft_46\";s:5:\"xhtml\";s:11:\"field_id_47\";s:0:\"\";s:11:\"field_ft_47\";s:5:\"xhtml\";s:11:\"field_id_48\";s:0:\"\";s:11:\"field_ft_48\";s:4:\"none\";s:11:\"field_id_49\";s:0:\"\";s:11:\"field_ft_49\";s:4:\"none\";}}s:10:\"cats_fixed\";s:1:\"1\";s:10:\"categories\";a:0:{}}');
INSERT INTO `exp_relationships` VALUES(4, 15, 2, 'channel', '', 'a:3:{s:5:\"query\";O:18:\"CI_DB_mysql_result\":7:{s:7:\"conn_id\";i:0;s:9:\"result_id\";i:0;s:12:\"result_array\";a:1:{i:0;a:155:{s:8:\"entry_id\";s:2:\"15\";s:10:\"channel_id\";s:1:\"2\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:19:\"BRAMOR Orthophoto 4\";s:9:\"url_title\";s:19:\"bramor-orthophoto-4\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1363822463\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"03\";s:3:\"day\";s:2:\"21\";s:9:\"edit_date\";s:14:\"20130321123423\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:22:\"Featured Product Pages\";s:12:\"channel_name\";s:22:\"featured_product_pages\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"4\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:1:\"1\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:335:\"The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";s:11:\"field_id_38\";s:0:\"\";s:11:\"field_ft_38\";s:4:\"none\";s:11:\"field_id_39\";s:0:\"\";s:11:\"field_ft_39\";s:4:\"none\";s:11:\"field_id_40\";s:0:\"\";s:11:\"field_ft_40\";s:4:\"none\";s:11:\"field_id_42\";s:0:\"\";s:11:\"field_ft_42\";s:4:\"none\";s:11:\"field_id_43\";s:0:\"\";s:11:\"field_ft_43\";s:4:\"none\";s:11:\"field_id_44\";s:0:\"\";s:11:\"field_ft_44\";s:5:\"xhtml\";s:11:\"field_id_45\";s:0:\"\";s:11:\"field_ft_45\";s:5:\"xhtml\";s:11:\"field_id_46\";s:0:\"\";s:11:\"field_ft_46\";s:5:\"xhtml\";s:11:\"field_id_47\";s:0:\"\";s:11:\"field_ft_47\";s:5:\"xhtml\";s:11:\"field_id_48\";s:0:\"\";s:11:\"field_ft_48\";s:4:\"none\";s:11:\"field_id_49\";s:0:\"\";s:11:\"field_ft_49\";s:4:\"none\";}}s:13:\"result_object\";a:0:{}s:11:\"current_row\";i:0;s:8:\"num_rows\";i:1;s:8:\"row_data\";a:155:{s:8:\"entry_id\";s:2:\"15\";s:10:\"channel_id\";s:1:\"2\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:19:\"BRAMOR Orthophoto 4\";s:9:\"url_title\";s:19:\"bramor-orthophoto-4\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1363822463\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"03\";s:3:\"day\";s:2:\"21\";s:9:\"edit_date\";s:14:\"20130321123423\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:22:\"Featured Product Pages\";s:12:\"channel_name\";s:22:\"featured_product_pages\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"4\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:1:\"1\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:335:\"The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";s:11:\"field_id_38\";s:0:\"\";s:11:\"field_ft_38\";s:4:\"none\";s:11:\"field_id_39\";s:0:\"\";s:11:\"field_ft_39\";s:4:\"none\";s:11:\"field_id_40\";s:0:\"\";s:11:\"field_ft_40\";s:4:\"none\";s:11:\"field_id_42\";s:0:\"\";s:11:\"field_ft_42\";s:4:\"none\";s:11:\"field_id_43\";s:0:\"\";s:11:\"field_ft_43\";s:4:\"none\";s:11:\"field_id_44\";s:0:\"\";s:11:\"field_ft_44\";s:5:\"xhtml\";s:11:\"field_id_45\";s:0:\"\";s:11:\"field_ft_45\";s:5:\"xhtml\";s:11:\"field_id_46\";s:0:\"\";s:11:\"field_ft_46\";s:5:\"xhtml\";s:11:\"field_id_47\";s:0:\"\";s:11:\"field_ft_47\";s:5:\"xhtml\";s:11:\"field_id_48\";s:0:\"\";s:11:\"field_ft_48\";s:4:\"none\";s:11:\"field_id_49\";s:0:\"\";s:11:\"field_ft_49\";s:4:\"none\";}}s:10:\"cats_fixed\";s:1:\"1\";s:10:\"categories\";a:0:{}}');
INSERT INTO `exp_relationships` VALUES(5, 2, 29, 'channel', 'a:3:{s:5:\"query\";O:18:\"CI_DB_mysql_result\":7:{s:7:\"conn_id\";i:0;s:9:\"result_id\";i:0;s:12:\"result_array\";a:1:{i:0;a:155:{s:8:\"entry_id\";s:2:\"29\";s:10:\"channel_id\";s:2:\"11\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:6:\"Topcon\";s:9:\"url_title\";s:6:\"topcon\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1365731435\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"04\";s:3:\"day\";s:2:\"12\";s:9:\"edit_date\";s:14:\"20130412145035\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:13:\"Manufacturers\";s:12:\"channel_name\";s:13:\"manufacturers\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"0\";s:11:\"field_ft_16\";N;s:11:\"field_id_17\";s:0:\"\";s:11:\"field_ft_17\";N;s:11:\"field_id_18\";s:0:\"\";s:11:\"field_ft_18\";N;s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";N;s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";N;s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";N;s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";N;s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";N;s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";N;s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";N;s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";N;s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";N;s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";N;s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";N;s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";N;s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";N;s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";N;s:11:\"field_id_34\";s:29:\"{filedir_10}footer-topcon.gif\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:27:\"{filedir_10}topcon-logo.gif\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:46:\"{filedir_10}mega-menu-feature-product-logo.jpg\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";s:11:\"field_id_38\";s:0:\"\";s:11:\"field_ft_38\";s:4:\"none\";s:11:\"field_id_39\";s:0:\"\";s:11:\"field_ft_39\";s:4:\"none\";s:11:\"field_id_40\";s:0:\"\";s:11:\"field_ft_40\";s:4:\"none\";s:11:\"field_id_42\";s:0:\"\";s:11:\"field_ft_42\";s:4:\"none\";s:11:\"field_id_43\";s:0:\"\";s:11:\"field_ft_43\";s:4:\"none\";s:11:\"field_id_44\";s:0:\"\";s:11:\"field_ft_44\";s:5:\"xhtml\";s:11:\"field_id_45\";s:0:\"\";s:11:\"field_ft_45\";s:5:\"xhtml\";s:11:\"field_id_46\";s:0:\"\";s:11:\"field_ft_46\";s:5:\"xhtml\";s:11:\"field_id_47\";s:0:\"\";s:11:\"field_ft_47\";s:5:\"xhtml\";s:11:\"field_id_48\";s:0:\"\";s:11:\"field_ft_48\";s:4:\"none\";s:11:\"field_id_49\";s:0:\"\";s:11:\"field_ft_49\";s:4:\"none\";}}s:13:\"result_object\";a:0:{}s:11:\"current_row\";i:0;s:8:\"num_rows\";i:1;s:8:\"row_data\";a:155:{s:8:\"entry_id\";s:2:\"29\";s:10:\"channel_id\";s:2:\"11\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:6:\"Topcon\";s:9:\"url_title\";s:6:\"topcon\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1365731435\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"04\";s:3:\"day\";s:2:\"12\";s:9:\"edit_date\";s:14:\"20130412145035\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:13:\"Manufacturers\";s:12:\"channel_name\";s:13:\"manufacturers\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"0\";s:11:\"field_ft_16\";N;s:11:\"field_id_17\";s:0:\"\";s:11:\"field_ft_17\";N;s:11:\"field_id_18\";s:0:\"\";s:11:\"field_ft_18\";N;s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";N;s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";N;s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";N;s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";N;s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";N;s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";N;s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";N;s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";N;s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";N;s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";N;s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";N;s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";N;s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";N;s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";N;s:11:\"field_id_34\";s:29:\"{filedir_10}footer-topcon.gif\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:27:\"{filedir_10}topcon-logo.gif\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:46:\"{filedir_10}mega-menu-feature-product-logo.jpg\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";s:11:\"field_id_38\";s:0:\"\";s:11:\"field_ft_38\";s:4:\"none\";s:11:\"field_id_39\";s:0:\"\";s:11:\"field_ft_39\";s:4:\"none\";s:11:\"field_id_40\";s:0:\"\";s:11:\"field_ft_40\";s:4:\"none\";s:11:\"field_id_42\";s:0:\"\";s:11:\"field_ft_42\";s:4:\"none\";s:11:\"field_id_43\";s:0:\"\";s:11:\"field_ft_43\";s:4:\"none\";s:11:\"field_id_44\";s:0:\"\";s:11:\"field_ft_44\";s:5:\"xhtml\";s:11:\"field_id_45\";s:0:\"\";s:11:\"field_ft_45\";s:5:\"xhtml\";s:11:\"field_id_46\";s:0:\"\";s:11:\"field_ft_46\";s:5:\"xhtml\";s:11:\"field_id_47\";s:0:\"\";s:11:\"field_ft_47\";s:5:\"xhtml\";s:11:\"field_id_48\";s:0:\"\";s:11:\"field_ft_48\";s:4:\"none\";s:11:\"field_id_49\";s:0:\"\";s:11:\"field_ft_49\";s:4:\"none\";}}s:10:\"cats_fixed\";s:1:\"1\";s:10:\"categories\";a:0:{}}', '');

/* Table structure for table `exp_remember_me` */
DROP TABLE IF EXISTS `exp_remember_me`;

CREATE TABLE `exp_remember_me` (
  `remember_me_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) DEFAULT '0',
  `ip_address` varchar(45) DEFAULT '0',
  `user_agent` varchar(120) DEFAULT '',
  `admin_sess` tinyint(1) DEFAULT '0',
  `site_id` int(4) DEFAULT '1',
  `expiration` int(10) DEFAULT '0',
  `last_refresh` int(10) DEFAULT '0',
  PRIMARY KEY (`remember_me_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_reset_password` */
DROP TABLE IF EXISTS `exp_reset_password`;

CREATE TABLE `exp_reset_password` (
  `reset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL,
  `resetcode` varchar(12) NOT NULL,
  `date` int(10) NOT NULL,
  PRIMARY KEY (`reset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_revision_tracker` */
DROP TABLE IF EXISTS `exp_revision_tracker`;

CREATE TABLE `exp_revision_tracker` (
  `tracker_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `item_table` varchar(20) NOT NULL,
  `item_field` varchar(20) NOT NULL,
  `item_date` int(10) NOT NULL,
  `item_author_id` int(10) unsigned NOT NULL,
  `item_data` mediumtext NOT NULL,
  PRIMARY KEY (`tracker_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_rte_tools` */
DROP TABLE IF EXISTS `exp_rte_tools`;

CREATE TABLE `exp_rte_tools` (
  `tool_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(75) DEFAULT NULL,
  `class` varchar(75) DEFAULT NULL,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`tool_id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_rte_tools` */
INSERT INTO `exp_rte_tools` VALUES(1, 'Blockquote', 'Blockquote_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(2, 'Bold', 'Bold_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(3, 'Headings', 'Headings_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(4, 'Image', 'Image_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(5, 'Italic', 'Italic_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(6, 'Link', 'Link_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(7, 'Ordered List', 'Ordered_list_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(8, 'Underline', 'Underline_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(9, 'Unordered List', 'Unordered_list_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(10, 'View Source', 'View_source_rte', 'y');

/* Table structure for table `exp_rte_toolsets` */
DROP TABLE IF EXISTS `exp_rte_toolsets`;

CREATE TABLE `exp_rte_toolsets` (
  `toolset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `tools` text,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`toolset_id`),
  KEY `member_id` (`member_id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_rte_toolsets` */
INSERT INTO `exp_rte_toolsets` VALUES(1, 0, 'Default', '3|2|5|1|9|7|6|4|10', 'y');

/* Table structure for table `exp_search` */
DROP TABLE IF EXISTS `exp_search`;

CREATE TABLE `exp_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) NOT NULL DEFAULT '1',
  `search_date` int(10) NOT NULL,
  `keywords` varchar(60) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(6) NOT NULL,
  `per_page` tinyint(3) unsigned NOT NULL,
  `query` mediumtext,
  `custom_fields` mediumtext,
  `result_page` varchar(70) NOT NULL,
  PRIMARY KEY (`search_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/* dumping data for table `exp_search` */
INSERT INTO `exp_search` VALUES('aa85b41a2610678266276ea6b01bc80a', 1, 1366592166, 'top', 0, '127.0.0.1', 3, 50, 's:1416:\\\"SELECT DISTINCT(t.entry_id), t.entry_id, t.channel_id, t.forum_topic_id, t.author_id, t.ip_address, t.title, t.url_title, t.status, t.dst_enabled, t.view_count_one, t.view_count_two, t.view_count_three, t.view_count_four, t.allow_comments, t.comment_expiration_date, t.sticky, t.entry_date, t.year, t.month, t.day, t.entry_date, t.edit_date, t.expiration_date, t.recent_comment_date, t.comment_total, t.site_id as entry_site_id,\n				w.channel_title, w.channel_name, w.search_results_url, w.search_excerpt, w.channel_url, w.comment_url, w.comment_moderate, w.channel_html_formatting, w.channel_allow_img_urls, w.channel_auto_link_urls, w.comment_system_enabled, \n				m.username, m.email, m.url, m.screen_name, m.location, m.occupation, m.interests, m.aol_im, m.yahoo_im, m.msn_im, m.icq, m.signature, m.sig_img_filename, m.sig_img_width, m.sig_img_height, m.avatar_filename, m.avatar_width, m.avatar_height, m.photo_filename, m.photo_width, m.photo_height, m.group_id, m.member_id, m.bday_d, m.bday_m, m.bday_y, m.bio,\n				md.*,\n				wd.*\n			FROM MDBMPREFIXchannel_titles		AS t\n			LEFT JOIN MDBMPREFIXchannels 		AS w  ON t.channel_id = w.channel_id \n			LEFT JOIN MDBMPREFIXchannel_data	AS wd ON t.entry_id = wd.entry_id \n			LEFT JOIN MDBMPREFIXmembers		AS m  ON m.member_id = t.author_id \n			LEFT JOIN MDBMPREFIXmember_data	AS md ON md.member_id = m.member_id \n			WHERE t.entry_id IN (1,2,3)  ORDER BY entry_date  desc\\\";', 'a:16:{s:14:\\\"product_prices\\\";a:2:{i:0;s:1:\\\"1\\\";i:1;s:1:\\\"n\\\";}s:19:\\\"product_description\\\";a:2:{i:0;s:1:\\\"2\\\";i:1;s:1:\\\"n\\\";}s:24:\\\"technical_specifications\\\";a:2:{i:0;s:1:\\\"3\\\";i:1;s:1:\\\"n\\\";}s:14:\\\"product_videos\\\";a:2:{i:0;s:1:\\\"4\\\";i:1;s:1:\\\"n\\\";}s:15:\\\"rental_sections\\\";a:2:{i:0;s:1:\\\"5\\\";i:1;s:1:\\\"n\\\";}s:17:\\\"rental_pricing_nz\\\";a:2:{i:0;s:1:\\\"6\\\";i:1;s:1:\\\"n\\\";}s:17:\\\"rental_pricing_au\\\";a:2:{i:0;s:1:\\\"7\\\";i:1;s:1:\\\"n\\\";}s:24:\\\"featured_carousel_images\\\";a:2:{i:0;s:1:\\\"8\\\";i:1;s:1:\\\"n\\\";}s:19:\\\"is_featured_product\\\";a:2:{i:0;s:1:\\\"9\\\";i:1;s:1:\\\"y\\\";}s:14:\\\"product_images\\\";a:2:{i:0;s:2:\\\"10\\\";i:1;s:1:\\\"n\\\";}s:17:\\\"product_downloads\\\";a:2:{i:0;s:2:\\\"11\\\";i:1;s:1:\\\"n\\\";}s:15:\\\"service_content\\\";a:2:{i:0;s:2:\\\"26\\\";i:1;s:1:\\\"n\\\";}s:15:\\\"services_banner\\\";a:2:{i:0;s:2:\\\"27\\\";i:1;s:1:\\\"n\\\";}s:22:\\\"featured_megamenu_icon\\\";a:2:{i:0;s:2:\\\"33\\\";i:1;s:1:\\\"y\\\";}s:12:\\\"manufacturer\\\";a:2:{i:0;s:2:\\\"37\\\";i:1;s:1:\\\"y\\\";}s:16:\\\"services_country\\\";a:2:{i:0;s:2:\\\"47\\\";i:1;s:1:\\\"y\\\";}}', 'search/results');

/* Table structure for table `exp_search_log` */
DROP TABLE IF EXISTS `exp_search_log`;

CREATE TABLE `exp_search_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `search_date` int(10) NOT NULL,
  `search_type` varchar(32) NOT NULL,
  `search_terms` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_search_log` */
INSERT INTO `exp_search_log` VALUES(1, 1, 1, '96black', '127.0.0.1', 1365964656, 'site', 'top');
INSERT INTO `exp_search_log` VALUES(2, 1, 1, '96black', '127.0.0.1', 1365964773, 'site', 'top');
INSERT INTO `exp_search_log` VALUES(3, 1, 1, '96black', '127.0.0.1', 1365964785, 'site', 'tes');
INSERT INTO `exp_search_log` VALUES(4, 1, 1, '96black', '127.0.0.1', 1365964789, 'site', 'top');
INSERT INTO `exp_search_log` VALUES(5, 1, 1, '96black', '127.0.0.1', 1365965109, 'site', 'topc');
INSERT INTO `exp_search_log` VALUES(6, 1, 1, '96black', '127.0.0.1', 1365965121, 'site', 'topc');
INSERT INTO `exp_search_log` VALUES(7, 1, 1, '96black', '127.0.0.1', 1366071662, 'site', 'top');
INSERT INTO `exp_search_log` VALUES(8, 1, 1, '96black', '127.0.0.1', 1366076403, 'site', 'topcon');
INSERT INTO `exp_search_log` VALUES(9, 1, 0, '', '127.0.0.1', 1366095780, 'site', 'top');
INSERT INTO `exp_search_log` VALUES(10, 1, 1, '96black', '127.0.0.1', 1366097504, 'site', 'Survey &amp; Design');
INSERT INTO `exp_search_log` VALUES(11, 1, 1, '96black', '127.0.0.1', 1366097728, 'site', 'surv');
INSERT INTO `exp_search_log` VALUES(12, 1, 1, '96black', '127.0.0.1', 1366097768, 'site', 'surv');
INSERT INTO `exp_search_log` VALUES(13, 1, 1, '96black', '127.0.0.1', 1366104729, 'site', 'survey');
INSERT INTO `exp_search_log` VALUES(14, 1, 1, '96black', '127.0.0.1', 1366154684, 'site', 'topcon');
INSERT INTO `exp_search_log` VALUES(15, 1, 1, '96black', '127.0.0.1', 1366154698, 'site', 'top');
INSERT INTO `exp_search_log` VALUES(16, 1, 1, '96black', '127.0.0.1', 1366154706, 'site', 'survey');
INSERT INTO `exp_search_log` VALUES(17, 1, 1, '96black', '127.0.0.1', 1366154711, 'site', 'top');
INSERT INTO `exp_search_log` VALUES(18, 1, 0, '', '127.0.0.1', 1366592166, 'site', 'top');

/* Table structure for table `exp_security_hashes` */
DROP TABLE IF EXISTS `exp_security_hashes`;

CREATE TABLE `exp_security_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `hash` varchar(40) NOT NULL,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB AUTO_INCREMENT=6880 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_security_hashes` */
INSERT INTO `exp_security_hashes` VALUES(6517, 1366576844, '127.0.0.1', '2cd179d3fdd613aeab88ab9399735087554f10b7');
INSERT INTO `exp_security_hashes` VALUES(6518, 1366576844, '127.0.0.1', '223b3eafa5335f92707bce63588d2e22a0524587');
INSERT INTO `exp_security_hashes` VALUES(6519, 1366576845, '127.0.0.1', '7541d152c3b386105b7d66c47cfdb7f5d672cf31');
INSERT INTO `exp_security_hashes` VALUES(6520, 1366577005, '127.0.0.1', 'b8175575a90951590d6f94fd3ef77aa7d74c8be7');
INSERT INTO `exp_security_hashes` VALUES(6521, 1366577006, '127.0.0.1', '6e08e7df7860d70ba0262de7ec6b947e7bd76d77');
INSERT INTO `exp_security_hashes` VALUES(6522, 1366577007, '127.0.0.1', 'fff990bbf8d1fe8dcb939628a5565a857684f668');
INSERT INTO `exp_security_hashes` VALUES(6523, 1366577240, '127.0.0.1', 'fda1a79842afba239589d5904cfc154ddba0c3a3');
INSERT INTO `exp_security_hashes` VALUES(6524, 1366577240, '127.0.0.1', '8e75d44f09fefc63e9c1a3325bd8a49faac47b82');
INSERT INTO `exp_security_hashes` VALUES(6525, 1366577241, '127.0.0.1', '2b3d000535e906acb698a816a9145313201f4a56');
INSERT INTO `exp_security_hashes` VALUES(6526, 1366577261, '127.0.0.1', '820a12e9bc89d95cc5b68b4ec30a278ad3240556');
INSERT INTO `exp_security_hashes` VALUES(6527, 1366577262, '127.0.0.1', '040318af5c8ef955983b8b14496bcb1786b2a87d');
INSERT INTO `exp_security_hashes` VALUES(6528, 1366577263, '127.0.0.1', '8eb02c4010e90fc9cd5b1f2b38b2f75e0aa9b5b6');
INSERT INTO `exp_security_hashes` VALUES(6529, 1366577264, '127.0.0.1', 'cb8006506917a8b310efdb1c7736dfb187956ec8');
INSERT INTO `exp_security_hashes` VALUES(6530, 1366577264, '127.0.0.1', '6abd50cfb595838f70251344d71a97f606fa3da0');
INSERT INTO `exp_security_hashes` VALUES(6531, 1366577264, '127.0.0.1', '23885d481e4155c68b788eff1b510bc96b467d81');
INSERT INTO `exp_security_hashes` VALUES(6532, 1366577265, '127.0.0.1', '483a5ac2b1a9f554257be8d93b814a76fd642477');
INSERT INTO `exp_security_hashes` VALUES(6533, 1366577265, '127.0.0.1', '00a65ab8b1573154503068e73af82f0841606e73');
INSERT INTO `exp_security_hashes` VALUES(6534, 1366577299, '127.0.0.1', '222796451bd4b5c1f016476dce11e499522d2a2a');
INSERT INTO `exp_security_hashes` VALUES(6535, 1366577299, '127.0.0.1', 'a74cee9f25cbf5f4803183752dcb09a8b81aff41');
INSERT INTO `exp_security_hashes` VALUES(6536, 1366577300, '127.0.0.1', '83fc9ea600d8efa343fb4b3e54d6773d9db04722');
INSERT INTO `exp_security_hashes` VALUES(6537, 1366577300, '127.0.0.1', 'b062721011c179b90ab6b70040d2b794e97c82fd');
INSERT INTO `exp_security_hashes` VALUES(6538, 1366577300, '127.0.0.1', '7b04eb1989523d04de2a691239a89949e77e5a2e');
INSERT INTO `exp_security_hashes` VALUES(6539, 1366577301, '127.0.0.1', '534fe55f3c527578c31c5e53370aafee9ae76d22');
INSERT INTO `exp_security_hashes` VALUES(6540, 1366577302, '127.0.0.1', '6065002d776dc7b51ec93bc5e4c98b7d61d5fd28');
INSERT INTO `exp_security_hashes` VALUES(6541, 1366577303, '127.0.0.1', 'a1fe0b7ea6307f9c00e9eb6b23c1e907fe42a7fe');
INSERT INTO `exp_security_hashes` VALUES(6542, 1366577303, '127.0.0.1', 'cdefebd5121f9007b5684767604cd6cd32f3534d');
INSERT INTO `exp_security_hashes` VALUES(6543, 1366577304, '127.0.0.1', 'bbdb2a24f9ce4374ccce4da64511ed83fb83ff63');
INSERT INTO `exp_security_hashes` VALUES(6544, 1366577306, '127.0.0.1', '066c690ab5dff2d5a16751050cb3d7a7fe4f91e9');
INSERT INTO `exp_security_hashes` VALUES(6545, 1366577680, '127.0.0.1', 'c21642c10aec0661be0e116a3066f0f5c6bc6796');
INSERT INTO `exp_security_hashes` VALUES(6546, 1366577680, '127.0.0.1', '3bfac3218739af41bd2239f8bb00c9a5b27e372e');
INSERT INTO `exp_security_hashes` VALUES(6547, 1366577680, '127.0.0.1', '8333aab394f8ab56c1d3dbf9742ac22e439de94d');
INSERT INTO `exp_security_hashes` VALUES(6548, 1366577681, '127.0.0.1', '87fea461adaa2eb3099d31b492ac331556acc0f9');
INSERT INTO `exp_security_hashes` VALUES(6549, 1366577681, '127.0.0.1', '726d20f8faa6b92faad8e37f82e41d304015d90b');
INSERT INTO `exp_security_hashes` VALUES(6550, 1366577684, '127.0.0.1', '8496fbc476a0cd654d8cbdf11d6dd3beb6212f2c');
INSERT INTO `exp_security_hashes` VALUES(6551, 1366577684, '127.0.0.1', '690b16a41c09c33ac6fe641924fa6ab24ee2d41c');
INSERT INTO `exp_security_hashes` VALUES(6552, 1366577684, '127.0.0.1', '495270b9b039377b3ab41d3c8be79d1acdc01cb7');
INSERT INTO `exp_security_hashes` VALUES(6553, 1366577685, '127.0.0.1', '20552f778113b6d7957690816fedae53cbfd09ac');
INSERT INTO `exp_security_hashes` VALUES(6554, 1366577685, '127.0.0.1', 'f5879e44b10bac78096815d7f288c61da0d3d1a7');
INSERT INTO `exp_security_hashes` VALUES(6555, 1366577687, '127.0.0.1', '84447ec46c8c8dd73b7a6956996ffc358bfeddae');
INSERT INTO `exp_security_hashes` VALUES(6556, 1366577690, '127.0.0.1', '6bf39fcf525c30992bde5b0561ac892815726474');
INSERT INTO `exp_security_hashes` VALUES(6557, 1366577690, '127.0.0.1', 'ecb5306df88acf02cb2fa4d234aea39b065fed91');
INSERT INTO `exp_security_hashes` VALUES(6558, 1366577690, '127.0.0.1', 'a6da31cf826154937dee57cffc07af1121edd1d1');
INSERT INTO `exp_security_hashes` VALUES(6559, 1366577690, '127.0.0.1', 'ab6e8481eaacdaf1c6de7e44a9fc820f3ed4ade7');
INSERT INTO `exp_security_hashes` VALUES(6560, 1366577691, '127.0.0.1', '377ae46f0b714a1a8152ea64758edf9206e474c9');
INSERT INTO `exp_security_hashes` VALUES(6561, 1366577693, '127.0.0.1', '78f96befd0eefc979fffa1e9d25bb7e206eecc7e');
INSERT INTO `exp_security_hashes` VALUES(6562, 1366577693, '127.0.0.1', '97a1037d3e454678e1d848a2b2dbb51ac19cccf4');
INSERT INTO `exp_security_hashes` VALUES(6563, 1366577693, '127.0.0.1', 'aaf88e83047016bf509ed021317eface79e4fa15');
INSERT INTO `exp_security_hashes` VALUES(6564, 1366577694, '127.0.0.1', '07e276af60c5b34b45e5daa09cf2e6f5a8697eca');
INSERT INTO `exp_security_hashes` VALUES(6565, 1366577694, '127.0.0.1', '2a02bcb070950f261db77d727629e1b29bdc8daa');
INSERT INTO `exp_security_hashes` VALUES(6566, 1366577713, '127.0.0.1', '697b27b4202aab9b52f33db4d6c9f706e102345e');
INSERT INTO `exp_security_hashes` VALUES(6567, 1366577713, '127.0.0.1', 'a796e76c25c88ca255bd846a14578861e43e8573');
INSERT INTO `exp_security_hashes` VALUES(6568, 1366577713, '127.0.0.1', '92ee487b8fbf95c54094f0a92a69c7fbdcc2a600');
INSERT INTO `exp_security_hashes` VALUES(6569, 1366577713, '127.0.0.1', 'dc52d94de186c7a6f50dfac3f8f2e201e9b43303');
INSERT INTO `exp_security_hashes` VALUES(6570, 1366577714, '127.0.0.1', '9d828462adcb2780627d60af2016e00160b2c9cf');
INSERT INTO `exp_security_hashes` VALUES(6571, 1366577779, '127.0.0.1', 'b049f5d5ed8cc5b270b593ce1d5b1f1970d0f473');
INSERT INTO `exp_security_hashes` VALUES(6572, 1366577779, '127.0.0.1', '10c2f39e247da3f89887e86df430e6b2d3f883c4');
INSERT INTO `exp_security_hashes` VALUES(6573, 1366577779, '127.0.0.1', '8cb6f2d107631ec0eb42e7defaf6241799dd122b');
INSERT INTO `exp_security_hashes` VALUES(6574, 1366577780, '127.0.0.1', '8dcb3209be147acecec826158d72ee8f3b43abef');
INSERT INTO `exp_security_hashes` VALUES(6575, 1366577780, '127.0.0.1', 'f61055dcea68fc605d93e43ab4b3933d72e0e928');
INSERT INTO `exp_security_hashes` VALUES(6576, 1366577780, '127.0.0.1', '2f61e18e0742757044f6dfa7372be9d09cbf38e5');
INSERT INTO `exp_security_hashes` VALUES(6577, 1366577780, '127.0.0.1', 'dff46c3bfdbcf0d751020aa6ffc03eba870ef5de');
INSERT INTO `exp_security_hashes` VALUES(6578, 1366577781, '127.0.0.1', 'acf300bc56c71aaac66f6604bfdc7181c45c7bec');
INSERT INTO `exp_security_hashes` VALUES(6579, 1366577989, '127.0.0.1', '979f24b56741179b324993e1f77193a02edc3d91');
INSERT INTO `exp_security_hashes` VALUES(6580, 1366577989, '127.0.0.1', '665d52645a341515ef7ed94f3606bf9bd9b3880d');
INSERT INTO `exp_security_hashes` VALUES(6581, 1366577989, '127.0.0.1', '121d45eca684f8dbc605aafcfbd2cf5d7fc750b3');
INSERT INTO `exp_security_hashes` VALUES(6582, 1366577990, '127.0.0.1', '6fc7afdb1e1733f5a7c052bdb1d2a587c03f0f06');
INSERT INTO `exp_security_hashes` VALUES(6583, 1366577990, '127.0.0.1', '8977782f40d37b3377ab74aafacd88b8a3116450');
INSERT INTO `exp_security_hashes` VALUES(6584, 1366578205, '127.0.0.1', '1339d3683c99945893d439db0215e2a5b13df17a');
INSERT INTO `exp_security_hashes` VALUES(6585, 1366578205, '127.0.0.1', '1beb5731adaaffbf28f1946fba43c7203d199bf9');
INSERT INTO `exp_security_hashes` VALUES(6586, 1366578206, '127.0.0.1', '013f17bfc44c406be92c8d07964177cba62dae54');
INSERT INTO `exp_security_hashes` VALUES(6587, 1366578248, '127.0.0.1', 'ae1e1f45d71b8f6f3483e6cfcb51c91d9037dfb0');
INSERT INTO `exp_security_hashes` VALUES(6588, 1366578249, '127.0.0.1', 'c3ea19c116404a136e0aff2202fa1412e7f5c534');
INSERT INTO `exp_security_hashes` VALUES(6589, 1366578249, '127.0.0.1', '26ab247eab2b3a59dd726be6e8682d6b0184960a');
INSERT INTO `exp_security_hashes` VALUES(6590, 1366578251, '127.0.0.1', '1c295b2be7842335bf53b8d64ee38202c63846a5');
INSERT INTO `exp_security_hashes` VALUES(6591, 1366578251, '127.0.0.1', 'a4bd731b7ac15c7b1e1d36614a010e8829c4cf08');
INSERT INTO `exp_security_hashes` VALUES(6592, 1366578252, '127.0.0.1', 'c1791e904e74c4d447a0559ac55ba552961d92c8');
INSERT INTO `exp_security_hashes` VALUES(6593, 1366578252, '127.0.0.1', '22d8953a7e9f7505096ad26d9bd9242583576de6');
INSERT INTO `exp_security_hashes` VALUES(6594, 1366578253, '127.0.0.1', 'dc93cb8f5f9361398f93d643f5cd98cfbc939f18');
INSERT INTO `exp_security_hashes` VALUES(6595, 1366578253, '127.0.0.1', '1c30ce49fbe6478741d2927bac1d248afb4133a7');
INSERT INTO `exp_security_hashes` VALUES(6596, 1366578254, '127.0.0.1', '5bf15de6de0abe43d3ad75a0952744d525bbd211');
INSERT INTO `exp_security_hashes` VALUES(6597, 1366578254, '127.0.0.1', '99be774c16bba323fb04c503ca9cbdf09fa6e07e');
INSERT INTO `exp_security_hashes` VALUES(6598, 1366578255, '127.0.0.1', '615f01db73adfb3415d4b51bf959b13760eebe61');
INSERT INTO `exp_security_hashes` VALUES(6599, 1366578255, '127.0.0.1', 'bb7fd9ffa623284d086b9471096091f388e580d5');
INSERT INTO `exp_security_hashes` VALUES(6600, 1366578256, '127.0.0.1', '4f798645665932e4f0af3dd013c15bd13332d89e');
INSERT INTO `exp_security_hashes` VALUES(6601, 1366578258, '127.0.0.1', 'a059424c058036e0782922404f78d54128b1592e');
INSERT INTO `exp_security_hashes` VALUES(6602, 1366578258, '127.0.0.1', '55cc301145e389181ee707d814b71263b0f5ce97');
INSERT INTO `exp_security_hashes` VALUES(6603, 1366578258, '127.0.0.1', '28e95c9f541d3afbbb6607cdffefb34d1e9b7c07');
INSERT INTO `exp_security_hashes` VALUES(6604, 1366578258, '127.0.0.1', '747b860eb910ff812988d74f9fab6e8076edf33e');
INSERT INTO `exp_security_hashes` VALUES(6605, 1366578259, '127.0.0.1', 'b0b650d210449f998d272d02d19ef0c3ea1a72e1');
INSERT INTO `exp_security_hashes` VALUES(6606, 1366578260, '127.0.0.1', '50ea6423e228c6aedfd8e67cf29c5e856346844e');
INSERT INTO `exp_security_hashes` VALUES(6607, 1366578260, '127.0.0.1', '2a48560b90cdb5ec3d5f3a5e9773bbd203a49dec');
INSERT INTO `exp_security_hashes` VALUES(6608, 1366578260, '127.0.0.1', 'fe6405ce19b2a38ac7100a9351cb82db731bf59b');
INSERT INTO `exp_security_hashes` VALUES(6609, 1366578261, '127.0.0.1', 'f095e0164746f7eedc81fb4382453390acd4f21d');
INSERT INTO `exp_security_hashes` VALUES(6610, 1366578261, '127.0.0.1', 'be3fdd3cf890ff31d282d5095343da13bb4a8d90');
INSERT INTO `exp_security_hashes` VALUES(6611, 1366578263, '127.0.0.1', '101e025ec2b403f48f96710be7ae3577b0b09754');
INSERT INTO `exp_security_hashes` VALUES(6612, 1366578263, '127.0.0.1', 'e92ab507a60ee84df44c5e8e5e367621a9c4babf');
INSERT INTO `exp_security_hashes` VALUES(6613, 1366578263, '127.0.0.1', '8776c2ec9c33e85542ab228ac2025f65e42ea744');
INSERT INTO `exp_security_hashes` VALUES(6614, 1366578264, '127.0.0.1', '3a330c057e11eb32d7ab5c8432ae8405e1026261');
INSERT INTO `exp_security_hashes` VALUES(6615, 1366578264, '127.0.0.1', '42f2de1e0eef7f363dd79c2dbfa022fc6956a5ae');
INSERT INTO `exp_security_hashes` VALUES(6616, 1366578265, '127.0.0.1', 'dfd6ae1d64c31baf5feaefc044a085153f7611d6');
INSERT INTO `exp_security_hashes` VALUES(6617, 1366578265, '127.0.0.1', 'a5894279d8c6dc8187af2d1291a80fe923673d5e');
INSERT INTO `exp_security_hashes` VALUES(6618, 1366578265, '127.0.0.1', 'efa47fbb59ea40eba0333fd3eb1cbc5eec574082');
INSERT INTO `exp_security_hashes` VALUES(6619, 1366578266, '127.0.0.1', 'dc20345130f22f2c83e51449c5d851f26dc6051a');
INSERT INTO `exp_security_hashes` VALUES(6620, 1366578267, '127.0.0.1', '312bcd32aa07ced5359cdc19f403e3c27f0c37af');
INSERT INTO `exp_security_hashes` VALUES(6621, 1366578474, '127.0.0.1', '76413b184222fbb6dcbbdb2daae10f974158f851');
INSERT INTO `exp_security_hashes` VALUES(6622, 1366578474, '127.0.0.1', '7ae2730da07786959cdb74985505809c2bd1003b');
INSERT INTO `exp_security_hashes` VALUES(6623, 1366578475, '127.0.0.1', 'd4d1fc724ba26cb224023bb53fae78edd280558b');
INSERT INTO `exp_security_hashes` VALUES(6624, 1366578514, '127.0.0.1', 'ec311f06590aae92533b6a3d170991a966f3a51d');
INSERT INTO `exp_security_hashes` VALUES(6625, 1366578514, '127.0.0.1', 'ae63f336c8a5025d9ccad90bcdf4be38f9f1d615');
INSERT INTO `exp_security_hashes` VALUES(6626, 1366578514, '127.0.0.1', '5d0d3316088ae84eef05672a18488058471d79c5');
INSERT INTO `exp_security_hashes` VALUES(6627, 1366578515, '127.0.0.1', '473a61bb82d980adc8c3b2ebd29973061329d3e7');
INSERT INTO `exp_security_hashes` VALUES(6628, 1366578515, '127.0.0.1', 'd98cd67571ad768e9bb7644a46162ae08f06da2d');
INSERT INTO `exp_security_hashes` VALUES(6629, 1366578606, '127.0.0.1', '74c42fd4d487a128fa4fde07d5ed95aa60e2790e');
INSERT INTO `exp_security_hashes` VALUES(6630, 1366578606, '127.0.0.1', 'c5e34e92cb8c82d321b0f5385cbf1d973c4f9cbc');
INSERT INTO `exp_security_hashes` VALUES(6631, 1366578606, '127.0.0.1', '1bcde07349f6d9fc96472fd65cffaf8b5b53a043');
INSERT INTO `exp_security_hashes` VALUES(6632, 1366578607, '127.0.0.1', 'e570674b93bf77c2e82c7c17a7fddde2ab112816');
INSERT INTO `exp_security_hashes` VALUES(6633, 1366578607, '127.0.0.1', 'b101684f92bbac9e58d31084d3b20957090ecafa');
INSERT INTO `exp_security_hashes` VALUES(6634, 1366579788, '127.0.0.1', 'c2d8485ace092071c84994a84d0f520d69e7763d');
INSERT INTO `exp_security_hashes` VALUES(6635, 1366579788, '127.0.0.1', 'd56a7dd51455e282dfc64d86a9164ca30c0c4f6f');
INSERT INTO `exp_security_hashes` VALUES(6636, 1366579789, '127.0.0.1', '60018256f6c9b960959dd7393bf241ae1c3f3fe3');
INSERT INTO `exp_security_hashes` VALUES(6637, 1366579830, '127.0.0.1', 'e4a73d2343f84d6e76e418b979eaccbdff6509c5');
INSERT INTO `exp_security_hashes` VALUES(6638, 1366579830, '127.0.0.1', '0e154897ef37a8cd2fe41b204dc04a7b685ec89f');
INSERT INTO `exp_security_hashes` VALUES(6639, 1366579830, '127.0.0.1', '86fc2f76228a092f959c965789d05d16822c53d9');
INSERT INTO `exp_security_hashes` VALUES(6640, 1366579831, '127.0.0.1', 'e3c421d2bc32bb0032c2748d9243dcdfbdf6dc84');
INSERT INTO `exp_security_hashes` VALUES(6641, 1366579832, '127.0.0.1', '17709f731a97b782b2ab8bcdfa931c2a8f837a6d');
INSERT INTO `exp_security_hashes` VALUES(6642, 1366579832, '127.0.0.1', '2437993f6a64bdb708f0ae2d3d3e8574cebe3f6d');
INSERT INTO `exp_security_hashes` VALUES(6643, 1366579832, '127.0.0.1', 'f5999edb4f71503c13ac86bdaecc5787aa8573bc');
INSERT INTO `exp_security_hashes` VALUES(6644, 1366579832, '127.0.0.1', '839d0aca3dde13b305a7fc5e8b8d2a39c1bb6d10');
INSERT INTO `exp_security_hashes` VALUES(6645, 1366579832, '127.0.0.1', '1c0228ab63795d4d42574b9bfa317a035dc470c3');
INSERT INTO `exp_security_hashes` VALUES(6646, 1366579832, '127.0.0.1', '8e4cded25a216d8601f23527c01224987325b68b');
INSERT INTO `exp_security_hashes` VALUES(6647, 1366579832, '127.0.0.1', '02a594406447a25ab6aba3fc0a54d988aca644fc');
INSERT INTO `exp_security_hashes` VALUES(6648, 1366579832, '127.0.0.1', 'b417e463f42c68ed7cf8037a08f38ddca6a4fafa');
INSERT INTO `exp_security_hashes` VALUES(6649, 1366579833, '127.0.0.1', 'e58aabaffa8289b8602cdd76693bde8ee8116e32');
INSERT INTO `exp_security_hashes` VALUES(6650, 1366579833, '127.0.0.1', '1c85e7a69247a6dd0ab05bdbfb76fb95fce8abd7');
INSERT INTO `exp_security_hashes` VALUES(6651, 1366579834, '127.0.0.1', 'd83799ccbaf572df656a4db7923c6532474b9b74');
INSERT INTO `exp_security_hashes` VALUES(6652, 1366579835, '127.0.0.1', 'a68c33f8765e5ac3e76571169a758f18aa54c46a');
INSERT INTO `exp_security_hashes` VALUES(6653, 1366579835, '127.0.0.1', '84a749e8f6d91863ea9ee912a8dce9b8579abe2e');
INSERT INTO `exp_security_hashes` VALUES(6654, 1366579835, '127.0.0.1', 'c55d4ee268d5f676fc2646928fe8b92f181a55f4');
INSERT INTO `exp_security_hashes` VALUES(6655, 1366579836, '127.0.0.1', '3f4247edfbe08d5b73cfc19d64fa2068fc172f65');
INSERT INTO `exp_security_hashes` VALUES(6656, 1366579837, '127.0.0.1', 'ca8eb4a1f2b021f08b15f2c0f13ebb8b82abef9a');
INSERT INTO `exp_security_hashes` VALUES(6657, 1366579838, '127.0.0.1', 'aa38ca7228eb9cc8e55e8cab8a82e2ae7533e912');
INSERT INTO `exp_security_hashes` VALUES(6658, 1366579838, '127.0.0.1', '26e17998d987a4e9bf1f620e1c9b7644c0502677');
INSERT INTO `exp_security_hashes` VALUES(6659, 1366579838, '127.0.0.1', 'abcfa9332f2b757d560c416972d809adc6c04296');
INSERT INTO `exp_security_hashes` VALUES(6660, 1366579838, '127.0.0.1', 'cd7710561d179083b7d9516f080855cfda73f420');
INSERT INTO `exp_security_hashes` VALUES(6661, 1366579839, '127.0.0.1', '2ebe728feccb5b9f059936ed0e976202d89ad708');
INSERT INTO `exp_security_hashes` VALUES(6662, 1366579840, '127.0.0.1', 'd487d4b417d83c52966f9e0eb5b381568e1aee99');
INSERT INTO `exp_security_hashes` VALUES(6663, 1366581249, '127.0.0.1', 'a7511c4b315827e4d7b2b406b0cb54298d5e305d');
INSERT INTO `exp_security_hashes` VALUES(6664, 1366581249, '127.0.0.1', '52ba2489356c2a1b636fe326ea8d9bcbbefa934f');
INSERT INTO `exp_security_hashes` VALUES(6665, 1366581250, '127.0.0.1', 'd2a5dd64f5f1c286f78c3fd11b1014454dc38012');
INSERT INTO `exp_security_hashes` VALUES(6666, 1366581250, '127.0.0.1', '8126fa396f63359cfe7a45445a783718a8d5f85d');
INSERT INTO `exp_security_hashes` VALUES(6667, 1366582112, '127.0.0.1', '853126fab857a4cb8d5f59f394f4f5f4c4537cb9');
INSERT INTO `exp_security_hashes` VALUES(6668, 1366582113, '127.0.0.1', 'f8483ed9baa916b8c9d5f158f5e3004d7708abb4');
INSERT INTO `exp_security_hashes` VALUES(6669, 1366582113, '127.0.0.1', '069df7da113f66de31465174dd51186f1ae1fef6');
INSERT INTO `exp_security_hashes` VALUES(6670, 1366582114, '127.0.0.1', 'becf07f7271c3c19f3121fd7db2b83bd506209f2');
INSERT INTO `exp_security_hashes` VALUES(6671, 1366582118, '127.0.0.1', '1711df050dbe095b9735eac03e78df5c28bdd404');
INSERT INTO `exp_security_hashes` VALUES(6672, 1366582118, '127.0.0.1', '72861f393712dec7b631eab29a2dcb95256eb1cd');
INSERT INTO `exp_security_hashes` VALUES(6673, 1366582119, '127.0.0.1', '6c0c20a4dcc12c9c8ea901266252527542be2644');
INSERT INTO `exp_security_hashes` VALUES(6674, 1366582119, '127.0.0.1', 'cf7e1615a01a3ccebc91a6ce5e95c4fa2f48bfda');
INSERT INTO `exp_security_hashes` VALUES(6675, 1366582123, '127.0.0.1', 'b70dc8e6850bc90271c98ff3cce0f87863aadc45');
INSERT INTO `exp_security_hashes` VALUES(6676, 1366582124, '127.0.0.1', '54bcdb416aba1d76e90f3cc5597f609b7c479af2');
INSERT INTO `exp_security_hashes` VALUES(6677, 1366582125, '127.0.0.1', '938d9bf6b60dd5b7e27ff2097147a19e9d1e615a');
INSERT INTO `exp_security_hashes` VALUES(6678, 1366582129, '127.0.0.1', '9dd532fbeb41380b7ef1cbe53720f7236b2b5bdb');
INSERT INTO `exp_security_hashes` VALUES(6679, 1366582129, '127.0.0.1', '36cfeda9fe83bff3a60184c28819f4c264ce43e2');
INSERT INTO `exp_security_hashes` VALUES(6680, 1366582133, '127.0.0.1', '7b726592c4befa84b4cf5a881017df6a60e2df87');
INSERT INTO `exp_security_hashes` VALUES(6681, 1366582525, '127.0.0.1', '3648a2b0db6408adaac2595dbf9b77117bdf44c7');
INSERT INTO `exp_security_hashes` VALUES(6682, 1366582526, '127.0.0.1', '0d069a532fe2f851c1997c87a86667d66345e876');
INSERT INTO `exp_security_hashes` VALUES(6683, 1366582526, '127.0.0.1', 'c5d9e27979423cb1fd7815400aec4f49d58b0f63');
INSERT INTO `exp_security_hashes` VALUES(6684, 1366582527, '127.0.0.1', '828ab6c9ec725558bc190d70150ba31c647934b1');
INSERT INTO `exp_security_hashes` VALUES(6685, 1366582660, '127.0.0.1', 'f2586e0916dc472e260888a4b50435dfd31fd049');
INSERT INTO `exp_security_hashes` VALUES(6686, 1366582660, '127.0.0.1', 'c39c3782a728578225d4160caf86a540c4da113d');
INSERT INTO `exp_security_hashes` VALUES(6687, 1366582661, '127.0.0.1', '5f3e58566cbae7c418af3ebe1575dcaf887cb6c4');
INSERT INTO `exp_security_hashes` VALUES(6688, 1366582662, '127.0.0.1', '7255a5229d10681178f55e751802c6f451ddb132');
INSERT INTO `exp_security_hashes` VALUES(6689, 1366582706, '127.0.0.1', '4f62e82f4c05118129ae1707176a38f44b0131a2');
INSERT INTO `exp_security_hashes` VALUES(6690, 1366582706, '127.0.0.1', 'aca5a029f49ba6bb7cf7ca4fca589cbf885724b3');
INSERT INTO `exp_security_hashes` VALUES(6691, 1366582706, '127.0.0.1', 'a78eb2b3eaeae3ba9f0a7e69b72a7c11f5d85e8a');
INSERT INTO `exp_security_hashes` VALUES(6692, 1366582707, '127.0.0.1', 'e40552c3067a692070af251e05d4b371ed85d07d');
INSERT INTO `exp_security_hashes` VALUES(6693, 1366582986, '127.0.0.1', '59dbf776eb4cde6305d72bb4fac698bb78da59b0');
INSERT INTO `exp_security_hashes` VALUES(6694, 1366582986, '127.0.0.1', 'e4cb6823574c9c08fc9747a3c3f2ccb48f7b0d58');
INSERT INTO `exp_security_hashes` VALUES(6695, 1366582987, '127.0.0.1', 'ee7a7435e225c8c9d677dbb86e6a782fd44a0153');
INSERT INTO `exp_security_hashes` VALUES(6696, 1366583020, '127.0.0.1', '3953fb2e485ba02fb63f6262cc47104e5bb056ec');
INSERT INTO `exp_security_hashes` VALUES(6697, 1366583020, '127.0.0.1', '21bdddf6b96e815161a9014588ba064e4d85cc86');
INSERT INTO `exp_security_hashes` VALUES(6698, 1366583020, '127.0.0.1', 'e0dea9d78779e19e1551c2a810c0a531d0319649');
INSERT INTO `exp_security_hashes` VALUES(6699, 1366583021, '127.0.0.1', '976006add844fec6bb64329d00a9cf870b010f46');
INSERT INTO `exp_security_hashes` VALUES(6700, 1366583021, '127.0.0.1', '549d6d9095c0a994328e4a0b3ad6be4cd8829b1d');
INSERT INTO `exp_security_hashes` VALUES(6701, 1366583023, '127.0.0.1', 'de555334b1ad1c5877127ea8589f91388bbb2819');
INSERT INTO `exp_security_hashes` VALUES(6702, 1366583023, '127.0.0.1', 'd69d27e945f1a3e976ccdfc9a188946ba07dd353');
INSERT INTO `exp_security_hashes` VALUES(6703, 1366583023, '127.0.0.1', '568ad6d31ceb489f8bb226003f3200817346daf2');
INSERT INTO `exp_security_hashes` VALUES(6704, 1366583024, '127.0.0.1', '5f02130255b2293329f8b98fabccaa3f1b963627');
INSERT INTO `exp_security_hashes` VALUES(6705, 1366583025, '127.0.0.1', '59e904f86ea56d500ffc60d53b9d35be476f70d0');
INSERT INTO `exp_security_hashes` VALUES(6706, 1366583225, '127.0.0.1', 'bab37925775d537f97d7eda1a240bb3ac5694923');
INSERT INTO `exp_security_hashes` VALUES(6707, 1366583225, '127.0.0.1', 'a6361d83cd4e548c11c3c5fbb62dd521de5dfe6a');
INSERT INTO `exp_security_hashes` VALUES(6708, 1366583225, '127.0.0.1', '0d546621e1a9ae64ec362f197257697ce793dfad');
INSERT INTO `exp_security_hashes` VALUES(6709, 1366583226, '127.0.0.1', '2b4d12607281e6742ac33c16688aca37b75c7030');
INSERT INTO `exp_security_hashes` VALUES(6710, 1366583227, '127.0.0.1', 'ab0523bceac95fd779d29a6cfb785d3f6ece133b');
INSERT INTO `exp_security_hashes` VALUES(6711, 1366583237, '127.0.0.1', '64be2313ad3ffc2f06b99c0b0d90ab297ffae4d6');
INSERT INTO `exp_security_hashes` VALUES(6712, 1366584198, '127.0.0.1', '866ddf35396c7a9f9515e207440f1824cc7a8678');
INSERT INTO `exp_security_hashes` VALUES(6713, 1366584199, '127.0.0.1', 'cbf929f593e30a08672b7bd87cc596f0446f7bbf');
INSERT INTO `exp_security_hashes` VALUES(6714, 1366584204, '127.0.0.1', 'f71adae05f90d14564f8ee663a3a2961c8b7953d');
INSERT INTO `exp_security_hashes` VALUES(6715, 1366584204, '127.0.0.1', '38cbfe428f8d2e0f3b8232c2303aa53cfe0a0770');
INSERT INTO `exp_security_hashes` VALUES(6716, 1366584205, '127.0.0.1', '3db5afb9811f731fec2f6f9b3be8e20afa894b62');
INSERT INTO `exp_security_hashes` VALUES(6717, 1366584207, '127.0.0.1', 'c4e33d9e4e116e6a1819eed87b719bc065fa36e7');
INSERT INTO `exp_security_hashes` VALUES(6718, 1366584207, '127.0.0.1', '5a49468538857e4dc1bcd6b4db2b02e2db23a82f');
INSERT INTO `exp_security_hashes` VALUES(6719, 1366584207, '127.0.0.1', 'e21447e17618c2d532063dd235984e2601706455');
INSERT INTO `exp_security_hashes` VALUES(6720, 1366584208, '127.0.0.1', 'a9405d2782f2d22bf0311b429f2836b115bd66e5');
INSERT INTO `exp_security_hashes` VALUES(6721, 1366584209, '127.0.0.1', 'c0e7045b63f0f0aabbf7c8d2ea4d1b30d12386cc');
INSERT INTO `exp_security_hashes` VALUES(6722, 1366584333, '127.0.0.1', '3b10348924232932e724f22fa7a3b6ff94bfaf1e');
INSERT INTO `exp_security_hashes` VALUES(6723, 1366584334, '127.0.0.1', '33d5c374bffcc0b5d3d00735061b68872e1d6893');
INSERT INTO `exp_security_hashes` VALUES(6724, 1366584334, '127.0.0.1', 'be4b975457a2d50730e97990fe809a2bc2eef15e');
INSERT INTO `exp_security_hashes` VALUES(6725, 1366584449, '127.0.0.1', '88cb4aad8df1c013bb4b54d538704dfb295152be');
INSERT INTO `exp_security_hashes` VALUES(6726, 1366584449, '127.0.0.1', '4bc83d879f27e363693f2cd852eb438ae7ddf389');
INSERT INTO `exp_security_hashes` VALUES(6727, 1366584450, '127.0.0.1', '6a4e861a8f4d58dcfda4ccf18d641b678c07fb3b');
INSERT INTO `exp_security_hashes` VALUES(6728, 1366584451, '127.0.0.1', 'bbee512238ebff90a6bae823507c285d1b7acc56');
INSERT INTO `exp_security_hashes` VALUES(6729, 1366584451, '127.0.0.1', '0cf1b5aa36525976afca61db9a49dfaa4289df8a');
INSERT INTO `exp_security_hashes` VALUES(6730, 1366584452, '127.0.0.1', '20d0c490313029e16e4e568044d7e10bd4191d5a');
INSERT INTO `exp_security_hashes` VALUES(6731, 1366584745, '127.0.0.1', 'a555abb5253bbc645d7b13cf007a6b124639970e');
INSERT INTO `exp_security_hashes` VALUES(6732, 1366584746, '127.0.0.1', 'dc6cf840943a8aa084773e459a84856322eacb98');
INSERT INTO `exp_security_hashes` VALUES(6733, 1366584780, '127.0.0.1', '20a63066965c410b60965a71e7b0ea12818c514c');
INSERT INTO `exp_security_hashes` VALUES(6734, 1366584781, '127.0.0.1', '071635b65c0e08515d7d67768c9f75102f09a878');
INSERT INTO `exp_security_hashes` VALUES(6735, 1366584783, '127.0.0.1', '8d56c4274a421adba258d992a0d2e50c1c2cee8e');
INSERT INTO `exp_security_hashes` VALUES(6736, 1366584784, '127.0.0.1', 'e7a7fa8d682f87225d47bc97fc21bda28522d185');
INSERT INTO `exp_security_hashes` VALUES(6737, 1366584826, '127.0.0.1', '26bbb3fb8e2fa0814aa01d9bff683c15041a5bac');
INSERT INTO `exp_security_hashes` VALUES(6738, 1366584826, '127.0.0.1', '82aa7606a341ec0550cbbc1cd92c6d9239181430');
INSERT INTO `exp_security_hashes` VALUES(6739, 1366584826, '127.0.0.1', '43ac3c6fb42051675e2d2d96bba6e2bd3a8e59d5');
INSERT INTO `exp_security_hashes` VALUES(6740, 1366584827, '127.0.0.1', '9236b828d2276b5b55b589d240a714a22268f6de');
INSERT INTO `exp_security_hashes` VALUES(6741, 1366584827, '127.0.0.1', '9957feea93dd276cc83b7327ae6d736a33eea3af');
INSERT INTO `exp_security_hashes` VALUES(6742, 1366584828, '127.0.0.1', '58a03d963df6022d6f47d4dd8587b9fd71b7bd9d');
INSERT INTO `exp_security_hashes` VALUES(6743, 1366584829, '127.0.0.1', '2546c9b79d62d2abaca1ca6ca3ba913947d9711a');
INSERT INTO `exp_security_hashes` VALUES(6744, 1366584829, '127.0.0.1', '10e09c5d3bdaf389838b9d0aafebc01b387feeba');
INSERT INTO `exp_security_hashes` VALUES(6745, 1366584829, '127.0.0.1', '485a27de683b7a65c0073e483073ad571b474924');
INSERT INTO `exp_security_hashes` VALUES(6746, 1366584829, '127.0.0.1', 'c8cefd8dd4d1ac626406e56cd4047a6f01a9c94b');
INSERT INTO `exp_security_hashes` VALUES(6747, 1366584829, '127.0.0.1', '7793da3823685ddb78cb740ec74922a0f05c925f');
INSERT INTO `exp_security_hashes` VALUES(6748, 1366584830, '127.0.0.1', 'c899130f2ee846f7d6c542fded1e62af2f331820');
INSERT INTO `exp_security_hashes` VALUES(6749, 1366584830, '127.0.0.1', 'c44d6112c7dfadd63c1cbf439203a3460d908d3c');
INSERT INTO `exp_security_hashes` VALUES(6750, 1366584831, '127.0.0.1', '6f89efb7dec410bc0bda5119daba13ee498ce9e1');
INSERT INTO `exp_security_hashes` VALUES(6751, 1366584944, '127.0.0.1', '3bf58b720a7013de8fdde328f72644802e852b05');
INSERT INTO `exp_security_hashes` VALUES(6752, 1366584945, '127.0.0.1', '6a3af6b90d72a65d199a43083c20e328429a725d');
INSERT INTO `exp_security_hashes` VALUES(6753, 1366584963, '127.0.0.1', 'f4f6787ab9ce8e24525fed596742c9c86a10e9d8');
INSERT INTO `exp_security_hashes` VALUES(6754, 1366584963, '127.0.0.1', 'ecd65b947d99012d603a544f71897f7aacfa87b8');
INSERT INTO `exp_security_hashes` VALUES(6755, 1366584964, '127.0.0.1', '4db166c074539c882bcbd332b0cd0a32d14f9fce');
INSERT INTO `exp_security_hashes` VALUES(6756, 1366584964, '127.0.0.1', '60a65c280a4729c97c6c3114b831b29c3186a557');
INSERT INTO `exp_security_hashes` VALUES(6757, 1366584964, '127.0.0.1', 'd89574acba2e1be1779e31f32dbc14746f1d10f6');
INSERT INTO `exp_security_hashes` VALUES(6758, 1366584964, '127.0.0.1', '384368178796a9d488cbbf45fe434dfb0fe0af58');
INSERT INTO `exp_security_hashes` VALUES(6759, 1366584964, '127.0.0.1', 'bc3d6e02ac7c5a5429ca1c266310938570de84c1');
INSERT INTO `exp_security_hashes` VALUES(6760, 1366584964, '127.0.0.1', '827b8658776f7c913b303acaed638ad4486ad5ff');
INSERT INTO `exp_security_hashes` VALUES(6761, 1366584965, '127.0.0.1', '92ca59bef9ec95aef40efc420a87d0d69135ee08');
INSERT INTO `exp_security_hashes` VALUES(6762, 1366584965, '127.0.0.1', 'a80be14b1e2eaafe781712b0c6646b1f137be8ae');
INSERT INTO `exp_security_hashes` VALUES(6763, 1366584965, '127.0.0.1', 'e36e67a52856a260d6f04dae16cb643e20b07d42');
INSERT INTO `exp_security_hashes` VALUES(6764, 1366584966, '127.0.0.1', 'b55b56b92c8e431c80556d34f88ed7bc7c097685');
INSERT INTO `exp_security_hashes` VALUES(6765, 1366584966, '127.0.0.1', '6048419dcb164dee2ef29a80433e7ff2c28a0ef4');
INSERT INTO `exp_security_hashes` VALUES(6766, 1366584966, '127.0.0.1', 'da85b731bbeafb8c9b3dc9e30de47d6f0573c000');
INSERT INTO `exp_security_hashes` VALUES(6767, 1366584966, '127.0.0.1', '1ab6e541d54583288a78ec0fc5edfbb6cd4332fc');
INSERT INTO `exp_security_hashes` VALUES(6768, 1366584966, '127.0.0.1', '9f242f4ddbac34c014098c2f50061dc7c8b24775');
INSERT INTO `exp_security_hashes` VALUES(6769, 1366584966, '127.0.0.1', '2b7fc693f38624d2454eb25afe0f9ed7a0202ec1');
INSERT INTO `exp_security_hashes` VALUES(6770, 1366584967, '127.0.0.1', '415c61732ffa05da408e265f5ff27b328422e3fc');
INSERT INTO `exp_security_hashes` VALUES(6771, 1366584967, '127.0.0.1', 'f86206d7be0003470dd54b9b0be7f4267dab945c');
INSERT INTO `exp_security_hashes` VALUES(6772, 1366584968, '127.0.0.1', 'ab694d12b46b496fbd9cdb8ebd43f1879321c336');
INSERT INTO `exp_security_hashes` VALUES(6773, 1366584968, '127.0.0.1', '9534f5172ffc1e691813435316339403da2035c0');
INSERT INTO `exp_security_hashes` VALUES(6774, 1366584968, '127.0.0.1', 'fcb95608355b4ac7cfd3ac53f6312e62c43a56e7');
INSERT INTO `exp_security_hashes` VALUES(6775, 1366584969, '127.0.0.1', '52ba12115ed15bf0df420d5724ade9b84b17a766');
INSERT INTO `exp_security_hashes` VALUES(6776, 1366584969, '127.0.0.1', 'ceec9efb7cef6c0e07e6601077200cf17a8899cc');
INSERT INTO `exp_security_hashes` VALUES(6777, 1366584970, '127.0.0.1', '1e199ed84cb43fa6c42d0367b3745e75a79b05fe');
INSERT INTO `exp_security_hashes` VALUES(6778, 1366585025, '127.0.0.1', 'c5ae6038e688930604ccc9d36cb06b7691566fa6');
INSERT INTO `exp_security_hashes` VALUES(6779, 1366585025, '127.0.0.1', 'dbd7f46230a7719d410b7a95d800e2813fdd2a0e');
INSERT INTO `exp_security_hashes` VALUES(6780, 1366585026, '127.0.0.1', '6dd640e2cee1cfa96f5eac153703426145d07916');
INSERT INTO `exp_security_hashes` VALUES(6781, 1366585029, '127.0.0.1', '92cefd3ee2b28b4d82d09cee46323d0cc7bfaf92');
INSERT INTO `exp_security_hashes` VALUES(6782, 1366585030, '127.0.0.1', '753f556eedd05841c2d370f4bd61944370c9e7a8');
INSERT INTO `exp_security_hashes` VALUES(6783, 1366585133, '127.0.0.1', '06896e8bb106d9d483632780db35c4a705b8e88b');
INSERT INTO `exp_security_hashes` VALUES(6784, 1366585134, '127.0.0.1', 'a45c7fa16f69844e2693a0bc1903dd885e4dbe6d');
INSERT INTO `exp_security_hashes` VALUES(6785, 1366585134, '127.0.0.1', '4f2aa63a3de3546bae4e524c4d408805545f1094');
INSERT INTO `exp_security_hashes` VALUES(6786, 1366585166, '127.0.0.1', '557f4417a46e93712d55f42403b63abc56e170fc');
INSERT INTO `exp_security_hashes` VALUES(6787, 1366585167, '127.0.0.1', '3ac89351ee3b96d0582b0537e92e412754379e83');
INSERT INTO `exp_security_hashes` VALUES(6788, 1366585167, '127.0.0.1', '76129d8f76737e775fee4607dd0fbc92e1a1e1d0');
INSERT INTO `exp_security_hashes` VALUES(6789, 1366585167, '127.0.0.1', '56d5e35390bb0cd0c10e4ed3b5be6b331f09618c');
INSERT INTO `exp_security_hashes` VALUES(6790, 1366585167, '127.0.0.1', 'bbf1af62fe1e96267c558d078b0c4ca4d902cb76');
INSERT INTO `exp_security_hashes` VALUES(6791, 1366585168, '127.0.0.1', 'ee25c4c235f4d0aaad4c38f6b932685a477e0f94');
INSERT INTO `exp_security_hashes` VALUES(6792, 1366585169, '127.0.0.1', '216e65a1a2dad606afd47b688ab3ce75b41277db');
INSERT INTO `exp_security_hashes` VALUES(6793, 1366585204, '127.0.0.1', 'f8c810c469296d9814a4ccf0ef76b23a154b8862');
INSERT INTO `exp_security_hashes` VALUES(6794, 1366585205, '127.0.0.1', 'd58f0ba6aaf1e4cb5e197ffdcb6c11074c64fa9a');
INSERT INTO `exp_security_hashes` VALUES(6795, 1366585205, '127.0.0.1', '69f52a8c61a7726ee13e71525cdd2bc5a4e62f7c');
INSERT INTO `exp_security_hashes` VALUES(6796, 1366585307, '127.0.0.1', '5e188a632075a259bca2af780093795e34115ff8');
INSERT INTO `exp_security_hashes` VALUES(6797, 1366585307, '127.0.0.1', 'c17d0c2724a7f70773d7829e33af93850b1c9d76');
INSERT INTO `exp_security_hashes` VALUES(6798, 1366585307, '127.0.0.1', '40c68947610fcbb1cc4459e0035f4abfcc83d407');
INSERT INTO `exp_security_hashes` VALUES(6799, 1366585308, '127.0.0.1', '552cbf2edfc4dc84f2a8a9ab94365e1577bb1ab9');
INSERT INTO `exp_security_hashes` VALUES(6800, 1366585308, '127.0.0.1', '31f5f10c9db3102a0405168d826c29a024f96cb6');
INSERT INTO `exp_security_hashes` VALUES(6801, 1366585309, '127.0.0.1', 'dec62b60ac3242d8c3e6bf8aaa0ba6119a371ac9');
INSERT INTO `exp_security_hashes` VALUES(6802, 1366585309, '127.0.0.1', 'c7f13c837cc4b448a8dc76fdc7f083b5ac3414a0');
INSERT INTO `exp_security_hashes` VALUES(6803, 1366585309, '127.0.0.1', '4909fdda149a9f5bc57a523204fb8a91606e5a3c');
INSERT INTO `exp_security_hashes` VALUES(6804, 1366585310, '127.0.0.1', '6a7ea7ec07b29143be3dfb58956255ff4faf2e5d');
INSERT INTO `exp_security_hashes` VALUES(6805, 1366585311, '127.0.0.1', 'de5a02f03a0c9c6b9276b57140d7bb342bad10dc');
INSERT INTO `exp_security_hashes` VALUES(6806, 1366586285, '127.0.0.1', 'f6c70244ef22e34bd4f55b9690d88da3ad255e33');
INSERT INTO `exp_security_hashes` VALUES(6807, 1366586285, '127.0.0.1', 'f42224fa0dfaf4eda4650637398be8895c432317');
INSERT INTO `exp_security_hashes` VALUES(6808, 1366586285, '127.0.0.1', '1559e3ca429e46217ba7c223afb86795962e8fd2');
INSERT INTO `exp_security_hashes` VALUES(6809, 1366586286, '127.0.0.1', '68be8a52fc08c7fdcee1204f3b1bb58826399a9c');
INSERT INTO `exp_security_hashes` VALUES(6810, 1366586287, '127.0.0.1', '449363e5dbb0273c5329fc6d7e0d66082a021d39');
INSERT INTO `exp_security_hashes` VALUES(6811, 1366586306, '127.0.0.1', 'd5a107a574d6c0c27afff3c4125e6931011239b2');
INSERT INTO `exp_security_hashes` VALUES(6812, 1366586306, '127.0.0.1', '45a20947887721c1d0b0bef04e5928e027699dea');
INSERT INTO `exp_security_hashes` VALUES(6813, 1366586306, '127.0.0.1', '8d9d2bde95c19e5ed18ad22a91e2d404b4b91160');
INSERT INTO `exp_security_hashes` VALUES(6814, 1366586307, '127.0.0.1', 'c73c73713496b06752acdea9b969a82664825db3');
INSERT INTO `exp_security_hashes` VALUES(6815, 1366586308, '127.0.0.1', '3ac5411b22f96b5bcad952db9f222c853ffc1a47');
INSERT INTO `exp_security_hashes` VALUES(6816, 1366588222, '127.0.0.1', '5add725036246d732f6db7cc093fcb03aeb8512e');
INSERT INTO `exp_security_hashes` VALUES(6817, 1366588222, '127.0.0.1', '9ec3ecc70d8c52eeae2353c4dc000b08d8c545c4');
INSERT INTO `exp_security_hashes` VALUES(6818, 1366588222, '127.0.0.1', '02f4c3fb4acb5b8ef487db4dceca30cb6122daf5');
INSERT INTO `exp_security_hashes` VALUES(6819, 1366588222, '127.0.0.1', '3534401eb7683ef3121129455cfd0e73d3f81f85');
INSERT INTO `exp_security_hashes` VALUES(6820, 1366588223, '127.0.0.1', '4dfc00f24042abc6c19e1782a0ba16aef0ce0863');
INSERT INTO `exp_security_hashes` VALUES(6821, 1366592064, '127.0.0.1', '2df61e3b8f4ceb6c18ee1446aa8c501856333732');
INSERT INTO `exp_security_hashes` VALUES(6822, 1366592064, '127.0.0.1', '346c1221fc6f67e3d88d31ffab6c4fb4e61fc6bb');
INSERT INTO `exp_security_hashes` VALUES(6823, 1366592064, '127.0.0.1', 'c5238284272209c1f27e3c3408254416a2b93169');
INSERT INTO `exp_security_hashes` VALUES(6824, 1366592065, '127.0.0.1', '103f9793819aae43ec26f218861c38e49500b52f');
INSERT INTO `exp_security_hashes` VALUES(6825, 1366592065, '127.0.0.1', 'a2443fa2c7bdd4623db8678837524b5e75befd71');
INSERT INTO `exp_security_hashes` VALUES(6826, 1366592134, '127.0.0.1', '986a85fba1dd03d5572c1c4a245236545906705b');
INSERT INTO `exp_security_hashes` VALUES(6827, 1366592134, '127.0.0.1', 'cc2f0196a4309e0fd60f0e16f67e76232cb9ee07');
INSERT INTO `exp_security_hashes` VALUES(6828, 1366592134, '127.0.0.1', 'f1b3bd5bdc6a7aa6eac606fa37838013b92a246d');
INSERT INTO `exp_security_hashes` VALUES(6829, 1366592135, '127.0.0.1', '27f3106e59a36b8a06211f581897f7e777f96276');
INSERT INTO `exp_security_hashes` VALUES(6830, 1366592135, '127.0.0.1', '2da3ada33de15fee4823e6a3e98b00d59dc00b34');
INSERT INTO `exp_security_hashes` VALUES(6831, 1366592135, '127.0.0.1', '801c3ecd185c8bdec2ea79d056412b46bb740614');
INSERT INTO `exp_security_hashes` VALUES(6832, 1366592135, '127.0.0.1', '0768562eeb882ca335fdb7c4fd19a16be6c7e9bf');
INSERT INTO `exp_security_hashes` VALUES(6833, 1366592135, '127.0.0.1', '6f5297e2dff79748495c33d4539a5624420550c2');
INSERT INTO `exp_security_hashes` VALUES(6834, 1366592136, '127.0.0.1', '4f7458d9061485e779bc8ab28e68ce0ae4d507ee');
INSERT INTO `exp_security_hashes` VALUES(6835, 1366592136, '127.0.0.1', '06e19271a239ddaa22d22150dc90ec9bc7176492');
INSERT INTO `exp_security_hashes` VALUES(6836, 1366592137, '127.0.0.1', '93c447a79d3cd380932598ff7946f9269fa8a488');
INSERT INTO `exp_security_hashes` VALUES(6837, 1366592137, '127.0.0.1', '3ff76e123ac419cd21f586fe061fdb7843f8623a');
INSERT INTO `exp_security_hashes` VALUES(6838, 1366592137, '127.0.0.1', 'a1fc053700a17e0fb99cee5cd1c2e7919513a23a');
INSERT INTO `exp_security_hashes` VALUES(6839, 1366592141, '127.0.0.1', '32e7ee9541454cb2e030b2e55665f808e4baae04');
INSERT INTO `exp_security_hashes` VALUES(6840, 1366592141, '127.0.0.1', '285da57ea9e4d6be03aa024c5c98ed9e902f6774');
INSERT INTO `exp_security_hashes` VALUES(6841, 1366592141, '127.0.0.1', '25dad1d17beca198ee6487ea71a654d49d51533c');
INSERT INTO `exp_security_hashes` VALUES(6842, 1366592152, '127.0.0.1', 'af80d8c9b4a8e21ab14c9cca940f13c36689bc30');
INSERT INTO `exp_security_hashes` VALUES(6843, 1366592152, '127.0.0.1', 'ed13eda35843084a258869cb4c787b5c80b8148d');
INSERT INTO `exp_security_hashes` VALUES(6844, 1366592152, '127.0.0.1', '484d66e888127b7e300dc0e571c2e77b6cce2c1e');
INSERT INTO `exp_security_hashes` VALUES(6845, 1366592152, '127.0.0.1', 'cc3263ef4b33c254c5c2b56b4f9784eba9471a0b');
INSERT INTO `exp_security_hashes` VALUES(6846, 1366592153, '127.0.0.1', '5556f57ba0bf10c619485452c32229799a8d99ec');
INSERT INTO `exp_security_hashes` VALUES(6847, 1366592154, '127.0.0.1', '9a4384002c42208e9daa531c0e7231d1b8eabcea');
INSERT INTO `exp_security_hashes` VALUES(6848, 1366592154, '127.0.0.1', '4a1f04fb72e2f0475cd26c265f6f46950797cacd');
INSERT INTO `exp_security_hashes` VALUES(6849, 1366592154, '127.0.0.1', '3d8de98b77106602e3a0d471d82a63538430027c');
INSERT INTO `exp_security_hashes` VALUES(6850, 1366592155, '127.0.0.1', '1d12686a0508d01c381c7c4c100eea77b73c16a3');
INSERT INTO `exp_security_hashes` VALUES(6851, 1366592156, '127.0.0.1', '80e380c1795095b6132d0e92b8a5d8c8dd0303e8');
INSERT INTO `exp_security_hashes` VALUES(6852, 1366592167, '127.0.0.1', '2b54aa90238f81c05988badc5f078f68e2c30d5f');
INSERT INTO `exp_security_hashes` VALUES(6853, 1366592167, '127.0.0.1', '8d2451762a982df6fd8990ad55ecc73c85d35791');
INSERT INTO `exp_security_hashes` VALUES(6854, 1366592167, '127.0.0.1', '156e525d508f06e971af50f64b5c7a8932228ed5');
INSERT INTO `exp_security_hashes` VALUES(6855, 1366592168, '127.0.0.1', '8d2781de9d9f5efa8f07a176b855b8ee4335e2c6');
INSERT INTO `exp_security_hashes` VALUES(6856, 1366592190, '127.0.0.1', 'c2dedbc1caf81d8e946ff95ac407ff22aa6ccaab');
INSERT INTO `exp_security_hashes` VALUES(6857, 1366592190, '127.0.0.1', '990c2a1e6e3582762399eeec2b5c30489914dcb2');
INSERT INTO `exp_security_hashes` VALUES(6858, 1366592190, '127.0.0.1', 'd7eb818eba1e6a3f38fa328e55dd384fb04699ec');
INSERT INTO `exp_security_hashes` VALUES(6859, 1366592191, '127.0.0.1', '2f8e67de5689f8e7cc32b29aba887a13148bc45c');
INSERT INTO `exp_security_hashes` VALUES(6860, 1366592212, '127.0.0.1', '4065c41dcec00c4b92ba79e9952acc63410451bb');
INSERT INTO `exp_security_hashes` VALUES(6861, 1366592212, '127.0.0.1', '82fb1dc8e3ab577c5e937616f10cf766ef6e9874');
INSERT INTO `exp_security_hashes` VALUES(6862, 1366592213, '127.0.0.1', 'ec56c8c07a91e4088b0f4d6bf79f65146e7c9afe');
INSERT INTO `exp_security_hashes` VALUES(6863, 1366592213, '127.0.0.1', '8828e43371bd1f4c7be83271d1b70e72c93a4385');
INSERT INTO `exp_security_hashes` VALUES(6864, 1366592347, '127.0.0.1', '106ce2bd9dd04c2e9ae923dde7cfd1acda1634d1');
INSERT INTO `exp_security_hashes` VALUES(6865, 1366592347, '127.0.0.1', 'da6d7d13070b562a704e1a529183242c486703dc');
INSERT INTO `exp_security_hashes` VALUES(6866, 1366592348, '127.0.0.1', '5c20864921e70c432ed5b23ab61c5fc8dbe3f67d');
INSERT INTO `exp_security_hashes` VALUES(6867, 1366592348, '127.0.0.1', '67644cdedf769c2d62e171236a08d118035e72c1');
INSERT INTO `exp_security_hashes` VALUES(6868, 1366592355, '127.0.0.1', '767d8607960d1faf9ca9a3f8ea12e0e041de04d0');
INSERT INTO `exp_security_hashes` VALUES(6869, 1366592355, '127.0.0.1', '5264df07b7c551877653d5c940297db413450d1c');
INSERT INTO `exp_security_hashes` VALUES(6870, 1366592355, '127.0.0.1', 'f9b2f19669f661cae6259a4826fc7b8118ce52e8');
INSERT INTO `exp_security_hashes` VALUES(6871, 1366592356, '127.0.0.1', 'ff69eec07f6e57cef765b5da650dab996561c808');
INSERT INTO `exp_security_hashes` VALUES(6872, 1366592397, '127.0.0.1', 'ece7fdf1041565daa903ac5e035feffc62928655');
INSERT INTO `exp_security_hashes` VALUES(6873, 1366592397, '127.0.0.1', '4bf3661abb16700aa0e8c871270a5c091fc89879');
INSERT INTO `exp_security_hashes` VALUES(6874, 1366592397, '127.0.0.1', '3721d38164fb68690e57375abfc7b1ad4a7eb364');
INSERT INTO `exp_security_hashes` VALUES(6875, 1366592398, '127.0.0.1', 'c558c301ea79d9d0771514cb4cd95c7f232a5187');
INSERT INTO `exp_security_hashes` VALUES(6876, 1366592410, '127.0.0.1', 'cebdb704d93ce8832e44955301f812b0516261f6');
INSERT INTO `exp_security_hashes` VALUES(6877, 1366592410, '127.0.0.1', '5470cb649e794e10477656cbcd205ab4cb2c9e5d');
INSERT INTO `exp_security_hashes` VALUES(6878, 1366592411, '127.0.0.1', '091c674354833edb005cd05d74044516f3157670');
INSERT INTO `exp_security_hashes` VALUES(6879, 1366592411, '127.0.0.1', '82e9a7443d4646250dba61c6e1c337ecd76ff477');

/* Table structure for table `exp_sessions` */
DROP TABLE IF EXISTS `exp_sessions`;

CREATE TABLE `exp_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `admin_sess` tinyint(1) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  KEY `member_id` (`member_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_sites` */
DROP TABLE IF EXISTS `exp_sites`;

CREATE TABLE `exp_sites` (
  `site_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `site_label` varchar(100) NOT NULL DEFAULT '',
  `site_name` varchar(50) NOT NULL DEFAULT '',
  `site_description` text,
  `site_system_preferences` mediumtext NOT NULL,
  `site_mailinglist_preferences` text NOT NULL,
  `site_member_preferences` text NOT NULL,
  `site_template_preferences` text NOT NULL,
  `site_channel_preferences` text NOT NULL,
  `site_bootstrap_checksums` text NOT NULL,
  PRIMARY KEY (`site_id`),
  KEY `site_name` (`site_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_sites` */
INSERT INTO `exp_sites` VALUES(1, 'Synergy Positioning Systems (NZ)', 'synergy_nz', '', 'YTo5Mjp7czoxMDoic2l0ZV9pbmRleCI7czowOiIiO3M6ODoic2l0ZV91cmwiO3M6MjE6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsLyI7czoxNjoidGhlbWVfZm9sZGVyX3VybCI7czoyODoiaHR0cDovL3N5bmVyZ3kubG9jYWwvdGhlbWVzLyI7czoxNToid2VibWFzdGVyX2VtYWlsIjtzOjE5OiJqYW1lc0A5NmJsYWNrLmNvLm56IjtzOjE0OiJ3ZWJtYXN0ZXJfbmFtZSI7czowOiIiO3M6MjA6ImNoYW5uZWxfbm9tZW5jbGF0dXJlIjtzOjc6ImNoYW5uZWwiO3M6MTA6Im1heF9jYWNoZXMiO3M6MzoiMTUwIjtzOjExOiJjYXB0Y2hhX3VybCI7czozNzoiaHR0cDovL3N5bmVyZ3kubG9jYWwvaW1hZ2VzL2NhcHRjaGFzLyI7czoxMjoiY2FwdGNoYV9wYXRoIjtzOjU0OiIvVXNlcnMvamFtZXMvUHJvamVjdHMvOTZibGFjay9TeW5lcmd5L2ltYWdlcy9jYXB0Y2hhcy8iO3M6MTI6ImNhcHRjaGFfZm9udCI7czoxOiJ5IjtzOjEyOiJjYXB0Y2hhX3JhbmQiO3M6MToieSI7czoyMzoiY2FwdGNoYV9yZXF1aXJlX21lbWJlcnMiO3M6MToibiI7czoxNzoiZW5hYmxlX2RiX2NhY2hpbmciO3M6MToibiI7czoxODoiZW5hYmxlX3NxbF9jYWNoaW5nIjtzOjE6Im4iO3M6MTg6ImZvcmNlX3F1ZXJ5X3N0cmluZyI7czoxOiJuIjtzOjEzOiJzaG93X3Byb2ZpbGVyIjtzOjE6Im4iO3M6MTg6InRlbXBsYXRlX2RlYnVnZ2luZyI7czoxOiJuIjtzOjE1OiJpbmNsdWRlX3NlY29uZHMiO3M6MToibiI7czoxMzoiY29va2llX2RvbWFpbiI7czowOiIiO3M6MTE6ImNvb2tpZV9wYXRoIjtzOjA6IiI7czoxNzoidXNlcl9zZXNzaW9uX3R5cGUiO3M6MToiYyI7czoxODoiYWRtaW5fc2Vzc2lvbl90eXBlIjtzOjI6ImNzIjtzOjIxOiJhbGxvd191c2VybmFtZV9jaGFuZ2UiO3M6MToieSI7czoxODoiYWxsb3dfbXVsdGlfbG9naW5zIjtzOjE6InkiO3M6MTY6InBhc3N3b3JkX2xvY2tvdXQiO3M6MToieSI7czoyNToicGFzc3dvcmRfbG9ja291dF9pbnRlcnZhbCI7czoxOiIxIjtzOjIwOiJyZXF1aXJlX2lwX2Zvcl9sb2dpbiI7czoxOiJ5IjtzOjIyOiJyZXF1aXJlX2lwX2Zvcl9wb3N0aW5nIjtzOjE6InkiO3M6MjQ6InJlcXVpcmVfc2VjdXJlX3Bhc3N3b3JkcyI7czoxOiJuIjtzOjE5OiJhbGxvd19kaWN0aW9uYXJ5X3B3IjtzOjE6InkiO3M6MjM6Im5hbWVfb2ZfZGljdGlvbmFyeV9maWxlIjtzOjA6IiI7czoxNzoieHNzX2NsZWFuX3VwbG9hZHMiO3M6MToieSI7czoxNToicmVkaXJlY3RfbWV0aG9kIjtzOjg6InJlZGlyZWN0IjtzOjk6ImRlZnRfbGFuZyI7czo3OiJlbmdsaXNoIjtzOjg6InhtbF9sYW5nIjtzOjI6ImVuIjtzOjEyOiJzZW5kX2hlYWRlcnMiO3M6MToieSI7czoxMToiZ3ppcF9vdXRwdXQiO3M6MToibiI7czoxMzoibG9nX3JlZmVycmVycyI7czoxOiJuIjtzOjEzOiJtYXhfcmVmZXJyZXJzIjtzOjM6IjUwMCI7czoxMToidGltZV9mb3JtYXQiO3M6MjoidXMiO3M6MTU6InNlcnZlcl90aW1lem9uZSI7czo0OiJVUDEyIjtzOjEzOiJzZXJ2ZXJfb2Zmc2V0IjtzOjA6IiI7czoxNjoiZGF5bGlnaHRfc2F2aW5ncyI7czoxOiJ5IjtzOjIxOiJkZWZhdWx0X3NpdGVfdGltZXpvbmUiO3M6NDoiVVAxMiI7czoxNjoiZGVmYXVsdF9zaXRlX2RzdCI7czoxOiJ5IjtzOjE1OiJob25vcl9lbnRyeV9kc3QiO3M6MToieSI7czoxMzoibWFpbF9wcm90b2NvbCI7czo0OiJtYWlsIjtzOjExOiJzbXRwX3NlcnZlciI7czowOiIiO3M6MTM6InNtdHBfdXNlcm5hbWUiO3M6MDoiIjtzOjEzOiJzbXRwX3Bhc3N3b3JkIjtzOjA6IiI7czoxMToiZW1haWxfZGVidWciO3M6MToibiI7czoxMzoiZW1haWxfY2hhcnNldCI7czo1OiJ1dGYtOCI7czoxNToiZW1haWxfYmF0Y2htb2RlIjtzOjE6Im4iO3M6MTY6ImVtYWlsX2JhdGNoX3NpemUiO3M6MDoiIjtzOjExOiJtYWlsX2Zvcm1hdCI7czo1OiJwbGFpbiI7czo5OiJ3b3JkX3dyYXAiO3M6MToieSI7czoyMjoiZW1haWxfY29uc29sZV90aW1lbG9jayI7czoxOiI1IjtzOjIyOiJsb2dfZW1haWxfY29uc29sZV9tc2dzIjtzOjE6InkiO3M6ODoiY3BfdGhlbWUiO3M6NzoiZGVmYXVsdCI7czoyMToiZW1haWxfbW9kdWxlX2NhcHRjaGFzIjtzOjE6Im4iO3M6MTY6ImxvZ19zZWFyY2hfdGVybXMiO3M6MToieSI7czoxMjoic2VjdXJlX2Zvcm1zIjtzOjE6InkiO3M6MTk6ImRlbnlfZHVwbGljYXRlX2RhdGEiO3M6MToieSI7czoyNDoicmVkaXJlY3Rfc3VibWl0dGVkX2xpbmtzIjtzOjE6Im4iO3M6MTY6ImVuYWJsZV9jZW5zb3JpbmciO3M6MToibiI7czoxNDoiY2Vuc29yZWRfd29yZHMiO3M6MDoiIjtzOjE4OiJjZW5zb3JfcmVwbGFjZW1lbnQiO3M6MDoiIjtzOjEwOiJiYW5uZWRfaXBzIjtzOjA6IiI7czoxMzoiYmFubmVkX2VtYWlscyI7czowOiIiO3M6MTY6ImJhbm5lZF91c2VybmFtZXMiO3M6MDoiIjtzOjE5OiJiYW5uZWRfc2NyZWVuX25hbWVzIjtzOjA6IiI7czoxMDoiYmFuX2FjdGlvbiI7czo4OiJyZXN0cmljdCI7czoxMToiYmFuX21lc3NhZ2UiO3M6MzQ6IlRoaXMgc2l0ZSBpcyBjdXJyZW50bHkgdW5hdmFpbGFibGUiO3M6MTU6ImJhbl9kZXN0aW5hdGlvbiI7czoyMToiaHR0cDovL3d3dy55YWhvby5jb20vIjtzOjE2OiJlbmFibGVfZW1vdGljb25zIjtzOjE6InkiO3M6MTI6ImVtb3RpY29uX3VybCI7czozNjoiaHR0cDovL3N5bmVyZ3kubG9jYWwvaW1hZ2VzL3NtaWxleXMvIjtzOjE5OiJyZWNvdW50X2JhdGNoX3RvdGFsIjtzOjQ6IjEwMDAiO3M6MTc6Im5ld192ZXJzaW9uX2NoZWNrIjtzOjE6InkiO3M6MTc6ImVuYWJsZV90aHJvdHRsaW5nIjtzOjE6Im4iO3M6MTc6ImJhbmlzaF9tYXNrZWRfaXBzIjtzOjE6InkiO3M6MTQ6Im1heF9wYWdlX2xvYWRzIjtzOjI6IjEwIjtzOjEzOiJ0aW1lX2ludGVydmFsIjtzOjE6IjgiO3M6MTI6ImxvY2tvdXRfdGltZSI7czoyOiIzMCI7czoxNToiYmFuaXNobWVudF90eXBlIjtzOjc6Im1lc3NhZ2UiO3M6MTQ6ImJhbmlzaG1lbnRfdXJsIjtzOjA6IiI7czoxODoiYmFuaXNobWVudF9tZXNzYWdlIjtzOjUwOiJZb3UgaGF2ZSBleGNlZWRlZCB0aGUgYWxsb3dlZCBwYWdlIGxvYWQgZnJlcXVlbmN5LiI7czoxNzoiZW5hYmxlX3NlYXJjaF9sb2ciO3M6MToieSI7czoxOToibWF4X2xvZ2dlZF9zZWFyY2hlcyI7czozOiI1MDAiO3M6MTc6InRoZW1lX2ZvbGRlcl9wYXRoIjtzOjQ1OiIvVXNlcnMvamFtZXMvUHJvamVjdHMvOTZibGFjay9TeW5lcmd5L3RoZW1lcy8iO3M6MTA6ImlzX3NpdGVfb24iO3M6MToieSI7czoxMToicnRlX2VuYWJsZWQiO3M6MToieSI7czoyMjoicnRlX2RlZmF1bHRfdG9vbHNldF9pZCI7czoxOiIxIjt9', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6MzY6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9hdmF0YXJzLyI7czoxMToiYXZhdGFyX3BhdGgiO3M6NTM6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL2F2YXRhcnMvIjtzOjE2OiJhdmF0YXJfbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNzoiYXZhdGFyX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEzOiJhdmF0YXJfbWF4X2tiIjtzOjI6IjUwIjtzOjEzOiJlbmFibGVfcGhvdG9zIjtzOjE6Im4iO3M6OToicGhvdG9fdXJsIjtzOjQyOiJodHRwOi8vc3luZXJneS5sb2NhbC9pbWFnZXMvbWVtYmVyX3Bob3Rvcy8iO3M6MTA6InBob3RvX3BhdGgiO3M6NTk6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjE1OiJwaG90b19tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE2OiJwaG90b19tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMjoicGhvdG9fbWF4X2tiIjtzOjI6IjUwIjtzOjE2OiJhbGxvd19zaWduYXR1cmVzIjtzOjE6InkiO3M6MTM6InNpZ19tYXhsZW5ndGgiO3M6MzoiNTAwIjtzOjIxOiJzaWdfYWxsb3dfaW1nX2hvdGxpbmsiO3M6MToibiI7czoyMDoic2lnX2FsbG93X2ltZ191cGxvYWQiO3M6MToibiI7czoxMToic2lnX2ltZ191cmwiO3M6NTA6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9zaWduYXR1cmVfYXR0YWNobWVudHMvIjtzOjEyOiJzaWdfaW1nX3BhdGgiO3M6Njc6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo2MDoiL1VzZXJzL2phbWVzL1Byb2plY3RzLzk2YmxhY2svU3luZXJneS9pbWFnZXMvcG1fYXR0YWNobWVudHMvIjtzOjIzOiJwcnZfbXNnX21heF9hdHRhY2htZW50cyI7czoxOiIzIjtzOjIyOiJwcnZfbXNnX2F0dGFjaF9tYXhzaXplIjtzOjM6IjI1MCI7czoyMDoicHJ2X21zZ19hdHRhY2hfdG90YWwiO3M6MzoiMTAwIjtzOjE5OiJwcnZfbXNnX2h0bWxfZm9ybWF0IjtzOjQ6InNhZmUiO3M6MTg6InBydl9tc2dfYXV0b19saW5rcyI7czoxOiJ5IjtzOjE3OiJwcnZfbXNnX21heF9jaGFycyI7czo0OiI2MDAwIjtzOjE5OiJtZW1iZXJsaXN0X29yZGVyX2J5IjtzOjExOiJ0b3RhbF9wb3N0cyI7czoyMToibWVtYmVybGlzdF9zb3J0X29yZGVyIjtzOjQ6ImRlc2MiO3M6MjA6Im1lbWJlcmxpc3Rfcm93X2xpbWl0IjtzOjI6IjIwIjt9', 'YTo2OntzOjExOiJzdHJpY3RfdXJscyI7czoxOiJuIjtzOjg6InNpdGVfNDA0IjtzOjA6IiI7czoxOToic2F2ZV90bXBsX3JldmlzaW9ucyI7czoxOiJuIjtzOjE4OiJtYXhfdG1wbF9yZXZpc2lvbnMiO3M6MToiNSI7czoxNToic2F2ZV90bXBsX2ZpbGVzIjtzOjE6InkiO3M6MTg6InRtcGxfZmlsZV9iYXNlcGF0aCI7czo0NjoiL1VzZXJzL2phbWVzbWNmYWxsL1Byb2plY3RzL1N5bmVyZ3kvdGVtcGxhdGVzLyI7fQ==', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YToyOntzOjc6ImVtYWlsZWQiO2E6MDp7fXM6NDU6Ii9Vc2Vycy9qYW1lc21jZmFsbC9Qcm9qZWN0cy9TeW5lcmd5L2luZGV4LnBocCI7czozMjoiZjFiMTliOGE0NDU5NmM1MzQ2MTViZWJlMTMyNmYwNjgiO30=');
INSERT INTO `exp_sites` VALUES(2, 'Synergy Positioning Systems (AU)', 'synergy_au', '', 'YTo5MTp7czoxMDoiaXNfc2l0ZV9vbiI7czoxOiJ5IjtzOjEwOiJzaXRlX2luZGV4IjtzOjA6IiI7czo4OiJzaXRlX3VybCI7czoyMToiaHR0cDovL3N5bmVyZ3kubG9jYWwvIjtzOjE2OiJ0aGVtZV9mb2xkZXJfdXJsIjtzOjI4OiJodHRwOi8vc3luZXJneS5sb2NhbC90aGVtZXMvIjtzOjE3OiJ0aGVtZV9mb2xkZXJfcGF0aCI7czo0NToiL1VzZXJzL2phbWVzL1Byb2plY3RzLzk2YmxhY2svU3luZXJneS90aGVtZXMvIjtzOjE1OiJ3ZWJtYXN0ZXJfZW1haWwiO3M6MTk6ImphbWVzQDk2YmxhY2suY28ubnoiO3M6MTQ6IndlYm1hc3Rlcl9uYW1lIjtzOjA6IiI7czoyMDoiY2hhbm5lbF9ub21lbmNsYXR1cmUiO3M6NzoiY2hhbm5lbCI7czoxMDoibWF4X2NhY2hlcyI7czozOiIxNTAiO3M6MTE6ImNhcHRjaGFfdXJsIjtzOjM3OiJodHRwOi8vc3luZXJneS5sb2NhbC9pbWFnZXMvY2FwdGNoYXMvIjtzOjEyOiJjYXB0Y2hhX3BhdGgiO3M6NTQ6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL2NhcHRjaGFzLyI7czoxMjoiY2FwdGNoYV9mb250IjtzOjE6InkiO3M6MTI6ImNhcHRjaGFfcmFuZCI7czoxOiJ5IjtzOjIzOiJjYXB0Y2hhX3JlcXVpcmVfbWVtYmVycyI7czoxOiJuIjtzOjE3OiJlbmFibGVfZGJfY2FjaGluZyI7czoxOiJuIjtzOjE4OiJlbmFibGVfc3FsX2NhY2hpbmciO3M6MToibiI7czoxODoiZm9yY2VfcXVlcnlfc3RyaW5nIjtzOjE6Im4iO3M6MTM6InNob3dfcHJvZmlsZXIiO3M6MToibiI7czoxODoidGVtcGxhdGVfZGVidWdnaW5nIjtzOjE6Im4iO3M6MTU6ImluY2x1ZGVfc2Vjb25kcyI7czoxOiJuIjtzOjEzOiJjb29raWVfZG9tYWluIjtzOjA6IiI7czoxMToiY29va2llX3BhdGgiO3M6MDoiIjtzOjE3OiJ1c2VyX3Nlc3Npb25fdHlwZSI7czoxOiJjIjtzOjE4OiJhZG1pbl9zZXNzaW9uX3R5cGUiO3M6MjoiY3MiO3M6MjE6ImFsbG93X3VzZXJuYW1lX2NoYW5nZSI7czoxOiJ5IjtzOjE4OiJhbGxvd19tdWx0aV9sb2dpbnMiO3M6MToieSI7czoxNjoicGFzc3dvcmRfbG9ja291dCI7czoxOiJ5IjtzOjI1OiJwYXNzd29yZF9sb2Nrb3V0X2ludGVydmFsIjtzOjE6IjEiO3M6MjA6InJlcXVpcmVfaXBfZm9yX2xvZ2luIjtzOjE6InkiO3M6MjI6InJlcXVpcmVfaXBfZm9yX3Bvc3RpbmciO3M6MToieSI7czoyNDoicmVxdWlyZV9zZWN1cmVfcGFzc3dvcmRzIjtzOjE6Im4iO3M6MTk6ImFsbG93X2RpY3Rpb25hcnlfcHciO3M6MToieSI7czoyMzoibmFtZV9vZl9kaWN0aW9uYXJ5X2ZpbGUiO3M6MDoiIjtzOjE3OiJ4c3NfY2xlYW5fdXBsb2FkcyI7czoxOiJ5IjtzOjE1OiJyZWRpcmVjdF9tZXRob2QiO3M6ODoicmVkaXJlY3QiO3M6OToiZGVmdF9sYW5nIjtzOjc6ImVuZ2xpc2giO3M6ODoieG1sX2xhbmciO3M6MjoiZW4iO3M6MTI6InNlbmRfaGVhZGVycyI7czoxOiJ5IjtzOjExOiJnemlwX291dHB1dCI7czoxOiJuIjtzOjEzOiJsb2dfcmVmZXJyZXJzIjtzOjE6Im4iO3M6MTM6Im1heF9yZWZlcnJlcnMiO3M6MzoiNTAwIjtzOjExOiJ0aW1lX2Zvcm1hdCI7czoyOiJ1cyI7czoxNToic2VydmVyX3RpbWV6b25lIjtzOjQ6IlVQMTIiO3M6MTM6InNlcnZlcl9vZmZzZXQiO3M6MDoiIjtzOjE2OiJkYXlsaWdodF9zYXZpbmdzIjtzOjE6InkiO3M6MjE6ImRlZmF1bHRfc2l0ZV90aW1lem9uZSI7czo0OiJVUDEyIjtzOjE2OiJkZWZhdWx0X3NpdGVfZHN0IjtzOjE6InkiO3M6MTM6Im1haWxfcHJvdG9jb2wiO3M6NDoibWFpbCI7czoxMToic210cF9zZXJ2ZXIiO3M6MDoiIjtzOjEzOiJzbXRwX3VzZXJuYW1lIjtzOjA6IiI7czoxMzoic210cF9wYXNzd29yZCI7czowOiIiO3M6MTE6ImVtYWlsX2RlYnVnIjtzOjE6Im4iO3M6MTM6ImVtYWlsX2NoYXJzZXQiO3M6NToidXRmLTgiO3M6MTU6ImVtYWlsX2JhdGNobW9kZSI7czoxOiJuIjtzOjE2OiJlbWFpbF9iYXRjaF9zaXplIjtzOjA6IiI7czoxMToibWFpbF9mb3JtYXQiO3M6NToicGxhaW4iO3M6OToid29yZF93cmFwIjtzOjE6InkiO3M6MjI6ImVtYWlsX2NvbnNvbGVfdGltZWxvY2siO3M6MToiNSI7czoyMjoibG9nX2VtYWlsX2NvbnNvbGVfbXNncyI7czoxOiJ5IjtzOjg6ImNwX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MjE6ImVtYWlsX21vZHVsZV9jYXB0Y2hhcyI7czoxOiJuIjtzOjE2OiJsb2dfc2VhcmNoX3Rlcm1zIjtzOjE6InkiO3M6MTI6InNlY3VyZV9mb3JtcyI7czoxOiJ5IjtzOjE5OiJkZW55X2R1cGxpY2F0ZV9kYXRhIjtzOjE6InkiO3M6MjQ6InJlZGlyZWN0X3N1Ym1pdHRlZF9saW5rcyI7czoxOiJuIjtzOjE2OiJlbmFibGVfY2Vuc29yaW5nIjtzOjE6Im4iO3M6MTQ6ImNlbnNvcmVkX3dvcmRzIjtzOjA6IiI7czoxODoiY2Vuc29yX3JlcGxhY2VtZW50IjtzOjA6IiI7czoxMDoiYmFubmVkX2lwcyI7czowOiIiO3M6MTM6ImJhbm5lZF9lbWFpbHMiO3M6MDoiIjtzOjE2OiJiYW5uZWRfdXNlcm5hbWVzIjtzOjA6IiI7czoxOToiYmFubmVkX3NjcmVlbl9uYW1lcyI7czowOiIiO3M6MTA6ImJhbl9hY3Rpb24iO3M6ODoicmVzdHJpY3QiO3M6MTE6ImJhbl9tZXNzYWdlIjtzOjM0OiJUaGlzIHNpdGUgaXMgY3VycmVudGx5IHVuYXZhaWxhYmxlIjtzOjE1OiJiYW5fZGVzdGluYXRpb24iO3M6MjE6Imh0dHA6Ly93d3cueWFob28uY29tLyI7czoxNjoiZW5hYmxlX2Vtb3RpY29ucyI7czoxOiJ5IjtzOjEyOiJlbW90aWNvbl91cmwiO3M6MzY6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9zbWlsZXlzLyI7czoxOToicmVjb3VudF9iYXRjaF90b3RhbCI7czo0OiIxMDAwIjtzOjE3OiJuZXdfdmVyc2lvbl9jaGVjayI7czoxOiJ5IjtzOjE3OiJlbmFibGVfdGhyb3R0bGluZyI7czoxOiJuIjtzOjE3OiJiYW5pc2hfbWFza2VkX2lwcyI7czoxOiJ5IjtzOjE0OiJtYXhfcGFnZV9sb2FkcyI7czoyOiIxMCI7czoxMzoidGltZV9pbnRlcnZhbCI7czoxOiI4IjtzOjEyOiJsb2Nrb3V0X3RpbWUiO3M6MjoiMzAiO3M6MTU6ImJhbmlzaG1lbnRfdHlwZSI7czo3OiJtZXNzYWdlIjtzOjE0OiJiYW5pc2htZW50X3VybCI7czowOiIiO3M6MTg6ImJhbmlzaG1lbnRfbWVzc2FnZSI7czo1MDoiWW91IGhhdmUgZXhjZWVkZWQgdGhlIGFsbG93ZWQgcGFnZSBsb2FkIGZyZXF1ZW5jeS4iO3M6MTc6ImVuYWJsZV9zZWFyY2hfbG9nIjtzOjE6InkiO3M6MTk6Im1heF9sb2dnZWRfc2VhcmNoZXMiO3M6MzoiNTAwIjtzOjExOiJydGVfZW5hYmxlZCI7czoxOiJ5IjtzOjIyOiJydGVfZGVmYXVsdF90b29sc2V0X2lkIjtzOjE6IjEiO30=', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6MzY6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9hdmF0YXJzLyI7czoxMToiYXZhdGFyX3BhdGgiO3M6NTM6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL2F2YXRhcnMvIjtzOjE2OiJhdmF0YXJfbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNzoiYXZhdGFyX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEzOiJhdmF0YXJfbWF4X2tiIjtzOjI6IjUwIjtzOjEzOiJlbmFibGVfcGhvdG9zIjtzOjE6Im4iO3M6OToicGhvdG9fdXJsIjtzOjQyOiJodHRwOi8vc3luZXJneS5sb2NhbC9pbWFnZXMvbWVtYmVyX3Bob3Rvcy8iO3M6MTA6InBob3RvX3BhdGgiO3M6NTk6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjE1OiJwaG90b19tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE2OiJwaG90b19tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMjoicGhvdG9fbWF4X2tiIjtzOjI6IjUwIjtzOjE2OiJhbGxvd19zaWduYXR1cmVzIjtzOjE6InkiO3M6MTM6InNpZ19tYXhsZW5ndGgiO3M6MzoiNTAwIjtzOjIxOiJzaWdfYWxsb3dfaW1nX2hvdGxpbmsiO3M6MToibiI7czoyMDoic2lnX2FsbG93X2ltZ191cGxvYWQiO3M6MToibiI7czoxMToic2lnX2ltZ191cmwiO3M6NTA6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9zaWduYXR1cmVfYXR0YWNobWVudHMvIjtzOjEyOiJzaWdfaW1nX3BhdGgiO3M6Njc6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo2MDoiL1VzZXJzL2phbWVzL1Byb2plY3RzLzk2YmxhY2svU3luZXJneS9pbWFnZXMvcG1fYXR0YWNobWVudHMvIjtzOjIzOiJwcnZfbXNnX21heF9hdHRhY2htZW50cyI7czoxOiIzIjtzOjIyOiJwcnZfbXNnX2F0dGFjaF9tYXhzaXplIjtzOjM6IjI1MCI7czoyMDoicHJ2X21zZ19hdHRhY2hfdG90YWwiO3M6MzoiMTAwIjtzOjE5OiJwcnZfbXNnX2h0bWxfZm9ybWF0IjtzOjQ6InNhZmUiO3M6MTg6InBydl9tc2dfYXV0b19saW5rcyI7czoxOiJ5IjtzOjE3OiJwcnZfbXNnX21heF9jaGFycyI7czo0OiI2MDAwIjtzOjE5OiJtZW1iZXJsaXN0X29yZGVyX2J5IjtzOjExOiJ0b3RhbF9wb3N0cyI7czoyMToibWVtYmVybGlzdF9zb3J0X29yZGVyIjtzOjQ6ImRlc2MiO3M6MjA6Im1lbWJlcmxpc3Rfcm93X2xpbWl0IjtzOjI6IjIwIjt9', 'YTo2OntzOjE1OiJzYXZlX3RtcGxfZmlsZXMiO3M6MToieSI7czoxODoidG1wbF9maWxlX2Jhc2VwYXRoIjtzOjQ4OiIvVXNlcnMvamFtZXMvUHJvamVjdHMvOTZibGFjay9TeW5lcmd5L3RlbXBsYXRlcy8iO3M6ODoic2l0ZV80MDQiO3M6MDoiIjtzOjE5OiJzYXZlX3RtcGxfcmV2aXNpb25zIjtzOjE6Im4iO3M6MTg6Im1heF90bXBsX3JldmlzaW9ucyI7czoxOiI1IjtzOjExOiJzdHJpY3RfdXJscyI7czoxOiJuIjt9', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YToyOntzOjc6ImVtYWlsZWQiO2E6MDp7fXM6NDc6Ii9Vc2Vycy9qYW1lc21jZmFsbC9Qcm9qZWN0cy9TeW5lcmd5QVUvaW5kZXgucGhwIjtzOjMyOiJiZjViZGMzMGYwYWVmZDI4Mjc0MmU4MDUxNTAyMWY4MSI7fQ==');

/* Table structure for table `exp_snippets` */
DROP TABLE IF EXISTS `exp_snippets`;

CREATE TABLE `exp_snippets` (
  `snippet_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) NOT NULL,
  `snippet_name` varchar(75) NOT NULL,
  `snippet_contents` text,
  PRIMARY KEY (`snippet_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_specialty_templates` */
DROP TABLE IF EXISTS `exp_specialty_templates`;

CREATE TABLE `exp_specialty_templates` (
  `template_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_name` varchar(50) NOT NULL,
  `data_title` varchar(80) NOT NULL,
  `template_data` text NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_specialty_templates` */
INSERT INTO `exp_specialty_templates` VALUES(1, 1, 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>');
INSERT INTO `exp_specialty_templates` VALUES(2, 1, 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=\'content-type\' content=\'text/html; charset={charset}\' />\n\n{meta_refresh}\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>');
INSERT INTO `exp_specialty_templates` VALUES(3, 1, 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}');
INSERT INTO `exp_specialty_templates` VALUES(4, 1, 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n');
INSERT INTO `exp_specialty_templates` VALUES(5, 1, 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}');
INSERT INTO `exp_specialty_templates` VALUES(6, 1, 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}');
INSERT INTO `exp_specialty_templates` VALUES(7, 1, 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(8, 1, 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(9, 1, 'y', 'reset_password_notification', 'New Login Information', '{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(10, 1, 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(11, 1, 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe\'re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(12, 1, 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the \"{mailing_list}\" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}');
INSERT INTO `exp_specialty_templates` VALUES(13, 1, 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}');
INSERT INTO `exp_specialty_templates` VALUES(14, 1, 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}');
INSERT INTO `exp_specialty_templates` VALUES(15, 1, 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled �{message_subject}�.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(16, 1, 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(17, 2, 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>');
INSERT INTO `exp_specialty_templates` VALUES(18, 2, 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=\'content-type\' content=\'text/html; charset={charset}\' />\n\n{meta_refresh}\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>');
INSERT INTO `exp_specialty_templates` VALUES(19, 2, 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}');
INSERT INTO `exp_specialty_templates` VALUES(20, 2, 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n');
INSERT INTO `exp_specialty_templates` VALUES(21, 2, 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}');
INSERT INTO `exp_specialty_templates` VALUES(22, 2, 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}');
INSERT INTO `exp_specialty_templates` VALUES(23, 2, 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(24, 2, 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(25, 2, 'y', 'reset_password_notification', 'New Login Information', '{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(26, 2, 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(27, 2, 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe\'re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(28, 2, 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the \"{mailing_list}\" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}');
INSERT INTO `exp_specialty_templates` VALUES(29, 2, 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}');
INSERT INTO `exp_specialty_templates` VALUES(30, 2, 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}');
INSERT INTO `exp_specialty_templates` VALUES(31, 2, 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled �{message_subject}�.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(32, 2, 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}');

/* Table structure for table `exp_stats` */
DROP TABLE IF EXISTS `exp_stats`;

CREATE TABLE `exp_stats` (
  `stat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `total_members` mediumint(7) NOT NULL DEFAULT '0',
  `recent_member_id` int(10) NOT NULL DEFAULT '0',
  `recent_member` varchar(50) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `most_visitors` mediumint(7) NOT NULL DEFAULT '0',
  `most_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_cache_clear` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`stat_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_stats` */
INSERT INTO `exp_stats` VALUES(1, 1, 1, 1, '96black', 37, 0, 0, 0, 1366153158, 0, 0, 1366592411, 34, 1365643350, 1366678558);
INSERT INTO `exp_stats` VALUES(2, 2, 1, 1, '96black', 0, 0, 0, 0, 0, 0, 0, 1365848246, 6, 1365845962, 1366235389);

/* Table structure for table `exp_status_groups` */
DROP TABLE IF EXISTS `exp_status_groups`;

CREATE TABLE `exp_status_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_status_groups` */
INSERT INTO `exp_status_groups` VALUES(1, 1, 'Statuses');
INSERT INTO `exp_status_groups` VALUES(2, 2, 'Statuses');

/* Table structure for table `exp_status_no_access` */
DROP TABLE IF EXISTS `exp_status_no_access`;

CREATE TABLE `exp_status_no_access` (
  `status_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`status_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_statuses` */
DROP TABLE IF EXISTS `exp_statuses`;

CREATE TABLE `exp_statuses` (
  `status_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `status_order` int(3) unsigned NOT NULL,
  `highlight` varchar(30) NOT NULL,
  PRIMARY KEY (`status_id`),
  KEY `group_id` (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_statuses` */
INSERT INTO `exp_statuses` VALUES(1, 1, 1, 'open', 1, '009933');
INSERT INTO `exp_statuses` VALUES(2, 1, 1, 'closed', 2, '990000');
INSERT INTO `exp_statuses` VALUES(3, 2, 2, 'open', 1, '009933');
INSERT INTO `exp_statuses` VALUES(4, 2, 2, 'closed', 2, '990000');

/* Table structure for table `exp_template_groups` */
DROP TABLE IF EXISTS `exp_template_groups`;

CREATE TABLE `exp_template_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `group_order` int(3) unsigned NOT NULL,
  `is_site_default` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_template_groups` */
INSERT INTO `exp_template_groups` VALUES(1, 1, 'Routing', 1, 'y');
INSERT INTO `exp_template_groups` VALUES(5, 1, 'Home', 3, 'n');
INSERT INTO `exp_template_groups` VALUES(6, 1, 'Products', 4, 'n');
INSERT INTO `exp_template_groups` VALUES(8, 1, 'News', 6, 'n');
INSERT INTO `exp_template_groups` VALUES(9, 1, 'About-us', 7, 'n');
INSERT INTO `exp_template_groups` VALUES(10, 1, 'Contact-us', 8, 'n');
INSERT INTO `exp_template_groups` VALUES(11, 1, 'Services', 9, 'n');
INSERT INTO `exp_template_groups` VALUES(12, 1, 'Common', 10, 'n');
INSERT INTO `exp_template_groups` VALUES(13, 1, 'Featured-products', 10, 'n');
INSERT INTO `exp_template_groups` VALUES(14, 1, 'Case-studies', 11, 'n');
INSERT INTO `exp_template_groups` VALUES(15, 1, 'Search', 11, 'n');

/* Table structure for table `exp_template_member_groups` */
DROP TABLE IF EXISTS `exp_template_member_groups`;

CREATE TABLE `exp_template_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `template_group_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`template_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_template_no_access` */
DROP TABLE IF EXISTS `exp_template_no_access`;

CREATE TABLE `exp_template_no_access` (
  `template_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`template_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_templates` */
DROP TABLE IF EXISTS `exp_templates`;

CREATE TABLE `exp_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `template_name` varchar(50) NOT NULL,
  `save_template_file` char(1) NOT NULL DEFAULT 'n',
  `template_type` varchar(16) NOT NULL DEFAULT 'webpage',
  `template_data` mediumtext,
  `template_notes` text,
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `last_author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cache` char(1) NOT NULL DEFAULT 'n',
  `refresh` int(6) unsigned NOT NULL DEFAULT '0',
  `no_auth_bounce` varchar(50) NOT NULL DEFAULT '',
  `enable_http_auth` char(1) NOT NULL DEFAULT 'n',
  `allow_php` char(1) NOT NULL DEFAULT 'n',
  `php_parse_location` char(1) NOT NULL DEFAULT 'o',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_id`),
  KEY `group_id` (`group_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_templates` */
INSERT INTO `exp_templates` VALUES(1, 1, 1, 'index', 'y', 'webpage', '{embed=\"Common/header\"}\n\n{embed=\"Home/index\"}\n\n{embed=\"Common/footer\"}', '', 1365458557, 1, 'n', 0, '', 'n', 'y', 'o', 9032);
INSERT INTO `exp_templates` VALUES(4, 2, 1, 'monkey-butt', 'y', 'webpage', 'OH SNAP', '', 1355889015, 1, 'n', 0, '', 'n', 'n', 'o', 1);
INSERT INTO `exp_templates` VALUES(9, 1, 5, 'index', 'y', 'webpage', 'Home test', '', 1355945914, 1, 'n', 0, '', 'n', 'y', 'i', 4);
INSERT INTO `exp_templates` VALUES(10, 1, 6, 'index', 'y', 'webpage', '', '', 1355945909, 1, 'n', 0, '', 'n', 'y', 'o', 1725);
INSERT INTO `exp_templates` VALUES(12, 1, 8, 'index', 'y', 'webpage', '', '', 1355945899, 1, 'n', 0, '', 'n', 'n', 'o', 91);
INSERT INTO `exp_templates` VALUES(13, 1, 9, 'index', 'y', 'webpage', '', '', 1355945894, 1, 'n', 0, '', 'n', 'n', 'o', 109);
INSERT INTO `exp_templates` VALUES(14, 1, 10, 'index', 'y', 'webpage', '', '', 1355945889, 1, 'n', 0, '', 'n', 'y', 'i', 161);
INSERT INTO `exp_templates` VALUES(15, 1, 11, 'index', 'y', 'webpage', '', '', 1355945883, 1, 'n', 0, '', 'n', 'n', 'o', 274);
INSERT INTO `exp_templates` VALUES(16, 1, 12, 'index', 'y', 'webpage', '', '', 1355945878, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(17, 1, 12, 'header', 'y', 'webpage', '', '', 1362949938, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(18, 1, 12, 'footer', 'y', 'webpage', '', '', 1355946002, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(19, 1, 12, '_menu', 'y', 'webpage', '', '', 1362965438, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(20, 1, 6, 'product-page', 'y', 'webpage', '', '', 1358280171, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(22, 1, 6, 'category-listing', 'y', 'webpage', 'Category listing!', '', 1358280845, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(23, 1, 6, '_product-nav', 'y', 'webpage', '<?\n# We want to get the second to last segment of the URL to use in the breadcrumbs.\n$segments = explode(\"/\", $_SERVER[\"REQUEST_URI\"]);\n$last = $segments[count($segments) - 1]; # -1 because index starts from 0 (count doesn\'t).\n$secToLast = $segments[count($segments) - 2]; # -2 because index starts from 0 (count doesn\'t).\n\n# Category segment to match on is normally the last segment.\n$catSegment = $last;\n\n# If we get a channel back when checking the last segment , we\'re on a product page.\n# So we\'ll use the second to last segment instead.\nif (Synergy_Utils::getChannelForEntry($last)) {\n    $catSegment = $secToLast;\n}\n\n?>\n\n<h3>Products</h3>\n{exp:gwcode_catmenu group_id=\"1\" cat_url_title=\"<?=$catSegment?>\" id=\"sideMenu\"}\n<a href=\"/products/{complete_path}\" {if active} class=\"active\"{/if}>\n    {cat_name}\n</a>\n{/exp:gwcode_catmenu}\n<!--\n{exp:gwcode_categories channel=\"products\"}\n<a href=\"/products/{complete_path}\">{cat_name}</a>\n{/exp:gwcode_categories}\n-->\n\n{embed=\"Products/_enquiry-form\"}\n{embed=\"Products/_rental-form\"}', '', 1366083047, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(24, 1, 6, 'category-landing', 'y', 'webpage', '', '', 1358365005, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(26, 1, 6, '_category-listing-products-blocks', 'y', 'webpage', '', '', 1362954501, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(27, 1, 8, 'article-list', 'y', 'webpage', '', '', 1362957827, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(28, 1, 8, 'article', 'y', 'webpage', '', '', 1362957835, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(29, 1, 10, '_contact-form', 'y', 'webpage', '', '', 1363570256, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(30, 1, 13, 'index', 'y', 'webpage', '', '', 1363828125, 1, 'n', 0, '', 'n', 'n', 'o', 1);
INSERT INTO `exp_templates` VALUES(31, 1, 13, 'page-intros', 'y', 'webpage', '{exp:channel:entries channel=\"products\" entry_id=\"{embed:product_id}\" limit=\"1\"}\n<div class=\"additionalBlocks\">\n    <ul>\n        {reverse_related_entries id=\"featured_product\"}\n        <li class=\"{switch=\'||third\'}\">\n            <h3><a href=\"{url_title}\">{title}</a></h3>\n            <p>{featured_content_intro}</p>\n            <a href=\"{url_title}\" class=\"readMore\">Read More</a>\n        </li>\n        {/reverse_related_entries}\n    </ul>\n    <div class=\"clear\"></div>\n</div>\n{/exp:channel:entries}', '', 1363827515, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(32, 1, 13, 'page', 'y', 'webpage', '', '', 1363823952, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(33, 1, 6, 'featured_subpage', 'y', 'webpage', '', '', 1363829194, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(34, 1, 14, 'index', 'y', 'webpage', '', '', 1363904378, 1, 'n', 0, '', 'n', 'n', 'o', 53);
INSERT INTO `exp_templates` VALUES(35, 1, 14, 'article-list', 'y', 'webpage', '', '', 1363904459, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(36, 1, 14, 'article', 'y', 'webpage', '', '', 1363904454, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(37, 1, 11, '_services-nav', 'y', 'webpage', '<h3>Services</h3>\n<ul id=\"sideMenu\">\n    <li><a href=\"#\">Roads &amp; Services</a></li>\n    <li><a href=\"#\">Mapping</a></li>\n    <li><a href=\"#\">Lorem Ipsum</a> </li>\n    <li><a href=\"#\">Lorem Ipsum</a> </li>\n</ul>', null, 1365479093, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(38, 1, 11, 'service-landing', 'y', 'webpage', '<div id=\"content\" class=\"std\">\n    <div class=\"middle\">\n        <div id=\"sideBar\">\n            {embed=\"Services/_services-nav\"}\n        </div>\n\n        <div id=\"copy\" class=\"services\">\n            <div class=\"inner\">\n\n                <ul class=\"breadCrumb\">\n                    <li><a href=\"#\">Home</a></li>\n                    <li class=\"current\"><a href=\"#\">Services</a></li>\n                </ul>\n\n                <div class=\"main\">\n                    <h1>Services</h1>\n                </div>\n                <ul class=\"servicesList\">\n                    <li><img src=\"/images/temp/services-temp.jpg\" alt=\"Roads &amp; Services\" />\n                        <h2><a href=\"#\">Roads and Services</a></h2>\n                        <div class=\"excerpt\"><p>One of our core services, Synergy Positioning are experts in asphalt paving technologies. We offer a complete solution for all your paving requirements. Using a host of suppliers including: Moba Matic, Topcon & Roadware, we have complete control of any surfacing project.</p></div>\n                        <a href=\"#\" class=\"readMore\">Read More</a>\n                    </li>\n                    <li class=\"second\"><img src=\"/images/temp/services-temp.jpg\" alt=\"Mapping\" />\n                        <h2><a href=\"#\">Mapping</a></h2>\n                        <div class=\"excerpt\"><p>One of our core services, Synergy Positioning are experts in asphalt paving technologies. We offer a complete solution for all your paving requirements. Using a host of suppliers including: Moba Matic, Topcon & Roadware, we have complete control of any surfacing project.</p></div>\n                        <a href=\"#\" class=\"readMore\">Read More</a>\n                    </li>\n                    <li><img src=\"/images/temp/services-temp.jpg\" alt=\"Lorem Ipsum Dolor\" />\n                        <h2><a href=\"#\">Lorem Ipsum Dolor</a></h2>\n                        <div class=\"excerpt\"><p>One of our core services, Synergy Positioning are experts in asphalt paving technologies. We offer a complete solution for all your paving requirements. Using a host of suppliers including: Moba Matic, Topcon & Roadware, we have complete control of any surfacing project.</p></div>\n                        <a href=\"#\" class=\"readMore\">Read More</a>\n                    </li>\n                    <li class=\"second\"><img src=\"/images/temp/services-temp.jpg\" alt=\"Lorem Ipsum Dolor\" />\n                        <h2><a href=\"#\">Lorem Ipsum Dolor</a></h2>\n                        <div class=\"excerpt\"><p>One of our core services, Synergy Positioning are experts in asphalt paving technologies. We offer a complete solution for all your paving requirements. Using a host of suppliers including: Moba Matic, Topcon & Roadware, we have complete control of any surfacing project.</p></div>\n                        <a href=\"#\" class=\"readMore\">Read More</a>\n                    </li>\n                </ul>	\n\n\n            </div>\n\n\n\n        </div>\n        <div class=\"clear\"></div>\n    </div>\n</div>', null, 1365479411, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(39, 1, 11, 'service-listing', 'y', 'webpage', '\n<div id=\"copy\" class=\"services servicesDetail\">\n    <div class=\"topPageImage\">\n        <img src=\"/images/temp/services-temp-top.jpg\" alt=\"Roads &amp; Services\" />\n    </div>\n\n    <div class=\"inner\">\n\n        <ul class=\"breadCrumb\">\n            <li><a href=\"#\">Home</a></li>\n            <li><a href=\"#\">Services</a></li>\n            <li><a href=\"#\">Roads &amp; Services</a></li>\n            <li class=\"current\"><a href=\"#\">Survey &amp; Design</a></li>\n\n        </ul>\n\n        <div class=\"main\">\n            <h1>Survey &amp; Design</h1>\n            <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus.</p>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. Aliquam eleifend enim eleifend lacus pretium ac pretium risus sodales. Aliquam mi nisi, suscipit in ornare in, tempus a justo. Vivamus porta, justo in sollicitudin vulputate, velit massa congue mi, a vehicula enim metus a lectus. Sed felis tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n            <hr />\n\n            <p><img src=\"/images/temp/services-temp-detail-1.jpg\" alt=\"Survey &amp; Design\" class=\"alignLeft\" /></p>\n\n            <h3>Lorem Ipsum Dolor</h3>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullam]is tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n            <h3>Pellentesque mattis ultrices dapibu uis ullamcorp</h3>\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit m]is tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n\n            <hr />\n            <iframe class=\"alignRight\" width=\"417\" height=\"267\" src=\"http://www.youtube.com/embed/39zYdKe6vLs\" frameborder=\"0\" allowfullscreen></iframe>\n            <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus.</h3>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper</p>\n        </div>\n    </div>\n\n    <div class=\"additionalBlocks\">\n        <ul>\n            <li>\n                <h3><a href=\"#\">Survey &amp; Design</a></h3>\n                <p>Synergy Positioning uses an in-house survey team with many years of experience in the asphalt and roading industries. Our in-house experts in CAD, manages the design and control of final surfacing levels to ensure a smooth and cost effective result, on time and within budget</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n            <li>\n                <h3><a href=\"#\">Paving Control</a></h3>\n                <p>Our paving managers are the best in the industry. Taking full control of the paving crew, we manage the project from start to finish.</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n            <li class=\"third\">\n                <h3><a href=\"#\">Additional Information</a></h3>\n                <p>Sed sagittis tincidunt lobortis. In non ligula a urna blandit tempor. Aenean a leo ut mauris posuere aliquet. Phasellus faucibus convallis aliquet. Proin velit mi, tempus quis varius eu, suscipit sit amet nisl.</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n        </ul>\n        <div class=\"clear\"></div>\n    </div>\n\n\n</div>\n', null, 1365481672, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(40, 1, 11, 'service-page', 'y', 'webpage', '<div id=\"copy\" class=\"services servicesDetail\">\n    <div class=\"topPageImage\">\n        <img src=\"/images/temp/services-temp-top.jpg\" alt=\"Roads &amp; Services\" />\n    </div>\n\n    <div class=\"inner\">\n\n        <ul class=\"breadCrumb\">\n            <li><a href=\"#\">Home</a></li>\n            <li><a href=\"#\">Services</a></li>\n            <li><a href=\"#\">Roads &amp; Services</a></li>\n            <li class=\"current\"><a href=\"#\">Survey &amp; Design</a></li>\n\n        </ul>\n\n        <div class=\"main\">\n            <h1>Survey &amp; Design</h1>\n            <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus.</p>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. Aliquam eleifend enim eleifend lacus pretium ac pretium risus sodales. Aliquam mi nisi, suscipit in ornare in, tempus a justo. Vivamus porta, justo in sollicitudin vulputate, velit massa congue mi, a vehicula enim metus a lectus. Sed felis tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n            <hr />\n\n            <p><img src=\"/images/temp/services-temp-detail-1.jpg\" alt=\"Survey &amp; Design\" class=\"alignLeft\" /></p>\n\n            <h3>Lorem Ipsum Dolor</h3>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullam]is tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n            <h3>Pellentesque mattis ultrices dapibu uis ullamcorp</h3>\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit m]is tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n\n            <hr />\n            <iframe class=\"alignRight\" width=\"417\" height=\"267\" src=\"http://www.youtube.com/embed/39zYdKe6vLs\" frameborder=\"0\" allowfullscreen></iframe>\n            <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus.</h3>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper</p>\n        </div>\n    </div>\n\n    <div class=\"additionalBlocks\">\n        <ul>\n            <li>\n                <h3><a href=\"#\">Survey &amp; Design</a></h3>\n                <p>Synergy Positioning uses an in-house survey team with many years of experience in the asphalt and roading industries. Our in-house experts in CAD, manages the design and control of final surfacing levels to ensure a smooth and cost effective result, on time and within budget</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n            <li>\n                <h3><a href=\"#\">Paving Control</a></h3>\n                <p>Our paving managers are the best in the industry. Taking full control of the paving crew, we manage the project from start to finish.</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n            <li class=\"third\">\n                <h3><a href=\"#\">Additional Information</a></h3>\n                <p>Sed sagittis tincidunt lobortis. In non ligula a urna blandit tempor. Aenean a leo ut mauris posuere aliquet. Phasellus faucibus convallis aliquet. Proin velit mi, tempus quis varius eu, suscipit sit amet nisl.</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n        </ul>\n        <div class=\"clear\"></div>\n    </div>\n\n\n</div>\n', null, 1365481672, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(41, 1, 9, 'page', 'y', 'webpage', '<div id=\"copy\" class=\"about\">\n            <div class=\"topPageImage\">\n                <img src=\"/images/temp/about-temp.jpg\" alt=\"About Synergy Positioning Systems\" />\n            </div>\n\n            <div class=\"inner\">\n\n                <ul class=\"breadCrumb\">\n                    <li><a href=\"#\">Home</a></li>\n                    <li class=\"current\"><a href=\"#\">About Us</a></li>\n\n                </ul>\n\n                <div class=\"main\">\n                    <h1>About Us</h1>\n\n                    <p class=\"intro\">Synergy Positioning Systems Ltd is 100% New Zealand owned and operated. It was formed when two of the industry�s most trusted companies merged in 2006 - Synergy Equipment and Systems Ltd and Geodetic Instruments Ltd. The result is a company that brings you over 30 years experience delivering the most advanced and innovative technologies combined with a commitment to outstanding customer service.</p>\n\n                    <p>Our aim is to make your job faster and easier, more accurate and cost efficient. We\'ll do this by giving you expert and honest advice and by offering you the best solution from our range of innovative positioning systems.</p>\n\n                    <p>Reliability is paramount - equipment breakages cost time and money. There are many low quality brands on the market that just don\'t stand up to the rigours of our construction industry. We only sell reliable brands we can trust and service.</p>\n\n                    <p>Our experienced team are always on hand for after sales support, product training and servicing, giving you the confidence and skills to get the best out of your equipment. Our product range is extensive and always growing.</p>\n\n                    <h2>Why choose synergy?</h2>\n\n\n                </div>\n            </div>\n\n            <div class=\"additionalBlocks\">\n                <ul>\n                    <li>\n                        <h3>Simple really....</h3>\n                        <p>You are dealing with an established company representing world renowned brands that have been tried and tested in increasing productivity and reducing costs.</p>\n\n                    </li>\n                    <li>\n                        <h3>Your profit...</h3>\n                        <p>With our equipment and systems, you gain efficiency &amp; reduce costs, saving you money on all aspects of the job including: fuel, wages, wear &amp; tear, materials &amp; processing. </p>\n\n                    </li>\n                    <li>\n                        <h3>Our Products</h3>\n                        <p>We only represent equipment &amp; brands that we trust. Topcon, world leaders in positioning equipment for over 80 years &amp; well established throughout the world, offers reliable &amp; robust equipment for any application.</p>\n\n                    </li>\n                    <li>\n                        <h3>Our Experience</h3>\n                        <p>Synergy Positioning Systems Ltd is 100% New Zealand owned and operated. Our industry exprience spans more than 30 years. </p>\n\n                    </li>\n                    <li>\n                        <h3>Our Service</h3>\n                        <p>We are commited to our customers. Your success is our reward. Our factory trained technicians and in-house industry experts ensure that what we sell is fully supported.</p>\n\n                    </li>\n                </ul>\n                <div class=\"clear\"></div>\n            </div>\n\n\n\n        </div>', null, 1365632661, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(42, 1, 9, 'management-page', 'y', 'webpage', '<div id=\"copy\" class=\"management\">\n\n    <div class=\"inner\">\n\n        <ul class=\"breadCrumb\">\n            <li><a href=\"#\">Home</a></li>\n            <li><a href=\"#\">About Us</a></li>\n            <li class=\"current\"><a href=\"#\">Management</a></li>\n\n        </ul>\n\n        <div class=\"main\">\n            <h1>Management</h1>\n\n            <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet.</p>\n\n            <hr />\n\n            <h2>New Zealand</h2>\n\n            <ul class=\"profileList\">\n                <li class=\"first\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 3 </strong>\n                        <span class=\"title\">Title</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n\n                <li class=\"second\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"third\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"fourth\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"fifth\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n            </ul>\n\n            <hr />\n\n            <h2>Australia</h2>\n\n            <ul class=\"profileList\">\n                <li class=\"first\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 3 </strong>\n                        <span class=\"title\">Title</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n\n                <li class=\"second\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"third\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"fourth\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"fifth\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n            </ul>\n        </div>\n    </div>\n\n\n\n\n\n</div>', '', 1365635326, 1, 'n', 0, '', 'n', 'n', 'o', 12);
INSERT INTO `exp_templates` VALUES(43, 1, 15, 'index', 'y', 'webpage', '{exp:search:simple_form channel=\"news\"}\n        <p>\n                <label for=\"keywords\">Search:</label><br>\n                <input type=\"text\" name=\"keywords\" id=\"keywords\" value=\"\" size=\"18\" maxlength=\"100\">\n        </p>\n        <p>\n                <a href=\"{path=\'search/index\'}\">Advanced Search</a>\n        </p>\n        <p>\n                <input type=\"submit\" value=\"submit\" class=\"submit\">\n        </p>\n{/exp:search:simple_form}', '', 1365964420, 1, 'n', 0, '', 'n', 'n', 'o', 5);
INSERT INTO `exp_templates` VALUES(44, 1, 15, 'results', 'y', 'webpage', '{exp:search:simple_form channel=\"products|services\" result_page=\"search/results\"}\n        <p>\n                <label for=\"keywords\">Search:</label><br>\n                <input type=\"text\" name=\"keywords\" id=\"keywords\" value=\"\" size=\"18\" maxlength=\"100\">\n        </p>\n        <p>\n                <a href=\"{path=\'search/index\'}\">Advanced Search</a>\n        </p>\n        <p>\n                <input type=\"submit\" value=\"submit\" class=\"submit\">\n        </p>\n{/exp:search:simple_form}\n\n<table border=\"0\" cellpadding=\"6\" cellspacing=\"1\" width=\"100%\">\n    <tr>\n        <th>{lang:title}</th>\n        <th>{lang:excerpt}</th>\n        <th>{lang:author}</th>\n        <th>{lang:date}</th>\n        <th>{lang:total_comments}</th>\n        <th>{lang:recent_comments}</th>\n    </tr>\n\n{exp:search:search_results switch=\"resultRowOne|resultRowTwo\"}\n\n    <tr class=\"{switch}\">\n        <td width=\"30%\" valign=\"top\"><b><a href=\"{auto_path}\">{title}</a></b></td>\n        <td width=\"30%\" valign=\"top\">{excerpt}</td>\n        <td width=\"10%\" valign=\"top\"><a href=\"{member_path=\'member/index\'}\">{author}</a></td>\n        <td width=\"10%\" valign=\"top\">{entry_date format=\"%m/%d/%y\"}</td>\n        <td width=\"10%\" valign=\"top\">{comment_total}</td>\n        <td width=\"10%\" valign=\"top\">{recent_comment_date format=\"%m/%d/%y\"}</td>\n    </tr>\n\n    {if count == total_results}\n        </table>\n    {/if}\n\n    {paginate}\n        <p>Page {current_page} of {total_pages} pages {pagination_links}</p>\n    {/paginate}\n\n{/exp:search:search_results}', '', 1365965029, 1, 'n', 0, '', 'n', 'y', 'o', 45);
INSERT INTO `exp_templates` VALUES(45, 1, 15, '_search-form', 'y', 'webpage', '{exp:search:simple_form channel=\"products|services\" result_page=\"search/_results\"}\n        <p>\n                <label for=\"keywords\">Search:</label><br>\n                <input type=\"text\" name=\"keywords\" id=\"keywords\" value=\"\" size=\"18\" maxlength=\"100\">\n        </p>\n        <p>\n                <input type=\"submit\" value=\"submit\" class=\"submit\">\n        </p>\n{/exp:search:simple_form}', '', 1365965186, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(46, 1, 6, '_enquiry-form', 'y', 'webpage', 'f', '', 1365966324, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(47, 1, 6, '_rental-form', 'y', 'webpage', '', '', 1366083052, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(48, 1, 15, '_product_block', 'y', 'webpage', '', '', 1366096389, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(49, 1, 15, '_services_block', 'y', 'webpage', '', '', 1366104991, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(50, 1, 6, '_tab_downloads', 'y', 'webpage', '<div class=\"block\" id=\"second\">\n    <h4>Product Downloads Section</h4>\n    <ul class=\"table downloads\">\n        {product_downloads}\n        {download_file}\n        <li>\n            <strong>{file_title}</strong>\n            <span><?= Synergy_Utils::parseFileSize(\"{file_size}\"); ?></span>\n            <span>{extension}</span>\n        </li>\n        {/download_file}\n        {/product_downloads}\n    </ul>\n</div>', null, 1366331800, 1, 'n', 0, '', 'n', 'y', 'o', 0);

/* Table structure for table `exp_throttle` */
DROP TABLE IF EXISTS `exp_throttle`;

CREATE TABLE `exp_throttle` (
  `throttle_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL,
  `locked_out` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`throttle_id`),
  KEY `ip_address` (`ip_address`),
  KEY `last_activity` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_upload_no_access` */
DROP TABLE IF EXISTS `exp_upload_no_access`;

CREATE TABLE `exp_upload_no_access` (
  `upload_id` int(6) unsigned NOT NULL,
  `upload_loc` varchar(3) NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`upload_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_upload_prefs` */
DROP TABLE IF EXISTS `exp_upload_prefs`;

CREATE TABLE `exp_upload_prefs` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `server_path` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL,
  `allowed_types` varchar(3) NOT NULL DEFAULT 'img',
  `max_size` varchar(16) DEFAULT NULL,
  `max_height` varchar(6) DEFAULT NULL,
  `max_width` varchar(6) DEFAULT NULL,
  `properties` varchar(120) DEFAULT NULL,
  `pre_format` varchar(120) DEFAULT NULL,
  `post_format` varchar(120) DEFAULT NULL,
  `file_properties` varchar(120) DEFAULT NULL,
  `file_pre_format` varchar(120) DEFAULT NULL,
  `file_post_format` varchar(120) DEFAULT NULL,
  `cat_group` varchar(255) DEFAULT NULL,
  `batch_location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_upload_prefs` */
INSERT INTO `exp_upload_prefs` VALUES(1, 1, 'Product Images', '/Users/jamesmcfall/Projects/Synergy/uploads/product-images/', 'http://synergy.local/uploads/product-images/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(2, 1, 'Product Downloads', '/Users/jamesmcfall/Projects/Synergy/uploads/product-downloads/', 'http://synergy.local/uploads/product-downloads/', 'all', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(3, 1, 'Category Images', '/Users/jamesmcfall/Projects/Synergy/uploads/category-images/', 'http://synergy.local/uploads/category-images/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(4, 1, 'News Article Banner Image', '/Users/jamesmcfall/Projects/Synergy/uploads/news-images/', 'http://synergy.local/uploads/news-images/', 'img', '', '373', '866', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(5, 1, 'Case Study Banner Image', '/Users/jamesmcfall/Projects/Synergy/uploads/case-study-image/', 'http://synergy.local/uploads/case-study-image/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(6, 1, 'Home Page Banners', '/Users/jamesmcfall/Projects/Synergy/uploads/home-page-banners/', 'http://synergy.local/uploads/home-page-banners/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(7, 1, 'Services Banners', '/Users/jamesmcfall/Projects/Synergy/uploads/services-banners/', 'http://synergy.local/uploads/services-banners/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(8, 1, 'About Banners', '/Users/jamesmcfall/Projects/Synergy/uploads/about-banners/', 'http://synergy.local/uploads/about-banners/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(9, 1, 'Staff Pictures', '/Users/jamesmcfall/Projects/Synergy/uploads/staff-pictures/', 'http://synergy.local/uploads/staff-pictures/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(10, 1, 'Manufacturer Images', '/Users/jamesmcfall/Projects/Synergy/uploads/manufacturer-images/', 'http://synergy.local/uploads/manufacturer-images/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(11, 1, 'Contact Maps', '/Users/jamesmcfall/Projects/Synergy/uploads/contact-maps/', 'http://synergy.local/uploads/contact-maps/', 'img', '', '', '', '', '', '', '', '', '', '', null);

/* Table structure for table `exp_wygwam_configs` */
DROP TABLE IF EXISTS `exp_wygwam_configs`;

CREATE TABLE `exp_wygwam_configs` (
  `config_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `config_name` varchar(32) DEFAULT NULL,
  `settings` text,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_wygwam_configs` */
INSERT INTO `exp_wygwam_configs` VALUES(1, 'Basic', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTA6e2k6MDtzOjQ6IkJvbGQiO2k6MTtzOjY6Ikl0YWxpYyI7aToyO3M6OToiVW5kZXJsaW5lIjtpOjM7czo2OiJTdHJpa2UiO2k6NDtzOjEyOiJOdW1iZXJlZExpc3QiO2k6NTtzOjEyOiJCdWxsZXRlZExpc3QiO2k6NjtzOjQ6IkxpbmsiO2k6NztzOjY6IlVubGluayI7aTo4O3M6NjoiQW5jaG9yIjtpOjk7czo1OiJBYm91dCI7fXM6NjoiaGVpZ2h0IjtzOjM6IjIwMCI7czoxNDoicmVzaXplX2VuYWJsZWQiO3M6MToieSI7czoxMToiY29udGVudHNDc3MiO2E6MDp7fXM6MTA6InVwbG9hZF9kaXIiO3M6MDoiIjt9');
INSERT INTO `exp_wygwam_configs` VALUES(2, 'Full', 'YTo1OntzOjc6InRvb2xiYXIiO2E6Njc6e2k6MDtzOjY6IlNvdXJjZSI7aToxO3M6NDoiU2F2ZSI7aToyO3M6NzoiTmV3UGFnZSI7aTozO3M6NzoiUHJldmlldyI7aTo0O3M6OToiVGVtcGxhdGVzIjtpOjU7czozOiJDdXQiO2k6NjtzOjQ6IkNvcHkiO2k6NztzOjU6IlBhc3RlIjtpOjg7czo5OiJQYXN0ZVRleHQiO2k6OTtzOjEzOiJQYXN0ZUZyb21Xb3JkIjtpOjEwO3M6NToiUHJpbnQiO2k6MTE7czoxMjoiU3BlbGxDaGVja2VyIjtpOjEyO3M6NToiU2NheXQiO2k6MTM7czo0OiJVbmRvIjtpOjE0O3M6NDoiUmVkbyI7aToxNTtzOjQ6IkZpbmQiO2k6MTY7czo3OiJSZXBsYWNlIjtpOjE3O3M6OToiU2VsZWN0QWxsIjtpOjE4O3M6MTI6IlJlbW92ZUZvcm1hdCI7aToxOTtzOjQ6IkZvcm0iO2k6MjA7czo4OiJDaGVja2JveCI7aToyMTtzOjU6IlJhZGlvIjtpOjIyO3M6OToiVGV4dEZpZWxkIjtpOjIzO3M6ODoiVGV4dGFyZWEiO2k6MjQ7czo2OiJTZWxlY3QiO2k6MjU7czo2OiJCdXR0b24iO2k6MjY7czoxMToiSW1hZ2VCdXR0b24iO2k6Mjc7czoxMToiSGlkZGVuRmllbGQiO2k6Mjg7czoxOiIvIjtpOjI5O3M6NDoiQm9sZCI7aTozMDtzOjY6Ikl0YWxpYyI7aTozMTtzOjk6IlVuZGVybGluZSI7aTozMjtzOjY6IlN0cmlrZSI7aTozMztzOjk6IlN1YnNjcmlwdCI7aTozNDtzOjExOiJTdXBlcnNjcmlwdCI7aTozNTtzOjEyOiJOdW1iZXJlZExpc3QiO2k6MzY7czoxMjoiQnVsbGV0ZWRMaXN0IjtpOjM3O3M6NzoiT3V0ZGVudCI7aTozODtzOjY6IkluZGVudCI7aTozOTtzOjEwOiJCbG9ja3F1b3RlIjtpOjQwO3M6OToiQ3JlYXRlRGl2IjtpOjQxO3M6MTE6Ikp1c3RpZnlMZWZ0IjtpOjQyO3M6MTM6Ikp1c3RpZnlDZW50ZXIiO2k6NDM7czoxMjoiSnVzdGlmeVJpZ2h0IjtpOjQ0O3M6MTI6Ikp1c3RpZnlCbG9jayI7aTo0NTtzOjQ6IkxpbmsiO2k6NDY7czo2OiJVbmxpbmsiO2k6NDc7czo2OiJBbmNob3IiO2k6NDg7czo1OiJJbWFnZSI7aTo0OTtzOjU6IkZsYXNoIjtpOjUwO3M6NToiVGFibGUiO2k6NTE7czoxNDoiSG9yaXpvbnRhbFJ1bGUiO2k6NTI7czo2OiJTbWlsZXkiO2k6NTM7czoxMToiU3BlY2lhbENoYXIiO2k6NTQ7czo5OiJQYWdlQnJlYWsiO2k6NTU7czo4OiJSZWFkTW9yZSI7aTo1NjtzOjEwOiJFbWJlZE1lZGlhIjtpOjU3O3M6MToiLyI7aTo1ODtzOjY6IlN0eWxlcyI7aTo1OTtzOjY6IkZvcm1hdCI7aTo2MDtzOjQ6IkZvbnQiO2k6NjE7czo4OiJGb250U2l6ZSI7aTo2MjtzOjk6IlRleHRDb2xvciI7aTo2MztzOjc6IkJHQ29sb3IiO2k6NjQ7czo4OiJNYXhpbWl6ZSI7aTo2NTtzOjEwOiJTaG93QmxvY2tzIjtpOjY2O3M6NToiQWJvdXQiO31zOjY6ImhlaWdodCI7czozOiIyMDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ==');
INSERT INTO `exp_wygwam_configs` VALUES(3, 'News', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTE6e2k6MDtzOjY6IkZvcm1hdCI7aToxO3M6NDoiQm9sZCI7aToyO3M6NjoiSXRhbGljIjtpOjM7czo5OiJVbmRlcmxpbmUiO2k6NDtzOjY6IlN0cmlrZSI7aTo1O3M6MTI6Ik51bWJlcmVkTGlzdCI7aTo2O3M6MTI6IkJ1bGxldGVkTGlzdCI7aTo3O3M6NDoiTGluayI7aTo4O3M6NjoiVW5saW5rIjtpOjk7czo2OiJBbmNob3IiO2k6MTA7czo1OiJBYm91dCI7fXM6NjoiaGVpZ2h0IjtzOjM6IjIwMCI7czoxNDoicmVzaXplX2VuYWJsZWQiO3M6MToieSI7czoxMToiY29udGVudHNDc3MiO2E6MDp7fXM6MTA6InVwbG9hZF9kaXIiO3M6MDoiIjt9');
INSERT INTO `exp_wygwam_configs` VALUES(4, 'Featured Products Content', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTg6e2k6MDtzOjY6IkZvcm1hdCI7aToxO3M6MTE6Ikp1c3RpZnlMZWZ0IjtpOjI7czoxMzoiSnVzdGlmeUNlbnRlciI7aTozO3M6MTI6Ikp1c3RpZnlSaWdodCI7aTo0O3M6MTI6Ikp1c3RpZnlCbG9jayI7aTo1O3M6NDoiQm9sZCI7aTo2O3M6NjoiSXRhbGljIjtpOjc7czo5OiJVbmRlcmxpbmUiO2k6ODtzOjY6IlN0cmlrZSI7aTo5O3M6MTI6Ik51bWJlcmVkTGlzdCI7aToxMDtzOjEyOiJCdWxsZXRlZExpc3QiO2k6MTE7czo1OiJJbWFnZSI7aToxMjtzOjU6IkZsYXNoIjtpOjEzO3M6MTA6IkVtYmVkTWVkaWEiO2k6MTQ7czo1OiJBYm91dCI7aToxNTtzOjQ6IkxpbmsiO2k6MTY7czo2OiJVbmxpbmsiO2k6MTc7czo2OiJBbmNob3IiO31zOjY6ImhlaWdodCI7czozOiIyMDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ==');
INSERT INTO `exp_wygwam_configs` VALUES(5, 'Synergy Default', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTk6e2k6MDtzOjY6IkZvcm1hdCI7aToxO3M6NDoiQm9sZCI7aToyO3M6NjoiSXRhbGljIjtpOjM7czo5OiJVbmRlcmxpbmUiO2k6NDtzOjY6IlN0cmlrZSI7aTo1O3M6MTE6Ikp1c3RpZnlMZWZ0IjtpOjY7czoxMzoiSnVzdGlmeUNlbnRlciI7aTo3O3M6MTI6Ikp1c3RpZnlSaWdodCI7aTo4O3M6MTI6Ikp1c3RpZnlCbG9jayI7aTo5O3M6MTI6Ik51bWJlcmVkTGlzdCI7aToxMDtzOjEyOiJCdWxsZXRlZExpc3QiO2k6MTE7czo0OiJMaW5rIjtpOjEyO3M6NjoiVW5saW5rIjtpOjEzO3M6NjoiQW5jaG9yIjtpOjE0O3M6NToiSW1hZ2UiO2k6MTU7czo1OiJGbGFzaCI7aToxNjtzOjE0OiJIb3Jpem9udGFsUnVsZSI7aToxNztzOjEwOiJFbWJlZE1lZGlhIjtpOjE4O3M6NToiQWJvdXQiO31zOjY6ImhlaWdodCI7czozOiI0MDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ==');


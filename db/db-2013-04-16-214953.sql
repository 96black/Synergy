/* Table structure for table `exp_accessories` */
DROP TABLE IF EXISTS `exp_accessories`;

CREATE TABLE `exp_accessories` (
  `accessory_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(75) NOT NULL DEFAULT '',
  `member_groups` varchar(50) NOT NULL DEFAULT 'all',
  `controllers` text,
  `accessory_version` varchar(12) NOT NULL,
  PRIMARY KEY (`accessory_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_accessories` */
INSERT INTO `exp_accessories` VALUES(1, 'Expressionengine_info_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.0');
INSERT INTO `exp_accessories` VALUES(2, 'Mx_cloner_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|sites|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.0.3');

/* Table structure for table `exp_actions` */
DROP TABLE IF EXISTS `exp_actions`;

CREATE TABLE `exp_actions` (
  `action_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_actions` */
INSERT INTO `exp_actions` VALUES(1, 'Comment', 'insert_new_comment');
INSERT INTO `exp_actions` VALUES(2, 'Comment_mcp', 'delete_comment_notification');
INSERT INTO `exp_actions` VALUES(3, 'Comment', 'comment_subscribe');
INSERT INTO `exp_actions` VALUES(4, 'Comment', 'edit_comment');
INSERT INTO `exp_actions` VALUES(5, 'Email', 'send_email');
INSERT INTO `exp_actions` VALUES(6, 'Safecracker', 'submit_entry');
INSERT INTO `exp_actions` VALUES(7, 'Safecracker', 'combo_loader');
INSERT INTO `exp_actions` VALUES(8, 'Search', 'do_search');
INSERT INTO `exp_actions` VALUES(9, 'Channel', 'insert_new_entry');
INSERT INTO `exp_actions` VALUES(10, 'Channel', 'filemanager_endpoint');
INSERT INTO `exp_actions` VALUES(11, 'Channel', 'smiley_pop');
INSERT INTO `exp_actions` VALUES(12, 'Member', 'registration_form');
INSERT INTO `exp_actions` VALUES(13, 'Member', 'register_member');
INSERT INTO `exp_actions` VALUES(14, 'Member', 'activate_member');
INSERT INTO `exp_actions` VALUES(15, 'Member', 'member_login');
INSERT INTO `exp_actions` VALUES(16, 'Member', 'member_logout');
INSERT INTO `exp_actions` VALUES(17, 'Member', 'retrieve_password');
INSERT INTO `exp_actions` VALUES(18, 'Member', 'reset_password');
INSERT INTO `exp_actions` VALUES(19, 'Member', 'send_member_email');
INSERT INTO `exp_actions` VALUES(20, 'Member', 'update_un_pw');
INSERT INTO `exp_actions` VALUES(21, 'Member', 'member_search');
INSERT INTO `exp_actions` VALUES(22, 'Member', 'member_delete');
INSERT INTO `exp_actions` VALUES(23, 'Rte', 'get_js');
INSERT INTO `exp_actions` VALUES(24, 'Playa_mcp', 'filter_entries');
INSERT INTO `exp_actions` VALUES(25, 'Freeform', 'save_form');

/* Table structure for table `exp_captcha` */
DROP TABLE IF EXISTS `exp_captcha`;

CREATE TABLE `exp_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_categories` */
DROP TABLE IF EXISTS `exp_categories`;

CREATE TABLE `exp_categories` (
  `cat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `parent_id` int(4) unsigned NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_url_title` varchar(75) NOT NULL,
  `cat_description` text,
  `cat_image` varchar(120) DEFAULT NULL,
  `cat_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `group_id` (`group_id`),
  KEY `cat_name` (`cat_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_categories` */
INSERT INTO `exp_categories` VALUES(1, 1, 1, 0, 'UAV', 'uav', '', '{filedir_3}cat-image.jpg', 3);
INSERT INTO `exp_categories` VALUES(2, 1, 1, 0, 'Optical', 'optical', 'Category description here', '{filedir_3}cat-image.jpg', 2);
INSERT INTO `exp_categories` VALUES(3, 1, 1, 2, 'Total Station', 'total-station', '', '{filedir_3}cat-image.jpg', 3);
INSERT INTO `exp_categories` VALUES(4, 1, 1, 3, 'Robotic', 'robotic', '', '{filedir_3}cat-image.jpg', 2);
INSERT INTO `exp_categories` VALUES(5, 1, 1, 3, 'Windows Based', 'windows-based', '', '{filedir_3}cat-image.jpg', 3);
INSERT INTO `exp_categories` VALUES(6, 1, 1, 3, 'Construction', 'construction', '', '{filedir_3}cat-image.jpg', 1);
INSERT INTO `exp_categories` VALUES(7, 1, 1, 0, 'GPS Equipment', 'gps-equipment', 'Lorem ipsum stuff', '{filedir_3}cat-image.jpg', 1);
INSERT INTO `exp_categories` VALUES(8, 1, 1, 2, 'Levels', 'levels', 'Test content', '{filedir_3}cat-image.jpg', 1);
INSERT INTO `exp_categories` VALUES(9, 1, 1, 2, 'Theyodolites', 'theyodolites', 'Test content', '{filedir_3}cat-image.jpg', 2);
INSERT INTO `exp_categories` VALUES(10, 1, 2, 0, 'Roads And Surfaces', 'roads-and-surfaces', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim. ', '{filedir_3}services-temp-top.jpg', 3);
INSERT INTO `exp_categories` VALUES(11, 1, 2, 0, 'Mapping', 'mapping', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim. ', '{filedir_3}services-temp-top.jpg', 2);
INSERT INTO `exp_categories` VALUES(12, 1, 2, 0, 'Lorem ipsum', 'lorem-ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim. ', '{filedir_3}services-temp-top.jpg', 1);

/* Table structure for table `exp_category_field_data` */
DROP TABLE IF EXISTS `exp_category_field_data`;

CREATE TABLE `exp_category_field_data` (
  `cat_id` int(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_category_field_data` */
INSERT INTO `exp_category_field_data` VALUES(1, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(2, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(3, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(4, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(5, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(6, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(7, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(8, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(9, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(10, 1, 2);
INSERT INTO `exp_category_field_data` VALUES(11, 1, 2);
INSERT INTO `exp_category_field_data` VALUES(12, 1, 2);

/* Table structure for table `exp_category_fields` */
DROP TABLE IF EXISTS `exp_category_fields`;

CREATE TABLE `exp_category_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `field_label` varchar(50) NOT NULL DEFAULT '',
  `field_type` varchar(12) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_maxl` smallint(3) NOT NULL DEFAULT '128',
  `field_ta_rows` tinyint(2) NOT NULL DEFAULT '8',
  `field_default_fmt` varchar(40) NOT NULL DEFAULT 'none',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`field_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_category_groups` */
DROP TABLE IF EXISTS `exp_category_groups`;

CREATE TABLE `exp_category_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `sort_order` char(1) NOT NULL DEFAULT 'a',
  `exclude_group` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `can_edit_categories` text,
  `can_delete_categories` text,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_category_groups` */
INSERT INTO `exp_category_groups` VALUES(1, 1, 'Product Categories', 'a', 0, 'all', '', '');
INSERT INTO `exp_category_groups` VALUES(2, 1, 'Service Categories', 'a', 0, 'all', '', '');

/* Table structure for table `exp_category_posts` */
DROP TABLE IF EXISTS `exp_category_posts`;

CREATE TABLE `exp_category_posts` (
  `entry_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_category_posts` */
INSERT INTO `exp_category_posts` VALUES(1, 2);
INSERT INTO `exp_category_posts` VALUES(1, 3);
INSERT INTO `exp_category_posts` VALUES(1, 6);
INSERT INTO `exp_category_posts` VALUES(2, 2);
INSERT INTO `exp_category_posts` VALUES(2, 3);
INSERT INTO `exp_category_posts` VALUES(2, 6);
INSERT INTO `exp_category_posts` VALUES(3, 2);
INSERT INTO `exp_category_posts` VALUES(3, 3);
INSERT INTO `exp_category_posts` VALUES(3, 6);
INSERT INTO `exp_category_posts` VALUES(26, 10);
INSERT INTO `exp_category_posts` VALUES(35, 11);

/* Table structure for table `exp_channel_data` */
DROP TABLE IF EXISTS `exp_channel_data`;

CREATE TABLE `exp_channel_data` (
  `entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `field_id_1` text,
  `field_ft_1` tinytext,
  `field_id_2` text,
  `field_ft_2` tinytext,
  `field_id_3` text,
  `field_ft_3` tinytext,
  `field_id_4` text,
  `field_ft_4` tinytext,
  `field_id_5` text,
  `field_ft_5` tinytext,
  `field_id_6` text,
  `field_ft_6` tinytext,
  `field_id_7` text,
  `field_ft_7` tinytext,
  `field_id_8` text,
  `field_ft_8` tinytext,
  `field_id_9` text,
  `field_ft_9` tinytext,
  `field_id_10` text,
  `field_ft_10` tinytext,
  `field_id_11` text,
  `field_ft_11` tinytext,
  `field_id_12` text,
  `field_ft_12` tinytext,
  `field_id_13` text,
  `field_ft_13` tinytext,
  `field_id_14` text,
  `field_ft_14` tinytext,
  `field_id_15` text,
  `field_ft_15` tinytext,
  `field_id_16` int(10) DEFAULT '0',
  `field_ft_16` tinytext,
  `field_id_17` text,
  `field_ft_17` tinytext,
  `field_id_18` text,
  `field_ft_18` tinytext,
  `field_id_19` text,
  `field_ft_19` tinytext,
  `field_id_20` text,
  `field_ft_20` tinytext,
  `field_id_22` text,
  `field_ft_22` tinytext,
  `field_id_23` text,
  `field_ft_23` tinytext,
  `field_id_24` text,
  `field_ft_24` tinytext,
  `field_id_25` text,
  `field_ft_25` tinytext,
  `field_id_26` text,
  `field_ft_26` tinytext,
  `field_id_27` text,
  `field_ft_27` tinytext,
  `field_id_28` text,
  `field_ft_28` tinytext,
  `field_id_29` text,
  `field_ft_29` tinytext,
  `field_id_30` text,
  `field_ft_30` tinytext,
  `field_id_31` text,
  `field_ft_31` tinytext,
  `field_id_32` text,
  `field_ft_32` tinytext,
  `field_id_33` text,
  `field_ft_33` tinytext,
  `field_id_34` text,
  `field_ft_34` tinytext,
  `field_id_35` text,
  `field_ft_35` tinytext,
  `field_id_36` text,
  `field_ft_36` tinytext,
  `field_id_37` int(10) DEFAULT '0',
  `field_ft_37` tinytext,
  `field_id_38` text,
  `field_ft_38` tinytext,
  `field_id_39` text,
  `field_ft_39` tinytext,
  `field_id_40` text,
  `field_ft_40` tinytext,
  `field_id_42` text,
  `field_ft_42` tinytext,
  `field_id_43` text,
  `field_ft_43` tinytext,
  `field_id_44` text,
  `field_ft_44` tinytext,
  `field_id_45` text,
  `field_ft_45` tinytext,
  `field_id_46` text,
  `field_ft_46` tinytext,
  `field_id_47` text,
  `field_ft_47` tinytext,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_channel_data` */
INSERT INTO `exp_channel_data` VALUES(1, 1, 1, 'NZD\nAUD\n', 'none', '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', 'Yes', 'none', '1', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(2, 1, 1, 'NZD\nAUD\n', 'none', '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', 'Yes', 'none', '1', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '{filedir_1}mega-menu-feature-product-image_(1).jpg', 'none', '', 'none', '', 'none', '', 'none', 5, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(3, 1, 1, 'NZD\nAUD\n', 'none', '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '', 'none', '', 'none', '1', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(4, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'All', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(5, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'All', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(6, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'All', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(7, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'All', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(8, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'All', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(9, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'All', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(10, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 'All', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(11, 1, 4, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quam justo, egestas sed tincidunt id, feugiat eu urna. Suspendisse ornare, magna cursus tristique ullamcorper, massa velit ultricies metus, eu ultrices massa eros nec tortor. Sed auctor vehicula quam, ac condimentum mauris tristique non. Sed sed sodales nisi. Aliquam erat volutpat. Vivamus sit amet felis et urna lacinia ullamcorper eget et metus. Nunc laoreet eleifend molestie.</p>', 'none', '1', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', 'NZ\nAU\n', 'none', '1', 'none', '<p>\n	G&#39;day Mate.</p>\n<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quam justo, egestas sed tincidunt id, feugiat eu urna. Suspendisse ornare, magna cursus tristique ullamcorper, massa velit ultricies metus, eu ultrices massa eros nec tortor. Sed auctor vehicula quam, ac condimentum mauris tristique non. Sed sed sodales nisi. Aliquam erat volutpat. Vivamus sit amet felis et urna lacinia ullamcorper eget et metus. Nunc laoreet eleifend molestie.</p>', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(12, 1, 2, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 1, 'none', '1', 'none', 'The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(13, 1, 2, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 2, 'none', '1', 'none', 'The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(14, 1, 2, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 3, 'none', '1', 'none', 'The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(15, 1, 2, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 4, 'none', '1', 'none', 'The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(16, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', 'All', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(17, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', 'All', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(18, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', 'All', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(19, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', 'All', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(20, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', 'All', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(21, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', 'All', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(22, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', 'All', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(23, 1, 8, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '1', 'none', '1', 'none', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non felis dignissim leo faucibus commodo. Aliquam urna velit, sollicitudin eget mollis ac, condimentum quis sapien. Aliquam aliquam vestibulum libero. Donec scelerisque lorem sollicitudin leo rhoncus eget dignissim felis blandit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ', 'none', '{filedir_6}services.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', 'All', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(24, 1, 8, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '1', 'none', '1', 'none', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non felis dignissim leo faucibus commodo. Aliquam urna velit, sollicitudin eget mollis ac, condimentum quis sapien. Aliquam aliquam vestibulum libero. Donec scelerisque lorem sollicitudin leo rhoncus eget dignissim felis blandit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ', 'none', '{filedir_6}products.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', 'All', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(25, 1, 8, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '1', 'none', '1', 'none', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non felis dignissim leo faucibus commodo. Aliquam urna velit, sollicitudin eget mollis ac, condimentum quis sapien. Aliquam aliquam vestibulum libero. Donec scelerisque lorem sollicitudin leo rhoncus eget dignissim felis blandit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ', 'none', '{filedir_6}about.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', 'NZ', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(26, 1, 7, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '1', 'none', '{filedir_7}services-temp-top.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', 'NZ', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(27, 1, 10, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<h1>\n	<strong style=\"font-size: 12px;\">Synergy Positioning Systems Ltd is 100% New Zealand owned and operated. It was formed when two of the industry&rsquo;s most trusted companies merged in 2006 - Synergy Equipment and Systems Ltd and Geodetic Instruments Ltd. The result is a company that brings you over 30 years experience delivering the most advanced and innovative technologies combined with a commitment to outstanding customer service.</strong></h1>\n<p>\n	Our aim is to make your job faster and easier, more accurate and cost efficient. We&#39;ll do this by giving you expert and honest advice and by offering you the best solution from our range of innovative positioning systems.<br />\n	Reliability is paramount - equipment breakages cost time and money. There are many low quality brands on the market that just don&#39;t stand up to the rigours of our construction industry. We only sell reliable brands we can trust and service.</p>\n<p>\n	Our experienced team are always on hand for after sales support, product training and servicing, giving you the confidence and skills to get the best out of your equipment. Our product range is extensive and always growing.</p>\n<h2>\n	Why choose synergy?</h2>', 'none', '1', 'none', '{filedir_8}about-temp.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '1', 'none', '<p>\n	<strong>AUS CONTENT Synergy Positioning Systems Ltd is 100% New Zealand owned and operated. It was formed when two of the industry&rsquo;s most trusted companies merged in 2006 - Synergy Equipment and Systems Ltd and Geodetic Instruments Ltd. The result is a company that brings you over 30 years experience delivering the most advanced and innovative technologies combined with a commitment to outstanding customer service.</strong></p>\n<p>\n	Our aim is to make your job faster and easier, more accurate and cost efficient. We&#39;ll do this by giving you expert and honest advice and by offering you the best solution from our range of innovative positioning systems.<br />\n	Reliability is paramount - equipment breakages cost time and money. There are many low quality brands on the market that just don&#39;t stand up to the rigours of our construction industry. We only sell reliable brands we can trust and service.</p>\n<p>\n	Our experienced team are always on hand for after sales support, product training and servicing, giving you the confidence and skills to get the best out of your equipment. Our product range is extensive and always growing.</p>\n<h2>\n	Why choose synergy?</h2>', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(28, 1, 9, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<p>\n	<strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</strong></p>\n<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet.</p>', 'none', '', null, '', null, '', null, 'NZ\nAU\n', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(29, 1, 11, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '{filedir_10}footer-topcon.gif', 'none', '{filedir_10}topcon-logo.gif', 'none', '{filedir_10}mega-menu-feature-product-logo.jpg', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(30, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<p>\n	Test NZ article</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, 'NZ', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(31, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<p>\n	AU only!</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, 'AU', 'xhtml', '', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(32, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	NZ only case study</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, 'NZ', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(33, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	AU only.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, 'AU', 'xhtml', '', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(34, 1, 8, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '1', 'none', '1', 'none', 'Australia lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non felis dignissim leo faucibus commodo. Aliquam urna velit, sollicitudin eget mollis ac, condimentum quis sapien. Aliquam aliquam vestibulum libero. Donec scelerisque lorem sollicitudin leo rhoncus eget dignissim felis blandit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ', 'none', '{filedir_6}about.jpg', 'none', '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 'AU', 'xhtml', '', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(35, 1, 7, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '1', 'none', '{filedir_7}services-temp-top.jpg', 'none', '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 'All', 'xhtml');
INSERT INTO `exp_channel_data` VALUES(36, 1, 10, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<p>\n	Test NZ content</p>', 'none', '', 'none', '{filedir_8}about-temp.jpg', 'none', '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', 'none', '<p>\n	Test AU content</p>', 'none', '', null, '', null, '', null, '', null);

/* Table structure for table `exp_channel_entries_autosave` */
DROP TABLE IF EXISTS `exp_channel_entries_autosave`;

CREATE TABLE `exp_channel_entries_autosave` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original_entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  `entry_data` text,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_channel_fields` */
DROP TABLE IF EXISTS `exp_channel_fields`;

CREATE TABLE `exp_channel_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL,
  `field_label` varchar(50) NOT NULL,
  `field_instructions` text,
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_pre_populate` char(1) NOT NULL DEFAULT 'n',
  `field_pre_channel_id` int(6) unsigned DEFAULT NULL,
  `field_pre_field_id` int(6) unsigned DEFAULT NULL,
  `field_related_to` varchar(12) NOT NULL DEFAULT 'channel',
  `field_related_id` int(6) unsigned NOT NULL DEFAULT '0',
  `field_related_orderby` varchar(12) NOT NULL DEFAULT 'date',
  `field_related_sort` varchar(4) NOT NULL DEFAULT 'desc',
  `field_related_max` smallint(4) NOT NULL DEFAULT '0',
  `field_ta_rows` tinyint(2) DEFAULT '8',
  `field_maxl` smallint(3) DEFAULT NULL,
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_search` char(1) NOT NULL DEFAULT 'n',
  `field_is_hidden` char(1) NOT NULL DEFAULT 'n',
  `field_fmt` varchar(40) NOT NULL DEFAULT 'xhtml',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_order` int(3) unsigned NOT NULL,
  `field_content_type` varchar(20) NOT NULL DEFAULT 'any',
  `field_settings` text,
  PRIMARY KEY (`field_id`),
  KEY `group_id` (`group_id`),
  KEY `field_type` (`field_type`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_channel_fields` */
INSERT INTO `exp_channel_fields` VALUES(1, 1, 1, 'product_prices', 'Product Prices', 'Enter the AU and NZ prices for this product in here and they will be dynamically output to the customer based on the website they are viewing.', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjIiO3M6ODoibWF4X3Jvd3MiO3M6MToiMiI7czo3OiJjb2xfaWRzIjthOjI6e2k6MDtzOjE6IjEiO2k6MTtzOjE6IjIiO319');
INSERT INTO `exp_channel_fields` VALUES(2, 1, 1, 'product_description', 'Product Description', 'On the product page, only the first couple of paragraphs will be shown until the user presses \"read more\"', 'wygwam', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIyIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(3, 1, 1, 'technical_specifications', 'Technical Specifications', 'Below you can create the technical specifications section for this product. Once a title is specified in the left column (i.e. Measurement Range) you can create a table in the right column of specifications relevant to this title. ', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MToiMyI7aToxO3M6MToiNCI7fX0=');
INSERT INTO `exp_channel_fields` VALUES(4, 1, 1, 'product_videos', 'Product Videos', 'Use this field to embed a Youtube video and give a title/description to the video. Any videos will appear under the \"Videos\" tab for this product.', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 4, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mzp7aTowO2k6NjtpOjE7aTo3O2k6MjtpOjg7fX0=');
INSERT INTO `exp_channel_fields` VALUES(5, 1, 1, 'rental_sections', 'Rental Sections', 'Add as many paragraphs to the rental section as you want.', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO2k6OTtpOjE7aToxMDt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(6, 1, 1, 'rental_pricing_nz', 'Rental Pricing (NZ)', 'This field controls the contents of the \"Availability & Costs\" section under the rental tab (for the NZ site).', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 6, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMTEiO2k6MTtzOjI6IjEyIjt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(7, 1, 1, 'rental_pricing_au', 'Rental Pricing (AU)', 'This field controls the contents of the \"Availability & Costs\" section under the rental tab (for the AU site).', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 7, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMTMiO2k6MTtzOjI6IjE0Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(8, 1, 1, 'featured_carousel_images', 'Featured Carousel Images', 'If this product is to be a featured product, you can upload many banner images to appear at the top of its featured page.\n\nThe banner will cycle between these images.', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 8, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMTUiO2k6MTtzOjI6IjI0Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(9, 1, 1, 'is_featured_product', 'Is Featured Product?', 'If you want this product to be a featured product, tick this checkbox.\n\nPlease make sure you have created some featured product entries for this product first (in the publish menu).', 'checkboxes', 'Yes', 'n', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 9, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');
INSERT INTO `exp_channel_fields` VALUES(10, 1, 1, 'product_images', 'Product Images', '', 'matrix', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 10, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6MTp7aTowO3M6MjoiMTYiO319');
INSERT INTO `exp_channel_fields` VALUES(11, 1, 1, 'product_downloads', 'Product Downloads', 'Upload files to be available in the \"downloads\" tab', 'matrix', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 11, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMTciO2k6MTtzOjI6IjE4Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(12, 1, 3, 'article_content', 'Article Content', 'This is the main body content of the news item. Please note the first paragraph will be automatically made bold.', 'wygwam', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(13, 1, 3, 'article_banner_image', 'Article Banner Image', 'This is the image that appears at the top of the news article and on the news page. Please note the images should be exactly 866px by 373px and will have a thumbnail automatically cropped to 396px by 88px for the news page.', 'file', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiNCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(14, 1, 4, 'contact_introduction', 'Contact Introduction', 'The initial content before the branch information.', 'wygwam', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(15, 1, 4, 'branches', 'Branches', 'Please note the first branch entered here will be shown on the home page for the New Zealand site.', 'matrix', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'y', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Njp7aTowO3M6MjoiMTkiO2k6MTtzOjI6IjIwIjtpOjI7czoyOiIyMSI7aTozO3M6MjoiMjIiO2k6NDtzOjI6IjIzIjtpOjU7czoyOiI0NyI7fX0=');
INSERT INTO `exp_channel_fields` VALUES(16, 1, 2, 'featured_product', 'Featured Product', 'Pick the featured product this sub page is related to.', 'rel', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'y', 'n', 'none', 'n', 1, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');
INSERT INTO `exp_channel_fields` VALUES(17, 1, 2, 'featured_content', 'Featured Content', 'Each block of content you add will create a new block separated by a horizontal rule.', 'matrix', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6MTp7aTowO3M6MjoiMjUiO319');
INSERT INTO `exp_channel_fields` VALUES(18, 1, 2, 'featured_content_intro', 'Featured Content Intro', 'This is shown on the featured product page as the preview for this page.', 'textarea', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');
INSERT INTO `exp_channel_fields` VALUES(19, 1, 5, 'study_content', 'Study Content', 'This is the main body content of the news item. Please note the first paragraph will be automatically made bold.', 'wygwam', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(20, 1, 5, 'study_banner_image', 'Study Banner Image', '', 'file', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiNSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(22, 1, 7, 'home_marketing_message', 'Home Marketing Message', 'This is the large and smaller text that appears over the top of the banner image.', 'matrix', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO2k6MjY7aToxO2k6Mjc7fX0=');
INSERT INTO `exp_channel_fields` VALUES(23, 1, 7, 'home_banner_links', 'Home Banner Links', 'This field controls the content and location of the home page banner links (both over the banner and below in the tabs).', 'matrix', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 0, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMjgiO2k6MTtzOjI6IjI5Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(24, 1, 7, 'home_banner_tab_content', 'Home Banner Tab Content', 'The content of the tabs below the banners. A limit of 190 characters will be shown from this box.', 'textarea', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 3, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 4, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');
INSERT INTO `exp_channel_fields` VALUES(25, 1, 7, 'home_banner_image', 'Home Banner Image', 'This is the large background image shown in the banner.', 'file', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiNiI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(26, 1, 6, 'service_content', 'Service Content', 'Each block of content you add will create a new block separated by a horizontal rule.', 'matrix', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6MTp7aTowO2k6MzA7fX0=');
INSERT INTO `exp_channel_fields` VALUES(27, 1, 6, 'services_banner', 'Services Banner', 'An image with dimensions of 866px - 305px is best.', 'file', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiNyI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(28, 1, 8, 'management_page_content', 'Page Content', 'This is the content at the top of the management page.', 'wygwam', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo5OntzOjc6ImNvbnZlcnQiO3M6MDoiIjtzOjY6ImNvbmZpZyI7czoxOiI1IjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(29, 1, 9, 'about_page_content', 'Page Content', '', 'wygwam', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiI1IjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(30, 1, 9, 'about_us_grid', 'About Us Grid', 'Add as many blocks as you want to the bottom of the about us page', 'matrix', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMzIiO2k6MTtzOjI6IjMxIjt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(31, 1, 9, 'about_page_banner', 'About Page Banner', 'This should be a banner image ideally 866px x 305px, however it may be larger. The image will be constricted width-wise to 866px.', 'file', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiOCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(32, 1, 8, 'staff_members', 'Staff Members', 'Enter a row for each staff member here.', 'matrix', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6NDp7aTowO3M6MjoiMzMiO2k6MTtzOjI6IjM0IjtpOjI7czoyOiIzNSI7aTozO2k6NDY7fX0=');
INSERT INTO `exp_channel_fields` VALUES(33, 1, 1, 'featured_megamenu_icon', 'Featured Megamenu Icon', 'This is the small image that appears in the mega menu for features products. This needs to be an image with the dimensions 302px x 89px and will be constrained to this when displayed. Please note if this image is not supplied on a featured product, it will not be displayed in the featured section of the products megamenu.', 'file', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 12, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiMSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(34, 1, 10, 'manufacturer_footer_image', 'Footer Image', 'This image is output in the footer. It should be a grey/white text on a background colour of #3c3c3c.\nIt must be 30px in height and no wider than 225px (it can be smaller).', 'file', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MjoiMTAiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(35, 1, 10, 'manufacturer_product_page_image', 'Product Page Image', 'This image is used on the product page when you select this manufacturer for the given product. The logo in the design is 140px wide and 24px high (so we would recommend something similar), however you can upload different sizes as long as they fit in your page.', 'file', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MjoiMTAiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(36, 1, 10, 'featured_product_megamenu_image', 'Featured Product Megamenu Image', 'This image is only ever output in the product mega menu if a product with this manufacturer is a featured product. This is a fixed 166px by 49px image and will be constrained to this. Please note if this is not set and this product is featured, the product will not show in the featured megamenu block.', 'file', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 3, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MjoiMTAiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(37, 1, 1, 'manufacturer', 'Manufacturer', 'This field is used to display the manufacturer logo on the product page.', 'rel', '', '0', 0, 0, 'channel', 11, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 13, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');
INSERT INTO `exp_channel_fields` VALUES(38, 1, 4, 'header_contact_details', 'Header Contact Details', 'This is the contact information that is shown in the header of the website.', 'matrix', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjIiO3M6ODoibWF4X3Jvd3MiO3M6MToiMiI7czo3OiJjb2xfaWRzIjthOjM6e2k6MDtzOjI6IjM4IjtpOjE7czoyOiIzNiI7aToyO3M6MjoiMzciO319');
INSERT INTO `exp_channel_fields` VALUES(39, 1, 4, 'branches_au', 'Branches', 'Please note the first branch entered here will be shown on the home page for the Australian site.', 'matrix', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'y', 'n', 'none', 'n', 4, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Njp7aTowO3M6MjoiMzkiO2k6MTtzOjI6IjQwIjtpOjI7czoyOiI0MSI7aTozO3M6MjoiNDIiO2k6NDtzOjI6IjQzIjtpOjU7aTo0ODt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(40, 1, 4, 'contact_introduction_au', 'Contact Introduction', 'The initial content before the branch information.', 'wygwam', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(42, 1, 9, 'about_us_grid_au', 'About Us Grid', 'Add as many blocks as you want to the bottom of the about us page', 'matrix', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiNDQiO2k6MTtzOjI6IjQ1Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(43, 1, 9, 'about_page_content_au', 'Page Content', '', 'wygwam', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiI1IjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(44, 1, 3, 'article_country', 'Article Country', 'Use this to select whether the article should only be available on NZ, AU or both countries.', 'pt_dropdown', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'y', 'n', 'xhtml', 'n', 3, 'any', 'YTo3OntzOjc6Im9wdGlvbnMiO2E6Mzp7czozOiJBbGwiO3M6MzoiQWxsIjtzOjI6Ik5aIjtzOjI6Ik5aIjtzOjI6IkFVIjtzOjI6IkFVIjt9czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(45, 1, 5, 'study_country', 'Study Country', 'Use this to select whether the case study should only be available on NZ, AU or both countries.', 'pt_dropdown', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'y', 'n', 'xhtml', 'n', 3, 'any', 'YTo3OntzOjc6Im9wdGlvbnMiO2E6Mzp7czozOiJBbGwiO3M6MzoiQWxsIjtzOjI6Ik5aIjtzOjI6Ik5aIjtzOjI6IkFVIjtzOjI6IkFVIjt9czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(46, 1, 7, 'home_banner_country', 'Home Banner Country', 'Whether this banner is to be used for the NZ, AU or both home pages.', 'pt_dropdown', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'y', 'n', 'xhtml', 'n', 5, 'any', 'YTo3OntzOjc6Im9wdGlvbnMiO2E6Mzp7czozOiJBbGwiO3M6MzoiQWxsIjtzOjI6Ik5aIjtzOjI6Ik5aIjtzOjI6IkFVIjtzOjI6IkFVIjt9czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(47, 1, 6, 'services_country', 'Services Country', 'Use this field to make services available in specific countries.', 'pt_dropdown', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'y', 'n', 'xhtml', 'n', 3, 'any', 'YTo3OntzOjc6Im9wdGlvbnMiO2E6Mzp7czozOiJBbGwiO3M6MzoiQWxsIjtzOjI6Ik5aIjtzOjI6Ik5aIjtzOjI6IkFVIjtzOjI6IkFVIjt9czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');

/* Table structure for table `exp_channel_member_groups` */
DROP TABLE IF EXISTS `exp_channel_member_groups`;

CREATE TABLE `exp_channel_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `channel_id` int(6) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_channel_titles` */
DROP TABLE IF EXISTS `exp_channel_titles`;

CREATE TABLE `exp_channel_titles` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_channel_titles` */
INSERT INTO `exp_channel_titles` VALUES(1, 1, 1, 1, 0, null, '127.0.0.1', 'Topcon GTS-100N Construction', 'topcon-gts-100n-construction', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358293518, 'n', '2013', '01', '16', 0, 0, 20130411225519, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(2, 1, 1, 1, 0, null, '127.0.0.1', 'Topcon GTS-100N Robotic', 'topcon-gts-100n-robotic', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358293544, 'n', '2013', '01', '16', 0, 0, 20130412020645, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(3, 1, 1, 1, 0, null, '127.0.0.1', 'Topcon GTS-100N Windows Based', 'topcon-gts-100n-windows-based', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358293500, 'n', '2013', '01', '16', 0, 0, 20130313211401, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(4, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 1', 'news-article-1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961454, 'n', '2013', '03', '11', 0, 0, 20130415223715, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(5, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 2', 'news-article-2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961449, 'n', '2013', '03', '11', 0, 0, 20130415223710, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(6, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 6', 'news-article-6', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961456, 'n', '2013', '03', '11', 0, 0, 20130415223717, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(7, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 5', 'news-article-5', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961446, 'n', '2013', '03', '11', 0, 0, 20130415223707, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(8, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 4', 'news-article-4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961461, 'n', '2013', '03', '11', 0, 0, 20130415223722, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(9, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 3', 'news-article-3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961459, 'n', '2013', '03', '11', 0, 0, 20130415223720, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(10, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 7', 'news-article-7', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961451, 'n', '2013', '03', '11', 0, 0, 20130415223712, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(11, 1, 4, 1, 0, null, '127.0.0.1', 'Contact Us', 'contact-us', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362967280, 'n', '2013', '03', '11', 0, 0, 20130416031121, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(12, 1, 2, 1, 0, null, '127.0.0.1', 'BRAMOR Orthophoto', 'bramor-orthophoto', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363822454, 'n', '2013', '03', '21', 0, 0, 20130320235515, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(13, 1, 2, 1, 0, null, '127.0.0.1', 'BRAMOR Orthophoto 2', 'bramor-orthophoto-2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363822493, 'n', '2013', '03', '21', 0, 0, 20130321123453, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(14, 1, 2, 1, 0, null, '127.0.0.1', 'BRAMOR Orthophoto 3', 'bramor-orthophoto-3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363822446, 'n', '2013', '03', '21', 0, 0, 20130321123406, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(15, 1, 2, 1, 0, null, '127.0.0.1', 'BRAMOR Orthophoto 4', 'bramor-orthophoto-4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363822463, 'n', '2013', '03', '21', 0, 0, 20130321123423, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(16, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study', 'test-case-study', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903639, 'n', '2013', '03', '22', 0, 0, 20130415230120, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(17, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 1', 'test-case-study-1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903727, 'n', '2013', '03', '22', 0, 0, 20130415230148, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(18, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 2', 'test-case-study-2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903691, 'n', '2013', '03', '22', 0, 0, 20130415230212, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(19, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 3', 'test-case-study-3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903655, 'n', '2013', '03', '22', 0, 0, 20130415230136, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(20, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 4', 'test-case-study-4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903737, 'n', '2013', '03', '22', 0, 0, 20130415230158, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(21, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 5', 'test-case-study-5', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903721, 'n', '2013', '03', '22', 0, 0, 20130415230142, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(22, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 6', 'test-case-study-6', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903698, 'n', '2013', '03', '22', 0, 0, 20130415230219, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(23, 1, 8, 1, 0, null, '127.0.0.1', 'Services', 'services', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365459710, 'n', '2013', '04', '09', 0, 0, 20130416010651, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(24, 1, 8, 1, 0, null, '127.0.0.1', 'Products', 'products', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365460066, 'n', '2013', '04', '09', 0, 0, 20130416010647, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(25, 1, 8, 1, 0, null, '127.0.0.1', 'About Us NZ', 'about-us', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365460126, 'n', '2013', '04', '09', 0, 0, 20130416010947, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(26, 1, 7, 1, 0, null, '127.0.0.1', 'Survey & Design', 'survey-design', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365483884, 'n', '2013', '04', '09', 0, 0, 20130416083245, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(27, 1, 10, 1, 0, null, '127.0.0.1', 'About Us', 'about-us', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365633138, 'n', '2013', '04', '11', 0, 0, 20130415002319, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(28, 1, 9, 1, 0, null, '127.0.0.1', 'Management', 'management', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365635717, 'n', '2013', '04', '11', 0, 0, 20130415004818, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(29, 1, 11, 1, 0, null, '127.0.0.1', 'Topcon', 'topcon', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365731435, 'n', '2013', '04', '12', 0, 0, 20130412145035, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(30, 1, 3, 1, 0, null, '127.0.0.1', 'Test NZ only article', 'test-nz-only-article', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1366065476, 'n', '2013', '04', '16', 0, 0, 20130416113756, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(31, 1, 3, 1, 0, null, '127.0.0.1', 'Test AU only article', 'test-au-only-article', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1366065432, 'n', '2013', '04', '16', 0, 0, 20130416113712, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(32, 1, 5, 1, 0, null, '127.0.0.1', 'NZ Only Case Study', 'nz-only-case-study', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1366066443, 'n', '2013', '04', '16', 0, 0, 20130415230204, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(33, 1, 5, 1, 0, null, '127.0.0.1', 'AU Only Case Study', 'au-only-case-study', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1366066488, 'n', '2013', '04', '16', 0, 0, 20130415225849, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(34, 1, 8, 1, 0, null, '127.0.0.1', 'About Us AU', 'about-us-au', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365460128, 'n', '2013', '04', '09', 0, 0, 20130416010949, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(35, 1, 7, 1, 0, null, '127.0.0.1', 'Other test service', 'other-test-service', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365483870, 'n', '2013', '04', '09', 0, 0, 20130416092931, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(36, 1, 10, 1, 0, null, '127.0.0.1', 'History', 'history', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1366105507, 'n', '2013', '04', '16', 0, 0, 20130416224507, 0, 0);

/* Table structure for table `exp_channels` */
DROP TABLE IF EXISTS `exp_channels`;

CREATE TABLE `exp_channels` (
  `channel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_name` varchar(40) NOT NULL,
  `channel_title` varchar(100) NOT NULL,
  `channel_url` varchar(100) NOT NULL,
  `channel_description` varchar(225) DEFAULT NULL,
  `channel_lang` varchar(12) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `cat_group` varchar(255) DEFAULT NULL,
  `status_group` int(4) unsigned DEFAULT NULL,
  `deft_status` varchar(50) NOT NULL DEFAULT 'open',
  `field_group` int(4) unsigned DEFAULT NULL,
  `search_excerpt` int(4) unsigned DEFAULT NULL,
  `deft_category` varchar(60) DEFAULT NULL,
  `deft_comments` char(1) NOT NULL DEFAULT 'y',
  `channel_require_membership` char(1) NOT NULL DEFAULT 'y',
  `channel_max_chars` int(5) unsigned DEFAULT NULL,
  `channel_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `channel_allow_img_urls` char(1) NOT NULL DEFAULT 'y',
  `channel_auto_link_urls` char(1) NOT NULL DEFAULT 'n',
  `channel_notify` char(1) NOT NULL DEFAULT 'n',
  `channel_notify_emails` varchar(255) DEFAULT NULL,
  `comment_url` varchar(80) DEFAULT NULL,
  `comment_system_enabled` char(1) NOT NULL DEFAULT 'y',
  `comment_require_membership` char(1) NOT NULL DEFAULT 'n',
  `comment_use_captcha` char(1) NOT NULL DEFAULT 'n',
  `comment_moderate` char(1) NOT NULL DEFAULT 'n',
  `comment_max_chars` int(5) unsigned DEFAULT '5000',
  `comment_timelock` int(5) unsigned NOT NULL DEFAULT '0',
  `comment_require_email` char(1) NOT NULL DEFAULT 'y',
  `comment_text_formatting` char(5) NOT NULL DEFAULT 'xhtml',
  `comment_html_formatting` char(4) NOT NULL DEFAULT 'safe',
  `comment_allow_img_urls` char(1) NOT NULL DEFAULT 'n',
  `comment_auto_link_urls` char(1) NOT NULL DEFAULT 'y',
  `comment_notify` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_authors` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_emails` varchar(255) DEFAULT NULL,
  `comment_expiration` int(4) unsigned NOT NULL DEFAULT '0',
  `search_results_url` varchar(80) DEFAULT NULL,
  `ping_return_url` varchar(80) DEFAULT NULL,
  `show_button_cluster` char(1) NOT NULL DEFAULT 'y',
  `rss_url` varchar(80) DEFAULT NULL,
  `enable_versioning` char(1) NOT NULL DEFAULT 'n',
  `max_revisions` smallint(4) unsigned NOT NULL DEFAULT '10',
  `default_entry_title` varchar(100) DEFAULT NULL,
  `url_title_prefix` varchar(80) DEFAULT NULL,
  `live_look_template` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`),
  KEY `cat_group` (`cat_group`),
  KEY `status_group` (`status_group`),
  KEY `field_group` (`field_group`),
  KEY `channel_name` (`channel_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_channels` */
INSERT INTO `exp_channels` VALUES(1, 1, 'products', 'Products', 'http://synergy.local/', null, 'en', 3, 0, 1358293544, 0, '1', 1, 'open', 1, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(2, 1, 'featured_product_pages', 'Featured Product Pages', 'http://synergy.local/', null, 'en', 4, 0, 1363822493, 0, '', 1, 'open', 2, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(3, 1, 'news', 'News', 'http://synergy.local/', null, 'en', 9, 0, 1366065476, 0, '', 1, 'open', 3, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(4, 1, 'contact_us', 'Contact Us', 'http://synergy.local/', null, 'en', 1, 0, 1362967280, 0, '', 1, 'open', 4, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(5, 1, 'case_studies', 'Case Studies', 'http://synergy.local/', null, 'en', 9, 0, 1366066488, 0, '', 1, 'open', 5, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(7, 1, 'services', 'Services', 'http://synergy.local/', null, 'en', 2, 0, 1365483884, 0, '2', 1, 'open', 6, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(8, 1, 'home_page_banners', 'Home Page Banners', 'http://synergy.local/', null, 'en', 4, 0, 1365460128, 0, '', 1, 'open', 7, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(9, 1, 'management', 'Management', 'http://synergy.local/', null, 'en', 1, 0, 1365635717, 0, '', 1, 'open', 8, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(10, 1, 'about_us', 'About Us', 'http://synergy.local/', null, 'en', 2, 0, 1366105507, 0, '', 1, 'open', 9, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(11, 1, 'manufacturers', 'Manufacturers', 'http://synergy.local/', null, 'en', 1, 0, 1365731435, 0, '', 1, 'open', 10, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);

/* Table structure for table `exp_comment_subscriptions` */
DROP TABLE IF EXISTS `exp_comment_subscriptions`;

CREATE TABLE `exp_comment_subscriptions` (
  `subscription_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `member_id` int(10) DEFAULT '0',
  `email` varchar(50) DEFAULT NULL,
  `subscription_date` varchar(10) DEFAULT NULL,
  `notification_sent` char(1) DEFAULT 'n',
  `hash` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`subscription_id`),
  KEY `entry_id` (`entry_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_comments` */
DROP TABLE IF EXISTS `exp_comments`;

CREATE TABLE `exp_comments` (
  `comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT '0',
  `channel_id` int(4) unsigned DEFAULT '1',
  `author_id` int(10) unsigned DEFAULT '0',
  `status` char(1) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `url` varchar(75) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `comment_date` int(10) DEFAULT NULL,
  `edit_date` int(10) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`comment_id`),
  KEY `entry_id` (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `status` (`status`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_cp_log` */
DROP TABLE IF EXISTS `exp_cp_log`;

CREATE TABLE `exp_cp_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `username` varchar(32) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `act_date` int(10) NOT NULL,
  `action` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_cp_log` */
INSERT INTO `exp_cp_log` VALUES(1, 1, 1, '96black', '127.0.0.1', 1355881673, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(2, 1, 1, '96black', '127.0.0.1', 1355883062, 'Site Updated&nbsp;&nbsp;Synergy Positioning Systems (NZ)');
INSERT INTO `exp_cp_log` VALUES(3, 1, 1, '96black', '127.0.0.1', 1355883097, 'Site Created&nbsp;&nbsp;Synergy Positioning Systems (AU)');
INSERT INTO `exp_cp_log` VALUES(4, 2, 1, '96black', '127.0.0.1', 1355943021, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(5, 2, 1, '96black', '127.0.0.1', 1355945651, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(6, 1, 1, '96black', '127.0.0.1', 1356034419, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(7, 2, 1, '96black', '127.0.0.1', 1356039515, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(8, 2, 1, '96black', '127.0.0.1', 1356039525, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(9, 1, 1, '96black', '127.0.0.1', 1358202174, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(10, 1, 1, '96black', '127.0.0.1', 1358202207, 'Channel Created:&nbsp;&nbsp;Products');
INSERT INTO `exp_cp_log` VALUES(11, 1, 1, '96black', '127.0.0.1', 1358202239, 'Field Group Created:&nbsp;Products');
INSERT INTO `exp_cp_log` VALUES(12, 1, 1, '96black', '127.0.0.1', 1358210877, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(13, 1, 1, '96black', '127.0.0.1', 1358218127, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(14, 1, 1, '96black', '127.0.0.1', 1358218180, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(15, 1, 1, '96black', '127.0.0.1', 1358218184, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(16, 1, 1, '96black', '127.0.0.1', 1358218208, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(17, 1, 1, '96black', '127.0.0.1', 1358220028, 'Channel Created:&nbsp;&nbsp;Featured Product Pages');
INSERT INTO `exp_cp_log` VALUES(18, 1, 1, '96black', '127.0.0.1', 1358220074, 'Field Group Created:&nbsp;Featured Product Pages');
INSERT INTO `exp_cp_log` VALUES(19, 1, 1, '96black', '127.0.0.1', 1358220742, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(20, 1, 1, '96black', '127.0.0.1', 1358274914, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(21, 1, 1, '96black', '127.0.0.1', 1358279266, 'Category Group Created:&nbsp;&nbsp;Product Categories');
INSERT INTO `exp_cp_log` VALUES(22, 1, 1, '96black', '127.0.0.1', 1358293233, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(23, 1, 1, '96black', '127.0.0.1', 1358362941, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(24, 1, 1, '96black', '127.0.0.1', 1359494506, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(25, 1, 1, '96black', '127.0.0.1', 1362948766, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(26, 1, 1, '96black', '127.0.0.1', 1362960488, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(27, 1, 1, '96black', '127.0.0.1', 1362960564, 'Channel Created:&nbsp;&nbsp;News');
INSERT INTO `exp_cp_log` VALUES(28, 1, 1, '96black', '127.0.0.1', 1362960570, 'Field Group Created:&nbsp;News');
INSERT INTO `exp_cp_log` VALUES(29, 1, 1, '96black', '127.0.0.1', 1362966730, 'Field Group Created:&nbsp;Contact Us');
INSERT INTO `exp_cp_log` VALUES(30, 1, 1, '96black', '127.0.0.1', 1362966734, 'Channel Created:&nbsp;&nbsp;Contact Us');
INSERT INTO `exp_cp_log` VALUES(31, 1, 1, '96black', '127.0.0.1', 1362974544, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(32, 1, 1, '96black', '127.0.0.1', 1362976053, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(33, 1, 1, '96black', '127.0.0.1', 1362976069, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(34, 1, 1, '96black', '127.0.0.1', 1362976093, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(35, 1, 1, '96black', '127.0.0.1', 1362992592, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(36, 1, 1, '96black', '127.0.0.1', 1363143606, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(37, 1, 1, '96black', '127.0.0.1', 1363209017, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(38, 1, 1, '96black', '127.0.0.1', 1363320699, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(39, 1, 1, '96black', '127.0.0.1', 1363567001, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(40, 1, 1, '96black', '127.0.0.1', 1363577373, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(41, 1, 1, '96black', '127.0.0.1', 1363578978, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(42, 1, 1, '96black', '127.0.0.1', 1363579481, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(43, 1, 1, '96black', '127.0.0.1', 1363660090, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(44, 1, 1, '96black', '127.0.0.1', 1363729623, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(45, 1, 1, '96black', '127.0.0.1', 1363736810, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(46, 1, 1, '96black', '127.0.0.1', 1363739939, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(47, 1, 1, '96black', '127.0.0.1', 1363739963, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(48, 1, 1, '96black', '127.0.0.1', 1363747179, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(49, 1, 1, '96black', '127.0.0.1', 1363747203, 'Category Group Created:&nbsp;&nbsp;Service Categories');
INSERT INTO `exp_cp_log` VALUES(50, 1, 1, '96black', '127.0.0.1', 1363820443, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(51, 1, 1, '96black', '127.0.0.1', 1363829155, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(52, 1, 1, '96black', '127.0.0.1', 1363833477, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(53, 1, 1, '96black', '127.0.0.1', 1363840598, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(54, 1, 1, '96black', '127.0.0.1', 1363902966, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(55, 1, 1, '96black', '127.0.0.1', 1363903295, 'Channel Created:&nbsp;&nbsp;Case Studies');
INSERT INTO `exp_cp_log` VALUES(56, 1, 1, '96black', '127.0.0.1', 1363903335, 'Field Group Created:&nbsp;Case Studies');
INSERT INTO `exp_cp_log` VALUES(57, 1, 1, '96black', '127.0.0.1', 1364159867, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(58, 1, 1, '96black', '127.0.0.1', 1364160031, 'Channel Created:&nbsp;&nbsp;Servoces');
INSERT INTO `exp_cp_log` VALUES(59, 1, 1, '96black', '127.0.0.1', 1364160036, 'Field Group Created:&nbsp;Services');
INSERT INTO `exp_cp_log` VALUES(60, 1, 1, '96black', '127.0.0.1', 1364160044, 'Channel Deleted:&nbsp;&nbsp;Servoces');
INSERT INTO `exp_cp_log` VALUES(61, 1, 1, '96black', '127.0.0.1', 1364160059, 'Channel Created:&nbsp;&nbsp;Services');
INSERT INTO `exp_cp_log` VALUES(62, 1, 1, '96black', '127.0.0.1', 1364160673, 'Channel Created:&nbsp;&nbsp;Home Page Banners');
INSERT INTO `exp_cp_log` VALUES(63, 1, 1, '96black', '127.0.0.1', 1364160678, 'Field Group Created:&nbsp;Home Page Banners');
INSERT INTO `exp_cp_log` VALUES(64, 1, 1, '96black', '127.0.0.1', 1365458478, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(65, 1, 1, '96black', '127.0.0.1', 1365459691, 'Custom Field Deleted:&nbsp;Home Main Heading');
INSERT INTO `exp_cp_log` VALUES(66, 1, 1, '96black', '127.0.0.1', 1365466907, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(67, 1, 1, '96black', '127.0.0.1', 1365467178, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(68, 1, 1, '96black', '127.0.0.1', 1365467422, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(69, 1, 1, '96black', '127.0.0.1', 1365467428, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(70, 1, 1, '96black', '127.0.0.1', 1365467460, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(71, 1, 1, '96black', '127.0.0.1', 1365467784, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(72, 1, 1, '96black', '127.0.0.1', 1365467819, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(73, 1, 1, '96black', '127.0.0.1', 1365467842, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(74, 1, 1, '96black', '127.0.0.1', 1365471782, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(75, 1, 1, '96black', '127.0.0.1', 1365472101, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(76, 1, 1, '96black', '127.0.0.1', 1365479239, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(77, 1, 1, '96black', '127.0.0.1', 1365479505, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(78, 1, 1, '96black', '127.0.0.1', 1365479533, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(79, 1, 1, '96black', '127.0.0.1', 1365558230, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(80, 1, 1, '96black', '127.0.0.1', 1365630778, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(81, 1, 1, '96black', '127.0.0.1', 1365630802, 'Channel Created:&nbsp;&nbsp;Management');
INSERT INTO `exp_cp_log` VALUES(82, 1, 1, '96black', '127.0.0.1', 1365630809, 'Field Group Created:&nbsp;Management');
INSERT INTO `exp_cp_log` VALUES(83, 1, 1, '96black', '127.0.0.1', 1365632300, 'Channel Created:&nbsp;&nbsp;About Us');
INSERT INTO `exp_cp_log` VALUES(84, 1, 1, '96black', '127.0.0.1', 1365632892, 'Field Group Created:&nbsp;About Us');
INSERT INTO `exp_cp_log` VALUES(85, 1, 1, '96black', '127.0.0.1', 1365641498, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(86, 1, 1, '96black', '127.0.0.1', 1365644175, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(87, 1, 1, '96black', '127.0.0.1', 1365653399, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(88, 1, 1, '96black', '127.0.0.1', 1365708737, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(89, 1, 1, '96black', '127.0.0.1', 1365725809, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(90, 1, 1, '96black', '127.0.0.1', 1365728123, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(91, 1, 1, '96black', '127.0.0.1', 1365729769, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(92, 1, 1, '96black', '127.0.0.1', 1365729854, 'Channel Created:&nbsp;&nbsp;Manufacturers');
INSERT INTO `exp_cp_log` VALUES(93, 1, 1, '96black', '127.0.0.1', 1365729858, 'Field Group Created:&nbsp;Manufacturers');
INSERT INTO `exp_cp_log` VALUES(94, 1, 1, '96black', '127.0.0.1', 1365737079, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(95, 1, 1, '96black', '127.0.0.1', 1365741053, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(96, 1, 1, '96black', '127.0.0.1', 1365741080, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(97, 1, 1, '96black', '127.0.0.1', 1365845178, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(98, 2, 1, '96black', '127.0.0.1', 1365848014, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(99, 2, 1, '96black', '127.0.0.1', 1365848029, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(100, 1, 1, '96black', '127.0.0.1', 1365848058, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(101, 2, 1, '96black', '127.0.0.1', 1365929769, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(102, 1, 1, '96black', '127.0.0.1', 1365964296, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(103, 1, 1, '96black', '127.0.0.1', 1365970813, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(104, 1, 1, '96black', '127.0.0.1', 1365973435, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(105, 1, 1, '96black', '127.0.0.1', 1365981775, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(106, 1, 1, '96black', '127.0.0.1', 1365985288, 'Custom Field Deleted:&nbsp;Page Content AU');
INSERT INTO `exp_cp_log` VALUES(107, 1, 1, '96black', '127.0.0.1', 1366065334, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(108, 1, 1, '96black', '127.0.0.1', 1366073562, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(109, 1, 1, '96black', '127.0.0.1', 1366088721, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(110, 1, 1, '96black', '127.0.0.1', 1366089583, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(111, 1, 1, '96black', '127.0.0.1', 1366090168, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(112, 1, 1, '96black', '127.0.0.1', 1366096141, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(113, 1, 1, '96black', '127.0.0.1', 1366096159, 'Logged in');

/* Table structure for table `exp_cp_search_index` */
DROP TABLE IF EXISTS `exp_cp_search_index`;

CREATE TABLE `exp_cp_search_index` (
  `search_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(20) DEFAULT NULL,
  `method` varchar(50) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `access` varchar(50) DEFAULT NULL,
  `keywords` text,
  PRIMARY KEY (`search_id`),
  FULLTEXT KEY `keywords` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


/* Table structure for table `exp_developer_log` */
DROP TABLE IF EXISTS `exp_developer_log`;

CREATE TABLE `exp_developer_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(10) unsigned NOT NULL,
  `viewed` char(1) NOT NULL DEFAULT 'n',
  `description` text,
  `function` varchar(100) DEFAULT NULL,
  `line` int(10) unsigned DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `deprecated_since` varchar(10) DEFAULT NULL,
  `use_instead` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_cache` */
DROP TABLE IF EXISTS `exp_email_cache`;

CREATE TABLE `exp_email_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `total_sent` int(6) unsigned NOT NULL,
  `from_name` varchar(70) NOT NULL,
  `from_email` varchar(70) NOT NULL,
  `recipient` text NOT NULL,
  `cc` text NOT NULL,
  `bcc` text NOT NULL,
  `recipient_array` mediumtext NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  `plaintext_alt` mediumtext NOT NULL,
  `mailinglist` char(1) NOT NULL DEFAULT 'n',
  `mailtype` varchar(6) NOT NULL,
  `text_fmt` varchar(40) NOT NULL,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `priority` char(1) NOT NULL DEFAULT '3',
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_cache_mg` */
DROP TABLE IF EXISTS `exp_email_cache_mg`;

CREATE TABLE `exp_email_cache_mg` (
  `cache_id` int(6) unsigned NOT NULL,
  `group_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_cache_ml` */
DROP TABLE IF EXISTS `exp_email_cache_ml`;

CREATE TABLE `exp_email_cache_ml` (
  `cache_id` int(6) unsigned NOT NULL,
  `list_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_console_cache` */
DROP TABLE IF EXISTS `exp_email_console_cache`;

CREATE TABLE `exp_email_console_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `member_id` int(10) unsigned NOT NULL,
  `member_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `recipient` varchar(70) NOT NULL,
  `recipient_name` varchar(50) NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_tracker` */
DROP TABLE IF EXISTS `exp_email_tracker`;

CREATE TABLE `exp_email_tracker` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_ip` varchar(16) NOT NULL,
  `sender_email` varchar(75) NOT NULL,
  `sender_username` varchar(50) NOT NULL,
  `number_recipients` int(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_entry_ping_status` */
DROP TABLE IF EXISTS `exp_entry_ping_status`;

CREATE TABLE `exp_entry_ping_status` (
  `entry_id` int(10) unsigned NOT NULL,
  `ping_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`ping_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_entry_versioning` */
DROP TABLE IF EXISTS `exp_entry_versioning`;

CREATE TABLE `exp_entry_versioning` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL,
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `version_date` int(10) NOT NULL,
  `version_data` mediumtext NOT NULL,
  PRIMARY KEY (`version_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_extensions` */
DROP TABLE IF EXISTS `exp_extensions`;

CREATE TABLE `exp_extensions` (
  `extension_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL DEFAULT '',
  `method` varchar(50) NOT NULL DEFAULT '',
  `hook` varchar(50) NOT NULL DEFAULT '',
  `settings` text NOT NULL,
  `priority` int(2) NOT NULL DEFAULT '10',
  `version` varchar(10) NOT NULL DEFAULT '',
  `enabled` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`extension_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_extensions` */
INSERT INTO `exp_extensions` VALUES(1, 'Safecracker_ext', 'form_declaration_modify_data', 'form_declaration_modify_data', '', 10, '2.1', 'y');
INSERT INTO `exp_extensions` VALUES(2, 'Rte_ext', 'myaccount_nav_setup', 'myaccount_nav_setup', '', 10, '1.0', 'y');
INSERT INTO `exp_extensions` VALUES(3, 'Rte_ext', 'cp_menu_array', 'cp_menu_array', '', 10, '1.0', 'y');
INSERT INTO `exp_extensions` VALUES(4, 'Rte_ext', 'publish_form_entry_data', 'publish_form_entry_data', '', 10, '1.0', 'y');
INSERT INTO `exp_extensions` VALUES(5, 'Matrix_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 10, '2.5.2', 'y');
INSERT INTO `exp_extensions` VALUES(6, 'Playa_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 9, '4.3.3', 'y');
INSERT INTO `exp_extensions` VALUES(7, 'Low_seg2cat_ext', 'sessions_end', 'sessions_end', 'a:3:{s:15:\"category_groups\";a:0:{}s:11:\"uri_pattern\";s:0:\"\";s:16:\"set_all_segments\";s:1:\"n\";}', 1, '2.6.3', 'y');
INSERT INTO `exp_extensions` VALUES(8, 'Mx_cloner_ext', 'publish_form_entry_data', 'publish_form_entry_data', 'a:1:{s:13:\"multilanguage\";s:1:\"n\";}', 10, '1.0.3', 'y');

/* Table structure for table `exp_field_formatting` */
DROP TABLE IF EXISTS `exp_field_formatting`;

CREATE TABLE `exp_field_formatting` (
  `formatting_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(10) unsigned NOT NULL,
  `field_fmt` varchar(40) NOT NULL,
  PRIMARY KEY (`formatting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=142 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_field_formatting` */
INSERT INTO `exp_field_formatting` VALUES(1, 1, 'none');
INSERT INTO `exp_field_formatting` VALUES(2, 1, 'br');
INSERT INTO `exp_field_formatting` VALUES(3, 1, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(4, 2, 'none');
INSERT INTO `exp_field_formatting` VALUES(5, 2, 'br');
INSERT INTO `exp_field_formatting` VALUES(6, 2, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(7, 3, 'none');
INSERT INTO `exp_field_formatting` VALUES(8, 3, 'br');
INSERT INTO `exp_field_formatting` VALUES(9, 3, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(10, 4, 'none');
INSERT INTO `exp_field_formatting` VALUES(11, 4, 'br');
INSERT INTO `exp_field_formatting` VALUES(12, 4, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(13, 5, 'none');
INSERT INTO `exp_field_formatting` VALUES(14, 5, 'br');
INSERT INTO `exp_field_formatting` VALUES(15, 5, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(16, 6, 'none');
INSERT INTO `exp_field_formatting` VALUES(17, 6, 'br');
INSERT INTO `exp_field_formatting` VALUES(18, 6, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(19, 7, 'none');
INSERT INTO `exp_field_formatting` VALUES(20, 7, 'br');
INSERT INTO `exp_field_formatting` VALUES(21, 7, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(22, 8, 'none');
INSERT INTO `exp_field_formatting` VALUES(23, 8, 'br');
INSERT INTO `exp_field_formatting` VALUES(24, 8, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(25, 9, 'none');
INSERT INTO `exp_field_formatting` VALUES(26, 9, 'br');
INSERT INTO `exp_field_formatting` VALUES(27, 9, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(28, 10, 'none');
INSERT INTO `exp_field_formatting` VALUES(29, 10, 'br');
INSERT INTO `exp_field_formatting` VALUES(30, 10, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(31, 11, 'none');
INSERT INTO `exp_field_formatting` VALUES(32, 11, 'br');
INSERT INTO `exp_field_formatting` VALUES(33, 11, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(34, 12, 'none');
INSERT INTO `exp_field_formatting` VALUES(35, 12, 'br');
INSERT INTO `exp_field_formatting` VALUES(36, 12, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(37, 13, 'none');
INSERT INTO `exp_field_formatting` VALUES(38, 13, 'br');
INSERT INTO `exp_field_formatting` VALUES(39, 13, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(40, 14, 'none');
INSERT INTO `exp_field_formatting` VALUES(41, 14, 'br');
INSERT INTO `exp_field_formatting` VALUES(42, 14, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(43, 15, 'none');
INSERT INTO `exp_field_formatting` VALUES(44, 15, 'br');
INSERT INTO `exp_field_formatting` VALUES(45, 15, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(46, 16, 'none');
INSERT INTO `exp_field_formatting` VALUES(47, 16, 'br');
INSERT INTO `exp_field_formatting` VALUES(48, 16, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(49, 17, 'none');
INSERT INTO `exp_field_formatting` VALUES(50, 17, 'br');
INSERT INTO `exp_field_formatting` VALUES(51, 17, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(52, 18, 'none');
INSERT INTO `exp_field_formatting` VALUES(53, 18, 'br');
INSERT INTO `exp_field_formatting` VALUES(54, 18, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(55, 19, 'none');
INSERT INTO `exp_field_formatting` VALUES(56, 19, 'br');
INSERT INTO `exp_field_formatting` VALUES(57, 19, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(58, 20, 'none');
INSERT INTO `exp_field_formatting` VALUES(59, 20, 'br');
INSERT INTO `exp_field_formatting` VALUES(60, 20, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(64, 22, 'none');
INSERT INTO `exp_field_formatting` VALUES(65, 22, 'br');
INSERT INTO `exp_field_formatting` VALUES(66, 22, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(67, 23, 'none');
INSERT INTO `exp_field_formatting` VALUES(68, 23, 'br');
INSERT INTO `exp_field_formatting` VALUES(69, 23, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(70, 24, 'none');
INSERT INTO `exp_field_formatting` VALUES(71, 24, 'br');
INSERT INTO `exp_field_formatting` VALUES(72, 24, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(73, 25, 'none');
INSERT INTO `exp_field_formatting` VALUES(74, 25, 'br');
INSERT INTO `exp_field_formatting` VALUES(75, 25, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(76, 26, 'none');
INSERT INTO `exp_field_formatting` VALUES(77, 26, 'br');
INSERT INTO `exp_field_formatting` VALUES(78, 26, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(79, 27, 'none');
INSERT INTO `exp_field_formatting` VALUES(80, 27, 'br');
INSERT INTO `exp_field_formatting` VALUES(81, 27, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(82, 28, 'none');
INSERT INTO `exp_field_formatting` VALUES(83, 28, 'br');
INSERT INTO `exp_field_formatting` VALUES(84, 28, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(85, 29, 'none');
INSERT INTO `exp_field_formatting` VALUES(86, 29, 'br');
INSERT INTO `exp_field_formatting` VALUES(87, 29, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(88, 30, 'none');
INSERT INTO `exp_field_formatting` VALUES(89, 30, 'br');
INSERT INTO `exp_field_formatting` VALUES(90, 30, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(91, 31, 'none');
INSERT INTO `exp_field_formatting` VALUES(92, 31, 'br');
INSERT INTO `exp_field_formatting` VALUES(93, 31, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(94, 32, 'none');
INSERT INTO `exp_field_formatting` VALUES(95, 32, 'br');
INSERT INTO `exp_field_formatting` VALUES(96, 32, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(97, 33, 'none');
INSERT INTO `exp_field_formatting` VALUES(98, 33, 'br');
INSERT INTO `exp_field_formatting` VALUES(99, 33, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(100, 34, 'none');
INSERT INTO `exp_field_formatting` VALUES(101, 34, 'br');
INSERT INTO `exp_field_formatting` VALUES(102, 34, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(103, 35, 'none');
INSERT INTO `exp_field_formatting` VALUES(104, 35, 'br');
INSERT INTO `exp_field_formatting` VALUES(105, 35, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(106, 36, 'none');
INSERT INTO `exp_field_formatting` VALUES(107, 36, 'br');
INSERT INTO `exp_field_formatting` VALUES(108, 36, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(109, 37, 'none');
INSERT INTO `exp_field_formatting` VALUES(110, 37, 'br');
INSERT INTO `exp_field_formatting` VALUES(111, 37, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(112, 38, 'none');
INSERT INTO `exp_field_formatting` VALUES(113, 38, 'br');
INSERT INTO `exp_field_formatting` VALUES(114, 38, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(115, 39, 'none');
INSERT INTO `exp_field_formatting` VALUES(116, 39, 'br');
INSERT INTO `exp_field_formatting` VALUES(117, 39, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(118, 40, 'none');
INSERT INTO `exp_field_formatting` VALUES(119, 40, 'br');
INSERT INTO `exp_field_formatting` VALUES(120, 40, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(124, 42, 'none');
INSERT INTO `exp_field_formatting` VALUES(125, 42, 'br');
INSERT INTO `exp_field_formatting` VALUES(126, 42, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(127, 43, 'none');
INSERT INTO `exp_field_formatting` VALUES(128, 43, 'br');
INSERT INTO `exp_field_formatting` VALUES(129, 43, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(130, 44, 'none');
INSERT INTO `exp_field_formatting` VALUES(131, 44, 'br');
INSERT INTO `exp_field_formatting` VALUES(132, 44, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(133, 45, 'none');
INSERT INTO `exp_field_formatting` VALUES(134, 45, 'br');
INSERT INTO `exp_field_formatting` VALUES(135, 45, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(136, 46, 'none');
INSERT INTO `exp_field_formatting` VALUES(137, 46, 'br');
INSERT INTO `exp_field_formatting` VALUES(138, 46, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(139, 47, 'none');
INSERT INTO `exp_field_formatting` VALUES(140, 47, 'br');
INSERT INTO `exp_field_formatting` VALUES(141, 47, 'xhtml');

/* Table structure for table `exp_field_groups` */
DROP TABLE IF EXISTS `exp_field_groups`;

CREATE TABLE `exp_field_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_field_groups` */
INSERT INTO `exp_field_groups` VALUES(1, 1, 'Products');
INSERT INTO `exp_field_groups` VALUES(2, 1, 'Featured Product Pages');
INSERT INTO `exp_field_groups` VALUES(3, 1, 'News');
INSERT INTO `exp_field_groups` VALUES(4, 1, 'Contact Us');
INSERT INTO `exp_field_groups` VALUES(5, 1, 'Case Studies');
INSERT INTO `exp_field_groups` VALUES(6, 1, 'Services');
INSERT INTO `exp_field_groups` VALUES(7, 1, 'Home Page Banners');
INSERT INTO `exp_field_groups` VALUES(8, 1, 'Management');
INSERT INTO `exp_field_groups` VALUES(9, 1, 'About Us');
INSERT INTO `exp_field_groups` VALUES(10, 1, 'Manufacturers');

/* Table structure for table `exp_fieldtypes` */
DROP TABLE IF EXISTS `exp_fieldtypes`;

CREATE TABLE `exp_fieldtypes` (
  `fieldtype_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `version` varchar(12) NOT NULL,
  `settings` text,
  `has_global_settings` char(1) DEFAULT 'n',
  PRIMARY KEY (`fieldtype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_fieldtypes` */
INSERT INTO `exp_fieldtypes` VALUES(1, 'select', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(2, 'text', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(3, 'textarea', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(4, 'date', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(5, 'file', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(6, 'multi_select', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(7, 'checkboxes', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(8, 'radio', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(9, 'rel', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(10, 'rte', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(11, 'matrix', '2.5.2', 'YTowOnt9', 'y');
INSERT INTO `exp_fieldtypes` VALUES(12, 'playa', '4.3.3', 'YTowOnt9', 'y');
INSERT INTO `exp_fieldtypes` VALUES(13, 'wygwam', '2.6.3', 'YTowOnt9', 'y');
INSERT INTO `exp_fieldtypes` VALUES(14, 'pt_checkboxes', '1.0.3', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(15, 'pt_dropdown', '1.0.3', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(16, 'pt_multiselect', '1.0.3', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(17, 'pt_radio_buttons', '1.0.3', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(18, 'nolan', '1.0.3.1', 'YTowOnt9', 'n');

/* Table structure for table `exp_file_categories` */
DROP TABLE IF EXISTS `exp_file_categories`;

CREATE TABLE `exp_file_categories` (
  `file_id` int(10) unsigned DEFAULT NULL,
  `cat_id` int(10) unsigned DEFAULT NULL,
  `sort` int(10) unsigned DEFAULT '0',
  `is_cover` char(1) DEFAULT 'n',
  KEY `file_id` (`file_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_file_dimensions` */
DROP TABLE IF EXISTS `exp_file_dimensions`;

CREATE TABLE `exp_file_dimensions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `upload_location_id` int(4) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT '',
  `short_name` varchar(255) DEFAULT '',
  `resize_type` varchar(50) DEFAULT '',
  `width` int(10) DEFAULT '0',
  `height` int(10) DEFAULT '0',
  `watermark_id` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `upload_location_id` (`upload_location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_file_dimensions` */
INSERT INTO `exp_file_dimensions` VALUES(1, 1, 4, 'article-list-thumb', 'article-list-thumb', 'crop', 396, 88, 0);
INSERT INTO `exp_file_dimensions` VALUES(2, 1, 5, 'case-study-list-thumb', 'case-study-list-thumb', 'crop', 396, 88, 0);

/* Table structure for table `exp_file_watermarks` */
DROP TABLE IF EXISTS `exp_file_watermarks`;

CREATE TABLE `exp_file_watermarks` (
  `wm_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `wm_name` varchar(80) DEFAULT NULL,
  `wm_type` varchar(10) DEFAULT 'text',
  `wm_image_path` varchar(100) DEFAULT NULL,
  `wm_test_image_path` varchar(100) DEFAULT NULL,
  `wm_use_font` char(1) DEFAULT 'y',
  `wm_font` varchar(30) DEFAULT NULL,
  `wm_font_size` int(3) unsigned DEFAULT NULL,
  `wm_text` varchar(100) DEFAULT NULL,
  `wm_vrt_alignment` varchar(10) DEFAULT 'top',
  `wm_hor_alignment` varchar(10) DEFAULT 'left',
  `wm_padding` int(3) unsigned DEFAULT NULL,
  `wm_opacity` int(3) unsigned DEFAULT NULL,
  `wm_hor_offset` int(4) unsigned DEFAULT NULL,
  `wm_vrt_offset` int(4) unsigned DEFAULT NULL,
  `wm_x_transp` int(4) DEFAULT NULL,
  `wm_y_transp` int(4) DEFAULT NULL,
  `wm_font_color` varchar(7) DEFAULT NULL,
  `wm_use_drop_shadow` char(1) DEFAULT 'y',
  `wm_shadow_distance` int(3) unsigned DEFAULT NULL,
  `wm_shadow_color` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`wm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_files` */
DROP TABLE IF EXISTS `exp_files`;

CREATE TABLE `exp_files` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `title` varchar(255) DEFAULT NULL,
  `upload_location_id` int(4) unsigned DEFAULT '0',
  `rel_path` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_size` int(10) DEFAULT '0',
  `description` text,
  `credit` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `uploaded_by_member_id` int(10) unsigned DEFAULT '0',
  `upload_date` int(10) DEFAULT NULL,
  `modified_by_member_id` int(10) unsigned DEFAULT '0',
  `modified_date` int(10) DEFAULT NULL,
  `file_hw_original` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`file_id`),
  KEY `upload_location_id` (`upload_location_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_files` */
INSERT INTO `exp_files` VALUES(1, 1, 'large-product-image.jpg', 1, '/Users/james/Projects/96black/Synergy/uploads/product-images/large-product-image.jpg', 'image/jpeg', 'large-product-image.jpg', 82647, null, null, null, 1, 1358293635, 1, 1358293635, '485 352');
INSERT INTO `exp_files` VALUES(3, 1, 'large-product-image2.jpg', 2, '/Users/james/Projects/96black/Synergy/uploads/product-images/large-product-image2.jpg', 'image/jpeg', 'large-product-image2.jpg', 82647, null, null, null, 1, 1358294245, 1, 1358294248, '485 352');
INSERT INTO `exp_files` VALUES(4, 1, 'large-product-image3.jpg', 2, '/Users/james/Projects/96black/Synergy/uploads/product-images/large-product-image3.jpg', 'image/jpeg', 'large-product-image3.jpg', 82647, null, null, null, 1, 1358294270, 1, 1358294270, '485 352');
INSERT INTO `exp_files` VALUES(5, 1, 'large-product-image4.jpg', 2, '/Users/james/Projects/96black/Synergy/uploads/product-downloads/large-product-image4.jpg', 'image/jpeg', 'large-product-image4.jpg', 82647, null, null, null, 1, 1358294478, 1, 1358294478, '485 352');
INSERT INTO `exp_files` VALUES(6, 1, 'large-product-image6.jpg', 1, '/Users/james/Projects/96black/Synergy/uploads/product-images/large-product-image6.jpg', 'image/jpeg', 'large-product-image6.jpg', 82647, null, null, null, 1, 1358363557, 1, 1358363557, '485 352');
INSERT INTO `exp_files` VALUES(7, 1, 'cat-image.jpg', 3, '/Users/jamesmcfall/Projects/Synergy/uploads/category-images/cat-image.jpg', 'image/jpeg', 'cat-image.jpg', 8376, null, null, null, 1, 1362950904, 1, 1362950904, '211 153');
INSERT INTO `exp_files` VALUES(8, 1, 'news-temp-top.jpg', 4, '/Users/jamesmcfall/Projects/Synergy/uploads/news-images/news-temp-top.jpg', 'image/jpeg', 'news-temp-top.jpg', 56244, null, null, null, 1, 1362961635, 1, 1362961635, '373 866');
INSERT INTO `exp_files` VALUES(9, 1, 'cat-feature-slide1.jpg', 1, '/Users/jamesmcfall/Projects/Synergy/uploads/product-images/cat-feature-slide1.jpg', 'image/jpeg', 'cat-feature-slide1.jpg', 19917, null, null, null, 1, 1363820695, 1, 1363820695, '411 866');
INSERT INTO `exp_files` VALUES(10, 1, 'cat-feature-slide3.jpg', 1, '/Users/jamesmcfall/Projects/Synergy/uploads/product-images/cat-feature-slide3.jpg', 'image/jpeg', 'cat-feature-slide3.jpg', 20183, null, null, null, 1, 1363820736, 1, 1363820736, '411 866');
INSERT INTO `exp_files` VALUES(11, 1, 'news-temp-top_(1).jpg', 5, '/Users/jamesmcfall/Projects/Synergy/uploads/case-study-image/news-temp-top_(1).jpg', 'image/jpeg', 'news-temp-top_(1).jpg', 56300, null, null, null, 1, 1363903862, 1, 1363903862, '373 866');
INSERT INTO `exp_files` VALUES(12, 1, 'news-temp-top_(2).jpg', 5, '/Users/jamesmcfall/Projects/Synergy/uploads/case-study-image/news-temp-top_(2).jpg', 'image/jpeg', 'news-temp-top_(2).jpg', 56300, null, null, null, 1, 1363904837, 1, 1363904841, '373 866');
INSERT INTO `exp_files` VALUES(13, 1, 'news-temp-top_(3).jpg', 5, '/Users/jamesmcfall/Projects/Synergy/uploads/case-study-image/news-temp-top_(3).jpg', 'image/jpeg', 'news-temp-top_(3).jpg', 56300, null, null, null, 1, 1363904916, 1, 1363904920, '373 866');
INSERT INTO `exp_files` VALUES(14, 1, 'services.jpg', 6, '/Users/jamesmcfall/Projects/Synergy/uploads/home-page-banners/services.jpg', 'image/jpeg', 'services.jpg', 88125, null, null, null, 1, 1365459810, 1, 1365459810, '714 1600');
INSERT INTO `exp_files` VALUES(15, 1, 'products.jpg', 6, '/Users/jamesmcfall/Projects/Synergy/uploads/home-page-banners/products.jpg', 'image/jpeg', 'products.jpg', 85228, null, null, null, 1, 1365460093, 1, 1365460093, '714 1600');
INSERT INTO `exp_files` VALUES(16, 1, 'about.jpg', 6, '/Users/jamesmcfall/Projects/Synergy/uploads/home-page-banners/about.jpg', 'image/jpeg', 'about.jpg', 111165, null, null, null, 1, 1365460222, 1, 1365460222, '714 1600');
INSERT INTO `exp_files` VALUES(17, 1, 'services-temp-top.jpg', 3, '/Users/jamesmcfall/Projects/Synergy/uploads/category-images/services-temp-top.jpg', 'image/jpeg', 'services-temp-top.jpg', 94945, null, null, null, 1, 1365480417, 1, 1365480417, '305 866');
INSERT INTO `exp_files` VALUES(18, 1, 'services-temp-top.jpg', 7, '/Users/jamesmcfall/Projects/Synergy/uploads/services-banners/services-temp-top.jpg', 'image/jpeg', 'services-temp-top.jpg', 94945, null, null, null, 1, 1365562517, 1, 1365562517, '305 866');
INSERT INTO `exp_files` VALUES(19, 1, 'about-temp.jpg', 8, '/Users/jamesmcfall/Projects/Synergy/uploads/about-banners/about-temp.jpg', 'image/jpeg', 'about-temp.jpg', 55081, null, null, null, 1, 1365633903, 1, 1365633903, '305 867');
INSERT INTO `exp_files` VALUES(20, 1, 'james.jpg', 9, '/Users/jamesmcfall/Projects/Synergy/uploads/staff-pictures/james.jpg', 'image/jpeg', 'james.jpg', 8591, null, null, null, 1, 1365636639, 1, 1365636639, '211 153');
INSERT INTO `exp_files` VALUES(21, 1, 'mega-menu-feature-product-image_(1).jpg', 1, '/Users/jamesmcfall/Projects/Synergy/uploads/product-images/mega-menu-feature-product-image_(1).jpg', 'image/jpeg', 'mega-menu-feature-product-image_(1).jpg', 10854, null, null, null, 1, 1365720765, 1, 1365720765, '89 302');
INSERT INTO `exp_files` VALUES(22, 1, 'footer-topcon.gif', 10, '/Users/jamesmcfall/Projects/Synergy/uploads/manufacturer-images/footer-topcon.gif', 'image/gif', 'footer-topcon.gif', 3881, null, null, null, 1, 1365731455, 1, 1365731455, '28 173');
INSERT INTO `exp_files` VALUES(23, 1, 'topcon-logo.gif', 10, '/Users/jamesmcfall/Projects/Synergy/uploads/manufacturer-images/topcon-logo.gif', 'image/gif', 'topcon-logo.gif', 3820, null, null, null, 1, 1365731467, 1, 1365731467, '24 140');
INSERT INTO `exp_files` VALUES(24, 1, 'mega-menu-feature-product-logo.jpg', 10, '/Users/jamesmcfall/Projects/Synergy/uploads/manufacturer-images/mega-menu-feature-product-logo.jpg', 'image/jpeg', 'mega-menu-feature-product-logo.jpg', 2918, null, null, null, 1, 1365731492, 1, 1365731492, '48 166');
INSERT INTO `exp_files` VALUES(25, 1, 'map1.jpg', 11, '/Users/jamesmcfall/Projects/Synergy/uploads/contact-maps/map1.jpg', 'image/jpeg', 'map1.jpg', 51845, null, null, null, 1, 1366081553, 1, 1366081553, '305 866');
INSERT INTO `exp_files` VALUES(26, 1, 'map2.jpg', 11, '/Users/jamesmcfall/Projects/Synergy/uploads/contact-maps/map2.jpg', 'image/jpeg', 'map2.jpg', 51610, null, null, null, 1, 1366081562, 1, 1366081562, '305 866');

/* Table structure for table `exp_freeform_composer_layouts` */
DROP TABLE IF EXISTS `exp_freeform_composer_layouts`;

CREATE TABLE `exp_freeform_composer_layouts` (
  `composer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `composer_data` text,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `preview` char(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`composer_id`),
  KEY `preview` (`preview`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_freeform_composer_templates` */
DROP TABLE IF EXISTS `exp_freeform_composer_templates`;

CREATE TABLE `exp_freeform_composer_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `template_name` varchar(150) NOT NULL DEFAULT 'default',
  `template_label` varchar(150) NOT NULL DEFAULT 'default',
  `template_description` text,
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_data` text,
  `param_data` text,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_freeform_fields` */
DROP TABLE IF EXISTS `exp_freeform_fields`;

CREATE TABLE `exp_freeform_fields` (
  `field_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `field_name` varchar(150) NOT NULL DEFAULT 'default',
  `field_label` varchar(150) NOT NULL DEFAULT 'default',
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `settings` text,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `required` char(1) NOT NULL DEFAULT 'n',
  `submissions_page` char(1) NOT NULL DEFAULT 'y',
  `moderation_page` char(1) NOT NULL DEFAULT 'y',
  `composer_use` char(1) NOT NULL DEFAULT 'y',
  `field_description` text,
  PRIMARY KEY (`field_id`),
  KEY `field_name` (`field_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_fields` */
INSERT INTO `exp_freeform_fields` VALUES(1, 1, 'first_name', 'First Name', 'text', '{\"field_length\":150,\"field_content_type\":\"any\"}', 1, 1365716048, 0, 'n', 'y', 'y', 'y', 'This field contains the user\'s first name.');
INSERT INTO `exp_freeform_fields` VALUES(2, 1, 'last_name', 'Last Name', 'text', '{\"field_length\":150,\"field_content_type\":\"any\"}', 1, 1365716048, 0, 'n', 'y', 'y', 'y', 'This field contains the user\'s last name.');
INSERT INTO `exp_freeform_fields` VALUES(3, 1, 'email', 'Email', 'text', '{\"field_length\":150,\"field_content_type\":\"email\"}', 1, 1365716048, 0, 'n', 'y', 'y', 'y', 'A basic email field for collecting stuff like an email address.');
INSERT INTO `exp_freeform_fields` VALUES(4, 1, 'user_message', 'Message', 'textarea', '{\"field_ta_rows\":6}', 1, 1365716048, 0, 'n', 'y', 'y', 'y', 'This field contains the user\'s message.');
INSERT INTO `exp_freeform_fields` VALUES(5, 1, 'company_name', 'Company Name', 'text', '{\"field_length\":\"255\",\"field_content_type\":\"any\",\"disallow_html_rendering\":\"y\"}', 1, 1365716329, 0, 'n', 'y', 'y', 'y', 'This field contains the company name.');
INSERT INTO `exp_freeform_fields` VALUES(6, 1, 'full_name', 'Full Name', 'text', '{\"field_length\":\"150\",\"field_content_type\":\"any\",\"disallow_html_rendering\":\"y\"}', 1, 1365716586, 0, 'n', 'y', 'y', 'y', 'This contains the full name.');
INSERT INTO `exp_freeform_fields` VALUES(7, 1, 'phone_number', 'Phone Number', 'text', '{\"field_length\":\"150\",\"field_content_type\":\"number\",\"disallow_html_rendering\":\"y\"}', 1, 1365716640, 0, 'n', 'y', 'y', 'y', '');

/* Table structure for table `exp_freeform_fieldtypes` */
DROP TABLE IF EXISTS `exp_freeform_fieldtypes`;

CREATE TABLE `exp_freeform_fieldtypes` (
  `fieldtype_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fieldtype_name` varchar(250) DEFAULT NULL,
  `settings` text,
  `default_field` char(1) NOT NULL DEFAULT 'n',
  `version` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`fieldtype_id`),
  KEY `fieldtype_name` (`fieldtype_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_fieldtypes` */
INSERT INTO `exp_freeform_fieldtypes` VALUES(1, 'file_upload', '[]', 'n', '4.0.11');
INSERT INTO `exp_freeform_fieldtypes` VALUES(2, 'mailinglist', '[]', 'n', '4.0.11');
INSERT INTO `exp_freeform_fieldtypes` VALUES(3, 'text', '[]', 'n', '4.0.11');
INSERT INTO `exp_freeform_fieldtypes` VALUES(4, 'textarea', '[]', 'n', '4.0.11');

/* Table structure for table `exp_freeform_file_uploads` */
DROP TABLE IF EXISTS `exp_freeform_file_uploads`;

CREATE TABLE `exp_freeform_file_uploads` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `field_id` int(10) unsigned NOT NULL DEFAULT '0',
  `server_path` varchar(750) DEFAULT NULL,
  `filename` varchar(250) DEFAULT NULL,
  `extension` varchar(20) DEFAULT NULL,
  `filesize` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`file_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `extension` (`extension`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_freeform_form_entries_1` */
DROP TABLE IF EXISTS `exp_freeform_form_entries_1`;

CREATE TABLE `exp_freeform_form_entries_1` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `complete` varchar(1) NOT NULL DEFAULT 'y',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(50) DEFAULT NULL,
  `form_field_3` text,
  `form_field_4` text,
  `form_field_6` text,
  `form_field_7` text,
  `form_field_5` text,
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_form_entries_1` */
INSERT INTO `exp_freeform_form_entries_1` VALUES(1, 1, 0, 'y', '127.0.0.1', 1365716048, 0, 'pending', 'support@solspace.com', 'Welcome to Freeform. We hope that you will enjoy Solspace software.', null, null, null);
INSERT INTO `exp_freeform_form_entries_1` VALUES(2, 1, 1, 'y', '127.0.0.1', 1365717771, 0, 'pending', '', '', '', '', '');
INSERT INTO `exp_freeform_form_entries_1` VALUES(3, 1, 1, 'y', '127.0.0.1', 1365718729, 0, 'pending', 'james@96black.co.nz', 'Test message submission.', 'James McFall', '093603493', '96black');

/* Table structure for table `exp_freeform_form_entries_2` */
DROP TABLE IF EXISTS `exp_freeform_form_entries_2`;

CREATE TABLE `exp_freeform_form_entries_2` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `complete` varchar(1) NOT NULL DEFAULT 'y',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(50) DEFAULT NULL,
  `form_field_5` text,
  `form_field_6` text,
  `form_field_7` text,
  `form_field_3` text,
  `form_field_4` text,
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_form_entries_2` */
INSERT INTO `exp_freeform_form_entries_2` VALUES(1, 1, 1, 'y', '127.0.0.1', 1365971016, 0, 'pending', '', '', '', '', 'fgsdfgdsf');
INSERT INTO `exp_freeform_form_entries_2` VALUES(2, 1, 1, 'y', '127.0.0.1', 1365971102, 0, 'pending', '', 'sdfsdfds', '', '', '');
INSERT INTO `exp_freeform_form_entries_2` VALUES(3, 1, 1, 'y', '127.0.0.1', 1365972061, 0, 'pending', 'Test', 'James', '12345', 'james@96black.co.nz', 'asfsaf');
INSERT INTO `exp_freeform_form_entries_2` VALUES(4, 1, 0, 'y', '127.0.0.1', 1365973613, 0, 'pending', 'asfadsf', 'asfdsa', '13213', 'sdf@sfsdfd.com', 'safasdfasdf');

/* Table structure for table `exp_freeform_form_entries_3` */
DROP TABLE IF EXISTS `exp_freeform_form_entries_3`;

CREATE TABLE `exp_freeform_form_entries_3` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `complete` varchar(1) NOT NULL DEFAULT 'y',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(50) DEFAULT NULL,
  `form_field_3` text,
  `form_field_4` text,
  `form_field_5` text,
  `form_field_6` text,
  `form_field_7` text,
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_form_entries_3` */
INSERT INTO `exp_freeform_form_entries_3` VALUES(1, 1, 1, 'y', '127.0.0.1', 1366083791, 0, 'pending', 'test@test.com', 'test', 'test', 'test', '123');

/* Table structure for table `exp_freeform_forms` */
DROP TABLE IF EXISTS `exp_freeform_forms`;

CREATE TABLE `exp_freeform_forms` (
  `form_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_name` varchar(150) NOT NULL DEFAULT 'default',
  `form_label` varchar(150) NOT NULL DEFAULT 'default',
  `default_status` varchar(150) NOT NULL DEFAULT 'default',
  `notify_user` char(1) NOT NULL DEFAULT 'n',
  `notify_admin` char(1) NOT NULL DEFAULT 'n',
  `user_email_field` varchar(150) NOT NULL DEFAULT '',
  `user_notification_id` int(10) unsigned NOT NULL DEFAULT '0',
  `admin_notification_id` int(10) unsigned NOT NULL DEFAULT '0',
  `admin_notification_email` text,
  `form_description` text,
  `field_ids` text,
  `field_order` text,
  `template_id` int(10) unsigned NOT NULL DEFAULT '0',
  `composer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `settings` text,
  PRIMARY KEY (`form_id`),
  KEY `form_name` (`form_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_forms` */
INSERT INTO `exp_freeform_forms` VALUES(1, 1, 'contact', 'Contact', 'pending', 'n', 'y', '', 0, 0, 'james@96black.co.nz', 'This is a basic contact form.', '3|4|5|6|7', '6|5|3|7|4', 0, 0, 1, 1365716048, 1365716871, null);
INSERT INTO `exp_freeform_forms` VALUES(2, 1, 'product_enquiry_form', 'Product Enquiry Form', 'pending', 'n', 'y', '', 0, 0, 'james@96black.co.nz', 'This form handles product enquiries.', '3|4|5|6|7', '6|5|7|3|4', 0, 0, 1, 1365716187, 1366084109, null);
INSERT INTO `exp_freeform_forms` VALUES(3, 1, 'product_rental_form', 'Product Rental Form', 'pending', 'n', 'n', '', 0, 0, 'james@96black.co.nz', 'This form handles product rental enquiries.', '3|4|5|6|7', '6|5|7|3|4', 0, 0, 1, 1366083126, 0, null);

/* Table structure for table `exp_freeform_multipage_hashes` */
DROP TABLE IF EXISTS `exp_freeform_multipage_hashes`;

CREATE TABLE `exp_freeform_multipage_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `hash` varchar(32) NOT NULL DEFAULT '',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit` char(1) NOT NULL DEFAULT 'n',
  `data` text,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`),
  KEY `ip_address` (`ip_address`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_freeform_notification_templates` */
DROP TABLE IF EXISTS `exp_freeform_notification_templates`;

CREATE TABLE `exp_freeform_notification_templates` (
  `notification_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `notification_name` varchar(150) NOT NULL DEFAULT 'default',
  `notification_label` varchar(150) NOT NULL DEFAULT 'default',
  `notification_description` text,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `allow_html` char(1) NOT NULL DEFAULT 'n',
  `from_name` varchar(150) NOT NULL DEFAULT '',
  `from_email` varchar(250) NOT NULL DEFAULT '',
  `reply_to_email` varchar(250) NOT NULL DEFAULT '',
  `email_subject` varchar(128) NOT NULL DEFAULT 'default',
  `include_attachments` char(1) NOT NULL DEFAULT 'n',
  `template_data` text,
  PRIMARY KEY (`notification_id`),
  KEY `notification_name` (`notification_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_freeform_params` */
DROP TABLE IF EXISTS `exp_freeform_params`;

CREATE TABLE `exp_freeform_params` (
  `params_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text,
  PRIMARY KEY (`params_id`)
) ENGINE=InnoDB AUTO_INCREMENT=476 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_params` */
INSERT INTO `exp_freeform_params` VALUES(472, 1366104729, '{\"form_id\":\"2\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/search\\/results\\/797f94701de0adc9f39276b257eaabf3\\/\",\"inline_error_return\":\"search\\/results\\/797f94701de0adc9f39276b257eaabf3\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(473, 1366105206, '{\"form_id\":\"2\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/search\\/results\\/797f94701de0adc9f39276b257eaabf3\\/\",\"inline_error_return\":\"search\\/results\\/797f94701de0adc9f39276b257eaabf3\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(474, 1366105221, '{\"form_id\":\"2\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"\\/search\\/results\\/797f94701de0adc9f39276b257eaabf3\\/\",\"inline_error_return\":\"search\\/results\\/797f94701de0adc9f39276b257eaabf3\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":false,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');
INSERT INTO `exp_freeform_params` VALUES(475, 1366105782, '{\"form_id\":\"1\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"Contact-us\\/index\\/%%entry_id%%\",\"inline_error_return\":\"contact-us\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":true,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');

/* Table structure for table `exp_freeform_preferences` */
DROP TABLE IF EXISTS `exp_freeform_preferences`;

CREATE TABLE `exp_freeform_preferences` (
  `preference_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `preference_name` varchar(80) DEFAULT NULL,
  `preference_value` text,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`preference_id`),
  KEY `preference_name` (`preference_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_preferences` */
INSERT INTO `exp_freeform_preferences` VALUES(1, 'ffp', 'n', 0);
INSERT INTO `exp_freeform_preferences` VALUES(2, 'field_layout_prefs', '{\"entry_layout_prefs\":{\"member\":{\"1\":{\"visible\":[\"6\",\"5\",\"7\",\"3\",\"4\",\"author\",\"ip_address\",\"entry_date\",\"edit_date\",\"status\"],\"hidden\":[\"complete\",\"entry_id\",\"site_id\"]}}}}', 1);

/* Table structure for table `exp_freeform_user_email` */
DROP TABLE IF EXISTS `exp_freeform_user_email`;

CREATE TABLE `exp_freeform_user_email` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `email_count` int(10) unsigned NOT NULL DEFAULT '0',
  `email_addresses` text,
  PRIMARY KEY (`email_id`),
  KEY `ip_address` (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_global_variables` */
DROP TABLE IF EXISTS `exp_global_variables`;

CREATE TABLE `exp_global_variables` (
  `variable_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `variable_name` varchar(50) NOT NULL,
  `variable_data` text NOT NULL,
  PRIMARY KEY (`variable_id`),
  KEY `variable_name` (`variable_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_html_buttons` */
DROP TABLE IF EXISTS `exp_html_buttons`;

CREATE TABLE `exp_html_buttons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `tag_name` varchar(32) NOT NULL,
  `tag_open` varchar(120) NOT NULL,
  `tag_close` varchar(120) NOT NULL,
  `accesskey` varchar(32) NOT NULL,
  `tag_order` int(3) unsigned NOT NULL,
  `tag_row` char(1) NOT NULL DEFAULT '1',
  `classname` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_html_buttons` */
INSERT INTO `exp_html_buttons` VALUES(1, 1, 0, 'b', '<strong>', '</strong>', 'b', 1, '1', 'btn_b');
INSERT INTO `exp_html_buttons` VALUES(2, 1, 0, 'i', '<em>', '</em>', 'i', 2, '1', 'btn_i');
INSERT INTO `exp_html_buttons` VALUES(3, 1, 0, 'blockquote', '<blockquote>', '</blockquote>', 'q', 3, '1', 'btn_blockquote');
INSERT INTO `exp_html_buttons` VALUES(4, 1, 0, 'a', '<a href=\"[![Link:!:http://]!]\"(!( title=\"[![Title]!]\")!)>', '</a>', 'a', 4, '1', 'btn_a');
INSERT INTO `exp_html_buttons` VALUES(5, 1, 0, 'img', '<img src=\"[![Link:!:http://]!]\" alt=\"[![Alternative text]!]\" />', '', '', 5, '1', 'btn_img');
INSERT INTO `exp_html_buttons` VALUES(6, 2, 0, 'b', '<strong>', '</strong>', 'b', 1, '1', 'btn_b');
INSERT INTO `exp_html_buttons` VALUES(7, 2, 0, 'i', '<em>', '</em>', 'i', 2, '1', 'btn_i');
INSERT INTO `exp_html_buttons` VALUES(8, 2, 0, 'blockquote', '<blockquote>', '</blockquote>', 'q', 3, '1', 'btn_blockquote');
INSERT INTO `exp_html_buttons` VALUES(9, 2, 0, 'a', '<a href=\"[![Link:!:http://]!]\"(!( title=\"[![Title]!]\")!)>', '</a>', 'a', 4, '1', 'btn_a');
INSERT INTO `exp_html_buttons` VALUES(10, 2, 0, 'img', '<img src=\"[![Link:!:http://]!]\" alt=\"[![Alternative text]!]\" />', '', '', 5, '1', 'btn_img');

/* Table structure for table `exp_layout_publish` */
DROP TABLE IF EXISTS `exp_layout_publish`;

CREATE TABLE `exp_layout_publish` (
  `layout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_group` int(4) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(4) unsigned NOT NULL DEFAULT '0',
  `field_layout` text,
  PRIMARY KEY (`layout_id`),
  KEY `site_id` (`site_id`),
  KEY `member_group` (`member_group`),
  KEY `channel_id` (`channel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_layout_publish` */
INSERT INTO `exp_layout_publish` VALUES(10, 1, 1, 2, 'a:4:{s:7:\"publish\";a:6:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:16;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:18;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:17;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:10:\"Categories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}');
INSERT INTO `exp_layout_publish` VALUES(12, 1, 1, 1, 'a:9:{s:7:\"publish\";a:7:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:37;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:1;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:2;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:10;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:15:\"technical_specs\";a:2:{s:10:\"_tab_label\";s:15:\"Technical Specs\";i:3;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"downloads\";a:2:{s:10:\"_tab_label\";s:9:\"Downloads\";i:11;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:6:\"videos\";a:2:{s:10:\"_tab_label\";s:6:\"Videos\";i:4;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:14:\"rental_details\";a:4:{s:10:\"_tab_label\";s:14:\"Rental Details\";i:5;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:6;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:7;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:16:\"featured_details\";a:4:{s:10:\"_tab_label\";s:16:\"Featured Details\";i:9;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:33;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:8;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:10:\"Categories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}');
INSERT INTO `exp_layout_publish` VALUES(16, 1, 1, 4, 'a:6:{s:7:\"publish\";a:4:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:38;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:19:\"new_zealand_content\";a:3:{s:10:\"_tab_label\";s:19:\"New Zealand Content\";i:14;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:15;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:18:\"australian_content\";a:3:{s:10:\"_tab_label\";s:18:\"Australian Content\";i:40;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:39;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:10:\"Categories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}');
INSERT INTO `exp_layout_publish` VALUES(20, 1, 1, 10, 'a:4:{s:7:\"publish\";a:7:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:31;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:8:\"category\";a:4:{s:7:\"visible\";b:0;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:19:\"new_zealand_content\";a:3:{s:10:\"_tab_label\";s:19:\"New Zealand Content\";i:29;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:30;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:18:\"australian_content\";a:3:{s:10:\"_tab_label\";s:18:\"Australian Content\";i:43;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:42;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:7:{s:10:\"_tab_label\";s:7:\"Options\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}');

/* Table structure for table `exp_matrix_cols` */
DROP TABLE IF EXISTS `exp_matrix_cols`;

CREATE TABLE `exp_matrix_cols` (
  `col_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `col_name` varchar(32) DEFAULT NULL,
  `col_label` varchar(50) DEFAULT NULL,
  `col_instructions` text,
  `col_type` varchar(50) DEFAULT 'text',
  `col_required` char(1) DEFAULT 'n',
  `col_search` char(1) DEFAULT 'n',
  `col_order` int(3) unsigned DEFAULT NULL,
  `col_width` varchar(4) DEFAULT NULL,
  `col_settings` text,
  PRIMARY KEY (`col_id`),
  KEY `site_id` (`site_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_matrix_cols` */
INSERT INTO `exp_matrix_cols` VALUES(1, 1, 1, null, 'currency', 'Currency', '', 'pt_dropdown', 'n', 'y', 0, '33%', 'YToxOntzOjc6Im9wdGlvbnMiO2E6Mjp7czozOiJOWkQiO3M6MzoiTlpEIjtzOjM6IkFVRCI7czozOiJBVUQiO319');
INSERT INTO `exp_matrix_cols` VALUES(2, 1, 1, null, 'amount', 'Amount', 'Please enter the dollar value (without &#36; or any decimals as they will be output automatically)', 'number', 'n', 'n', 1, '', 'YTozOntzOjk6Im1pbl92YWx1ZSI7czowOiIiO3M6OToibWF4X3ZhbHVlIjtzOjA6IiI7czo4OiJkZWNpbWFscyI7czoxOiIwIjt9');
INSERT INTO `exp_matrix_cols` VALUES(3, 1, 3, null, 'section_title', 'Section Title', 'i.e. Dimensions', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(4, 1, 3, null, 'specification_details', 'Specification Details', 'This is where you enter i.e. Length, 200mm', 'nolan', 'n', 'n', 1, '', 'YToyOntzOjE2OiJub2xhbl9jb2xfbGFiZWxzIjtzOjQwOiJTcGVjaWZpY2F0aW9uIE5hbWUgfCBTcGVjaWZpY2F0aW9uIFZhbHVlIjtzOjE1OiJub2xhbl9jb2xfbmFtZXMiO3M6NDA6InNwZWNpZmljYXRpb25fbmFtZSB8IHNwZWNpZmljYXRpb25fdmFsdWUiO30=');
INSERT INTO `exp_matrix_cols` VALUES(6, 1, 4, null, 'video_heading', 'Video Heading', 'The heading to appear above the video', 'text', 'n', 'n', 0, '25%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(7, 1, 4, null, 'video_embed_url', 'Video Embed URL', 'The embed url from Youtube', 'text', 'n', 'n', 1, '25%', 'YTo0OntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(8, 1, 4, null, 'video_description', 'Video Description', 'This description appears beneath the video', 'wygwam', 'n', 'n', 2, '50%', 'YTozOntzOjc6ImNvbnZlcnQiO3M6MDoiIjtzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(9, 1, 5, null, 'rental_heading', 'Rental Heading', '', 'text', 'n', 'n', 0, '25%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(10, 1, 5, null, 'rental_content', 'Rental Content', '', 'wygwam', 'n', 'n', 1, '75%', 'YTozOntzOjc6ImNvbnZlcnQiO3M6MDoiIjtzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(11, 1, 6, null, 'rental_duration_nz', 'Rental Duration (NZ)', 'i.e. 1-3 days', 'text', 'n', 'n', 0, '20%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(12, 1, 6, null, 'rental_pricing_nz', 'Rental Pricing (NZ)', 'Please exclude the &#36; sign and decimal values.', 'number', 'n', 'n', 1, '', 'YTozOntzOjk6Im1pbl92YWx1ZSI7czowOiIiO3M6OToibWF4X3ZhbHVlIjtzOjA6IiI7czo4OiJkZWNpbWFscyI7czoxOiIwIjt9');
INSERT INTO `exp_matrix_cols` VALUES(13, 1, 7, null, 'rental_duration_au', 'Rental Duration (AU)', 'i.e. 1-3 days', 'text', 'n', 'n', 0, '20%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(14, 1, 7, null, 'rental_pricing_au', 'Rental Pricing (AU)', 'Please exclude the &#36; sign and decimal values.', 'number', 'n', 'n', 1, '', 'YTozOntzOjk6Im1pbl92YWx1ZSI7czowOiIiO3M6OToibWF4X3ZhbHVlIjtzOjA6IiI7czo4OiJkZWNpbWFscyI7czoxOiIwIjt9');
INSERT INTO `exp_matrix_cols` VALUES(15, 1, 8, null, 'featured_banner_image', 'Featured Banner Image', '', 'file', 'n', 'n', 0, '33%', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiIxIjtzOjEyOiJjb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO30=');
INSERT INTO `exp_matrix_cols` VALUES(16, 1, 10, null, 'product_image', 'Product Image', '', 'file', 'n', 'n', 0, '33%', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiIxIjtzOjEyOiJjb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO30=');
INSERT INTO `exp_matrix_cols` VALUES(17, 1, 11, null, 'file_title', 'File Title', '', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(18, 1, 11, null, 'download_file', 'Files', '', 'file', 'n', 'n', 1, '', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiIyIjtzOjEyOiJjb250ZW50X3R5cGUiO3M6MzoiYWxsIjt9');
INSERT INTO `exp_matrix_cols` VALUES(19, 1, 15, null, 'branch_region', 'Branch Region', 'i.e. Auckland/Christchurch', 'text', 'y', 'n', 0, '20%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(20, 1, 15, null, 'contact_information', 'Contact Information', '', 'nolan', 'y', 'n', 1, '', 'YToyOntzOjE2OiJub2xhbl9jb2xfbGFiZWxzIjtzOjI0OiJDb250YWN0IE1ldGhvZCB8IERldGFpbHMiO3M6MTU6Im5vbGFuX2NvbF9uYW1lcyI7czozMjoiY29udGFjdF9tZXRob2QgfCBjb250YWN0X2RldGFpbHMiO30=');
INSERT INTO `exp_matrix_cols` VALUES(21, 1, 15, null, 'operating_hours', 'Operating Hours', 'i.e. 8:00am to 5:00pm', 'text', 'y', 'n', 2, '', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(22, 1, 15, null, 'physical_address', 'Physical Address', '', 'text', 'y', 'n', 3, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czoyOiJiciI7czozOiJkaXIiO3M6MzoibHRyIjt9');
INSERT INTO `exp_matrix_cols` VALUES(23, 1, 15, null, 'postal_address', 'Postal Address', '', 'text', 'y', 'n', 4, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czoyOiJiciI7czozOiJkaXIiO3M6MzoibHRyIjt9');
INSERT INTO `exp_matrix_cols` VALUES(24, 1, 8, null, 'featured_banner_description', 'Featured Banner Description', '', 'text', 'n', 'n', 1, '', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(25, 1, 17, null, 'content', 'Content', '', 'wygwam', 'y', 'n', 0, '', 'YToyOntzOjY6ImNvbmZpZyI7czoxOiI0IjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(26, 1, 22, null, 'banner_large_text', 'Banner Large Text', '', 'text', 'y', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(27, 1, 22, null, 'banner_small_text', 'Banner Small Text', '', 'text', 'n', 'n', 1, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(28, 1, 23, null, 'link_text', 'Link Text', '', 'text', 'y', 'n', 0, '50%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(29, 1, 23, null, 'link_location', 'Link Location', 'The page you want the link to point to. It must be prefixed with \"/\". For example \"/services\"', 'text', 'y', 'n', 1, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(30, 1, 26, null, 'content', 'Content', '', 'wygwam', 'n', 'n', 0, '', 'YTozOntzOjc6ImNvbnZlcnQiO3M6MDoiIjtzOjY6ImNvbmZpZyI7czoxOiI0IjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(31, 1, 30, null, 'grid_content', 'Grid Content', '', 'wygwam', 'n', 'n', 1, '', 'YToyOntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(32, 1, 30, null, 'grid_title', 'Grid Title', '', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(33, 1, 32, null, 'name', 'Name', '', 'text', 'y', 'n', 0, '', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(34, 1, 32, null, 'position', 'Position', '', 'text', 'y', 'n', 1, '', 'YTozOntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(35, 1, 32, null, 'image', 'Image', 'Image dimensions: 153px x 211px. If not supplied a default image is used.', 'file', 'n', 'n', 2, '', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiI5IjtzOjEyOiJjb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO30=');
INSERT INTO `exp_matrix_cols` VALUES(36, 1, 38, null, 'contact_number', 'Contact Number', '', 'text', 'y', 'n', 1, '40%', 'YTozOntzOjQ6Im1heGwiO3M6MzoiMTAwIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(37, 1, 38, null, 'email_address', 'Email Address', '', 'text', 'y', 'n', 2, '40%', 'YTozOntzOjQ6Im1heGwiO3M6MzoiMTAwIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(38, 1, 38, null, 'country', 'Country', '\"AU\" or \"NZ\"', 'pt_dropdown', 'y', 'y', 0, '20%', 'YToxOntzOjc6Im9wdGlvbnMiO2E6Mjp7czoyOiJBVSI7czoyOiJBVSI7czoyOiJOWiI7czoyOiJOWiI7fX0=');
INSERT INTO `exp_matrix_cols` VALUES(39, 1, 39, null, 'branch_region', 'Branch Region', 'i.e. Auckland/Christchurch', 'text', 'y', 'n', 0, '20%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(40, 1, 39, null, 'contact_information', 'Contact Information', '', 'nolan', 'y', 'n', 1, '', 'YToyOntzOjE2OiJub2xhbl9jb2xfbGFiZWxzIjtzOjI0OiJDb250YWN0IE1ldGhvZCB8IERldGFpbHMiO3M6MTU6Im5vbGFuX2NvbF9uYW1lcyI7czozMjoiY29udGFjdF9tZXRob2QgfCBjb250YWN0X2RldGFpbHMiO30=');
INSERT INTO `exp_matrix_cols` VALUES(41, 1, 39, null, 'operating_hours', 'Operating Hours', 'i.e. 8:00am to 5:00pm', 'text', 'y', 'n', 2, '', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(42, 1, 39, null, 'physical_address', 'Physical Address', '', 'text', 'y', 'n', 3, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czoyOiJiciI7czozOiJkaXIiO3M6MzoibHRyIjt9');
INSERT INTO `exp_matrix_cols` VALUES(43, 1, 39, null, 'postal_address', 'Postal Address', '', 'text', 'y', 'n', 4, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czoyOiJiciI7czozOiJkaXIiO3M6MzoibHRyIjt9');
INSERT INTO `exp_matrix_cols` VALUES(44, 1, 42, null, 'grid_title', 'Grid Title', '', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(45, 1, 42, null, 'grid_content', 'Grid Content', '', 'wygwam', 'n', 'n', 1, '', 'YToyOntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(46, 1, 32, null, 'country', 'Staff Country', '', 'pt_dropdown', 'y', 'y', 3, '', 'YToxOntzOjc6Im9wdGlvbnMiO2E6Mjp7czoyOiJOWiI7czoyOiJOWiI7czoyOiJBVSI7czoyOiJBVSI7fX0=');
INSERT INTO `exp_matrix_cols` VALUES(47, 1, 15, null, 'map_image', 'Map Image', 'An image with the dimensions 866px x 305 px', 'file', 'y', 'n', 5, '', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoyOiIxMSI7czoxMjoiY29udGVudF90eXBlIjtzOjU6ImltYWdlIjt9');
INSERT INTO `exp_matrix_cols` VALUES(48, 1, 39, null, 'map_image', 'Map Image', 'An image with the dimensions 866px x 305 px', 'file', 'y', 'n', 5, '', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoyOiIxMSI7czoxMjoiY29udGVudF90eXBlIjtzOjU6ImltYWdlIjt9');

/* Table structure for table `exp_matrix_data` */
DROP TABLE IF EXISTS `exp_matrix_data`;

CREATE TABLE `exp_matrix_data` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `is_draft` tinyint(1) unsigned DEFAULT '0',
  `row_order` int(4) unsigned DEFAULT NULL,
  `col_id_1` text,
  `col_id_2` int(11) DEFAULT '0',
  `col_id_3` text,
  `col_id_4` text,
  `col_id_6` text,
  `col_id_7` text,
  `col_id_8` text,
  `col_id_9` text,
  `col_id_10` text,
  `col_id_11` text,
  `col_id_12` int(11) DEFAULT '0',
  `col_id_13` text,
  `col_id_14` int(11) DEFAULT '0',
  `col_id_15` text,
  `col_id_16` text,
  `col_id_17` text,
  `col_id_18` text,
  `col_id_19` text,
  `col_id_20` text,
  `col_id_21` text,
  `col_id_22` text,
  `col_id_23` text,
  `col_id_24` text,
  `col_id_25` text,
  `col_id_26` text,
  `col_id_27` text,
  `col_id_28` text,
  `col_id_29` text,
  `col_id_30` text,
  `col_id_31` text,
  `col_id_32` text,
  `col_id_33` text,
  `col_id_34` text,
  `col_id_35` text,
  `col_id_36` text,
  `col_id_37` text,
  `col_id_38` text,
  `col_id_39` text,
  `col_id_40` text,
  `col_id_41` text,
  `col_id_42` text,
  `col_id_43` text,
  `col_id_44` text,
  `col_id_45` text,
  `col_id_46` text,
  `col_id_47` text,
  `col_id_48` text,
  PRIMARY KEY (`row_id`),
  KEY `site_id` (`site_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_matrix_data` */
INSERT INTO `exp_matrix_data` VALUES(1, 1, 1, 1, null, 0, 1, 'NZD', 45000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(2, 1, 1, 1, null, 0, 2, 'AUD', 40000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(3, 1, 1, 3, null, 0, 1, null, 0, 'Telescope', 'a:7:{i:0;a:2:{s:18:\"specification_name\";s:6:\"Length\";s:19:\"specification_value\";s:15:\"150 Millimeters\";}i:1;a:2:{s:18:\"specification_name\";s:23:\"Objective Lens Diameter\";s:19:\"specification_value\";s:15:\"45mm (EDM:50mm)\";}i:2;a:2:{s:18:\"specification_name\";s:13:\"Magnification\";s:19:\"specification_value\";s:3:\"30x\";}i:3;a:2:{s:18:\"specification_name\";s:5:\"Image\";s:19:\"specification_value\";s:5:\"Erect\";}i:4;a:2:{s:18:\"specification_name\";s:13:\"Field of View\";s:19:\"specification_value\";s:3:\"130\";}i:5;a:2:{s:18:\"specification_name\";s:13:\"Resolve Power\";s:19:\"specification_value\";s:2:\"3\"\";}i:6;a:2:{s:18:\"specification_name\";s:18:\"Min Focus Distance\";s:19:\"specification_value\";s:10:\"1.3 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(4, 1, 1, 3, null, 0, 2, null, 0, 'Measurement Range', 'a:2:{i:0;a:2:{s:18:\"specification_name\";s:8:\"1 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}i:1;a:2:{s:18:\"specification_name\";s:8:\"3 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(5, 1, 1, 10, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(6, 1, 1, 11, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, 'Test uploaded file', '{filedir_2}large-product-image4.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(7, 1, 1, 4, null, 0, 1, null, 0, null, null, 'Test Video Of My 4x4', 'http://www.youtube.com/embed/HNIA-nesZEU', '<p>\n	Test vid of my 4x4.&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae gravida tortor. Maecenas turpis purus, imperdiet id elementum eget, feugiat in tellus. Duis molestie, nulla nec sollicitudin consectetur, magna nisl condimentum neque, at consequat magna enim a tellus. Maecenas porttitor mattis aliquet. Ut lobortis, tortor eget lacinia commodo, mi sapien molestie neque, quis mattis metus lorem posuere massa. Vivamus id pretium lacus. Aliquam ac mauris orci, vulputate ullamcorper elit. Donec eget eros eros. Etiam ac sapien a sem ullamcorper consequat. Suspendisse ut elementum arcu. Cras fringilla tortor pellentesque nibh pharetra pellentesque. Sed et mauris sit amet ligula scelerisque sodales sed nec lorem. In vel quam sit amet eros lobortis consectetur sit amet egestas nisl. Cras erat turpis, suscipit posuere mollis nec, dictum eget lectus.</p>', null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(8, 1, 1, 5, null, 0, 1, null, 0, null, null, null, null, null, 'Test heading', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(9, 1, 1, 5, null, 0, 2, null, 0, null, null, null, null, null, 'Test heading 2', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(10, 1, 1, 6, null, 0, 1, null, 0, null, null, null, null, null, null, null, '1-3 days', 100, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(11, 1, 1, 6, null, 0, 2, null, 0, null, null, null, null, null, null, null, '4-6 days', 180, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(12, 1, 1, 7, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, '1-3 days', 80, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(13, 1, 1, 7, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, '4-6 days', 160, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(14, 1, 1, 10, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image6.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(15, 1, 2, 1, null, 0, 1, 'NZD', 45000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(16, 1, 2, 1, null, 0, 2, 'AUD', 40000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(17, 1, 2, 3, null, 0, 1, null, 0, 'Telescope', 'a:7:{i:0;a:2:{s:18:\"specification_name\";s:6:\"Length\";s:19:\"specification_value\";s:15:\"150 Millimeters\";}i:1;a:2:{s:18:\"specification_name\";s:23:\"Objective Lens Diameter\";s:19:\"specification_value\";s:15:\"45mm (EDM:50mm)\";}i:2;a:2:{s:18:\"specification_name\";s:13:\"Magnification\";s:19:\"specification_value\";s:3:\"30x\";}i:3;a:2:{s:18:\"specification_name\";s:5:\"Image\";s:19:\"specification_value\";s:5:\"Erect\";}i:4;a:2:{s:18:\"specification_name\";s:13:\"Field of View\";s:19:\"specification_value\";s:3:\"130\";}i:5;a:2:{s:18:\"specification_name\";s:13:\"Resolve Power\";s:19:\"specification_value\";s:2:\"3\"\";}i:6;a:2:{s:18:\"specification_name\";s:18:\"Min Focus Distance\";s:19:\"specification_value\";s:10:\"1.3 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(18, 1, 2, 3, null, 0, 2, null, 0, 'Measurement Range', 'a:2:{i:0;a:2:{s:18:\"specification_name\";s:8:\"1 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}i:1;a:2:{s:18:\"specification_name\";s:8:\"3 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(19, 1, 2, 4, null, 0, 1, null, 0, null, null, 'Test Video Of My 4x4', 'http://www.youtube.com/embed/HNIA-nesZEU', '<p>\n	Test vid of my 4x4.&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae gravida tortor. Maecenas turpis purus, imperdiet id elementum eget, feugiat in tellus. Duis molestie, nulla nec sollicitudin consectetur, magna nisl condimentum neque, at consequat magna enim a tellus. Maecenas porttitor mattis aliquet. Ut lobortis, tortor eget lacinia commodo, mi sapien molestie neque, quis mattis metus lorem posuere massa. Vivamus id pretium lacus. Aliquam ac mauris orci, vulputate ullamcorper elit. Donec eget eros eros. Etiam ac sapien a sem ullamcorper consequat. Suspendisse ut elementum arcu. Cras fringilla tortor pellentesque nibh pharetra pellentesque. Sed et mauris sit amet ligula scelerisque sodales sed nec lorem. In vel quam sit amet eros lobortis consectetur sit amet egestas nisl. Cras erat turpis, suscipit posuere mollis nec, dictum eget lectus.</p>', null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(20, 1, 2, 5, null, 0, 1, null, 0, null, null, null, null, null, 'Test heading', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(21, 1, 2, 5, null, 0, 2, null, 0, null, null, null, null, null, 'Test heading 2', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(22, 1, 2, 6, null, 0, 1, null, 0, null, null, null, null, null, null, null, '1-3 days', 100, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(23, 1, 2, 6, null, 0, 2, null, 0, null, null, null, null, null, null, null, '4-6 days', 180, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(24, 1, 2, 7, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, '1-3 days', 80, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(25, 1, 2, 7, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, '4-6 days', 160, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(26, 1, 2, 10, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(27, 1, 2, 10, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image6.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(28, 1, 2, 11, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, 'Test uploaded file', '{filedir_2}large-product-image4.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(29, 1, 3, 1, null, 0, 1, 'NZD', 45000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(30, 1, 3, 1, null, 0, 2, 'AUD', 40000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(31, 1, 3, 3, null, 0, 1, null, 0, 'Telescope', 'a:7:{i:0;a:2:{s:18:\"specification_name\";s:6:\"Length\";s:19:\"specification_value\";s:15:\"150 Millimeters\";}i:1;a:2:{s:18:\"specification_name\";s:23:\"Objective Lens Diameter\";s:19:\"specification_value\";s:15:\"45mm (EDM:50mm)\";}i:2;a:2:{s:18:\"specification_name\";s:13:\"Magnification\";s:19:\"specification_value\";s:3:\"30x\";}i:3;a:2:{s:18:\"specification_name\";s:5:\"Image\";s:19:\"specification_value\";s:5:\"Erect\";}i:4;a:2:{s:18:\"specification_name\";s:13:\"Field of View\";s:19:\"specification_value\";s:3:\"130\";}i:5;a:2:{s:18:\"specification_name\";s:13:\"Resolve Power\";s:19:\"specification_value\";s:2:\"3\"\";}i:6;a:2:{s:18:\"specification_name\";s:18:\"Min Focus Distance\";s:19:\"specification_value\";s:10:\"1.3 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(32, 1, 3, 3, null, 0, 2, null, 0, 'Measurement Range', 'a:2:{i:0;a:2:{s:18:\"specification_name\";s:8:\"1 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}i:1;a:2:{s:18:\"specification_name\";s:8:\"3 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(33, 1, 3, 4, null, 0, 1, null, 0, null, null, 'Test Video Of My 4x4', 'http://www.youtube.com/embed/HNIA-nesZEU', '<p>\n	Test vid of my 4x4.&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae gravida tortor. Maecenas turpis purus, imperdiet id elementum eget, feugiat in tellus. Duis molestie, nulla nec sollicitudin consectetur, magna nisl condimentum neque, at consequat magna enim a tellus. Maecenas porttitor mattis aliquet. Ut lobortis, tortor eget lacinia commodo, mi sapien molestie neque, quis mattis metus lorem posuere massa. Vivamus id pretium lacus. Aliquam ac mauris orci, vulputate ullamcorper elit. Donec eget eros eros. Etiam ac sapien a sem ullamcorper consequat. Suspendisse ut elementum arcu. Cras fringilla tortor pellentesque nibh pharetra pellentesque. Sed et mauris sit amet ligula scelerisque sodales sed nec lorem. In vel quam sit amet eros lobortis consectetur sit amet egestas nisl. Cras erat turpis, suscipit posuere mollis nec, dictum eget lectus.</p>', null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(34, 1, 3, 5, null, 0, 1, null, 0, null, null, null, null, null, 'Test heading', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(35, 1, 3, 5, null, 0, 2, null, 0, null, null, null, null, null, 'Test heading 2', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(36, 1, 3, 6, null, 0, 1, null, 0, null, null, null, null, null, null, null, '1-3 days', 100, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(37, 1, 3, 6, null, 0, 2, null, 0, null, null, null, null, null, null, null, '4-6 days', 180, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(38, 1, 3, 7, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, '1-3 days', 80, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(39, 1, 3, 7, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, '4-6 days', 160, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(40, 1, 3, 10, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(41, 1, 3, 10, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image6.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(42, 1, 3, 11, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, 'Test uploaded file', '{filedir_2}large-product-image4.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(43, 1, 11, 15, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, 'Auckland', 'a:4:{i:0;a:2:{s:14:\"contact_method\";s:9:\"Free Call\";s:15:\"contact_details\";s:12:\"0800-867-266\";}i:1;a:2:{s:14:\"contact_method\";s:5:\"Phone\";s:15:\"contact_details\";s:14:\"+64-9-476-5151\";}i:2;a:2:{s:14:\"contact_method\";s:3:\"Fax\";s:15:\"contact_details\";s:14:\"+63-9-476-5140\";}i:3;a:2:{s:14:\"contact_method\";s:5:\"Email\";s:15:\"contact_details\";s:30:\" info@synergypositioning.co.nz\";}}', '8:00am to 5:00pm', '3/52 Arrenway Drive \nAlbany \nAuckland \nNew Zealand', 'P O Box 100450 \nNSMC \nAuckland \nNew Zealand \n0745', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '{filedir_11}map1.jpg', null);
INSERT INTO `exp_matrix_data` VALUES(44, 1, 11, 15, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, 'Christchurch', 'a:4:{i:0;a:2:{s:14:\"contact_method\";s:9:\"Free Call\";s:15:\"contact_details\";s:13:\" 0800-867-266\";}i:1;a:2:{s:14:\"contact_method\";s:5:\"Phone\";s:15:\"contact_details\";s:14:\"+64-9-476-5151\";}i:2;a:2:{s:14:\"contact_method\";s:3:\"Fax\";s:15:\"contact_details\";s:14:\"+63-9-476-5140\";}i:3;a:2:{s:14:\"contact_method\";s:5:\"Email\";s:15:\"contact_details\";s:30:\" info@synergypositioning.co.nz\";}}', '8:00am to 5:00pm', '3/52 Arrenway Drive \nAlbany \nAuckland \nNew Zealand', 'P O Box 100450 \nNSMC \nAuckland \nNew Zealand \n0745', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '{filedir_11}map2.jpg', null);
INSERT INTO `exp_matrix_data` VALUES(45, 1, 3, 10, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image6.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(46, 1, 2, 8, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, '{filedir_1}cat-feature-slide1.jpg', null, null, null, null, null, null, null, null, 'Test description 1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(47, 1, 2, 8, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, '{filedir_1}cat-feature-slide3.jpg', null, null, null, null, null, null, null, null, 'Test description 2', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(48, 1, 12, 17, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	BRAMOR Orthophoto</h2>\n<p>\n	The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability, ergonomic and user friendly ground control station, durability and mobility. The airframe is manufactured with CAD/CAM technology from advanced composite materials (Kevlar/Carbon/Vectran) that provide a high level of survivability. A high visibility color scheme and strobe and NAV lights are optional. Embedded autonomous flight procedures include an array of failsafe options based on man-rated standards.</p>\n<p>\n	The system consists of the air vehicle with a 25 megapixel DSLR E/O sensor, a foldable portable take off ramp and rugged ground control station. The whole system fits into one rugged transport case. The system is flight ready in under five minutes. The system is safely operated by one operator/pilot in command.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(49, 1, 12, 17, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>\n<p>\n	Solutpat metus, nec auctor mi metus eu nibh. Integer id nibh natis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malsapien, et vulputate libero. Praesent interdum risus a libero consequat dapibus. Nulla facilisi. Aliquam erat nisi, placerat id laoreet quis, fringilla ac dolor. Nam interdum tristique augue.</p>\n<p>\n	Nullam accumsan suscipit magna, non vestibulum tortor eleifend eget. Fusce mollis venenatis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(50, 1, 12, 17, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel.</p>\n<p>\n	Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(51, 1, 13, 17, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	BRAMOR Orthophoto</h2>\n<p>\n	The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability, ergonomic and user friendly ground control station, durability and mobility. The airframe is manufactured with CAD/CAM technology from advanced composite materials (Kevlar/Carbon/Vectran) that provide a high level of survivability. A high visibility color scheme and strobe and NAV lights are optional. Embedded autonomous flight procedures include an array of failsafe options based on man-rated standards.</p>\n<p>\n	The system consists of the air vehicle with a 25 megapixel DSLR E/O sensor, a foldable portable take off ramp and rugged ground control station. The whole system fits into one rugged transport case. The system is flight ready in under five minutes. The system is safely operated by one operator/pilot in command.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(52, 1, 13, 17, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>\n<p>\n	Solutpat metus, nec auctor mi metus eu nibh. Integer id nibh natis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malsapien, et vulputate libero. Praesent interdum risus a libero consequat dapibus. Nulla facilisi. Aliquam erat nisi, placerat id laoreet quis, fringilla ac dolor. Nam interdum tristique augue.</p>\n<p>\n	Nullam accumsan suscipit magna, non vestibulum tortor eleifend eget. Fusce mollis venenatis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(53, 1, 13, 17, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel.</p>\n<p>\n	Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(54, 1, 14, 17, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	BRAMOR Orthophoto</h2>\n<p>\n	The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability, ergonomic and user friendly ground control station, durability and mobility. The airframe is manufactured with CAD/CAM technology from advanced composite materials (Kevlar/Carbon/Vectran) that provide a high level of survivability. A high visibility color scheme and strobe and NAV lights are optional. Embedded autonomous flight procedures include an array of failsafe options based on man-rated standards.</p>\n<p>\n	The system consists of the air vehicle with a 25 megapixel DSLR E/O sensor, a foldable portable take off ramp and rugged ground control station. The whole system fits into one rugged transport case. The system is flight ready in under five minutes. The system is safely operated by one operator/pilot in command.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(55, 1, 14, 17, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>\n<p>\n	Solutpat metus, nec auctor mi metus eu nibh. Integer id nibh natis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malsapien, et vulputate libero. Praesent interdum risus a libero consequat dapibus. Nulla facilisi. Aliquam erat nisi, placerat id laoreet quis, fringilla ac dolor. Nam interdum tristique augue.</p>\n<p>\n	Nullam accumsan suscipit magna, non vestibulum tortor eleifend eget. Fusce mollis venenatis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(56, 1, 14, 17, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel.</p>\n<p>\n	Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(57, 1, 15, 17, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	BRAMOR Orthophoto</h2>\n<p>\n	The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability, ergonomic and user friendly ground control station, durability and mobility. The airframe is manufactured with CAD/CAM technology from advanced composite materials (Kevlar/Carbon/Vectran) that provide a high level of survivability. A high visibility color scheme and strobe and NAV lights are optional. Embedded autonomous flight procedures include an array of failsafe options based on man-rated standards.</p>\n<p>\n	The system consists of the air vehicle with a 25 megapixel DSLR E/O sensor, a foldable portable take off ramp and rugged ground control station. The whole system fits into one rugged transport case. The system is flight ready in under five minutes. The system is safely operated by one operator/pilot in command.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(58, 1, 15, 17, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>\n<p>\n	Solutpat metus, nec auctor mi metus eu nibh. Integer id nibh natis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malsapien, et vulputate libero. Praesent interdum risus a libero consequat dapibus. Nulla facilisi. Aliquam erat nisi, placerat id laoreet quis, fringilla ac dolor. Nam interdum tristique augue.</p>\n<p>\n	Nullam accumsan suscipit magna, non vestibulum tortor eleifend eget. Fusce mollis venenatis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(59, 1, 15, 17, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel.</p>\n<p>\n	Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(60, 1, 23, 22, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, 'Services.', 'Marketing message here', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(61, 1, 23, 23, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, 'Read more about services', '/services', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(62, 1, 24, 22, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, 'Trusted precision and control.', 'Marketing message here', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(63, 1, 24, 23, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, 'Browse, enquire & hire from our product range', '/products', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(64, 1, 25, 22, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, 'Synergy Positioning Systems', 'Marketing message here.', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(65, 1, 25, 23, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, 'Read more about Synergy', '/about-us', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(66, 1, 26, 26, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(67, 1, 26, 26, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	<img alt=\"\" src=\"{filedir_5}news-temp-top_(1).jpg\" style=\"width: 200px; height: 86px; float: right;\" /></p>\n<p>\n	L</p>\n<p>\n	orem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odi</p>\n<p>\n	o, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(68, 1, 27, 30, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	You are dealing with an established company representing world renowned brands that have been tried and tested in increasing productivity and reducing costs.</p>', 'Simple really....', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(69, 1, 27, 30, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	With our equipment and systems, you gain efficiency &amp; reduce costs, saving you money on all aspects of the job including: fuel, wages, wear &amp; tear, materials &amp; processing.</p>', 'Your profit...\n', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(70, 1, 27, 30, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	We only represent equipment &amp; brands that we trust. Topcon, world leaders in positioning equipment for over 80 years &amp; well established throughout the world, offers reliable &amp; robust equipment for any application.</p>', 'Our Products\n', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(71, 1, 27, 30, null, 0, 4, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	Synergy Positioning Systems Ltd is 100% New Zealand owned and operated. Our industry exprience spans more than 30 years.</p>', 'Our Experience\n', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(72, 1, 27, 30, null, 0, 5, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	We are commited to our customers. Your success is our reward. Our factory trained technicians and in-house industry experts ensure that what we sell is fully supported.</p>', 'Our Service\n', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(73, 1, 28, 32, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'James McFall', 'The man', '{filedir_9}james.jpg', null, null, null, null, null, null, null, null, null, null, 'NZ', null, null);
INSERT INTO `exp_matrix_data` VALUES(74, 1, 28, 32, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'John Smith', 'Unknown', null, null, null, null, null, null, null, null, null, null, null, 'AU', null, null);
INSERT INTO `exp_matrix_data` VALUES(75, 1, 1, 8, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, '{filedir_1}cat-feature-slide3.jpg', null, null, null, null, null, null, null, null, '', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(76, 1, 11, 38, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0800-867-266', 'info@96black.co.nz', 'NZ', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(77, 1, 11, 38, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0900-123-456', 'info@96black.co.nz', 'AU', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(78, 1, 11, 39, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'Sydney', 'a:4:{i:0;a:2:{s:14:\"contact_method\";s:9:\"Free Call\";s:15:\"contact_details\";s:13:\"0900-123-4567\";}i:1;a:2:{s:14:\"contact_method\";s:5:\"Phone\";s:15:\"contact_details\";s:6:\"123456\";}i:2;a:2:{s:14:\"contact_method\";s:3:\"Fax\";s:15:\"contact_details\";s:6:\"123456\";}i:3;a:2:{s:14:\"contact_method\";s:5:\"Email\";s:15:\"contact_details\";s:31:\" info@synergypositioning.com.au\";}}', '8:00am to 5:00pm', '123 Some Steet', 'Some PO Box', null, null, null, null, '{filedir_11}map1.jpg');
INSERT INTO `exp_matrix_data` VALUES(79, 1, 27, 42, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'Test grid item', '<p>\n	This is a test grid item</p>', null, null, null);
INSERT INTO `exp_matrix_data` VALUES(80, 1, 34, 22, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, 'Synergy Positioning Systems', 'Marketing message here.', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(81, 1, 34, 23, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, 'Read more about Synergy', '/about-us', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(82, 1, 35, 26, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(83, 1, 35, 26, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	<img alt=\"\" src=\"{filedir_5}news-temp-top_(1).jpg\" style=\"width: 200px; height: 86px; float: left;\" /></p>\n<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a.</p>\n<p>\n	o, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.</p>', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

/* Table structure for table `exp_member_bulletin_board` */
DROP TABLE IF EXISTS `exp_member_bulletin_board`;

CREATE TABLE `exp_member_bulletin_board` (
  `bulletin_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `bulletin_group` int(8) unsigned NOT NULL,
  `bulletin_date` int(10) unsigned NOT NULL,
  `hash` varchar(10) NOT NULL DEFAULT '',
  `bulletin_expires` int(10) unsigned NOT NULL DEFAULT '0',
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_id`),
  KEY `sender_id` (`sender_id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_member_data` */
DROP TABLE IF EXISTS `exp_member_data`;

CREATE TABLE `exp_member_data` (
  `member_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_member_data` */
INSERT INTO `exp_member_data` VALUES(1);

/* Table structure for table `exp_member_fields` */
DROP TABLE IF EXISTS `exp_member_fields`;

CREATE TABLE `exp_member_fields` (
  `m_field_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `m_field_name` varchar(32) NOT NULL,
  `m_field_label` varchar(50) NOT NULL,
  `m_field_description` text NOT NULL,
  `m_field_type` varchar(12) NOT NULL DEFAULT 'text',
  `m_field_list_items` text NOT NULL,
  `m_field_ta_rows` tinyint(2) DEFAULT '8',
  `m_field_maxl` smallint(3) NOT NULL,
  `m_field_width` varchar(6) NOT NULL,
  `m_field_search` char(1) NOT NULL DEFAULT 'y',
  `m_field_required` char(1) NOT NULL DEFAULT 'n',
  `m_field_public` char(1) NOT NULL DEFAULT 'y',
  `m_field_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_cp_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_fmt` char(5) NOT NULL DEFAULT 'none',
  `m_field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`m_field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_member_groups` */
DROP TABLE IF EXISTS `exp_member_groups`;

CREATE TABLE `exp_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_title` varchar(100) NOT NULL,
  `group_description` text NOT NULL,
  `is_locked` char(1) NOT NULL DEFAULT 'y',
  `can_view_offline_system` char(1) NOT NULL DEFAULT 'n',
  `can_view_online_system` char(1) NOT NULL DEFAULT 'y',
  `can_access_cp` char(1) NOT NULL DEFAULT 'y',
  `can_access_content` char(1) NOT NULL DEFAULT 'n',
  `can_access_publish` char(1) NOT NULL DEFAULT 'n',
  `can_access_edit` char(1) NOT NULL DEFAULT 'n',
  `can_access_files` char(1) NOT NULL DEFAULT 'n',
  `can_access_fieldtypes` char(1) NOT NULL DEFAULT 'n',
  `can_access_design` char(1) NOT NULL DEFAULT 'n',
  `can_access_addons` char(1) NOT NULL DEFAULT 'n',
  `can_access_modules` char(1) NOT NULL DEFAULT 'n',
  `can_access_extensions` char(1) NOT NULL DEFAULT 'n',
  `can_access_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_access_plugins` char(1) NOT NULL DEFAULT 'n',
  `can_access_members` char(1) NOT NULL DEFAULT 'n',
  `can_access_admin` char(1) NOT NULL DEFAULT 'n',
  `can_access_sys_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_content_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_tools` char(1) NOT NULL DEFAULT 'n',
  `can_access_comm` char(1) NOT NULL DEFAULT 'n',
  `can_access_utilities` char(1) NOT NULL DEFAULT 'n',
  `can_access_data` char(1) NOT NULL DEFAULT 'n',
  `can_access_logs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_channels` char(1) NOT NULL DEFAULT 'n',
  `can_admin_upload_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_design` char(1) NOT NULL DEFAULT 'n',
  `can_admin_members` char(1) NOT NULL DEFAULT 'n',
  `can_delete_members` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_groups` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_templates` char(1) NOT NULL DEFAULT 'n',
  `can_ban_users` char(1) NOT NULL DEFAULT 'n',
  `can_admin_modules` char(1) NOT NULL DEFAULT 'n',
  `can_admin_templates` char(1) NOT NULL DEFAULT 'n',
  `can_admin_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_edit_categories` char(1) NOT NULL DEFAULT 'n',
  `can_delete_categories` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_edit_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_assign_post_authors` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self_entries` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_entries` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_moderate_comments` char(1) NOT NULL DEFAULT 'n',
  `can_send_email` char(1) NOT NULL DEFAULT 'n',
  `can_send_cached_email` char(1) NOT NULL DEFAULT 'n',
  `can_email_member_groups` char(1) NOT NULL DEFAULT 'n',
  `can_email_mailinglist` char(1) NOT NULL DEFAULT 'n',
  `can_email_from_profile` char(1) NOT NULL DEFAULT 'n',
  `can_view_profiles` char(1) NOT NULL DEFAULT 'n',
  `can_edit_html_buttons` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self` char(1) NOT NULL DEFAULT 'n',
  `mbr_delete_notify_emails` varchar(255) DEFAULT NULL,
  `can_post_comments` char(1) NOT NULL DEFAULT 'n',
  `exclude_from_moderation` char(1) NOT NULL DEFAULT 'n',
  `can_search` char(1) NOT NULL DEFAULT 'n',
  `search_flood_control` mediumint(5) unsigned NOT NULL,
  `can_send_private_messages` char(1) NOT NULL DEFAULT 'n',
  `prv_msg_send_limit` smallint(5) unsigned NOT NULL DEFAULT '20',
  `prv_msg_storage_limit` smallint(5) unsigned NOT NULL DEFAULT '60',
  `can_attach_in_private_messages` char(1) NOT NULL DEFAULT 'n',
  `can_send_bulletins` char(1) NOT NULL DEFAULT 'n',
  `include_in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `include_in_memberlist` char(1) NOT NULL DEFAULT 'y',
  `include_in_mailinglists` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`group_id`,`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_member_groups` */
INSERT INTO `exp_member_groups` VALUES(1, 1, 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y');
INSERT INTO `exp_member_groups` VALUES(1, 2, 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y');
INSERT INTO `exp_member_groups` VALUES(2, 1, 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', 60, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(2, 2, 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', 60, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(3, 1, 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(3, 2, 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(4, 1, 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(4, 2, 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(5, 1, 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', 10, 'y', 20, 60, 'y', 'n', 'n', 'y', 'y');
INSERT INTO `exp_member_groups` VALUES(5, 2, 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', 10, 'y', 20, 60, 'y', 'n', 'n', 'y', 'y');

/* Table structure for table `exp_member_homepage` */
DROP TABLE IF EXISTS `exp_member_homepage`;

CREATE TABLE `exp_member_homepage` (
  `member_id` int(10) unsigned NOT NULL,
  `recent_entries` char(1) NOT NULL DEFAULT 'l',
  `recent_entries_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_comments` char(1) NOT NULL DEFAULT 'l',
  `recent_comments_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_members` char(1) NOT NULL DEFAULT 'n',
  `recent_members_order` int(3) unsigned NOT NULL DEFAULT '0',
  `site_statistics` char(1) NOT NULL DEFAULT 'r',
  `site_statistics_order` int(3) unsigned NOT NULL DEFAULT '0',
  `member_search_form` char(1) NOT NULL DEFAULT 'n',
  `member_search_form_order` int(3) unsigned NOT NULL DEFAULT '0',
  `notepad` char(1) NOT NULL DEFAULT 'r',
  `notepad_order` int(3) unsigned NOT NULL DEFAULT '0',
  `bulletin_board` char(1) NOT NULL DEFAULT 'r',
  `bulletin_board_order` int(3) unsigned NOT NULL DEFAULT '0',
  `pmachine_news_feed` char(1) NOT NULL DEFAULT 'n',
  `pmachine_news_feed_order` int(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_member_homepage` */
INSERT INTO `exp_member_homepage` VALUES(1, 'l', 1, 'l', 2, 'n', 0, 'r', 1, 'n', 0, 'r', 2, 'r', 0, 'l', 0);

/* Table structure for table `exp_member_search` */
DROP TABLE IF EXISTS `exp_member_search`;

CREATE TABLE `exp_member_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `search_date` int(10) unsigned NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `fields` varchar(200) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(8) unsigned NOT NULL,
  `query` text NOT NULL,
  PRIMARY KEY (`search_id`),
  KEY `member_id` (`member_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_members` */
DROP TABLE IF EXISTS `exp_members`;

CREATE TABLE `exp_members` (
  `member_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` smallint(4) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL DEFAULT '',
  `unique_id` varchar(40) NOT NULL,
  `crypt_key` varchar(40) DEFAULT NULL,
  `authcode` varchar(10) DEFAULT NULL,
  `email` varchar(72) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `occupation` varchar(80) DEFAULT NULL,
  `interests` varchar(120) DEFAULT NULL,
  `bday_d` int(2) DEFAULT NULL,
  `bday_m` int(2) DEFAULT NULL,
  `bday_y` int(4) DEFAULT NULL,
  `aol_im` varchar(50) DEFAULT NULL,
  `yahoo_im` varchar(50) DEFAULT NULL,
  `msn_im` varchar(50) DEFAULT NULL,
  `icq` varchar(50) DEFAULT NULL,
  `bio` text,
  `signature` text,
  `avatar_filename` varchar(120) DEFAULT NULL,
  `avatar_width` int(4) unsigned DEFAULT NULL,
  `avatar_height` int(4) unsigned DEFAULT NULL,
  `photo_filename` varchar(120) DEFAULT NULL,
  `photo_width` int(4) unsigned DEFAULT NULL,
  `photo_height` int(4) unsigned DEFAULT NULL,
  `sig_img_filename` varchar(120) DEFAULT NULL,
  `sig_img_width` int(4) unsigned DEFAULT NULL,
  `sig_img_height` int(4) unsigned DEFAULT NULL,
  `ignore_list` text,
  `private_messages` int(4) unsigned NOT NULL DEFAULT '0',
  `accept_messages` char(1) NOT NULL DEFAULT 'y',
  `last_view_bulletins` int(10) NOT NULL DEFAULT '0',
  `last_bulletin_date` int(10) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `join_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visit` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `total_entries` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_comments` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `accept_admin_email` char(1) NOT NULL DEFAULT 'y',
  `accept_user_email` char(1) NOT NULL DEFAULT 'y',
  `notify_by_default` char(1) NOT NULL DEFAULT 'y',
  `notify_of_pm` char(1) NOT NULL DEFAULT 'y',
  `display_avatars` char(1) NOT NULL DEFAULT 'y',
  `display_signatures` char(1) NOT NULL DEFAULT 'y',
  `parse_smileys` char(1) NOT NULL DEFAULT 'y',
  `smart_notifications` char(1) NOT NULL DEFAULT 'y',
  `language` varchar(50) NOT NULL,
  `timezone` varchar(8) NOT NULL,
  `daylight_savings` char(1) NOT NULL DEFAULT 'n',
  `localization_is_site_default` char(1) NOT NULL DEFAULT 'n',
  `time_format` char(2) NOT NULL DEFAULT 'us',
  `cp_theme` varchar(32) DEFAULT NULL,
  `profile_theme` varchar(32) DEFAULT NULL,
  `forum_theme` varchar(32) DEFAULT NULL,
  `tracker` text,
  `template_size` varchar(2) NOT NULL DEFAULT '20',
  `notepad` text,
  `notepad_size` varchar(2) NOT NULL DEFAULT '18',
  `quick_links` text,
  `quick_tabs` text,
  `show_sidebar` char(1) NOT NULL DEFAULT 'n',
  `pmember_id` int(10) NOT NULL DEFAULT '0',
  `rte_enabled` char(1) NOT NULL DEFAULT 'y',
  `rte_toolset_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`),
  KEY `group_id` (`group_id`),
  KEY `unique_id` (`unique_id`),
  KEY `password` (`password`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_members` */
INSERT INTO `exp_members` VALUES(1, 1, '96black', '96black', '106cfaa54b9caa63de2a620edb474b900a68d8e76e79847360bbef4323d51b29ea822b5dfe7f5352be760a6ae6a6de7b8abb78f37ad429db302a103d864acad8', '=WDztY<`dk>S{94N7[sY$1\'WCM{:qPT;Z{P44_rlj\'GU8,WH]l$%3d27{+oz,-=(&Ed*nE>q)!+z4p=a;?`yYL|]l:.`/U.=P3lU@\"n*jX_-=GO>!E+?()Cqj<ai>Q=a', '2d3c4dab12f0973e5396766a007ac2efd798eb16', 'b9cbee325c2edd1db5f8b3ef603a2ad0bb317a5f', null, 'james@96black.co.nz', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 0, 'y', 0, 0, '127.0.0.1', 1355879065, 1366090168, 1366105668, 36, 0, 0, 0, 1366105568, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UP12', 'y', 'n', 'us', null, null, null, null, '20', null, '18', '', 'Template Manager|C=design&M=manager|1', 'n', 0, 'y', 0);

/* Table structure for table `exp_message_attachments` */
DROP TABLE IF EXISTS `exp_message_attachments`;

CREATE TABLE `exp_message_attachments` (
  `attachment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_name` varchar(50) NOT NULL DEFAULT '',
  `attachment_hash` varchar(40) NOT NULL DEFAULT '',
  `attachment_extension` varchar(20) NOT NULL DEFAULT '',
  `attachment_location` varchar(150) NOT NULL DEFAULT '',
  `attachment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_size` int(10) unsigned NOT NULL DEFAULT '0',
  `is_temp` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`attachment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_message_copies` */
DROP TABLE IF EXISTS `exp_message_copies`;

CREATE TABLE `exp_message_copies` (
  `copy_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `recipient_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_received` char(1) NOT NULL DEFAULT 'n',
  `message_read` char(1) NOT NULL DEFAULT 'n',
  `message_time_read` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_downloaded` char(1) NOT NULL DEFAULT 'n',
  `message_folder` int(10) unsigned NOT NULL DEFAULT '1',
  `message_authcode` varchar(10) NOT NULL DEFAULT '',
  `message_deleted` char(1) NOT NULL DEFAULT 'n',
  `message_status` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`copy_id`),
  KEY `message_id` (`message_id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_message_data` */
DROP TABLE IF EXISTS `exp_message_data`;

CREATE TABLE `exp_message_data` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_date` int(10) unsigned NOT NULL DEFAULT '0',
  `message_subject` varchar(255) NOT NULL DEFAULT '',
  `message_body` text NOT NULL,
  `message_tracking` char(1) NOT NULL DEFAULT 'y',
  `message_attachments` char(1) NOT NULL DEFAULT 'n',
  `message_recipients` varchar(200) NOT NULL DEFAULT '',
  `message_cc` varchar(200) NOT NULL DEFAULT '',
  `message_hide_cc` char(1) NOT NULL DEFAULT 'n',
  `message_sent_copy` char(1) NOT NULL DEFAULT 'n',
  `total_recipients` int(5) unsigned NOT NULL DEFAULT '0',
  `message_status` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`message_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_message_folders` */
DROP TABLE IF EXISTS `exp_message_folders`;

CREATE TABLE `exp_message_folders` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `folder1_name` varchar(50) NOT NULL DEFAULT 'InBox',
  `folder2_name` varchar(50) NOT NULL DEFAULT 'Sent',
  `folder3_name` varchar(50) NOT NULL DEFAULT '',
  `folder4_name` varchar(50) NOT NULL DEFAULT '',
  `folder5_name` varchar(50) NOT NULL DEFAULT '',
  `folder6_name` varchar(50) NOT NULL DEFAULT '',
  `folder7_name` varchar(50) NOT NULL DEFAULT '',
  `folder8_name` varchar(50) NOT NULL DEFAULT '',
  `folder9_name` varchar(50) NOT NULL DEFAULT '',
  `folder10_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_message_folders` */
INSERT INTO `exp_message_folders` VALUES(1, 'InBox', 'Sent', '', '', '', '', '', '', '', '');

/* Table structure for table `exp_message_listed` */
DROP TABLE IF EXISTS `exp_message_listed`;

CREATE TABLE `exp_message_listed` (
  `listed_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_member` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_description` varchar(100) NOT NULL DEFAULT '',
  `listed_type` varchar(10) NOT NULL DEFAULT 'blocked',
  PRIMARY KEY (`listed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_module_member_groups` */
DROP TABLE IF EXISTS `exp_module_member_groups`;

CREATE TABLE `exp_module_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `module_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_modules` */
DROP TABLE IF EXISTS `exp_modules`;

CREATE TABLE `exp_modules` (
  `module_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(50) NOT NULL,
  `module_version` varchar(12) NOT NULL,
  `has_cp_backend` char(1) NOT NULL DEFAULT 'n',
  `has_publish_fields` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_modules` */
INSERT INTO `exp_modules` VALUES(1, 'Comment', '2.3', 'y', 'n');
INSERT INTO `exp_modules` VALUES(2, 'Email', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(3, 'Emoticon', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(4, 'File', '1.0.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(5, 'Jquery', '1.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(6, 'Rss', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(7, 'Safecracker', '2.1', 'y', 'n');
INSERT INTO `exp_modules` VALUES(8, 'Search', '2.2', 'n', 'n');
INSERT INTO `exp_modules` VALUES(9, 'Channel', '2.0.1', 'n', 'n');
INSERT INTO `exp_modules` VALUES(10, 'Member', '2.1', 'n', 'n');
INSERT INTO `exp_modules` VALUES(11, 'Stats', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(12, 'Rte', '1.0', 'y', 'n');
INSERT INTO `exp_modules` VALUES(13, 'Playa', '4.3.3', 'n', 'n');
INSERT INTO `exp_modules` VALUES(14, 'Wygwam', '2.6.3', 'y', 'n');
INSERT INTO `exp_modules` VALUES(15, 'Query', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(16, 'Freeform', '4.0.11', 'y', 'n');

/* Table structure for table `exp_online_users` */
DROP TABLE IF EXISTS `exp_online_users`;

CREATE TABLE `exp_online_users` (
  `online_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `in_forum` char(1) NOT NULL DEFAULT 'n',
  `name` varchar(50) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `anon` char(1) NOT NULL,
  PRIMARY KEY (`online_id`),
  KEY `date` (`date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=196 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_online_users` */
INSERT INTO `exp_online_users` VALUES(6, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(8, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(9, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(129, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(140, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(153, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(154, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(156, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(157, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(159, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(160, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(163, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(164, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(165, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(166, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(169, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(170, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(171, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(172, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(174, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(177, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(178, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(181, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(182, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(183, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(186, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(188, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(189, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(190, 1, 1, 'n', '96black', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(191, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(192, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(193, 1, 1, 'n', '96black', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(194, 1, 1, 'n', '96black', '127.0.0.1', 1366105783, '');
INSERT INTO `exp_online_users` VALUES(195, 1, 0, 'n', '', '127.0.0.1', 1366105783, '');

/* Table structure for table `exp_password_lockout` */
DROP TABLE IF EXISTS `exp_password_lockout`;

CREATE TABLE `exp_password_lockout` (
  `lockout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`lockout_id`),
  KEY `login_date` (`login_date`),
  KEY `ip_address` (`ip_address`),
  KEY `user_agent` (`user_agent`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_password_lockout` */
INSERT INTO `exp_password_lockout` VALUES(1, 1365558225, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.160 Safari/537.22', '96black');

/* Table structure for table `exp_ping_servers` */
DROP TABLE IF EXISTS `exp_ping_servers`;

CREATE TABLE `exp_ping_servers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `server_name` varchar(32) NOT NULL,
  `server_url` varchar(150) NOT NULL,
  `port` varchar(4) NOT NULL DEFAULT '80',
  `ping_protocol` varchar(12) NOT NULL DEFAULT 'xmlrpc',
  `is_default` char(1) NOT NULL DEFAULT 'y',
  `server_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_playa_relationships` */
DROP TABLE IF EXISTS `exp_playa_relationships`;

CREATE TABLE `exp_playa_relationships` (
  `rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_entry_id` int(10) unsigned DEFAULT NULL,
  `parent_field_id` int(6) unsigned DEFAULT NULL,
  `parent_col_id` int(6) unsigned DEFAULT NULL,
  `parent_row_id` int(10) unsigned DEFAULT NULL,
  `parent_var_id` int(6) unsigned DEFAULT NULL,
  `child_entry_id` int(10) unsigned DEFAULT NULL,
  `rel_order` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `parent_entry_id` (`parent_entry_id`),
  KEY `parent_field_id` (`parent_field_id`),
  KEY `parent_col_id` (`parent_col_id`),
  KEY `parent_row_id` (`parent_row_id`),
  KEY `parent_var_id` (`parent_var_id`),
  KEY `child_entry_id` (`child_entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_relationships` */
DROP TABLE IF EXISTS `exp_relationships`;

CREATE TABLE `exp_relationships` (
  `rel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `rel_parent_id` int(10) NOT NULL DEFAULT '0',
  `rel_child_id` int(10) NOT NULL DEFAULT '0',
  `rel_type` varchar(12) NOT NULL,
  `rel_data` mediumtext NOT NULL,
  `reverse_rel_data` mediumtext NOT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `rel_parent_id` (`rel_parent_id`),
  KEY `rel_child_id` (`rel_child_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_relationships` */
INSERT INTO `exp_relationships` VALUES(1, 12, 2, 'channel', '', '');
INSERT INTO `exp_relationships` VALUES(2, 13, 2, 'channel', '', '');
INSERT INTO `exp_relationships` VALUES(3, 14, 2, 'channel', '', '');
INSERT INTO `exp_relationships` VALUES(4, 15, 2, 'channel', '', '');
INSERT INTO `exp_relationships` VALUES(5, 2, 29, 'channel', 'a:3:{s:5:\"query\";O:18:\"CI_DB_mysql_result\":7:{s:7:\"conn_id\";i:0;s:9:\"result_id\";i:0;s:12:\"result_array\";a:1:{i:0;a:151:{s:8:\"entry_id\";s:2:\"29\";s:10:\"channel_id\";s:2:\"11\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:6:\"Topcon\";s:9:\"url_title\";s:6:\"topcon\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1365731435\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"04\";s:3:\"day\";s:2:\"12\";s:9:\"edit_date\";s:14:\"20130412145035\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:13:\"Manufacturers\";s:12:\"channel_name\";s:13:\"manufacturers\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"0\";s:11:\"field_ft_16\";N;s:11:\"field_id_17\";s:0:\"\";s:11:\"field_ft_17\";N;s:11:\"field_id_18\";s:0:\"\";s:11:\"field_ft_18\";N;s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";N;s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";N;s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";N;s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";N;s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";N;s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";N;s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";N;s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";N;s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";N;s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";N;s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";N;s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";N;s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";N;s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";N;s:11:\"field_id_34\";s:29:\"{filedir_10}footer-topcon.gif\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:27:\"{filedir_10}topcon-logo.gif\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:46:\"{filedir_10}mega-menu-feature-product-logo.jpg\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";s:11:\"field_id_38\";s:0:\"\";s:11:\"field_ft_38\";s:4:\"none\";s:11:\"field_id_39\";s:0:\"\";s:11:\"field_ft_39\";s:4:\"none\";s:11:\"field_id_40\";s:0:\"\";s:11:\"field_ft_40\";s:4:\"none\";s:11:\"field_id_42\";s:0:\"\";s:11:\"field_ft_42\";s:4:\"none\";s:11:\"field_id_43\";s:0:\"\";s:11:\"field_ft_43\";s:4:\"none\";s:11:\"field_id_44\";s:0:\"\";s:11:\"field_ft_44\";s:5:\"xhtml\";s:11:\"field_id_45\";s:0:\"\";s:11:\"field_ft_45\";s:5:\"xhtml\";s:11:\"field_id_46\";s:0:\"\";s:11:\"field_ft_46\";s:5:\"xhtml\";s:11:\"field_id_47\";s:0:\"\";s:11:\"field_ft_47\";s:5:\"xhtml\";}}s:13:\"result_object\";a:0:{}s:11:\"current_row\";i:0;s:8:\"num_rows\";i:1;s:8:\"row_data\";a:151:{s:8:\"entry_id\";s:2:\"29\";s:10:\"channel_id\";s:2:\"11\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:6:\"Topcon\";s:9:\"url_title\";s:6:\"topcon\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1365731435\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"04\";s:3:\"day\";s:2:\"12\";s:9:\"edit_date\";s:14:\"20130412145035\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:13:\"Manufacturers\";s:12:\"channel_name\";s:13:\"manufacturers\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"0\";s:11:\"field_ft_16\";N;s:11:\"field_id_17\";s:0:\"\";s:11:\"field_ft_17\";N;s:11:\"field_id_18\";s:0:\"\";s:11:\"field_ft_18\";N;s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";N;s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";N;s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";N;s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";N;s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";N;s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";N;s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";N;s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";N;s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";N;s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";N;s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";N;s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";N;s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";N;s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";N;s:11:\"field_id_34\";s:29:\"{filedir_10}footer-topcon.gif\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:27:\"{filedir_10}topcon-logo.gif\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:46:\"{filedir_10}mega-menu-feature-product-logo.jpg\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";s:11:\"field_id_38\";s:0:\"\";s:11:\"field_ft_38\";s:4:\"none\";s:11:\"field_id_39\";s:0:\"\";s:11:\"field_ft_39\";s:4:\"none\";s:11:\"field_id_40\";s:0:\"\";s:11:\"field_ft_40\";s:4:\"none\";s:11:\"field_id_42\";s:0:\"\";s:11:\"field_ft_42\";s:4:\"none\";s:11:\"field_id_43\";s:0:\"\";s:11:\"field_ft_43\";s:4:\"none\";s:11:\"field_id_44\";s:0:\"\";s:11:\"field_ft_44\";s:5:\"xhtml\";s:11:\"field_id_45\";s:0:\"\";s:11:\"field_ft_45\";s:5:\"xhtml\";s:11:\"field_id_46\";s:0:\"\";s:11:\"field_ft_46\";s:5:\"xhtml\";s:11:\"field_id_47\";s:0:\"\";s:11:\"field_ft_47\";s:5:\"xhtml\";}}s:10:\"cats_fixed\";s:1:\"1\";s:10:\"categories\";a:0:{}}', '');

/* Table structure for table `exp_remember_me` */
DROP TABLE IF EXISTS `exp_remember_me`;

CREATE TABLE `exp_remember_me` (
  `remember_me_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) DEFAULT '0',
  `ip_address` varchar(45) DEFAULT '0',
  `user_agent` varchar(120) DEFAULT '',
  `admin_sess` tinyint(1) DEFAULT '0',
  `site_id` int(4) DEFAULT '1',
  `expiration` int(10) DEFAULT '0',
  `last_refresh` int(10) DEFAULT '0',
  PRIMARY KEY (`remember_me_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_reset_password` */
DROP TABLE IF EXISTS `exp_reset_password`;

CREATE TABLE `exp_reset_password` (
  `reset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL,
  `resetcode` varchar(12) NOT NULL,
  `date` int(10) NOT NULL,
  PRIMARY KEY (`reset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_revision_tracker` */
DROP TABLE IF EXISTS `exp_revision_tracker`;

CREATE TABLE `exp_revision_tracker` (
  `tracker_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `item_table` varchar(20) NOT NULL,
  `item_field` varchar(20) NOT NULL,
  `item_date` int(10) NOT NULL,
  `item_author_id` int(10) unsigned NOT NULL,
  `item_data` mediumtext NOT NULL,
  PRIMARY KEY (`tracker_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_rte_tools` */
DROP TABLE IF EXISTS `exp_rte_tools`;

CREATE TABLE `exp_rte_tools` (
  `tool_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(75) DEFAULT NULL,
  `class` varchar(75) DEFAULT NULL,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`tool_id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_rte_tools` */
INSERT INTO `exp_rte_tools` VALUES(1, 'Blockquote', 'Blockquote_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(2, 'Bold', 'Bold_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(3, 'Headings', 'Headings_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(4, 'Image', 'Image_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(5, 'Italic', 'Italic_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(6, 'Link', 'Link_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(7, 'Ordered List', 'Ordered_list_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(8, 'Underline', 'Underline_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(9, 'Unordered List', 'Unordered_list_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(10, 'View Source', 'View_source_rte', 'y');

/* Table structure for table `exp_rte_toolsets` */
DROP TABLE IF EXISTS `exp_rte_toolsets`;

CREATE TABLE `exp_rte_toolsets` (
  `toolset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `tools` text,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`toolset_id`),
  KEY `member_id` (`member_id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_rte_toolsets` */
INSERT INTO `exp_rte_toolsets` VALUES(1, 0, 'Default', '3|2|5|1|9|7|6|4|10', 'y');

/* Table structure for table `exp_search` */
DROP TABLE IF EXISTS `exp_search`;

CREATE TABLE `exp_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) NOT NULL DEFAULT '1',
  `search_date` int(10) NOT NULL,
  `keywords` varchar(60) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(6) NOT NULL,
  `per_page` tinyint(3) unsigned NOT NULL,
  `query` mediumtext,
  `custom_fields` mediumtext,
  `result_page` varchar(70) NOT NULL,
  PRIMARY KEY (`search_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/* dumping data for table `exp_search` */
INSERT INTO `exp_search` VALUES('797f94701de0adc9f39276b257eaabf3', 1, 1366104729, 'survey', 1, '127.0.0.1', 1, 50, 's:1413:\\\"SELECT DISTINCT(t.entry_id), t.entry_id, t.channel_id, t.forum_topic_id, t.author_id, t.ip_address, t.title, t.url_title, t.status, t.dst_enabled, t.view_count_one, t.view_count_two, t.view_count_three, t.view_count_four, t.allow_comments, t.comment_expiration_date, t.sticky, t.entry_date, t.year, t.month, t.day, t.entry_date, t.edit_date, t.expiration_date, t.recent_comment_date, t.comment_total, t.site_id as entry_site_id,\n				w.channel_title, w.channel_name, w.search_results_url, w.search_excerpt, w.channel_url, w.comment_url, w.comment_moderate, w.channel_html_formatting, w.channel_allow_img_urls, w.channel_auto_link_urls, w.comment_system_enabled, \n				m.username, m.email, m.url, m.screen_name, m.location, m.occupation, m.interests, m.aol_im, m.yahoo_im, m.msn_im, m.icq, m.signature, m.sig_img_filename, m.sig_img_width, m.sig_img_height, m.avatar_filename, m.avatar_width, m.avatar_height, m.photo_filename, m.photo_width, m.photo_height, m.group_id, m.member_id, m.bday_d, m.bday_m, m.bday_y, m.bio,\n				md.*,\n				wd.*\n			FROM MDBMPREFIXchannel_titles		AS t\n			LEFT JOIN MDBMPREFIXchannels 		AS w  ON t.channel_id = w.channel_id \n			LEFT JOIN MDBMPREFIXchannel_data	AS wd ON t.entry_id = wd.entry_id \n			LEFT JOIN MDBMPREFIXmembers		AS m  ON m.member_id = t.author_id \n			LEFT JOIN MDBMPREFIXmember_data	AS md ON md.member_id = m.member_id \n			WHERE t.entry_id IN (26)  ORDER BY entry_date  desc\\\";', 'a:16:{s:14:\\\"product_prices\\\";a:2:{i:0;s:1:\\\"1\\\";i:1;s:1:\\\"n\\\";}s:19:\\\"product_description\\\";a:2:{i:0;s:1:\\\"2\\\";i:1;s:1:\\\"n\\\";}s:24:\\\"technical_specifications\\\";a:2:{i:0;s:1:\\\"3\\\";i:1;s:1:\\\"n\\\";}s:14:\\\"product_videos\\\";a:2:{i:0;s:1:\\\"4\\\";i:1;s:1:\\\"n\\\";}s:15:\\\"rental_sections\\\";a:2:{i:0;s:1:\\\"5\\\";i:1;s:1:\\\"n\\\";}s:17:\\\"rental_pricing_nz\\\";a:2:{i:0;s:1:\\\"6\\\";i:1;s:1:\\\"n\\\";}s:17:\\\"rental_pricing_au\\\";a:2:{i:0;s:1:\\\"7\\\";i:1;s:1:\\\"n\\\";}s:24:\\\"featured_carousel_images\\\";a:2:{i:0;s:1:\\\"8\\\";i:1;s:1:\\\"n\\\";}s:19:\\\"is_featured_product\\\";a:2:{i:0;s:1:\\\"9\\\";i:1;s:1:\\\"y\\\";}s:14:\\\"product_images\\\";a:2:{i:0;s:2:\\\"10\\\";i:1;s:1:\\\"n\\\";}s:17:\\\"product_downloads\\\";a:2:{i:0;s:2:\\\"11\\\";i:1;s:1:\\\"n\\\";}s:15:\\\"service_content\\\";a:2:{i:0;s:2:\\\"26\\\";i:1;s:1:\\\"n\\\";}s:15:\\\"services_banner\\\";a:2:{i:0;s:2:\\\"27\\\";i:1;s:1:\\\"n\\\";}s:22:\\\"featured_megamenu_icon\\\";a:2:{i:0;s:2:\\\"33\\\";i:1;s:1:\\\"y\\\";}s:12:\\\"manufacturer\\\";a:2:{i:0;s:2:\\\"37\\\";i:1;s:1:\\\"y\\\";}s:16:\\\"services_country\\\";a:2:{i:0;s:2:\\\"47\\\";i:1;s:1:\\\"y\\\";}}', 'search/results');

/* Table structure for table `exp_search_log` */
DROP TABLE IF EXISTS `exp_search_log`;

CREATE TABLE `exp_search_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `search_date` int(10) NOT NULL,
  `search_type` varchar(32) NOT NULL,
  `search_terms` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_search_log` */
INSERT INTO `exp_search_log` VALUES(1, 1, 1, '96black', '127.0.0.1', 1365964656, 'site', 'top');
INSERT INTO `exp_search_log` VALUES(2, 1, 1, '96black', '127.0.0.1', 1365964773, 'site', 'top');
INSERT INTO `exp_search_log` VALUES(3, 1, 1, '96black', '127.0.0.1', 1365964785, 'site', 'tes');
INSERT INTO `exp_search_log` VALUES(4, 1, 1, '96black', '127.0.0.1', 1365964789, 'site', 'top');
INSERT INTO `exp_search_log` VALUES(5, 1, 1, '96black', '127.0.0.1', 1365965109, 'site', 'topc');
INSERT INTO `exp_search_log` VALUES(6, 1, 1, '96black', '127.0.0.1', 1365965121, 'site', 'topc');
INSERT INTO `exp_search_log` VALUES(7, 1, 1, '96black', '127.0.0.1', 1366071662, 'site', 'top');
INSERT INTO `exp_search_log` VALUES(8, 1, 1, '96black', '127.0.0.1', 1366076403, 'site', 'topcon');
INSERT INTO `exp_search_log` VALUES(9, 1, 0, '', '127.0.0.1', 1366095780, 'site', 'top');
INSERT INTO `exp_search_log` VALUES(10, 1, 1, '96black', '127.0.0.1', 1366097504, 'site', 'Survey &amp; Design');
INSERT INTO `exp_search_log` VALUES(11, 1, 1, '96black', '127.0.0.1', 1366097728, 'site', 'surv');
INSERT INTO `exp_search_log` VALUES(12, 1, 1, '96black', '127.0.0.1', 1366097768, 'site', 'surv');
INSERT INTO `exp_search_log` VALUES(13, 1, 1, '96black', '127.0.0.1', 1366104729, 'site', 'survey');

/* Table structure for table `exp_security_hashes` */
DROP TABLE IF EXISTS `exp_security_hashes`;

CREATE TABLE `exp_security_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `hash` varchar(40) NOT NULL,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB AUTO_INCREMENT=4868 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_security_hashes` */
INSERT INTO `exp_security_hashes` VALUES(4309, 1366095215, '127.0.0.1', 'c199b39b2d9546454a267a4724ea2c92f99cd50c');
INSERT INTO `exp_security_hashes` VALUES(4310, 1366095215, '127.0.0.1', '7cea938e4bd862724304f4d8b1414b45db47c63a');
INSERT INTO `exp_security_hashes` VALUES(4311, 1366095215, '127.0.0.1', '52f2b272f303d542451d499078e618d7175fe561');
INSERT INTO `exp_security_hashes` VALUES(4312, 1366095216, '127.0.0.1', '3e12fc2f1def505bba34960df13494d94741451f');
INSERT INTO `exp_security_hashes` VALUES(4313, 1366095217, '127.0.0.1', '23d676603ac290624e579498eb00b11b9f8ec470');
INSERT INTO `exp_security_hashes` VALUES(4314, 1366095229, '127.0.0.1', 'e88bf2a63262744bce4959a5e605727f21f94d5e');
INSERT INTO `exp_security_hashes` VALUES(4315, 1366095240, '127.0.0.1', '2d6b0c1745ea5aee271f309de75c6ba8591199a4');
INSERT INTO `exp_security_hashes` VALUES(4316, 1366095240, '127.0.0.1', '69a39ae04863e0d280d7e4d988b3b127f32398c3');
INSERT INTO `exp_security_hashes` VALUES(4317, 1366095241, '127.0.0.1', 'b369bbbd47b16ff6e7d7fab4b7619f7b1d3350ba');
INSERT INTO `exp_security_hashes` VALUES(4318, 1366095241, '127.0.0.1', 'a6db57c0f5503316b4590f9373d700adcd123946');
INSERT INTO `exp_security_hashes` VALUES(4319, 1366095242, '127.0.0.1', 'c4a3b4ae2143655f728e1e00fd5e61f61828ccbd');
INSERT INTO `exp_security_hashes` VALUES(4320, 1366095242, '127.0.0.1', '57a77cdee3d9603464e9c5931e8dd739ac1503f1');
INSERT INTO `exp_security_hashes` VALUES(4321, 1366095242, '127.0.0.1', '64e10fb747a0a5a67216e49adf9a780ab953db0b');
INSERT INTO `exp_security_hashes` VALUES(4322, 1366095243, '127.0.0.1', 'e4e3d2c7d86b463453148dca4c2167a5da83787e');
INSERT INTO `exp_security_hashes` VALUES(4323, 1366095244, '127.0.0.1', '7a4a263cf7a69740b7a028e0d528edd0ec859d5a');
INSERT INTO `exp_security_hashes` VALUES(4324, 1366095244, '127.0.0.1', '043ecbbad91a2a4403474a3eef3755382cfda0c4');
INSERT INTO `exp_security_hashes` VALUES(4325, 1366095244, '127.0.0.1', '9259edaca48192f37eae3b9a6e482b49233757d8');
INSERT INTO `exp_security_hashes` VALUES(4326, 1366095244, '127.0.0.1', '911a801333b1dcca00ab8523a12916c74b6d23f9');
INSERT INTO `exp_security_hashes` VALUES(4327, 1366095246, '127.0.0.1', '5829c4d4ad1b3d1c089f0dad9cecb24749d3ff1d');
INSERT INTO `exp_security_hashes` VALUES(4328, 1366095246, '127.0.0.1', '4c98e2a6f85265d774cf1c075c5bb9c7caa26b1f');
INSERT INTO `exp_security_hashes` VALUES(4329, 1366095246, '127.0.0.1', 'a02b2c58a904bf4f47daff1d03a77f8063adebae');
INSERT INTO `exp_security_hashes` VALUES(4330, 1366095247, '127.0.0.1', 'e3e9f20643cc89334bf67f68997c84f6e33ce9cd');
INSERT INTO `exp_security_hashes` VALUES(4331, 1366095248, '127.0.0.1', '0e7103ed7c31eded5325ca8ebca259598fc7b674');
INSERT INTO `exp_security_hashes` VALUES(4332, 1366095250, '127.0.0.1', 'e2a20b9a70325295c5e61ae6c4c9c04a0c87aed7');
INSERT INTO `exp_security_hashes` VALUES(4333, 1366095261, '127.0.0.1', 'ea792ff677ef6e1d1c80b5a9bb8f2b7c3e86e8a8');
INSERT INTO `exp_security_hashes` VALUES(4334, 1366095261, '127.0.0.1', '99348548cd51a64f6f5195bde2e318d9eed8de7e');
INSERT INTO `exp_security_hashes` VALUES(4335, 1366095261, '127.0.0.1', 'be21caeffab2f205c5a903fa051137b410abdf01');
INSERT INTO `exp_security_hashes` VALUES(4336, 1366095262, '127.0.0.1', '5cbbab02f279fe9e18ce9aacbbbeaae5baad4c8a');
INSERT INTO `exp_security_hashes` VALUES(4337, 1366095284, '127.0.0.1', 'a34ef56327bb6fe475253a37fd3a9acb303f4d93');
INSERT INTO `exp_security_hashes` VALUES(4338, 1366095284, '127.0.0.1', '2d547a8987abfe27da1d000a4804ed83115a0104');
INSERT INTO `exp_security_hashes` VALUES(4339, 1366095284, '127.0.0.1', '9062765413a128e8db71c417bbba4e24afed34ff');
INSERT INTO `exp_security_hashes` VALUES(4340, 1366095285, '127.0.0.1', '431322c87bba3f66bc8886033add3b957edae31b');
INSERT INTO `exp_security_hashes` VALUES(4341, 1366095288, '127.0.0.1', 'e355b116a044d4ff091b89d716efacfb423522ac');
INSERT INTO `exp_security_hashes` VALUES(4342, 1366095288, '127.0.0.1', '08a5f0525ea39d40c1a5ad9c21d04521d7cbfa36');
INSERT INTO `exp_security_hashes` VALUES(4343, 1366095288, '127.0.0.1', '2c177bf230bae954081088dfb61a9e2e1f510063');
INSERT INTO `exp_security_hashes` VALUES(4344, 1366095288, '127.0.0.1', '2700a5addb0e32aad7c69780bceec26073b131f3');
INSERT INTO `exp_security_hashes` VALUES(4345, 1366095301, '127.0.0.1', '661c8a6b72b6381c9f0365e6b5f32e8a45e1533f');
INSERT INTO `exp_security_hashes` VALUES(4346, 1366095301, '127.0.0.1', 'e8ae91bfdd92bf0968f2c74386bfccb3b45af091');
INSERT INTO `exp_security_hashes` VALUES(4347, 1366095301, '127.0.0.1', '6c9ce5bc0cfe872a0764b335a6c715799ad70701');
INSERT INTO `exp_security_hashes` VALUES(4348, 1366095301, '127.0.0.1', '550d5133b671a50db8aedf43947cbf90e7af2ff2');
INSERT INTO `exp_security_hashes` VALUES(4349, 1366095490, '127.0.0.1', 'bd7a460d55a132fd301ea23a45cf5e584cf1477a');
INSERT INTO `exp_security_hashes` VALUES(4350, 1366095490, '127.0.0.1', 'cf476275c6b01f3b00a2efba8c00677b2737bf6f');
INSERT INTO `exp_security_hashes` VALUES(4351, 1366095490, '127.0.0.1', '2a55ac2c54a47f84c4edbc8117dfbcd49c6e8d12');
INSERT INTO `exp_security_hashes` VALUES(4352, 1366095490, '127.0.0.1', '2210e4bc626b1ab07363f313fc13e7b376ceeeb0');
INSERT INTO `exp_security_hashes` VALUES(4353, 1366095526, '127.0.0.1', '72960c207b1399abe034da46fe7ee645e95d2fb8');
INSERT INTO `exp_security_hashes` VALUES(4354, 1366095526, '127.0.0.1', 'f2b65e31ca135d10573a6a2bb1736b124de21d26');
INSERT INTO `exp_security_hashes` VALUES(4355, 1366095526, '127.0.0.1', '033bcc69a01d9c7f3c9ff74c66dc0ab843b3c64b');
INSERT INTO `exp_security_hashes` VALUES(4356, 1366095527, '127.0.0.1', 'a61b0ffc7f05097e2a15b25baa9a3bfef22f57cd');
INSERT INTO `exp_security_hashes` VALUES(4357, 1366095551, '127.0.0.1', 'f33816b1118ffaf670bdbdd85a1b3ff833f3ff08');
INSERT INTO `exp_security_hashes` VALUES(4358, 1366095551, '127.0.0.1', '10e93b644a7af96a8a277ce2eb1d874e6628c355');
INSERT INTO `exp_security_hashes` VALUES(4359, 1366095551, '127.0.0.1', '4a4f4ab2781ca0c983f7d40e78b27ecb20e013ff');
INSERT INTO `exp_security_hashes` VALUES(4360, 1366095552, '127.0.0.1', 'f06ccdee3a6e7bd40d6c4af86765d542599d8d8b');
INSERT INTO `exp_security_hashes` VALUES(4361, 1366095580, '127.0.0.1', '1c9ad0b6ddf01335e05e3a4af5846451ff1bc29a');
INSERT INTO `exp_security_hashes` VALUES(4362, 1366095580, '127.0.0.1', '5591d276ddbd8d87a653d2661a53d3b62b44ee9b');
INSERT INTO `exp_security_hashes` VALUES(4363, 1366095580, '127.0.0.1', 'd05fff191118c1cc1f2122d6368ff06dafef786a');
INSERT INTO `exp_security_hashes` VALUES(4364, 1366095580, '127.0.0.1', '71410df239cee5144237869ffebd990f1ea79958');
INSERT INTO `exp_security_hashes` VALUES(4365, 1366095580, '127.0.0.1', 'a44f89a5d4e8da02fb900f62dd60c44a72b4f781');
INSERT INTO `exp_security_hashes` VALUES(4366, 1366095580, '127.0.0.1', 'e6f65cb4ee6b63b600f48772df14357ffb638ed9');
INSERT INTO `exp_security_hashes` VALUES(4367, 1366095581, '127.0.0.1', '421cf147ea18f24c63687b67f0da43c931aeea81');
INSERT INTO `exp_security_hashes` VALUES(4368, 1366095581, '127.0.0.1', 'd37400167873b20ccc6da61cbcde3558500053ea');
INSERT INTO `exp_security_hashes` VALUES(4369, 1366095582, '127.0.0.1', '7a50e738ba1c7b86b1a4b879ede39503fa7a553a');
INSERT INTO `exp_security_hashes` VALUES(4370, 1366095583, '127.0.0.1', '571250fbfd83d6e54300458163755b79544af95c');
INSERT INTO `exp_security_hashes` VALUES(4371, 1366095583, '127.0.0.1', '9a72cebfea8657845a072b588ae90c52d8a520e4');
INSERT INTO `exp_security_hashes` VALUES(4372, 1366095583, '127.0.0.1', '3b5f66cc066dde9a55f0e7362de3a735b3c15aef');
INSERT INTO `exp_security_hashes` VALUES(4373, 1366095584, '127.0.0.1', '70f1e32ab6e8846dc2ab0ff733ecea1a0410556c');
INSERT INTO `exp_security_hashes` VALUES(4374, 1366095584, '127.0.0.1', '866ce8ce892fd77bd92c17ef981d60b90c0260b3');
INSERT INTO `exp_security_hashes` VALUES(4375, 1366095749, '127.0.0.1', '6943251f2386d032670a82519ab2cd18bbb974c3');
INSERT INTO `exp_security_hashes` VALUES(4376, 1366095750, '127.0.0.1', '17a7730ad9257b9a9d1dc920250f524f447b0ea9');
INSERT INTO `exp_security_hashes` VALUES(4377, 1366095750, '127.0.0.1', '7184bd3015b3e6512949ba16ad08273c9a32b326');
INSERT INTO `exp_security_hashes` VALUES(4378, 1366095754, '127.0.0.1', 'e2fbab82eef9ef01c2a9283a9eb315e27fa723c0');
INSERT INTO `exp_security_hashes` VALUES(4379, 1366095781, '127.0.0.1', '5240f33a4c98d34f0ebc7addbc0747d2a75c2b15');
INSERT INTO `exp_security_hashes` VALUES(4380, 1366095781, '127.0.0.1', '7d734777d0ae15eaa8f75870a299fd9965b973a1');
INSERT INTO `exp_security_hashes` VALUES(4381, 1366095781, '127.0.0.1', '34c71776bb505967cedcdfaa02cfeaa71c45353b');
INSERT INTO `exp_security_hashes` VALUES(4382, 1366095782, '127.0.0.1', '52f539b873975364f7b3921fff2272ecca0ae7ba');
INSERT INTO `exp_security_hashes` VALUES(4383, 1366095786, '127.0.0.1', 'e25c4b248fb4b10033dfd1da439d5e28a6bc6bf4');
INSERT INTO `exp_security_hashes` VALUES(4384, 1366095788, '127.0.0.1', 'da9160d6d6e3d65e79f700eba1e24d1b1de40636');
INSERT INTO `exp_security_hashes` VALUES(4385, 1366095857, '127.0.0.1', '120acfebfa952b85d1ad1c0e74a7b9758add5b15');
INSERT INTO `exp_security_hashes` VALUES(4386, 1366095857, '127.0.0.1', 'dc0913f5b0bd53d02b09737adcdc7e2c1abc291f');
INSERT INTO `exp_security_hashes` VALUES(4387, 1366095858, '127.0.0.1', '6309954d290c4d50243d0965bf124fefe86dfaf4');
INSERT INTO `exp_security_hashes` VALUES(4388, 1366096130, '127.0.0.1', 'aae0f3c5bd9919420084f6007fa3d6f089a71962');
INSERT INTO `exp_security_hashes` VALUES(4389, 1366096130, '127.0.0.1', '371c2e2725ddd9e39cf590965656289bac2e0509');
INSERT INTO `exp_security_hashes` VALUES(4390, 1366096131, '127.0.0.1', '2afe8a8f49ac0137b2fe2f4e8556cbf7d9a55800');
INSERT INTO `exp_security_hashes` VALUES(4391, 1366096141, '127.0.0.1', '60cd1a108f238ccf66bb19a0050f4c8b58c3c6d8');
INSERT INTO `exp_security_hashes` VALUES(4392, 1366096141, '127.0.0.1', '69a27d5de240d6499fb93961c7cc98d9b0003dd5');
INSERT INTO `exp_security_hashes` VALUES(4393, 1366096141, '127.0.0.1', 'b56cc25a0002d73322b21f4d20432c8af48e5d2c');
INSERT INTO `exp_security_hashes` VALUES(4394, 1366096142, '127.0.0.1', '67725c5fa76982fe4b32211a67fe0fd302f615eb');
INSERT INTO `exp_security_hashes` VALUES(4395, 1366096155, '127.0.0.1', '9ace2feee9f7b808a949333076290b01cf40448c');
INSERT INTO `exp_security_hashes` VALUES(4396, 1366096155, '127.0.0.1', '4cceb76ead33683889a05e3e9ab266e281084a92');
INSERT INTO `exp_security_hashes` VALUES(4397, 1366096155, '127.0.0.1', '504efc77660d20a8610ccf0942d0ee5d2966cce2');
INSERT INTO `exp_security_hashes` VALUES(4398, 1366096159, '127.0.0.1', 'e75fa752e980cbc487d5f06b34f84c6228dc749f');
INSERT INTO `exp_security_hashes` VALUES(4399, 1366096159, '127.0.0.1', '3b6db64cc08719482e6d7210579204a1cd1ed0c4');
INSERT INTO `exp_security_hashes` VALUES(4400, 1366096160, '127.0.0.1', '79c0e41df0d572cc200552a6274bd8d22f808b01');
INSERT INTO `exp_security_hashes` VALUES(4401, 1366096170, '127.0.0.1', '30d2bef88494f6da277c04e3284344dc2d93f13d');
INSERT INTO `exp_security_hashes` VALUES(4402, 1366096170, '127.0.0.1', '3338b5c2167ad303dfb23241000ad920fe6413ed');
INSERT INTO `exp_security_hashes` VALUES(4403, 1366096179, '127.0.0.1', 'd2d6a451e99164ffdcc83d90c5beb3fd2bac98fe');
INSERT INTO `exp_security_hashes` VALUES(4404, 1366096182, '127.0.0.1', '0556e9fdbdbef0cef0851ac545d3bd64bbc80132');
INSERT INTO `exp_security_hashes` VALUES(4405, 1366096183, '127.0.0.1', '1b1a6b077fe4f52f07a56ea4a3b11092f5eef8d0');
INSERT INTO `exp_security_hashes` VALUES(4406, 1366096186, '127.0.0.1', '7762203d991b413ec4a006ce3cdd344938697cc8');
INSERT INTO `exp_security_hashes` VALUES(4407, 1366096186, '127.0.0.1', 'a890874fcaaec8e1f1be1b5451693a5cc0365366');
INSERT INTO `exp_security_hashes` VALUES(4408, 1366096187, '127.0.0.1', 'd52244ba6893659daa66cace783b36e65ede1621');
INSERT INTO `exp_security_hashes` VALUES(4409, 1366096187, '127.0.0.1', 'f20d2a30392cda91193ca7cd600515c17d5fc420');
INSERT INTO `exp_security_hashes` VALUES(4410, 1366096189, '127.0.0.1', 'f3565b5d57ee468a6be5aedf8a6c785f508a4dc1');
INSERT INTO `exp_security_hashes` VALUES(4411, 1366096191, '127.0.0.1', 'fb969fbb3ae3052e74e910b5d951a3ac59aa342e');
INSERT INTO `exp_security_hashes` VALUES(4412, 1366096192, '127.0.0.1', '6ec987d4990a631fe9153662777b53015cbdf2b7');
INSERT INTO `exp_security_hashes` VALUES(4413, 1366096225, '127.0.0.1', '11b506e94c95f191a430c505d66fd1e2415129fd');
INSERT INTO `exp_security_hashes` VALUES(4414, 1366096226, '127.0.0.1', 'a4de30f4194745fe294e3c9881984af92e839efa');
INSERT INTO `exp_security_hashes` VALUES(4415, 1366096226, '127.0.0.1', 'eab2c350dc5c3c51a2b948fabc0e41e3d421aabd');
INSERT INTO `exp_security_hashes` VALUES(4416, 1366096227, '127.0.0.1', 'f54e8336149cff1c5dc2854ffa65c879c73a31b8');
INSERT INTO `exp_security_hashes` VALUES(4417, 1366096227, '127.0.0.1', '5708a316387623e9a5f78b6126fef51cda3c9a09');
INSERT INTO `exp_security_hashes` VALUES(4418, 1366096227, '127.0.0.1', 'eec871e53508986178a08f2c9cab104af24f28ce');
INSERT INTO `exp_security_hashes` VALUES(4419, 1366096227, '127.0.0.1', 'deef1920f577d9e4fbff1759021a9a783d090a06');
INSERT INTO `exp_security_hashes` VALUES(4420, 1366096228, '127.0.0.1', 'bff2ed7c80d64b127211d81520075264561e3dc9');
INSERT INTO `exp_security_hashes` VALUES(4421, 1366096321, '127.0.0.1', 'a3c1a5e9f79dbd7217425a68b75511969db4e463');
INSERT INTO `exp_security_hashes` VALUES(4422, 1366096321, '127.0.0.1', 'eac6fcd8110cef041aac1f1f5f970e2db5eb075f');
INSERT INTO `exp_security_hashes` VALUES(4423, 1366096321, '127.0.0.1', '15a9e62c8d7376898ef7c8094412bec1fb0b7b4a');
INSERT INTO `exp_security_hashes` VALUES(4424, 1366096322, '127.0.0.1', 'dc4a406793de7524ed0d73a0f7e44ea1d12271e7');
INSERT INTO `exp_security_hashes` VALUES(4425, 1366096337, '127.0.0.1', '4c383fdbe1c6c0cd44f133cd189f342bbc0af8d2');
INSERT INTO `exp_security_hashes` VALUES(4426, 1366096337, '127.0.0.1', '12c115c1678cecca4ead3928f2171415ebfbbbc5');
INSERT INTO `exp_security_hashes` VALUES(4427, 1366096338, '127.0.0.1', '43b864a2c87457512bc5951c031b32b975338b46');
INSERT INTO `exp_security_hashes` VALUES(4428, 1366096338, '127.0.0.1', '3be761bffb57567acf95d02988ef1947fbda3182');
INSERT INTO `exp_security_hashes` VALUES(4429, 1366096344, '127.0.0.1', '7547487c35b719ce2df12772ca3b73f82a1826b2');
INSERT INTO `exp_security_hashes` VALUES(4430, 1366096344, '127.0.0.1', '7ea7d0439fef0ddc7acb6db785218f2d6184c10c');
INSERT INTO `exp_security_hashes` VALUES(4431, 1366096345, '127.0.0.1', 'b1ae8b49641838b2cbf8b510eaf26b08cabbbd9d');
INSERT INTO `exp_security_hashes` VALUES(4432, 1366096345, '127.0.0.1', '279594802473a79779b50cc1e091344f7003d95c');
INSERT INTO `exp_security_hashes` VALUES(4433, 1366096375, '127.0.0.1', 'ec0355cc9077346d0dd0a8934d0516c5c98486e1');
INSERT INTO `exp_security_hashes` VALUES(4434, 1366096376, '127.0.0.1', '63b56f6d450071f761f1415b25ca68d8e751a81a');
INSERT INTO `exp_security_hashes` VALUES(4435, 1366096387, '127.0.0.1', 'e8224bfa6896d19bae6d6db1e6803c916a2def48');
INSERT INTO `exp_security_hashes` VALUES(4436, 1366096388, '127.0.0.1', '3c98f519a18d7f181b3ff1a3dc7ceb0a67ce7f38');
INSERT INTO `exp_security_hashes` VALUES(4437, 1366096389, '127.0.0.1', '1c444503284b728c9f5480f63ec382c35b9b2afc');
INSERT INTO `exp_security_hashes` VALUES(4438, 1366096389, '127.0.0.1', '287cf293738c563b68c047f1c6f709fa7f7eeffc');
INSERT INTO `exp_security_hashes` VALUES(4439, 1366096390, '127.0.0.1', '8dd0dc7c4afc7e7f5704c13331dc6346c8d09ec9');
INSERT INTO `exp_security_hashes` VALUES(4440, 1366096393, '127.0.0.1', 'da21c00f3a51687329d2e1db1a1d514bb3855de0');
INSERT INTO `exp_security_hashes` VALUES(4441, 1366096394, '127.0.0.1', 'f9b07a349b6cb8af639fcfbad87a9b021ef9fd80');
INSERT INTO `exp_security_hashes` VALUES(4442, 1366096684, '127.0.0.1', 'aacd2520fcb1aeee453a57d8bc7979cd675fcab9');
INSERT INTO `exp_security_hashes` VALUES(4443, 1366096684, '127.0.0.1', '2b6c57b448404adfb57ffc79e290cf11e183c748');
INSERT INTO `exp_security_hashes` VALUES(4444, 1366096685, '127.0.0.1', '63babdf427d7135a84bfdb390e90aedec1b0ac72');
INSERT INTO `exp_security_hashes` VALUES(4445, 1366096685, '127.0.0.1', 'af550828fac1605e8f58a7e7d2d1d4b3474a73e6');
INSERT INTO `exp_security_hashes` VALUES(4446, 1366096847, '127.0.0.1', '97226ffaa2688699bcea29e17d74d07b04a37a51');
INSERT INTO `exp_security_hashes` VALUES(4447, 1366096847, '127.0.0.1', '62bf96220396401cb309a22759f9986fd44f489c');
INSERT INTO `exp_security_hashes` VALUES(4448, 1366096847, '127.0.0.1', 'b8b6012b076ceab05fbf2dbc245b00a5ae43d066');
INSERT INTO `exp_security_hashes` VALUES(4449, 1366096848, '127.0.0.1', '39933f8263105c4dd6f10db0b42e7d97683b6d04');
INSERT INTO `exp_security_hashes` VALUES(4450, 1366096871, '127.0.0.1', 'c2ffcf09b56fb356f8fca800d303b79435cc242a');
INSERT INTO `exp_security_hashes` VALUES(4451, 1366096871, '127.0.0.1', '4e2c4bfb58fc387f724480e34ece6495db6199f0');
INSERT INTO `exp_security_hashes` VALUES(4452, 1366096872, '127.0.0.1', '09e12886676e75870a118404e4f0241fa7362090');
INSERT INTO `exp_security_hashes` VALUES(4453, 1366096874, '127.0.0.1', '795a637e74dc21b858a23f1358d590da7490b1c1');
INSERT INTO `exp_security_hashes` VALUES(4454, 1366096874, '127.0.0.1', '20b16ad3c4eb4834c4db8704549dd7a3793d1e92');
INSERT INTO `exp_security_hashes` VALUES(4455, 1366096875, '127.0.0.1', 'da81ae9ebacf739fe931d3234333bad3c0b958ba');
INSERT INTO `exp_security_hashes` VALUES(4456, 1366096875, '127.0.0.1', 'fbc14bdfbfc68e08c0ae5e44c79628b9d6f05105');
INSERT INTO `exp_security_hashes` VALUES(4457, 1366096891, '127.0.0.1', '4a9e0f401e70b6b6fa42dba31f75ea32b1b2973f');
INSERT INTO `exp_security_hashes` VALUES(4458, 1366096891, '127.0.0.1', '57f10ccfa93f09d8b52026241ec498f32eda8476');
INSERT INTO `exp_security_hashes` VALUES(4459, 1366096892, '127.0.0.1', 'eb41182ad706d2e68586778af2853c6a005773c4');
INSERT INTO `exp_security_hashes` VALUES(4460, 1366096892, '127.0.0.1', 'e7046661f15d48adb2fdad5d7071a1091cad1679');
INSERT INTO `exp_security_hashes` VALUES(4461, 1366096927, '127.0.0.1', '5ce80d89ccb5ddb032499ce8347ce8b8b4d512b0');
INSERT INTO `exp_security_hashes` VALUES(4462, 1366096927, '127.0.0.1', 'c5d8509bb38dd766931f0d9875df7c6eaa445dd1');
INSERT INTO `exp_security_hashes` VALUES(4463, 1366096928, '127.0.0.1', 'ff1ac6fcbd0bcc239b1c8988be4a9c753c1eac8d');
INSERT INTO `exp_security_hashes` VALUES(4464, 1366096928, '127.0.0.1', '7da885c871c4b0937ae6e258b67a76fcc24d1d67');
INSERT INTO `exp_security_hashes` VALUES(4465, 1366096983, '127.0.0.1', '1e77d4a1a9bb00f2fa689fb0a6a08550e23cf077');
INSERT INTO `exp_security_hashes` VALUES(4466, 1366096983, '127.0.0.1', 'fcc35de982b10d70be3cd86baa87896fb1ed7abd');
INSERT INTO `exp_security_hashes` VALUES(4467, 1366096983, '127.0.0.1', 'df0ef06b342f9caaa2cf16c7d2ea51ac7866ba5e');
INSERT INTO `exp_security_hashes` VALUES(4468, 1366096984, '127.0.0.1', '5fb1bdc465c5fd220e957714fa15bbed743595b1');
INSERT INTO `exp_security_hashes` VALUES(4469, 1366097040, '127.0.0.1', '67d29ffb18e87e48f4de77eedaa69ebaf2481d6f');
INSERT INTO `exp_security_hashes` VALUES(4470, 1366097040, '127.0.0.1', 'ac1bcb5df60550e8787e3a0afceffcb10d91a5a1');
INSERT INTO `exp_security_hashes` VALUES(4471, 1366097041, '127.0.0.1', 'eaf291e4b2ed0111f64731d1d7e6d98934d4d2eb');
INSERT INTO `exp_security_hashes` VALUES(4472, 1366097041, '127.0.0.1', 'e353dc6bf00e2b11d6227defbe52dba2316791a1');
INSERT INTO `exp_security_hashes` VALUES(4473, 1366097046, '127.0.0.1', '7bb859abf2148af9917dc8102e94a02224c7a2ee');
INSERT INTO `exp_security_hashes` VALUES(4474, 1366097046, '127.0.0.1', 'b02399ecead28dfc0218650d3fc06a0c64fe27ab');
INSERT INTO `exp_security_hashes` VALUES(4475, 1366097046, '127.0.0.1', '45a90d15f6563ae6ffc5eb2390719406c731128a');
INSERT INTO `exp_security_hashes` VALUES(4476, 1366097046, '127.0.0.1', '4d4034842fc06546e3a26c274071d678027c31c3');
INSERT INTO `exp_security_hashes` VALUES(4477, 1366097047, '127.0.0.1', 'd5a155accca6adfa38a36d36de7290227702731c');
INSERT INTO `exp_security_hashes` VALUES(4478, 1366097048, '127.0.0.1', 'fefdfc704892115eb4bea1636c288a99e94ff540');
INSERT INTO `exp_security_hashes` VALUES(4479, 1366097069, '127.0.0.1', '5e3dd56f5433af96455d44256aac2696b1b1403f');
INSERT INTO `exp_security_hashes` VALUES(4480, 1366097069, '127.0.0.1', '700a3bc0730079e4f0c45e1e48034005fc613f6f');
INSERT INTO `exp_security_hashes` VALUES(4481, 1366097070, '127.0.0.1', '9dcb36e40423afdf63df091a3e5d99c6b6ce7fe6');
INSERT INTO `exp_security_hashes` VALUES(4482, 1366097306, '127.0.0.1', '017362885faa1fe80055ab0a03ce80642832465d');
INSERT INTO `exp_security_hashes` VALUES(4483, 1366097306, '127.0.0.1', 'c561cc58b407b395208fc4998900dc0361ffee07');
INSERT INTO `exp_security_hashes` VALUES(4484, 1366097307, '127.0.0.1', '188b7523876b07f12efac2c8acb5ceeae055d84a');
INSERT INTO `exp_security_hashes` VALUES(4485, 1366097493, '127.0.0.1', '61232d5e82d07296db314a3dedcc79524af68d5e');
INSERT INTO `exp_security_hashes` VALUES(4486, 1366097493, '127.0.0.1', 'bb8c408d87f42729c6ef2683e888410a6e76e4b1');
INSERT INTO `exp_security_hashes` VALUES(4487, 1366097493, '127.0.0.1', 'dd54f93e108fa209ce09e3a537bbfadb3e63e5a0');
INSERT INTO `exp_security_hashes` VALUES(4488, 1366097494, '127.0.0.1', '8b2750a01418695cda86af7c982c4fde6f0165fc');
INSERT INTO `exp_security_hashes` VALUES(4489, 1366097494, '127.0.0.1', 'b246afd5580866452a5a1d23c4a762b4f165d283');
INSERT INTO `exp_security_hashes` VALUES(4490, 1366097495, '127.0.0.1', '500e1f074edb44b2ccfd1eed577ede79076ad007');
INSERT INTO `exp_security_hashes` VALUES(4491, 1366097495, '127.0.0.1', '255abd40c1e957825ce347083cf5db98e1dad681');
INSERT INTO `exp_security_hashes` VALUES(4492, 1366097504, '127.0.0.1', '29f3734d3f7db7ac2404d0243abc6c59451ff010');
INSERT INTO `exp_security_hashes` VALUES(4493, 1366097504, '127.0.0.1', '0ef947e5137b29d3283f84c12a91b0410b8eadb9');
INSERT INTO `exp_security_hashes` VALUES(4494, 1366097505, '127.0.0.1', '9814d247b202c7975ff44856deee3a2b2856878b');
INSERT INTO `exp_security_hashes` VALUES(4495, 1366097505, '127.0.0.1', '974a284efc6ed6221c72711c752a3f350c7f496d');
INSERT INTO `exp_security_hashes` VALUES(4496, 1366097708, '127.0.0.1', '4f4e68fbce41aec956b7d6873a6b0ae38441f60f');
INSERT INTO `exp_security_hashes` VALUES(4497, 1366097709, '127.0.0.1', '047f248ce3b9de526c0a7d442b53cae459be9cba');
INSERT INTO `exp_security_hashes` VALUES(4498, 1366097709, '127.0.0.1', 'd10283d4cdcaced4ff218a4e2e1368c8e699f5ac');
INSERT INTO `exp_security_hashes` VALUES(4499, 1366097710, '127.0.0.1', '471afa88cc24b7d24c1a7ac1f4b5fcaf2e77b12e');
INSERT INTO `exp_security_hashes` VALUES(4500, 1366097710, '127.0.0.1', '52c88533a4206c6f8f0e5e40bf776056f5aa5816');
INSERT INTO `exp_security_hashes` VALUES(4501, 1366097710, '127.0.0.1', 'f8db5815dfcbdebfcee9d3c68751be114d0f7abc');
INSERT INTO `exp_security_hashes` VALUES(4502, 1366097710, '127.0.0.1', '7da9262ac219ea89f51cbeefff9b02389ea081d5');
INSERT INTO `exp_security_hashes` VALUES(4503, 1366097711, '127.0.0.1', 'd6fb973ddca85ed6f4de00a8f4fc0a48ef1c0745');
INSERT INTO `exp_security_hashes` VALUES(4504, 1366097711, '127.0.0.1', 'fb7c74606e0779f48609f557a899a4a7ed9c889d');
INSERT INTO `exp_security_hashes` VALUES(4505, 1366097712, '127.0.0.1', '5201de5f2c718d3bc32d600396a37532082400fc');
INSERT INTO `exp_security_hashes` VALUES(4506, 1366097712, '127.0.0.1', '091275624150328f8c05a3504d696b95352ef58e');
INSERT INTO `exp_security_hashes` VALUES(4507, 1366097723, '127.0.0.1', '12ce6276cb7fcc5c19fe09f8f9593100df9363d2');
INSERT INTO `exp_security_hashes` VALUES(4508, 1366097726, '127.0.0.1', '9fcfd858732396961d90432ab45b437329331413');
INSERT INTO `exp_security_hashes` VALUES(4509, 1366097729, '127.0.0.1', '5aeecd0c93f6bf2e35da262cb06e6a2119c55c84');
INSERT INTO `exp_security_hashes` VALUES(4510, 1366097729, '127.0.0.1', '3f177f8653e2a2ba9c35d651acba4d368927b6b8');
INSERT INTO `exp_security_hashes` VALUES(4511, 1366097729, '127.0.0.1', '7df9bebefc660389c56123a51332b99293d41ba2');
INSERT INTO `exp_security_hashes` VALUES(4512, 1366097730, '127.0.0.1', 'de3515ae55bf7e00024855aa43ee2f0404085b09');
INSERT INTO `exp_security_hashes` VALUES(4513, 1366097754, '127.0.0.1', '4b8807128dee8584716804287a22d9704dee7326');
INSERT INTO `exp_security_hashes` VALUES(4514, 1366097754, '127.0.0.1', 'ec216e8dca79b73d5eb35c3881b16f504d12772c');
INSERT INTO `exp_security_hashes` VALUES(4515, 1366097768, '127.0.0.1', '9d5ce65118be95e536627457b3af9f54d1ca22ec');
INSERT INTO `exp_security_hashes` VALUES(4516, 1366097768, '127.0.0.1', '3495057f3b1dc5afd63fcab71c619fc5bc59dbe0');
INSERT INTO `exp_security_hashes` VALUES(4517, 1366097769, '127.0.0.1', 'c69dbebcdb68214d864e2d3401eae6d0a37ff36d');
INSERT INTO `exp_security_hashes` VALUES(4518, 1366097769, '127.0.0.1', '4b649e7f0a52a0154697f18b4db7f18dbb72beab');
INSERT INTO `exp_security_hashes` VALUES(4519, 1366097777, '127.0.0.1', 'cfcb1e925d2554693434a8343d219600d2a668a2');
INSERT INTO `exp_security_hashes` VALUES(4520, 1366097777, '127.0.0.1', 'b9d2389d8916e317e6a280d991f9ef7ef33a6cf9');
INSERT INTO `exp_security_hashes` VALUES(4521, 1366097778, '127.0.0.1', '7179e42216c02720f5d1017651995dc679be59cf');
INSERT INTO `exp_security_hashes` VALUES(4522, 1366097778, '127.0.0.1', 'fb987b35f6774c6ce55840f80442ddba9df09482');
INSERT INTO `exp_security_hashes` VALUES(4523, 1366097778, '127.0.0.1', '5f7b3bcc9e421d479cecc0b7a7dd087936c7f986');
INSERT INTO `exp_security_hashes` VALUES(4524, 1366097779, '127.0.0.1', 'eccc914cb36597d67d20379ebae2ffa366bd9273');
INSERT INTO `exp_security_hashes` VALUES(4525, 1366097779, '127.0.0.1', '333621fde66961f34421b1982977ca80071eff4b');
INSERT INTO `exp_security_hashes` VALUES(4526, 1366097779, '127.0.0.1', '5deed8032910190b6b32bd3897d141359f6a31d0');
INSERT INTO `exp_security_hashes` VALUES(4527, 1366097962, '127.0.0.1', 'a562f23efe72e3357bc507f691b8c947666f9dd2');
INSERT INTO `exp_security_hashes` VALUES(4528, 1366097963, '127.0.0.1', 'b4391b6538b7a605c9083db1be63f14bd7c08742');
INSERT INTO `exp_security_hashes` VALUES(4529, 1366097963, '127.0.0.1', '1c689fdc1d1e6db90cf9c8edf6b2be3a539d115d');
INSERT INTO `exp_security_hashes` VALUES(4530, 1366097988, '127.0.0.1', 'aaffb10acf8f6807c01a5f4a2c6f2db4ca0f964a');
INSERT INTO `exp_security_hashes` VALUES(4531, 1366097989, '127.0.0.1', '0d8b368f41c9a4d8b245e9a72d8bab9f48e44448');
INSERT INTO `exp_security_hashes` VALUES(4532, 1366097989, '127.0.0.1', 'c121d153ae55f1e971bb164ca26dec7aa1811e0c');
INSERT INTO `exp_security_hashes` VALUES(4533, 1366097989, '127.0.0.1', '2ebcec260b6b6252d2ea81041d40ea3b4f3383f8');
INSERT INTO `exp_security_hashes` VALUES(4534, 1366097990, '127.0.0.1', '6927ce5d19e07a65c0a921916d3f58d8f511e792');
INSERT INTO `exp_security_hashes` VALUES(4535, 1366097990, '127.0.0.1', '69133a1b26aea7a19fe290b784b952b88b522a48');
INSERT INTO `exp_security_hashes` VALUES(4536, 1366098092, '127.0.0.1', 'b7ce6d1e710436630f18d60bcd71e86654d09519');
INSERT INTO `exp_security_hashes` VALUES(4537, 1366098092, '127.0.0.1', 'ea6892db79faeb307ad72bf4112570fcd3272e9d');
INSERT INTO `exp_security_hashes` VALUES(4538, 1366098094, '127.0.0.1', 'a974071ba534eebf2ad66aa16356f4b4d7cea9d8');
INSERT INTO `exp_security_hashes` VALUES(4539, 1366098094, '127.0.0.1', '90543e951594bacfda5f8384b2ac22c7d7ef8242');
INSERT INTO `exp_security_hashes` VALUES(4540, 1366098122, '127.0.0.1', '527d7dc7a9d7f24cd0bfea72cd5e9d78bb56b15e');
INSERT INTO `exp_security_hashes` VALUES(4541, 1366098123, '127.0.0.1', '74548171916dae0ab98527e692d1d8b43b064610');
INSERT INTO `exp_security_hashes` VALUES(4542, 1366098123, '127.0.0.1', 'ddbf86946a381e9650f901772887d19a1ce9b2a7');
INSERT INTO `exp_security_hashes` VALUES(4543, 1366099398, '127.0.0.1', 'da109898f4e8e90a0fd9455972fdd9c119c03152');
INSERT INTO `exp_security_hashes` VALUES(4544, 1366099399, '127.0.0.1', 'bf7b57b33441c1c25228f3a42b3965ee78ce2230');
INSERT INTO `exp_security_hashes` VALUES(4545, 1366099406, '127.0.0.1', '2b79146fee192584521c011c50fbb29c7f6cdb61');
INSERT INTO `exp_security_hashes` VALUES(4546, 1366099414, '127.0.0.1', 'dd9a6a656ec382a066f59baa810edab53af00c1f');
INSERT INTO `exp_security_hashes` VALUES(4547, 1366099482, '127.0.0.1', 'ef12b43cedac743749c6358c9c6f7b562ac41b3a');
INSERT INTO `exp_security_hashes` VALUES(4548, 1366099482, '127.0.0.1', 'ad6a68a0aa20044bef4d99db17e5e6fd6dfe27f7');
INSERT INTO `exp_security_hashes` VALUES(4549, 1366099484, '127.0.0.1', '97f900879d8318ac85162fe1cadb5c4796542342');
INSERT INTO `exp_security_hashes` VALUES(4550, 1366099485, '127.0.0.1', 'a60b06fe198fe0011e738994c84510708849cd6e');
INSERT INTO `exp_security_hashes` VALUES(4551, 1366099486, '127.0.0.1', '03a79f4440793a02ad61e320153c787b206cef86');
INSERT INTO `exp_security_hashes` VALUES(4552, 1366099487, '127.0.0.1', '1d8bc33642e41d8c40af086df190bf2e9219f773');
INSERT INTO `exp_security_hashes` VALUES(4553, 1366099528, '127.0.0.1', 'd77eeab4e2ae90e164accf88e801137db9fdcd76');
INSERT INTO `exp_security_hashes` VALUES(4554, 1366099528, '127.0.0.1', '7da31d0d529f76504ac0e6105580de87c807fa5d');
INSERT INTO `exp_security_hashes` VALUES(4555, 1366099529, '127.0.0.1', '229ed9c08802f578adc2f95a1725313b715d3a07');
INSERT INTO `exp_security_hashes` VALUES(4556, 1366099532, '127.0.0.1', '0024a7c7e7adc04de022934e8d7d85475c474519');
INSERT INTO `exp_security_hashes` VALUES(4557, 1366099533, '127.0.0.1', '03b3bf5583a5a09bf007a97f0235412f0c86f968');
INSERT INTO `exp_security_hashes` VALUES(4558, 1366099534, '127.0.0.1', '197f8f192a864f5818c06e3f078fe35bd3c6ac2b');
INSERT INTO `exp_security_hashes` VALUES(4559, 1366099534, '127.0.0.1', '97e1420834451df33c316a8138d427aa8866f9c4');
INSERT INTO `exp_security_hashes` VALUES(4560, 1366099534, '127.0.0.1', 'fbfeefddfd723b946e4cbed28c8cb3a97c70e82c');
INSERT INTO `exp_security_hashes` VALUES(4561, 1366099534, '127.0.0.1', '4d979cbe86d7d32530324c4cc4b1c6c0646f251b');
INSERT INTO `exp_security_hashes` VALUES(4562, 1366099534, '127.0.0.1', 'edd0637c88e7b3fe8b19fef630f0f899ade5cc68');
INSERT INTO `exp_security_hashes` VALUES(4563, 1366099535, '127.0.0.1', 'ca9671c734ec20c1242f80bfda376ed53c4f9886');
INSERT INTO `exp_security_hashes` VALUES(4564, 1366099535, '127.0.0.1', '33ec892d30dd46562b36930792979dc83175e7a7');
INSERT INTO `exp_security_hashes` VALUES(4565, 1366099536, '127.0.0.1', 'af4da962888db13394a5c2d9bb08d8cd367293ed');
INSERT INTO `exp_security_hashes` VALUES(4566, 1366099536, '127.0.0.1', '97371a83f02e611291bfec570c1a9404d0055747');
INSERT INTO `exp_security_hashes` VALUES(4567, 1366099536, '127.0.0.1', 'e29ce79ac488842d0638b1e4bdf38caa91b4cd32');
INSERT INTO `exp_security_hashes` VALUES(4568, 1366099538, '127.0.0.1', 'e2221ea7c695b66faa7969c2662d41bc0caeef29');
INSERT INTO `exp_security_hashes` VALUES(4569, 1366099538, '127.0.0.1', 'ee0f5c593bba107c6b1762cffe647fd054970706');
INSERT INTO `exp_security_hashes` VALUES(4570, 1366099539, '127.0.0.1', 'a58e2b2ce24a80d32b55de401d2a90c21f61733c');
INSERT INTO `exp_security_hashes` VALUES(4571, 1366099540, '127.0.0.1', '061dd0df4797f943edb118f851bcdf34ddb33419');
INSERT INTO `exp_security_hashes` VALUES(4572, 1366099541, '127.0.0.1', '80d7485afa6b9ab026f34cd8274044521342d49b');
INSERT INTO `exp_security_hashes` VALUES(4573, 1366099542, '127.0.0.1', '8cd83e214290a68e6129c147bf481aa69f9f9e0c');
INSERT INTO `exp_security_hashes` VALUES(4574, 1366099542, '127.0.0.1', '92821dccad3bd7521960f647aee4aeb7dc9de26f');
INSERT INTO `exp_security_hashes` VALUES(4575, 1366099542, '127.0.0.1', '1cd4843dfc72f4c1ef882ca2db257bb884d57fe8');
INSERT INTO `exp_security_hashes` VALUES(4576, 1366099542, '127.0.0.1', '56f523b70af2e2c2bfa81bdb930d2643f2d5fd6c');
INSERT INTO `exp_security_hashes` VALUES(4577, 1366099542, '127.0.0.1', '004e5fb9b58d8980298f245b5cd8d600b756a9a8');
INSERT INTO `exp_security_hashes` VALUES(4578, 1366099543, '127.0.0.1', 'd84a10da5eaa848e4416691eec3c417b8ebc244f');
INSERT INTO `exp_security_hashes` VALUES(4579, 1366099543, '127.0.0.1', '5c35cf092f3fa03828c519a4aadf3a2399457767');
INSERT INTO `exp_security_hashes` VALUES(4580, 1366099543, '127.0.0.1', 'a48691476f79a15e61ed62a5626b51e52ea73fd9');
INSERT INTO `exp_security_hashes` VALUES(4581, 1366099544, '127.0.0.1', '11fdc06d71e499aad8256e5f13f5ce40ae301c63');
INSERT INTO `exp_security_hashes` VALUES(4582, 1366099544, '127.0.0.1', 'af4080dde20ff47610e181438eb9f296c487f4fa');
INSERT INTO `exp_security_hashes` VALUES(4583, 1366099573, '127.0.0.1', '85e9e889849881b0b6975351e428fc14c1b5347e');
INSERT INTO `exp_security_hashes` VALUES(4584, 1366099573, '127.0.0.1', '5b5f2247d1ad762f626df021c4d8ab2a6643f6ed');
INSERT INTO `exp_security_hashes` VALUES(4585, 1366099574, '127.0.0.1', '5bb64105972b1cd143b5c534cb31bf870714a32d');
INSERT INTO `exp_security_hashes` VALUES(4586, 1366101110, '127.0.0.1', '1f7350068a5cb004fa3ac9258032b18548c4c990');
INSERT INTO `exp_security_hashes` VALUES(4587, 1366101111, '127.0.0.1', '6e19d7837e6098d510a852e2e9cf964dc543a895');
INSERT INTO `exp_security_hashes` VALUES(4588, 1366101127, '127.0.0.1', '61bcf53b058dc1a6ee7d521a5ddc46d9c6a53c46');
INSERT INTO `exp_security_hashes` VALUES(4589, 1366101128, '127.0.0.1', '46aaf7913900b9361a6f1ddd35f675f67df39c5a');
INSERT INTO `exp_security_hashes` VALUES(4590, 1366101128, '127.0.0.1', '9473ff72629b3bdf4bb3ad8f0223a022e9b5fd35');
INSERT INTO `exp_security_hashes` VALUES(4591, 1366101128, '127.0.0.1', '2a7d6857255740fec4d8074ca05ca57efb6c85d4');
INSERT INTO `exp_security_hashes` VALUES(4592, 1366101128, '127.0.0.1', '5e491b4846c5e45847a4c40892d35df9699a35b9');
INSERT INTO `exp_security_hashes` VALUES(4593, 1366101128, '127.0.0.1', '7411c09841c19726b88771267d139250343a1c28');
INSERT INTO `exp_security_hashes` VALUES(4594, 1366101129, '127.0.0.1', 'c55b68d89db4c799116f9ef361f1518c1dc02598');
INSERT INTO `exp_security_hashes` VALUES(4595, 1366101129, '127.0.0.1', 'e5f54cfc570b5e2db12fe59a56b0df1419997117');
INSERT INTO `exp_security_hashes` VALUES(4596, 1366101130, '127.0.0.1', '07ecd1e5c8ac6e4f267dc1e1699424afe1bb20ef');
INSERT INTO `exp_security_hashes` VALUES(4597, 1366101136, '127.0.0.1', '3dd1f6fc1c9feb5bf030d7c89563d278d7fd4c84');
INSERT INTO `exp_security_hashes` VALUES(4598, 1366101158, '127.0.0.1', 'fb811e76f99c3b2461d751e8898d18000b81b97a');
INSERT INTO `exp_security_hashes` VALUES(4599, 1366101158, '127.0.0.1', '7a09f16117439cabac2aa5af0f8a0f4f9b9afb5e');
INSERT INTO `exp_security_hashes` VALUES(4600, 1366101159, '127.0.0.1', 'ed861809570faa1b029df0b91997024faf2f262c');
INSERT INTO `exp_security_hashes` VALUES(4601, 1366101159, '127.0.0.1', '5136c76d4ce5cfbd4c294799f5aa0d57f038e10b');
INSERT INTO `exp_security_hashes` VALUES(4602, 1366101159, '127.0.0.1', '8048da3209791cca9df8857f9b58904d4ee8b471');
INSERT INTO `exp_security_hashes` VALUES(4603, 1366101159, '127.0.0.1', 'c050225cfd1ca13c7a665b421460649abbb264ef');
INSERT INTO `exp_security_hashes` VALUES(4604, 1366101159, '127.0.0.1', 'c0e61a428bd3293aaf0663eebeeb03811381e6de');
INSERT INTO `exp_security_hashes` VALUES(4605, 1366101159, '127.0.0.1', '7b22b24c9f37b6294a871e43eaf5de3a8edb5222');
INSERT INTO `exp_security_hashes` VALUES(4606, 1366101159, '127.0.0.1', 'd582d4e92669c96bda1586dbc4db7454a612c09e');
INSERT INTO `exp_security_hashes` VALUES(4607, 1366101160, '127.0.0.1', '79d07fc247cae37eb2bb343cb0fe9f6d710ff831');
INSERT INTO `exp_security_hashes` VALUES(4608, 1366101160, '127.0.0.1', '032d993fd46eb046326389db9bfe951b4da45de3');
INSERT INTO `exp_security_hashes` VALUES(4609, 1366101160, '127.0.0.1', 'cc04c321f0453cf3d1f330bc3c46f6a015bb9c58');
INSERT INTO `exp_security_hashes` VALUES(4610, 1366101160, '127.0.0.1', '42939735860ad1d2a6053f741bf284aa9b44e47f');
INSERT INTO `exp_security_hashes` VALUES(4611, 1366101160, '127.0.0.1', '95e1a5d870fa9d5d0332680096a631a6112d223a');
INSERT INTO `exp_security_hashes` VALUES(4612, 1366101161, '127.0.0.1', 'f984011f70322c13a2b017fea75accd6f41fe884');
INSERT INTO `exp_security_hashes` VALUES(4613, 1366101161, '127.0.0.1', 'a4862f18e2b33ecc2a4e6f279f4b7b3249eed891');
INSERT INTO `exp_security_hashes` VALUES(4614, 1366101162, '127.0.0.1', '979f833f7e468e025c5220ed8df5d80f9707ee75');
INSERT INTO `exp_security_hashes` VALUES(4615, 1366101162, '127.0.0.1', '0a58e7db0d0dfa2fd019fc42d4d33112e2e2c3ab');
INSERT INTO `exp_security_hashes` VALUES(4616, 1366101165, '127.0.0.1', 'b8f73bb7f670df56bc900aeaabe21d5b810bc4bf');
INSERT INTO `exp_security_hashes` VALUES(4617, 1366101165, '127.0.0.1', 'd7d6df0bcc48ee94703b2cebf1b8076ed79f2b7c');
INSERT INTO `exp_security_hashes` VALUES(4618, 1366101166, '127.0.0.1', 'e4a947b1c61489431ec2d265aae7f4bdb4732840');
INSERT INTO `exp_security_hashes` VALUES(4619, 1366101169, '127.0.0.1', '82ab3ab3b06aba1c2f8251ea7344b516bccd463f');
INSERT INTO `exp_security_hashes` VALUES(4620, 1366101169, '127.0.0.1', '8c7caa8b5b718ca25c22c3ecdd8853babfa3fe39');
INSERT INTO `exp_security_hashes` VALUES(4621, 1366101170, '127.0.0.1', 'b42ac52cd300e85f437bb2140f68c445541ee81e');
INSERT INTO `exp_security_hashes` VALUES(4622, 1366101204, '127.0.0.1', 'b1662bf504549280f13cbc91c881780a33e5279c');
INSERT INTO `exp_security_hashes` VALUES(4623, 1366101205, '127.0.0.1', '3d0a3ca4218ba035b2e0ca182cf0c0ef3d96e5c3');
INSERT INTO `exp_security_hashes` VALUES(4624, 1366101205, '127.0.0.1', '9e8bfcb88f2ce7d4de62f2d40d5fd6a04acf984f');
INSERT INTO `exp_security_hashes` VALUES(4625, 1366101205, '127.0.0.1', 'df59353f86cd332449c406da56e756dd73393d8f');
INSERT INTO `exp_security_hashes` VALUES(4626, 1366101205, '127.0.0.1', 'a454ac978e56a9cf74a1d173196fed4719688ef1');
INSERT INTO `exp_security_hashes` VALUES(4627, 1366101205, '127.0.0.1', '79942cf4c31242723519a005ad3b37698c8ae19d');
INSERT INTO `exp_security_hashes` VALUES(4628, 1366101206, '127.0.0.1', 'fc3f571d36a4928d189fba7916b608de8e9091a9');
INSERT INTO `exp_security_hashes` VALUES(4629, 1366101206, '127.0.0.1', '7dcf5a63e66a7bf661bb961f589c3d20854fc613');
INSERT INTO `exp_security_hashes` VALUES(4630, 1366101207, '127.0.0.1', 'acffff917eb751ac819e0beee43804ffcbb9cc8b');
INSERT INTO `exp_security_hashes` VALUES(4631, 1366101208, '127.0.0.1', 'a27e88fb846963e3bbd7cda0223585fc2c5e90b1');
INSERT INTO `exp_security_hashes` VALUES(4632, 1366101208, '127.0.0.1', 'a409212331f426090f2bdd4359532ee2faf74669');
INSERT INTO `exp_security_hashes` VALUES(4633, 1366101209, '127.0.0.1', '40258b1c800d5d785358802f2423429ffbd1bc1a');
INSERT INTO `exp_security_hashes` VALUES(4634, 1366101212, '127.0.0.1', 'f9ad3ec8e3c07f21913dcbee516f420338f74bf6');
INSERT INTO `exp_security_hashes` VALUES(4635, 1366101212, '127.0.0.1', '8d02388bd016904244bcad1934f28e946663279d');
INSERT INTO `exp_security_hashes` VALUES(4636, 1366101212, '127.0.0.1', 'ff20a5ae547f757867c638c75b0b8bf8172585f1');
INSERT INTO `exp_security_hashes` VALUES(4637, 1366101213, '127.0.0.1', 'e12d82724c6a7ee1832ee1001182d0cf2cdb67c2');
INSERT INTO `exp_security_hashes` VALUES(4638, 1366101213, '127.0.0.1', '33dae9d120e5cdb7fda675dcdf34351d94679890');
INSERT INTO `exp_security_hashes` VALUES(4639, 1366101213, '127.0.0.1', '4da3b4fc9615518233f9e00edd525fa763f55a87');
INSERT INTO `exp_security_hashes` VALUES(4640, 1366101213, '127.0.0.1', 'aa1739f8e33cd5b57b774c2a8db6a16517a1b9a1');
INSERT INTO `exp_security_hashes` VALUES(4641, 1366101214, '127.0.0.1', 'be6d3194c588a25896c5caa4ffc737f449b38c04');
INSERT INTO `exp_security_hashes` VALUES(4642, 1366101214, '127.0.0.1', '0e02af43f245cad1f98572ab67b368dce8a54eb8');
INSERT INTO `exp_security_hashes` VALUES(4643, 1366101215, '127.0.0.1', '6f790a850581d7dc630520f76b0a3e396141131e');
INSERT INTO `exp_security_hashes` VALUES(4644, 1366101217, '127.0.0.1', 'dfd4834a4f510c44e857819d5e44afee60042bd0');
INSERT INTO `exp_security_hashes` VALUES(4645, 1366101217, '127.0.0.1', '9798df832a7437f1a57df6d0120ec5619289600b');
INSERT INTO `exp_security_hashes` VALUES(4646, 1366101217, '127.0.0.1', '4b91c9ab4db328397a909c9525e7a7795df957fe');
INSERT INTO `exp_security_hashes` VALUES(4647, 1366101429, '127.0.0.1', '56e01e8e400546f60a13343a84ca551722e8a19f');
INSERT INTO `exp_security_hashes` VALUES(4648, 1366101430, '127.0.0.1', '54b7ebcfce0265f31c1b76bf035a2d176b50fd46');
INSERT INTO `exp_security_hashes` VALUES(4649, 1366101430, '127.0.0.1', '38a7992fc9fb615b5421a2bcff21a4031140c83c');
INSERT INTO `exp_security_hashes` VALUES(4650, 1366101505, '127.0.0.1', 'aa8c1453e60052680a994de788a6c8de4b5491a7');
INSERT INTO `exp_security_hashes` VALUES(4651, 1366101506, '127.0.0.1', '51f6257ad242c8a07bd96843b0daee1e24941a09');
INSERT INTO `exp_security_hashes` VALUES(4652, 1366101507, '127.0.0.1', '6a769a1bee737ba5691e8ec70bbddcebd99a77c3');
INSERT INTO `exp_security_hashes` VALUES(4653, 1366101509, '127.0.0.1', 'f07a4852094ef34552d83e36b484e75641b14dda');
INSERT INTO `exp_security_hashes` VALUES(4654, 1366101509, '127.0.0.1', '27047793c29f014f14840a671ceed4129ea2e702');
INSERT INTO `exp_security_hashes` VALUES(4655, 1366101510, '127.0.0.1', '01300d41778c15437c2410a3fa63ecbbd32ac5e7');
INSERT INTO `exp_security_hashes` VALUES(4656, 1366101551, '127.0.0.1', '1de7ea91b073f0061ca2cc9c6f2b36ae2ab82abd');
INSERT INTO `exp_security_hashes` VALUES(4657, 1366101559, '127.0.0.1', '1f8077fd68e963675852a33b7515673010e8997b');
INSERT INTO `exp_security_hashes` VALUES(4658, 1366101560, '127.0.0.1', '8442ec8b00e0f0897ce1f0e31608a24ebec03e9e');
INSERT INTO `exp_security_hashes` VALUES(4659, 1366101564, '127.0.0.1', 'bfc3e51a3ad80073ca4bb28fcc8ed1b73320884f');
INSERT INTO `exp_security_hashes` VALUES(4660, 1366101564, '127.0.0.1', '683df30f1826fe8790342913605216f7d22a37e7');
INSERT INTO `exp_security_hashes` VALUES(4661, 1366101565, '127.0.0.1', '524bb563ebff6b579c6ef1477b319f58c0587397');
INSERT INTO `exp_security_hashes` VALUES(4662, 1366101578, '127.0.0.1', 'cb19f7077ac0492252dd104d32348efc7079bfb2');
INSERT INTO `exp_security_hashes` VALUES(4663, 1366101578, '127.0.0.1', 'a8f8021a2df19bbc94b02a17b85528516a9e7b20');
INSERT INTO `exp_security_hashes` VALUES(4664, 1366101579, '127.0.0.1', '7e57b53ff5dffaab3e0ddf26db108df05c5b6a99');
INSERT INTO `exp_security_hashes` VALUES(4665, 1366101581, '127.0.0.1', '7712bd66c92e0f9aa8612cb20006350b1f63281c');
INSERT INTO `exp_security_hashes` VALUES(4666, 1366101581, '127.0.0.1', '4d3ce366044885e24bebaa558a356cfe368eb11b');
INSERT INTO `exp_security_hashes` VALUES(4667, 1366101582, '127.0.0.1', '8d1d7268fe0cbc1122dff0685663bbf18a33b921');
INSERT INTO `exp_security_hashes` VALUES(4668, 1366101585, '127.0.0.1', 'b121e7921aa241f4a3336274cde0cb78d09b2d9b');
INSERT INTO `exp_security_hashes` VALUES(4669, 1366101585, '127.0.0.1', '70af2eb2df504f45bf3f44c83b592f9b60cd21c6');
INSERT INTO `exp_security_hashes` VALUES(4670, 1366101586, '127.0.0.1', '91d1c5ad20fca0832d77b9687f74b83868eb8d18');
INSERT INTO `exp_security_hashes` VALUES(4671, 1366101588, '127.0.0.1', '34fdb953830de13d1895638aec0cf69f9ee40197');
INSERT INTO `exp_security_hashes` VALUES(4672, 1366101589, '127.0.0.1', 'fd2fd43688d47994eaa1749feb449cbe40ccc535');
INSERT INTO `exp_security_hashes` VALUES(4673, 1366101589, '127.0.0.1', '170f948641405afe6415894474c6243aab70f27d');
INSERT INTO `exp_security_hashes` VALUES(4674, 1366101627, '127.0.0.1', '5dfa293161e2c83ef6d9919a80526dd949e981e0');
INSERT INTO `exp_security_hashes` VALUES(4675, 1366101627, '127.0.0.1', 'e522eb8c93effe521646ef0e24a74ba93ac8e0d8');
INSERT INTO `exp_security_hashes` VALUES(4676, 1366101628, '127.0.0.1', '1caa8acc2ccf5ce81f2c81996d5acae1cd1a60c4');
INSERT INTO `exp_security_hashes` VALUES(4677, 1366101629, '127.0.0.1', '55c6f8fdcb6d6940048ea664b95e04967ebc9a17');
INSERT INTO `exp_security_hashes` VALUES(4678, 1366101629, '127.0.0.1', 'a8920a3c37597cfca08b984792e06dc44d8a8b87');
INSERT INTO `exp_security_hashes` VALUES(4679, 1366101630, '127.0.0.1', '705cb8991eb96349109ef06c46ad687032f7ca8d');
INSERT INTO `exp_security_hashes` VALUES(4680, 1366101631, '127.0.0.1', '1e8bb8c534562fa4876833a5d01a14d2d3cebed2');
INSERT INTO `exp_security_hashes` VALUES(4681, 1366101632, '127.0.0.1', '6b4ef202cb2ad8543fad3c62528f0542ffaa61e6');
INSERT INTO `exp_security_hashes` VALUES(4682, 1366101634, '127.0.0.1', '8d692726bf3d0ce6d274c864f46190fec4c527bb');
INSERT INTO `exp_security_hashes` VALUES(4683, 1366101781, '127.0.0.1', '0d227497e2c7486f4ca947f247bf0eb30d53f029');
INSERT INTO `exp_security_hashes` VALUES(4684, 1366101782, '127.0.0.1', '3a6a8cf73937a244cfe2c3cb47c34ebae6bba058');
INSERT INTO `exp_security_hashes` VALUES(4685, 1366101782, '127.0.0.1', '59e6dc11abef25e23b53872581f08d6ee34821f7');
INSERT INTO `exp_security_hashes` VALUES(4686, 1366101834, '127.0.0.1', '276029792df0006cb8442c54bc0fa9a275019a9c');
INSERT INTO `exp_security_hashes` VALUES(4687, 1366101834, '127.0.0.1', 'd025f77b5673408e37a69175f23e58a28b4ca5d9');
INSERT INTO `exp_security_hashes` VALUES(4688, 1366101835, '127.0.0.1', 'c6ac42c9e4f5e9dc9adea755a7cf9d22430652f1');
INSERT INTO `exp_security_hashes` VALUES(4689, 1366101840, '127.0.0.1', '4f5b741a3f0ecbb7fea5ee526fcd1d2d09f5673a');
INSERT INTO `exp_security_hashes` VALUES(4690, 1366101840, '127.0.0.1', 'd45483922b40f1eed6fac6bb42733a3d552f6171');
INSERT INTO `exp_security_hashes` VALUES(4691, 1366101841, '127.0.0.1', '0417a84e5a669ef3071fc326d0ef9fd9e91994d3');
INSERT INTO `exp_security_hashes` VALUES(4692, 1366101851, '127.0.0.1', '752f86d4043ea97a1cd53a57b8cb8ca9c48cbf55');
INSERT INTO `exp_security_hashes` VALUES(4693, 1366101852, '127.0.0.1', '111d9d943a9cda5345c03ddfcf8658e3277c2d01');
INSERT INTO `exp_security_hashes` VALUES(4694, 1366101852, '127.0.0.1', '076657ec882beb31872c93ffd660f6d64e7a0d49');
INSERT INTO `exp_security_hashes` VALUES(4695, 1366104206, '127.0.0.1', '19d4dce479fb7ba40844419434d700cb33679d13');
INSERT INTO `exp_security_hashes` VALUES(4696, 1366104206, '127.0.0.1', '062a4eb342322349f68c138823ad0091f5f414f6');
INSERT INTO `exp_security_hashes` VALUES(4697, 1366104207, '127.0.0.1', 'e6eec2290e5d7dac489f5e9e8feae1f0dca7d947');
INSERT INTO `exp_security_hashes` VALUES(4698, 1366104241, '127.0.0.1', '9cc491b201d67ea555daa26b5f8f5af8503fafe8');
INSERT INTO `exp_security_hashes` VALUES(4699, 1366104242, '127.0.0.1', '7799ebb205917390b01f797fc85fa4261f502607');
INSERT INTO `exp_security_hashes` VALUES(4700, 1366104242, '127.0.0.1', '0f5b73e3bd7bc313eb73923d4f2061f616ac4fc0');
INSERT INTO `exp_security_hashes` VALUES(4701, 1366104265, '127.0.0.1', '7876908d10b915c4523787ffdf6b2e45a540a32d');
INSERT INTO `exp_security_hashes` VALUES(4702, 1366104265, '127.0.0.1', '8279b058187bef8573437fbafd7be5d9f13b7995');
INSERT INTO `exp_security_hashes` VALUES(4703, 1366104266, '127.0.0.1', 'c135a283f9c54b0c7ab0653e6fc07fe5ea0cbea7');
INSERT INTO `exp_security_hashes` VALUES(4704, 1366104282, '127.0.0.1', 'ab44421c43da94fd726abdc1740ba5cdc11158d9');
INSERT INTO `exp_security_hashes` VALUES(4705, 1366104283, '127.0.0.1', 'a1c1adec2a57f1c7bb3b84d098698fe16f2cfa8a');
INSERT INTO `exp_security_hashes` VALUES(4706, 1366104285, '127.0.0.1', 'd00c040c7f84f064985e41bda285a6718bcb1fe4');
INSERT INTO `exp_security_hashes` VALUES(4707, 1366104308, '127.0.0.1', 'b6e2e91deae5e64d5dcabf47e99dbd11b2ff1d53');
INSERT INTO `exp_security_hashes` VALUES(4708, 1366104309, '127.0.0.1', '53e402707e44d931de184e018dfbc7df6456514a');
INSERT INTO `exp_security_hashes` VALUES(4709, 1366104309, '127.0.0.1', 'dd5bcde094a40e6b96dc5110e98e1277beadcf69');
INSERT INTO `exp_security_hashes` VALUES(4710, 1366104473, '127.0.0.1', 'abe4e575e9d3764fcf88a36454dff1f99c455bdd');
INSERT INTO `exp_security_hashes` VALUES(4711, 1366104473, '127.0.0.1', 'b809026a908f957b6a74e1ca27d3a42815e68a39');
INSERT INTO `exp_security_hashes` VALUES(4712, 1366104474, '127.0.0.1', 'bac0168c42cfebe86d0d48fc396e39ed5acf6354');
INSERT INTO `exp_security_hashes` VALUES(4713, 1366104498, '127.0.0.1', '781028bebc6d9c15ae3e638c7983036fe08c283f');
INSERT INTO `exp_security_hashes` VALUES(4714, 1366104498, '127.0.0.1', 'a6ecbc8eb91641763d5deac319fa0d20e0427310');
INSERT INTO `exp_security_hashes` VALUES(4715, 1366104499, '127.0.0.1', '324e635dd55ba4e17b47b74168e3e0e0324081e5');
INSERT INTO `exp_security_hashes` VALUES(4716, 1366104534, '127.0.0.1', 'c49689e8dc54f17d47312b1e10aa99bd6ee4994a');
INSERT INTO `exp_security_hashes` VALUES(4717, 1366104534, '127.0.0.1', 'eaebb8907f9a4bb2a4491606b542df0f02caaa3d');
INSERT INTO `exp_security_hashes` VALUES(4718, 1366104535, '127.0.0.1', 'ccd97624624411f8957cdd088a649aa6378ee897');
INSERT INTO `exp_security_hashes` VALUES(4719, 1366104542, '127.0.0.1', 'd205bb8b41d20f1e5716dd8977da4c1a2064272c');
INSERT INTO `exp_security_hashes` VALUES(4720, 1366104543, '127.0.0.1', '6b3180e6165abb6501ca0e5dc520e765b764a5e6');
INSERT INTO `exp_security_hashes` VALUES(4721, 1366104544, '127.0.0.1', '601d786b7a9b3eac344f7b3fa72568a990a62b5c');
INSERT INTO `exp_security_hashes` VALUES(4722, 1366104544, '127.0.0.1', '5853a174aa368d1d1dc1e02cb2493327ef92116f');
INSERT INTO `exp_security_hashes` VALUES(4723, 1366104545, '127.0.0.1', 'beb67c4d0975f24a75d8b946e95c4a0024962f3a');
INSERT INTO `exp_security_hashes` VALUES(4724, 1366104545, '127.0.0.1', '84ad26968ad413767071ffa02206c1a5288a7529');
INSERT INTO `exp_security_hashes` VALUES(4725, 1366104545, '127.0.0.1', 'db4faa7fd8aa084e208c2784f246e5c13a9e1893');
INSERT INTO `exp_security_hashes` VALUES(4726, 1366104545, '127.0.0.1', 'ec2bdebf522bec5ff265d81eb46ea65ca798695c');
INSERT INTO `exp_security_hashes` VALUES(4727, 1366104546, '127.0.0.1', '513dd4432597cb1d38004dad138f20e9fa60eba2');
INSERT INTO `exp_security_hashes` VALUES(4728, 1366104546, '127.0.0.1', '0883fad9d7ede23dcfb1f5e428770eb8bd9388d1');
INSERT INTO `exp_security_hashes` VALUES(4729, 1366104547, '127.0.0.1', 'cb736bb007b03ab92073d1dbeac0c7f49ac38cbb');
INSERT INTO `exp_security_hashes` VALUES(4730, 1366104549, '127.0.0.1', 'd8d947de150857d2742b5f155875d47be6334fb9');
INSERT INTO `exp_security_hashes` VALUES(4731, 1366104549, '127.0.0.1', '848aa6c1f48472df28d57c36ac3727f3270e8db1');
INSERT INTO `exp_security_hashes` VALUES(4732, 1366104550, '127.0.0.1', '8e7dc7522cfccc705870678c3136dcb6832ba636');
INSERT INTO `exp_security_hashes` VALUES(4733, 1366104552, '127.0.0.1', '2def90509627f4f471d044814df4056aa5d902d2');
INSERT INTO `exp_security_hashes` VALUES(4734, 1366104552, '127.0.0.1', 'd0b88a3f26282bd073bf4c237ae2dc1333ec5860');
INSERT INTO `exp_security_hashes` VALUES(4735, 1366104553, '127.0.0.1', 'fa21da3c0d9cb00b00d85b8fc6c18d8ff20885c3');
INSERT INTO `exp_security_hashes` VALUES(4736, 1366104568, '127.0.0.1', '05d9034276cda4424b32ffa14267bcf4037dd6ab');
INSERT INTO `exp_security_hashes` VALUES(4737, 1366104568, '127.0.0.1', '951a11e040f3dcc6e205d0d8926173537e1c2f0a');
INSERT INTO `exp_security_hashes` VALUES(4738, 1366104568, '127.0.0.1', '09e5935e4d653c34aba1d5be2f221195fcf8f3d1');
INSERT INTO `exp_security_hashes` VALUES(4739, 1366104568, '127.0.0.1', '97c93cfa90090935ee5a946865773cabcd70c608');
INSERT INTO `exp_security_hashes` VALUES(4740, 1366104568, '127.0.0.1', '70d381e7fc22a27bdb8bcd5c022cca6437211629');
INSERT INTO `exp_security_hashes` VALUES(4741, 1366104569, '127.0.0.1', 'fd3009df596dee6c718f1261bc40e85b8bf1adcc');
INSERT INTO `exp_security_hashes` VALUES(4742, 1366104569, '127.0.0.1', 'd9cf22c8a4901d1823e4f9fb24100bdb6e60abb9');
INSERT INTO `exp_security_hashes` VALUES(4743, 1366104570, '127.0.0.1', '52e67eb53c296f149fcec2d19621d0c6d6da1f43');
INSERT INTO `exp_security_hashes` VALUES(4744, 1366104570, '127.0.0.1', 'a4d916381cbf2216a6b632bc640184a51c5b1250');
INSERT INTO `exp_security_hashes` VALUES(4745, 1366104571, '127.0.0.1', '5a5ba12139482a5a106723723f822d1ef3ed0959');
INSERT INTO `exp_security_hashes` VALUES(4746, 1366104571, '127.0.0.1', '1d8ffb22bb0dae7ab3e838ad4608bf94a1cf2b1b');
INSERT INTO `exp_security_hashes` VALUES(4747, 1366104572, '127.0.0.1', '56c773d830c92996faee5c1b9f6100605b0e122b');
INSERT INTO `exp_security_hashes` VALUES(4748, 1366104573, '127.0.0.1', '556efd56e16161a90eef43830d0c4aa33295c8a4');
INSERT INTO `exp_security_hashes` VALUES(4749, 1366104574, '127.0.0.1', '27d83051a75d29e6959ff9d1b3cf94a232bd9a0a');
INSERT INTO `exp_security_hashes` VALUES(4750, 1366104574, '127.0.0.1', '3744069729fcd2c5b75934b5310a44321d7a9236');
INSERT INTO `exp_security_hashes` VALUES(4751, 1366104590, '127.0.0.1', '959d71d69145432415efa29d4dc5a644051631d7');
INSERT INTO `exp_security_hashes` VALUES(4752, 1366104591, '127.0.0.1', 'eb5611f37c89ed8cf24ee72a10efb633f8c74fba');
INSERT INTO `exp_security_hashes` VALUES(4753, 1366104591, '127.0.0.1', 'a3b07b143e9d52be778e4c94c7561bc344828eae');
INSERT INTO `exp_security_hashes` VALUES(4754, 1366104604, '127.0.0.1', '56174d345f89caee506481c3e66980a059863bdf');
INSERT INTO `exp_security_hashes` VALUES(4755, 1366104605, '127.0.0.1', '4d07b7e6e0c7ed8440973fc022bcaef802573dab');
INSERT INTO `exp_security_hashes` VALUES(4756, 1366104605, '127.0.0.1', '1205751bdfaa8cb33d96359dbf2bc827c9f5828e');
INSERT INTO `exp_security_hashes` VALUES(4757, 1366104608, '127.0.0.1', 'd8d62df6c7ed8c1c1680bb428cc4955d92eb321e');
INSERT INTO `exp_security_hashes` VALUES(4758, 1366104608, '127.0.0.1', 'c917fdb38648475893c5e58cfbdf597eba706f72');
INSERT INTO `exp_security_hashes` VALUES(4759, 1366104609, '127.0.0.1', 'fbf533d1b859919405881d2d35499122112ca76f');
INSERT INTO `exp_security_hashes` VALUES(4760, 1366104609, '127.0.0.1', '1cfcdbe39f77bb3c8258f80516b85418e94e3131');
INSERT INTO `exp_security_hashes` VALUES(4761, 1366104610, '127.0.0.1', 'afb140735276b4bab3ef55f1cdce557514b93198');
INSERT INTO `exp_security_hashes` VALUES(4762, 1366104610, '127.0.0.1', '8abfaaa75a9fe6f386b65c6ad33a003f25547969');
INSERT INTO `exp_security_hashes` VALUES(4763, 1366104729, '127.0.0.1', '64fc4ef6f045a4e3d2756e5716e7c3917bd21f04');
INSERT INTO `exp_security_hashes` VALUES(4764, 1366104730, '127.0.0.1', 'b640d41c497aaaf54af3ca90c592a9fa7e38d9c8');
INSERT INTO `exp_security_hashes` VALUES(4765, 1366104730, '127.0.0.1', '28cc357e5346076c26c4fa9748b0e1365ab07571');
INSERT INTO `exp_security_hashes` VALUES(4766, 1366104731, '127.0.0.1', '383ed70b831cb077275714eb2bee4f2441b4120c');
INSERT INTO `exp_security_hashes` VALUES(4767, 1366104969, '127.0.0.1', '968a37b07288f012ca968d90daebd030fc4a244c');
INSERT INTO `exp_security_hashes` VALUES(4768, 1366104970, '127.0.0.1', '691cf8db41f0d1eb07490a080c2df64391509114');
INSERT INTO `exp_security_hashes` VALUES(4769, 1366104973, '127.0.0.1', '266bef08316edd17f0c832bd4260b929cdce5348');
INSERT INTO `exp_security_hashes` VALUES(4770, 1366104975, '127.0.0.1', 'e9a73d3a42762ef7be6533c30a7aca9dce9cc6ed');
INSERT INTO `exp_security_hashes` VALUES(4771, 1366104975, '127.0.0.1', '8d76a1adc0d8d1ab8ff7bf14547662d653c91521');
INSERT INTO `exp_security_hashes` VALUES(4772, 1366104983, '127.0.0.1', 'f5c13fa116739feb92b99b9570e352c3cd62115b');
INSERT INTO `exp_security_hashes` VALUES(4773, 1366104983, '127.0.0.1', '12bb9411f675f6300c9b81247e58491440124895');
INSERT INTO `exp_security_hashes` VALUES(4774, 1366104990, '127.0.0.1', '805de04b94dbd31feaced7889ace5965f1f394ec');
INSERT INTO `exp_security_hashes` VALUES(4775, 1366104991, '127.0.0.1', 'f41097006feb669a2663ebac6f9f1607e617a501');
INSERT INTO `exp_security_hashes` VALUES(4776, 1366104991, '127.0.0.1', '847f83d47c472c175c5acbb7a2ce44de011ff390');
INSERT INTO `exp_security_hashes` VALUES(4777, 1366104992, '127.0.0.1', 'b320734be9593ceda2358900a045fd7d1f9c148a');
INSERT INTO `exp_security_hashes` VALUES(4778, 1366105206, '127.0.0.1', '8c5d355075be49ec98be0b8b0e8b1bed358ecc4e');
INSERT INTO `exp_security_hashes` VALUES(4779, 1366105206, '127.0.0.1', 'ece14bdb7eeb5d89f743014bc6c8e9555cbea25f');
INSERT INTO `exp_security_hashes` VALUES(4780, 1366105206, '127.0.0.1', '2aa1d72c25bb97888dae8100d9dc654ce35f5b09');
INSERT INTO `exp_security_hashes` VALUES(4781, 1366105207, '127.0.0.1', '24906417bfa2bdf4315f83fcec7b1f26681186b0');
INSERT INTO `exp_security_hashes` VALUES(4782, 1366105221, '127.0.0.1', '5c401a07633a88bce3c5f8af47b753045ef41b02');
INSERT INTO `exp_security_hashes` VALUES(4783, 1366105221, '127.0.0.1', '691b22ce67386b21b846b6ec4650d4093d5d2126');
INSERT INTO `exp_security_hashes` VALUES(4784, 1366105222, '127.0.0.1', '5bfdcc61182010f7fa8c85c5f175c09a5a0d449b');
INSERT INTO `exp_security_hashes` VALUES(4785, 1366105222, '127.0.0.1', '75c3bfc4a0581e8c7c6dcd78b62a920327c773d4');
INSERT INTO `exp_security_hashes` VALUES(4786, 1366105239, '127.0.0.1', '00c33785482f668b8647d8204aaf94c0cf85ee9d');
INSERT INTO `exp_security_hashes` VALUES(4787, 1366105240, '127.0.0.1', '2a0a3e6fd585e9422ac0a492123940e8b9466cac');
INSERT INTO `exp_security_hashes` VALUES(4788, 1366105240, '127.0.0.1', '4c5c172a1c1a19e9c83d440f4c3ed6abda209cc1');
INSERT INTO `exp_security_hashes` VALUES(4789, 1366105251, '127.0.0.1', '2655064b029a1a7ddfeaad22f2a7abb2be3eb511');
INSERT INTO `exp_security_hashes` VALUES(4790, 1366105326, '127.0.0.1', 'c5b1564cda02645aa33ad56bfd1627eadb6ed053');
INSERT INTO `exp_security_hashes` VALUES(4791, 1366105326, '127.0.0.1', '825e41b49f08312aa1eff8c51ab4291069e1195f');
INSERT INTO `exp_security_hashes` VALUES(4792, 1366105327, '127.0.0.1', 'd137d5de2382a128ae763c71c06f0240d67c56d0');
INSERT INTO `exp_security_hashes` VALUES(4793, 1366105397, '127.0.0.1', '837b2cfb2538b3234faa88396b4a24515feaac26');
INSERT INTO `exp_security_hashes` VALUES(4794, 1366105397, '127.0.0.1', 'aacf317427fc105053621eae9f2df4caf8ee3bf6');
INSERT INTO `exp_security_hashes` VALUES(4795, 1366105398, '127.0.0.1', '16a379bfe27ac5d0fb88b8b78e76793aa5df96e6');
INSERT INTO `exp_security_hashes` VALUES(4796, 1366105521, '127.0.0.1', '5e572477aac178ac9d427b796e54a02f62e2d4cd');
INSERT INTO `exp_security_hashes` VALUES(4797, 1366105528, '127.0.0.1', '90b07a0b129f4319c3f1af7992c1e8fb7fc00d9d');
INSERT INTO `exp_security_hashes` VALUES(4798, 1366105529, '127.0.0.1', '97110125f182a00e1ba6482a2ac6be6d8190e50b');
INSERT INTO `exp_security_hashes` VALUES(4799, 1366105529, '127.0.0.1', '122a684f5fe09266f143924080ebb9e2b68f8768');
INSERT INTO `exp_security_hashes` VALUES(4800, 1366105531, '127.0.0.1', '20dac39d18f1e5210737fc92cdf97eba462c0935');
INSERT INTO `exp_security_hashes` VALUES(4801, 1366105532, '127.0.0.1', 'beed73c77a66211bb47a3538f73f3f3a213a135f');
INSERT INTO `exp_security_hashes` VALUES(4802, 1366105532, '127.0.0.1', '3c28d70352bad3ae7588c45c969eebb42239aaac');
INSERT INTO `exp_security_hashes` VALUES(4803, 1366105538, '127.0.0.1', '6299a81fa1fef3b49a54ba221a92b1c3a3459fe7');
INSERT INTO `exp_security_hashes` VALUES(4804, 1366105538, '127.0.0.1', '1b5a5cb581c31741d306f8d5fb5207eb83b2f3f3');
INSERT INTO `exp_security_hashes` VALUES(4805, 1366105538, '127.0.0.1', '551ac766db4a26473d8cf1cd8052ac11abfde490');
INSERT INTO `exp_security_hashes` VALUES(4806, 1366105539, '127.0.0.1', '460bcbd135968cebf1d2f723470a933d0aa63d5f');
INSERT INTO `exp_security_hashes` VALUES(4807, 1366105539, '127.0.0.1', 'b585b981f788faa7fc2abbfac27fc4e52a041f43');
INSERT INTO `exp_security_hashes` VALUES(4808, 1366105539, '127.0.0.1', 'b7681e86f0d398b4775255813c61790d68a8061f');
INSERT INTO `exp_security_hashes` VALUES(4809, 1366105539, '127.0.0.1', '0fb1371bfe50b79d7fcd0bc2a511f9b078d812e4');
INSERT INTO `exp_security_hashes` VALUES(4810, 1366105540, '127.0.0.1', 'c6673e1b8a3cad29df66632ac8307f5a5421ae21');
INSERT INTO `exp_security_hashes` VALUES(4811, 1366105540, '127.0.0.1', '1587d1f6b174a72a95b54728c42c13b9bf984ba2');
INSERT INTO `exp_security_hashes` VALUES(4812, 1366105548, '127.0.0.1', '16554edba24ed7380c23d396f12fdac939e3eb93');
INSERT INTO `exp_security_hashes` VALUES(4813, 1366105549, '127.0.0.1', '68b1fed3bd14853b74d39726a7ead1a84517fee8');
INSERT INTO `exp_security_hashes` VALUES(4814, 1366105550, '127.0.0.1', '3a13b9da21598e7b61c7e7d4f1cabb8e0a79e2ea');
INSERT INTO `exp_security_hashes` VALUES(4815, 1366105568, '127.0.0.1', 'be134bb9ec8a55f9944f94958d9aeca43277515c');
INSERT INTO `exp_security_hashes` VALUES(4816, 1366105568, '127.0.0.1', '418621f68f7cad97ee84abe648ca152b3748eac1');
INSERT INTO `exp_security_hashes` VALUES(4817, 1366105569, '127.0.0.1', '5c7234463eb11ded331040077d093f939be14a1d');
INSERT INTO `exp_security_hashes` VALUES(4818, 1366105576, '127.0.0.1', '88ff29a57ee45adf82afe1bb0c8564265c8fb18b');
INSERT INTO `exp_security_hashes` VALUES(4819, 1366105577, '127.0.0.1', 'add3cbed6e48aa1c92368cfb60ebca5737283fbf');
INSERT INTO `exp_security_hashes` VALUES(4820, 1366105577, '127.0.0.1', '389c1210768998ffd8b6063ab310869574ccf2dd');
INSERT INTO `exp_security_hashes` VALUES(4821, 1366105669, '127.0.0.1', '2e9792c76ee620844a38366049c9ee10c3edba76');
INSERT INTO `exp_security_hashes` VALUES(4822, 1366105669, '127.0.0.1', '2b1cbadeff300997f0d97c83d36edcd94ec522d7');
INSERT INTO `exp_security_hashes` VALUES(4823, 1366105670, '127.0.0.1', '2a2fe536315fcc9a590f0381d3311a1877678339');
INSERT INTO `exp_security_hashes` VALUES(4824, 1366105677, '127.0.0.1', '19e45cf9968eae9f231518b3146a991113dd9678');
INSERT INTO `exp_security_hashes` VALUES(4825, 1366105678, '127.0.0.1', 'f62a758f6097e1536829416b8b736be9b24e977b');
INSERT INTO `exp_security_hashes` VALUES(4826, 1366105678, '127.0.0.1', '6df8f2394f1d534f5e42b3ff367136efb5cc3c2c');
INSERT INTO `exp_security_hashes` VALUES(4827, 1366105678, '127.0.0.1', '0db01ce7d1ab9ea6b53b890b8fb0ba2e0ef6a3d5');
INSERT INTO `exp_security_hashes` VALUES(4828, 1366105678, '127.0.0.1', '478bc65b7e5fac52fab0b69b650efe1117595647');
INSERT INTO `exp_security_hashes` VALUES(4829, 1366105678, '127.0.0.1', 'fa465f67795fa68c931e1fd1b445eba2cd473dcf');
INSERT INTO `exp_security_hashes` VALUES(4830, 1366105679, '127.0.0.1', '95a3516dbd722f1f9adbb6399bfd206d10d8d085');
INSERT INTO `exp_security_hashes` VALUES(4831, 1366105679, '127.0.0.1', '9f560741868407f070c3a1b0496731bd4222255e');
INSERT INTO `exp_security_hashes` VALUES(4832, 1366105680, '127.0.0.1', 'c83be1a59b1e6864d40a8824caa498dd63368ffa');
INSERT INTO `exp_security_hashes` VALUES(4833, 1366105680, '127.0.0.1', '6efa05be3eaad904e2767fcf595a0c4ab780f05f');
INSERT INTO `exp_security_hashes` VALUES(4834, 1366105680, '127.0.0.1', '9ec10d0d3af49927c804719a29c6343cc6d6798a');
INSERT INTO `exp_security_hashes` VALUES(4835, 1366105681, '127.0.0.1', '31c39ba4aff96aa2ed20810ac1ab7b4716bbe90c');
INSERT INTO `exp_security_hashes` VALUES(4836, 1366105682, '127.0.0.1', 'a69b0b8b563c1f6a5c77c0e1866e730dc5915abb');
INSERT INTO `exp_security_hashes` VALUES(4837, 1366105685, '127.0.0.1', 'f35726fa2fa50656f8c437347393a7389985a362');
INSERT INTO `exp_security_hashes` VALUES(4838, 1366105686, '127.0.0.1', 'c661ca1845505e686eb7377b2dd5bc6617d54af3');
INSERT INTO `exp_security_hashes` VALUES(4839, 1366105686, '127.0.0.1', '32b897263c3676c8b143bdc4131971706fea636d');
INSERT INTO `exp_security_hashes` VALUES(4840, 1366105689, '127.0.0.1', 'fdfedfce126cf54c79d06ca324d5c9c1a4b060dc');
INSERT INTO `exp_security_hashes` VALUES(4841, 1366105690, '127.0.0.1', 'f9db52a2bcb9b2db964e5f6e8227c6b5d54cfa0d');
INSERT INTO `exp_security_hashes` VALUES(4842, 1366105690, '127.0.0.1', 'ae37ce3fb641686ca5eb2efdb2c0f4773fe36c53');
INSERT INTO `exp_security_hashes` VALUES(4843, 1366105716, '127.0.0.1', 'f4d7f0e3acb3d3ae70651fe1654a7269464b0826');
INSERT INTO `exp_security_hashes` VALUES(4844, 1366105716, '127.0.0.1', '7d90de7abdb8ccb6b485f3148401c6c9af63f0e9');
INSERT INTO `exp_security_hashes` VALUES(4845, 1366105717, '127.0.0.1', '1a19a3a03904eded9d3b2df25aaa9cd50e0b9ed9');
INSERT INTO `exp_security_hashes` VALUES(4846, 1366105717, '127.0.0.1', '14f77928517366e799b6114d39241e6fa1a6115b');
INSERT INTO `exp_security_hashes` VALUES(4847, 1366105718, '127.0.0.1', '9687223db0da161884a4ef7fbe33b541e1bb2eca');
INSERT INTO `exp_security_hashes` VALUES(4848, 1366105718, '127.0.0.1', 'b3da071d5d232c4013bb70f8807bc77dc1eb6671');
INSERT INTO `exp_security_hashes` VALUES(4849, 1366105734, '127.0.0.1', '1c44f811a538bd8dd77176558a22c56706489776');
INSERT INTO `exp_security_hashes` VALUES(4850, 1366105734, '127.0.0.1', 'a3d696e080e59f1fee7b13919a60d190e8038fe4');
INSERT INTO `exp_security_hashes` VALUES(4851, 1366105735, '127.0.0.1', '3f62455d4d27be25fa30f7f8bb6dbb021ad192e5');
INSERT INTO `exp_security_hashes` VALUES(4852, 1366105739, '127.0.0.1', '466d2c62a2e214fe10a6d77f6b34aa53fbe7708b');
INSERT INTO `exp_security_hashes` VALUES(4853, 1366105740, '127.0.0.1', '3c2053e6758de58c27654d786d657dfc84655eb6');
INSERT INTO `exp_security_hashes` VALUES(4854, 1366105740, '127.0.0.1', 'be93bec0e9264d586ad3697be5353e16d9afd844');
INSERT INTO `exp_security_hashes` VALUES(4855, 1366105743, '127.0.0.1', '4b9ace2f68caa2ad7de82da34eefe14d021c1278');
INSERT INTO `exp_security_hashes` VALUES(4856, 1366105743, '127.0.0.1', 'c52125cf5017879b610904805e174ae8275f464e');
INSERT INTO `exp_security_hashes` VALUES(4857, 1366105744, '127.0.0.1', '77e63a47197ae2d8d2c1917aa30cd34a4255c6c7');
INSERT INTO `exp_security_hashes` VALUES(4858, 1366105745, '127.0.0.1', '3983547a8f872d66326d7268ad68a75e9d3cf0da');
INSERT INTO `exp_security_hashes` VALUES(4859, 1366105746, '127.0.0.1', '3f531181a93eb9e6b6417a82ef0e632ac3162506');
INSERT INTO `exp_security_hashes` VALUES(4860, 1366105746, '127.0.0.1', '1fae1a2b90589c057a4fe22365ab128e447b89e7');
INSERT INTO `exp_security_hashes` VALUES(4861, 1366105746, '127.0.0.1', '210682e495ab736a6a78b98d044c1461183d89d1');
INSERT INTO `exp_security_hashes` VALUES(4862, 1366105746, '127.0.0.1', 'c587075ef7574cfb8e01bc6e0738a61637623ba2');
INSERT INTO `exp_security_hashes` VALUES(4863, 1366105747, '127.0.0.1', 'eef97ef3cb0f64482a94c519c2af4c0f608344a4');
INSERT INTO `exp_security_hashes` VALUES(4864, 1366105783, '127.0.0.1', '5883b614821718534a0928b1c66bfd8c8cbc45d5');
INSERT INTO `exp_security_hashes` VALUES(4865, 1366105783, '127.0.0.1', '52e525085810a63f87d440777b35beba37dbc85a');
INSERT INTO `exp_security_hashes` VALUES(4866, 1366105783, '127.0.0.1', '8b2d2c00c85547df8ad41e5121711295841620ab');
INSERT INTO `exp_security_hashes` VALUES(4867, 1366105784, '127.0.0.1', '9b99e2fa75d2ad8cc6b283e2ab05e7f635d43826');

/* Table structure for table `exp_sessions` */
DROP TABLE IF EXISTS `exp_sessions`;

CREATE TABLE `exp_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `admin_sess` tinyint(1) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  KEY `member_id` (`member_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_sessions` */
INSERT INTO `exp_sessions` VALUES('26968678a5c8fe6ee338cfec361dfed07a7b1e50', 1, 1, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/26.0.1410.65 Safari/537.31', 1366105783);

/* Table structure for table `exp_sites` */
DROP TABLE IF EXISTS `exp_sites`;

CREATE TABLE `exp_sites` (
  `site_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `site_label` varchar(100) NOT NULL DEFAULT '',
  `site_name` varchar(50) NOT NULL DEFAULT '',
  `site_description` text,
  `site_system_preferences` mediumtext NOT NULL,
  `site_mailinglist_preferences` text NOT NULL,
  `site_member_preferences` text NOT NULL,
  `site_template_preferences` text NOT NULL,
  `site_channel_preferences` text NOT NULL,
  `site_bootstrap_checksums` text NOT NULL,
  PRIMARY KEY (`site_id`),
  KEY `site_name` (`site_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_sites` */
INSERT INTO `exp_sites` VALUES(1, 'Synergy Positioning Systems (NZ)', 'synergy_nz', '', 'YTo5Mjp7czoxMDoic2l0ZV9pbmRleCI7czowOiIiO3M6ODoic2l0ZV91cmwiO3M6MjE6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsLyI7czoxNjoidGhlbWVfZm9sZGVyX3VybCI7czoyODoiaHR0cDovL3N5bmVyZ3kubG9jYWwvdGhlbWVzLyI7czoxNToid2VibWFzdGVyX2VtYWlsIjtzOjE5OiJqYW1lc0A5NmJsYWNrLmNvLm56IjtzOjE0OiJ3ZWJtYXN0ZXJfbmFtZSI7czowOiIiO3M6MjA6ImNoYW5uZWxfbm9tZW5jbGF0dXJlIjtzOjc6ImNoYW5uZWwiO3M6MTA6Im1heF9jYWNoZXMiO3M6MzoiMTUwIjtzOjExOiJjYXB0Y2hhX3VybCI7czozNzoiaHR0cDovL3N5bmVyZ3kubG9jYWwvaW1hZ2VzL2NhcHRjaGFzLyI7czoxMjoiY2FwdGNoYV9wYXRoIjtzOjU0OiIvVXNlcnMvamFtZXMvUHJvamVjdHMvOTZibGFjay9TeW5lcmd5L2ltYWdlcy9jYXB0Y2hhcy8iO3M6MTI6ImNhcHRjaGFfZm9udCI7czoxOiJ5IjtzOjEyOiJjYXB0Y2hhX3JhbmQiO3M6MToieSI7czoyMzoiY2FwdGNoYV9yZXF1aXJlX21lbWJlcnMiO3M6MToibiI7czoxNzoiZW5hYmxlX2RiX2NhY2hpbmciO3M6MToibiI7czoxODoiZW5hYmxlX3NxbF9jYWNoaW5nIjtzOjE6Im4iO3M6MTg6ImZvcmNlX3F1ZXJ5X3N0cmluZyI7czoxOiJuIjtzOjEzOiJzaG93X3Byb2ZpbGVyIjtzOjE6Im4iO3M6MTg6InRlbXBsYXRlX2RlYnVnZ2luZyI7czoxOiJuIjtzOjE1OiJpbmNsdWRlX3NlY29uZHMiO3M6MToibiI7czoxMzoiY29va2llX2RvbWFpbiI7czowOiIiO3M6MTE6ImNvb2tpZV9wYXRoIjtzOjA6IiI7czoxNzoidXNlcl9zZXNzaW9uX3R5cGUiO3M6MToiYyI7czoxODoiYWRtaW5fc2Vzc2lvbl90eXBlIjtzOjI6ImNzIjtzOjIxOiJhbGxvd191c2VybmFtZV9jaGFuZ2UiO3M6MToieSI7czoxODoiYWxsb3dfbXVsdGlfbG9naW5zIjtzOjE6InkiO3M6MTY6InBhc3N3b3JkX2xvY2tvdXQiO3M6MToieSI7czoyNToicGFzc3dvcmRfbG9ja291dF9pbnRlcnZhbCI7czoxOiIxIjtzOjIwOiJyZXF1aXJlX2lwX2Zvcl9sb2dpbiI7czoxOiJ5IjtzOjIyOiJyZXF1aXJlX2lwX2Zvcl9wb3N0aW5nIjtzOjE6InkiO3M6MjQ6InJlcXVpcmVfc2VjdXJlX3Bhc3N3b3JkcyI7czoxOiJuIjtzOjE5OiJhbGxvd19kaWN0aW9uYXJ5X3B3IjtzOjE6InkiO3M6MjM6Im5hbWVfb2ZfZGljdGlvbmFyeV9maWxlIjtzOjA6IiI7czoxNzoieHNzX2NsZWFuX3VwbG9hZHMiO3M6MToieSI7czoxNToicmVkaXJlY3RfbWV0aG9kIjtzOjg6InJlZGlyZWN0IjtzOjk6ImRlZnRfbGFuZyI7czo3OiJlbmdsaXNoIjtzOjg6InhtbF9sYW5nIjtzOjI6ImVuIjtzOjEyOiJzZW5kX2hlYWRlcnMiO3M6MToieSI7czoxMToiZ3ppcF9vdXRwdXQiO3M6MToibiI7czoxMzoibG9nX3JlZmVycmVycyI7czoxOiJuIjtzOjEzOiJtYXhfcmVmZXJyZXJzIjtzOjM6IjUwMCI7czoxMToidGltZV9mb3JtYXQiO3M6MjoidXMiO3M6MTU6InNlcnZlcl90aW1lem9uZSI7czo0OiJVUDEyIjtzOjEzOiJzZXJ2ZXJfb2Zmc2V0IjtzOjA6IiI7czoxNjoiZGF5bGlnaHRfc2F2aW5ncyI7czoxOiJ5IjtzOjIxOiJkZWZhdWx0X3NpdGVfdGltZXpvbmUiO3M6NDoiVVAxMiI7czoxNjoiZGVmYXVsdF9zaXRlX2RzdCI7czoxOiJ5IjtzOjE1OiJob25vcl9lbnRyeV9kc3QiO3M6MToieSI7czoxMzoibWFpbF9wcm90b2NvbCI7czo0OiJtYWlsIjtzOjExOiJzbXRwX3NlcnZlciI7czowOiIiO3M6MTM6InNtdHBfdXNlcm5hbWUiO3M6MDoiIjtzOjEzOiJzbXRwX3Bhc3N3b3JkIjtzOjA6IiI7czoxMToiZW1haWxfZGVidWciO3M6MToibiI7czoxMzoiZW1haWxfY2hhcnNldCI7czo1OiJ1dGYtOCI7czoxNToiZW1haWxfYmF0Y2htb2RlIjtzOjE6Im4iO3M6MTY6ImVtYWlsX2JhdGNoX3NpemUiO3M6MDoiIjtzOjExOiJtYWlsX2Zvcm1hdCI7czo1OiJwbGFpbiI7czo5OiJ3b3JkX3dyYXAiO3M6MToieSI7czoyMjoiZW1haWxfY29uc29sZV90aW1lbG9jayI7czoxOiI1IjtzOjIyOiJsb2dfZW1haWxfY29uc29sZV9tc2dzIjtzOjE6InkiO3M6ODoiY3BfdGhlbWUiO3M6NzoiZGVmYXVsdCI7czoyMToiZW1haWxfbW9kdWxlX2NhcHRjaGFzIjtzOjE6Im4iO3M6MTY6ImxvZ19zZWFyY2hfdGVybXMiO3M6MToieSI7czoxMjoic2VjdXJlX2Zvcm1zIjtzOjE6InkiO3M6MTk6ImRlbnlfZHVwbGljYXRlX2RhdGEiO3M6MToieSI7czoyNDoicmVkaXJlY3Rfc3VibWl0dGVkX2xpbmtzIjtzOjE6Im4iO3M6MTY6ImVuYWJsZV9jZW5zb3JpbmciO3M6MToibiI7czoxNDoiY2Vuc29yZWRfd29yZHMiO3M6MDoiIjtzOjE4OiJjZW5zb3JfcmVwbGFjZW1lbnQiO3M6MDoiIjtzOjEwOiJiYW5uZWRfaXBzIjtzOjA6IiI7czoxMzoiYmFubmVkX2VtYWlscyI7czowOiIiO3M6MTY6ImJhbm5lZF91c2VybmFtZXMiO3M6MDoiIjtzOjE5OiJiYW5uZWRfc2NyZWVuX25hbWVzIjtzOjA6IiI7czoxMDoiYmFuX2FjdGlvbiI7czo4OiJyZXN0cmljdCI7czoxMToiYmFuX21lc3NhZ2UiO3M6MzQ6IlRoaXMgc2l0ZSBpcyBjdXJyZW50bHkgdW5hdmFpbGFibGUiO3M6MTU6ImJhbl9kZXN0aW5hdGlvbiI7czoyMToiaHR0cDovL3d3dy55YWhvby5jb20vIjtzOjE2OiJlbmFibGVfZW1vdGljb25zIjtzOjE6InkiO3M6MTI6ImVtb3RpY29uX3VybCI7czozNjoiaHR0cDovL3N5bmVyZ3kubG9jYWwvaW1hZ2VzL3NtaWxleXMvIjtzOjE5OiJyZWNvdW50X2JhdGNoX3RvdGFsIjtzOjQ6IjEwMDAiO3M6MTc6Im5ld192ZXJzaW9uX2NoZWNrIjtzOjE6InkiO3M6MTc6ImVuYWJsZV90aHJvdHRsaW5nIjtzOjE6Im4iO3M6MTc6ImJhbmlzaF9tYXNrZWRfaXBzIjtzOjE6InkiO3M6MTQ6Im1heF9wYWdlX2xvYWRzIjtzOjI6IjEwIjtzOjEzOiJ0aW1lX2ludGVydmFsIjtzOjE6IjgiO3M6MTI6ImxvY2tvdXRfdGltZSI7czoyOiIzMCI7czoxNToiYmFuaXNobWVudF90eXBlIjtzOjc6Im1lc3NhZ2UiO3M6MTQ6ImJhbmlzaG1lbnRfdXJsIjtzOjA6IiI7czoxODoiYmFuaXNobWVudF9tZXNzYWdlIjtzOjUwOiJZb3UgaGF2ZSBleGNlZWRlZCB0aGUgYWxsb3dlZCBwYWdlIGxvYWQgZnJlcXVlbmN5LiI7czoxNzoiZW5hYmxlX3NlYXJjaF9sb2ciO3M6MToieSI7czoxOToibWF4X2xvZ2dlZF9zZWFyY2hlcyI7czozOiI1MDAiO3M6MTc6InRoZW1lX2ZvbGRlcl9wYXRoIjtzOjQ1OiIvVXNlcnMvamFtZXMvUHJvamVjdHMvOTZibGFjay9TeW5lcmd5L3RoZW1lcy8iO3M6MTA6ImlzX3NpdGVfb24iO3M6MToieSI7czoxMToicnRlX2VuYWJsZWQiO3M6MToieSI7czoyMjoicnRlX2RlZmF1bHRfdG9vbHNldF9pZCI7czoxOiIxIjt9', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6MzY6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9hdmF0YXJzLyI7czoxMToiYXZhdGFyX3BhdGgiO3M6NTM6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL2F2YXRhcnMvIjtzOjE2OiJhdmF0YXJfbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNzoiYXZhdGFyX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEzOiJhdmF0YXJfbWF4X2tiIjtzOjI6IjUwIjtzOjEzOiJlbmFibGVfcGhvdG9zIjtzOjE6Im4iO3M6OToicGhvdG9fdXJsIjtzOjQyOiJodHRwOi8vc3luZXJneS5sb2NhbC9pbWFnZXMvbWVtYmVyX3Bob3Rvcy8iO3M6MTA6InBob3RvX3BhdGgiO3M6NTk6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjE1OiJwaG90b19tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE2OiJwaG90b19tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMjoicGhvdG9fbWF4X2tiIjtzOjI6IjUwIjtzOjE2OiJhbGxvd19zaWduYXR1cmVzIjtzOjE6InkiO3M6MTM6InNpZ19tYXhsZW5ndGgiO3M6MzoiNTAwIjtzOjIxOiJzaWdfYWxsb3dfaW1nX2hvdGxpbmsiO3M6MToibiI7czoyMDoic2lnX2FsbG93X2ltZ191cGxvYWQiO3M6MToibiI7czoxMToic2lnX2ltZ191cmwiO3M6NTA6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9zaWduYXR1cmVfYXR0YWNobWVudHMvIjtzOjEyOiJzaWdfaW1nX3BhdGgiO3M6Njc6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo2MDoiL1VzZXJzL2phbWVzL1Byb2plY3RzLzk2YmxhY2svU3luZXJneS9pbWFnZXMvcG1fYXR0YWNobWVudHMvIjtzOjIzOiJwcnZfbXNnX21heF9hdHRhY2htZW50cyI7czoxOiIzIjtzOjIyOiJwcnZfbXNnX2F0dGFjaF9tYXhzaXplIjtzOjM6IjI1MCI7czoyMDoicHJ2X21zZ19hdHRhY2hfdG90YWwiO3M6MzoiMTAwIjtzOjE5OiJwcnZfbXNnX2h0bWxfZm9ybWF0IjtzOjQ6InNhZmUiO3M6MTg6InBydl9tc2dfYXV0b19saW5rcyI7czoxOiJ5IjtzOjE3OiJwcnZfbXNnX21heF9jaGFycyI7czo0OiI2MDAwIjtzOjE5OiJtZW1iZXJsaXN0X29yZGVyX2J5IjtzOjExOiJ0b3RhbF9wb3N0cyI7czoyMToibWVtYmVybGlzdF9zb3J0X29yZGVyIjtzOjQ6ImRlc2MiO3M6MjA6Im1lbWJlcmxpc3Rfcm93X2xpbWl0IjtzOjI6IjIwIjt9', 'YTo2OntzOjExOiJzdHJpY3RfdXJscyI7czoxOiJuIjtzOjg6InNpdGVfNDA0IjtzOjA6IiI7czoxOToic2F2ZV90bXBsX3JldmlzaW9ucyI7czoxOiJuIjtzOjE4OiJtYXhfdG1wbF9yZXZpc2lvbnMiO3M6MToiNSI7czoxNToic2F2ZV90bXBsX2ZpbGVzIjtzOjE6InkiO3M6MTg6InRtcGxfZmlsZV9iYXNlcGF0aCI7czo0NjoiL1VzZXJzL2phbWVzbWNmYWxsL1Byb2plY3RzL1N5bmVyZ3kvdGVtcGxhdGVzLyI7fQ==', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YToyOntzOjc6ImVtYWlsZWQiO2E6MDp7fXM6NDU6Ii9Vc2Vycy9qYW1lc21jZmFsbC9Qcm9qZWN0cy9TeW5lcmd5L2luZGV4LnBocCI7czozMjoiZjFiMTliOGE0NDU5NmM1MzQ2MTViZWJlMTMyNmYwNjgiO30=');
INSERT INTO `exp_sites` VALUES(2, 'Synergy Positioning Systems (AU)', 'synergy_au', '', 'YTo5MTp7czoxMDoiaXNfc2l0ZV9vbiI7czoxOiJ5IjtzOjEwOiJzaXRlX2luZGV4IjtzOjA6IiI7czo4OiJzaXRlX3VybCI7czoyMToiaHR0cDovL3N5bmVyZ3kubG9jYWwvIjtzOjE2OiJ0aGVtZV9mb2xkZXJfdXJsIjtzOjI4OiJodHRwOi8vc3luZXJneS5sb2NhbC90aGVtZXMvIjtzOjE3OiJ0aGVtZV9mb2xkZXJfcGF0aCI7czo0NToiL1VzZXJzL2phbWVzL1Byb2plY3RzLzk2YmxhY2svU3luZXJneS90aGVtZXMvIjtzOjE1OiJ3ZWJtYXN0ZXJfZW1haWwiO3M6MTk6ImphbWVzQDk2YmxhY2suY28ubnoiO3M6MTQ6IndlYm1hc3Rlcl9uYW1lIjtzOjA6IiI7czoyMDoiY2hhbm5lbF9ub21lbmNsYXR1cmUiO3M6NzoiY2hhbm5lbCI7czoxMDoibWF4X2NhY2hlcyI7czozOiIxNTAiO3M6MTE6ImNhcHRjaGFfdXJsIjtzOjM3OiJodHRwOi8vc3luZXJneS5sb2NhbC9pbWFnZXMvY2FwdGNoYXMvIjtzOjEyOiJjYXB0Y2hhX3BhdGgiO3M6NTQ6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL2NhcHRjaGFzLyI7czoxMjoiY2FwdGNoYV9mb250IjtzOjE6InkiO3M6MTI6ImNhcHRjaGFfcmFuZCI7czoxOiJ5IjtzOjIzOiJjYXB0Y2hhX3JlcXVpcmVfbWVtYmVycyI7czoxOiJuIjtzOjE3OiJlbmFibGVfZGJfY2FjaGluZyI7czoxOiJuIjtzOjE4OiJlbmFibGVfc3FsX2NhY2hpbmciO3M6MToibiI7czoxODoiZm9yY2VfcXVlcnlfc3RyaW5nIjtzOjE6Im4iO3M6MTM6InNob3dfcHJvZmlsZXIiO3M6MToibiI7czoxODoidGVtcGxhdGVfZGVidWdnaW5nIjtzOjE6Im4iO3M6MTU6ImluY2x1ZGVfc2Vjb25kcyI7czoxOiJuIjtzOjEzOiJjb29raWVfZG9tYWluIjtzOjA6IiI7czoxMToiY29va2llX3BhdGgiO3M6MDoiIjtzOjE3OiJ1c2VyX3Nlc3Npb25fdHlwZSI7czoxOiJjIjtzOjE4OiJhZG1pbl9zZXNzaW9uX3R5cGUiO3M6MjoiY3MiO3M6MjE6ImFsbG93X3VzZXJuYW1lX2NoYW5nZSI7czoxOiJ5IjtzOjE4OiJhbGxvd19tdWx0aV9sb2dpbnMiO3M6MToieSI7czoxNjoicGFzc3dvcmRfbG9ja291dCI7czoxOiJ5IjtzOjI1OiJwYXNzd29yZF9sb2Nrb3V0X2ludGVydmFsIjtzOjE6IjEiO3M6MjA6InJlcXVpcmVfaXBfZm9yX2xvZ2luIjtzOjE6InkiO3M6MjI6InJlcXVpcmVfaXBfZm9yX3Bvc3RpbmciO3M6MToieSI7czoyNDoicmVxdWlyZV9zZWN1cmVfcGFzc3dvcmRzIjtzOjE6Im4iO3M6MTk6ImFsbG93X2RpY3Rpb25hcnlfcHciO3M6MToieSI7czoyMzoibmFtZV9vZl9kaWN0aW9uYXJ5X2ZpbGUiO3M6MDoiIjtzOjE3OiJ4c3NfY2xlYW5fdXBsb2FkcyI7czoxOiJ5IjtzOjE1OiJyZWRpcmVjdF9tZXRob2QiO3M6ODoicmVkaXJlY3QiO3M6OToiZGVmdF9sYW5nIjtzOjc6ImVuZ2xpc2giO3M6ODoieG1sX2xhbmciO3M6MjoiZW4iO3M6MTI6InNlbmRfaGVhZGVycyI7czoxOiJ5IjtzOjExOiJnemlwX291dHB1dCI7czoxOiJuIjtzOjEzOiJsb2dfcmVmZXJyZXJzIjtzOjE6Im4iO3M6MTM6Im1heF9yZWZlcnJlcnMiO3M6MzoiNTAwIjtzOjExOiJ0aW1lX2Zvcm1hdCI7czoyOiJ1cyI7czoxNToic2VydmVyX3RpbWV6b25lIjtzOjQ6IlVQMTIiO3M6MTM6InNlcnZlcl9vZmZzZXQiO3M6MDoiIjtzOjE2OiJkYXlsaWdodF9zYXZpbmdzIjtzOjE6InkiO3M6MjE6ImRlZmF1bHRfc2l0ZV90aW1lem9uZSI7czo0OiJVUDEyIjtzOjE2OiJkZWZhdWx0X3NpdGVfZHN0IjtzOjE6InkiO3M6MTM6Im1haWxfcHJvdG9jb2wiO3M6NDoibWFpbCI7czoxMToic210cF9zZXJ2ZXIiO3M6MDoiIjtzOjEzOiJzbXRwX3VzZXJuYW1lIjtzOjA6IiI7czoxMzoic210cF9wYXNzd29yZCI7czowOiIiO3M6MTE6ImVtYWlsX2RlYnVnIjtzOjE6Im4iO3M6MTM6ImVtYWlsX2NoYXJzZXQiO3M6NToidXRmLTgiO3M6MTU6ImVtYWlsX2JhdGNobW9kZSI7czoxOiJuIjtzOjE2OiJlbWFpbF9iYXRjaF9zaXplIjtzOjA6IiI7czoxMToibWFpbF9mb3JtYXQiO3M6NToicGxhaW4iO3M6OToid29yZF93cmFwIjtzOjE6InkiO3M6MjI6ImVtYWlsX2NvbnNvbGVfdGltZWxvY2siO3M6MToiNSI7czoyMjoibG9nX2VtYWlsX2NvbnNvbGVfbXNncyI7czoxOiJ5IjtzOjg6ImNwX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MjE6ImVtYWlsX21vZHVsZV9jYXB0Y2hhcyI7czoxOiJuIjtzOjE2OiJsb2dfc2VhcmNoX3Rlcm1zIjtzOjE6InkiO3M6MTI6InNlY3VyZV9mb3JtcyI7czoxOiJ5IjtzOjE5OiJkZW55X2R1cGxpY2F0ZV9kYXRhIjtzOjE6InkiO3M6MjQ6InJlZGlyZWN0X3N1Ym1pdHRlZF9saW5rcyI7czoxOiJuIjtzOjE2OiJlbmFibGVfY2Vuc29yaW5nIjtzOjE6Im4iO3M6MTQ6ImNlbnNvcmVkX3dvcmRzIjtzOjA6IiI7czoxODoiY2Vuc29yX3JlcGxhY2VtZW50IjtzOjA6IiI7czoxMDoiYmFubmVkX2lwcyI7czowOiIiO3M6MTM6ImJhbm5lZF9lbWFpbHMiO3M6MDoiIjtzOjE2OiJiYW5uZWRfdXNlcm5hbWVzIjtzOjA6IiI7czoxOToiYmFubmVkX3NjcmVlbl9uYW1lcyI7czowOiIiO3M6MTA6ImJhbl9hY3Rpb24iO3M6ODoicmVzdHJpY3QiO3M6MTE6ImJhbl9tZXNzYWdlIjtzOjM0OiJUaGlzIHNpdGUgaXMgY3VycmVudGx5IHVuYXZhaWxhYmxlIjtzOjE1OiJiYW5fZGVzdGluYXRpb24iO3M6MjE6Imh0dHA6Ly93d3cueWFob28uY29tLyI7czoxNjoiZW5hYmxlX2Vtb3RpY29ucyI7czoxOiJ5IjtzOjEyOiJlbW90aWNvbl91cmwiO3M6MzY6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9zbWlsZXlzLyI7czoxOToicmVjb3VudF9iYXRjaF90b3RhbCI7czo0OiIxMDAwIjtzOjE3OiJuZXdfdmVyc2lvbl9jaGVjayI7czoxOiJ5IjtzOjE3OiJlbmFibGVfdGhyb3R0bGluZyI7czoxOiJuIjtzOjE3OiJiYW5pc2hfbWFza2VkX2lwcyI7czoxOiJ5IjtzOjE0OiJtYXhfcGFnZV9sb2FkcyI7czoyOiIxMCI7czoxMzoidGltZV9pbnRlcnZhbCI7czoxOiI4IjtzOjEyOiJsb2Nrb3V0X3RpbWUiO3M6MjoiMzAiO3M6MTU6ImJhbmlzaG1lbnRfdHlwZSI7czo3OiJtZXNzYWdlIjtzOjE0OiJiYW5pc2htZW50X3VybCI7czowOiIiO3M6MTg6ImJhbmlzaG1lbnRfbWVzc2FnZSI7czo1MDoiWW91IGhhdmUgZXhjZWVkZWQgdGhlIGFsbG93ZWQgcGFnZSBsb2FkIGZyZXF1ZW5jeS4iO3M6MTc6ImVuYWJsZV9zZWFyY2hfbG9nIjtzOjE6InkiO3M6MTk6Im1heF9sb2dnZWRfc2VhcmNoZXMiO3M6MzoiNTAwIjtzOjExOiJydGVfZW5hYmxlZCI7czoxOiJ5IjtzOjIyOiJydGVfZGVmYXVsdF90b29sc2V0X2lkIjtzOjE6IjEiO30=', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6MzY6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9hdmF0YXJzLyI7czoxMToiYXZhdGFyX3BhdGgiO3M6NTM6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL2F2YXRhcnMvIjtzOjE2OiJhdmF0YXJfbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNzoiYXZhdGFyX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEzOiJhdmF0YXJfbWF4X2tiIjtzOjI6IjUwIjtzOjEzOiJlbmFibGVfcGhvdG9zIjtzOjE6Im4iO3M6OToicGhvdG9fdXJsIjtzOjQyOiJodHRwOi8vc3luZXJneS5sb2NhbC9pbWFnZXMvbWVtYmVyX3Bob3Rvcy8iO3M6MTA6InBob3RvX3BhdGgiO3M6NTk6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjE1OiJwaG90b19tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE2OiJwaG90b19tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMjoicGhvdG9fbWF4X2tiIjtzOjI6IjUwIjtzOjE2OiJhbGxvd19zaWduYXR1cmVzIjtzOjE6InkiO3M6MTM6InNpZ19tYXhsZW5ndGgiO3M6MzoiNTAwIjtzOjIxOiJzaWdfYWxsb3dfaW1nX2hvdGxpbmsiO3M6MToibiI7czoyMDoic2lnX2FsbG93X2ltZ191cGxvYWQiO3M6MToibiI7czoxMToic2lnX2ltZ191cmwiO3M6NTA6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9zaWduYXR1cmVfYXR0YWNobWVudHMvIjtzOjEyOiJzaWdfaW1nX3BhdGgiO3M6Njc6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo2MDoiL1VzZXJzL2phbWVzL1Byb2plY3RzLzk2YmxhY2svU3luZXJneS9pbWFnZXMvcG1fYXR0YWNobWVudHMvIjtzOjIzOiJwcnZfbXNnX21heF9hdHRhY2htZW50cyI7czoxOiIzIjtzOjIyOiJwcnZfbXNnX2F0dGFjaF9tYXhzaXplIjtzOjM6IjI1MCI7czoyMDoicHJ2X21zZ19hdHRhY2hfdG90YWwiO3M6MzoiMTAwIjtzOjE5OiJwcnZfbXNnX2h0bWxfZm9ybWF0IjtzOjQ6InNhZmUiO3M6MTg6InBydl9tc2dfYXV0b19saW5rcyI7czoxOiJ5IjtzOjE3OiJwcnZfbXNnX21heF9jaGFycyI7czo0OiI2MDAwIjtzOjE5OiJtZW1iZXJsaXN0X29yZGVyX2J5IjtzOjExOiJ0b3RhbF9wb3N0cyI7czoyMToibWVtYmVybGlzdF9zb3J0X29yZGVyIjtzOjQ6ImRlc2MiO3M6MjA6Im1lbWJlcmxpc3Rfcm93X2xpbWl0IjtzOjI6IjIwIjt9', 'YTo2OntzOjE1OiJzYXZlX3RtcGxfZmlsZXMiO3M6MToieSI7czoxODoidG1wbF9maWxlX2Jhc2VwYXRoIjtzOjQ4OiIvVXNlcnMvamFtZXMvUHJvamVjdHMvOTZibGFjay9TeW5lcmd5L3RlbXBsYXRlcy8iO3M6ODoic2l0ZV80MDQiO3M6MDoiIjtzOjE5OiJzYXZlX3RtcGxfcmV2aXNpb25zIjtzOjE6Im4iO3M6MTg6Im1heF90bXBsX3JldmlzaW9ucyI7czoxOiI1IjtzOjExOiJzdHJpY3RfdXJscyI7czoxOiJuIjt9', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YToyOntzOjc6ImVtYWlsZWQiO2E6MDp7fXM6NDc6Ii9Vc2Vycy9qYW1lc21jZmFsbC9Qcm9qZWN0cy9TeW5lcmd5QVUvaW5kZXgucGhwIjtzOjMyOiJiZjViZGMzMGYwYWVmZDI4Mjc0MmU4MDUxNTAyMWY4MSI7fQ==');

/* Table structure for table `exp_snippets` */
DROP TABLE IF EXISTS `exp_snippets`;

CREATE TABLE `exp_snippets` (
  `snippet_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) NOT NULL,
  `snippet_name` varchar(75) NOT NULL,
  `snippet_contents` text,
  PRIMARY KEY (`snippet_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_specialty_templates` */
DROP TABLE IF EXISTS `exp_specialty_templates`;

CREATE TABLE `exp_specialty_templates` (
  `template_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_name` varchar(50) NOT NULL,
  `data_title` varchar(80) NOT NULL,
  `template_data` text NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_specialty_templates` */
INSERT INTO `exp_specialty_templates` VALUES(1, 1, 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>');
INSERT INTO `exp_specialty_templates` VALUES(2, 1, 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=\'content-type\' content=\'text/html; charset={charset}\' />\n\n{meta_refresh}\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>');
INSERT INTO `exp_specialty_templates` VALUES(3, 1, 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}');
INSERT INTO `exp_specialty_templates` VALUES(4, 1, 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n');
INSERT INTO `exp_specialty_templates` VALUES(5, 1, 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}');
INSERT INTO `exp_specialty_templates` VALUES(6, 1, 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}');
INSERT INTO `exp_specialty_templates` VALUES(7, 1, 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(8, 1, 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(9, 1, 'y', 'reset_password_notification', 'New Login Information', '{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(10, 1, 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(11, 1, 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe\'re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(12, 1, 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the \"{mailing_list}\" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}');
INSERT INTO `exp_specialty_templates` VALUES(13, 1, 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}');
INSERT INTO `exp_specialty_templates` VALUES(14, 1, 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}');
INSERT INTO `exp_specialty_templates` VALUES(15, 1, 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled �{message_subject}�.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(16, 1, 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(17, 2, 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>');
INSERT INTO `exp_specialty_templates` VALUES(18, 2, 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=\'content-type\' content=\'text/html; charset={charset}\' />\n\n{meta_refresh}\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>');
INSERT INTO `exp_specialty_templates` VALUES(19, 2, 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}');
INSERT INTO `exp_specialty_templates` VALUES(20, 2, 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n');
INSERT INTO `exp_specialty_templates` VALUES(21, 2, 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}');
INSERT INTO `exp_specialty_templates` VALUES(22, 2, 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}');
INSERT INTO `exp_specialty_templates` VALUES(23, 2, 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(24, 2, 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(25, 2, 'y', 'reset_password_notification', 'New Login Information', '{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(26, 2, 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(27, 2, 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe\'re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(28, 2, 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the \"{mailing_list}\" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}');
INSERT INTO `exp_specialty_templates` VALUES(29, 2, 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}');
INSERT INTO `exp_specialty_templates` VALUES(30, 2, 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}');
INSERT INTO `exp_specialty_templates` VALUES(31, 2, 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled �{message_subject}�.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(32, 2, 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}');

/* Table structure for table `exp_stats` */
DROP TABLE IF EXISTS `exp_stats`;

CREATE TABLE `exp_stats` (
  `stat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `total_members` mediumint(7) NOT NULL DEFAULT '0',
  `recent_member_id` int(10) NOT NULL DEFAULT '0',
  `recent_member` varchar(50) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `most_visitors` mediumint(7) NOT NULL DEFAULT '0',
  `most_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_cache_clear` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`stat_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_stats` */
INSERT INTO `exp_stats` VALUES(1, 1, 1, 1, '96black', 36, 0, 0, 0, 1366105507, 0, 0, 1366105783, 34, 1365643350, 1366678558);
INSERT INTO `exp_stats` VALUES(2, 2, 1, 1, '96black', 0, 0, 0, 0, 0, 0, 0, 1365848246, 6, 1365845962, 1366235389);

/* Table structure for table `exp_status_groups` */
DROP TABLE IF EXISTS `exp_status_groups`;

CREATE TABLE `exp_status_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_status_groups` */
INSERT INTO `exp_status_groups` VALUES(1, 1, 'Statuses');
INSERT INTO `exp_status_groups` VALUES(2, 2, 'Statuses');

/* Table structure for table `exp_status_no_access` */
DROP TABLE IF EXISTS `exp_status_no_access`;

CREATE TABLE `exp_status_no_access` (
  `status_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`status_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_statuses` */
DROP TABLE IF EXISTS `exp_statuses`;

CREATE TABLE `exp_statuses` (
  `status_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `status_order` int(3) unsigned NOT NULL,
  `highlight` varchar(30) NOT NULL,
  PRIMARY KEY (`status_id`),
  KEY `group_id` (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_statuses` */
INSERT INTO `exp_statuses` VALUES(1, 1, 1, 'open', 1, '009933');
INSERT INTO `exp_statuses` VALUES(2, 1, 1, 'closed', 2, '990000');
INSERT INTO `exp_statuses` VALUES(3, 2, 2, 'open', 1, '009933');
INSERT INTO `exp_statuses` VALUES(4, 2, 2, 'closed', 2, '990000');

/* Table structure for table `exp_template_groups` */
DROP TABLE IF EXISTS `exp_template_groups`;

CREATE TABLE `exp_template_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `group_order` int(3) unsigned NOT NULL,
  `is_site_default` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_template_groups` */
INSERT INTO `exp_template_groups` VALUES(1, 1, 'Routing', 1, 'y');
INSERT INTO `exp_template_groups` VALUES(5, 1, 'Home', 3, 'n');
INSERT INTO `exp_template_groups` VALUES(6, 1, 'Products', 4, 'n');
INSERT INTO `exp_template_groups` VALUES(8, 1, 'News', 6, 'n');
INSERT INTO `exp_template_groups` VALUES(9, 1, 'About-us', 7, 'n');
INSERT INTO `exp_template_groups` VALUES(10, 1, 'Contact-us', 8, 'n');
INSERT INTO `exp_template_groups` VALUES(11, 1, 'Services', 9, 'n');
INSERT INTO `exp_template_groups` VALUES(12, 1, 'Common', 10, 'n');
INSERT INTO `exp_template_groups` VALUES(13, 1, 'Featured-products', 10, 'n');
INSERT INTO `exp_template_groups` VALUES(14, 1, 'Case-studies', 11, 'n');
INSERT INTO `exp_template_groups` VALUES(15, 1, 'Search', 11, 'n');

/* Table structure for table `exp_template_member_groups` */
DROP TABLE IF EXISTS `exp_template_member_groups`;

CREATE TABLE `exp_template_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `template_group_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`template_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_template_no_access` */
DROP TABLE IF EXISTS `exp_template_no_access`;

CREATE TABLE `exp_template_no_access` (
  `template_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`template_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_templates` */
DROP TABLE IF EXISTS `exp_templates`;

CREATE TABLE `exp_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `template_name` varchar(50) NOT NULL,
  `save_template_file` char(1) NOT NULL DEFAULT 'n',
  `template_type` varchar(16) NOT NULL DEFAULT 'webpage',
  `template_data` mediumtext,
  `template_notes` text,
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `last_author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cache` char(1) NOT NULL DEFAULT 'n',
  `refresh` int(6) unsigned NOT NULL DEFAULT '0',
  `no_auth_bounce` varchar(50) NOT NULL DEFAULT '',
  `enable_http_auth` char(1) NOT NULL DEFAULT 'n',
  `allow_php` char(1) NOT NULL DEFAULT 'n',
  `php_parse_location` char(1) NOT NULL DEFAULT 'o',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_id`),
  KEY `group_id` (`group_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_templates` */
INSERT INTO `exp_templates` VALUES(1, 1, 1, 'index', 'y', 'webpage', '{embed=\"Common/header\"}\n\n{embed=\"Home/index\"}\n\n{embed=\"Common/footer\"}', '', 1365458557, 1, 'n', 0, '', 'n', 'y', 'o', 7970);
INSERT INTO `exp_templates` VALUES(4, 2, 1, 'monkey-butt', 'y', 'webpage', 'OH SNAP', '', 1355889015, 1, 'n', 0, '', 'n', 'n', 'o', 1);
INSERT INTO `exp_templates` VALUES(9, 1, 5, 'index', 'y', 'webpage', 'Home test', '', 1355945914, 1, 'n', 0, '', 'n', 'y', 'i', 4);
INSERT INTO `exp_templates` VALUES(10, 1, 6, 'index', 'y', 'webpage', '', '', 1355945909, 1, 'n', 0, '', 'n', 'y', 'o', 1528);
INSERT INTO `exp_templates` VALUES(12, 1, 8, 'index', 'y', 'webpage', '', '', 1355945899, 1, 'n', 0, '', 'n', 'n', 'o', 67);
INSERT INTO `exp_templates` VALUES(13, 1, 9, 'index', 'y', 'webpage', '', '', 1355945894, 1, 'n', 0, '', 'n', 'n', 'o', 90);
INSERT INTO `exp_templates` VALUES(14, 1, 10, 'index', 'y', 'webpage', '', '', 1355945889, 1, 'n', 0, '', 'n', 'y', 'i', 132);
INSERT INTO `exp_templates` VALUES(15, 1, 11, 'index', 'y', 'webpage', '', '', 1355945883, 1, 'n', 0, '', 'n', 'n', 'o', 224);
INSERT INTO `exp_templates` VALUES(16, 1, 12, 'index', 'y', 'webpage', '', '', 1355945878, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(17, 1, 12, 'header', 'y', 'webpage', '', '', 1362949938, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(18, 1, 12, 'footer', 'y', 'webpage', '', '', 1355946002, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(19, 1, 12, '_menu', 'y', 'webpage', '', '', 1362965438, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(20, 1, 6, 'product-page', 'y', 'webpage', '', '', 1358280171, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(22, 1, 6, 'category-listing', 'y', 'webpage', 'Category listing!', '', 1358280845, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(23, 1, 6, '_product-nav', 'y', 'webpage', '<?\n# We want to get the second to last segment of the URL to use in the breadcrumbs.\n$segments = explode(\"/\", $_SERVER[\"REQUEST_URI\"]);\n$last = $segments[count($segments) - 1]; # -1 because index starts from 0 (count doesn\'t).\n$secToLast = $segments[count($segments) - 2]; # -2 because index starts from 0 (count doesn\'t).\n\n# Category segment to match on is normally the last segment.\n$catSegment = $last;\n\n# If we get a channel back when checking the last segment , we\'re on a product page.\n# So we\'ll use the second to last segment instead.\nif (Synergy_Utils::getChannelForEntry($last)) {\n    $catSegment = $secToLast;\n}\n\n?>\n\n<h3>Products</h3>\n{exp:gwcode_catmenu group_id=\"1\" cat_url_title=\"<?=$catSegment?>\" id=\"sideMenu\"}\n<a href=\"/products/{complete_path}\" {if active} class=\"active\"{/if}>\n    {cat_name}\n</a>\n{/exp:gwcode_catmenu}\n<!--\n{exp:gwcode_categories channel=\"products\"}\n<a href=\"/products/{complete_path}\">{cat_name}</a>\n{/exp:gwcode_categories}\n-->\n\n{embed=\"Products/_enquiry-form\"}\n{embed=\"Products/_rental-form\"}', '', 1366083047, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(24, 1, 6, 'category-landing', 'y', 'webpage', '', '', 1358365005, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(26, 1, 6, '_category-listing-products-blocks', 'y', 'webpage', '', '', 1362954501, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(27, 1, 8, 'article-list', 'y', 'webpage', '', '', 1362957827, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(28, 1, 8, 'article', 'y', 'webpage', '', '', 1362957835, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(29, 1, 10, '_contact-form', 'y', 'webpage', '', '', 1363570256, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(30, 1, 13, 'index', 'y', 'webpage', '', '', 1363828125, 1, 'n', 0, '', 'n', 'n', 'o', 1);
INSERT INTO `exp_templates` VALUES(31, 1, 13, 'page-intros', 'y', 'webpage', '{exp:channel:entries channel=\"products\" entry_id=\"{embed:product_id}\" limit=\"1\"}\n<div class=\"additionalBlocks\">\n    <ul>\n        {reverse_related_entries id=\"featured_product\"}\n        <li class=\"{switch=\'||third\'}\">\n            <h3><a href=\"{url_title}\">{title}</a></h3>\n            <p>{featured_content_intro}</p>\n            <a href=\"{url_title}\" class=\"readMore\">Read More</a>\n        </li>\n        {/reverse_related_entries}\n    </ul>\n    <div class=\"clear\"></div>\n</div>\n{/exp:channel:entries}', '', 1363827515, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(32, 1, 13, 'page', 'y', 'webpage', '', '', 1363823952, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(33, 1, 6, 'featured_subpage', 'y', 'webpage', '', '', 1363829194, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(34, 1, 14, 'index', 'y', 'webpage', '', '', 1363904378, 1, 'n', 0, '', 'n', 'n', 'o', 52);
INSERT INTO `exp_templates` VALUES(35, 1, 14, 'article-list', 'y', 'webpage', '', '', 1363904459, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(36, 1, 14, 'article', 'y', 'webpage', '', '', 1363904454, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(37, 1, 11, '_services-nav', 'y', 'webpage', '<h3>Services</h3>\n<ul id=\"sideMenu\">\n    <li><a href=\"#\">Roads &amp; Services</a></li>\n    <li><a href=\"#\">Mapping</a></li>\n    <li><a href=\"#\">Lorem Ipsum</a> </li>\n    <li><a href=\"#\">Lorem Ipsum</a> </li>\n</ul>', null, 1365479093, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(38, 1, 11, 'service-landing', 'y', 'webpage', '<div id=\"content\" class=\"std\">\n    <div class=\"middle\">\n        <div id=\"sideBar\">\n            {embed=\"Services/_services-nav\"}\n        </div>\n\n        <div id=\"copy\" class=\"services\">\n            <div class=\"inner\">\n\n                <ul class=\"breadCrumb\">\n                    <li><a href=\"#\">Home</a></li>\n                    <li class=\"current\"><a href=\"#\">Services</a></li>\n                </ul>\n\n                <div class=\"main\">\n                    <h1>Services</h1>\n                </div>\n                <ul class=\"servicesList\">\n                    <li><img src=\"/images/temp/services-temp.jpg\" alt=\"Roads &amp; Services\" />\n                        <h2><a href=\"#\">Roads and Services</a></h2>\n                        <div class=\"excerpt\"><p>One of our core services, Synergy Positioning are experts in asphalt paving technologies. We offer a complete solution for all your paving requirements. Using a host of suppliers including: Moba Matic, Topcon & Roadware, we have complete control of any surfacing project.</p></div>\n                        <a href=\"#\" class=\"readMore\">Read More</a>\n                    </li>\n                    <li class=\"second\"><img src=\"/images/temp/services-temp.jpg\" alt=\"Mapping\" />\n                        <h2><a href=\"#\">Mapping</a></h2>\n                        <div class=\"excerpt\"><p>One of our core services, Synergy Positioning are experts in asphalt paving technologies. We offer a complete solution for all your paving requirements. Using a host of suppliers including: Moba Matic, Topcon & Roadware, we have complete control of any surfacing project.</p></div>\n                        <a href=\"#\" class=\"readMore\">Read More</a>\n                    </li>\n                    <li><img src=\"/images/temp/services-temp.jpg\" alt=\"Lorem Ipsum Dolor\" />\n                        <h2><a href=\"#\">Lorem Ipsum Dolor</a></h2>\n                        <div class=\"excerpt\"><p>One of our core services, Synergy Positioning are experts in asphalt paving technologies. We offer a complete solution for all your paving requirements. Using a host of suppliers including: Moba Matic, Topcon & Roadware, we have complete control of any surfacing project.</p></div>\n                        <a href=\"#\" class=\"readMore\">Read More</a>\n                    </li>\n                    <li class=\"second\"><img src=\"/images/temp/services-temp.jpg\" alt=\"Lorem Ipsum Dolor\" />\n                        <h2><a href=\"#\">Lorem Ipsum Dolor</a></h2>\n                        <div class=\"excerpt\"><p>One of our core services, Synergy Positioning are experts in asphalt paving technologies. We offer a complete solution for all your paving requirements. Using a host of suppliers including: Moba Matic, Topcon & Roadware, we have complete control of any surfacing project.</p></div>\n                        <a href=\"#\" class=\"readMore\">Read More</a>\n                    </li>\n                </ul>	\n\n\n            </div>\n\n\n\n        </div>\n        <div class=\"clear\"></div>\n    </div>\n</div>', null, 1365479411, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(39, 1, 11, 'service-listing', 'y', 'webpage', '\n<div id=\"copy\" class=\"services servicesDetail\">\n    <div class=\"topPageImage\">\n        <img src=\"/images/temp/services-temp-top.jpg\" alt=\"Roads &amp; Services\" />\n    </div>\n\n    <div class=\"inner\">\n\n        <ul class=\"breadCrumb\">\n            <li><a href=\"#\">Home</a></li>\n            <li><a href=\"#\">Services</a></li>\n            <li><a href=\"#\">Roads &amp; Services</a></li>\n            <li class=\"current\"><a href=\"#\">Survey &amp; Design</a></li>\n\n        </ul>\n\n        <div class=\"main\">\n            <h1>Survey &amp; Design</h1>\n            <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus.</p>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. Aliquam eleifend enim eleifend lacus pretium ac pretium risus sodales. Aliquam mi nisi, suscipit in ornare in, tempus a justo. Vivamus porta, justo in sollicitudin vulputate, velit massa congue mi, a vehicula enim metus a lectus. Sed felis tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n            <hr />\n\n            <p><img src=\"/images/temp/services-temp-detail-1.jpg\" alt=\"Survey &amp; Design\" class=\"alignLeft\" /></p>\n\n            <h3>Lorem Ipsum Dolor</h3>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullam]is tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n            <h3>Pellentesque mattis ultrices dapibu uis ullamcorp</h3>\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit m]is tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n\n            <hr />\n            <iframe class=\"alignRight\" width=\"417\" height=\"267\" src=\"http://www.youtube.com/embed/39zYdKe6vLs\" frameborder=\"0\" allowfullscreen></iframe>\n            <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus.</h3>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper</p>\n        </div>\n    </div>\n\n    <div class=\"additionalBlocks\">\n        <ul>\n            <li>\n                <h3><a href=\"#\">Survey &amp; Design</a></h3>\n                <p>Synergy Positioning uses an in-house survey team with many years of experience in the asphalt and roading industries. Our in-house experts in CAD, manages the design and control of final surfacing levels to ensure a smooth and cost effective result, on time and within budget</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n            <li>\n                <h3><a href=\"#\">Paving Control</a></h3>\n                <p>Our paving managers are the best in the industry. Taking full control of the paving crew, we manage the project from start to finish.</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n            <li class=\"third\">\n                <h3><a href=\"#\">Additional Information</a></h3>\n                <p>Sed sagittis tincidunt lobortis. In non ligula a urna blandit tempor. Aenean a leo ut mauris posuere aliquet. Phasellus faucibus convallis aliquet. Proin velit mi, tempus quis varius eu, suscipit sit amet nisl.</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n        </ul>\n        <div class=\"clear\"></div>\n    </div>\n\n\n</div>\n', null, 1365481672, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(40, 1, 11, 'service-page', 'y', 'webpage', '<div id=\"copy\" class=\"services servicesDetail\">\n    <div class=\"topPageImage\">\n        <img src=\"/images/temp/services-temp-top.jpg\" alt=\"Roads &amp; Services\" />\n    </div>\n\n    <div class=\"inner\">\n\n        <ul class=\"breadCrumb\">\n            <li><a href=\"#\">Home</a></li>\n            <li><a href=\"#\">Services</a></li>\n            <li><a href=\"#\">Roads &amp; Services</a></li>\n            <li class=\"current\"><a href=\"#\">Survey &amp; Design</a></li>\n\n        </ul>\n\n        <div class=\"main\">\n            <h1>Survey &amp; Design</h1>\n            <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus.</p>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. Aliquam eleifend enim eleifend lacus pretium ac pretium risus sodales. Aliquam mi nisi, suscipit in ornare in, tempus a justo. Vivamus porta, justo in sollicitudin vulputate, velit massa congue mi, a vehicula enim metus a lectus. Sed felis tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n            <hr />\n\n            <p><img src=\"/images/temp/services-temp-detail-1.jpg\" alt=\"Survey &amp; Design\" class=\"alignLeft\" /></p>\n\n            <h3>Lorem Ipsum Dolor</h3>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullam]is tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n            <h3>Pellentesque mattis ultrices dapibu uis ullamcorp</h3>\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit m]is tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n\n            <hr />\n            <iframe class=\"alignRight\" width=\"417\" height=\"267\" src=\"http://www.youtube.com/embed/39zYdKe6vLs\" frameborder=\"0\" allowfullscreen></iframe>\n            <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus.</h3>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper</p>\n        </div>\n    </div>\n\n    <div class=\"additionalBlocks\">\n        <ul>\n            <li>\n                <h3><a href=\"#\">Survey &amp; Design</a></h3>\n                <p>Synergy Positioning uses an in-house survey team with many years of experience in the asphalt and roading industries. Our in-house experts in CAD, manages the design and control of final surfacing levels to ensure a smooth and cost effective result, on time and within budget</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n            <li>\n                <h3><a href=\"#\">Paving Control</a></h3>\n                <p>Our paving managers are the best in the industry. Taking full control of the paving crew, we manage the project from start to finish.</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n            <li class=\"third\">\n                <h3><a href=\"#\">Additional Information</a></h3>\n                <p>Sed sagittis tincidunt lobortis. In non ligula a urna blandit tempor. Aenean a leo ut mauris posuere aliquet. Phasellus faucibus convallis aliquet. Proin velit mi, tempus quis varius eu, suscipit sit amet nisl.</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n        </ul>\n        <div class=\"clear\"></div>\n    </div>\n\n\n</div>\n', null, 1365481672, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(41, 1, 9, 'page', 'y', 'webpage', '<div id=\"copy\" class=\"about\">\n            <div class=\"topPageImage\">\n                <img src=\"/images/temp/about-temp.jpg\" alt=\"About Synergy Positioning Systems\" />\n            </div>\n\n            <div class=\"inner\">\n\n                <ul class=\"breadCrumb\">\n                    <li><a href=\"#\">Home</a></li>\n                    <li class=\"current\"><a href=\"#\">About Us</a></li>\n\n                </ul>\n\n                <div class=\"main\">\n                    <h1>About Us</h1>\n\n                    <p class=\"intro\">Synergy Positioning Systems Ltd is 100% New Zealand owned and operated. It was formed when two of the industry�s most trusted companies merged in 2006 - Synergy Equipment and Systems Ltd and Geodetic Instruments Ltd. The result is a company that brings you over 30 years experience delivering the most advanced and innovative technologies combined with a commitment to outstanding customer service.</p>\n\n                    <p>Our aim is to make your job faster and easier, more accurate and cost efficient. We\'ll do this by giving you expert and honest advice and by offering you the best solution from our range of innovative positioning systems.</p>\n\n                    <p>Reliability is paramount - equipment breakages cost time and money. There are many low quality brands on the market that just don\'t stand up to the rigours of our construction industry. We only sell reliable brands we can trust and service.</p>\n\n                    <p>Our experienced team are always on hand for after sales support, product training and servicing, giving you the confidence and skills to get the best out of your equipment. Our product range is extensive and always growing.</p>\n\n                    <h2>Why choose synergy?</h2>\n\n\n                </div>\n            </div>\n\n            <div class=\"additionalBlocks\">\n                <ul>\n                    <li>\n                        <h3>Simple really....</h3>\n                        <p>You are dealing with an established company representing world renowned brands that have been tried and tested in increasing productivity and reducing costs.</p>\n\n                    </li>\n                    <li>\n                        <h3>Your profit...</h3>\n                        <p>With our equipment and systems, you gain efficiency &amp; reduce costs, saving you money on all aspects of the job including: fuel, wages, wear &amp; tear, materials &amp; processing. </p>\n\n                    </li>\n                    <li>\n                        <h3>Our Products</h3>\n                        <p>We only represent equipment &amp; brands that we trust. Topcon, world leaders in positioning equipment for over 80 years &amp; well established throughout the world, offers reliable &amp; robust equipment for any application.</p>\n\n                    </li>\n                    <li>\n                        <h3>Our Experience</h3>\n                        <p>Synergy Positioning Systems Ltd is 100% New Zealand owned and operated. Our industry exprience spans more than 30 years. </p>\n\n                    </li>\n                    <li>\n                        <h3>Our Service</h3>\n                        <p>We are commited to our customers. Your success is our reward. Our factory trained technicians and in-house industry experts ensure that what we sell is fully supported.</p>\n\n                    </li>\n                </ul>\n                <div class=\"clear\"></div>\n            </div>\n\n\n\n        </div>', null, 1365632661, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(42, 1, 9, 'management-page', 'y', 'webpage', '<div id=\"copy\" class=\"management\">\n\n    <div class=\"inner\">\n\n        <ul class=\"breadCrumb\">\n            <li><a href=\"#\">Home</a></li>\n            <li><a href=\"#\">About Us</a></li>\n            <li class=\"current\"><a href=\"#\">Management</a></li>\n\n        </ul>\n\n        <div class=\"main\">\n            <h1>Management</h1>\n\n            <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet.</p>\n\n            <hr />\n\n            <h2>New Zealand</h2>\n\n            <ul class=\"profileList\">\n                <li class=\"first\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 3 </strong>\n                        <span class=\"title\">Title</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n\n                <li class=\"second\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"third\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"fourth\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"fifth\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n            </ul>\n\n            <hr />\n\n            <h2>Australia</h2>\n\n            <ul class=\"profileList\">\n                <li class=\"first\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 3 </strong>\n                        <span class=\"title\">Title</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n\n                <li class=\"second\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"third\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"fourth\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"fifth\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n            </ul>\n        </div>\n    </div>\n\n\n\n\n\n</div>', '', 1365635326, 1, 'n', 0, '', 'n', 'n', 'o', 12);
INSERT INTO `exp_templates` VALUES(43, 1, 15, 'index', 'y', 'webpage', '{exp:search:simple_form channel=\"news\"}\n        <p>\n                <label for=\"keywords\">Search:</label><br>\n                <input type=\"text\" name=\"keywords\" id=\"keywords\" value=\"\" size=\"18\" maxlength=\"100\">\n        </p>\n        <p>\n                <a href=\"{path=\'search/index\'}\">Advanced Search</a>\n        </p>\n        <p>\n                <input type=\"submit\" value=\"submit\" class=\"submit\">\n        </p>\n{/exp:search:simple_form}', '', 1365964420, 1, 'n', 0, '', 'n', 'n', 'o', 5);
INSERT INTO `exp_templates` VALUES(44, 1, 15, 'results', 'y', 'webpage', '{exp:search:simple_form channel=\"products|services\" result_page=\"search/results\"}\n        <p>\n                <label for=\"keywords\">Search:</label><br>\n                <input type=\"text\" name=\"keywords\" id=\"keywords\" value=\"\" size=\"18\" maxlength=\"100\">\n        </p>\n        <p>\n                <a href=\"{path=\'search/index\'}\">Advanced Search</a>\n        </p>\n        <p>\n                <input type=\"submit\" value=\"submit\" class=\"submit\">\n        </p>\n{/exp:search:simple_form}\n\n<table border=\"0\" cellpadding=\"6\" cellspacing=\"1\" width=\"100%\">\n    <tr>\n        <th>{lang:title}</th>\n        <th>{lang:excerpt}</th>\n        <th>{lang:author}</th>\n        <th>{lang:date}</th>\n        <th>{lang:total_comments}</th>\n        <th>{lang:recent_comments}</th>\n    </tr>\n\n{exp:search:search_results switch=\"resultRowOne|resultRowTwo\"}\n\n    <tr class=\"{switch}\">\n        <td width=\"30%\" valign=\"top\"><b><a href=\"{auto_path}\">{title}</a></b></td>\n        <td width=\"30%\" valign=\"top\">{excerpt}</td>\n        <td width=\"10%\" valign=\"top\"><a href=\"{member_path=\'member/index\'}\">{author}</a></td>\n        <td width=\"10%\" valign=\"top\">{entry_date format=\"%m/%d/%y\"}</td>\n        <td width=\"10%\" valign=\"top\">{comment_total}</td>\n        <td width=\"10%\" valign=\"top\">{recent_comment_date format=\"%m/%d/%y\"}</td>\n    </tr>\n\n    {if count == total_results}\n        </table>\n    {/if}\n\n    {paginate}\n        <p>Page {current_page} of {total_pages} pages {pagination_links}</p>\n    {/paginate}\n\n{/exp:search:search_results}', '', 1365965029, 1, 'n', 0, '', 'n', 'y', 'o', 34);
INSERT INTO `exp_templates` VALUES(45, 1, 15, '_search-form', 'y', 'webpage', '{exp:search:simple_form channel=\"products|services\" result_page=\"search/_results\"}\n        <p>\n                <label for=\"keywords\">Search:</label><br>\n                <input type=\"text\" name=\"keywords\" id=\"keywords\" value=\"\" size=\"18\" maxlength=\"100\">\n        </p>\n        <p>\n                <input type=\"submit\" value=\"submit\" class=\"submit\">\n        </p>\n{/exp:search:simple_form}', '', 1365965186, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(46, 1, 6, '_enquiry-form', 'y', 'webpage', 'f', '', 1365966324, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(47, 1, 6, '_rental-form', 'y', 'webpage', '', '', 1366083052, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(48, 1, 15, '_product_block', 'y', 'webpage', '', '', 1366096389, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(49, 1, 15, '_services_block', 'y', 'webpage', '', '', 1366104991, 1, 'n', 0, '', 'n', 'y', 'o', 0);

/* Table structure for table `exp_throttle` */
DROP TABLE IF EXISTS `exp_throttle`;

CREATE TABLE `exp_throttle` (
  `throttle_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL,
  `locked_out` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`throttle_id`),
  KEY `ip_address` (`ip_address`),
  KEY `last_activity` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_upload_no_access` */
DROP TABLE IF EXISTS `exp_upload_no_access`;

CREATE TABLE `exp_upload_no_access` (
  `upload_id` int(6) unsigned NOT NULL,
  `upload_loc` varchar(3) NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`upload_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_upload_prefs` */
DROP TABLE IF EXISTS `exp_upload_prefs`;

CREATE TABLE `exp_upload_prefs` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `server_path` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL,
  `allowed_types` varchar(3) NOT NULL DEFAULT 'img',
  `max_size` varchar(16) DEFAULT NULL,
  `max_height` varchar(6) DEFAULT NULL,
  `max_width` varchar(6) DEFAULT NULL,
  `properties` varchar(120) DEFAULT NULL,
  `pre_format` varchar(120) DEFAULT NULL,
  `post_format` varchar(120) DEFAULT NULL,
  `file_properties` varchar(120) DEFAULT NULL,
  `file_pre_format` varchar(120) DEFAULT NULL,
  `file_post_format` varchar(120) DEFAULT NULL,
  `cat_group` varchar(255) DEFAULT NULL,
  `batch_location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_upload_prefs` */
INSERT INTO `exp_upload_prefs` VALUES(1, 1, 'Product Images', '/Users/jamesmcfall/Projects/Synergy/uploads/product-images/', 'http://synergy.local/uploads/product-images/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(2, 1, 'Product Downloads', '/Users/jamesmcfall/Projects/Synergy/uploads/product-downloads/', 'http://synergy.local/uploads/product-downloads/', 'all', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(3, 1, 'Category Images', '/Users/jamesmcfall/Projects/Synergy/uploads/category-images/', 'http://synergy.local/uploads/category-images/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(4, 1, 'News Article Banner Image', '/Users/jamesmcfall/Projects/Synergy/uploads/news-images/', 'http://synergy.local/uploads/news-images/', 'img', '', '373', '866', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(5, 1, 'Case Study Banner Image', '/Users/jamesmcfall/Projects/Synergy/uploads/case-study-image/', 'http://synergy.local/uploads/case-study-image/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(6, 1, 'Home Page Banners', '/Users/jamesmcfall/Projects/Synergy/uploads/home-page-banners/', 'http://synergy.local/uploads/home-page-banners/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(7, 1, 'Services Banners', '/Users/jamesmcfall/Projects/Synergy/uploads/services-banners/', 'http://synergy.local/uploads/services-banners/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(8, 1, 'About Banners', '/Users/jamesmcfall/Projects/Synergy/uploads/about-banners/', 'http://synergy.local/uploads/about-banners/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(9, 1, 'Staff Pictures', '/Users/jamesmcfall/Projects/Synergy/uploads/staff-pictures/', 'http://synergy.local/uploads/staff-pictures/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(10, 1, 'Manufacturer Images', '/Users/jamesmcfall/Projects/Synergy/uploads/manufacturer-images/', 'http://synergy.local/uploads/manufacturer-images/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(11, 1, 'Contact Maps', '/Users/jamesmcfall/Projects/Synergy/uploads/contact-maps/', 'http://synergy.local/uploads/contact-maps/', 'img', '', '', '', '', '', '', '', '', '', '', null);

/* Table structure for table `exp_wygwam_configs` */
DROP TABLE IF EXISTS `exp_wygwam_configs`;

CREATE TABLE `exp_wygwam_configs` (
  `config_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `config_name` varchar(32) DEFAULT NULL,
  `settings` text,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_wygwam_configs` */
INSERT INTO `exp_wygwam_configs` VALUES(1, 'Basic', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTA6e2k6MDtzOjQ6IkJvbGQiO2k6MTtzOjY6Ikl0YWxpYyI7aToyO3M6OToiVW5kZXJsaW5lIjtpOjM7czo2OiJTdHJpa2UiO2k6NDtzOjEyOiJOdW1iZXJlZExpc3QiO2k6NTtzOjEyOiJCdWxsZXRlZExpc3QiO2k6NjtzOjQ6IkxpbmsiO2k6NztzOjY6IlVubGluayI7aTo4O3M6NjoiQW5jaG9yIjtpOjk7czo1OiJBYm91dCI7fXM6NjoiaGVpZ2h0IjtzOjM6IjIwMCI7czoxNDoicmVzaXplX2VuYWJsZWQiO3M6MToieSI7czoxMToiY29udGVudHNDc3MiO2E6MDp7fXM6MTA6InVwbG9hZF9kaXIiO3M6MDoiIjt9');
INSERT INTO `exp_wygwam_configs` VALUES(2, 'Full', 'YTo1OntzOjc6InRvb2xiYXIiO2E6Njc6e2k6MDtzOjY6IlNvdXJjZSI7aToxO3M6NDoiU2F2ZSI7aToyO3M6NzoiTmV3UGFnZSI7aTozO3M6NzoiUHJldmlldyI7aTo0O3M6OToiVGVtcGxhdGVzIjtpOjU7czozOiJDdXQiO2k6NjtzOjQ6IkNvcHkiO2k6NztzOjU6IlBhc3RlIjtpOjg7czo5OiJQYXN0ZVRleHQiO2k6OTtzOjEzOiJQYXN0ZUZyb21Xb3JkIjtpOjEwO3M6NToiUHJpbnQiO2k6MTE7czoxMjoiU3BlbGxDaGVja2VyIjtpOjEyO3M6NToiU2NheXQiO2k6MTM7czo0OiJVbmRvIjtpOjE0O3M6NDoiUmVkbyI7aToxNTtzOjQ6IkZpbmQiO2k6MTY7czo3OiJSZXBsYWNlIjtpOjE3O3M6OToiU2VsZWN0QWxsIjtpOjE4O3M6MTI6IlJlbW92ZUZvcm1hdCI7aToxOTtzOjQ6IkZvcm0iO2k6MjA7czo4OiJDaGVja2JveCI7aToyMTtzOjU6IlJhZGlvIjtpOjIyO3M6OToiVGV4dEZpZWxkIjtpOjIzO3M6ODoiVGV4dGFyZWEiO2k6MjQ7czo2OiJTZWxlY3QiO2k6MjU7czo2OiJCdXR0b24iO2k6MjY7czoxMToiSW1hZ2VCdXR0b24iO2k6Mjc7czoxMToiSGlkZGVuRmllbGQiO2k6Mjg7czoxOiIvIjtpOjI5O3M6NDoiQm9sZCI7aTozMDtzOjY6Ikl0YWxpYyI7aTozMTtzOjk6IlVuZGVybGluZSI7aTozMjtzOjY6IlN0cmlrZSI7aTozMztzOjk6IlN1YnNjcmlwdCI7aTozNDtzOjExOiJTdXBlcnNjcmlwdCI7aTozNTtzOjEyOiJOdW1iZXJlZExpc3QiO2k6MzY7czoxMjoiQnVsbGV0ZWRMaXN0IjtpOjM3O3M6NzoiT3V0ZGVudCI7aTozODtzOjY6IkluZGVudCI7aTozOTtzOjEwOiJCbG9ja3F1b3RlIjtpOjQwO3M6OToiQ3JlYXRlRGl2IjtpOjQxO3M6MTE6Ikp1c3RpZnlMZWZ0IjtpOjQyO3M6MTM6Ikp1c3RpZnlDZW50ZXIiO2k6NDM7czoxMjoiSnVzdGlmeVJpZ2h0IjtpOjQ0O3M6MTI6Ikp1c3RpZnlCbG9jayI7aTo0NTtzOjQ6IkxpbmsiO2k6NDY7czo2OiJVbmxpbmsiO2k6NDc7czo2OiJBbmNob3IiO2k6NDg7czo1OiJJbWFnZSI7aTo0OTtzOjU6IkZsYXNoIjtpOjUwO3M6NToiVGFibGUiO2k6NTE7czoxNDoiSG9yaXpvbnRhbFJ1bGUiO2k6NTI7czo2OiJTbWlsZXkiO2k6NTM7czoxMToiU3BlY2lhbENoYXIiO2k6NTQ7czo5OiJQYWdlQnJlYWsiO2k6NTU7czo4OiJSZWFkTW9yZSI7aTo1NjtzOjEwOiJFbWJlZE1lZGlhIjtpOjU3O3M6MToiLyI7aTo1ODtzOjY6IlN0eWxlcyI7aTo1OTtzOjY6IkZvcm1hdCI7aTo2MDtzOjQ6IkZvbnQiO2k6NjE7czo4OiJGb250U2l6ZSI7aTo2MjtzOjk6IlRleHRDb2xvciI7aTo2MztzOjc6IkJHQ29sb3IiO2k6NjQ7czo4OiJNYXhpbWl6ZSI7aTo2NTtzOjEwOiJTaG93QmxvY2tzIjtpOjY2O3M6NToiQWJvdXQiO31zOjY6ImhlaWdodCI7czozOiIyMDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ==');
INSERT INTO `exp_wygwam_configs` VALUES(3, 'News', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTE6e2k6MDtzOjY6IkZvcm1hdCI7aToxO3M6NDoiQm9sZCI7aToyO3M6NjoiSXRhbGljIjtpOjM7czo5OiJVbmRlcmxpbmUiO2k6NDtzOjY6IlN0cmlrZSI7aTo1O3M6MTI6Ik51bWJlcmVkTGlzdCI7aTo2O3M6MTI6IkJ1bGxldGVkTGlzdCI7aTo3O3M6NDoiTGluayI7aTo4O3M6NjoiVW5saW5rIjtpOjk7czo2OiJBbmNob3IiO2k6MTA7czo1OiJBYm91dCI7fXM6NjoiaGVpZ2h0IjtzOjM6IjIwMCI7czoxNDoicmVzaXplX2VuYWJsZWQiO3M6MToieSI7czoxMToiY29udGVudHNDc3MiO2E6MDp7fXM6MTA6InVwbG9hZF9kaXIiO3M6MDoiIjt9');
INSERT INTO `exp_wygwam_configs` VALUES(4, 'Featured Products Content', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTg6e2k6MDtzOjY6IkZvcm1hdCI7aToxO3M6MTE6Ikp1c3RpZnlMZWZ0IjtpOjI7czoxMzoiSnVzdGlmeUNlbnRlciI7aTozO3M6MTI6Ikp1c3RpZnlSaWdodCI7aTo0O3M6MTI6Ikp1c3RpZnlCbG9jayI7aTo1O3M6NDoiQm9sZCI7aTo2O3M6NjoiSXRhbGljIjtpOjc7czo5OiJVbmRlcmxpbmUiO2k6ODtzOjY6IlN0cmlrZSI7aTo5O3M6MTI6Ik51bWJlcmVkTGlzdCI7aToxMDtzOjEyOiJCdWxsZXRlZExpc3QiO2k6MTE7czo1OiJJbWFnZSI7aToxMjtzOjU6IkZsYXNoIjtpOjEzO3M6MTA6IkVtYmVkTWVkaWEiO2k6MTQ7czo1OiJBYm91dCI7aToxNTtzOjQ6IkxpbmsiO2k6MTY7czo2OiJVbmxpbmsiO2k6MTc7czo2OiJBbmNob3IiO31zOjY6ImhlaWdodCI7czozOiIyMDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ==');
INSERT INTO `exp_wygwam_configs` VALUES(5, 'Synergy Default', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTk6e2k6MDtzOjY6IkZvcm1hdCI7aToxO3M6NDoiQm9sZCI7aToyO3M6NjoiSXRhbGljIjtpOjM7czo5OiJVbmRlcmxpbmUiO2k6NDtzOjY6IlN0cmlrZSI7aTo1O3M6MTE6Ikp1c3RpZnlMZWZ0IjtpOjY7czoxMzoiSnVzdGlmeUNlbnRlciI7aTo3O3M6MTI6Ikp1c3RpZnlSaWdodCI7aTo4O3M6MTI6Ikp1c3RpZnlCbG9jayI7aTo5O3M6MTI6Ik51bWJlcmVkTGlzdCI7aToxMDtzOjEyOiJCdWxsZXRlZExpc3QiO2k6MTE7czo0OiJMaW5rIjtpOjEyO3M6NjoiVW5saW5rIjtpOjEzO3M6NjoiQW5jaG9yIjtpOjE0O3M6NToiSW1hZ2UiO2k6MTU7czo1OiJGbGFzaCI7aToxNjtzOjE0OiJIb3Jpem9udGFsUnVsZSI7aToxNztzOjEwOiJFbWJlZE1lZGlhIjtpOjE4O3M6NToiQWJvdXQiO31zOjY6ImhlaWdodCI7czozOiI0MDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ==');


/* Table structure for table `exp_accessories` */
DROP TABLE IF EXISTS `exp_accessories`;

CREATE TABLE `exp_accessories` (
  `accessory_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(75) NOT NULL DEFAULT '',
  `member_groups` varchar(50) NOT NULL DEFAULT 'all',
  `controllers` text,
  `accessory_version` varchar(12) NOT NULL,
  PRIMARY KEY (`accessory_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_accessories` */
INSERT INTO `exp_accessories` VALUES(1, 'Expressionengine_info_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.0');
INSERT INTO `exp_accessories` VALUES(2, 'Mx_cloner_acc', '1|5', 'addons|addons_accessories|addons_extensions|addons_fieldtypes|addons_modules|addons_plugins|admin_content|admin_system|content|content_edit|content_files|content_files_modal|content_publish|design|homepage|members|myaccount|sites|tools|tools_communicate|tools_data|tools_logs|tools_utilities', '1.0.3');

/* Table structure for table `exp_actions` */
DROP TABLE IF EXISTS `exp_actions`;

CREATE TABLE `exp_actions` (
  `action_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  PRIMARY KEY (`action_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_actions` */
INSERT INTO `exp_actions` VALUES(1, 'Comment', 'insert_new_comment');
INSERT INTO `exp_actions` VALUES(2, 'Comment_mcp', 'delete_comment_notification');
INSERT INTO `exp_actions` VALUES(3, 'Comment', 'comment_subscribe');
INSERT INTO `exp_actions` VALUES(4, 'Comment', 'edit_comment');
INSERT INTO `exp_actions` VALUES(5, 'Email', 'send_email');
INSERT INTO `exp_actions` VALUES(6, 'Safecracker', 'submit_entry');
INSERT INTO `exp_actions` VALUES(7, 'Safecracker', 'combo_loader');
INSERT INTO `exp_actions` VALUES(8, 'Search', 'do_search');
INSERT INTO `exp_actions` VALUES(9, 'Channel', 'insert_new_entry');
INSERT INTO `exp_actions` VALUES(10, 'Channel', 'filemanager_endpoint');
INSERT INTO `exp_actions` VALUES(11, 'Channel', 'smiley_pop');
INSERT INTO `exp_actions` VALUES(12, 'Member', 'registration_form');
INSERT INTO `exp_actions` VALUES(13, 'Member', 'register_member');
INSERT INTO `exp_actions` VALUES(14, 'Member', 'activate_member');
INSERT INTO `exp_actions` VALUES(15, 'Member', 'member_login');
INSERT INTO `exp_actions` VALUES(16, 'Member', 'member_logout');
INSERT INTO `exp_actions` VALUES(17, 'Member', 'retrieve_password');
INSERT INTO `exp_actions` VALUES(18, 'Member', 'reset_password');
INSERT INTO `exp_actions` VALUES(19, 'Member', 'send_member_email');
INSERT INTO `exp_actions` VALUES(20, 'Member', 'update_un_pw');
INSERT INTO `exp_actions` VALUES(21, 'Member', 'member_search');
INSERT INTO `exp_actions` VALUES(22, 'Member', 'member_delete');
INSERT INTO `exp_actions` VALUES(23, 'Rte', 'get_js');
INSERT INTO `exp_actions` VALUES(24, 'Playa_mcp', 'filter_entries');
INSERT INTO `exp_actions` VALUES(25, 'Freeform', 'save_form');

/* Table structure for table `exp_captcha` */
DROP TABLE IF EXISTS `exp_captcha`;

CREATE TABLE `exp_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_categories` */
DROP TABLE IF EXISTS `exp_categories`;

CREATE TABLE `exp_categories` (
  `cat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `parent_id` int(4) unsigned NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_url_title` varchar(75) NOT NULL,
  `cat_description` text,
  `cat_image` varchar(120) DEFAULT NULL,
  `cat_order` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `group_id` (`group_id`),
  KEY `cat_name` (`cat_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_categories` */
INSERT INTO `exp_categories` VALUES(1, 1, 1, 0, 'UAV', 'uav', '', '{filedir_3}cat-image.jpg', 3);
INSERT INTO `exp_categories` VALUES(2, 1, 1, 0, 'Optical', 'optical', 'Category description here', '{filedir_3}cat-image.jpg', 2);
INSERT INTO `exp_categories` VALUES(3, 1, 1, 2, 'Total Station', 'total-station', '', '{filedir_3}cat-image.jpg', 3);
INSERT INTO `exp_categories` VALUES(4, 1, 1, 3, 'Robotic', 'robotic', '', '{filedir_3}cat-image.jpg', 2);
INSERT INTO `exp_categories` VALUES(5, 1, 1, 3, 'Windows Based', 'windows-based', '', '{filedir_3}cat-image.jpg', 3);
INSERT INTO `exp_categories` VALUES(6, 1, 1, 3, 'Construction', 'construction', '', '{filedir_3}cat-image.jpg', 1);
INSERT INTO `exp_categories` VALUES(7, 1, 1, 0, 'GPS Equipment', 'gps-equipment', 'Lorem ipsum stuff', '{filedir_3}cat-image.jpg', 1);
INSERT INTO `exp_categories` VALUES(8, 1, 1, 2, 'Levels', 'levels', 'Test content', '{filedir_3}cat-image.jpg', 1);
INSERT INTO `exp_categories` VALUES(9, 1, 1, 2, 'Theyodolites', 'theyodolites', 'Test content', '{filedir_3}cat-image.jpg', 2);
INSERT INTO `exp_categories` VALUES(10, 1, 2, 0, 'Roads And Surfaces', 'roads-and-surfaces', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim. ', '{filedir_3}services-temp-top.jpg', 3);
INSERT INTO `exp_categories` VALUES(11, 1, 2, 0, 'Mapping', 'mapping', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim. ', '{filedir_3}services-temp-top.jpg', 2);
INSERT INTO `exp_categories` VALUES(12, 1, 2, 0, 'Lorem ipsum', 'lorem-ipsum', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim. ', '{filedir_3}services-temp-top.jpg', 1);

/* Table structure for table `exp_category_field_data` */
DROP TABLE IF EXISTS `exp_category_field_data`;

CREATE TABLE `exp_category_field_data` (
  `cat_id` int(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_category_field_data` */
INSERT INTO `exp_category_field_data` VALUES(1, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(2, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(3, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(4, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(5, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(6, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(7, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(8, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(9, 1, 1);
INSERT INTO `exp_category_field_data` VALUES(10, 1, 2);
INSERT INTO `exp_category_field_data` VALUES(11, 1, 2);
INSERT INTO `exp_category_field_data` VALUES(12, 1, 2);

/* Table structure for table `exp_category_fields` */
DROP TABLE IF EXISTS `exp_category_fields`;

CREATE TABLE `exp_category_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `field_label` varchar(50) NOT NULL DEFAULT '',
  `field_type` varchar(12) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_maxl` smallint(3) NOT NULL DEFAULT '128',
  `field_ta_rows` tinyint(2) NOT NULL DEFAULT '8',
  `field_default_fmt` varchar(40) NOT NULL DEFAULT 'none',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`field_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_category_groups` */
DROP TABLE IF EXISTS `exp_category_groups`;

CREATE TABLE `exp_category_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `sort_order` char(1) NOT NULL DEFAULT 'a',
  `exclude_group` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `field_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `can_edit_categories` text,
  `can_delete_categories` text,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_category_groups` */
INSERT INTO `exp_category_groups` VALUES(1, 1, 'Product Categories', 'a', 0, 'all', '', '');
INSERT INTO `exp_category_groups` VALUES(2, 1, 'Service Categories', 'a', 0, 'all', '', '');

/* Table structure for table `exp_category_posts` */
DROP TABLE IF EXISTS `exp_category_posts`;

CREATE TABLE `exp_category_posts` (
  `entry_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_category_posts` */
INSERT INTO `exp_category_posts` VALUES(1, 2);
INSERT INTO `exp_category_posts` VALUES(1, 3);
INSERT INTO `exp_category_posts` VALUES(1, 6);
INSERT INTO `exp_category_posts` VALUES(2, 2);
INSERT INTO `exp_category_posts` VALUES(2, 3);
INSERT INTO `exp_category_posts` VALUES(2, 6);
INSERT INTO `exp_category_posts` VALUES(3, 2);
INSERT INTO `exp_category_posts` VALUES(3, 3);
INSERT INTO `exp_category_posts` VALUES(3, 6);
INSERT INTO `exp_category_posts` VALUES(26, 10);

/* Table structure for table `exp_channel_data` */
DROP TABLE IF EXISTS `exp_channel_data`;

CREATE TABLE `exp_channel_data` (
  `entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `field_id_1` text,
  `field_ft_1` tinytext,
  `field_id_2` text,
  `field_ft_2` tinytext,
  `field_id_3` text,
  `field_ft_3` tinytext,
  `field_id_4` text,
  `field_ft_4` tinytext,
  `field_id_5` text,
  `field_ft_5` tinytext,
  `field_id_6` text,
  `field_ft_6` tinytext,
  `field_id_7` text,
  `field_ft_7` tinytext,
  `field_id_8` text,
  `field_ft_8` tinytext,
  `field_id_9` text,
  `field_ft_9` tinytext,
  `field_id_10` text,
  `field_ft_10` tinytext,
  `field_id_11` text,
  `field_ft_11` tinytext,
  `field_id_12` text,
  `field_ft_12` tinytext,
  `field_id_13` text,
  `field_ft_13` tinytext,
  `field_id_14` text,
  `field_ft_14` tinytext,
  `field_id_15` text,
  `field_ft_15` tinytext,
  `field_id_16` int(10) DEFAULT '0',
  `field_ft_16` tinytext,
  `field_id_17` text,
  `field_ft_17` tinytext,
  `field_id_18` text,
  `field_ft_18` tinytext,
  `field_id_19` text,
  `field_ft_19` tinytext,
  `field_id_20` text,
  `field_ft_20` tinytext,
  `field_id_22` text,
  `field_ft_22` tinytext,
  `field_id_23` text,
  `field_ft_23` tinytext,
  `field_id_24` text,
  `field_ft_24` tinytext,
  `field_id_25` text,
  `field_ft_25` tinytext,
  `field_id_26` text,
  `field_ft_26` tinytext,
  `field_id_27` text,
  `field_ft_27` tinytext,
  `field_id_28` text,
  `field_ft_28` tinytext,
  `field_id_29` text,
  `field_ft_29` tinytext,
  `field_id_30` text,
  `field_ft_30` tinytext,
  `field_id_31` text,
  `field_ft_31` tinytext,
  `field_id_32` text,
  `field_ft_32` tinytext,
  `field_id_33` text,
  `field_ft_33` tinytext,
  `field_id_34` text,
  `field_ft_34` tinytext,
  `field_id_35` text,
  `field_ft_35` tinytext,
  `field_id_36` text,
  `field_ft_36` tinytext,
  `field_id_37` int(10) DEFAULT '0',
  `field_ft_37` tinytext,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_channel_data` */
INSERT INTO `exp_channel_data` VALUES(1, 1, 1, 'NZD\nAUD\n', 'none', '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', 'Yes', 'none', '1', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(2, 1, 1, 'NZD\nAUD\n', 'none', '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', 'Yes', 'none', '1', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '{filedir_1}mega-menu-feature-product-image_(1).jpg', 'none', '', 'none', '', 'none', '', 'none', 5, 'none');
INSERT INTO `exp_channel_data` VALUES(3, 1, 1, 'NZD\nAUD\n', 'none', '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '1', 'none', '', 'none', '', 'none', '1', 'none', '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(4, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(5, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(6, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(7, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(8, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(9, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(10, 1, 3, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_4}news-temp-top.jpg', 'none', '', 'none', '', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(11, 1, 4, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', 'none', '', null, '', null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quam justo, egestas sed tincidunt id, feugiat eu urna. Suspendisse ornare, magna cursus tristique ullamcorper, massa velit ultricies metus, eu ultrices massa eros nec tortor. Sed auctor vehicula quam, ac condimentum mauris tristique non. Sed sed sodales nisi. Aliquam erat volutpat. Vivamus sit amet felis et urna lacinia ullamcorper eget et metus. Nunc laoreet eleifend molestie.</p>', 'none', '1', 'none', 0, 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(12, 1, 2, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 1, 'none', '1', 'none', 'The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(13, 1, 2, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 2, 'none', '1', 'none', 'The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(14, 1, 2, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 3, 'none', '1', 'none', 'The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(15, 1, 2, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 4, 'none', '1', 'none', 'The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(16, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(17, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(18, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(19, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(20, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(21, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(22, 1, 5, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus.</p>\n<p>\n	Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>\n<h2>\n	Lorem ipsum dolor sit</h2>\n<p>\n	&nbsp;</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>\n<p>\n	Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>', 'none', '{filedir_5}news-temp-top_(3).jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(23, 1, 8, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '1', 'none', '1', 'none', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non felis dignissim leo faucibus commodo. Aliquam urna velit, sollicitudin eget mollis ac, condimentum quis sapien. Aliquam aliquam vestibulum libero. Donec scelerisque lorem sollicitudin leo rhoncus eget dignissim felis blandit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ', 'none', '{filedir_6}services.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(24, 1, 8, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '1', 'none', '1', 'none', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non felis dignissim leo faucibus commodo. Aliquam urna velit, sollicitudin eget mollis ac, condimentum quis sapien. Aliquam aliquam vestibulum libero. Donec scelerisque lorem sollicitudin leo rhoncus eget dignissim felis blandit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ', 'none', '{filedir_6}products.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(25, 1, 8, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '1', 'none', '1', 'none', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non felis dignissim leo faucibus commodo. Aliquam urna velit, sollicitudin eget mollis ac, condimentum quis sapien. Aliquam aliquam vestibulum libero. Donec scelerisque lorem sollicitudin leo rhoncus eget dignissim felis blandit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ', 'none', '{filedir_6}about.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(26, 1, 7, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '1', 'none', '{filedir_7}services-temp-top.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(27, 1, 10, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<h1>\n	<strong style=\"font-size: 12px;\">Synergy Positioning Systems Ltd is 100% New Zealand owned and operated. It was formed when two of the industry&rsquo;s most trusted companies merged in 2006 - Synergy Equipment and Systems Ltd and Geodetic Instruments Ltd. The result is a company that brings you over 30 years experience delivering the most advanced and innovative technologies combined with a commitment to outstanding customer service.</strong></h1>\n<p>\n	Our aim is to make your job faster and easier, more accurate and cost efficient. We&#39;ll do this by giving you expert and honest advice and by offering you the best solution from our range of innovative positioning systems.<br />\n	Reliability is paramount - equipment breakages cost time and money. There are many low quality brands on the market that just don&#39;t stand up to the rigours of our construction industry. We only sell reliable brands we can trust and service.</p>\n<p>\n	Our experienced team are always on hand for after sales support, product training and servicing, giving you the confidence and skills to get the best out of your equipment. Our product range is extensive and always growing.</p>\n<h2>\n	Why choose synergy?</h2>', 'none', '1', 'none', '{filedir_8}about-temp.jpg', 'none', '', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(28, 1, 9, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '<p>\n	<strong>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</strong></p>\n<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet.</p>', 'none', '', null, '', null, '', null, '1', 'none', '', 'none', '', 'none', '', 'none', '', 'none', 0, 'none');
INSERT INTO `exp_channel_data` VALUES(29, 1, 11, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, 0, null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '', null, '{filedir_10}footer-topcon.gif', 'none', '{filedir_10}topcon-logo.gif', 'none', '{filedir_10}mega-menu-feature-product-logo.jpg', 'none', 0, 'none');

/* Table structure for table `exp_channel_entries_autosave` */
DROP TABLE IF EXISTS `exp_channel_entries_autosave`;

CREATE TABLE `exp_channel_entries_autosave` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `original_entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  `entry_data` text,
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_channel_fields` */
DROP TABLE IF EXISTS `exp_channel_fields`;

CREATE TABLE `exp_channel_fields` (
  `field_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL,
  `field_label` varchar(50) NOT NULL,
  `field_instructions` text,
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `field_list_items` text NOT NULL,
  `field_pre_populate` char(1) NOT NULL DEFAULT 'n',
  `field_pre_channel_id` int(6) unsigned DEFAULT NULL,
  `field_pre_field_id` int(6) unsigned DEFAULT NULL,
  `field_related_to` varchar(12) NOT NULL DEFAULT 'channel',
  `field_related_id` int(6) unsigned NOT NULL DEFAULT '0',
  `field_related_orderby` varchar(12) NOT NULL DEFAULT 'date',
  `field_related_sort` varchar(4) NOT NULL DEFAULT 'desc',
  `field_related_max` smallint(4) NOT NULL DEFAULT '0',
  `field_ta_rows` tinyint(2) DEFAULT '8',
  `field_maxl` smallint(3) DEFAULT NULL,
  `field_required` char(1) NOT NULL DEFAULT 'n',
  `field_text_direction` char(3) NOT NULL DEFAULT 'ltr',
  `field_search` char(1) NOT NULL DEFAULT 'n',
  `field_is_hidden` char(1) NOT NULL DEFAULT 'n',
  `field_fmt` varchar(40) NOT NULL DEFAULT 'xhtml',
  `field_show_fmt` char(1) NOT NULL DEFAULT 'y',
  `field_order` int(3) unsigned NOT NULL,
  `field_content_type` varchar(20) NOT NULL DEFAULT 'any',
  `field_settings` text,
  PRIMARY KEY (`field_id`),
  KEY `group_id` (`group_id`),
  KEY `field_type` (`field_type`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_channel_fields` */
INSERT INTO `exp_channel_fields` VALUES(1, 1, 1, 'product_prices', 'Product Prices', 'Enter the AU and NZ prices for this product in here and they will be dynamically output to the customer based on the website they are viewing.', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjIiO3M6ODoibWF4X3Jvd3MiO3M6MToiMiI7czo3OiJjb2xfaWRzIjthOjI6e2k6MDtzOjE6IjEiO2k6MTtzOjE6IjIiO319');
INSERT INTO `exp_channel_fields` VALUES(2, 1, 1, 'product_description', 'Product Description', 'On the product page, only the first couple of paragraphs will be shown until the user presses \"read more\"', 'wygwam', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIyIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(3, 1, 1, 'technical_specifications', 'Technical Specifications', 'Below you can create the technical specifications section for this product. Once a title is specified in the left column (i.e. Measurement Range) you can create a table in the right column of specifications relevant to this title. ', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MToiMyI7aToxO3M6MToiNCI7fX0=');
INSERT INTO `exp_channel_fields` VALUES(4, 1, 1, 'product_videos', 'Product Videos', 'Use this field to embed a Youtube video and give a title/description to the video. Any videos will appear under the \"Videos\" tab for this product.', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 4, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mzp7aTowO2k6NjtpOjE7aTo3O2k6MjtpOjg7fX0=');
INSERT INTO `exp_channel_fields` VALUES(5, 1, 1, 'rental_sections', 'Rental Sections', 'Add as many paragraphs to the rental section as you want.', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO2k6OTtpOjE7aToxMDt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(6, 1, 1, 'rental_pricing_nz', 'Rental Pricing (NZ)', 'This field controls the contents of the \"Availability & Costs\" section under the rental tab (for the NZ site).', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 6, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMTEiO2k6MTtzOjI6IjEyIjt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(7, 1, 1, 'rental_pricing_au', 'Rental Pricing (AU)', 'This field controls the contents of the \"Availability & Costs\" section under the rental tab (for the AU site).', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 7, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMTMiO2k6MTtzOjI6IjE0Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(8, 1, 1, 'featured_carousel_images', 'Featured Carousel Images', 'If this product is to be a featured product, you can upload many banner images to appear at the top of its featured page.\n\nThe banner will cycle between these images.', 'matrix', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 8, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMTUiO2k6MTtzOjI6IjI0Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(9, 1, 1, 'is_featured_product', 'Is Featured Product?', 'If you want this product to be a featured product, tick this checkbox.\n\nPlease make sure you have created some featured product entries for this product first (in the publish menu).', 'checkboxes', 'Yes', 'n', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 9, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');
INSERT INTO `exp_channel_fields` VALUES(10, 1, 1, 'product_images', 'Product Images', '', 'matrix', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 10, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6MTp7aTowO3M6MjoiMTYiO319');
INSERT INTO `exp_channel_fields` VALUES(11, 1, 1, 'product_downloads', 'Product Downloads', 'Upload files to be available in the \"downloads\" tab', 'matrix', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 11, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMTciO2k6MTtzOjI6IjE4Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(12, 1, 3, 'article_content', 'Article Content', 'This is the main body content of the news item. Please note the first paragraph will be automatically made bold.', 'wygwam', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(13, 1, 3, 'article_banner_image', 'Article Banner Image', 'This is the image that appears at the top of the news article and on the news page. Please note the images should be exactly 866px by 373px and will have a thumbnail automatically cropped to 396px by 88px for the news page.', 'file', '', '0', 0, 0, 'channel', 2, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiNCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(14, 1, 4, 'contact_introduction', 'Contact Introduction', 'The initial content before the branch information.', 'wygwam', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(15, 1, 4, 'branches', 'Branches', '', 'matrix', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6NTp7aTowO3M6MjoiMTkiO2k6MTtzOjI6IjIwIjtpOjI7czoyOiIyMSI7aTozO3M6MjoiMjIiO2k6NDtzOjI6IjIzIjt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(16, 1, 2, 'featured_product', 'Featured Product', 'Pick the featured product this sub page is related to.', 'rel', '', '0', 0, 0, 'channel', 1, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'y', 'n', 'none', 'n', 1, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');
INSERT INTO `exp_channel_fields` VALUES(17, 1, 2, 'featured_content', 'Featured Content', 'Each block of content you add will create a new block separated by a horizontal rule.', 'matrix', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6MTp7aTowO3M6MjoiMjUiO319');
INSERT INTO `exp_channel_fields` VALUES(18, 1, 2, 'featured_content_intro', 'Featured Content Intro', 'This is shown on the featured product page as the preview for this page.', 'textarea', '', '0', 0, 0, 'channel', 4, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');
INSERT INTO `exp_channel_fields` VALUES(19, 1, 5, 'study_content', 'Study Content', 'This is the main body content of the news item. Please note the first paragraph will be automatically made bold.', 'wygwam', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiIzIjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(20, 1, 5, 'study_banner_image', 'Study Banner Image', '', 'file', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiNSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(22, 1, 7, 'home_marketing_message', 'Home Marketing Message', 'This is the large and smaller text that appears over the top of the banner image.', 'matrix', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO2k6MjY7aToxO2k6Mjc7fX0=');
INSERT INTO `exp_channel_fields` VALUES(23, 1, 7, 'home_banner_links', 'Home Banner Links', 'This field controls the content and location of the home page banner links (both over the banner and below in the tabs).', 'matrix', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 0, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMjgiO2k6MTtzOjI6IjI5Ijt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(24, 1, 7, 'home_banner_tab_content', 'Home Banner Tab Content', 'The content of the tabs below the banners. A limit of 190 characters will be shown from this box.', 'textarea', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 3, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 4, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');
INSERT INTO `exp_channel_fields` VALUES(25, 1, 7, 'home_banner_image', 'Home Banner Image', 'This is the large background image shown in the banner.', 'file', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 5, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiNiI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(26, 1, 6, 'service_content', 'Service Content', 'Each block of content you add will create a new block separated by a horizontal rule.', 'matrix', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjEiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6MTp7aTowO2k6MzA7fX0=');
INSERT INTO `exp_channel_fields` VALUES(27, 1, 6, 'services_banner', 'Services Banner', 'An image with dimensions of 866px - 305px is best.', 'file', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiNyI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(28, 1, 8, 'management_page_content', 'Page Content', 'This is the content at the top of the management page.', 'wygwam', '', '0', 0, 0, 'channel', 5, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo5OntzOjc6ImNvbnZlcnQiO3M6MDoiIjtzOjY6ImNvbmZpZyI7czoxOiI1IjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(29, 1, 9, 'about_page_content', 'Page Content', '', 'wygwam', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjY6ImNvbmZpZyI7czoxOiI1IjtzOjU6ImRlZmVyIjtzOjE6Im4iO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(30, 1, 9, 'about_us_grid', 'About Us Grid', 'Add as many blocks as you want to the bottom of the about us page', 'matrix', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mjp7aTowO3M6MjoiMzIiO2k6MTtzOjI6IjMxIjt9fQ==');
INSERT INTO `exp_channel_fields` VALUES(31, 1, 9, 'about_page_banner', 'About Page Banner', 'This should be a banner image ideally 866px x 305px, however it may be larger. The image will be constricted width-wise to 866px.', 'file', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 3, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiOCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(32, 1, 8, 'staff_members', 'Staff Members', 'Enter a row for each staff member here.', 'matrix', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'y', 'ltr', 'n', 'n', 'none', 'n', 2, 'any', 'YTozOntzOjg6Im1pbl9yb3dzIjtzOjE6IjAiO3M6ODoibWF4X3Jvd3MiO3M6MDoiIjtzOjc6ImNvbF9pZHMiO2E6Mzp7aTowO3M6MjoiMzMiO2k6MTtzOjI6IjM0IjtpOjI7czoyOiIzNSI7fX0=');
INSERT INTO `exp_channel_fields` VALUES(33, 1, 1, 'featured_megamenu_icon', 'Featured Megamenu Icon', 'This is the small image that appears in the mega menu for features products. This needs to be an image with the dimensions 302px x 89px and will be constrained to this when displayed. Please note if this image is not supplied on a featured product, it will not be displayed in the featured section of the products megamenu.', 'file', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 12, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiMSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');
INSERT INTO `exp_channel_fields` VALUES(34, 1, 10, 'manufacturer_footer_image', 'Footer Image', 'This image is output in the footer. It should be a grey/white text on a background colour of #3c3c3c.\nIt must be 30px in height and no wider than 225px (it can be smaller).', 'file', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 1, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MjoiMTAiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(35, 1, 10, 'manufacturer_product_page_image', 'Product Page Image', 'This image is used on the product page when you select this manufacturer for the given product. The logo in the design is 140px wide and 24px high (so we would recommend something similar), however you can upload different sizes as long as they fit in your page.', 'file', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 2, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MjoiMTAiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(36, 1, 10, 'featured_product_megamenu_image', 'Featured Product Megamenu Image', 'This image is only ever output in the product mega menu if a product with this manufacturer is a featured product. This is a fixed 166px by 49px image and will be constrained to this. Please note if this is not set and this product is featured, the product will not show in the featured megamenu block.', 'file', '', '0', 0, 0, 'channel', 10, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 3, 'any', 'YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MjoiMTAiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ==');
INSERT INTO `exp_channel_fields` VALUES(37, 1, 1, 'manufacturer', 'Manufacturer', 'This field is used to display the manufacturer logo on the product page.', 'rel', '', '0', 0, 0, 'channel', 11, 'title', 'desc', 0, 6, 128, 'n', 'ltr', 'y', 'n', 'none', 'n', 13, 'any', 'YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30=');

/* Table structure for table `exp_channel_member_groups` */
DROP TABLE IF EXISTS `exp_channel_member_groups`;

CREATE TABLE `exp_channel_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `channel_id` int(6) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_channel_titles` */
DROP TABLE IF EXISTS `exp_channel_titles`;

CREATE TABLE `exp_channel_titles` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `pentry_id` int(10) NOT NULL DEFAULT '0',
  `forum_topic_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL DEFAULT 'n',
  `view_count_one` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_two` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_three` int(10) unsigned NOT NULL DEFAULT '0',
  `view_count_four` int(10) unsigned NOT NULL DEFAULT '0',
  `allow_comments` varchar(1) NOT NULL DEFAULT 'y',
  `sticky` varchar(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL DEFAULT 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL DEFAULT '0',
  `comment_expiration_date` int(10) NOT NULL DEFAULT '0',
  `edit_date` bigint(14) DEFAULT NULL,
  `recent_comment_date` int(10) DEFAULT NULL,
  `comment_total` int(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_channel_titles` */
INSERT INTO `exp_channel_titles` VALUES(1, 1, 1, 1, 0, null, '127.0.0.1', 'Topcon GTS-100N Construction', 'topcon-gts-100n-construction', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358293518, 'n', '2013', '01', '16', 0, 0, 20130411225519, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(2, 1, 1, 1, 0, null, '127.0.0.1', 'Topcon GTS-100N Robotic', 'topcon-gts-100n-robotic', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358293544, 'n', '2013', '01', '16', 0, 0, 20130412020645, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(3, 1, 1, 1, 0, null, '127.0.0.1', 'Topcon GTS-100N Windows Based', 'topcon-gts-100n-windows-based', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1358293500, 'n', '2013', '01', '16', 0, 0, 20130313211401, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(4, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 1', 'news-article-1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961462, 'n', '2013', '03', '11', 0, 0, 20130311132422, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(5, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 2', 'news-article-2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961474, 'n', '2013', '03', '11', 0, 0, 20130311132434, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(6, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 6', 'news-article-6', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961462, 'n', '2013', '03', '11', 0, 0, 20130311003823, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(7, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 5', 'news-article-5', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961483, 'n', '2013', '03', '11', 0, 0, 20130311132443, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(8, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 4', 'news-article-4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961442, 'n', '2013', '03', '11', 0, 0, 20130311132402, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(9, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 3', 'news-article-3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961452, 'n', '2013', '03', '11', 0, 0, 20130311132412, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(10, 1, 3, 1, 0, null, '127.0.0.1', 'News Article 7', 'news-article-7', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362961463, 'n', '2013', '03', '11', 0, 0, 20130311132423, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(11, 1, 4, 1, 0, null, '127.0.0.1', 'Contact Us', 'contact-us', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1362967324, 'n', '2013', '03', '11', 0, 0, 20130318040605, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(12, 1, 2, 1, 0, null, '127.0.0.1', 'BRAMOR Orthophoto', 'bramor-orthophoto', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363822454, 'n', '2013', '03', '21', 0, 0, 20130320235515, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(13, 1, 2, 1, 0, null, '127.0.0.1', 'BRAMOR Orthophoto 2', 'bramor-orthophoto-2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363822493, 'n', '2013', '03', '21', 0, 0, 20130321123453, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(14, 1, 2, 1, 0, null, '127.0.0.1', 'BRAMOR Orthophoto 3', 'bramor-orthophoto-3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363822446, 'n', '2013', '03', '21', 0, 0, 20130321123406, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(15, 1, 2, 1, 0, null, '127.0.0.1', 'BRAMOR Orthophoto 4', 'bramor-orthophoto-4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363822463, 'n', '2013', '03', '21', 0, 0, 20130321123423, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(16, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study', 'test-case-study', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903711, 'n', '2013', '03', '22', 0, 0, 20130321222932, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(17, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 1', 'test-case-study-1', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903722, 'n', '2013', '03', '22', 0, 0, 20130321222843, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(18, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 2', 'test-case-study-2', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903681, 'n', '2013', '03', '22', 0, 0, 20130321222902, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(19, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 3', 'test-case-study-3', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903703, 'n', '2013', '03', '22', 0, 0, 20130321222924, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(20, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 4', 'test-case-study-4', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903733, 'n', '2013', '03', '22', 0, 0, 20130321222854, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(21, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 5', 'test-case-study-5', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903718, 'n', '2013', '03', '22', 0, 0, 20130321222939, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(22, 1, 5, 1, 0, null, '127.0.0.1', 'Test Case Study 6', 'test-case-study-6', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1363903695, 'n', '2013', '03', '22', 0, 0, 20130321222916, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(23, 1, 8, 1, 0, null, '127.0.0.1', 'Services', 'services', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365459670, 'n', '2013', '04', '09', 0, 0, 20130409112110, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(24, 1, 8, 1, 0, null, '127.0.0.1', 'Products', 'products', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365460036, 'n', '2013', '04', '09', 0, 0, 20130409112716, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(25, 1, 8, 1, 0, null, '127.0.0.1', 'About Us.', 'about-us', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365460111, 'n', '2013', '04', '09', 0, 0, 20130409112831, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(26, 1, 7, 1, 0, null, '127.0.0.1', 'Survey & Design', 'survey-design', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365483859, 'n', '2013', '04', '09', 0, 0, 20130410025520, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(27, 1, 10, 1, 0, null, '127.0.0.1', 'About Us', 'about-us', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365633126, 'n', '2013', '04', '11', 0, 0, 20130410224507, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(28, 1, 9, 1, 0, null, '127.0.0.1', 'Management', 'management', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365635743, 'n', '2013', '04', '11', 0, 0, 20130410233744, 0, 0);
INSERT INTO `exp_channel_titles` VALUES(29, 1, 11, 1, 0, null, '127.0.0.1', 'Topcon', 'topcon', 'open', 'y', 0, 0, 0, 0, 'y', 'n', 1365731435, 'n', '2013', '04', '12', 0, 0, 20130412145035, 0, 0);

/* Table structure for table `exp_channels` */
DROP TABLE IF EXISTS `exp_channels`;

CREATE TABLE `exp_channels` (
  `channel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `channel_name` varchar(40) NOT NULL,
  `channel_title` varchar(100) NOT NULL,
  `channel_url` varchar(100) NOT NULL,
  `channel_description` varchar(225) DEFAULT NULL,
  `channel_lang` varchar(12) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `cat_group` varchar(255) DEFAULT NULL,
  `status_group` int(4) unsigned DEFAULT NULL,
  `deft_status` varchar(50) NOT NULL DEFAULT 'open',
  `field_group` int(4) unsigned DEFAULT NULL,
  `search_excerpt` int(4) unsigned DEFAULT NULL,
  `deft_category` varchar(60) DEFAULT NULL,
  `deft_comments` char(1) NOT NULL DEFAULT 'y',
  `channel_require_membership` char(1) NOT NULL DEFAULT 'y',
  `channel_max_chars` int(5) unsigned DEFAULT NULL,
  `channel_html_formatting` char(4) NOT NULL DEFAULT 'all',
  `channel_allow_img_urls` char(1) NOT NULL DEFAULT 'y',
  `channel_auto_link_urls` char(1) NOT NULL DEFAULT 'n',
  `channel_notify` char(1) NOT NULL DEFAULT 'n',
  `channel_notify_emails` varchar(255) DEFAULT NULL,
  `comment_url` varchar(80) DEFAULT NULL,
  `comment_system_enabled` char(1) NOT NULL DEFAULT 'y',
  `comment_require_membership` char(1) NOT NULL DEFAULT 'n',
  `comment_use_captcha` char(1) NOT NULL DEFAULT 'n',
  `comment_moderate` char(1) NOT NULL DEFAULT 'n',
  `comment_max_chars` int(5) unsigned DEFAULT '5000',
  `comment_timelock` int(5) unsigned NOT NULL DEFAULT '0',
  `comment_require_email` char(1) NOT NULL DEFAULT 'y',
  `comment_text_formatting` char(5) NOT NULL DEFAULT 'xhtml',
  `comment_html_formatting` char(4) NOT NULL DEFAULT 'safe',
  `comment_allow_img_urls` char(1) NOT NULL DEFAULT 'n',
  `comment_auto_link_urls` char(1) NOT NULL DEFAULT 'y',
  `comment_notify` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_authors` char(1) NOT NULL DEFAULT 'n',
  `comment_notify_emails` varchar(255) DEFAULT NULL,
  `comment_expiration` int(4) unsigned NOT NULL DEFAULT '0',
  `search_results_url` varchar(80) DEFAULT NULL,
  `ping_return_url` varchar(80) DEFAULT NULL,
  `show_button_cluster` char(1) NOT NULL DEFAULT 'y',
  `rss_url` varchar(80) DEFAULT NULL,
  `enable_versioning` char(1) NOT NULL DEFAULT 'n',
  `max_revisions` smallint(4) unsigned NOT NULL DEFAULT '10',
  `default_entry_title` varchar(100) DEFAULT NULL,
  `url_title_prefix` varchar(80) DEFAULT NULL,
  `live_look_template` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`channel_id`),
  KEY `cat_group` (`cat_group`),
  KEY `status_group` (`status_group`),
  KEY `field_group` (`field_group`),
  KEY `channel_name` (`channel_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_channels` */
INSERT INTO `exp_channels` VALUES(1, 1, 'products', 'Products', 'http://synergy.local/', null, 'en', 3, 0, 1358293544, 0, '1', 1, 'open', 1, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(2, 1, 'featured_product_pages', 'Featured Product Pages', 'http://synergy.local/', null, 'en', 4, 0, 1363822493, 0, '', 1, 'open', 2, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(3, 1, 'news', 'News', 'http://synergy.local/', null, 'en', 7, 0, 1362961483, 0, '', 1, 'open', 3, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(4, 1, 'contact_us', 'Contact Us', 'http://synergy.local/', null, 'en', 1, 0, 1362967324, 0, '', 1, 'open', 4, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(5, 1, 'case_studies', 'Case Studies', 'http://synergy.local/', null, 'en', 7, 0, 1363903733, 0, '', 1, 'open', 5, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(7, 1, 'services', 'Services', 'http://synergy.local/', null, 'en', 1, 0, 1365483859, 0, '2', 1, 'open', 6, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(8, 1, 'home_page_banners', 'Home Page Banners', 'http://synergy.local/', null, 'en', 3, 0, 1365460111, 0, '', 1, 'open', 7, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(9, 1, 'management', 'Management', 'http://synergy.local/', null, 'en', 1, 0, 1365635743, 0, '', 1, 'open', 8, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(10, 1, 'about_us', 'About Us', 'http://synergy.local/', null, 'en', 1, 0, 1365633126, 0, '', 1, 'open', 9, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);
INSERT INTO `exp_channels` VALUES(11, 1, 'manufacturers', 'Manufacturers', 'http://synergy.local/', null, 'en', 1, 0, 1365731435, 0, '', 1, 'open', 10, null, null, 'y', 'y', null, 'all', 'y', 'n', 'n', null, null, 'y', 'n', 'n', 'n', 5000, 0, 'y', 'xhtml', 'safe', 'n', 'y', 'n', 'n', null, 0, null, null, 'y', null, 'n', 10, '', '', 0);

/* Table structure for table `exp_comment_subscriptions` */
DROP TABLE IF EXISTS `exp_comment_subscriptions`;

CREATE TABLE `exp_comment_subscriptions` (
  `subscription_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned DEFAULT NULL,
  `member_id` int(10) DEFAULT '0',
  `email` varchar(50) DEFAULT NULL,
  `subscription_date` varchar(10) DEFAULT NULL,
  `notification_sent` char(1) DEFAULT 'n',
  `hash` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`subscription_id`),
  KEY `entry_id` (`entry_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_comments` */
DROP TABLE IF EXISTS `exp_comments`;

CREATE TABLE `exp_comments` (
  `comment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT '0',
  `channel_id` int(4) unsigned DEFAULT '1',
  `author_id` int(10) unsigned DEFAULT '0',
  `status` char(1) DEFAULT '0',
  `name` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `url` varchar(75) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `comment_date` int(10) DEFAULT NULL,
  `edit_date` int(10) DEFAULT NULL,
  `comment` text,
  PRIMARY KEY (`comment_id`),
  KEY `entry_id` (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `status` (`status`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_cp_log` */
DROP TABLE IF EXISTS `exp_cp_log`;

CREATE TABLE `exp_cp_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `username` varchar(32) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `act_date` int(10) NOT NULL,
  `action` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_cp_log` */
INSERT INTO `exp_cp_log` VALUES(1, 1, 1, '96black', '127.0.0.1', 1355881673, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(2, 1, 1, '96black', '127.0.0.1', 1355883062, 'Site Updated&nbsp;&nbsp;Synergy Positioning Systems (NZ)');
INSERT INTO `exp_cp_log` VALUES(3, 1, 1, '96black', '127.0.0.1', 1355883097, 'Site Created&nbsp;&nbsp;Synergy Positioning Systems (AU)');
INSERT INTO `exp_cp_log` VALUES(4, 2, 1, '96black', '127.0.0.1', 1355943021, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(5, 2, 1, '96black', '127.0.0.1', 1355945651, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(6, 1, 1, '96black', '127.0.0.1', 1356034419, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(7, 2, 1, '96black', '127.0.0.1', 1356039515, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(8, 2, 1, '96black', '127.0.0.1', 1356039525, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(9, 1, 1, '96black', '127.0.0.1', 1358202174, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(10, 1, 1, '96black', '127.0.0.1', 1358202207, 'Channel Created:&nbsp;&nbsp;Products');
INSERT INTO `exp_cp_log` VALUES(11, 1, 1, '96black', '127.0.0.1', 1358202239, 'Field Group Created:&nbsp;Products');
INSERT INTO `exp_cp_log` VALUES(12, 1, 1, '96black', '127.0.0.1', 1358210877, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(13, 1, 1, '96black', '127.0.0.1', 1358218127, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(14, 1, 1, '96black', '127.0.0.1', 1358218180, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(15, 1, 1, '96black', '127.0.0.1', 1358218184, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(16, 1, 1, '96black', '127.0.0.1', 1358218208, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(17, 1, 1, '96black', '127.0.0.1', 1358220028, 'Channel Created:&nbsp;&nbsp;Featured Product Pages');
INSERT INTO `exp_cp_log` VALUES(18, 1, 1, '96black', '127.0.0.1', 1358220074, 'Field Group Created:&nbsp;Featured Product Pages');
INSERT INTO `exp_cp_log` VALUES(19, 1, 1, '96black', '127.0.0.1', 1358220742, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(20, 1, 1, '96black', '127.0.0.1', 1358274914, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(21, 1, 1, '96black', '127.0.0.1', 1358279266, 'Category Group Created:&nbsp;&nbsp;Product Categories');
INSERT INTO `exp_cp_log` VALUES(22, 1, 1, '96black', '127.0.0.1', 1358293233, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(23, 1, 1, '96black', '127.0.0.1', 1358362941, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(24, 1, 1, '96black', '127.0.0.1', 1359494506, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(25, 1, 1, '96black', '127.0.0.1', 1362948766, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(26, 1, 1, '96black', '127.0.0.1', 1362960488, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(27, 1, 1, '96black', '127.0.0.1', 1362960564, 'Channel Created:&nbsp;&nbsp;News');
INSERT INTO `exp_cp_log` VALUES(28, 1, 1, '96black', '127.0.0.1', 1362960570, 'Field Group Created:&nbsp;News');
INSERT INTO `exp_cp_log` VALUES(29, 1, 1, '96black', '127.0.0.1', 1362966730, 'Field Group Created:&nbsp;Contact Us');
INSERT INTO `exp_cp_log` VALUES(30, 1, 1, '96black', '127.0.0.1', 1362966734, 'Channel Created:&nbsp;&nbsp;Contact Us');
INSERT INTO `exp_cp_log` VALUES(31, 1, 1, '96black', '127.0.0.1', 1362974544, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(32, 1, 1, '96black', '127.0.0.1', 1362976053, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(33, 1, 1, '96black', '127.0.0.1', 1362976069, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(34, 1, 1, '96black', '127.0.0.1', 1362976093, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(35, 1, 1, '96black', '127.0.0.1', 1362992592, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(36, 1, 1, '96black', '127.0.0.1', 1363143606, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(37, 1, 1, '96black', '127.0.0.1', 1363209017, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(38, 1, 1, '96black', '127.0.0.1', 1363320699, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(39, 1, 1, '96black', '127.0.0.1', 1363567001, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(40, 1, 1, '96black', '127.0.0.1', 1363577373, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(41, 1, 1, '96black', '127.0.0.1', 1363578978, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(42, 1, 1, '96black', '127.0.0.1', 1363579481, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(43, 1, 1, '96black', '127.0.0.1', 1363660090, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(44, 1, 1, '96black', '127.0.0.1', 1363729623, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(45, 1, 1, '96black', '127.0.0.1', 1363736810, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(46, 1, 1, '96black', '127.0.0.1', 1363739939, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(47, 1, 1, '96black', '127.0.0.1', 1363739963, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(48, 1, 1, '96black', '127.0.0.1', 1363747179, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(49, 1, 1, '96black', '127.0.0.1', 1363747203, 'Category Group Created:&nbsp;&nbsp;Service Categories');
INSERT INTO `exp_cp_log` VALUES(50, 1, 1, '96black', '127.0.0.1', 1363820443, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(51, 1, 1, '96black', '127.0.0.1', 1363829155, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(52, 1, 1, '96black', '127.0.0.1', 1363833477, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(53, 1, 1, '96black', '127.0.0.1', 1363840598, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(54, 1, 1, '96black', '127.0.0.1', 1363902966, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(55, 1, 1, '96black', '127.0.0.1', 1363903295, 'Channel Created:&nbsp;&nbsp;Case Studies');
INSERT INTO `exp_cp_log` VALUES(56, 1, 1, '96black', '127.0.0.1', 1363903335, 'Field Group Created:&nbsp;Case Studies');
INSERT INTO `exp_cp_log` VALUES(57, 1, 1, '96black', '127.0.0.1', 1364159867, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(58, 1, 1, '96black', '127.0.0.1', 1364160031, 'Channel Created:&nbsp;&nbsp;Servoces');
INSERT INTO `exp_cp_log` VALUES(59, 1, 1, '96black', '127.0.0.1', 1364160036, 'Field Group Created:&nbsp;Services');
INSERT INTO `exp_cp_log` VALUES(60, 1, 1, '96black', '127.0.0.1', 1364160044, 'Channel Deleted:&nbsp;&nbsp;Servoces');
INSERT INTO `exp_cp_log` VALUES(61, 1, 1, '96black', '127.0.0.1', 1364160059, 'Channel Created:&nbsp;&nbsp;Services');
INSERT INTO `exp_cp_log` VALUES(62, 1, 1, '96black', '127.0.0.1', 1364160673, 'Channel Created:&nbsp;&nbsp;Home Page Banners');
INSERT INTO `exp_cp_log` VALUES(63, 1, 1, '96black', '127.0.0.1', 1364160678, 'Field Group Created:&nbsp;Home Page Banners');
INSERT INTO `exp_cp_log` VALUES(64, 1, 1, '96black', '127.0.0.1', 1365458478, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(65, 1, 1, '96black', '127.0.0.1', 1365459691, 'Custom Field Deleted:&nbsp;Home Main Heading');
INSERT INTO `exp_cp_log` VALUES(66, 1, 1, '96black', '127.0.0.1', 1365466907, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(67, 1, 1, '96black', '127.0.0.1', 1365467178, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(68, 1, 1, '96black', '127.0.0.1', 1365467422, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(69, 1, 1, '96black', '127.0.0.1', 1365467428, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(70, 1, 1, '96black', '127.0.0.1', 1365467460, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(71, 1, 1, '96black', '127.0.0.1', 1365467784, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(72, 1, 1, '96black', '127.0.0.1', 1365467819, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(73, 1, 1, '96black', '127.0.0.1', 1365467842, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(74, 1, 1, '96black', '127.0.0.1', 1365471782, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(75, 1, 1, '96black', '127.0.0.1', 1365472101, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(76, 1, 1, '96black', '127.0.0.1', 1365479239, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(77, 1, 1, '96black', '127.0.0.1', 1365479505, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(78, 1, 1, '96black', '127.0.0.1', 1365479533, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(79, 1, 1, '96black', '127.0.0.1', 1365558230, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(80, 1, 1, '96black', '127.0.0.1', 1365630778, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(81, 1, 1, '96black', '127.0.0.1', 1365630802, 'Channel Created:&nbsp;&nbsp;Management');
INSERT INTO `exp_cp_log` VALUES(82, 1, 1, '96black', '127.0.0.1', 1365630809, 'Field Group Created:&nbsp;Management');
INSERT INTO `exp_cp_log` VALUES(83, 1, 1, '96black', '127.0.0.1', 1365632300, 'Channel Created:&nbsp;&nbsp;About Us');
INSERT INTO `exp_cp_log` VALUES(84, 1, 1, '96black', '127.0.0.1', 1365632892, 'Field Group Created:&nbsp;About Us');
INSERT INTO `exp_cp_log` VALUES(85, 1, 1, '96black', '127.0.0.1', 1365641498, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(86, 1, 1, '96black', '127.0.0.1', 1365644175, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(87, 1, 1, '96black', '127.0.0.1', 1365653399, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(88, 1, 1, '96black', '127.0.0.1', 1365708737, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(89, 1, 1, '96black', '127.0.0.1', 1365725809, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(90, 1, 1, '96black', '127.0.0.1', 1365728123, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(91, 1, 1, '96black', '127.0.0.1', 1365729769, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(92, 1, 1, '96black', '127.0.0.1', 1365729854, 'Channel Created:&nbsp;&nbsp;Manufacturers');
INSERT INTO `exp_cp_log` VALUES(93, 1, 1, '96black', '127.0.0.1', 1365729858, 'Field Group Created:&nbsp;Manufacturers');
INSERT INTO `exp_cp_log` VALUES(94, 1, 1, '96black', '127.0.0.1', 1365737079, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(95, 1, 1, '96black', '127.0.0.1', 1365741053, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(96, 1, 1, '96black', '127.0.0.1', 1365741080, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(97, 1, 1, '96black', '127.0.0.1', 1365845178, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(98, 2, 1, '96black', '127.0.0.1', 1365848014, 'Logged out');
INSERT INTO `exp_cp_log` VALUES(99, 2, 1, '96black', '127.0.0.1', 1365848029, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(100, 1, 1, '96black', '127.0.0.1', 1365848058, 'Logged in');
INSERT INTO `exp_cp_log` VALUES(101, 2, 1, '96black', '127.0.0.1', 1365929769, 'Logged in');

/* Table structure for table `exp_cp_search_index` */
DROP TABLE IF EXISTS `exp_cp_search_index`;

CREATE TABLE `exp_cp_search_index` (
  `search_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(20) DEFAULT NULL,
  `method` varchar(50) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `access` varchar(50) DEFAULT NULL,
  `keywords` text,
  PRIMARY KEY (`search_id`),
  FULLTEXT KEY `keywords` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


/* Table structure for table `exp_developer_log` */
DROP TABLE IF EXISTS `exp_developer_log`;

CREATE TABLE `exp_developer_log` (
  `log_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(10) unsigned NOT NULL,
  `viewed` char(1) NOT NULL DEFAULT 'n',
  `description` text,
  `function` varchar(100) DEFAULT NULL,
  `line` int(10) unsigned DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `deprecated_since` varchar(10) DEFAULT NULL,
  `use_instead` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_cache` */
DROP TABLE IF EXISTS `exp_email_cache`;

CREATE TABLE `exp_email_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `total_sent` int(6) unsigned NOT NULL,
  `from_name` varchar(70) NOT NULL,
  `from_email` varchar(70) NOT NULL,
  `recipient` text NOT NULL,
  `cc` text NOT NULL,
  `bcc` text NOT NULL,
  `recipient_array` mediumtext NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  `plaintext_alt` mediumtext NOT NULL,
  `mailinglist` char(1) NOT NULL DEFAULT 'n',
  `mailtype` varchar(6) NOT NULL,
  `text_fmt` varchar(40) NOT NULL,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `priority` char(1) NOT NULL DEFAULT '3',
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_cache_mg` */
DROP TABLE IF EXISTS `exp_email_cache_mg`;

CREATE TABLE `exp_email_cache_mg` (
  `cache_id` int(6) unsigned NOT NULL,
  `group_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_cache_ml` */
DROP TABLE IF EXISTS `exp_email_cache_ml`;

CREATE TABLE `exp_email_cache_ml` (
  `cache_id` int(6) unsigned NOT NULL,
  `list_id` smallint(4) NOT NULL,
  PRIMARY KEY (`cache_id`,`list_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_console_cache` */
DROP TABLE IF EXISTS `exp_email_console_cache`;

CREATE TABLE `exp_email_console_cache` (
  `cache_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `cache_date` int(10) unsigned NOT NULL DEFAULT '0',
  `member_id` int(10) unsigned NOT NULL,
  `member_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `recipient` varchar(70) NOT NULL,
  `recipient_name` varchar(50) NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  PRIMARY KEY (`cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_email_tracker` */
DROP TABLE IF EXISTS `exp_email_tracker`;

CREATE TABLE `exp_email_tracker` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_ip` varchar(16) NOT NULL,
  `sender_email` varchar(75) NOT NULL,
  `sender_username` varchar(50) NOT NULL,
  `number_recipients` int(4) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`email_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_entry_ping_status` */
DROP TABLE IF EXISTS `exp_entry_ping_status`;

CREATE TABLE `exp_entry_ping_status` (
  `entry_id` int(10) unsigned NOT NULL,
  `ping_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`entry_id`,`ping_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_entry_versioning` */
DROP TABLE IF EXISTS `exp_entry_versioning`;

CREATE TABLE `exp_entry_versioning` (
  `version_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_id` int(10) unsigned NOT NULL,
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `version_date` int(10) NOT NULL,
  `version_data` mediumtext NOT NULL,
  PRIMARY KEY (`version_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_extensions` */
DROP TABLE IF EXISTS `exp_extensions`;

CREATE TABLE `exp_extensions` (
  `extension_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `class` varchar(50) NOT NULL DEFAULT '',
  `method` varchar(50) NOT NULL DEFAULT '',
  `hook` varchar(50) NOT NULL DEFAULT '',
  `settings` text NOT NULL,
  `priority` int(2) NOT NULL DEFAULT '10',
  `version` varchar(10) NOT NULL DEFAULT '',
  `enabled` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`extension_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_extensions` */
INSERT INTO `exp_extensions` VALUES(1, 'Safecracker_ext', 'form_declaration_modify_data', 'form_declaration_modify_data', '', 10, '2.1', 'y');
INSERT INTO `exp_extensions` VALUES(2, 'Rte_ext', 'myaccount_nav_setup', 'myaccount_nav_setup', '', 10, '1.0', 'y');
INSERT INTO `exp_extensions` VALUES(3, 'Rte_ext', 'cp_menu_array', 'cp_menu_array', '', 10, '1.0', 'y');
INSERT INTO `exp_extensions` VALUES(4, 'Rte_ext', 'publish_form_entry_data', 'publish_form_entry_data', '', 10, '1.0', 'y');
INSERT INTO `exp_extensions` VALUES(5, 'Matrix_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 10, '2.5.2', 'y');
INSERT INTO `exp_extensions` VALUES(6, 'Playa_ext', 'channel_entries_tagdata', 'channel_entries_tagdata', '', 9, '4.3.3', 'y');
INSERT INTO `exp_extensions` VALUES(7, 'Low_seg2cat_ext', 'sessions_end', 'sessions_end', 'a:3:{s:15:\"category_groups\";a:0:{}s:11:\"uri_pattern\";s:0:\"\";s:16:\"set_all_segments\";s:1:\"n\";}', 1, '2.6.3', 'y');
INSERT INTO `exp_extensions` VALUES(8, 'Mx_cloner_ext', 'publish_form_entry_data', 'publish_form_entry_data', 'a:1:{s:13:\"multilanguage\";s:1:\"n\";}', 10, '1.0.3', 'y');

/* Table structure for table `exp_field_formatting` */
DROP TABLE IF EXISTS `exp_field_formatting`;

CREATE TABLE `exp_field_formatting` (
  `formatting_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` int(10) unsigned NOT NULL,
  `field_fmt` varchar(40) NOT NULL,
  PRIMARY KEY (`formatting_id`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_field_formatting` */
INSERT INTO `exp_field_formatting` VALUES(1, 1, 'none');
INSERT INTO `exp_field_formatting` VALUES(2, 1, 'br');
INSERT INTO `exp_field_formatting` VALUES(3, 1, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(4, 2, 'none');
INSERT INTO `exp_field_formatting` VALUES(5, 2, 'br');
INSERT INTO `exp_field_formatting` VALUES(6, 2, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(7, 3, 'none');
INSERT INTO `exp_field_formatting` VALUES(8, 3, 'br');
INSERT INTO `exp_field_formatting` VALUES(9, 3, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(10, 4, 'none');
INSERT INTO `exp_field_formatting` VALUES(11, 4, 'br');
INSERT INTO `exp_field_formatting` VALUES(12, 4, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(13, 5, 'none');
INSERT INTO `exp_field_formatting` VALUES(14, 5, 'br');
INSERT INTO `exp_field_formatting` VALUES(15, 5, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(16, 6, 'none');
INSERT INTO `exp_field_formatting` VALUES(17, 6, 'br');
INSERT INTO `exp_field_formatting` VALUES(18, 6, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(19, 7, 'none');
INSERT INTO `exp_field_formatting` VALUES(20, 7, 'br');
INSERT INTO `exp_field_formatting` VALUES(21, 7, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(22, 8, 'none');
INSERT INTO `exp_field_formatting` VALUES(23, 8, 'br');
INSERT INTO `exp_field_formatting` VALUES(24, 8, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(25, 9, 'none');
INSERT INTO `exp_field_formatting` VALUES(26, 9, 'br');
INSERT INTO `exp_field_formatting` VALUES(27, 9, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(28, 10, 'none');
INSERT INTO `exp_field_formatting` VALUES(29, 10, 'br');
INSERT INTO `exp_field_formatting` VALUES(30, 10, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(31, 11, 'none');
INSERT INTO `exp_field_formatting` VALUES(32, 11, 'br');
INSERT INTO `exp_field_formatting` VALUES(33, 11, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(34, 12, 'none');
INSERT INTO `exp_field_formatting` VALUES(35, 12, 'br');
INSERT INTO `exp_field_formatting` VALUES(36, 12, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(37, 13, 'none');
INSERT INTO `exp_field_formatting` VALUES(38, 13, 'br');
INSERT INTO `exp_field_formatting` VALUES(39, 13, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(40, 14, 'none');
INSERT INTO `exp_field_formatting` VALUES(41, 14, 'br');
INSERT INTO `exp_field_formatting` VALUES(42, 14, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(43, 15, 'none');
INSERT INTO `exp_field_formatting` VALUES(44, 15, 'br');
INSERT INTO `exp_field_formatting` VALUES(45, 15, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(46, 16, 'none');
INSERT INTO `exp_field_formatting` VALUES(47, 16, 'br');
INSERT INTO `exp_field_formatting` VALUES(48, 16, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(49, 17, 'none');
INSERT INTO `exp_field_formatting` VALUES(50, 17, 'br');
INSERT INTO `exp_field_formatting` VALUES(51, 17, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(52, 18, 'none');
INSERT INTO `exp_field_formatting` VALUES(53, 18, 'br');
INSERT INTO `exp_field_formatting` VALUES(54, 18, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(55, 19, 'none');
INSERT INTO `exp_field_formatting` VALUES(56, 19, 'br');
INSERT INTO `exp_field_formatting` VALUES(57, 19, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(58, 20, 'none');
INSERT INTO `exp_field_formatting` VALUES(59, 20, 'br');
INSERT INTO `exp_field_formatting` VALUES(60, 20, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(64, 22, 'none');
INSERT INTO `exp_field_formatting` VALUES(65, 22, 'br');
INSERT INTO `exp_field_formatting` VALUES(66, 22, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(67, 23, 'none');
INSERT INTO `exp_field_formatting` VALUES(68, 23, 'br');
INSERT INTO `exp_field_formatting` VALUES(69, 23, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(70, 24, 'none');
INSERT INTO `exp_field_formatting` VALUES(71, 24, 'br');
INSERT INTO `exp_field_formatting` VALUES(72, 24, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(73, 25, 'none');
INSERT INTO `exp_field_formatting` VALUES(74, 25, 'br');
INSERT INTO `exp_field_formatting` VALUES(75, 25, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(76, 26, 'none');
INSERT INTO `exp_field_formatting` VALUES(77, 26, 'br');
INSERT INTO `exp_field_formatting` VALUES(78, 26, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(79, 27, 'none');
INSERT INTO `exp_field_formatting` VALUES(80, 27, 'br');
INSERT INTO `exp_field_formatting` VALUES(81, 27, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(82, 28, 'none');
INSERT INTO `exp_field_formatting` VALUES(83, 28, 'br');
INSERT INTO `exp_field_formatting` VALUES(84, 28, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(85, 29, 'none');
INSERT INTO `exp_field_formatting` VALUES(86, 29, 'br');
INSERT INTO `exp_field_formatting` VALUES(87, 29, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(88, 30, 'none');
INSERT INTO `exp_field_formatting` VALUES(89, 30, 'br');
INSERT INTO `exp_field_formatting` VALUES(90, 30, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(91, 31, 'none');
INSERT INTO `exp_field_formatting` VALUES(92, 31, 'br');
INSERT INTO `exp_field_formatting` VALUES(93, 31, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(94, 32, 'none');
INSERT INTO `exp_field_formatting` VALUES(95, 32, 'br');
INSERT INTO `exp_field_formatting` VALUES(96, 32, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(97, 33, 'none');
INSERT INTO `exp_field_formatting` VALUES(98, 33, 'br');
INSERT INTO `exp_field_formatting` VALUES(99, 33, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(100, 34, 'none');
INSERT INTO `exp_field_formatting` VALUES(101, 34, 'br');
INSERT INTO `exp_field_formatting` VALUES(102, 34, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(103, 35, 'none');
INSERT INTO `exp_field_formatting` VALUES(104, 35, 'br');
INSERT INTO `exp_field_formatting` VALUES(105, 35, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(106, 36, 'none');
INSERT INTO `exp_field_formatting` VALUES(107, 36, 'br');
INSERT INTO `exp_field_formatting` VALUES(108, 36, 'xhtml');
INSERT INTO `exp_field_formatting` VALUES(109, 37, 'none');
INSERT INTO `exp_field_formatting` VALUES(110, 37, 'br');
INSERT INTO `exp_field_formatting` VALUES(111, 37, 'xhtml');

/* Table structure for table `exp_field_groups` */
DROP TABLE IF EXISTS `exp_field_groups`;

CREATE TABLE `exp_field_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_field_groups` */
INSERT INTO `exp_field_groups` VALUES(1, 1, 'Products');
INSERT INTO `exp_field_groups` VALUES(2, 1, 'Featured Product Pages');
INSERT INTO `exp_field_groups` VALUES(3, 1, 'News');
INSERT INTO `exp_field_groups` VALUES(4, 1, 'Contact Us');
INSERT INTO `exp_field_groups` VALUES(5, 1, 'Case Studies');
INSERT INTO `exp_field_groups` VALUES(6, 1, 'Services');
INSERT INTO `exp_field_groups` VALUES(7, 1, 'Home Page Banners');
INSERT INTO `exp_field_groups` VALUES(8, 1, 'Management');
INSERT INTO `exp_field_groups` VALUES(9, 1, 'About Us');
INSERT INTO `exp_field_groups` VALUES(10, 1, 'Manufacturers');

/* Table structure for table `exp_fieldtypes` */
DROP TABLE IF EXISTS `exp_fieldtypes`;

CREATE TABLE `exp_fieldtypes` (
  `fieldtype_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `version` varchar(12) NOT NULL,
  `settings` text,
  `has_global_settings` char(1) DEFAULT 'n',
  PRIMARY KEY (`fieldtype_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_fieldtypes` */
INSERT INTO `exp_fieldtypes` VALUES(1, 'select', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(2, 'text', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(3, 'textarea', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(4, 'date', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(5, 'file', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(6, 'multi_select', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(7, 'checkboxes', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(8, 'radio', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(9, 'rel', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(10, 'rte', '1.0', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(11, 'matrix', '2.5.2', 'YTowOnt9', 'y');
INSERT INTO `exp_fieldtypes` VALUES(12, 'playa', '4.3.3', 'YTowOnt9', 'y');
INSERT INTO `exp_fieldtypes` VALUES(13, 'wygwam', '2.6.3', 'YTowOnt9', 'y');
INSERT INTO `exp_fieldtypes` VALUES(14, 'pt_checkboxes', '1.0.3', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(15, 'pt_dropdown', '1.0.3', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(16, 'pt_multiselect', '1.0.3', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(17, 'pt_radio_buttons', '1.0.3', 'YTowOnt9', 'n');
INSERT INTO `exp_fieldtypes` VALUES(18, 'nolan', '1.0.3.1', 'YTowOnt9', 'n');

/* Table structure for table `exp_file_categories` */
DROP TABLE IF EXISTS `exp_file_categories`;

CREATE TABLE `exp_file_categories` (
  `file_id` int(10) unsigned DEFAULT NULL,
  `cat_id` int(10) unsigned DEFAULT NULL,
  `sort` int(10) unsigned DEFAULT '0',
  `is_cover` char(1) DEFAULT 'n',
  KEY `file_id` (`file_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_file_dimensions` */
DROP TABLE IF EXISTS `exp_file_dimensions`;

CREATE TABLE `exp_file_dimensions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `upload_location_id` int(4) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT '',
  `short_name` varchar(255) DEFAULT '',
  `resize_type` varchar(50) DEFAULT '',
  `width` int(10) DEFAULT '0',
  `height` int(10) DEFAULT '0',
  `watermark_id` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `upload_location_id` (`upload_location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_file_dimensions` */
INSERT INTO `exp_file_dimensions` VALUES(1, 1, 4, 'article-list-thumb', 'article-list-thumb', 'crop', 396, 88, 0);
INSERT INTO `exp_file_dimensions` VALUES(2, 1, 5, 'case-study-list-thumb', 'case-study-list-thumb', 'crop', 396, 88, 0);

/* Table structure for table `exp_file_watermarks` */
DROP TABLE IF EXISTS `exp_file_watermarks`;

CREATE TABLE `exp_file_watermarks` (
  `wm_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `wm_name` varchar(80) DEFAULT NULL,
  `wm_type` varchar(10) DEFAULT 'text',
  `wm_image_path` varchar(100) DEFAULT NULL,
  `wm_test_image_path` varchar(100) DEFAULT NULL,
  `wm_use_font` char(1) DEFAULT 'y',
  `wm_font` varchar(30) DEFAULT NULL,
  `wm_font_size` int(3) unsigned DEFAULT NULL,
  `wm_text` varchar(100) DEFAULT NULL,
  `wm_vrt_alignment` varchar(10) DEFAULT 'top',
  `wm_hor_alignment` varchar(10) DEFAULT 'left',
  `wm_padding` int(3) unsigned DEFAULT NULL,
  `wm_opacity` int(3) unsigned DEFAULT NULL,
  `wm_hor_offset` int(4) unsigned DEFAULT NULL,
  `wm_vrt_offset` int(4) unsigned DEFAULT NULL,
  `wm_x_transp` int(4) DEFAULT NULL,
  `wm_y_transp` int(4) DEFAULT NULL,
  `wm_font_color` varchar(7) DEFAULT NULL,
  `wm_use_drop_shadow` char(1) DEFAULT 'y',
  `wm_shadow_distance` int(3) unsigned DEFAULT NULL,
  `wm_shadow_color` varchar(7) DEFAULT NULL,
  PRIMARY KEY (`wm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_files` */
DROP TABLE IF EXISTS `exp_files`;

CREATE TABLE `exp_files` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `title` varchar(255) DEFAULT NULL,
  `upload_location_id` int(4) unsigned DEFAULT '0',
  `rel_path` varchar(255) DEFAULT NULL,
  `mime_type` varchar(255) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_size` int(10) DEFAULT '0',
  `description` text,
  `credit` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `uploaded_by_member_id` int(10) unsigned DEFAULT '0',
  `upload_date` int(10) DEFAULT NULL,
  `modified_by_member_id` int(10) unsigned DEFAULT '0',
  `modified_date` int(10) DEFAULT NULL,
  `file_hw_original` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`file_id`),
  KEY `upload_location_id` (`upload_location_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_files` */
INSERT INTO `exp_files` VALUES(1, 1, 'large-product-image.jpg', 1, '/Users/james/Projects/96black/Synergy/uploads/product-images/large-product-image.jpg', 'image/jpeg', 'large-product-image.jpg', 82647, null, null, null, 1, 1358293635, 1, 1358293635, '485 352');
INSERT INTO `exp_files` VALUES(3, 1, 'large-product-image2.jpg', 2, '/Users/james/Projects/96black/Synergy/uploads/product-images/large-product-image2.jpg', 'image/jpeg', 'large-product-image2.jpg', 82647, null, null, null, 1, 1358294245, 1, 1358294248, '485 352');
INSERT INTO `exp_files` VALUES(4, 1, 'large-product-image3.jpg', 2, '/Users/james/Projects/96black/Synergy/uploads/product-images/large-product-image3.jpg', 'image/jpeg', 'large-product-image3.jpg', 82647, null, null, null, 1, 1358294270, 1, 1358294270, '485 352');
INSERT INTO `exp_files` VALUES(5, 1, 'large-product-image4.jpg', 2, '/Users/james/Projects/96black/Synergy/uploads/product-downloads/large-product-image4.jpg', 'image/jpeg', 'large-product-image4.jpg', 82647, null, null, null, 1, 1358294478, 1, 1358294478, '485 352');
INSERT INTO `exp_files` VALUES(6, 1, 'large-product-image6.jpg', 1, '/Users/james/Projects/96black/Synergy/uploads/product-images/large-product-image6.jpg', 'image/jpeg', 'large-product-image6.jpg', 82647, null, null, null, 1, 1358363557, 1, 1358363557, '485 352');
INSERT INTO `exp_files` VALUES(7, 1, 'cat-image.jpg', 3, '/Users/jamesmcfall/Projects/Synergy/uploads/category-images/cat-image.jpg', 'image/jpeg', 'cat-image.jpg', 8376, null, null, null, 1, 1362950904, 1, 1362950904, '211 153');
INSERT INTO `exp_files` VALUES(8, 1, 'news-temp-top.jpg', 4, '/Users/jamesmcfall/Projects/Synergy/uploads/news-images/news-temp-top.jpg', 'image/jpeg', 'news-temp-top.jpg', 56244, null, null, null, 1, 1362961635, 1, 1362961635, '373 866');
INSERT INTO `exp_files` VALUES(9, 1, 'cat-feature-slide1.jpg', 1, '/Users/jamesmcfall/Projects/Synergy/uploads/product-images/cat-feature-slide1.jpg', 'image/jpeg', 'cat-feature-slide1.jpg', 19917, null, null, null, 1, 1363820695, 1, 1363820695, '411 866');
INSERT INTO `exp_files` VALUES(10, 1, 'cat-feature-slide3.jpg', 1, '/Users/jamesmcfall/Projects/Synergy/uploads/product-images/cat-feature-slide3.jpg', 'image/jpeg', 'cat-feature-slide3.jpg', 20183, null, null, null, 1, 1363820736, 1, 1363820736, '411 866');
INSERT INTO `exp_files` VALUES(11, 1, 'news-temp-top_(1).jpg', 5, '/Users/jamesmcfall/Projects/Synergy/uploads/case-study-image/news-temp-top_(1).jpg', 'image/jpeg', 'news-temp-top_(1).jpg', 56300, null, null, null, 1, 1363903862, 1, 1363903862, '373 866');
INSERT INTO `exp_files` VALUES(12, 1, 'news-temp-top_(2).jpg', 5, '/Users/jamesmcfall/Projects/Synergy/uploads/case-study-image/news-temp-top_(2).jpg', 'image/jpeg', 'news-temp-top_(2).jpg', 56300, null, null, null, 1, 1363904837, 1, 1363904841, '373 866');
INSERT INTO `exp_files` VALUES(13, 1, 'news-temp-top_(3).jpg', 5, '/Users/jamesmcfall/Projects/Synergy/uploads/case-study-image/news-temp-top_(3).jpg', 'image/jpeg', 'news-temp-top_(3).jpg', 56300, null, null, null, 1, 1363904916, 1, 1363904920, '373 866');
INSERT INTO `exp_files` VALUES(14, 1, 'services.jpg', 6, '/Users/jamesmcfall/Projects/Synergy/uploads/home-page-banners/services.jpg', 'image/jpeg', 'services.jpg', 88125, null, null, null, 1, 1365459810, 1, 1365459810, '714 1600');
INSERT INTO `exp_files` VALUES(15, 1, 'products.jpg', 6, '/Users/jamesmcfall/Projects/Synergy/uploads/home-page-banners/products.jpg', 'image/jpeg', 'products.jpg', 85228, null, null, null, 1, 1365460093, 1, 1365460093, '714 1600');
INSERT INTO `exp_files` VALUES(16, 1, 'about.jpg', 6, '/Users/jamesmcfall/Projects/Synergy/uploads/home-page-banners/about.jpg', 'image/jpeg', 'about.jpg', 111165, null, null, null, 1, 1365460222, 1, 1365460222, '714 1600');
INSERT INTO `exp_files` VALUES(17, 1, 'services-temp-top.jpg', 3, '/Users/jamesmcfall/Projects/Synergy/uploads/category-images/services-temp-top.jpg', 'image/jpeg', 'services-temp-top.jpg', 94945, null, null, null, 1, 1365480417, 1, 1365480417, '305 866');
INSERT INTO `exp_files` VALUES(18, 1, 'services-temp-top.jpg', 7, '/Users/jamesmcfall/Projects/Synergy/uploads/services-banners/services-temp-top.jpg', 'image/jpeg', 'services-temp-top.jpg', 94945, null, null, null, 1, 1365562517, 1, 1365562517, '305 866');
INSERT INTO `exp_files` VALUES(19, 1, 'about-temp.jpg', 8, '/Users/jamesmcfall/Projects/Synergy/uploads/about-banners/about-temp.jpg', 'image/jpeg', 'about-temp.jpg', 55081, null, null, null, 1, 1365633903, 1, 1365633903, '305 867');
INSERT INTO `exp_files` VALUES(20, 1, 'james.jpg', 9, '/Users/jamesmcfall/Projects/Synergy/uploads/staff-pictures/james.jpg', 'image/jpeg', 'james.jpg', 8591, null, null, null, 1, 1365636639, 1, 1365636639, '211 153');
INSERT INTO `exp_files` VALUES(21, 1, 'mega-menu-feature-product-image_(1).jpg', 1, '/Users/jamesmcfall/Projects/Synergy/uploads/product-images/mega-menu-feature-product-image_(1).jpg', 'image/jpeg', 'mega-menu-feature-product-image_(1).jpg', 10854, null, null, null, 1, 1365720765, 1, 1365720765, '89 302');
INSERT INTO `exp_files` VALUES(22, 1, 'footer-topcon.gif', 10, '/Users/jamesmcfall/Projects/Synergy/uploads/manufacturer-images/footer-topcon.gif', 'image/gif', 'footer-topcon.gif', 3881, null, null, null, 1, 1365731455, 1, 1365731455, '28 173');
INSERT INTO `exp_files` VALUES(23, 1, 'topcon-logo.gif', 10, '/Users/jamesmcfall/Projects/Synergy/uploads/manufacturer-images/topcon-logo.gif', 'image/gif', 'topcon-logo.gif', 3820, null, null, null, 1, 1365731467, 1, 1365731467, '24 140');
INSERT INTO `exp_files` VALUES(24, 1, 'mega-menu-feature-product-logo.jpg', 10, '/Users/jamesmcfall/Projects/Synergy/uploads/manufacturer-images/mega-menu-feature-product-logo.jpg', 'image/jpeg', 'mega-menu-feature-product-logo.jpg', 2918, null, null, null, 1, 1365731492, 1, 1365731492, '48 166');

/* Table structure for table `exp_freeform_composer_layouts` */
DROP TABLE IF EXISTS `exp_freeform_composer_layouts`;

CREATE TABLE `exp_freeform_composer_layouts` (
  `composer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `composer_data` text,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `preview` char(1) NOT NULL DEFAULT 'n',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`composer_id`),
  KEY `preview` (`preview`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_freeform_composer_templates` */
DROP TABLE IF EXISTS `exp_freeform_composer_templates`;

CREATE TABLE `exp_freeform_composer_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `template_name` varchar(150) NOT NULL DEFAULT 'default',
  `template_label` varchar(150) NOT NULL DEFAULT 'default',
  `template_description` text,
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_data` text,
  `param_data` text,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_freeform_fields` */
DROP TABLE IF EXISTS `exp_freeform_fields`;

CREATE TABLE `exp_freeform_fields` (
  `field_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `field_name` varchar(150) NOT NULL DEFAULT 'default',
  `field_label` varchar(150) NOT NULL DEFAULT 'default',
  `field_type` varchar(50) NOT NULL DEFAULT 'text',
  `settings` text,
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `required` char(1) NOT NULL DEFAULT 'n',
  `submissions_page` char(1) NOT NULL DEFAULT 'y',
  `moderation_page` char(1) NOT NULL DEFAULT 'y',
  `composer_use` char(1) NOT NULL DEFAULT 'y',
  `field_description` text,
  PRIMARY KEY (`field_id`),
  KEY `field_name` (`field_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_fields` */
INSERT INTO `exp_freeform_fields` VALUES(1, 1, 'first_name', 'First Name', 'text', '{\"field_length\":150,\"field_content_type\":\"any\"}', 1, 1365716048, 0, 'n', 'y', 'y', 'y', 'This field contains the user\'s first name.');
INSERT INTO `exp_freeform_fields` VALUES(2, 1, 'last_name', 'Last Name', 'text', '{\"field_length\":150,\"field_content_type\":\"any\"}', 1, 1365716048, 0, 'n', 'y', 'y', 'y', 'This field contains the user\'s last name.');
INSERT INTO `exp_freeform_fields` VALUES(3, 1, 'email', 'Email', 'text', '{\"field_length\":150,\"field_content_type\":\"email\"}', 1, 1365716048, 0, 'n', 'y', 'y', 'y', 'A basic email field for collecting stuff like an email address.');
INSERT INTO `exp_freeform_fields` VALUES(4, 1, 'user_message', 'Message', 'textarea', '{\"field_ta_rows\":6}', 1, 1365716048, 0, 'n', 'y', 'y', 'y', 'This field contains the user\'s message.');
INSERT INTO `exp_freeform_fields` VALUES(5, 1, 'company_name', 'Company Name', 'text', '{\"field_length\":\"255\",\"field_content_type\":\"any\",\"disallow_html_rendering\":\"y\"}', 1, 1365716329, 0, 'n', 'y', 'y', 'y', 'This field contains the company name.');
INSERT INTO `exp_freeform_fields` VALUES(6, 1, 'full_name', 'Full Name', 'text', '{\"field_length\":\"150\",\"field_content_type\":\"any\",\"disallow_html_rendering\":\"y\"}', 1, 1365716586, 0, 'n', 'y', 'y', 'y', 'This contains the full name.');
INSERT INTO `exp_freeform_fields` VALUES(7, 1, 'phone_number', 'Phone Number', 'text', '{\"field_length\":\"150\",\"field_content_type\":\"number\",\"disallow_html_rendering\":\"y\"}', 1, 1365716640, 0, 'n', 'y', 'y', 'y', '');

/* Table structure for table `exp_freeform_fieldtypes` */
DROP TABLE IF EXISTS `exp_freeform_fieldtypes`;

CREATE TABLE `exp_freeform_fieldtypes` (
  `fieldtype_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fieldtype_name` varchar(250) DEFAULT NULL,
  `settings` text,
  `default_field` char(1) NOT NULL DEFAULT 'n',
  `version` varchar(12) DEFAULT NULL,
  PRIMARY KEY (`fieldtype_id`),
  KEY `fieldtype_name` (`fieldtype_name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_fieldtypes` */
INSERT INTO `exp_freeform_fieldtypes` VALUES(1, 'file_upload', '[]', 'n', '4.0.11');
INSERT INTO `exp_freeform_fieldtypes` VALUES(2, 'mailinglist', '[]', 'n', '4.0.11');
INSERT INTO `exp_freeform_fieldtypes` VALUES(3, 'text', '[]', 'n', '4.0.11');
INSERT INTO `exp_freeform_fieldtypes` VALUES(4, 'textarea', '[]', 'n', '4.0.11');

/* Table structure for table `exp_freeform_file_uploads` */
DROP TABLE IF EXISTS `exp_freeform_file_uploads`;

CREATE TABLE `exp_freeform_file_uploads` (
  `file_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `field_id` int(10) unsigned NOT NULL DEFAULT '0',
  `server_path` varchar(750) DEFAULT NULL,
  `filename` varchar(250) DEFAULT NULL,
  `extension` varchar(20) DEFAULT NULL,
  `filesize` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`file_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `extension` (`extension`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_freeform_form_entries_1` */
DROP TABLE IF EXISTS `exp_freeform_form_entries_1`;

CREATE TABLE `exp_freeform_form_entries_1` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `complete` varchar(1) NOT NULL DEFAULT 'y',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(50) DEFAULT NULL,
  `form_field_3` text,
  `form_field_4` text,
  `form_field_6` text,
  `form_field_7` text,
  `form_field_5` text,
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_form_entries_1` */
INSERT INTO `exp_freeform_form_entries_1` VALUES(1, 1, 0, 'y', '127.0.0.1', 1365716048, 0, 'pending', 'support@solspace.com', 'Welcome to Freeform. We hope that you will enjoy Solspace software.', null, null, null);
INSERT INTO `exp_freeform_form_entries_1` VALUES(2, 1, 1, 'y', '127.0.0.1', 1365717771, 0, 'pending', '', '', '', '', '');
INSERT INTO `exp_freeform_form_entries_1` VALUES(3, 1, 1, 'y', '127.0.0.1', 1365718729, 0, 'pending', 'james@96black.co.nz', 'Test message submission.', 'James McFall', '093603493', '96black');

/* Table structure for table `exp_freeform_form_entries_2` */
DROP TABLE IF EXISTS `exp_freeform_form_entries_2`;

CREATE TABLE `exp_freeform_form_entries_2` (
  `entry_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `complete` varchar(1) NOT NULL DEFAULT 'y',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `status` varchar(50) DEFAULT NULL,
  `form_field_5` text,
  `form_field_6` text,
  `form_field_7` text,
  `form_field_3` text,
  `form_field_4` text,
  PRIMARY KEY (`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_freeform_forms` */
DROP TABLE IF EXISTS `exp_freeform_forms`;

CREATE TABLE `exp_freeform_forms` (
  `form_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_name` varchar(150) NOT NULL DEFAULT 'default',
  `form_label` varchar(150) NOT NULL DEFAULT 'default',
  `default_status` varchar(150) NOT NULL DEFAULT 'default',
  `notify_user` char(1) NOT NULL DEFAULT 'n',
  `notify_admin` char(1) NOT NULL DEFAULT 'n',
  `user_email_field` varchar(150) NOT NULL DEFAULT '',
  `user_notification_id` int(10) unsigned NOT NULL DEFAULT '0',
  `admin_notification_id` int(10) unsigned NOT NULL DEFAULT '0',
  `admin_notification_email` text,
  `form_description` text,
  `field_ids` text,
  `field_order` text,
  `template_id` int(10) unsigned NOT NULL DEFAULT '0',
  `composer_id` int(10) unsigned NOT NULL DEFAULT '0',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit_date` int(10) unsigned NOT NULL DEFAULT '0',
  `settings` text,
  PRIMARY KEY (`form_id`),
  KEY `form_name` (`form_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_forms` */
INSERT INTO `exp_freeform_forms` VALUES(1, 1, 'contact', 'Contact', 'pending', 'n', 'y', '', 0, 0, 'james@96black.co.nz', 'This is a basic contact form.', '3|4|5|6|7', '6|5|3|7|4', 0, 0, 1, 1365716048, 1365716871, null);
INSERT INTO `exp_freeform_forms` VALUES(2, 1, 'product_enquiry_form', 'Product Enquiry Form', 'pending', 'n', 'y', '', 0, 0, 'james@96black.co.nz', 'This form handles product enquiries.', '3|4|5|6|7', '6|5|7|3|4', 0, 0, 1, 1365716187, 1365716735, null);

/* Table structure for table `exp_freeform_multipage_hashes` */
DROP TABLE IF EXISTS `exp_freeform_multipage_hashes`;

CREATE TABLE `exp_freeform_multipage_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `hash` varchar(32) NOT NULL DEFAULT '',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `edit` char(1) NOT NULL DEFAULT 'n',
  `data` text,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`),
  KEY `ip_address` (`ip_address`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_freeform_notification_templates` */
DROP TABLE IF EXISTS `exp_freeform_notification_templates`;

CREATE TABLE `exp_freeform_notification_templates` (
  `notification_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `notification_name` varchar(150) NOT NULL DEFAULT 'default',
  `notification_label` varchar(150) NOT NULL DEFAULT 'default',
  `notification_description` text,
  `wordwrap` char(1) NOT NULL DEFAULT 'y',
  `allow_html` char(1) NOT NULL DEFAULT 'n',
  `from_name` varchar(150) NOT NULL DEFAULT '',
  `from_email` varchar(250) NOT NULL DEFAULT '',
  `reply_to_email` varchar(250) NOT NULL DEFAULT '',
  `email_subject` varchar(128) NOT NULL DEFAULT 'default',
  `include_attachments` char(1) NOT NULL DEFAULT 'n',
  `template_data` text,
  PRIMARY KEY (`notification_id`),
  KEY `notification_name` (`notification_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_freeform_params` */
DROP TABLE IF EXISTS `exp_freeform_params`;

CREATE TABLE `exp_freeform_params` (
  `params_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text,
  PRIMARY KEY (`params_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_params` */
INSERT INTO `exp_freeform_params` VALUES(42, 1365845084, '{\"form_id\":\"1\",\"edit\":false,\"entry_id\":0,\"secure_action\":false,\"secure_return\":false,\"require_captcha\":false,\"require_ip\":true,\"return\":\"Contact-us\\/index\\/%%entry_id%%\",\"inline_error_return\":\"contact-us\",\"error_page\":\"\",\"ajax\":true,\"restrict_edit_to_author\":true,\"inline_errors\":true,\"prevent_duplicate_on\":\"\",\"prevent_duplicate_per_site\":false,\"secure_duplicate_redirect\":false,\"duplicate_redirect\":\"\",\"error_on_duplicate\":false,\"required\":\"full_name|email|user_message\",\"matching_fields\":\"\",\"last_page\":true,\"multipage\":false,\"redirect_on_timeout\":true,\"redirect_on_timeout_to\":\"\",\"page_marker\":\"page\",\"multipage_page\":\"\",\"paging_url\":\"\",\"multipage_page_names\":\"\",\"admin_notify\":\"james@96black.co.nz\",\"admin_cc_notify\":\"\",\"admin_bcc_notify\":\"\",\"notify_user\":false,\"notify_admin\":true,\"notify_on_edit\":false,\"user_email_field\":\"\",\"recipients\":false,\"recipients_limit\":\"3\",\"recipient_user_input\":false,\"recipient_user_limit\":\"3\",\"recipient_template\":\"\",\"recipient_user_template\":\"\",\"admin_notification_template\":\"0\",\"user_notification_template\":\"0\",\"status\":\"pending\",\"allow_status_edit\":false,\"recipients_list\":[]}');

/* Table structure for table `exp_freeform_preferences` */
DROP TABLE IF EXISTS `exp_freeform_preferences`;

CREATE TABLE `exp_freeform_preferences` (
  `preference_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `preference_name` varchar(80) DEFAULT NULL,
  `preference_value` text,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`preference_id`),
  KEY `preference_name` (`preference_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_freeform_preferences` */
INSERT INTO `exp_freeform_preferences` VALUES(1, 'ffp', 'n', 0);
INSERT INTO `exp_freeform_preferences` VALUES(2, 'field_layout_prefs', '{\"entry_layout_prefs\":{\"member\":{\"1\":{\"visible\":[\"6\",\"5\",\"7\",\"3\",\"4\",\"author\",\"ip_address\",\"entry_date\",\"edit_date\",\"status\"],\"hidden\":[\"complete\",\"entry_id\",\"site_id\"]}}}}', 1);

/* Table structure for table `exp_freeform_user_email` */
DROP TABLE IF EXISTS `exp_freeform_user_email`;

CREATE TABLE `exp_freeform_user_email` (
  `email_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(10) unsigned NOT NULL DEFAULT '1',
  `author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(40) NOT NULL DEFAULT '0',
  `entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `form_id` int(10) unsigned NOT NULL DEFAULT '0',
  `entry_id` int(10) unsigned NOT NULL DEFAULT '0',
  `email_count` int(10) unsigned NOT NULL DEFAULT '0',
  `email_addresses` text,
  PRIMARY KEY (`email_id`),
  KEY `ip_address` (`ip_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_global_variables` */
DROP TABLE IF EXISTS `exp_global_variables`;

CREATE TABLE `exp_global_variables` (
  `variable_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `variable_name` varchar(50) NOT NULL,
  `variable_data` text NOT NULL,
  PRIMARY KEY (`variable_id`),
  KEY `variable_name` (`variable_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_html_buttons` */
DROP TABLE IF EXISTS `exp_html_buttons`;

CREATE TABLE `exp_html_buttons` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `tag_name` varchar(32) NOT NULL,
  `tag_open` varchar(120) NOT NULL,
  `tag_close` varchar(120) NOT NULL,
  `accesskey` varchar(32) NOT NULL,
  `tag_order` int(3) unsigned NOT NULL,
  `tag_row` char(1) NOT NULL DEFAULT '1',
  `classname` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_html_buttons` */
INSERT INTO `exp_html_buttons` VALUES(1, 1, 0, 'b', '<strong>', '</strong>', 'b', 1, '1', 'btn_b');
INSERT INTO `exp_html_buttons` VALUES(2, 1, 0, 'i', '<em>', '</em>', 'i', 2, '1', 'btn_i');
INSERT INTO `exp_html_buttons` VALUES(3, 1, 0, 'blockquote', '<blockquote>', '</blockquote>', 'q', 3, '1', 'btn_blockquote');
INSERT INTO `exp_html_buttons` VALUES(4, 1, 0, 'a', '<a href=\"[![Link:!:http://]!]\"(!( title=\"[![Title]!]\")!)>', '</a>', 'a', 4, '1', 'btn_a');
INSERT INTO `exp_html_buttons` VALUES(5, 1, 0, 'img', '<img src=\"[![Link:!:http://]!]\" alt=\"[![Alternative text]!]\" />', '', '', 5, '1', 'btn_img');
INSERT INTO `exp_html_buttons` VALUES(6, 2, 0, 'b', '<strong>', '</strong>', 'b', 1, '1', 'btn_b');
INSERT INTO `exp_html_buttons` VALUES(7, 2, 0, 'i', '<em>', '</em>', 'i', 2, '1', 'btn_i');
INSERT INTO `exp_html_buttons` VALUES(8, 2, 0, 'blockquote', '<blockquote>', '</blockquote>', 'q', 3, '1', 'btn_blockquote');
INSERT INTO `exp_html_buttons` VALUES(9, 2, 0, 'a', '<a href=\"[![Link:!:http://]!]\"(!( title=\"[![Title]!]\")!)>', '</a>', 'a', 4, '1', 'btn_a');
INSERT INTO `exp_html_buttons` VALUES(10, 2, 0, 'img', '<img src=\"[![Link:!:http://]!]\" alt=\"[![Alternative text]!]\" />', '', '', 5, '1', 'btn_img');

/* Table structure for table `exp_layout_publish` */
DROP TABLE IF EXISTS `exp_layout_publish`;

CREATE TABLE `exp_layout_publish` (
  `layout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_group` int(4) unsigned NOT NULL DEFAULT '0',
  `channel_id` int(4) unsigned NOT NULL DEFAULT '0',
  `field_layout` text,
  PRIMARY KEY (`layout_id`),
  KEY `site_id` (`site_id`),
  KEY `member_group` (`member_group`),
  KEY `channel_id` (`channel_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_layout_publish` */
INSERT INTO `exp_layout_publish` VALUES(10, 1, 1, 2, 'a:4:{s:7:\"publish\";a:6:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:16;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:18;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:17;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:10:\"Categories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}');
INSERT INTO `exp_layout_publish` VALUES(12, 1, 1, 1, 'a:9:{s:7:\"publish\";a:7:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:37;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:1;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:2;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:10;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:15:\"technical_specs\";a:2:{s:10:\"_tab_label\";s:15:\"Technical Specs\";i:3;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:9:\"downloads\";a:2:{s:10:\"_tab_label\";s:9:\"Downloads\";i:11;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:6:\"videos\";a:2:{s:10:\"_tab_label\";s:6:\"Videos\";i:4;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:14:\"rental_details\";a:4:{s:10:\"_tab_label\";s:14:\"Rental Details\";i:5;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:6;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:7;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:16:\"featured_details\";a:4:{s:10:\"_tab_label\";s:16:\"Featured Details\";i:9;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:33;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:8;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:10:\"Categories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:4:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:23:\"comment_expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}');

/* Table structure for table `exp_matrix_cols` */
DROP TABLE IF EXISTS `exp_matrix_cols`;

CREATE TABLE `exp_matrix_cols` (
  `col_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `col_name` varchar(32) DEFAULT NULL,
  `col_label` varchar(50) DEFAULT NULL,
  `col_instructions` text,
  `col_type` varchar(50) DEFAULT 'text',
  `col_required` char(1) DEFAULT 'n',
  `col_search` char(1) DEFAULT 'n',
  `col_order` int(3) unsigned DEFAULT NULL,
  `col_width` varchar(4) DEFAULT NULL,
  `col_settings` text,
  PRIMARY KEY (`col_id`),
  KEY `site_id` (`site_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_matrix_cols` */
INSERT INTO `exp_matrix_cols` VALUES(1, 1, 1, null, 'currency', 'Currency', '', 'pt_dropdown', 'n', 'y', 0, '33%', 'YToxOntzOjc6Im9wdGlvbnMiO2E6Mjp7czozOiJOWkQiO3M6MzoiTlpEIjtzOjM6IkFVRCI7czozOiJBVUQiO319');
INSERT INTO `exp_matrix_cols` VALUES(2, 1, 1, null, 'amount', 'Amount', 'Please enter the dollar value (without &#36; or any decimals as they will be output automatically)', 'number', 'n', 'n', 1, '', 'YTozOntzOjk6Im1pbl92YWx1ZSI7czowOiIiO3M6OToibWF4X3ZhbHVlIjtzOjA6IiI7czo4OiJkZWNpbWFscyI7czoxOiIwIjt9');
INSERT INTO `exp_matrix_cols` VALUES(3, 1, 3, null, 'section_title', 'Section Title', 'i.e. Dimensions', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(4, 1, 3, null, 'specification_details', 'Specification Details', 'This is where you enter i.e. Length, 200mm', 'nolan', 'n', 'n', 1, '', 'YToyOntzOjE2OiJub2xhbl9jb2xfbGFiZWxzIjtzOjQwOiJTcGVjaWZpY2F0aW9uIE5hbWUgfCBTcGVjaWZpY2F0aW9uIFZhbHVlIjtzOjE1OiJub2xhbl9jb2xfbmFtZXMiO3M6NDA6InNwZWNpZmljYXRpb25fbmFtZSB8IHNwZWNpZmljYXRpb25fdmFsdWUiO30=');
INSERT INTO `exp_matrix_cols` VALUES(6, 1, 4, null, 'video_heading', 'Video Heading', 'The heading to appear above the video', 'text', 'n', 'n', 0, '25%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(7, 1, 4, null, 'video_embed_url', 'Video Embed URL', 'The embed url from Youtube', 'text', 'n', 'n', 1, '25%', 'YTo0OntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(8, 1, 4, null, 'video_description', 'Video Description', 'This description appears beneath the video', 'wygwam', 'n', 'n', 2, '50%', 'YTozOntzOjc6ImNvbnZlcnQiO3M6MDoiIjtzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(9, 1, 5, null, 'rental_heading', 'Rental Heading', '', 'text', 'n', 'n', 0, '25%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(10, 1, 5, null, 'rental_content', 'Rental Content', '', 'wygwam', 'n', 'n', 1, '75%', 'YTozOntzOjc6ImNvbnZlcnQiO3M6MDoiIjtzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(11, 1, 6, null, 'rental_duration_nz', 'Rental Duration (NZ)', 'i.e. 1-3 days', 'text', 'n', 'n', 0, '20%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(12, 1, 6, null, 'rental_pricing_nz', 'Rental Pricing (NZ)', 'Please exclude the &#36; sign and decimal values.', 'number', 'n', 'n', 1, '', 'YTozOntzOjk6Im1pbl92YWx1ZSI7czowOiIiO3M6OToibWF4X3ZhbHVlIjtzOjA6IiI7czo4OiJkZWNpbWFscyI7czoxOiIwIjt9');
INSERT INTO `exp_matrix_cols` VALUES(13, 1, 7, null, 'rental_duration_au', 'Rental Duration (AU)', 'i.e. 1-3 days', 'text', 'n', 'n', 0, '20%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(14, 1, 7, null, 'rental_pricing_au', 'Rental Pricing (AU)', 'Please exclude the &#36; sign and decimal values.', 'number', 'n', 'n', 1, '', 'YTozOntzOjk6Im1pbl92YWx1ZSI7czowOiIiO3M6OToibWF4X3ZhbHVlIjtzOjA6IiI7czo4OiJkZWNpbWFscyI7czoxOiIwIjt9');
INSERT INTO `exp_matrix_cols` VALUES(15, 1, 8, null, 'featured_banner_image', 'Featured Banner Image', '', 'file', 'n', 'n', 0, '33%', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiIxIjtzOjEyOiJjb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO30=');
INSERT INTO `exp_matrix_cols` VALUES(16, 1, 10, null, 'product_image', 'Product Image', '', 'file', 'n', 'n', 0, '33%', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiIxIjtzOjEyOiJjb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO30=');
INSERT INTO `exp_matrix_cols` VALUES(17, 1, 11, null, 'file_title', 'File Title', '', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(18, 1, 11, null, 'download_file', 'Files', '', 'file', 'n', 'n', 1, '', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiIyIjtzOjEyOiJjb250ZW50X3R5cGUiO3M6MzoiYWxsIjt9');
INSERT INTO `exp_matrix_cols` VALUES(19, 1, 15, null, 'branch_region', 'Branch Region', 'i.e. Auckland/Christchurch', 'text', 'y', 'n', 0, '20%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(20, 1, 15, null, 'contact_information', 'Contact Information', '', 'nolan', 'y', 'n', 1, '', 'YToyOntzOjE2OiJub2xhbl9jb2xfbGFiZWxzIjtzOjI0OiJDb250YWN0IE1ldGhvZCB8IERldGFpbHMiO3M6MTU6Im5vbGFuX2NvbF9uYW1lcyI7czozMjoiY29udGFjdF9tZXRob2QgfCBjb250YWN0X2RldGFpbHMiO30=');
INSERT INTO `exp_matrix_cols` VALUES(21, 1, 15, null, 'operating_hours', 'Operating Hours', 'i.e. 8:00am to 5:00pm', 'text', 'y', 'n', 2, '', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(22, 1, 15, null, 'physical_address', 'Physical Address', '', 'text', 'y', 'n', 3, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czoyOiJiciI7czozOiJkaXIiO3M6MzoibHRyIjt9');
INSERT INTO `exp_matrix_cols` VALUES(23, 1, 15, null, 'postal_address', 'Postal Address', '', 'text', 'y', 'n', 4, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czoyOiJiciI7czozOiJkaXIiO3M6MzoibHRyIjt9');
INSERT INTO `exp_matrix_cols` VALUES(24, 1, 8, null, 'featured_banner_description', 'Featured Banner Description', '', 'text', 'n', 'n', 1, '', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(25, 1, 17, null, 'content', 'Content', '', 'wygwam', 'y', 'n', 0, '', 'YToyOntzOjY6ImNvbmZpZyI7czoxOiI0IjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(26, 1, 22, null, 'banner_large_text', 'Banner Large Text', '', 'text', 'y', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(27, 1, 22, null, 'banner_small_text', 'Banner Small Text', '', 'text', 'n', 'n', 1, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(28, 1, 23, null, 'link_text', 'Link Text', '', 'text', 'y', 'n', 0, '50%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(29, 1, 23, null, 'link_location', 'Link Location', 'The page you want the link to point to. It must be prefixed with \"/\". For example \"/services\"', 'text', 'y', 'n', 1, '', 'YTo0OntzOjQ6Im1heGwiO3M6MDoiIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(30, 1, 26, null, 'content', 'Content', '', 'wygwam', 'n', 'n', 0, '', 'YTozOntzOjc6ImNvbnZlcnQiO3M6MDoiIjtzOjY6ImNvbmZpZyI7czoxOiI0IjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(31, 1, 30, null, 'grid_content', 'Grid Content', '', 'wygwam', 'n', 'n', 1, '', 'YToyOntzOjY6ImNvbmZpZyI7czoxOiIxIjtzOjU6ImRlZmVyIjtzOjE6Im4iO30=');
INSERT INTO `exp_matrix_cols` VALUES(32, 1, 30, null, 'grid_title', 'Grid Title', '', 'text', 'n', 'n', 0, '33%', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(33, 1, 32, null, 'name', 'Name', '', 'text', 'n', 'n', 0, '', 'YTozOntzOjQ6Im1heGwiO3M6MDoiIjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(34, 1, 32, null, 'position', 'Position', '', 'text', 'n', 'n', 1, '', 'YTo0OntzOjQ6Im1heGwiO3M6MzoiMTQwIjtzOjk6Im11bHRpbGluZSI7czoxOiJ5IjtzOjM6ImZtdCI7czo0OiJub25lIjtzOjM6ImRpciI7czozOiJsdHIiO30=');
INSERT INTO `exp_matrix_cols` VALUES(35, 1, 32, null, 'image', 'Image', 'Image dimensions: 153px x 211px. If not supplied a default image is used.', 'file', 'n', 'n', 2, '', 'YToyOntzOjk6ImRpcmVjdG9yeSI7czoxOiI5IjtzOjEyOiJjb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO30=');

/* Table structure for table `exp_matrix_data` */
DROP TABLE IF EXISTS `exp_matrix_data`;

CREATE TABLE `exp_matrix_data` (
  `row_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned DEFAULT '1',
  `entry_id` int(10) unsigned DEFAULT NULL,
  `field_id` int(6) unsigned DEFAULT NULL,
  `var_id` int(6) unsigned DEFAULT NULL,
  `is_draft` tinyint(1) unsigned DEFAULT '0',
  `row_order` int(4) unsigned DEFAULT NULL,
  `col_id_1` text,
  `col_id_2` int(11) DEFAULT '0',
  `col_id_3` text,
  `col_id_4` text,
  `col_id_6` text,
  `col_id_7` text,
  `col_id_8` text,
  `col_id_9` text,
  `col_id_10` text,
  `col_id_11` text,
  `col_id_12` int(11) DEFAULT '0',
  `col_id_13` text,
  `col_id_14` int(11) DEFAULT '0',
  `col_id_15` text,
  `col_id_16` text,
  `col_id_17` text,
  `col_id_18` text,
  `col_id_19` text,
  `col_id_20` text,
  `col_id_21` text,
  `col_id_22` text,
  `col_id_23` text,
  `col_id_24` text,
  `col_id_25` text,
  `col_id_26` text,
  `col_id_27` text,
  `col_id_28` text,
  `col_id_29` text,
  `col_id_30` text,
  `col_id_31` text,
  `col_id_32` text,
  `col_id_33` text,
  `col_id_34` text,
  `col_id_35` text,
  PRIMARY KEY (`row_id`),
  KEY `site_id` (`site_id`),
  KEY `entry_id` (`entry_id`),
  KEY `field_id` (`field_id`),
  KEY `var_id` (`var_id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_matrix_data` */
INSERT INTO `exp_matrix_data` VALUES(1, 1, 1, 1, null, 0, 1, 'NZD', 45000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(2, 1, 1, 1, null, 0, 2, 'AUD', 40000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(3, 1, 1, 3, null, 0, 1, null, 0, 'Telescope', 'a:7:{i:0;a:2:{s:18:\"specification_name\";s:6:\"Length\";s:19:\"specification_value\";s:15:\"150 Millimeters\";}i:1;a:2:{s:18:\"specification_name\";s:23:\"Objective Lens Diameter\";s:19:\"specification_value\";s:15:\"45mm (EDM:50mm)\";}i:2;a:2:{s:18:\"specification_name\";s:13:\"Magnification\";s:19:\"specification_value\";s:3:\"30x\";}i:3;a:2:{s:18:\"specification_name\";s:5:\"Image\";s:19:\"specification_value\";s:5:\"Erect\";}i:4;a:2:{s:18:\"specification_name\";s:13:\"Field of View\";s:19:\"specification_value\";s:3:\"130\";}i:5;a:2:{s:18:\"specification_name\";s:13:\"Resolve Power\";s:19:\"specification_value\";s:2:\"3\"\";}i:6;a:2:{s:18:\"specification_name\";s:18:\"Min Focus Distance\";s:19:\"specification_value\";s:10:\"1.3 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(4, 1, 1, 3, null, 0, 2, null, 0, 'Measurement Range', 'a:2:{i:0;a:2:{s:18:\"specification_name\";s:8:\"1 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}i:1;a:2:{s:18:\"specification_name\";s:8:\"3 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(5, 1, 1, 10, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(6, 1, 1, 11, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, 'Test uploaded file', '{filedir_2}large-product-image4.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(7, 1, 1, 4, null, 0, 1, null, 0, null, null, 'Test Video Of My 4x4', 'http://www.youtube.com/embed/HNIA-nesZEU', '<p>\n	Test vid of my 4x4.&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae gravida tortor. Maecenas turpis purus, imperdiet id elementum eget, feugiat in tellus. Duis molestie, nulla nec sollicitudin consectetur, magna nisl condimentum neque, at consequat magna enim a tellus. Maecenas porttitor mattis aliquet. Ut lobortis, tortor eget lacinia commodo, mi sapien molestie neque, quis mattis metus lorem posuere massa. Vivamus id pretium lacus. Aliquam ac mauris orci, vulputate ullamcorper elit. Donec eget eros eros. Etiam ac sapien a sem ullamcorper consequat. Suspendisse ut elementum arcu. Cras fringilla tortor pellentesque nibh pharetra pellentesque. Sed et mauris sit amet ligula scelerisque sodales sed nec lorem. In vel quam sit amet eros lobortis consectetur sit amet egestas nisl. Cras erat turpis, suscipit posuere mollis nec, dictum eget lectus.</p>', null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(8, 1, 1, 5, null, 0, 1, null, 0, null, null, null, null, null, 'Test heading', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(9, 1, 1, 5, null, 0, 2, null, 0, null, null, null, null, null, 'Test heading 2', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(10, 1, 1, 6, null, 0, 1, null, 0, null, null, null, null, null, null, null, '1-3 days', 100, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(11, 1, 1, 6, null, 0, 2, null, 0, null, null, null, null, null, null, null, '4-6 days', 180, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(12, 1, 1, 7, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, '1-3 days', 80, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(13, 1, 1, 7, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, '4-6 days', 160, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(14, 1, 1, 10, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image6.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(15, 1, 2, 1, null, 0, 1, 'NZD', 45000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(16, 1, 2, 1, null, 0, 2, 'AUD', 40000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(17, 1, 2, 3, null, 0, 1, null, 0, 'Telescope', 'a:7:{i:0;a:2:{s:18:\"specification_name\";s:6:\"Length\";s:19:\"specification_value\";s:15:\"150 Millimeters\";}i:1;a:2:{s:18:\"specification_name\";s:23:\"Objective Lens Diameter\";s:19:\"specification_value\";s:15:\"45mm (EDM:50mm)\";}i:2;a:2:{s:18:\"specification_name\";s:13:\"Magnification\";s:19:\"specification_value\";s:3:\"30x\";}i:3;a:2:{s:18:\"specification_name\";s:5:\"Image\";s:19:\"specification_value\";s:5:\"Erect\";}i:4;a:2:{s:18:\"specification_name\";s:13:\"Field of View\";s:19:\"specification_value\";s:3:\"130\";}i:5;a:2:{s:18:\"specification_name\";s:13:\"Resolve Power\";s:19:\"specification_value\";s:2:\"3\"\";}i:6;a:2:{s:18:\"specification_name\";s:18:\"Min Focus Distance\";s:19:\"specification_value\";s:10:\"1.3 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(18, 1, 2, 3, null, 0, 2, null, 0, 'Measurement Range', 'a:2:{i:0;a:2:{s:18:\"specification_name\";s:8:\"1 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}i:1;a:2:{s:18:\"specification_name\";s:8:\"3 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(19, 1, 2, 4, null, 0, 1, null, 0, null, null, 'Test Video Of My 4x4', 'http://www.youtube.com/embed/HNIA-nesZEU', '<p>\n	Test vid of my 4x4.&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae gravida tortor. Maecenas turpis purus, imperdiet id elementum eget, feugiat in tellus. Duis molestie, nulla nec sollicitudin consectetur, magna nisl condimentum neque, at consequat magna enim a tellus. Maecenas porttitor mattis aliquet. Ut lobortis, tortor eget lacinia commodo, mi sapien molestie neque, quis mattis metus lorem posuere massa. Vivamus id pretium lacus. Aliquam ac mauris orci, vulputate ullamcorper elit. Donec eget eros eros. Etiam ac sapien a sem ullamcorper consequat. Suspendisse ut elementum arcu. Cras fringilla tortor pellentesque nibh pharetra pellentesque. Sed et mauris sit amet ligula scelerisque sodales sed nec lorem. In vel quam sit amet eros lobortis consectetur sit amet egestas nisl. Cras erat turpis, suscipit posuere mollis nec, dictum eget lectus.</p>', null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(20, 1, 2, 5, null, 0, 1, null, 0, null, null, null, null, null, 'Test heading', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(21, 1, 2, 5, null, 0, 2, null, 0, null, null, null, null, null, 'Test heading 2', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(22, 1, 2, 6, null, 0, 1, null, 0, null, null, null, null, null, null, null, '1-3 days', 100, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(23, 1, 2, 6, null, 0, 2, null, 0, null, null, null, null, null, null, null, '4-6 days', 180, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(24, 1, 2, 7, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, '1-3 days', 80, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(25, 1, 2, 7, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, '4-6 days', 160, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(26, 1, 2, 10, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(27, 1, 2, 10, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image6.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(28, 1, 2, 11, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, 'Test uploaded file', '{filedir_2}large-product-image4.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(29, 1, 3, 1, null, 0, 1, 'NZD', 45000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(30, 1, 3, 1, null, 0, 2, 'AUD', 40000, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(31, 1, 3, 3, null, 0, 1, null, 0, 'Telescope', 'a:7:{i:0;a:2:{s:18:\"specification_name\";s:6:\"Length\";s:19:\"specification_value\";s:15:\"150 Millimeters\";}i:1;a:2:{s:18:\"specification_name\";s:23:\"Objective Lens Diameter\";s:19:\"specification_value\";s:15:\"45mm (EDM:50mm)\";}i:2;a:2:{s:18:\"specification_name\";s:13:\"Magnification\";s:19:\"specification_value\";s:3:\"30x\";}i:3;a:2:{s:18:\"specification_name\";s:5:\"Image\";s:19:\"specification_value\";s:5:\"Erect\";}i:4;a:2:{s:18:\"specification_name\";s:13:\"Field of View\";s:19:\"specification_value\";s:3:\"130\";}i:5;a:2:{s:18:\"specification_name\";s:13:\"Resolve Power\";s:19:\"specification_value\";s:2:\"3\"\";}i:6;a:2:{s:18:\"specification_name\";s:18:\"Min Focus Distance\";s:19:\"specification_value\";s:10:\"1.3 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(32, 1, 3, 3, null, 0, 2, null, 0, 'Measurement Range', 'a:2:{i:0;a:2:{s:18:\"specification_name\";s:8:\"1 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}i:1;a:2:{s:18:\"specification_name\";s:8:\"3 Prisim\";s:19:\"specification_value\";s:20:\"6500 ft, 2000 meters\";}}', null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(33, 1, 3, 4, null, 0, 1, null, 0, null, null, 'Test Video Of My 4x4', 'http://www.youtube.com/embed/HNIA-nesZEU', '<p>\n	Test vid of my 4x4.&nbsp;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae gravida tortor. Maecenas turpis purus, imperdiet id elementum eget, feugiat in tellus. Duis molestie, nulla nec sollicitudin consectetur, magna nisl condimentum neque, at consequat magna enim a tellus. Maecenas porttitor mattis aliquet. Ut lobortis, tortor eget lacinia commodo, mi sapien molestie neque, quis mattis metus lorem posuere massa. Vivamus id pretium lacus. Aliquam ac mauris orci, vulputate ullamcorper elit. Donec eget eros eros. Etiam ac sapien a sem ullamcorper consequat. Suspendisse ut elementum arcu. Cras fringilla tortor pellentesque nibh pharetra pellentesque. Sed et mauris sit amet ligula scelerisque sodales sed nec lorem. In vel quam sit amet eros lobortis consectetur sit amet egestas nisl. Cras erat turpis, suscipit posuere mollis nec, dictum eget lectus.</p>', null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(34, 1, 3, 5, null, 0, 1, null, 0, null, null, null, null, null, 'Test heading', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(35, 1, 3, 5, null, 0, 2, null, 0, null, null, null, null, null, 'Test heading 2', '<p>\n	Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.Test content in here.</p>', null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(36, 1, 3, 6, null, 0, 1, null, 0, null, null, null, null, null, null, null, '1-3 days', 100, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(37, 1, 3, 6, null, 0, 2, null, 0, null, null, null, null, null, null, null, '4-6 days', 180, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(38, 1, 3, 7, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, '1-3 days', 80, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(39, 1, 3, 7, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, '4-6 days', 160, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(40, 1, 3, 10, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(41, 1, 3, 10, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image6.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(42, 1, 3, 11, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, 'Test uploaded file', '{filedir_2}large-product-image4.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(43, 1, 11, 15, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, 'Auckland', 'a:4:{i:0;a:2:{s:14:\"contact_method\";s:9:\"Free Call\";s:15:\"contact_details\";s:12:\"0800-867-266\";}i:1;a:2:{s:14:\"contact_method\";s:5:\"Phone\";s:15:\"contact_details\";s:14:\"+64-9-476-5151\";}i:2;a:2:{s:14:\"contact_method\";s:3:\"Fax\";s:15:\"contact_details\";s:14:\"+63-9-476-5140\";}i:3;a:2:{s:14:\"contact_method\";s:5:\"Email\";s:15:\"contact_details\";s:30:\" info@synergypositioning.co.nz\";}}', '8:00am to 5:00pm', '3/52 Arrenway Drive \nAlbany \nAuckland \nNew Zealand', 'P O Box 100450 \nNSMC \nAuckland \nNew Zealand \n0745', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(44, 1, 11, 15, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, 'Christchurch', 'a:4:{i:0;a:2:{s:14:\"contact_method\";s:9:\"Free Call\";s:15:\"contact_details\";s:13:\" 0800-867-266\";}i:1;a:2:{s:14:\"contact_method\";s:5:\"Phone\";s:15:\"contact_details\";s:14:\"+64-9-476-5151\";}i:2;a:2:{s:14:\"contact_method\";s:3:\"Fax\";s:15:\"contact_details\";s:14:\"+63-9-476-5140\";}i:3;a:2:{s:14:\"contact_method\";s:5:\"Email\";s:15:\"contact_details\";s:30:\" info@synergypositioning.co.nz\";}}', '8:00am to 5:00pm', '3/52 Arrenway Drive \nAlbany \nAuckland \nNew Zealand', 'P O Box 100450 \nNSMC \nAuckland \nNew Zealand \n0745', null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(45, 1, 3, 10, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, '{filedir_1}large-product-image6.jpg', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(46, 1, 2, 8, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, '{filedir_1}cat-feature-slide1.jpg', null, null, null, null, null, null, null, null, 'Test description 1', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(47, 1, 2, 8, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, '{filedir_1}cat-feature-slide3.jpg', null, null, null, null, null, null, null, null, 'Test description 2', null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(48, 1, 12, 17, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	BRAMOR Orthophoto</h2>\n<p>\n	The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability, ergonomic and user friendly ground control station, durability and mobility. The airframe is manufactured with CAD/CAM technology from advanced composite materials (Kevlar/Carbon/Vectran) that provide a high level of survivability. A high visibility color scheme and strobe and NAV lights are optional. Embedded autonomous flight procedures include an array of failsafe options based on man-rated standards.</p>\n<p>\n	The system consists of the air vehicle with a 25 megapixel DSLR E/O sensor, a foldable portable take off ramp and rugged ground control station. The whole system fits into one rugged transport case. The system is flight ready in under five minutes. The system is safely operated by one operator/pilot in command.</p>', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(49, 1, 12, 17, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>\n<p>\n	Solutpat metus, nec auctor mi metus eu nibh. Integer id nibh natis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malsapien, et vulputate libero. Praesent interdum risus a libero consequat dapibus. Nulla facilisi. Aliquam erat nisi, placerat id laoreet quis, fringilla ac dolor. Nam interdum tristique augue.</p>\n<p>\n	Nullam accumsan suscipit magna, non vestibulum tortor eleifend eget. Fusce mollis venenatis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.</p>', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(50, 1, 12, 17, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel.</p>\n<p>\n	Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(51, 1, 13, 17, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	BRAMOR Orthophoto</h2>\n<p>\n	The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability, ergonomic and user friendly ground control station, durability and mobility. The airframe is manufactured with CAD/CAM technology from advanced composite materials (Kevlar/Carbon/Vectran) that provide a high level of survivability. A high visibility color scheme and strobe and NAV lights are optional. Embedded autonomous flight procedures include an array of failsafe options based on man-rated standards.</p>\n<p>\n	The system consists of the air vehicle with a 25 megapixel DSLR E/O sensor, a foldable portable take off ramp and rugged ground control station. The whole system fits into one rugged transport case. The system is flight ready in under five minutes. The system is safely operated by one operator/pilot in command.</p>', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(52, 1, 13, 17, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>\n<p>\n	Solutpat metus, nec auctor mi metus eu nibh. Integer id nibh natis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malsapien, et vulputate libero. Praesent interdum risus a libero consequat dapibus. Nulla facilisi. Aliquam erat nisi, placerat id laoreet quis, fringilla ac dolor. Nam interdum tristique augue.</p>\n<p>\n	Nullam accumsan suscipit magna, non vestibulum tortor eleifend eget. Fusce mollis venenatis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.</p>', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(53, 1, 13, 17, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel.</p>\n<p>\n	Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(54, 1, 14, 17, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	BRAMOR Orthophoto</h2>\n<p>\n	The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability, ergonomic and user friendly ground control station, durability and mobility. The airframe is manufactured with CAD/CAM technology from advanced composite materials (Kevlar/Carbon/Vectran) that provide a high level of survivability. A high visibility color scheme and strobe and NAV lights are optional. Embedded autonomous flight procedures include an array of failsafe options based on man-rated standards.</p>\n<p>\n	The system consists of the air vehicle with a 25 megapixel DSLR E/O sensor, a foldable portable take off ramp and rugged ground control station. The whole system fits into one rugged transport case. The system is flight ready in under five minutes. The system is safely operated by one operator/pilot in command.</p>', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(55, 1, 14, 17, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>\n<p>\n	Solutpat metus, nec auctor mi metus eu nibh. Integer id nibh natis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malsapien, et vulputate libero. Praesent interdum risus a libero consequat dapibus. Nulla facilisi. Aliquam erat nisi, placerat id laoreet quis, fringilla ac dolor. Nam interdum tristique augue.</p>\n<p>\n	Nullam accumsan suscipit magna, non vestibulum tortor eleifend eget. Fusce mollis venenatis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.</p>', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(56, 1, 14, 17, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel.</p>\n<p>\n	Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(57, 1, 15, 17, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	BRAMOR Orthophoto</h2>\n<p>\n	The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability, ergonomic and user friendly ground control station, durability and mobility. The airframe is manufactured with CAD/CAM technology from advanced composite materials (Kevlar/Carbon/Vectran) that provide a high level of survivability. A high visibility color scheme and strobe and NAV lights are optional. Embedded autonomous flight procedures include an array of failsafe options based on man-rated standards.</p>\n<p>\n	The system consists of the air vehicle with a 25 megapixel DSLR E/O sensor, a foldable portable take off ramp and rugged ground control station. The whole system fits into one rugged transport case. The system is flight ready in under five minutes. The system is safely operated by one operator/pilot in command.</p>', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(58, 1, 15, 17, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>\n<p>\n	Solutpat metus, nec auctor mi metus eu nibh. Integer id nibh natis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malsapien, et vulputate libero. Praesent interdum risus a libero consequat dapibus. Nulla facilisi. Aliquam erat nisi, placerat id laoreet quis, fringilla ac dolor. Nam interdum tristique augue.</p>\n<p>\n	Nullam accumsan suscipit magna, non vestibulum tortor eleifend eget. Fusce mollis venenatis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.</p>', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(59, 1, 15, 17, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, '<h2>\n	Lorem Ipsum dolor sit</h2>\n<p>\n	<br />\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel.</p>\n<p>\n	Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(60, 1, 23, 22, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, 'Services.', 'Marketing message here', null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(61, 1, 23, 23, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, 'Read more about services', '/services', null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(62, 1, 24, 22, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, 'Trusted precision and control.', 'Marketing message here', null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(63, 1, 24, 23, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, 'Browse, enquire & hire from our product range', '/products', null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(64, 1, 25, 22, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, 'Synergy Positioning Systems', 'Marketing message here.', null, null, null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(65, 1, 25, 23, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, 'Read more about Synergy', '/about-us', null, null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(66, 1, 26, 26, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.</p>', null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(67, 1, 26, 26, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	<img alt=\"\" src=\"{filedir_5}news-temp-top_(1).jpg\" style=\"width: 200px; height: 86px; float: right;\" /></p>\n<p>\n	L</p>\n<p>\n	orem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odi</p>\n<p>\n	o, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus fringilla, massa a sagittis mollis, leo elit ornare justo, et placerat massa sem ut mauris. Vestibulum lacinia viverra dui ut faucibus. Sed aliquet leo a metus rhoncus id pulvinar nisi egestas. Quisque eu ornare dolor. Etiam sollicitudin venenatis libero sit amet imperdiet. Praesent feugiat, lectus vel suscipit blandit, magna massa ultrices odio, quis porta nisl metus in augue. Cras ac justo ac ligula mattis scelerisque. Etiam sed lorem ullamcorper nisi iaculis dignissim.</p>', null, null, null, null, null);
INSERT INTO `exp_matrix_data` VALUES(68, 1, 27, 30, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	You are dealing with an established company representing world renowned brands that have been tried and tested in increasing productivity and reducing costs.</p>', 'Simple really....', null, null, null);
INSERT INTO `exp_matrix_data` VALUES(69, 1, 27, 30, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	With our equipment and systems, you gain efficiency &amp; reduce costs, saving you money on all aspects of the job including: fuel, wages, wear &amp; tear, materials &amp; processing.</p>', 'Your profit...\n', null, null, null);
INSERT INTO `exp_matrix_data` VALUES(70, 1, 27, 30, null, 0, 3, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	We only represent equipment &amp; brands that we trust. Topcon, world leaders in positioning equipment for over 80 years &amp; well established throughout the world, offers reliable &amp; robust equipment for any application.</p>', 'Our Products\n', null, null, null);
INSERT INTO `exp_matrix_data` VALUES(71, 1, 27, 30, null, 0, 4, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	Synergy Positioning Systems Ltd is 100% New Zealand owned and operated. Our industry exprience spans more than 30 years.</p>', 'Our Experience\n', null, null, null);
INSERT INTO `exp_matrix_data` VALUES(72, 1, 27, 30, null, 0, 5, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '<p>\n	We are commited to our customers. Your success is our reward. Our factory trained technicians and in-house industry experts ensure that what we sell is fully supported.</p>', 'Our Service\n', null, null, null);
INSERT INTO `exp_matrix_data` VALUES(73, 1, 28, 32, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'James McFall', 'The man', '{filedir_9}james.jpg');
INSERT INTO `exp_matrix_data` VALUES(74, 1, 28, 32, null, 0, 2, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'John Smith', 'Unknown', null);
INSERT INTO `exp_matrix_data` VALUES(75, 1, 1, 8, null, 0, 1, null, 0, null, null, null, null, null, null, null, null, 0, null, 0, '{filedir_1}cat-feature-slide3.jpg', null, null, null, null, null, null, null, null, '', null, null, null, null, null, null, null, null, null, null, null);

/* Table structure for table `exp_member_bulletin_board` */
DROP TABLE IF EXISTS `exp_member_bulletin_board`;

CREATE TABLE `exp_member_bulletin_board` (
  `bulletin_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `bulletin_group` int(8) unsigned NOT NULL,
  `bulletin_date` int(10) unsigned NOT NULL,
  `hash` varchar(10) NOT NULL DEFAULT '',
  `bulletin_expires` int(10) unsigned NOT NULL DEFAULT '0',
  `bulletin_message` text NOT NULL,
  PRIMARY KEY (`bulletin_id`),
  KEY `sender_id` (`sender_id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_member_data` */
DROP TABLE IF EXISTS `exp_member_data`;

CREATE TABLE `exp_member_data` (
  `member_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_member_data` */
INSERT INTO `exp_member_data` VALUES(1);

/* Table structure for table `exp_member_fields` */
DROP TABLE IF EXISTS `exp_member_fields`;

CREATE TABLE `exp_member_fields` (
  `m_field_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `m_field_name` varchar(32) NOT NULL,
  `m_field_label` varchar(50) NOT NULL,
  `m_field_description` text NOT NULL,
  `m_field_type` varchar(12) NOT NULL DEFAULT 'text',
  `m_field_list_items` text NOT NULL,
  `m_field_ta_rows` tinyint(2) DEFAULT '8',
  `m_field_maxl` smallint(3) NOT NULL,
  `m_field_width` varchar(6) NOT NULL,
  `m_field_search` char(1) NOT NULL DEFAULT 'y',
  `m_field_required` char(1) NOT NULL DEFAULT 'n',
  `m_field_public` char(1) NOT NULL DEFAULT 'y',
  `m_field_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_cp_reg` char(1) NOT NULL DEFAULT 'n',
  `m_field_fmt` char(5) NOT NULL DEFAULT 'none',
  `m_field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`m_field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_member_groups` */
DROP TABLE IF EXISTS `exp_member_groups`;

CREATE TABLE `exp_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_title` varchar(100) NOT NULL,
  `group_description` text NOT NULL,
  `is_locked` char(1) NOT NULL DEFAULT 'y',
  `can_view_offline_system` char(1) NOT NULL DEFAULT 'n',
  `can_view_online_system` char(1) NOT NULL DEFAULT 'y',
  `can_access_cp` char(1) NOT NULL DEFAULT 'y',
  `can_access_content` char(1) NOT NULL DEFAULT 'n',
  `can_access_publish` char(1) NOT NULL DEFAULT 'n',
  `can_access_edit` char(1) NOT NULL DEFAULT 'n',
  `can_access_files` char(1) NOT NULL DEFAULT 'n',
  `can_access_fieldtypes` char(1) NOT NULL DEFAULT 'n',
  `can_access_design` char(1) NOT NULL DEFAULT 'n',
  `can_access_addons` char(1) NOT NULL DEFAULT 'n',
  `can_access_modules` char(1) NOT NULL DEFAULT 'n',
  `can_access_extensions` char(1) NOT NULL DEFAULT 'n',
  `can_access_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_access_plugins` char(1) NOT NULL DEFAULT 'n',
  `can_access_members` char(1) NOT NULL DEFAULT 'n',
  `can_access_admin` char(1) NOT NULL DEFAULT 'n',
  `can_access_sys_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_content_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_access_tools` char(1) NOT NULL DEFAULT 'n',
  `can_access_comm` char(1) NOT NULL DEFAULT 'n',
  `can_access_utilities` char(1) NOT NULL DEFAULT 'n',
  `can_access_data` char(1) NOT NULL DEFAULT 'n',
  `can_access_logs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_channels` char(1) NOT NULL DEFAULT 'n',
  `can_admin_upload_prefs` char(1) NOT NULL DEFAULT 'n',
  `can_admin_design` char(1) NOT NULL DEFAULT 'n',
  `can_admin_members` char(1) NOT NULL DEFAULT 'n',
  `can_delete_members` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_groups` char(1) NOT NULL DEFAULT 'n',
  `can_admin_mbr_templates` char(1) NOT NULL DEFAULT 'n',
  `can_ban_users` char(1) NOT NULL DEFAULT 'n',
  `can_admin_modules` char(1) NOT NULL DEFAULT 'n',
  `can_admin_templates` char(1) NOT NULL DEFAULT 'n',
  `can_admin_accessories` char(1) NOT NULL DEFAULT 'n',
  `can_edit_categories` char(1) NOT NULL DEFAULT 'n',
  `can_delete_categories` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_edit_other_entries` char(1) NOT NULL DEFAULT 'n',
  `can_assign_post_authors` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self_entries` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_entries` char(1) NOT NULL DEFAULT 'n',
  `can_view_other_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_own_comments` char(1) NOT NULL DEFAULT 'n',
  `can_edit_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_delete_all_comments` char(1) NOT NULL DEFAULT 'n',
  `can_moderate_comments` char(1) NOT NULL DEFAULT 'n',
  `can_send_email` char(1) NOT NULL DEFAULT 'n',
  `can_send_cached_email` char(1) NOT NULL DEFAULT 'n',
  `can_email_member_groups` char(1) NOT NULL DEFAULT 'n',
  `can_email_mailinglist` char(1) NOT NULL DEFAULT 'n',
  `can_email_from_profile` char(1) NOT NULL DEFAULT 'n',
  `can_view_profiles` char(1) NOT NULL DEFAULT 'n',
  `can_edit_html_buttons` char(1) NOT NULL DEFAULT 'n',
  `can_delete_self` char(1) NOT NULL DEFAULT 'n',
  `mbr_delete_notify_emails` varchar(255) DEFAULT NULL,
  `can_post_comments` char(1) NOT NULL DEFAULT 'n',
  `exclude_from_moderation` char(1) NOT NULL DEFAULT 'n',
  `can_search` char(1) NOT NULL DEFAULT 'n',
  `search_flood_control` mediumint(5) unsigned NOT NULL,
  `can_send_private_messages` char(1) NOT NULL DEFAULT 'n',
  `prv_msg_send_limit` smallint(5) unsigned NOT NULL DEFAULT '20',
  `prv_msg_storage_limit` smallint(5) unsigned NOT NULL DEFAULT '60',
  `can_attach_in_private_messages` char(1) NOT NULL DEFAULT 'n',
  `can_send_bulletins` char(1) NOT NULL DEFAULT 'n',
  `include_in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `include_in_memberlist` char(1) NOT NULL DEFAULT 'y',
  `include_in_mailinglists` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`group_id`,`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_member_groups` */
INSERT INTO `exp_member_groups` VALUES(1, 1, 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y');
INSERT INTO `exp_member_groups` VALUES(1, 2, 'Super Admins', '', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', '', 'y', 'y', 'y', 0, 'y', 20, 60, 'y', 'y', 'y', 'y', 'y');
INSERT INTO `exp_member_groups` VALUES(2, 1, 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', 60, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(2, 2, 'Banned', '', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', '', 'n', 'n', 'n', 60, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(3, 1, 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(3, 2, 'Guests', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(4, 1, 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(4, 2, 'Pending', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'n', 'n', 'n', 'n', '', 'y', 'n', 'y', 15, 'n', 20, 60, 'n', 'n', 'n', 'n', 'n');
INSERT INTO `exp_member_groups` VALUES(5, 1, 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', 10, 'y', 20, 60, 'y', 'n', 'n', 'y', 'y');
INSERT INTO `exp_member_groups` VALUES(5, 2, 'Members', '', 'y', 'n', 'y', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'y', 'y', 'y', 'n', '', 'y', 'n', 'y', 10, 'y', 20, 60, 'y', 'n', 'n', 'y', 'y');

/* Table structure for table `exp_member_homepage` */
DROP TABLE IF EXISTS `exp_member_homepage`;

CREATE TABLE `exp_member_homepage` (
  `member_id` int(10) unsigned NOT NULL,
  `recent_entries` char(1) NOT NULL DEFAULT 'l',
  `recent_entries_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_comments` char(1) NOT NULL DEFAULT 'l',
  `recent_comments_order` int(3) unsigned NOT NULL DEFAULT '0',
  `recent_members` char(1) NOT NULL DEFAULT 'n',
  `recent_members_order` int(3) unsigned NOT NULL DEFAULT '0',
  `site_statistics` char(1) NOT NULL DEFAULT 'r',
  `site_statistics_order` int(3) unsigned NOT NULL DEFAULT '0',
  `member_search_form` char(1) NOT NULL DEFAULT 'n',
  `member_search_form_order` int(3) unsigned NOT NULL DEFAULT '0',
  `notepad` char(1) NOT NULL DEFAULT 'r',
  `notepad_order` int(3) unsigned NOT NULL DEFAULT '0',
  `bulletin_board` char(1) NOT NULL DEFAULT 'r',
  `bulletin_board_order` int(3) unsigned NOT NULL DEFAULT '0',
  `pmachine_news_feed` char(1) NOT NULL DEFAULT 'n',
  `pmachine_news_feed_order` int(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_member_homepage` */
INSERT INTO `exp_member_homepage` VALUES(1, 'l', 1, 'l', 2, 'n', 0, 'r', 1, 'n', 0, 'r', 2, 'r', 0, 'l', 0);

/* Table structure for table `exp_member_search` */
DROP TABLE IF EXISTS `exp_member_search`;

CREATE TABLE `exp_member_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `search_date` int(10) unsigned NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `fields` varchar(200) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(8) unsigned NOT NULL,
  `query` text NOT NULL,
  PRIMARY KEY (`search_id`),
  KEY `member_id` (`member_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_members` */
DROP TABLE IF EXISTS `exp_members`;

CREATE TABLE `exp_members` (
  `member_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` smallint(4) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL DEFAULT '',
  `unique_id` varchar(40) NOT NULL,
  `crypt_key` varchar(40) DEFAULT NULL,
  `authcode` varchar(10) DEFAULT NULL,
  `email` varchar(72) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `occupation` varchar(80) DEFAULT NULL,
  `interests` varchar(120) DEFAULT NULL,
  `bday_d` int(2) DEFAULT NULL,
  `bday_m` int(2) DEFAULT NULL,
  `bday_y` int(4) DEFAULT NULL,
  `aol_im` varchar(50) DEFAULT NULL,
  `yahoo_im` varchar(50) DEFAULT NULL,
  `msn_im` varchar(50) DEFAULT NULL,
  `icq` varchar(50) DEFAULT NULL,
  `bio` text,
  `signature` text,
  `avatar_filename` varchar(120) DEFAULT NULL,
  `avatar_width` int(4) unsigned DEFAULT NULL,
  `avatar_height` int(4) unsigned DEFAULT NULL,
  `photo_filename` varchar(120) DEFAULT NULL,
  `photo_width` int(4) unsigned DEFAULT NULL,
  `photo_height` int(4) unsigned DEFAULT NULL,
  `sig_img_filename` varchar(120) DEFAULT NULL,
  `sig_img_width` int(4) unsigned DEFAULT NULL,
  `sig_img_height` int(4) unsigned DEFAULT NULL,
  `ignore_list` text,
  `private_messages` int(4) unsigned NOT NULL DEFAULT '0',
  `accept_messages` char(1) NOT NULL DEFAULT 'y',
  `last_view_bulletins` int(10) NOT NULL DEFAULT '0',
  `last_bulletin_date` int(10) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `join_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visit` int(10) unsigned NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `total_entries` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_comments` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_email_date` int(10) unsigned NOT NULL DEFAULT '0',
  `in_authorlist` char(1) NOT NULL DEFAULT 'n',
  `accept_admin_email` char(1) NOT NULL DEFAULT 'y',
  `accept_user_email` char(1) NOT NULL DEFAULT 'y',
  `notify_by_default` char(1) NOT NULL DEFAULT 'y',
  `notify_of_pm` char(1) NOT NULL DEFAULT 'y',
  `display_avatars` char(1) NOT NULL DEFAULT 'y',
  `display_signatures` char(1) NOT NULL DEFAULT 'y',
  `parse_smileys` char(1) NOT NULL DEFAULT 'y',
  `smart_notifications` char(1) NOT NULL DEFAULT 'y',
  `language` varchar(50) NOT NULL,
  `timezone` varchar(8) NOT NULL,
  `daylight_savings` char(1) NOT NULL DEFAULT 'n',
  `localization_is_site_default` char(1) NOT NULL DEFAULT 'n',
  `time_format` char(2) NOT NULL DEFAULT 'us',
  `cp_theme` varchar(32) DEFAULT NULL,
  `profile_theme` varchar(32) DEFAULT NULL,
  `forum_theme` varchar(32) DEFAULT NULL,
  `tracker` text,
  `template_size` varchar(2) NOT NULL DEFAULT '20',
  `notepad` text,
  `notepad_size` varchar(2) NOT NULL DEFAULT '18',
  `quick_links` text,
  `quick_tabs` text,
  `show_sidebar` char(1) NOT NULL DEFAULT 'n',
  `pmember_id` int(10) NOT NULL DEFAULT '0',
  `rte_enabled` char(1) NOT NULL DEFAULT 'y',
  `rte_toolset_id` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`member_id`),
  KEY `group_id` (`group_id`),
  KEY `unique_id` (`unique_id`),
  KEY `password` (`password`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_members` */
INSERT INTO `exp_members` VALUES(1, 1, '96black', '96black', '106cfaa54b9caa63de2a620edb474b900a68d8e76e79847360bbef4323d51b29ea822b5dfe7f5352be760a6ae6a6de7b8abb78f37ad429db302a103d864acad8', '=WDztY<`dk>S{94N7[sY$1\'WCM{:qPT;Z{P44_rlj\'GU8,WH]l$%3d27{+oz,-=(&Ed*nE>q)!+z4p=a;?`yYL|]l:.`/U.=P3lU@\"n*jX_-=GO>!E+?()Cqj<ai>Q=a', '2d3c4dab12f0973e5396766a007ac2efd798eb16', 'b9cbee325c2edd1db5f8b3ef603a2ad0bb317a5f', null, 'james@96black.co.nz', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 0, 'y', 0, 0, '127.0.0.1', 1355879065, 1365848009, 1365930436, 29, 0, 0, 0, 1365731496, 0, 0, 0, 'n', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'y', 'english', 'UP12', 'y', 'n', 'us', null, null, null, null, '20', null, '18', '', 'Template Manager|C=design&M=manager|1', 'n', 0, 'y', 0);

/* Table structure for table `exp_message_attachments` */
DROP TABLE IF EXISTS `exp_message_attachments`;

CREATE TABLE `exp_message_attachments` (
  `attachment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_name` varchar(50) NOT NULL DEFAULT '',
  `attachment_hash` varchar(40) NOT NULL DEFAULT '',
  `attachment_extension` varchar(20) NOT NULL DEFAULT '',
  `attachment_location` varchar(150) NOT NULL DEFAULT '',
  `attachment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_size` int(10) unsigned NOT NULL DEFAULT '0',
  `is_temp` char(1) NOT NULL DEFAULT 'y',
  PRIMARY KEY (`attachment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_message_copies` */
DROP TABLE IF EXISTS `exp_message_copies`;

CREATE TABLE `exp_message_copies` (
  `copy_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `message_id` int(10) unsigned NOT NULL DEFAULT '0',
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `recipient_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_received` char(1) NOT NULL DEFAULT 'n',
  `message_read` char(1) NOT NULL DEFAULT 'n',
  `message_time_read` int(10) unsigned NOT NULL DEFAULT '0',
  `attachment_downloaded` char(1) NOT NULL DEFAULT 'n',
  `message_folder` int(10) unsigned NOT NULL DEFAULT '1',
  `message_authcode` varchar(10) NOT NULL DEFAULT '',
  `message_deleted` char(1) NOT NULL DEFAULT 'n',
  `message_status` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`copy_id`),
  KEY `message_id` (`message_id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_message_data` */
DROP TABLE IF EXISTS `exp_message_data`;

CREATE TABLE `exp_message_data` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL DEFAULT '0',
  `message_date` int(10) unsigned NOT NULL DEFAULT '0',
  `message_subject` varchar(255) NOT NULL DEFAULT '',
  `message_body` text NOT NULL,
  `message_tracking` char(1) NOT NULL DEFAULT 'y',
  `message_attachments` char(1) NOT NULL DEFAULT 'n',
  `message_recipients` varchar(200) NOT NULL DEFAULT '',
  `message_cc` varchar(200) NOT NULL DEFAULT '',
  `message_hide_cc` char(1) NOT NULL DEFAULT 'n',
  `message_sent_copy` char(1) NOT NULL DEFAULT 'n',
  `total_recipients` int(5) unsigned NOT NULL DEFAULT '0',
  `message_status` varchar(25) NOT NULL DEFAULT '',
  PRIMARY KEY (`message_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_message_folders` */
DROP TABLE IF EXISTS `exp_message_folders`;

CREATE TABLE `exp_message_folders` (
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `folder1_name` varchar(50) NOT NULL DEFAULT 'InBox',
  `folder2_name` varchar(50) NOT NULL DEFAULT 'Sent',
  `folder3_name` varchar(50) NOT NULL DEFAULT '',
  `folder4_name` varchar(50) NOT NULL DEFAULT '',
  `folder5_name` varchar(50) NOT NULL DEFAULT '',
  `folder6_name` varchar(50) NOT NULL DEFAULT '',
  `folder7_name` varchar(50) NOT NULL DEFAULT '',
  `folder8_name` varchar(50) NOT NULL DEFAULT '',
  `folder9_name` varchar(50) NOT NULL DEFAULT '',
  `folder10_name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_message_folders` */
INSERT INTO `exp_message_folders` VALUES(1, 'InBox', 'Sent', '', '', '', '', '', '', '', '');

/* Table structure for table `exp_message_listed` */
DROP TABLE IF EXISTS `exp_message_listed`;

CREATE TABLE `exp_message_listed` (
  `listed_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_member` int(10) unsigned NOT NULL DEFAULT '0',
  `listed_description` varchar(100) NOT NULL DEFAULT '',
  `listed_type` varchar(10) NOT NULL DEFAULT 'blocked',
  PRIMARY KEY (`listed_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_module_member_groups` */
DROP TABLE IF EXISTS `exp_module_member_groups`;

CREATE TABLE `exp_module_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `module_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`module_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_modules` */
DROP TABLE IF EXISTS `exp_modules`;

CREATE TABLE `exp_modules` (
  `module_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `module_name` varchar(50) NOT NULL,
  `module_version` varchar(12) NOT NULL,
  `has_cp_backend` char(1) NOT NULL DEFAULT 'n',
  `has_publish_fields` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`module_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_modules` */
INSERT INTO `exp_modules` VALUES(1, 'Comment', '2.3', 'y', 'n');
INSERT INTO `exp_modules` VALUES(2, 'Email', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(3, 'Emoticon', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(4, 'File', '1.0.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(5, 'Jquery', '1.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(6, 'Rss', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(7, 'Safecracker', '2.1', 'y', 'n');
INSERT INTO `exp_modules` VALUES(8, 'Search', '2.2', 'n', 'n');
INSERT INTO `exp_modules` VALUES(9, 'Channel', '2.0.1', 'n', 'n');
INSERT INTO `exp_modules` VALUES(10, 'Member', '2.1', 'n', 'n');
INSERT INTO `exp_modules` VALUES(11, 'Stats', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(12, 'Rte', '1.0', 'y', 'n');
INSERT INTO `exp_modules` VALUES(13, 'Playa', '4.3.3', 'n', 'n');
INSERT INTO `exp_modules` VALUES(14, 'Wygwam', '2.6.3', 'y', 'n');
INSERT INTO `exp_modules` VALUES(15, 'Query', '2.0', 'n', 'n');
INSERT INTO `exp_modules` VALUES(16, 'Freeform', '4.0.11', 'y', 'n');

/* Table structure for table `exp_online_users` */
DROP TABLE IF EXISTS `exp_online_users`;

CREATE TABLE `exp_online_users` (
  `online_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `in_forum` char(1) NOT NULL DEFAULT 'n',
  `name` varchar(50) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `date` int(10) unsigned NOT NULL DEFAULT '0',
  `anon` char(1) NOT NULL,
  PRIMARY KEY (`online_id`),
  KEY `date` (`date`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_online_users` */
INSERT INTO `exp_online_users` VALUES(6, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(8, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(9, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(129, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(140, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(153, 1, 0, 'n', '', '127.0.0.1', 1365930450, '');
INSERT INTO `exp_online_users` VALUES(154, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(156, 2, 0, 'n', '', '127.0.0.1', 1365848246, '');
INSERT INTO `exp_online_users` VALUES(157, 1, 0, 'n', '', '127.0.0.1', 1365930450, '');
INSERT INTO `exp_online_users` VALUES(158, 1, 1, 'n', '96black', '127.0.0.1', 1365929935, '');

/* Table structure for table `exp_password_lockout` */
DROP TABLE IF EXISTS `exp_password_lockout`;

CREATE TABLE `exp_password_lockout` (
  `lockout_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login_date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY (`lockout_id`),
  KEY `login_date` (`login_date`),
  KEY `ip_address` (`ip_address`),
  KEY `user_agent` (`user_agent`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_password_lockout` */
INSERT INTO `exp_password_lockout` VALUES(1, 1365558225, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.160 Safari/537.22', '96black');

/* Table structure for table `exp_ping_servers` */
DROP TABLE IF EXISTS `exp_ping_servers`;

CREATE TABLE `exp_ping_servers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `server_name` varchar(32) NOT NULL,
  `server_url` varchar(150) NOT NULL,
  `port` varchar(4) NOT NULL DEFAULT '80',
  `ping_protocol` varchar(12) NOT NULL DEFAULT 'xmlrpc',
  `is_default` char(1) NOT NULL DEFAULT 'y',
  `server_order` int(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_playa_relationships` */
DROP TABLE IF EXISTS `exp_playa_relationships`;

CREATE TABLE `exp_playa_relationships` (
  `rel_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_entry_id` int(10) unsigned DEFAULT NULL,
  `parent_field_id` int(6) unsigned DEFAULT NULL,
  `parent_col_id` int(6) unsigned DEFAULT NULL,
  `parent_row_id` int(10) unsigned DEFAULT NULL,
  `parent_var_id` int(6) unsigned DEFAULT NULL,
  `child_entry_id` int(10) unsigned DEFAULT NULL,
  `rel_order` int(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `parent_entry_id` (`parent_entry_id`),
  KEY `parent_field_id` (`parent_field_id`),
  KEY `parent_col_id` (`parent_col_id`),
  KEY `parent_row_id` (`parent_row_id`),
  KEY `parent_var_id` (`parent_var_id`),
  KEY `child_entry_id` (`child_entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_relationships` */
DROP TABLE IF EXISTS `exp_relationships`;

CREATE TABLE `exp_relationships` (
  `rel_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `rel_parent_id` int(10) NOT NULL DEFAULT '0',
  `rel_child_id` int(10) NOT NULL DEFAULT '0',
  `rel_type` varchar(12) NOT NULL,
  `rel_data` mediumtext NOT NULL,
  `reverse_rel_data` mediumtext NOT NULL,
  PRIMARY KEY (`rel_id`),
  KEY `rel_parent_id` (`rel_parent_id`),
  KEY `rel_child_id` (`rel_child_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_relationships` */
INSERT INTO `exp_relationships` VALUES(1, 12, 2, 'channel', 'a:3:{s:5:\"query\";O:18:\"CI_DB_mysql_result\":7:{s:7:\"conn_id\";i:0;s:9:\"result_id\";i:0;s:12:\"result_array\";a:1:{i:0;a:133:{s:8:\"entry_id\";s:1:\"2\";s:10:\"channel_id\";s:1:\"1\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:23:\"Topcon GTS-100N Robotic\";s:9:\"url_title\";s:23:\"topcon-gts-100n-robotic\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1358293544\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"01\";s:3:\"day\";s:2:\"16\";s:9:\"edit_date\";s:14:\"20130412020645\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:8:\"Products\";s:12:\"channel_name\";s:8:\"products\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";s:4:\"none\";s:10:\"field_id_2\";s:1211:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>\";s:10:\"field_ft_2\";s:4:\"none\";s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";s:4:\"none\";s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";s:4:\"none\";s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";s:4:\"none\";s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";s:4:\"none\";s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";s:4:\"none\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";s:4:\"none\";s:10:\"field_id_9\";s:3:\"Yes\";s:10:\"field_ft_9\";s:4:\"none\";s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";s:4:\"none\";s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";s:4:\"none\";s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";s:4:\"none\";s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";s:4:\"none\";s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";s:4:\"none\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";s:4:\"none\";s:11:\"field_id_16\";s:1:\"0\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:0:\"\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:0:\"\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:50:\"{filedir_1}mega-menu-feature-product-image_(1).jpg\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"5\";s:11:\"field_ft_37\";s:4:\"none\";}}s:13:\"result_object\";a:0:{}s:11:\"current_row\";i:0;s:8:\"num_rows\";i:1;s:8:\"row_data\";a:133:{s:8:\"entry_id\";s:1:\"2\";s:10:\"channel_id\";s:1:\"1\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:23:\"Topcon GTS-100N Robotic\";s:9:\"url_title\";s:23:\"topcon-gts-100n-robotic\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1358293544\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"01\";s:3:\"day\";s:2:\"16\";s:9:\"edit_date\";s:14:\"20130412020645\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:8:\"Products\";s:12:\"channel_name\";s:8:\"products\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";s:4:\"none\";s:10:\"field_id_2\";s:1211:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>\";s:10:\"field_ft_2\";s:4:\"none\";s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";s:4:\"none\";s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";s:4:\"none\";s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";s:4:\"none\";s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";s:4:\"none\";s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";s:4:\"none\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";s:4:\"none\";s:10:\"field_id_9\";s:3:\"Yes\";s:10:\"field_ft_9\";s:4:\"none\";s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";s:4:\"none\";s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";s:4:\"none\";s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";s:4:\"none\";s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";s:4:\"none\";s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";s:4:\"none\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";s:4:\"none\";s:11:\"field_id_16\";s:1:\"0\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:0:\"\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:0:\"\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:50:\"{filedir_1}mega-menu-feature-product-image_(1).jpg\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"5\";s:11:\"field_ft_37\";s:4:\"none\";}}s:10:\"cats_fixed\";s:1:\"1\";s:10:\"categories\";a:3:{i:0;a:7:{i:0;s:1:\"2\";i:1;s:1:\"0\";i:2;s:7:\"Optical\";i:3;s:24:\"{filedir_3}cat-image.jpg\";i:4;s:25:\"Category description here\";i:5;s:1:\"1\";i:6;s:7:\"optical\";}i:1;a:7:{i:0;s:1:\"3\";i:1;s:1:\"2\";i:2;s:13:\"Total Station\";i:3;s:24:\"{filedir_3}cat-image.jpg\";i:4;s:0:\"\";i:5;s:1:\"1\";i:6;s:13:\"total-station\";}i:2;a:7:{i:0;s:1:\"6\";i:1;s:1:\"3\";i:2;s:12:\"Construction\";i:3;s:24:\"{filedir_3}cat-image.jpg\";i:4;s:0:\"\";i:5;s:1:\"1\";i:6;s:12:\"construction\";}}}', 'a:3:{s:5:\"query\";O:18:\"CI_DB_mysql_result\":7:{s:7:\"conn_id\";i:0;s:9:\"result_id\";i:0;s:12:\"result_array\";a:1:{i:0;a:133:{s:8:\"entry_id\";s:2:\"12\";s:10:\"channel_id\";s:1:\"2\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:17:\"BRAMOR Orthophoto\";s:9:\"url_title\";s:17:\"bramor-orthophoto\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1363822454\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"03\";s:3:\"day\";s:2:\"21\";s:9:\"edit_date\";s:14:\"20130320235515\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:22:\"Featured Product Pages\";s:12:\"channel_name\";s:22:\"featured_product_pages\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"1\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:1:\"1\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:335:\"The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";}}s:13:\"result_object\";a:0:{}s:11:\"current_row\";i:0;s:8:\"num_rows\";i:1;s:8:\"row_data\";a:133:{s:8:\"entry_id\";s:2:\"12\";s:10:\"channel_id\";s:1:\"2\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:17:\"BRAMOR Orthophoto\";s:9:\"url_title\";s:17:\"bramor-orthophoto\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1363822454\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"03\";s:3:\"day\";s:2:\"21\";s:9:\"edit_date\";s:14:\"20130320235515\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:22:\"Featured Product Pages\";s:12:\"channel_name\";s:22:\"featured_product_pages\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"1\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:1:\"1\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:335:\"The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";}}s:10:\"cats_fixed\";s:1:\"1\";s:10:\"categories\";a:0:{}}');
INSERT INTO `exp_relationships` VALUES(2, 13, 2, 'channel', 'a:3:{s:5:\"query\";O:18:\"CI_DB_mysql_result\":7:{s:7:\"conn_id\";i:0;s:9:\"result_id\";i:0;s:12:\"result_array\";a:1:{i:0;a:133:{s:8:\"entry_id\";s:1:\"2\";s:10:\"channel_id\";s:1:\"1\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:23:\"Topcon GTS-100N Robotic\";s:9:\"url_title\";s:23:\"topcon-gts-100n-robotic\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1358293544\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"01\";s:3:\"day\";s:2:\"16\";s:9:\"edit_date\";s:14:\"20130412020645\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:8:\"Products\";s:12:\"channel_name\";s:8:\"products\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";s:4:\"none\";s:10:\"field_id_2\";s:1211:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>\";s:10:\"field_ft_2\";s:4:\"none\";s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";s:4:\"none\";s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";s:4:\"none\";s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";s:4:\"none\";s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";s:4:\"none\";s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";s:4:\"none\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";s:4:\"none\";s:10:\"field_id_9\";s:3:\"Yes\";s:10:\"field_ft_9\";s:4:\"none\";s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";s:4:\"none\";s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";s:4:\"none\";s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";s:4:\"none\";s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";s:4:\"none\";s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";s:4:\"none\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";s:4:\"none\";s:11:\"field_id_16\";s:1:\"0\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:0:\"\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:0:\"\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:50:\"{filedir_1}mega-menu-feature-product-image_(1).jpg\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"5\";s:11:\"field_ft_37\";s:4:\"none\";}}s:13:\"result_object\";a:0:{}s:11:\"current_row\";i:0;s:8:\"num_rows\";i:1;s:8:\"row_data\";a:133:{s:8:\"entry_id\";s:1:\"2\";s:10:\"channel_id\";s:1:\"1\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:23:\"Topcon GTS-100N Robotic\";s:9:\"url_title\";s:23:\"topcon-gts-100n-robotic\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1358293544\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"01\";s:3:\"day\";s:2:\"16\";s:9:\"edit_date\";s:14:\"20130412020645\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:8:\"Products\";s:12:\"channel_name\";s:8:\"products\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";s:4:\"none\";s:10:\"field_id_2\";s:1211:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>\";s:10:\"field_ft_2\";s:4:\"none\";s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";s:4:\"none\";s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";s:4:\"none\";s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";s:4:\"none\";s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";s:4:\"none\";s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";s:4:\"none\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";s:4:\"none\";s:10:\"field_id_9\";s:3:\"Yes\";s:10:\"field_ft_9\";s:4:\"none\";s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";s:4:\"none\";s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";s:4:\"none\";s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";s:4:\"none\";s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";s:4:\"none\";s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";s:4:\"none\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";s:4:\"none\";s:11:\"field_id_16\";s:1:\"0\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:0:\"\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:0:\"\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:50:\"{filedir_1}mega-menu-feature-product-image_(1).jpg\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"5\";s:11:\"field_ft_37\";s:4:\"none\";}}s:10:\"cats_fixed\";s:1:\"1\";s:10:\"categories\";a:3:{i:0;a:7:{i:0;s:1:\"2\";i:1;s:1:\"0\";i:2;s:7:\"Optical\";i:3;s:24:\"{filedir_3}cat-image.jpg\";i:4;s:25:\"Category description here\";i:5;s:1:\"1\";i:6;s:7:\"optical\";}i:1;a:7:{i:0;s:1:\"3\";i:1;s:1:\"2\";i:2;s:13:\"Total Station\";i:3;s:24:\"{filedir_3}cat-image.jpg\";i:4;s:0:\"\";i:5;s:1:\"1\";i:6;s:13:\"total-station\";}i:2;a:7:{i:0;s:1:\"6\";i:1;s:1:\"3\";i:2;s:12:\"Construction\";i:3;s:24:\"{filedir_3}cat-image.jpg\";i:4;s:0:\"\";i:5;s:1:\"1\";i:6;s:12:\"construction\";}}}', 'a:3:{s:5:\"query\";O:18:\"CI_DB_mysql_result\":7:{s:7:\"conn_id\";i:0;s:9:\"result_id\";i:0;s:12:\"result_array\";a:1:{i:0;a:133:{s:8:\"entry_id\";s:2:\"13\";s:10:\"channel_id\";s:1:\"2\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:19:\"BRAMOR Orthophoto 2\";s:9:\"url_title\";s:19:\"bramor-orthophoto-2\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1363822493\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"03\";s:3:\"day\";s:2:\"21\";s:9:\"edit_date\";s:14:\"20130321123453\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:22:\"Featured Product Pages\";s:12:\"channel_name\";s:22:\"featured_product_pages\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"2\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:1:\"1\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:335:\"The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";}}s:13:\"result_object\";a:0:{}s:11:\"current_row\";i:0;s:8:\"num_rows\";i:1;s:8:\"row_data\";a:133:{s:8:\"entry_id\";s:2:\"13\";s:10:\"channel_id\";s:1:\"2\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:19:\"BRAMOR Orthophoto 2\";s:9:\"url_title\";s:19:\"bramor-orthophoto-2\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1363822493\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"03\";s:3:\"day\";s:2:\"21\";s:9:\"edit_date\";s:14:\"20130321123453\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:22:\"Featured Product Pages\";s:12:\"channel_name\";s:22:\"featured_product_pages\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"2\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:1:\"1\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:335:\"The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";}}s:10:\"cats_fixed\";s:1:\"1\";s:10:\"categories\";a:0:{}}');
INSERT INTO `exp_relationships` VALUES(3, 14, 2, 'channel', 'a:3:{s:5:\"query\";O:18:\"CI_DB_mysql_result\":7:{s:7:\"conn_id\";i:0;s:9:\"result_id\";i:0;s:12:\"result_array\";a:1:{i:0;a:133:{s:8:\"entry_id\";s:1:\"2\";s:10:\"channel_id\";s:1:\"1\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:23:\"Topcon GTS-100N Robotic\";s:9:\"url_title\";s:23:\"topcon-gts-100n-robotic\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1358293544\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"01\";s:3:\"day\";s:2:\"16\";s:9:\"edit_date\";s:14:\"20130412020645\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:8:\"Products\";s:12:\"channel_name\";s:8:\"products\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";s:4:\"none\";s:10:\"field_id_2\";s:1211:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>\";s:10:\"field_ft_2\";s:4:\"none\";s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";s:4:\"none\";s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";s:4:\"none\";s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";s:4:\"none\";s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";s:4:\"none\";s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";s:4:\"none\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";s:4:\"none\";s:10:\"field_id_9\";s:3:\"Yes\";s:10:\"field_ft_9\";s:4:\"none\";s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";s:4:\"none\";s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";s:4:\"none\";s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";s:4:\"none\";s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";s:4:\"none\";s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";s:4:\"none\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";s:4:\"none\";s:11:\"field_id_16\";s:1:\"0\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:0:\"\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:0:\"\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:50:\"{filedir_1}mega-menu-feature-product-image_(1).jpg\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"5\";s:11:\"field_ft_37\";s:4:\"none\";}}s:13:\"result_object\";a:0:{}s:11:\"current_row\";i:0;s:8:\"num_rows\";i:1;s:8:\"row_data\";a:133:{s:8:\"entry_id\";s:1:\"2\";s:10:\"channel_id\";s:1:\"1\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:23:\"Topcon GTS-100N Robotic\";s:9:\"url_title\";s:23:\"topcon-gts-100n-robotic\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1358293544\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"01\";s:3:\"day\";s:2:\"16\";s:9:\"edit_date\";s:14:\"20130412020645\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:8:\"Products\";s:12:\"channel_name\";s:8:\"products\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";s:4:\"none\";s:10:\"field_id_2\";s:1211:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>\";s:10:\"field_ft_2\";s:4:\"none\";s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";s:4:\"none\";s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";s:4:\"none\";s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";s:4:\"none\";s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";s:4:\"none\";s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";s:4:\"none\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";s:4:\"none\";s:10:\"field_id_9\";s:3:\"Yes\";s:10:\"field_ft_9\";s:4:\"none\";s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";s:4:\"none\";s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";s:4:\"none\";s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";s:4:\"none\";s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";s:4:\"none\";s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";s:4:\"none\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";s:4:\"none\";s:11:\"field_id_16\";s:1:\"0\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:0:\"\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:0:\"\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:50:\"{filedir_1}mega-menu-feature-product-image_(1).jpg\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"5\";s:11:\"field_ft_37\";s:4:\"none\";}}s:10:\"cats_fixed\";s:1:\"1\";s:10:\"categories\";a:3:{i:0;a:7:{i:0;s:1:\"2\";i:1;s:1:\"0\";i:2;s:7:\"Optical\";i:3;s:24:\"{filedir_3}cat-image.jpg\";i:4;s:25:\"Category description here\";i:5;s:1:\"1\";i:6;s:7:\"optical\";}i:1;a:7:{i:0;s:1:\"3\";i:1;s:1:\"2\";i:2;s:13:\"Total Station\";i:3;s:24:\"{filedir_3}cat-image.jpg\";i:4;s:0:\"\";i:5;s:1:\"1\";i:6;s:13:\"total-station\";}i:2;a:7:{i:0;s:1:\"6\";i:1;s:1:\"3\";i:2;s:12:\"Construction\";i:3;s:24:\"{filedir_3}cat-image.jpg\";i:4;s:0:\"\";i:5;s:1:\"1\";i:6;s:12:\"construction\";}}}', 'a:3:{s:5:\"query\";O:18:\"CI_DB_mysql_result\":7:{s:7:\"conn_id\";i:0;s:9:\"result_id\";i:0;s:12:\"result_array\";a:1:{i:0;a:133:{s:8:\"entry_id\";s:2:\"14\";s:10:\"channel_id\";s:1:\"2\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:19:\"BRAMOR Orthophoto 3\";s:9:\"url_title\";s:19:\"bramor-orthophoto-3\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1363822446\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"03\";s:3:\"day\";s:2:\"21\";s:9:\"edit_date\";s:14:\"20130321123406\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:22:\"Featured Product Pages\";s:12:\"channel_name\";s:22:\"featured_product_pages\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"3\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:1:\"1\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:335:\"The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";}}s:13:\"result_object\";a:0:{}s:11:\"current_row\";i:0;s:8:\"num_rows\";i:1;s:8:\"row_data\";a:133:{s:8:\"entry_id\";s:2:\"14\";s:10:\"channel_id\";s:1:\"2\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:19:\"BRAMOR Orthophoto 3\";s:9:\"url_title\";s:19:\"bramor-orthophoto-3\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1363822446\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"03\";s:3:\"day\";s:2:\"21\";s:9:\"edit_date\";s:14:\"20130321123406\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:22:\"Featured Product Pages\";s:12:\"channel_name\";s:22:\"featured_product_pages\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"3\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:1:\"1\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:335:\"The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";}}s:10:\"cats_fixed\";s:1:\"1\";s:10:\"categories\";a:0:{}}');
INSERT INTO `exp_relationships` VALUES(4, 15, 2, 'channel', 'a:3:{s:5:\"query\";O:18:\"CI_DB_mysql_result\":7:{s:7:\"conn_id\";i:0;s:9:\"result_id\";i:0;s:12:\"result_array\";a:1:{i:0;a:133:{s:8:\"entry_id\";s:1:\"2\";s:10:\"channel_id\";s:1:\"1\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:23:\"Topcon GTS-100N Robotic\";s:9:\"url_title\";s:23:\"topcon-gts-100n-robotic\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1358293544\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"01\";s:3:\"day\";s:2:\"16\";s:9:\"edit_date\";s:14:\"20130412020645\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:8:\"Products\";s:12:\"channel_name\";s:8:\"products\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";s:4:\"none\";s:10:\"field_id_2\";s:1211:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>\";s:10:\"field_ft_2\";s:4:\"none\";s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";s:4:\"none\";s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";s:4:\"none\";s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";s:4:\"none\";s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";s:4:\"none\";s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";s:4:\"none\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";s:4:\"none\";s:10:\"field_id_9\";s:3:\"Yes\";s:10:\"field_ft_9\";s:4:\"none\";s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";s:4:\"none\";s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";s:4:\"none\";s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";s:4:\"none\";s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";s:4:\"none\";s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";s:4:\"none\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";s:4:\"none\";s:11:\"field_id_16\";s:1:\"0\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:0:\"\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:0:\"\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:50:\"{filedir_1}mega-menu-feature-product-image_(1).jpg\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"5\";s:11:\"field_ft_37\";s:4:\"none\";}}s:13:\"result_object\";a:0:{}s:11:\"current_row\";i:0;s:8:\"num_rows\";i:1;s:8:\"row_data\";a:133:{s:8:\"entry_id\";s:1:\"2\";s:10:\"channel_id\";s:1:\"1\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:23:\"Topcon GTS-100N Robotic\";s:9:\"url_title\";s:23:\"topcon-gts-100n-robotic\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1358293544\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"01\";s:3:\"day\";s:2:\"16\";s:9:\"edit_date\";s:14:\"20130412020645\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:8:\"Products\";s:12:\"channel_name\";s:8:\"products\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";s:4:\"none\";s:10:\"field_id_2\";s:1211:\"<p>\n	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>\n<p>\n	Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum</p>\n<ul>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n	<li>\n		Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>\n</ul>\";s:10:\"field_ft_2\";s:4:\"none\";s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";s:4:\"none\";s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";s:4:\"none\";s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";s:4:\"none\";s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";s:4:\"none\";s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";s:4:\"none\";s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";s:4:\"none\";s:10:\"field_id_9\";s:3:\"Yes\";s:10:\"field_ft_9\";s:4:\"none\";s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";s:4:\"none\";s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";s:4:\"none\";s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";s:4:\"none\";s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";s:4:\"none\";s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";s:4:\"none\";s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";s:4:\"none\";s:11:\"field_id_16\";s:1:\"0\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:0:\"\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:0:\"\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:50:\"{filedir_1}mega-menu-feature-product-image_(1).jpg\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"5\";s:11:\"field_ft_37\";s:4:\"none\";}}s:10:\"cats_fixed\";s:1:\"1\";s:10:\"categories\";a:3:{i:0;a:7:{i:0;s:1:\"2\";i:1;s:1:\"0\";i:2;s:7:\"Optical\";i:3;s:24:\"{filedir_3}cat-image.jpg\";i:4;s:25:\"Category description here\";i:5;s:1:\"1\";i:6;s:7:\"optical\";}i:1;a:7:{i:0;s:1:\"3\";i:1;s:1:\"2\";i:2;s:13:\"Total Station\";i:3;s:24:\"{filedir_3}cat-image.jpg\";i:4;s:0:\"\";i:5;s:1:\"1\";i:6;s:13:\"total-station\";}i:2;a:7:{i:0;s:1:\"6\";i:1;s:1:\"3\";i:2;s:12:\"Construction\";i:3;s:24:\"{filedir_3}cat-image.jpg\";i:4;s:0:\"\";i:5;s:1:\"1\";i:6;s:12:\"construction\";}}}', 'a:3:{s:5:\"query\";O:18:\"CI_DB_mysql_result\":7:{s:7:\"conn_id\";i:0;s:9:\"result_id\";i:0;s:12:\"result_array\";a:1:{i:0;a:133:{s:8:\"entry_id\";s:2:\"15\";s:10:\"channel_id\";s:1:\"2\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:19:\"BRAMOR Orthophoto 4\";s:9:\"url_title\";s:19:\"bramor-orthophoto-4\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1363822463\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"03\";s:3:\"day\";s:2:\"21\";s:9:\"edit_date\";s:14:\"20130321123423\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:22:\"Featured Product Pages\";s:12:\"channel_name\";s:22:\"featured_product_pages\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"4\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:1:\"1\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:335:\"The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";}}s:13:\"result_object\";a:0:{}s:11:\"current_row\";i:0;s:8:\"num_rows\";i:1;s:8:\"row_data\";a:133:{s:8:\"entry_id\";s:2:\"15\";s:10:\"channel_id\";s:1:\"2\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:19:\"BRAMOR Orthophoto 4\";s:9:\"url_title\";s:19:\"bramor-orthophoto-4\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1363822463\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"03\";s:3:\"day\";s:2:\"21\";s:9:\"edit_date\";s:14:\"20130321123423\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:22:\"Featured Product Pages\";s:12:\"channel_name\";s:22:\"featured_product_pages\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"4\";s:11:\"field_ft_16\";s:4:\"none\";s:11:\"field_id_17\";s:1:\"1\";s:11:\"field_ft_17\";s:4:\"none\";s:11:\"field_id_18\";s:335:\"The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability.\";s:11:\"field_ft_18\";s:4:\"none\";s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";s:4:\"none\";s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";s:4:\"none\";s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";s:4:\"none\";s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";s:4:\"none\";s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";s:4:\"none\";s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";s:4:\"none\";s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";s:4:\"none\";s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";s:4:\"none\";s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";s:4:\"none\";s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";s:4:\"none\";s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";s:4:\"none\";s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";s:4:\"none\";s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";s:4:\"none\";s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";s:4:\"none\";s:11:\"field_id_34\";s:0:\"\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:0:\"\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:0:\"\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";}}s:10:\"cats_fixed\";s:1:\"1\";s:10:\"categories\";a:0:{}}');
INSERT INTO `exp_relationships` VALUES(5, 2, 29, 'channel', 'a:3:{s:5:\"query\";O:18:\"CI_DB_mysql_result\":7:{s:7:\"conn_id\";i:0;s:9:\"result_id\";i:0;s:12:\"result_array\";a:1:{i:0;a:133:{s:8:\"entry_id\";s:2:\"29\";s:10:\"channel_id\";s:2:\"11\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:6:\"Topcon\";s:9:\"url_title\";s:6:\"topcon\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1365731435\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"04\";s:3:\"day\";s:2:\"12\";s:9:\"edit_date\";s:14:\"20130412145035\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:13:\"Manufacturers\";s:12:\"channel_name\";s:13:\"manufacturers\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"0\";s:11:\"field_ft_16\";N;s:11:\"field_id_17\";s:0:\"\";s:11:\"field_ft_17\";N;s:11:\"field_id_18\";s:0:\"\";s:11:\"field_ft_18\";N;s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";N;s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";N;s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";N;s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";N;s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";N;s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";N;s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";N;s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";N;s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";N;s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";N;s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";N;s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";N;s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";N;s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";N;s:11:\"field_id_34\";s:29:\"{filedir_10}footer-topcon.gif\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:27:\"{filedir_10}topcon-logo.gif\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:46:\"{filedir_10}mega-menu-feature-product-logo.jpg\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";}}s:13:\"result_object\";a:0:{}s:11:\"current_row\";i:0;s:8:\"num_rows\";i:1;s:8:\"row_data\";a:133:{s:8:\"entry_id\";s:2:\"29\";s:10:\"channel_id\";s:2:\"11\";s:14:\"forum_topic_id\";N;s:9:\"author_id\";s:1:\"1\";s:10:\"ip_address\";s:9:\"127.0.0.1\";s:5:\"title\";s:6:\"Topcon\";s:9:\"url_title\";s:6:\"topcon\";s:6:\"status\";s:4:\"open\";s:11:\"dst_enabled\";s:1:\"n\";s:14:\"view_count_one\";s:1:\"0\";s:14:\"view_count_two\";s:1:\"0\";s:16:\"view_count_three\";s:1:\"0\";s:15:\"view_count_four\";s:1:\"0\";s:14:\"allow_comments\";s:1:\"y\";s:23:\"comment_expiration_date\";s:1:\"0\";s:6:\"sticky\";s:1:\"n\";s:10:\"entry_date\";s:10:\"1365731435\";s:4:\"year\";s:4:\"2013\";s:5:\"month\";s:2:\"04\";s:3:\"day\";s:2:\"12\";s:9:\"edit_date\";s:14:\"20130412145035\";s:15:\"expiration_date\";s:1:\"0\";s:19:\"recent_comment_date\";s:1:\"0\";s:13:\"comment_total\";s:1:\"0\";s:13:\"entry_site_id\";s:1:\"1\";s:13:\"channel_title\";s:13:\"Manufacturers\";s:12:\"channel_name\";s:13:\"manufacturers\";s:11:\"channel_url\";s:21:\"http://synergy.local/\";s:11:\"comment_url\";N;s:16:\"comment_moderate\";s:1:\"n\";s:23:\"channel_html_formatting\";s:3:\"all\";s:22:\"channel_allow_img_urls\";s:1:\"y\";s:22:\"channel_auto_link_urls\";s:1:\"n\";s:8:\"username\";s:7:\"96black\";s:5:\"email\";s:19:\"james@96black.co.nz\";s:3:\"url\";N;s:11:\"screen_name\";s:7:\"96black\";s:8:\"location\";N;s:10:\"occupation\";N;s:9:\"interests\";N;s:6:\"aol_im\";N;s:8:\"yahoo_im\";N;s:6:\"msn_im\";N;s:3:\"icq\";N;s:9:\"signature\";N;s:16:\"sig_img_filename\";N;s:13:\"sig_img_width\";N;s:14:\"sig_img_height\";N;s:15:\"avatar_filename\";N;s:12:\"avatar_width\";N;s:13:\"avatar_height\";N;s:14:\"photo_filename\";N;s:11:\"photo_width\";N;s:12:\"photo_height\";N;s:8:\"group_id\";s:1:\"1\";s:9:\"member_id\";s:1:\"1\";s:6:\"bday_d\";N;s:6:\"bday_m\";N;s:6:\"bday_y\";N;s:3:\"bio\";N;s:7:\"site_id\";s:1:\"1\";s:10:\"field_id_1\";s:0:\"\";s:10:\"field_ft_1\";N;s:10:\"field_id_2\";s:0:\"\";s:10:\"field_ft_2\";N;s:10:\"field_id_3\";s:0:\"\";s:10:\"field_ft_3\";N;s:10:\"field_id_4\";s:0:\"\";s:10:\"field_ft_4\";N;s:10:\"field_id_5\";s:0:\"\";s:10:\"field_ft_5\";N;s:10:\"field_id_6\";s:0:\"\";s:10:\"field_ft_6\";N;s:10:\"field_id_7\";s:0:\"\";s:10:\"field_ft_7\";N;s:10:\"field_id_8\";s:0:\"\";s:10:\"field_ft_8\";N;s:10:\"field_id_9\";s:0:\"\";s:10:\"field_ft_9\";N;s:11:\"field_id_10\";s:0:\"\";s:11:\"field_ft_10\";N;s:11:\"field_id_11\";s:0:\"\";s:11:\"field_ft_11\";N;s:11:\"field_id_12\";s:0:\"\";s:11:\"field_ft_12\";N;s:11:\"field_id_13\";s:0:\"\";s:11:\"field_ft_13\";N;s:11:\"field_id_14\";s:0:\"\";s:11:\"field_ft_14\";N;s:11:\"field_id_15\";s:0:\"\";s:11:\"field_ft_15\";N;s:11:\"field_id_16\";s:1:\"0\";s:11:\"field_ft_16\";N;s:11:\"field_id_17\";s:0:\"\";s:11:\"field_ft_17\";N;s:11:\"field_id_18\";s:0:\"\";s:11:\"field_ft_18\";N;s:11:\"field_id_19\";s:0:\"\";s:11:\"field_ft_19\";N;s:11:\"field_id_20\";s:0:\"\";s:11:\"field_ft_20\";N;s:11:\"field_id_22\";s:0:\"\";s:11:\"field_ft_22\";N;s:11:\"field_id_23\";s:0:\"\";s:11:\"field_ft_23\";N;s:11:\"field_id_24\";s:0:\"\";s:11:\"field_ft_24\";N;s:11:\"field_id_25\";s:0:\"\";s:11:\"field_ft_25\";N;s:11:\"field_id_26\";s:0:\"\";s:11:\"field_ft_26\";N;s:11:\"field_id_27\";s:0:\"\";s:11:\"field_ft_27\";N;s:11:\"field_id_28\";s:0:\"\";s:11:\"field_ft_28\";N;s:11:\"field_id_29\";s:0:\"\";s:11:\"field_ft_29\";N;s:11:\"field_id_30\";s:0:\"\";s:11:\"field_ft_30\";N;s:11:\"field_id_31\";s:0:\"\";s:11:\"field_ft_31\";N;s:11:\"field_id_32\";s:0:\"\";s:11:\"field_ft_32\";N;s:11:\"field_id_33\";s:0:\"\";s:11:\"field_ft_33\";N;s:11:\"field_id_34\";s:29:\"{filedir_10}footer-topcon.gif\";s:11:\"field_ft_34\";s:4:\"none\";s:11:\"field_id_35\";s:27:\"{filedir_10}topcon-logo.gif\";s:11:\"field_ft_35\";s:4:\"none\";s:11:\"field_id_36\";s:46:\"{filedir_10}mega-menu-feature-product-logo.jpg\";s:11:\"field_ft_36\";s:4:\"none\";s:11:\"field_id_37\";s:1:\"0\";s:11:\"field_ft_37\";s:4:\"none\";}}s:10:\"cats_fixed\";s:1:\"1\";s:10:\"categories\";a:0:{}}', '');

/* Table structure for table `exp_remember_me` */
DROP TABLE IF EXISTS `exp_remember_me`;

CREATE TABLE `exp_remember_me` (
  `remember_me_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) DEFAULT '0',
  `ip_address` varchar(45) DEFAULT '0',
  `user_agent` varchar(120) DEFAULT '',
  `admin_sess` tinyint(1) DEFAULT '0',
  `site_id` int(4) DEFAULT '1',
  `expiration` int(10) DEFAULT '0',
  `last_refresh` int(10) DEFAULT '0',
  PRIMARY KEY (`remember_me_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_reset_password` */
DROP TABLE IF EXISTS `exp_reset_password`;

CREATE TABLE `exp_reset_password` (
  `reset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL,
  `resetcode` varchar(12) NOT NULL,
  `date` int(10) NOT NULL,
  PRIMARY KEY (`reset_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_revision_tracker` */
DROP TABLE IF EXISTS `exp_revision_tracker`;

CREATE TABLE `exp_revision_tracker` (
  `tracker_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned NOT NULL,
  `item_table` varchar(20) NOT NULL,
  `item_field` varchar(20) NOT NULL,
  `item_date` int(10) NOT NULL,
  `item_author_id` int(10) unsigned NOT NULL,
  `item_data` mediumtext NOT NULL,
  PRIMARY KEY (`tracker_id`),
  KEY `item_id` (`item_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_rte_tools` */
DROP TABLE IF EXISTS `exp_rte_tools`;

CREATE TABLE `exp_rte_tools` (
  `tool_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(75) DEFAULT NULL,
  `class` varchar(75) DEFAULT NULL,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`tool_id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_rte_tools` */
INSERT INTO `exp_rte_tools` VALUES(1, 'Blockquote', 'Blockquote_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(2, 'Bold', 'Bold_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(3, 'Headings', 'Headings_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(4, 'Image', 'Image_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(5, 'Italic', 'Italic_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(6, 'Link', 'Link_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(7, 'Ordered List', 'Ordered_list_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(8, 'Underline', 'Underline_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(9, 'Unordered List', 'Unordered_list_rte', 'y');
INSERT INTO `exp_rte_tools` VALUES(10, 'View Source', 'View_source_rte', 'y');

/* Table structure for table `exp_rte_toolsets` */
DROP TABLE IF EXISTS `exp_rte_toolsets`;

CREATE TABLE `exp_rte_toolsets` (
  `toolset_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(10) DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `tools` text,
  `enabled` char(1) DEFAULT 'y',
  PRIMARY KEY (`toolset_id`),
  KEY `member_id` (`member_id`),
  KEY `enabled` (`enabled`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_rte_toolsets` */
INSERT INTO `exp_rte_toolsets` VALUES(1, 0, 'Default', '3|2|5|1|9|7|6|4|10', 'y');

/* Table structure for table `exp_search` */
DROP TABLE IF EXISTS `exp_search`;

CREATE TABLE `exp_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) NOT NULL DEFAULT '1',
  `search_date` int(10) NOT NULL,
  `keywords` varchar(60) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(6) NOT NULL,
  `per_page` tinyint(3) unsigned NOT NULL,
  `query` mediumtext,
  `custom_fields` mediumtext,
  `result_page` varchar(70) NOT NULL,
  PRIMARY KEY (`search_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


/* Table structure for table `exp_search_log` */
DROP TABLE IF EXISTS `exp_search_log`;

CREATE TABLE `exp_search_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `member_id` int(10) unsigned NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `search_date` int(10) NOT NULL,
  `search_type` varchar(32) NOT NULL,
  `search_terms` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_security_hashes` */
DROP TABLE IF EXISTS `exp_security_hashes`;

CREATE TABLE `exp_security_hashes` (
  `hash_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `hash` varchar(40) NOT NULL,
  PRIMARY KEY (`hash_id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB AUTO_INCREMENT=1080 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_security_hashes` */
INSERT INTO `exp_security_hashes` VALUES(1049, 1365929758, '127.0.0.1', 'd95efa78547051bf4fd9e151195109eb27a66212');
INSERT INTO `exp_security_hashes` VALUES(1050, 1365929769, '127.0.0.1', '209c032bb82366c4f2b9334b887aaf7c023bd4eb');
INSERT INTO `exp_security_hashes` VALUES(1051, 1365929769, '127.0.0.1', '30db7d57763fb0120871dbc37a2e3009426a7b7b');
INSERT INTO `exp_security_hashes` VALUES(1052, 1365929786, '127.0.0.1', '9c38a6c61328f625eb70a3b2d55043e644e76c68');
INSERT INTO `exp_security_hashes` VALUES(1053, 1365929786, '127.0.0.1', '135116eff87f8024e40825534369146acd8b87b2');
INSERT INTO `exp_security_hashes` VALUES(1054, 1365929797, '127.0.0.1', '87474d346898e37777bcb58ed9cd1ee934af7b34');
INSERT INTO `exp_security_hashes` VALUES(1055, 1365929801, '127.0.0.1', '6d78c3ea874d8fda81f031aa0889198969a1e978');
INSERT INTO `exp_security_hashes` VALUES(1056, 1365929806, '127.0.0.1', '5ceacf5454e6aa6d419d56cf50dd0d952dc55a62');
INSERT INTO `exp_security_hashes` VALUES(1057, 1365929842, '127.0.0.1', 'd3e1c350fe5044626e4590fef8394363d0b66429');
INSERT INTO `exp_security_hashes` VALUES(1058, 1365929844, '127.0.0.1', '1ffbafc91c3136235569e58aa1d4383585d926a6');
INSERT INTO `exp_security_hashes` VALUES(1059, 1365929845, '127.0.0.1', '03af3150fee2cc88682633b0520295d1712425df');
INSERT INTO `exp_security_hashes` VALUES(1060, 1365929845, '127.0.0.1', 'c4dce0ffbce85a4d79a068ba4443fc9ff8c5f596');
INSERT INTO `exp_security_hashes` VALUES(1061, 1365929845, '127.0.0.1', 'b6da6cfa5e35295d61a0fd507a699445e70c80f5');
INSERT INTO `exp_security_hashes` VALUES(1062, 1365929845, '127.0.0.1', 'b1e2bfcad5ffb6a19cf3eab9fe9de26b8cf63add');
INSERT INTO `exp_security_hashes` VALUES(1063, 1365929868, '127.0.0.1', '3171e9cd6188eeddaa3ee8c4d28b91149e49c5c7');
INSERT INTO `exp_security_hashes` VALUES(1064, 1365929868, '127.0.0.1', '5c1d114320c42547e1a35b925599d54b0990b248');
INSERT INTO `exp_security_hashes` VALUES(1065, 1365929868, '127.0.0.1', '754c9291b80e2976be0750691cb99fd3319f9ebf');
INSERT INTO `exp_security_hashes` VALUES(1066, 1365929868, '127.0.0.1', '3b8acd7f5916ed49ae4fc389549000e172af18c8');
INSERT INTO `exp_security_hashes` VALUES(1067, 1365929868, '127.0.0.1', 'a31a5aa9b3ec570790f1aaeca9b6c2929a27fce6');
INSERT INTO `exp_security_hashes` VALUES(1068, 1365929873, '127.0.0.1', '8ffc59f172f198a27216d184a102b2a5d414f142');
INSERT INTO `exp_security_hashes` VALUES(1069, 1365929875, '127.0.0.1', '6d36414524c01686c07b49a0165bbfa91ccea308');
INSERT INTO `exp_security_hashes` VALUES(1070, 1365929876, '127.0.0.1', '4c15b8d6fefa00cbd40c868a32034648c0f21420');
INSERT INTO `exp_security_hashes` VALUES(1071, 1365929876, '127.0.0.1', 'd8ab44e35086329339f9a184c8b3913019b008f5');
INSERT INTO `exp_security_hashes` VALUES(1072, 1365929876, '127.0.0.1', 'c24716bd271d7bd6f57fbef2ff742a620c27e5b4');
INSERT INTO `exp_security_hashes` VALUES(1073, 1365929876, '127.0.0.1', 'f33641e6fad406acab9b272526e40dc262248661');
INSERT INTO `exp_security_hashes` VALUES(1074, 1365929882, '127.0.0.1', '75e780d4c6304f1471fa2a6247000bd3743f3ea5');
INSERT INTO `exp_security_hashes` VALUES(1075, 1365929885, '127.0.0.1', 'f541a33557ff3220b875078067cce34d75bb40e8');
INSERT INTO `exp_security_hashes` VALUES(1076, 1365930437, '127.0.0.1', '91f2608591cce7e8f887b4a5826a4e7f12ae3e19');
INSERT INTO `exp_security_hashes` VALUES(1077, 1365930440, '127.0.0.1', '9e5d71240864bb024c70668c695846db7d347d7a');
INSERT INTO `exp_security_hashes` VALUES(1078, 1365930442, '127.0.0.1', 'efa52dbc6d04a744461d78aebfad0cbc8aa5e19e');
INSERT INTO `exp_security_hashes` VALUES(1079, 1365930449, '127.0.0.1', 'c96d4dec4ea7a852049415afb312fb9ff6595ea8');

/* Table structure for table `exp_sessions` */
DROP TABLE IF EXISTS `exp_sessions`;

CREATE TABLE `exp_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `member_id` int(10) NOT NULL DEFAULT '0',
  `admin_sess` tinyint(1) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  KEY `member_id` (`member_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/* dumping data for table `exp_sessions` */
INSERT INTO `exp_sessions` VALUES('5630584b1b91a6e1885c80b427775986ffa88748', 1, 1, '127.0.0.1', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.160 Safari/537.22', 1365930449);

/* Table structure for table `exp_sites` */
DROP TABLE IF EXISTS `exp_sites`;

CREATE TABLE `exp_sites` (
  `site_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `site_label` varchar(100) NOT NULL DEFAULT '',
  `site_name` varchar(50) NOT NULL DEFAULT '',
  `site_description` text,
  `site_system_preferences` mediumtext NOT NULL,
  `site_mailinglist_preferences` text NOT NULL,
  `site_member_preferences` text NOT NULL,
  `site_template_preferences` text NOT NULL,
  `site_channel_preferences` text NOT NULL,
  `site_bootstrap_checksums` text NOT NULL,
  PRIMARY KEY (`site_id`),
  KEY `site_name` (`site_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_sites` */
INSERT INTO `exp_sites` VALUES(1, 'Synergy Positioning Systems (NZ)', 'synergy_nz', '', 'YTo5Mjp7czoxMDoic2l0ZV9pbmRleCI7czowOiIiO3M6ODoic2l0ZV91cmwiO3M6MjE6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsLyI7czoxNjoidGhlbWVfZm9sZGVyX3VybCI7czoyODoiaHR0cDovL3N5bmVyZ3kubG9jYWwvdGhlbWVzLyI7czoxNToid2VibWFzdGVyX2VtYWlsIjtzOjE5OiJqYW1lc0A5NmJsYWNrLmNvLm56IjtzOjE0OiJ3ZWJtYXN0ZXJfbmFtZSI7czowOiIiO3M6MjA6ImNoYW5uZWxfbm9tZW5jbGF0dXJlIjtzOjc6ImNoYW5uZWwiO3M6MTA6Im1heF9jYWNoZXMiO3M6MzoiMTUwIjtzOjExOiJjYXB0Y2hhX3VybCI7czozNzoiaHR0cDovL3N5bmVyZ3kubG9jYWwvaW1hZ2VzL2NhcHRjaGFzLyI7czoxMjoiY2FwdGNoYV9wYXRoIjtzOjU0OiIvVXNlcnMvamFtZXMvUHJvamVjdHMvOTZibGFjay9TeW5lcmd5L2ltYWdlcy9jYXB0Y2hhcy8iO3M6MTI6ImNhcHRjaGFfZm9udCI7czoxOiJ5IjtzOjEyOiJjYXB0Y2hhX3JhbmQiO3M6MToieSI7czoyMzoiY2FwdGNoYV9yZXF1aXJlX21lbWJlcnMiO3M6MToibiI7czoxNzoiZW5hYmxlX2RiX2NhY2hpbmciO3M6MToibiI7czoxODoiZW5hYmxlX3NxbF9jYWNoaW5nIjtzOjE6Im4iO3M6MTg6ImZvcmNlX3F1ZXJ5X3N0cmluZyI7czoxOiJuIjtzOjEzOiJzaG93X3Byb2ZpbGVyIjtzOjE6Im4iO3M6MTg6InRlbXBsYXRlX2RlYnVnZ2luZyI7czoxOiJuIjtzOjE1OiJpbmNsdWRlX3NlY29uZHMiO3M6MToibiI7czoxMzoiY29va2llX2RvbWFpbiI7czowOiIiO3M6MTE6ImNvb2tpZV9wYXRoIjtzOjA6IiI7czoxNzoidXNlcl9zZXNzaW9uX3R5cGUiO3M6MToiYyI7czoxODoiYWRtaW5fc2Vzc2lvbl90eXBlIjtzOjI6ImNzIjtzOjIxOiJhbGxvd191c2VybmFtZV9jaGFuZ2UiO3M6MToieSI7czoxODoiYWxsb3dfbXVsdGlfbG9naW5zIjtzOjE6InkiO3M6MTY6InBhc3N3b3JkX2xvY2tvdXQiO3M6MToieSI7czoyNToicGFzc3dvcmRfbG9ja291dF9pbnRlcnZhbCI7czoxOiIxIjtzOjIwOiJyZXF1aXJlX2lwX2Zvcl9sb2dpbiI7czoxOiJ5IjtzOjIyOiJyZXF1aXJlX2lwX2Zvcl9wb3N0aW5nIjtzOjE6InkiO3M6MjQ6InJlcXVpcmVfc2VjdXJlX3Bhc3N3b3JkcyI7czoxOiJuIjtzOjE5OiJhbGxvd19kaWN0aW9uYXJ5X3B3IjtzOjE6InkiO3M6MjM6Im5hbWVfb2ZfZGljdGlvbmFyeV9maWxlIjtzOjA6IiI7czoxNzoieHNzX2NsZWFuX3VwbG9hZHMiO3M6MToieSI7czoxNToicmVkaXJlY3RfbWV0aG9kIjtzOjg6InJlZGlyZWN0IjtzOjk6ImRlZnRfbGFuZyI7czo3OiJlbmdsaXNoIjtzOjg6InhtbF9sYW5nIjtzOjI6ImVuIjtzOjEyOiJzZW5kX2hlYWRlcnMiO3M6MToieSI7czoxMToiZ3ppcF9vdXRwdXQiO3M6MToibiI7czoxMzoibG9nX3JlZmVycmVycyI7czoxOiJuIjtzOjEzOiJtYXhfcmVmZXJyZXJzIjtzOjM6IjUwMCI7czoxMToidGltZV9mb3JtYXQiO3M6MjoidXMiO3M6MTU6InNlcnZlcl90aW1lem9uZSI7czo0OiJVUDEyIjtzOjEzOiJzZXJ2ZXJfb2Zmc2V0IjtzOjA6IiI7czoxNjoiZGF5bGlnaHRfc2F2aW5ncyI7czoxOiJ5IjtzOjIxOiJkZWZhdWx0X3NpdGVfdGltZXpvbmUiO3M6NDoiVVAxMiI7czoxNjoiZGVmYXVsdF9zaXRlX2RzdCI7czoxOiJ5IjtzOjE1OiJob25vcl9lbnRyeV9kc3QiO3M6MToieSI7czoxMzoibWFpbF9wcm90b2NvbCI7czo0OiJtYWlsIjtzOjExOiJzbXRwX3NlcnZlciI7czowOiIiO3M6MTM6InNtdHBfdXNlcm5hbWUiO3M6MDoiIjtzOjEzOiJzbXRwX3Bhc3N3b3JkIjtzOjA6IiI7czoxMToiZW1haWxfZGVidWciO3M6MToibiI7czoxMzoiZW1haWxfY2hhcnNldCI7czo1OiJ1dGYtOCI7czoxNToiZW1haWxfYmF0Y2htb2RlIjtzOjE6Im4iO3M6MTY6ImVtYWlsX2JhdGNoX3NpemUiO3M6MDoiIjtzOjExOiJtYWlsX2Zvcm1hdCI7czo1OiJwbGFpbiI7czo5OiJ3b3JkX3dyYXAiO3M6MToieSI7czoyMjoiZW1haWxfY29uc29sZV90aW1lbG9jayI7czoxOiI1IjtzOjIyOiJsb2dfZW1haWxfY29uc29sZV9tc2dzIjtzOjE6InkiO3M6ODoiY3BfdGhlbWUiO3M6NzoiZGVmYXVsdCI7czoyMToiZW1haWxfbW9kdWxlX2NhcHRjaGFzIjtzOjE6Im4iO3M6MTY6ImxvZ19zZWFyY2hfdGVybXMiO3M6MToieSI7czoxMjoic2VjdXJlX2Zvcm1zIjtzOjE6InkiO3M6MTk6ImRlbnlfZHVwbGljYXRlX2RhdGEiO3M6MToieSI7czoyNDoicmVkaXJlY3Rfc3VibWl0dGVkX2xpbmtzIjtzOjE6Im4iO3M6MTY6ImVuYWJsZV9jZW5zb3JpbmciO3M6MToibiI7czoxNDoiY2Vuc29yZWRfd29yZHMiO3M6MDoiIjtzOjE4OiJjZW5zb3JfcmVwbGFjZW1lbnQiO3M6MDoiIjtzOjEwOiJiYW5uZWRfaXBzIjtzOjA6IiI7czoxMzoiYmFubmVkX2VtYWlscyI7czowOiIiO3M6MTY6ImJhbm5lZF91c2VybmFtZXMiO3M6MDoiIjtzOjE5OiJiYW5uZWRfc2NyZWVuX25hbWVzIjtzOjA6IiI7czoxMDoiYmFuX2FjdGlvbiI7czo4OiJyZXN0cmljdCI7czoxMToiYmFuX21lc3NhZ2UiO3M6MzQ6IlRoaXMgc2l0ZSBpcyBjdXJyZW50bHkgdW5hdmFpbGFibGUiO3M6MTU6ImJhbl9kZXN0aW5hdGlvbiI7czoyMToiaHR0cDovL3d3dy55YWhvby5jb20vIjtzOjE2OiJlbmFibGVfZW1vdGljb25zIjtzOjE6InkiO3M6MTI6ImVtb3RpY29uX3VybCI7czozNjoiaHR0cDovL3N5bmVyZ3kubG9jYWwvaW1hZ2VzL3NtaWxleXMvIjtzOjE5OiJyZWNvdW50X2JhdGNoX3RvdGFsIjtzOjQ6IjEwMDAiO3M6MTc6Im5ld192ZXJzaW9uX2NoZWNrIjtzOjE6InkiO3M6MTc6ImVuYWJsZV90aHJvdHRsaW5nIjtzOjE6Im4iO3M6MTc6ImJhbmlzaF9tYXNrZWRfaXBzIjtzOjE6InkiO3M6MTQ6Im1heF9wYWdlX2xvYWRzIjtzOjI6IjEwIjtzOjEzOiJ0aW1lX2ludGVydmFsIjtzOjE6IjgiO3M6MTI6ImxvY2tvdXRfdGltZSI7czoyOiIzMCI7czoxNToiYmFuaXNobWVudF90eXBlIjtzOjc6Im1lc3NhZ2UiO3M6MTQ6ImJhbmlzaG1lbnRfdXJsIjtzOjA6IiI7czoxODoiYmFuaXNobWVudF9tZXNzYWdlIjtzOjUwOiJZb3UgaGF2ZSBleGNlZWRlZCB0aGUgYWxsb3dlZCBwYWdlIGxvYWQgZnJlcXVlbmN5LiI7czoxNzoiZW5hYmxlX3NlYXJjaF9sb2ciO3M6MToieSI7czoxOToibWF4X2xvZ2dlZF9zZWFyY2hlcyI7czozOiI1MDAiO3M6MTc6InRoZW1lX2ZvbGRlcl9wYXRoIjtzOjQ1OiIvVXNlcnMvamFtZXMvUHJvamVjdHMvOTZibGFjay9TeW5lcmd5L3RoZW1lcy8iO3M6MTA6ImlzX3NpdGVfb24iO3M6MToieSI7czoxMToicnRlX2VuYWJsZWQiO3M6MToieSI7czoyMjoicnRlX2RlZmF1bHRfdG9vbHNldF9pZCI7czoxOiIxIjt9', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6MzY6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9hdmF0YXJzLyI7czoxMToiYXZhdGFyX3BhdGgiO3M6NTM6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL2F2YXRhcnMvIjtzOjE2OiJhdmF0YXJfbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNzoiYXZhdGFyX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEzOiJhdmF0YXJfbWF4X2tiIjtzOjI6IjUwIjtzOjEzOiJlbmFibGVfcGhvdG9zIjtzOjE6Im4iO3M6OToicGhvdG9fdXJsIjtzOjQyOiJodHRwOi8vc3luZXJneS5sb2NhbC9pbWFnZXMvbWVtYmVyX3Bob3Rvcy8iO3M6MTA6InBob3RvX3BhdGgiO3M6NTk6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjE1OiJwaG90b19tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE2OiJwaG90b19tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMjoicGhvdG9fbWF4X2tiIjtzOjI6IjUwIjtzOjE2OiJhbGxvd19zaWduYXR1cmVzIjtzOjE6InkiO3M6MTM6InNpZ19tYXhsZW5ndGgiO3M6MzoiNTAwIjtzOjIxOiJzaWdfYWxsb3dfaW1nX2hvdGxpbmsiO3M6MToibiI7czoyMDoic2lnX2FsbG93X2ltZ191cGxvYWQiO3M6MToibiI7czoxMToic2lnX2ltZ191cmwiO3M6NTA6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9zaWduYXR1cmVfYXR0YWNobWVudHMvIjtzOjEyOiJzaWdfaW1nX3BhdGgiO3M6Njc6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo2MDoiL1VzZXJzL2phbWVzL1Byb2plY3RzLzk2YmxhY2svU3luZXJneS9pbWFnZXMvcG1fYXR0YWNobWVudHMvIjtzOjIzOiJwcnZfbXNnX21heF9hdHRhY2htZW50cyI7czoxOiIzIjtzOjIyOiJwcnZfbXNnX2F0dGFjaF9tYXhzaXplIjtzOjM6IjI1MCI7czoyMDoicHJ2X21zZ19hdHRhY2hfdG90YWwiO3M6MzoiMTAwIjtzOjE5OiJwcnZfbXNnX2h0bWxfZm9ybWF0IjtzOjQ6InNhZmUiO3M6MTg6InBydl9tc2dfYXV0b19saW5rcyI7czoxOiJ5IjtzOjE3OiJwcnZfbXNnX21heF9jaGFycyI7czo0OiI2MDAwIjtzOjE5OiJtZW1iZXJsaXN0X29yZGVyX2J5IjtzOjExOiJ0b3RhbF9wb3N0cyI7czoyMToibWVtYmVybGlzdF9zb3J0X29yZGVyIjtzOjQ6ImRlc2MiO3M6MjA6Im1lbWJlcmxpc3Rfcm93X2xpbWl0IjtzOjI6IjIwIjt9', 'YTo2OntzOjExOiJzdHJpY3RfdXJscyI7czoxOiJuIjtzOjg6InNpdGVfNDA0IjtzOjA6IiI7czoxOToic2F2ZV90bXBsX3JldmlzaW9ucyI7czoxOiJuIjtzOjE4OiJtYXhfdG1wbF9yZXZpc2lvbnMiO3M6MToiNSI7czoxNToic2F2ZV90bXBsX2ZpbGVzIjtzOjE6InkiO3M6MTg6InRtcGxfZmlsZV9iYXNlcGF0aCI7czo0NjoiL1VzZXJzL2phbWVzbWNmYWxsL1Byb2plY3RzL1N5bmVyZ3kvdGVtcGxhdGVzLyI7fQ==', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YToyOntzOjc6ImVtYWlsZWQiO2E6MDp7fXM6NDU6Ii9Vc2Vycy9qYW1lc21jZmFsbC9Qcm9qZWN0cy9TeW5lcmd5L2luZGV4LnBocCI7czozMjoiZjFiMTliOGE0NDU5NmM1MzQ2MTViZWJlMTMyNmYwNjgiO30=');
INSERT INTO `exp_sites` VALUES(2, 'Synergy Positioning Systems (AU)', 'synergy_au', '', 'YTo5MTp7czoxMDoiaXNfc2l0ZV9vbiI7czoxOiJ5IjtzOjEwOiJzaXRlX2luZGV4IjtzOjA6IiI7czo4OiJzaXRlX3VybCI7czoyMToiaHR0cDovL3N5bmVyZ3kubG9jYWwvIjtzOjE2OiJ0aGVtZV9mb2xkZXJfdXJsIjtzOjI4OiJodHRwOi8vc3luZXJneS5sb2NhbC90aGVtZXMvIjtzOjE3OiJ0aGVtZV9mb2xkZXJfcGF0aCI7czo0NToiL1VzZXJzL2phbWVzL1Byb2plY3RzLzk2YmxhY2svU3luZXJneS90aGVtZXMvIjtzOjE1OiJ3ZWJtYXN0ZXJfZW1haWwiO3M6MTk6ImphbWVzQDk2YmxhY2suY28ubnoiO3M6MTQ6IndlYm1hc3Rlcl9uYW1lIjtzOjA6IiI7czoyMDoiY2hhbm5lbF9ub21lbmNsYXR1cmUiO3M6NzoiY2hhbm5lbCI7czoxMDoibWF4X2NhY2hlcyI7czozOiIxNTAiO3M6MTE6ImNhcHRjaGFfdXJsIjtzOjM3OiJodHRwOi8vc3luZXJneS5sb2NhbC9pbWFnZXMvY2FwdGNoYXMvIjtzOjEyOiJjYXB0Y2hhX3BhdGgiO3M6NTQ6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL2NhcHRjaGFzLyI7czoxMjoiY2FwdGNoYV9mb250IjtzOjE6InkiO3M6MTI6ImNhcHRjaGFfcmFuZCI7czoxOiJ5IjtzOjIzOiJjYXB0Y2hhX3JlcXVpcmVfbWVtYmVycyI7czoxOiJuIjtzOjE3OiJlbmFibGVfZGJfY2FjaGluZyI7czoxOiJuIjtzOjE4OiJlbmFibGVfc3FsX2NhY2hpbmciO3M6MToibiI7czoxODoiZm9yY2VfcXVlcnlfc3RyaW5nIjtzOjE6Im4iO3M6MTM6InNob3dfcHJvZmlsZXIiO3M6MToibiI7czoxODoidGVtcGxhdGVfZGVidWdnaW5nIjtzOjE6Im4iO3M6MTU6ImluY2x1ZGVfc2Vjb25kcyI7czoxOiJuIjtzOjEzOiJjb29raWVfZG9tYWluIjtzOjA6IiI7czoxMToiY29va2llX3BhdGgiO3M6MDoiIjtzOjE3OiJ1c2VyX3Nlc3Npb25fdHlwZSI7czoxOiJjIjtzOjE4OiJhZG1pbl9zZXNzaW9uX3R5cGUiO3M6MjoiY3MiO3M6MjE6ImFsbG93X3VzZXJuYW1lX2NoYW5nZSI7czoxOiJ5IjtzOjE4OiJhbGxvd19tdWx0aV9sb2dpbnMiO3M6MToieSI7czoxNjoicGFzc3dvcmRfbG9ja291dCI7czoxOiJ5IjtzOjI1OiJwYXNzd29yZF9sb2Nrb3V0X2ludGVydmFsIjtzOjE6IjEiO3M6MjA6InJlcXVpcmVfaXBfZm9yX2xvZ2luIjtzOjE6InkiO3M6MjI6InJlcXVpcmVfaXBfZm9yX3Bvc3RpbmciO3M6MToieSI7czoyNDoicmVxdWlyZV9zZWN1cmVfcGFzc3dvcmRzIjtzOjE6Im4iO3M6MTk6ImFsbG93X2RpY3Rpb25hcnlfcHciO3M6MToieSI7czoyMzoibmFtZV9vZl9kaWN0aW9uYXJ5X2ZpbGUiO3M6MDoiIjtzOjE3OiJ4c3NfY2xlYW5fdXBsb2FkcyI7czoxOiJ5IjtzOjE1OiJyZWRpcmVjdF9tZXRob2QiO3M6ODoicmVkaXJlY3QiO3M6OToiZGVmdF9sYW5nIjtzOjc6ImVuZ2xpc2giO3M6ODoieG1sX2xhbmciO3M6MjoiZW4iO3M6MTI6InNlbmRfaGVhZGVycyI7czoxOiJ5IjtzOjExOiJnemlwX291dHB1dCI7czoxOiJuIjtzOjEzOiJsb2dfcmVmZXJyZXJzIjtzOjE6Im4iO3M6MTM6Im1heF9yZWZlcnJlcnMiO3M6MzoiNTAwIjtzOjExOiJ0aW1lX2Zvcm1hdCI7czoyOiJ1cyI7czoxNToic2VydmVyX3RpbWV6b25lIjtzOjQ6IlVQMTIiO3M6MTM6InNlcnZlcl9vZmZzZXQiO3M6MDoiIjtzOjE2OiJkYXlsaWdodF9zYXZpbmdzIjtzOjE6InkiO3M6MjE6ImRlZmF1bHRfc2l0ZV90aW1lem9uZSI7czo0OiJVUDEyIjtzOjE2OiJkZWZhdWx0X3NpdGVfZHN0IjtzOjE6InkiO3M6MTM6Im1haWxfcHJvdG9jb2wiO3M6NDoibWFpbCI7czoxMToic210cF9zZXJ2ZXIiO3M6MDoiIjtzOjEzOiJzbXRwX3VzZXJuYW1lIjtzOjA6IiI7czoxMzoic210cF9wYXNzd29yZCI7czowOiIiO3M6MTE6ImVtYWlsX2RlYnVnIjtzOjE6Im4iO3M6MTM6ImVtYWlsX2NoYXJzZXQiO3M6NToidXRmLTgiO3M6MTU6ImVtYWlsX2JhdGNobW9kZSI7czoxOiJuIjtzOjE2OiJlbWFpbF9iYXRjaF9zaXplIjtzOjA6IiI7czoxMToibWFpbF9mb3JtYXQiO3M6NToicGxhaW4iO3M6OToid29yZF93cmFwIjtzOjE6InkiO3M6MjI6ImVtYWlsX2NvbnNvbGVfdGltZWxvY2siO3M6MToiNSI7czoyMjoibG9nX2VtYWlsX2NvbnNvbGVfbXNncyI7czoxOiJ5IjtzOjg6ImNwX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MjE6ImVtYWlsX21vZHVsZV9jYXB0Y2hhcyI7czoxOiJuIjtzOjE2OiJsb2dfc2VhcmNoX3Rlcm1zIjtzOjE6InkiO3M6MTI6InNlY3VyZV9mb3JtcyI7czoxOiJ5IjtzOjE5OiJkZW55X2R1cGxpY2F0ZV9kYXRhIjtzOjE6InkiO3M6MjQ6InJlZGlyZWN0X3N1Ym1pdHRlZF9saW5rcyI7czoxOiJuIjtzOjE2OiJlbmFibGVfY2Vuc29yaW5nIjtzOjE6Im4iO3M6MTQ6ImNlbnNvcmVkX3dvcmRzIjtzOjA6IiI7czoxODoiY2Vuc29yX3JlcGxhY2VtZW50IjtzOjA6IiI7czoxMDoiYmFubmVkX2lwcyI7czowOiIiO3M6MTM6ImJhbm5lZF9lbWFpbHMiO3M6MDoiIjtzOjE2OiJiYW5uZWRfdXNlcm5hbWVzIjtzOjA6IiI7czoxOToiYmFubmVkX3NjcmVlbl9uYW1lcyI7czowOiIiO3M6MTA6ImJhbl9hY3Rpb24iO3M6ODoicmVzdHJpY3QiO3M6MTE6ImJhbl9tZXNzYWdlIjtzOjM0OiJUaGlzIHNpdGUgaXMgY3VycmVudGx5IHVuYXZhaWxhYmxlIjtzOjE1OiJiYW5fZGVzdGluYXRpb24iO3M6MjE6Imh0dHA6Ly93d3cueWFob28uY29tLyI7czoxNjoiZW5hYmxlX2Vtb3RpY29ucyI7czoxOiJ5IjtzOjEyOiJlbW90aWNvbl91cmwiO3M6MzY6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9zbWlsZXlzLyI7czoxOToicmVjb3VudF9iYXRjaF90b3RhbCI7czo0OiIxMDAwIjtzOjE3OiJuZXdfdmVyc2lvbl9jaGVjayI7czoxOiJ5IjtzOjE3OiJlbmFibGVfdGhyb3R0bGluZyI7czoxOiJuIjtzOjE3OiJiYW5pc2hfbWFza2VkX2lwcyI7czoxOiJ5IjtzOjE0OiJtYXhfcGFnZV9sb2FkcyI7czoyOiIxMCI7czoxMzoidGltZV9pbnRlcnZhbCI7czoxOiI4IjtzOjEyOiJsb2Nrb3V0X3RpbWUiO3M6MjoiMzAiO3M6MTU6ImJhbmlzaG1lbnRfdHlwZSI7czo3OiJtZXNzYWdlIjtzOjE0OiJiYW5pc2htZW50X3VybCI7czowOiIiO3M6MTg6ImJhbmlzaG1lbnRfbWVzc2FnZSI7czo1MDoiWW91IGhhdmUgZXhjZWVkZWQgdGhlIGFsbG93ZWQgcGFnZSBsb2FkIGZyZXF1ZW5jeS4iO3M6MTc6ImVuYWJsZV9zZWFyY2hfbG9nIjtzOjE6InkiO3M6MTk6Im1heF9sb2dnZWRfc2VhcmNoZXMiO3M6MzoiNTAwIjtzOjExOiJydGVfZW5hYmxlZCI7czoxOiJ5IjtzOjIyOiJydGVfZGVmYXVsdF90b29sc2V0X2lkIjtzOjE6IjEiO30=', 'YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==', 'YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6MzY6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9hdmF0YXJzLyI7czoxMToiYXZhdGFyX3BhdGgiO3M6NTM6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL2F2YXRhcnMvIjtzOjE2OiJhdmF0YXJfbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNzoiYXZhdGFyX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEzOiJhdmF0YXJfbWF4X2tiIjtzOjI6IjUwIjtzOjEzOiJlbmFibGVfcGhvdG9zIjtzOjE6Im4iO3M6OToicGhvdG9fdXJsIjtzOjQyOiJodHRwOi8vc3luZXJneS5sb2NhbC9pbWFnZXMvbWVtYmVyX3Bob3Rvcy8iO3M6MTA6InBob3RvX3BhdGgiO3M6NTk6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjE1OiJwaG90b19tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE2OiJwaG90b19tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMjoicGhvdG9fbWF4X2tiIjtzOjI6IjUwIjtzOjE2OiJhbGxvd19zaWduYXR1cmVzIjtzOjE6InkiO3M6MTM6InNpZ19tYXhsZW5ndGgiO3M6MzoiNTAwIjtzOjIxOiJzaWdfYWxsb3dfaW1nX2hvdGxpbmsiO3M6MToibiI7czoyMDoic2lnX2FsbG93X2ltZ191cGxvYWQiO3M6MToibiI7czoxMToic2lnX2ltZ191cmwiO3M6NTA6Imh0dHA6Ly9zeW5lcmd5LmxvY2FsL2ltYWdlcy9zaWduYXR1cmVfYXR0YWNobWVudHMvIjtzOjEyOiJzaWdfaW1nX3BhdGgiO3M6Njc6Ii9Vc2Vycy9qYW1lcy9Qcm9qZWN0cy85NmJsYWNrL1N5bmVyZ3kvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo2MDoiL1VzZXJzL2phbWVzL1Byb2plY3RzLzk2YmxhY2svU3luZXJneS9pbWFnZXMvcG1fYXR0YWNobWVudHMvIjtzOjIzOiJwcnZfbXNnX21heF9hdHRhY2htZW50cyI7czoxOiIzIjtzOjIyOiJwcnZfbXNnX2F0dGFjaF9tYXhzaXplIjtzOjM6IjI1MCI7czoyMDoicHJ2X21zZ19hdHRhY2hfdG90YWwiO3M6MzoiMTAwIjtzOjE5OiJwcnZfbXNnX2h0bWxfZm9ybWF0IjtzOjQ6InNhZmUiO3M6MTg6InBydl9tc2dfYXV0b19saW5rcyI7czoxOiJ5IjtzOjE3OiJwcnZfbXNnX21heF9jaGFycyI7czo0OiI2MDAwIjtzOjE5OiJtZW1iZXJsaXN0X29yZGVyX2J5IjtzOjExOiJ0b3RhbF9wb3N0cyI7czoyMToibWVtYmVybGlzdF9zb3J0X29yZGVyIjtzOjQ6ImRlc2MiO3M6MjA6Im1lbWJlcmxpc3Rfcm93X2xpbWl0IjtzOjI6IjIwIjt9', 'YTo2OntzOjE1OiJzYXZlX3RtcGxfZmlsZXMiO3M6MToieSI7czoxODoidG1wbF9maWxlX2Jhc2VwYXRoIjtzOjQ4OiIvVXNlcnMvamFtZXMvUHJvamVjdHMvOTZibGFjay9TeW5lcmd5L3RlbXBsYXRlcy8iO3M6ODoic2l0ZV80MDQiO3M6MDoiIjtzOjE5OiJzYXZlX3RtcGxfcmV2aXNpb25zIjtzOjE6Im4iO3M6MTg6Im1heF90bXBsX3JldmlzaW9ucyI7czoxOiI1IjtzOjExOiJzdHJpY3RfdXJscyI7czoxOiJuIjt9', 'YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=', 'YToyOntzOjc6ImVtYWlsZWQiO2E6MDp7fXM6NDc6Ii9Vc2Vycy9qYW1lc21jZmFsbC9Qcm9qZWN0cy9TeW5lcmd5QVUvaW5kZXgucGhwIjtzOjMyOiJiZjViZGMzMGYwYWVmZDI4Mjc0MmU4MDUxNTAyMWY4MSI7fQ==');

/* Table structure for table `exp_snippets` */
DROP TABLE IF EXISTS `exp_snippets`;

CREATE TABLE `exp_snippets` (
  `snippet_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) NOT NULL,
  `snippet_name` varchar(75) NOT NULL,
  `snippet_contents` text,
  PRIMARY KEY (`snippet_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_specialty_templates` */
DROP TABLE IF EXISTS `exp_specialty_templates`;

CREATE TABLE `exp_specialty_templates` (
  `template_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `enable_template` char(1) NOT NULL DEFAULT 'y',
  `template_name` varchar(50) NOT NULL,
  `data_title` varchar(80) NOT NULL,
  `template_data` text NOT NULL,
  PRIMARY KEY (`template_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_specialty_templates` */
INSERT INTO `exp_specialty_templates` VALUES(1, 1, 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>');
INSERT INTO `exp_specialty_templates` VALUES(2, 1, 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=\'content-type\' content=\'text/html; charset={charset}\' />\n\n{meta_refresh}\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>');
INSERT INTO `exp_specialty_templates` VALUES(3, 1, 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}');
INSERT INTO `exp_specialty_templates` VALUES(4, 1, 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n');
INSERT INTO `exp_specialty_templates` VALUES(5, 1, 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}');
INSERT INTO `exp_specialty_templates` VALUES(6, 1, 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}');
INSERT INTO `exp_specialty_templates` VALUES(7, 1, 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(8, 1, 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(9, 1, 'y', 'reset_password_notification', 'New Login Information', '{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(10, 1, 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(11, 1, 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe\'re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(12, 1, 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the \"{mailing_list}\" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}');
INSERT INTO `exp_specialty_templates` VALUES(13, 1, 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}');
INSERT INTO `exp_specialty_templates` VALUES(14, 1, 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}');
INSERT INTO `exp_specialty_templates` VALUES(15, 1, 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled �{message_subject}�.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(16, 1, 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(17, 2, 'y', 'offline_template', '', '<html>\n<head>\n\n<title>System Offline</title>\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>');
INSERT INTO `exp_specialty_templates` VALUES(18, 2, 'y', 'message_template', '', '<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=\'content-type\' content=\'text/html; charset={charset}\' />\n\n{meta_refresh}\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>');
INSERT INTO `exp_specialty_templates` VALUES(19, 2, 'y', 'admin_notify_reg', 'Notification of new member registration', 'New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}');
INSERT INTO `exp_specialty_templates` VALUES(20, 2, 'y', 'admin_notify_entry', 'A new channel entry has been posted', 'A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n');
INSERT INTO `exp_specialty_templates` VALUES(21, 2, 'y', 'admin_notify_mailinglist', 'Someone has subscribed to your mailing list', 'A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}');
INSERT INTO `exp_specialty_templates` VALUES(22, 2, 'y', 'admin_notify_comment', 'You have just received a comment', 'You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}');
INSERT INTO `exp_specialty_templates` VALUES(23, 2, 'y', 'mbr_activation_instructions', 'Enclosed is your activation code', 'Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(24, 2, 'y', 'forgot_password_instructions', 'Login information', '{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(25, 2, 'y', 'reset_password_notification', 'New Login Information', '{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(26, 2, 'y', 'validated_member_notify', 'Your membership account has been activated', '{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(27, 2, 'y', 'decline_member_validation', 'Your membership account has been declined', '{name},\n\nWe\'re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(28, 2, 'y', 'mailinglist_activation_instructions', 'Email Confirmation', 'Thank you for joining the \"{mailing_list}\" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}');
INSERT INTO `exp_specialty_templates` VALUES(29, 2, 'y', 'comment_notification', 'Someone just responded to your comment', '{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}');
INSERT INTO `exp_specialty_templates` VALUES(30, 2, 'y', 'comments_opened_notification', 'New comments have been added', 'Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}');
INSERT INTO `exp_specialty_templates` VALUES(31, 2, 'y', 'private_message_notification', 'Someone has sent you a Private Message', '\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled �{message_subject}�.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}');
INSERT INTO `exp_specialty_templates` VALUES(32, 2, 'y', 'pm_inbox_full', 'Your private message mailbox is full', '{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}');

/* Table structure for table `exp_stats` */
DROP TABLE IF EXISTS `exp_stats`;

CREATE TABLE `exp_stats` (
  `stat_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `total_members` mediumint(7) NOT NULL DEFAULT '0',
  `recent_member_id` int(10) NOT NULL DEFAULT '0',
  `recent_member` varchar(50) NOT NULL,
  `total_entries` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_topics` mediumint(8) NOT NULL DEFAULT '0',
  `total_forum_posts` mediumint(8) NOT NULL DEFAULT '0',
  `total_comments` mediumint(8) NOT NULL DEFAULT '0',
  `last_entry_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_forum_post_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_comment_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `most_visitors` mediumint(7) NOT NULL DEFAULT '0',
  `most_visitor_date` int(10) unsigned NOT NULL DEFAULT '0',
  `last_cache_clear` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`stat_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_stats` */
INSERT INTO `exp_stats` VALUES(1, 1, 1, 1, '96black', 29, 0, 0, 0, 1365731435, 0, 0, 1365930450, 34, 1365643350, 1366073681);
INSERT INTO `exp_stats` VALUES(2, 2, 1, 1, '96black', 0, 0, 0, 0, 0, 0, 0, 1365848246, 6, 1365845962, 1366235389);

/* Table structure for table `exp_status_groups` */
DROP TABLE IF EXISTS `exp_status_groups`;

CREATE TABLE `exp_status_groups` (
  `group_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_status_groups` */
INSERT INTO `exp_status_groups` VALUES(1, 1, 'Statuses');
INSERT INTO `exp_status_groups` VALUES(2, 2, 'Statuses');

/* Table structure for table `exp_status_no_access` */
DROP TABLE IF EXISTS `exp_status_no_access`;

CREATE TABLE `exp_status_no_access` (
  `status_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`status_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_statuses` */
DROP TABLE IF EXISTS `exp_statuses`;

CREATE TABLE `exp_statuses` (
  `status_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(4) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `status_order` int(3) unsigned NOT NULL,
  `highlight` varchar(30) NOT NULL,
  PRIMARY KEY (`status_id`),
  KEY `group_id` (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_statuses` */
INSERT INTO `exp_statuses` VALUES(1, 1, 1, 'open', 1, '009933');
INSERT INTO `exp_statuses` VALUES(2, 1, 1, 'closed', 2, '990000');
INSERT INTO `exp_statuses` VALUES(3, 2, 2, 'open', 1, '009933');
INSERT INTO `exp_statuses` VALUES(4, 2, 2, 'closed', 2, '990000');

/* Table structure for table `exp_template_groups` */
DROP TABLE IF EXISTS `exp_template_groups`;

CREATE TABLE `exp_template_groups` (
  `group_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_name` varchar(50) NOT NULL,
  `group_order` int(3) unsigned NOT NULL,
  `is_site_default` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_template_groups` */
INSERT INTO `exp_template_groups` VALUES(1, 1, 'Routing', 1, 'y');
INSERT INTO `exp_template_groups` VALUES(5, 1, 'Home', 3, 'n');
INSERT INTO `exp_template_groups` VALUES(6, 1, 'Products', 4, 'n');
INSERT INTO `exp_template_groups` VALUES(8, 1, 'News', 6, 'n');
INSERT INTO `exp_template_groups` VALUES(9, 1, 'About-us', 7, 'n');
INSERT INTO `exp_template_groups` VALUES(10, 1, 'Contact-us', 8, 'n');
INSERT INTO `exp_template_groups` VALUES(11, 1, 'Services', 9, 'n');
INSERT INTO `exp_template_groups` VALUES(12, 1, 'Common', 10, 'n');
INSERT INTO `exp_template_groups` VALUES(13, 1, 'Featured-products', 10, 'n');
INSERT INTO `exp_template_groups` VALUES(14, 1, 'Case-studies', 11, 'n');

/* Table structure for table `exp_template_member_groups` */
DROP TABLE IF EXISTS `exp_template_member_groups`;

CREATE TABLE `exp_template_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `template_group_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY (`group_id`,`template_group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_template_no_access` */
DROP TABLE IF EXISTS `exp_template_no_access`;

CREATE TABLE `exp_template_no_access` (
  `template_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`template_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_templates` */
DROP TABLE IF EXISTS `exp_templates`;

CREATE TABLE `exp_templates` (
  `template_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `group_id` int(6) unsigned NOT NULL,
  `template_name` varchar(50) NOT NULL,
  `save_template_file` char(1) NOT NULL DEFAULT 'n',
  `template_type` varchar(16) NOT NULL DEFAULT 'webpage',
  `template_data` mediumtext,
  `template_notes` text,
  `edit_date` int(10) NOT NULL DEFAULT '0',
  `last_author_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cache` char(1) NOT NULL DEFAULT 'n',
  `refresh` int(6) unsigned NOT NULL DEFAULT '0',
  `no_auth_bounce` varchar(50) NOT NULL DEFAULT '',
  `enable_http_auth` char(1) NOT NULL DEFAULT 'n',
  `allow_php` char(1) NOT NULL DEFAULT 'n',
  `php_parse_location` char(1) NOT NULL DEFAULT 'o',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`template_id`),
  KEY `group_id` (`group_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_templates` */
INSERT INTO `exp_templates` VALUES(1, 1, 1, 'index', 'y', 'webpage', '{embed=\"Common/header\"}\n\n{embed=\"Home/index\"}\n\n{embed=\"Common/footer\"}', '', 1365458557, 1, 'n', 0, '', 'n', 'y', 'o', 6138);
INSERT INTO `exp_templates` VALUES(4, 2, 1, 'monkey-butt', 'y', 'webpage', 'OH SNAP', '', 1355889015, 1, 'n', 0, '', 'n', 'n', 'o', 1);
INSERT INTO `exp_templates` VALUES(9, 1, 5, 'index', 'y', 'webpage', 'Home test', '', 1355945914, 1, 'n', 0, '', 'n', 'y', 'o', 4);
INSERT INTO `exp_templates` VALUES(10, 1, 6, 'index', 'y', 'webpage', '', '', 1355945909, 1, 'n', 0, '', 'n', 'y', 'o', 1270);
INSERT INTO `exp_templates` VALUES(12, 1, 8, 'index', 'y', 'webpage', '', '', 1355945899, 1, 'n', 0, '', 'n', 'n', 'o', 64);
INSERT INTO `exp_templates` VALUES(13, 1, 9, 'index', 'y', 'webpage', '', '', 1355945894, 1, 'n', 0, '', 'n', 'n', 'o', 55);
INSERT INTO `exp_templates` VALUES(14, 1, 10, 'index', 'y', 'webpage', '', '', 1355945889, 1, 'n', 0, '', 'n', 'n', 'o', 95);
INSERT INTO `exp_templates` VALUES(15, 1, 11, 'index', 'y', 'webpage', '', '', 1355945883, 1, 'n', 0, '', 'n', 'n', 'o', 149);
INSERT INTO `exp_templates` VALUES(16, 1, 12, 'index', 'y', 'webpage', '', '', 1355945878, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(17, 1, 12, 'header', 'y', 'webpage', '', '', 1362949938, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(18, 1, 12, 'footer', 'y', 'webpage', '', '', 1355946002, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(19, 1, 12, '_menu', 'y', 'webpage', '', '', 1362965438, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(20, 1, 6, 'product-page', 'y', 'webpage', '', '', 1358280171, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(22, 1, 6, 'category-listing', 'y', 'webpage', 'Category listing!', '', 1358280845, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(23, 1, 6, '_product-nav', 'y', 'webpage', '', '', 1358295203, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(24, 1, 6, 'category-landing', 'y', 'webpage', '', '', 1358365005, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(26, 1, 6, '_category-listing-products-blocks', 'y', 'webpage', '', '', 1362954501, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(27, 1, 8, 'article-list', 'y', 'webpage', '', '', 1362957827, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(28, 1, 8, 'article', 'y', 'webpage', '', '', 1362957835, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(29, 1, 10, '_contact-form', 'y', 'webpage', '', '', 1363570256, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(30, 1, 13, 'index', 'y', 'webpage', '', '', 1363828125, 1, 'n', 0, '', 'n', 'n', 'o', 1);
INSERT INTO `exp_templates` VALUES(31, 1, 13, 'page-intros', 'y', 'webpage', '{exp:channel:entries channel=\"products\" entry_id=\"{embed:product_id}\" limit=\"1\"}\n<div class=\"additionalBlocks\">\n    <ul>\n        {reverse_related_entries id=\"featured_product\"}\n        <li class=\"{switch=\'||third\'}\">\n            <h3><a href=\"{url_title}\">{title}</a></h3>\n            <p>{featured_content_intro}</p>\n            <a href=\"{url_title}\" class=\"readMore\">Read More</a>\n        </li>\n        {/reverse_related_entries}\n    </ul>\n    <div class=\"clear\"></div>\n</div>\n{/exp:channel:entries}', '', 1363827515, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(32, 1, 13, 'page', 'y', 'webpage', '', '', 1363823952, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(33, 1, 6, 'featured_subpage', 'y', 'webpage', '', '', 1363829194, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(34, 1, 14, 'index', 'y', 'webpage', '', '', 1363904378, 1, 'n', 0, '', 'n', 'n', 'o', 28);
INSERT INTO `exp_templates` VALUES(35, 1, 14, 'article-list', 'y', 'webpage', '', '', 1363904459, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(36, 1, 14, 'article', 'y', 'webpage', '', '', 1363904454, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(37, 1, 11, '_services-nav', 'y', 'webpage', '<h3>Services</h3>\n<ul id=\"sideMenu\">\n    <li><a href=\"#\">Roads &amp; Services</a></li>\n    <li><a href=\"#\">Mapping</a></li>\n    <li><a href=\"#\">Lorem Ipsum</a> </li>\n    <li><a href=\"#\">Lorem Ipsum</a> </li>\n</ul>', null, 1365479093, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(38, 1, 11, 'service-landing', 'y', 'webpage', '<div id=\"content\" class=\"std\">\n    <div class=\"middle\">\n        <div id=\"sideBar\">\n            {embed=\"Services/_services-nav\"}\n        </div>\n\n        <div id=\"copy\" class=\"services\">\n            <div class=\"inner\">\n\n                <ul class=\"breadCrumb\">\n                    <li><a href=\"#\">Home</a></li>\n                    <li class=\"current\"><a href=\"#\">Services</a></li>\n                </ul>\n\n                <div class=\"main\">\n                    <h1>Services</h1>\n                </div>\n                <ul class=\"servicesList\">\n                    <li><img src=\"/images/temp/services-temp.jpg\" alt=\"Roads &amp; Services\" />\n                        <h2><a href=\"#\">Roads and Services</a></h2>\n                        <div class=\"excerpt\"><p>One of our core services, Synergy Positioning are experts in asphalt paving technologies. We offer a complete solution for all your paving requirements. Using a host of suppliers including: Moba Matic, Topcon & Roadware, we have complete control of any surfacing project.</p></div>\n                        <a href=\"#\" class=\"readMore\">Read More</a>\n                    </li>\n                    <li class=\"second\"><img src=\"/images/temp/services-temp.jpg\" alt=\"Mapping\" />\n                        <h2><a href=\"#\">Mapping</a></h2>\n                        <div class=\"excerpt\"><p>One of our core services, Synergy Positioning are experts in asphalt paving technologies. We offer a complete solution for all your paving requirements. Using a host of suppliers including: Moba Matic, Topcon & Roadware, we have complete control of any surfacing project.</p></div>\n                        <a href=\"#\" class=\"readMore\">Read More</a>\n                    </li>\n                    <li><img src=\"/images/temp/services-temp.jpg\" alt=\"Lorem Ipsum Dolor\" />\n                        <h2><a href=\"#\">Lorem Ipsum Dolor</a></h2>\n                        <div class=\"excerpt\"><p>One of our core services, Synergy Positioning are experts in asphalt paving technologies. We offer a complete solution for all your paving requirements. Using a host of suppliers including: Moba Matic, Topcon & Roadware, we have complete control of any surfacing project.</p></div>\n                        <a href=\"#\" class=\"readMore\">Read More</a>\n                    </li>\n                    <li class=\"second\"><img src=\"/images/temp/services-temp.jpg\" alt=\"Lorem Ipsum Dolor\" />\n                        <h2><a href=\"#\">Lorem Ipsum Dolor</a></h2>\n                        <div class=\"excerpt\"><p>One of our core services, Synergy Positioning are experts in asphalt paving technologies. We offer a complete solution for all your paving requirements. Using a host of suppliers including: Moba Matic, Topcon & Roadware, we have complete control of any surfacing project.</p></div>\n                        <a href=\"#\" class=\"readMore\">Read More</a>\n                    </li>\n                </ul>	\n\n\n            </div>\n\n\n\n        </div>\n        <div class=\"clear\"></div>\n    </div>\n</div>', null, 1365479411, 1, 'n', 0, '', 'n', 'y', 'o', 0);
INSERT INTO `exp_templates` VALUES(39, 1, 11, 'service-listing', 'y', 'webpage', '\n<div id=\"copy\" class=\"services servicesDetail\">\n    <div class=\"topPageImage\">\n        <img src=\"/images/temp/services-temp-top.jpg\" alt=\"Roads &amp; Services\" />\n    </div>\n\n    <div class=\"inner\">\n\n        <ul class=\"breadCrumb\">\n            <li><a href=\"#\">Home</a></li>\n            <li><a href=\"#\">Services</a></li>\n            <li><a href=\"#\">Roads &amp; Services</a></li>\n            <li class=\"current\"><a href=\"#\">Survey &amp; Design</a></li>\n\n        </ul>\n\n        <div class=\"main\">\n            <h1>Survey &amp; Design</h1>\n            <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus.</p>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. Aliquam eleifend enim eleifend lacus pretium ac pretium risus sodales. Aliquam mi nisi, suscipit in ornare in, tempus a justo. Vivamus porta, justo in sollicitudin vulputate, velit massa congue mi, a vehicula enim metus a lectus. Sed felis tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n            <hr />\n\n            <p><img src=\"/images/temp/services-temp-detail-1.jpg\" alt=\"Survey &amp; Design\" class=\"alignLeft\" /></p>\n\n            <h3>Lorem Ipsum Dolor</h3>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullam]is tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n            <h3>Pellentesque mattis ultrices dapibu uis ullamcorp</h3>\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit m]is tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n\n            <hr />\n            <iframe class=\"alignRight\" width=\"417\" height=\"267\" src=\"http://www.youtube.com/embed/39zYdKe6vLs\" frameborder=\"0\" allowfullscreen></iframe>\n            <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus.</h3>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper</p>\n        </div>\n    </div>\n\n    <div class=\"additionalBlocks\">\n        <ul>\n            <li>\n                <h3><a href=\"#\">Survey &amp; Design</a></h3>\n                <p>Synergy Positioning uses an in-house survey team with many years of experience in the asphalt and roading industries. Our in-house experts in CAD, manages the design and control of final surfacing levels to ensure a smooth and cost effective result, on time and within budget</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n            <li>\n                <h3><a href=\"#\">Paving Control</a></h3>\n                <p>Our paving managers are the best in the industry. Taking full control of the paving crew, we manage the project from start to finish.</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n            <li class=\"third\">\n                <h3><a href=\"#\">Additional Information</a></h3>\n                <p>Sed sagittis tincidunt lobortis. In non ligula a urna blandit tempor. Aenean a leo ut mauris posuere aliquet. Phasellus faucibus convallis aliquet. Proin velit mi, tempus quis varius eu, suscipit sit amet nisl.</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n        </ul>\n        <div class=\"clear\"></div>\n    </div>\n\n\n</div>\n', null, 1365481672, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(40, 1, 11, 'service-page', 'y', 'webpage', '<div id=\"copy\" class=\"services servicesDetail\">\n    <div class=\"topPageImage\">\n        <img src=\"/images/temp/services-temp-top.jpg\" alt=\"Roads &amp; Services\" />\n    </div>\n\n    <div class=\"inner\">\n\n        <ul class=\"breadCrumb\">\n            <li><a href=\"#\">Home</a></li>\n            <li><a href=\"#\">Services</a></li>\n            <li><a href=\"#\">Roads &amp; Services</a></li>\n            <li class=\"current\"><a href=\"#\">Survey &amp; Design</a></li>\n\n        </ul>\n\n        <div class=\"main\">\n            <h1>Survey &amp; Design</h1>\n            <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus.</p>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. Aliquam eleifend enim eleifend lacus pretium ac pretium risus sodales. Aliquam mi nisi, suscipit in ornare in, tempus a justo. Vivamus porta, justo in sollicitudin vulputate, velit massa congue mi, a vehicula enim metus a lectus. Sed felis tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n            <hr />\n\n            <p><img src=\"/images/temp/services-temp-detail-1.jpg\" alt=\"Survey &amp; Design\" class=\"alignLeft\" /></p>\n\n            <h3>Lorem Ipsum Dolor</h3>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullam]is tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n            <h3>Pellentesque mattis ultrices dapibu uis ullamcorp</h3>\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit m]is tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>\n\n\n            <hr />\n            <iframe class=\"alignRight\" width=\"417\" height=\"267\" src=\"http://www.youtube.com/embed/39zYdKe6vLs\" frameborder=\"0\" allowfullscreen></iframe>\n            <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus.</h3>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper</p>\n        </div>\n    </div>\n\n    <div class=\"additionalBlocks\">\n        <ul>\n            <li>\n                <h3><a href=\"#\">Survey &amp; Design</a></h3>\n                <p>Synergy Positioning uses an in-house survey team with many years of experience in the asphalt and roading industries. Our in-house experts in CAD, manages the design and control of final surfacing levels to ensure a smooth and cost effective result, on time and within budget</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n            <li>\n                <h3><a href=\"#\">Paving Control</a></h3>\n                <p>Our paving managers are the best in the industry. Taking full control of the paving crew, we manage the project from start to finish.</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n            <li class=\"third\">\n                <h3><a href=\"#\">Additional Information</a></h3>\n                <p>Sed sagittis tincidunt lobortis. In non ligula a urna blandit tempor. Aenean a leo ut mauris posuere aliquet. Phasellus faucibus convallis aliquet. Proin velit mi, tempus quis varius eu, suscipit sit amet nisl.</p>\n                <a href=\"#\" class=\"readMore\">Read More</a>\n            </li>\n        </ul>\n        <div class=\"clear\"></div>\n    </div>\n\n\n</div>\n', null, 1365481672, 1, 'n', 0, '', 'n', 'y', 'i', 0);
INSERT INTO `exp_templates` VALUES(41, 1, 9, 'page', 'y', 'webpage', '<div id=\"copy\" class=\"about\">\n            <div class=\"topPageImage\">\n                <img src=\"/images/temp/about-temp.jpg\" alt=\"About Synergy Positioning Systems\" />\n            </div>\n\n            <div class=\"inner\">\n\n                <ul class=\"breadCrumb\">\n                    <li><a href=\"#\">Home</a></li>\n                    <li class=\"current\"><a href=\"#\">About Us</a></li>\n\n                </ul>\n\n                <div class=\"main\">\n                    <h1>About Us</h1>\n\n                    <p class=\"intro\">Synergy Positioning Systems Ltd is 100% New Zealand owned and operated. It was formed when two of the industry�s most trusted companies merged in 2006 - Synergy Equipment and Systems Ltd and Geodetic Instruments Ltd. The result is a company that brings you over 30 years experience delivering the most advanced and innovative technologies combined with a commitment to outstanding customer service.</p>\n\n                    <p>Our aim is to make your job faster and easier, more accurate and cost efficient. We\'ll do this by giving you expert and honest advice and by offering you the best solution from our range of innovative positioning systems.</p>\n\n                    <p>Reliability is paramount - equipment breakages cost time and money. There are many low quality brands on the market that just don\'t stand up to the rigours of our construction industry. We only sell reliable brands we can trust and service.</p>\n\n                    <p>Our experienced team are always on hand for after sales support, product training and servicing, giving you the confidence and skills to get the best out of your equipment. Our product range is extensive and always growing.</p>\n\n                    <h2>Why choose synergy?</h2>\n\n\n                </div>\n            </div>\n\n            <div class=\"additionalBlocks\">\n                <ul>\n                    <li>\n                        <h3>Simple really....</h3>\n                        <p>You are dealing with an established company representing world renowned brands that have been tried and tested in increasing productivity and reducing costs.</p>\n\n                    </li>\n                    <li>\n                        <h3>Your profit...</h3>\n                        <p>With our equipment and systems, you gain efficiency &amp; reduce costs, saving you money on all aspects of the job including: fuel, wages, wear &amp; tear, materials &amp; processing. </p>\n\n                    </li>\n                    <li>\n                        <h3>Our Products</h3>\n                        <p>We only represent equipment &amp; brands that we trust. Topcon, world leaders in positioning equipment for over 80 years &amp; well established throughout the world, offers reliable &amp; robust equipment for any application.</p>\n\n                    </li>\n                    <li>\n                        <h3>Our Experience</h3>\n                        <p>Synergy Positioning Systems Ltd is 100% New Zealand owned and operated. Our industry exprience spans more than 30 years. </p>\n\n                    </li>\n                    <li>\n                        <h3>Our Service</h3>\n                        <p>We are commited to our customers. Your success is our reward. Our factory trained technicians and in-house industry experts ensure that what we sell is fully supported.</p>\n\n                    </li>\n                </ul>\n                <div class=\"clear\"></div>\n            </div>\n\n\n\n        </div>', null, 1365632661, 1, 'n', 0, '', 'n', 'n', 'o', 0);
INSERT INTO `exp_templates` VALUES(42, 1, 9, 'management-page', 'y', 'webpage', '<div id=\"copy\" class=\"management\">\n\n    <div class=\"inner\">\n\n        <ul class=\"breadCrumb\">\n            <li><a href=\"#\">Home</a></li>\n            <li><a href=\"#\">About Us</a></li>\n            <li class=\"current\"><a href=\"#\">Management</a></li>\n\n        </ul>\n\n        <div class=\"main\">\n            <h1>Management</h1>\n\n            <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet.</p>\n\n            <hr />\n\n            <h2>New Zealand</h2>\n\n            <ul class=\"profileList\">\n                <li class=\"first\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 3 </strong>\n                        <span class=\"title\">Title</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n\n                <li class=\"second\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"third\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"fourth\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"fifth\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n            </ul>\n\n            <hr />\n\n            <h2>Australia</h2>\n\n            <ul class=\"profileList\">\n                <li class=\"first\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 3 </strong>\n                        <span class=\"title\">Title</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n\n                <li class=\"second\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"third\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"fourth\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n\n                <li class=\"fifth\">  \n                    <div class=\"image\"> <a href=\"#\" class=\"imageContainer viewProfile\" title=\"Mike Milne - Click here for profile detail.\"></a><img src=\"/images/profile-placeholder.jpg\" alt=\"Mike Milne\" /></div>\n                    <div class=\"shortDetail\">\n\n                        <strong>Mike Milne 4</strong>\n                        <span class=\"title\">Title 2</span>\n                        <a href=\"#\" class=\"profileTitle\" title=\"Mike Milne - Click here for profile detail.\"><span class=\"viewProfile\">View Profile</span>\n                        </a>\n                    </div>\n                    <div class=\"profileDetail\">\n                        <p class=\"intro\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n                        <h3>Lorem ipsum dolor sit amet</h3>\n\n                        <ul>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                            <li>Lorem ipsum dolor sit amet, consectetur </li>\n                        </ul>\n\n                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>\n\n\n                    </div>\n\n                </li>\n            </ul>\n        </div>\n    </div>\n\n\n\n\n\n</div>', '', 1365635326, 1, 'n', 0, '', 'n', 'n', 'o', 12);

/* Table structure for table `exp_throttle` */
DROP TABLE IF EXISTS `exp_throttle`;

CREATE TABLE `exp_throttle` (
  `throttle_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL,
  `locked_out` char(1) NOT NULL DEFAULT 'n',
  PRIMARY KEY (`throttle_id`),
  KEY `ip_address` (`ip_address`),
  KEY `last_activity` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_upload_no_access` */
DROP TABLE IF EXISTS `exp_upload_no_access`;

CREATE TABLE `exp_upload_no_access` (
  `upload_id` int(6) unsigned NOT NULL,
  `upload_loc` varchar(3) NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY (`upload_id`,`member_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


/* Table structure for table `exp_upload_prefs` */
DROP TABLE IF EXISTS `exp_upload_prefs`;

CREATE TABLE `exp_upload_prefs` (
  `id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `site_id` int(4) unsigned NOT NULL DEFAULT '1',
  `name` varchar(50) NOT NULL,
  `server_path` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(100) NOT NULL,
  `allowed_types` varchar(3) NOT NULL DEFAULT 'img',
  `max_size` varchar(16) DEFAULT NULL,
  `max_height` varchar(6) DEFAULT NULL,
  `max_width` varchar(6) DEFAULT NULL,
  `properties` varchar(120) DEFAULT NULL,
  `pre_format` varchar(120) DEFAULT NULL,
  `post_format` varchar(120) DEFAULT NULL,
  `file_properties` varchar(120) DEFAULT NULL,
  `file_pre_format` varchar(120) DEFAULT NULL,
  `file_post_format` varchar(120) DEFAULT NULL,
  `cat_group` varchar(255) DEFAULT NULL,
  `batch_location` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_upload_prefs` */
INSERT INTO `exp_upload_prefs` VALUES(1, 1, 'Product Images', '/Users/jamesmcfall/Projects/Synergy/uploads/product-images/', 'http://synergy.local/uploads/product-images/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(2, 1, 'Product Downloads', '/Users/jamesmcfall/Projects/Synergy/uploads/product-downloads/', 'http://synergy.local/uploads/product-downloads/', 'all', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(3, 1, 'Category Images', '/Users/jamesmcfall/Projects/Synergy/uploads/category-images/', 'http://synergy.local/uploads/category-images/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(4, 1, 'News Article Banner Image', '/Users/jamesmcfall/Projects/Synergy/uploads/news-images/', 'http://synergy.local/uploads/news-images/', 'img', '', '373', '866', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(5, 1, 'Case Study Banner Image', '/Users/jamesmcfall/Projects/Synergy/uploads/case-study-image/', 'http://synergy.local/uploads/case-study-image/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(6, 1, 'Home Page Banners', '/Users/jamesmcfall/Projects/Synergy/uploads/home-page-banners/', 'http://synergy.local/uploads/home-page-banners/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(7, 1, 'Services Banners', '/Users/jamesmcfall/Projects/Synergy/uploads/services-banners/', 'http://synergy.local/uploads/services-banners/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(8, 1, 'About Banners', '/Users/jamesmcfall/Projects/Synergy/uploads/about-banners/', 'http://synergy.local/uploads/about-banners/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(9, 1, 'Staff Pictures', '/Users/jamesmcfall/Projects/Synergy/uploads/staff-pictures/', 'http://synergy.local/uploads/staff-pictures/', 'img', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `exp_upload_prefs` VALUES(10, 1, 'Manufacturer Images', '/Users/jamesmcfall/Projects/Synergy/uploads/manufacturer-images/', 'http://synergy.local/uploads/manufacturer-images/', 'img', '', '', '', '', '', '', '', '', '', '', null);

/* Table structure for table `exp_wygwam_configs` */
DROP TABLE IF EXISTS `exp_wygwam_configs`;

CREATE TABLE `exp_wygwam_configs` (
  `config_id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `config_name` varchar(32) DEFAULT NULL,
  `settings` text,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/* dumping data for table `exp_wygwam_configs` */
INSERT INTO `exp_wygwam_configs` VALUES(1, 'Basic', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTA6e2k6MDtzOjQ6IkJvbGQiO2k6MTtzOjY6Ikl0YWxpYyI7aToyO3M6OToiVW5kZXJsaW5lIjtpOjM7czo2OiJTdHJpa2UiO2k6NDtzOjEyOiJOdW1iZXJlZExpc3QiO2k6NTtzOjEyOiJCdWxsZXRlZExpc3QiO2k6NjtzOjQ6IkxpbmsiO2k6NztzOjY6IlVubGluayI7aTo4O3M6NjoiQW5jaG9yIjtpOjk7czo1OiJBYm91dCI7fXM6NjoiaGVpZ2h0IjtzOjM6IjIwMCI7czoxNDoicmVzaXplX2VuYWJsZWQiO3M6MToieSI7czoxMToiY29udGVudHNDc3MiO2E6MDp7fXM6MTA6InVwbG9hZF9kaXIiO3M6MDoiIjt9');
INSERT INTO `exp_wygwam_configs` VALUES(2, 'Full', 'YTo1OntzOjc6InRvb2xiYXIiO2E6Njc6e2k6MDtzOjY6IlNvdXJjZSI7aToxO3M6NDoiU2F2ZSI7aToyO3M6NzoiTmV3UGFnZSI7aTozO3M6NzoiUHJldmlldyI7aTo0O3M6OToiVGVtcGxhdGVzIjtpOjU7czozOiJDdXQiO2k6NjtzOjQ6IkNvcHkiO2k6NztzOjU6IlBhc3RlIjtpOjg7czo5OiJQYXN0ZVRleHQiO2k6OTtzOjEzOiJQYXN0ZUZyb21Xb3JkIjtpOjEwO3M6NToiUHJpbnQiO2k6MTE7czoxMjoiU3BlbGxDaGVja2VyIjtpOjEyO3M6NToiU2NheXQiO2k6MTM7czo0OiJVbmRvIjtpOjE0O3M6NDoiUmVkbyI7aToxNTtzOjQ6IkZpbmQiO2k6MTY7czo3OiJSZXBsYWNlIjtpOjE3O3M6OToiU2VsZWN0QWxsIjtpOjE4O3M6MTI6IlJlbW92ZUZvcm1hdCI7aToxOTtzOjQ6IkZvcm0iO2k6MjA7czo4OiJDaGVja2JveCI7aToyMTtzOjU6IlJhZGlvIjtpOjIyO3M6OToiVGV4dEZpZWxkIjtpOjIzO3M6ODoiVGV4dGFyZWEiO2k6MjQ7czo2OiJTZWxlY3QiO2k6MjU7czo2OiJCdXR0b24iO2k6MjY7czoxMToiSW1hZ2VCdXR0b24iO2k6Mjc7czoxMToiSGlkZGVuRmllbGQiO2k6Mjg7czoxOiIvIjtpOjI5O3M6NDoiQm9sZCI7aTozMDtzOjY6Ikl0YWxpYyI7aTozMTtzOjk6IlVuZGVybGluZSI7aTozMjtzOjY6IlN0cmlrZSI7aTozMztzOjk6IlN1YnNjcmlwdCI7aTozNDtzOjExOiJTdXBlcnNjcmlwdCI7aTozNTtzOjEyOiJOdW1iZXJlZExpc3QiO2k6MzY7czoxMjoiQnVsbGV0ZWRMaXN0IjtpOjM3O3M6NzoiT3V0ZGVudCI7aTozODtzOjY6IkluZGVudCI7aTozOTtzOjEwOiJCbG9ja3F1b3RlIjtpOjQwO3M6OToiQ3JlYXRlRGl2IjtpOjQxO3M6MTE6Ikp1c3RpZnlMZWZ0IjtpOjQyO3M6MTM6Ikp1c3RpZnlDZW50ZXIiO2k6NDM7czoxMjoiSnVzdGlmeVJpZ2h0IjtpOjQ0O3M6MTI6Ikp1c3RpZnlCbG9jayI7aTo0NTtzOjQ6IkxpbmsiO2k6NDY7czo2OiJVbmxpbmsiO2k6NDc7czo2OiJBbmNob3IiO2k6NDg7czo1OiJJbWFnZSI7aTo0OTtzOjU6IkZsYXNoIjtpOjUwO3M6NToiVGFibGUiO2k6NTE7czoxNDoiSG9yaXpvbnRhbFJ1bGUiO2k6NTI7czo2OiJTbWlsZXkiO2k6NTM7czoxMToiU3BlY2lhbENoYXIiO2k6NTQ7czo5OiJQYWdlQnJlYWsiO2k6NTU7czo4OiJSZWFkTW9yZSI7aTo1NjtzOjEwOiJFbWJlZE1lZGlhIjtpOjU3O3M6MToiLyI7aTo1ODtzOjY6IlN0eWxlcyI7aTo1OTtzOjY6IkZvcm1hdCI7aTo2MDtzOjQ6IkZvbnQiO2k6NjE7czo4OiJGb250U2l6ZSI7aTo2MjtzOjk6IlRleHRDb2xvciI7aTo2MztzOjc6IkJHQ29sb3IiO2k6NjQ7czo4OiJNYXhpbWl6ZSI7aTo2NTtzOjEwOiJTaG93QmxvY2tzIjtpOjY2O3M6NToiQWJvdXQiO31zOjY6ImhlaWdodCI7czozOiIyMDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ==');
INSERT INTO `exp_wygwam_configs` VALUES(3, 'News', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTE6e2k6MDtzOjY6IkZvcm1hdCI7aToxO3M6NDoiQm9sZCI7aToyO3M6NjoiSXRhbGljIjtpOjM7czo5OiJVbmRlcmxpbmUiO2k6NDtzOjY6IlN0cmlrZSI7aTo1O3M6MTI6Ik51bWJlcmVkTGlzdCI7aTo2O3M6MTI6IkJ1bGxldGVkTGlzdCI7aTo3O3M6NDoiTGluayI7aTo4O3M6NjoiVW5saW5rIjtpOjk7czo2OiJBbmNob3IiO2k6MTA7czo1OiJBYm91dCI7fXM6NjoiaGVpZ2h0IjtzOjM6IjIwMCI7czoxNDoicmVzaXplX2VuYWJsZWQiO3M6MToieSI7czoxMToiY29udGVudHNDc3MiO2E6MDp7fXM6MTA6InVwbG9hZF9kaXIiO3M6MDoiIjt9');
INSERT INTO `exp_wygwam_configs` VALUES(4, 'Featured Products Content', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTg6e2k6MDtzOjY6IkZvcm1hdCI7aToxO3M6MTE6Ikp1c3RpZnlMZWZ0IjtpOjI7czoxMzoiSnVzdGlmeUNlbnRlciI7aTozO3M6MTI6Ikp1c3RpZnlSaWdodCI7aTo0O3M6MTI6Ikp1c3RpZnlCbG9jayI7aTo1O3M6NDoiQm9sZCI7aTo2O3M6NjoiSXRhbGljIjtpOjc7czo5OiJVbmRlcmxpbmUiO2k6ODtzOjY6IlN0cmlrZSI7aTo5O3M6MTI6Ik51bWJlcmVkTGlzdCI7aToxMDtzOjEyOiJCdWxsZXRlZExpc3QiO2k6MTE7czo1OiJJbWFnZSI7aToxMjtzOjU6IkZsYXNoIjtpOjEzO3M6MTA6IkVtYmVkTWVkaWEiO2k6MTQ7czo1OiJBYm91dCI7aToxNTtzOjQ6IkxpbmsiO2k6MTY7czo2OiJVbmxpbmsiO2k6MTc7czo2OiJBbmNob3IiO31zOjY6ImhlaWdodCI7czozOiIyMDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ==');
INSERT INTO `exp_wygwam_configs` VALUES(5, 'Synergy Default', 'YTo1OntzOjc6InRvb2xiYXIiO2E6MTk6e2k6MDtzOjY6IkZvcm1hdCI7aToxO3M6NDoiQm9sZCI7aToyO3M6NjoiSXRhbGljIjtpOjM7czo5OiJVbmRlcmxpbmUiO2k6NDtzOjY6IlN0cmlrZSI7aTo1O3M6MTE6Ikp1c3RpZnlMZWZ0IjtpOjY7czoxMzoiSnVzdGlmeUNlbnRlciI7aTo3O3M6MTI6Ikp1c3RpZnlSaWdodCI7aTo4O3M6MTI6Ikp1c3RpZnlCbG9jayI7aTo5O3M6MTI6Ik51bWJlcmVkTGlzdCI7aToxMDtzOjEyOiJCdWxsZXRlZExpc3QiO2k6MTE7czo0OiJMaW5rIjtpOjEyO3M6NjoiVW5saW5rIjtpOjEzO3M6NjoiQW5jaG9yIjtpOjE0O3M6NToiSW1hZ2UiO2k6MTU7czo1OiJGbGFzaCI7aToxNjtzOjE0OiJIb3Jpem9udGFsUnVsZSI7aToxNztzOjEwOiJFbWJlZE1lZGlhIjtpOjE4O3M6NToiQWJvdXQiO31zOjY6ImhlaWdodCI7czozOiI0MDAiO3M6MTQ6InJlc2l6ZV9lbmFibGVkIjtzOjE6InkiO3M6MTE6ImNvbnRlbnRzQ3NzIjthOjA6e31zOjEwOiJ1cGxvYWRfZGlyIjtzOjA6IiI7fQ==');


<?php
// turn error reporting on (only for dev environment)
error_reporting(E_ALL);
ini_set('display_errors', '1');

function _mysqldump($mysql_database) {
    $dbSQL = "";
    $sql = "show tables;";
    $result = mysql_query($sql);
    if ($result) {
        while ($row = mysql_fetch_row($result)) {
            $dbSQL .= _mysqldump_table_structure($row[0]);
            $dbSQL .= _mysqldump_table_data($row[0]);
        }
    } else {
        $dbSQL .= "/* no tables in $mysql_database */\n";
    }
    mysql_free_result($result);
    return $dbSQL;
}

function _mysqldump_table_structure($table) {
    $structure = "/* Table structure for table `$table` */\n";

    $structure .= "DROP TABLE IF EXISTS `$table`;\n\n";

    $sql = "show create table `$table`; ";
    $result = mysql_query($sql);
    if ($result) {
        if ($row = mysql_fetch_assoc($result)) {
            $structure .= $row['Create Table'] . ";\n\n";
        }
    }
    mysql_free_result($result);

    return $structure;
}

function _mysqldump_table_data($table) {

    $data = "";
    $sql = "select * from `$table`;";
    $result = mysql_query($sql);
    if ($result) {
        $num_rows = mysql_num_rows($result);
        $num_fields = mysql_num_fields($result);

        if ($num_rows > 0) {
            $data .= "/* dumping data for table `$table` */\n";

            $field_type = array();
            $i = 0;
            while ($i < $num_fields) {
                $meta = mysql_fetch_field($result, $i);
                array_push($field_type, $meta->type);
                $i++;
            }

            //print_r( $field_type);
            $index = 0;
            while ($row = mysql_fetch_row($result)) {
                $data .= "INSERT INTO `$table` VALUES";
                $data .= "(";
                for ($i = 0; $i < $num_fields; $i++) {
                    if (is_null($row[$i]))
                        $data .= "null";
                    else {
                        switch ($field_type[$i]) {
                            case 'int':
                                $data .= $row[$i];
                                break;
                            case 'string':
                            case 'blob' :
                            default:
                                $data .= "'" . mysql_real_escape_string($row[$i]) . "'";
                        }
                    }
                    if ($i < $num_fields - 1)
                        $data .= ", ";
                }
                $data .= ")";
                $data .= ";";
                $data .= "\n";

                $index++;
            }
        }
    }
    mysql_free_result($result);
    $data .= "\n";
    return $data;
}

$output_messages = array();

# File should be dated
$timeZone = new DateTimeZone("Pacific/Auckland");
$dateTime = new DateTime("now", $timeZone);

$myFile = "db-" . $dateTime->format("Y-m-d-His") . ".sql";

# Please note these details are stored in the .htaccess file
$connection = mysql_connect($_SERVER["DB_HOST"], $_SERVER["DB_USER"], $_SERVER["DB_PASS"]);
mysql_select_db($_SERVER["DB_NAME"], $connection);

$fh = fopen($myFile, 'w');
fwrite($fh, _mysqldump($_SERVER["DB_NAME"]));
fclose($fh);
echo "OK";

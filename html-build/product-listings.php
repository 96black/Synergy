
<? include(dirname(__FILE__) . '/templates/header.php'); ?>
<div id="content" class="std">
	<div class="middle">
        <div id="sideBar">
            <h3>Products</h3>
            <ul id="sideMenu">
                <li><a href="#">UAV</a></li>
                <li><a href="#">Faro</a></li>
                <li class="current"><a href="#">Optical</a>
                    <ul class="subMenu">
                        <li><a href="#">Total Station</a>
                        	 <ul class="subMenu">
                                <li><a href="#">Robotic</a></li>
                                <li><a href="#">Windows Based</a></li>
                                <li><a href="#">Construction</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Levels</a></li>
                        <li><a href="#">Theodolites</a></li>
                        <li><a href="#">Binoculars</a></li>
                    </ul>
                </li>
                <li><a href="#">GPS Equipment</a>
                    <ul class="subMenu">
                        <li><a href="#">Geodetic Receivers</a></li>
                        <li><a href="#">Mapping &amp GIS</a></li>
                        <li><a href="#">GPS Accessories</a></li>
                        <li><a href="#">Radio Range Extenders &amp; Repeaters</a></li>
                        <li><a href="#">Geodetic Antennas</a></li>
                    </ul>
                </li>
                <li><a href="#">Machine Control</a>
                    <ul class="subMenu">
                        <li><a href="#">3D Systems</a></li>
                        <li><a href="#">Automatic Systems</a></li>
                        <li><a href="#">Indicate Systems</a></li>
                        <li><a href="#">Add-On Equipment</a></li>
                    </ul>
                </li>
                <li><a href="#">Lasers</a>
                    <ul class="subMenu">
                        <li><a href="#">Construction Lasers</a></li>
                        <li><a href="#">Grade lasers</a></li>
                        <li><a href="#">Interior Lasers</a></li>
                        <li><a href="#">Pipe Lasers</a></li>
                        <li><a href="#">Lasers Accessories</a></li>
                    </ul>
                </li>
                <li><a href="#">Hand-Held Devices</a></li>
                <li><a href="#">Survey Accessories</a>
                    <ul class="subMenu">
                        <li><a href="#">Tripods</a></li>
                        <li><a href="#">Prisims and holders</a></li>
                        <li><a href="#">Tribrachs &amp Adapters</a></li>
                        <li><a href="#">Leveling Staves &amp; Range Poles</a></li>
                        <li><a href="#">Calculators</a></li>
                        <li><a href="#">Mapping</a></li>
                        <li><a href="#">Two Way Radios</a></li>
                    </ul>
                </li>
                <li><a href="#">Software</a>
                    <ul class="subMenu">
                        <li><a href="#">CAD</a></li>
                        <li><a href="#">Survey - Field Software</a></li>
                        <li><a href="#">Survey - Office Software</a></li>
                        <li><a href="#">Mapping &amp; GIS</a></li>
                    </ul>
                </li>
                <li><a href="#">Measuring Instruments</a>
                    <ul class="subMenu">
                        <li><a href="#">Measuring Wheels</a></li>
                        <li><a href="#">Laser Tape Measurers</a></li>
                        <li><a href="#">Digital Spirit Level</a></li>
                        <li><a href="#">Measuring Tapes</a></li>
                        <li><a href="#">Moisture Meters</a></li>
                        <li><a href="#">Range Finders</a></li>
                        <li><a href="#">Protractors</a></li>
                        <li><a href="#">Magnetic Locators</a></li>
                        <li><a href="#">Thermometer</a></li>
                        <li><a href="#">Height Poles</a></li>
                    </ul>
                </li>
                <li><a href="#">Protective Cases</a></li>
                <li><a href="#">Ground Testing Equipment</a>
                    <ul class="subMenu">
                        <li><a href="#">Penetrometer</a></li>
                        <li><a href="#">Impact Tester</a></li>
                        <li><a href="#">Shear Vane</a></li>
                    </ul>
                </li>
                <li><a href="#">Industrial Lighting</a></li>
                <li><a href="#">Telematics</a></li>
                <li><a href="#">Aerial Mapping</a></li>
                <li><a href="#">Used Instruments</a></li>
            </ul>
            
            <div id="enquireForm" class="sideForm">
            	<h3>Your enquiry</h3>
                <form action="" method="get">
                	<ul>
                    	<li><label>Name*</label> <input name="name" type="text" /></li>
                        <li><label>Company</label> <input name="company" type="text" /></li>
                        <li><label>Email*</label> <input name="email" type="text" /></li>
                        <li><label>Phone</label> <input name="phone" type="text" /></li>
                        <li><label>Message</label> <textarea name="" cols="" rows=""></textarea></li>
                        <li><input name="productName" type="hidden" value="" /></li>
                        <li class="send"><input name="" type="submit" value="Send Enquiry" class="largeBtn" /> <a href="#" class="hideForm readMore">Hide Form</a></li>
                    </ul>
                </form>
            </div>
        </div>
        
        <div id="copy" class="productListing">
        
            <div class="inner">
            
            <ul class="breadCrumb">
            	<li><a href="#">Home</a></li>
                <li><a href="#">Products</a></li>
                <li class="current"><a href="#">Optical</a></li>
            </ul>
            
            <span class="note">* Please note all prices exclude GST</span>
            
            <h1>Optical</h1>
            <ul class="catNav">
                <li class="first"><a href="#">Total Station</a>
                     <ul class="subMenu">
                        <li><a href="#">Robotic</a></li>
                        <li><a href="#">Windows Based</a></li>
                        <li><a href="#">Construction</a></li>
                    </ul>
                </li>
                <li class="second"><a href="#">Levels</a>
                 	<ul class="subMenu">
                        <li><a href="#">Auto / Dumpy Levels</a></li>
                        <li><a href="#">Digital Levels</a></li>
                        <li><a href="#">Abney Levels</a></li>
                    </ul>
                </li>
                <li class="third"><a href="#">Theodolites</a></li>
                <li class="fourth"><a href="#">Binoculars</a></li>
            </ul>
            
            
            <div class="productsBlock">
            <h2>Total Station</h2>
            <a href="#" class="browseAll">Browse all Total Station sub-categories and products</a>
            
            <ul class="listings">
            	<li class="first">
                	<a href="#" title="Topcon Quick Station - Click here for product detail." class="image"><div class="imageContainer"></div><img src="/images/profile-placeholder.jpg" alt="Mike Milne" /></a>
                    <div class="shortDetail">
                    	<a href="#" class="productTitle" title="Topcon Quick Station - Click here for product detail.">Topcon Quick Station</a>
                    	<span class="price">$45,000 <small>NZD</small></span>
                    </div>
                    
                    <div class="shortOptions">
                        <a href="#" class="enquire" title="Enquire about this product">Enquire</a>
                        <span> / </span>
                        <a href="#" class="rent" title="Rent this product">Rent</a>
                    </div>
                </li>
                <li class="second">
                	<a href="#" title="Topcon Quick Station - Click here for product detail." class="image"><div class="imageContainer"></div><img src="/images/temp/cat-image.jpg" alt="Optical" /></a>
                    <div class="shortDetail">
                    	<a href="#" class="productTitle" title="Topcon Quick Station - Click here for product detail.">Topcon Quick Station GTS-100 NFSZ100</a>
                    	<span class="price">$45,000 <small>NZD</small></span>
                    </div>
                    
                    <div class="shortOptions">
                        <a href="#" class="enquire" title="Enquire about this product">Enquire</a>
                        <span> / </span>
                        <a href="#" class="rent" title="Rent this product">Rent</a>
                    </div>
                </li>
                <li class="third">
                	<a href="#" title="Topcon Quick Station - Click here for product detail." class="image"><div class="imageContainer"></div><img src="/images/temp/cat-image.jpg" alt="Optical" /></a>
                    <div class="shortDetail">
                    	<a href="#" class="productTitle" title="Topcon Quick Station - Click here for product detail.">Topcon Quick Station</a>
                    	<span class="price">$45,000 <small>NZD</small></span>
                    </div>
                    
                    <div class="shortOptions">
                        <a href="#" class="enquire" title="Enquire about this product">Enquire</a>
                        <span> / </span>
                        <a href="#" class="rent" title="Rent this product">Rent</a>
                    </div>
                </li>
                <li class="fourth">
                	<a href="#" title="Topcon Quick Station - Click here for product detail." class="image"><div class="imageContainer"></div><img src="/images/temp/cat-image.jpg" alt="Optical" /></a>
                    <div class="shortDetail">
                    	<a href="#" class="productTitle" title="Topcon Quick Station - Click here for product detail.">Topcon Quick Station</a>
                    	<span class="price">$45,000 <small>NZD</small></span>
                    </div>
                    
                    <div class="shortOptions">
                        <a href="#" class="enquire" title="Enquire about this product">Enquire</a>
                        <span> / </span>
                        <a href="#" class="rent" title="Rent this product">Rent</a>
                    </div>
                </li>
                <li class="fifth">
                	<a href="#" title="Topcon Quick Station - Click here for product detail." class="image"><div class="imageContainer"></div><img src="/images/temp/cat-image.jpg" alt="Optical" /></a>
                    <div class="shortDetail">
                    	<a href="#" class="productTitle" title="Topcon Quick Station - Click here for product detail.">Topcon Quick Station</a>
                    	<span class="price">$45,000 <small>NZD</small></span>
                    </div>
                    
                    <div class="shortOptions">
                        <a href="#" class="enquire" title="Enquire about this product">Enquire</a>
                        <span> / </span>
                        <a href="#" class="rent" title="Rent this product">Rent</a>
                    </div>
                </li>
            </ul>
            </div>
            
             <div class="productsBlock">
            <h2>Total Station</h2>
            <a href="#" class="browseAll">Browse all Total Station sub-categories and products</a>
            
            <ul class="listings">
            	<li class="first">
                	<a href="#" title="Topcon Quick Station - Click here for product detail." class="image"><div class="imageContainer"></div><img src="/images/temp/cat-image.jpg" alt="Optical" /></a>
                    <div class="shortDetail">
                    	<a href="#" class="productTitle" title="Topcon Quick Station - Click here for product detail.">Topcon Quick Station</a>
                    	<span class="price">$45,000 <small>NZD</small></span>
                    </div>
                    
                    <div class="shortOptions">
                        <a href="#" class="enquire" title="Enquire about this product">Enquire</a>
                        <span> / </span>
                        <a href="#" class="rent" title="Rent this product">Rent</a>
                    </div>
                </li>
                <li class="second">
                	<a href="#" title="Topcon Quick Station - Click here for product detail." class="image"><div class="imageContainer"></div><img src="/images/temp/cat-image.jpg" alt="Optical" /></a>
                    <div class="shortDetail">
                    	<a href="#" class="productTitle" title="Topcon Quick Station - Click here for product detail.">Topcon Quick Station</a>
                    	<span class="price">$45,000 <small>NZD</small></span>
                    </div>
                    
                    <div class="shortOptions">
                        <a href="#" class="enquire" title="Enquire about this product">Enquire</a>
                        <span> / </span>
                        <a href="#" class="rent" title="Rent this product">Rent</a>
                    </div>
                </li>
                <li class="third">
                	<a href="#" title="Topcon Quick Station - Click here for product detail." class="image"><div class="imageContainer"></div><img src="/images/temp/cat-image.jpg" alt="Optical" /></a>
                    <div class="shortDetail">
                    	<a href="#" class="productTitle" title="Topcon Quick Station - Click here for product detail.">Topcon Quick Station</a>
                    	<span class="price">$45,000 <small>NZD</small></span>
                    </div>
                    
                    <div class="shortOptions">
                        <a href="#" class="enquire" title="Enquire about this product">Enquire</a>
                        <span> / </span>
                        <a href="#" class="rent" title="Rent this product">Rent</a>
                    </div>
                </li>
                <li class="fourth">
                	<a href="#" title="Topcon Quick Station - Click here for product detail." class="image"><div class="imageContainer"></div><img src="/images/temp/cat-image.jpg" alt="Optical" /></a>
                    <div class="shortDetail">
                    	<a href="#" class="productTitle" title="Topcon Quick Station - Click here for product detail.">Topcon Quick Station</a>
                    	<span class="price">$45,000 <small>NZD</small></span>
                    </div>
                    
                    <div class="shortOptions">
                        <a href="#" class="enquire" title="Enquire about this product">Enquire</a>
                        <span> / </span>
                        <a href="#" class="rent" title="Rent this product">Rent</a>
                    </div>
                </li>
                <li class="fifth">
                	<a href="#" title="Topcon Quick Station - Click here for product detail." class="image"><div class="imageContainer"></div><img src="/images/temp/cat-image.jpg" alt="Optical" /></a>
                    <div class="shortDetail">
                    	<a href="#" class="productTitle" title="Topcon Quick Station - Click here for product detail.">Topcon Quick Station</a>
                    	<span class="price">$45,000 <small>NZD</small></span>
                    </div>
                    
                    <div class="shortOptions">
                        <a href="#" class="enquire" title="Enquire about this product">Enquire</a>
                        <span> / </span>
                        <a href="#" class="rent" title="Rent this product">Rent</a>
                    </div>
                </li>
            </ul>
            </div>
            
             <div class="productsBlock">
            <h2>Total Station</h2>
            <a href="#" class="browseAll">Browse all Total Station sub-categories and products</a>
            
            <ul class="listings">
            	<li class="first">
                	<a href="#" title="Topcon Quick Station - Click here for product detail." class="image"><div class="imageContainer"></div><img src="/images/temp/cat-image.jpg" alt="Optical" /></a>
                    <div class="shortDetail">
                    	<a href="#" class="productTitle" title="Topcon Quick Station - Click here for product detail.">Topcon Quick Station</a>
                    	<span class="price">$45,000 <small>NZD</small></span>
                    </div>
                    
                    <div class="shortOptions">
                        <a href="#" class="enquire" title="Enquire about this product">Enquire</a>
                        <span> / </span>
                        <a href="#" class="rent" title="Rent this product">Rent</a>
                    </div>
                </li>
                <li class="second">
                	<a href="#" title="Topcon Quick Station - Click here for product detail." class="image"><div class="imageContainer"></div><img src="/images/temp/cat-image.jpg" alt="Optical" /></a>
                    <div class="shortDetail">
                    	<a href="#" class="productTitle" title="Topcon Quick Station - Click here for product detail.">Topcon Quick Station</a>
                    	<span class="price">$45,000 <small>NZD</small></span>
                    </div>
                    
                    <div class="shortOptions">
                        <a href="#" class="enquire" title="Enquire about this product">Enquire</a>
                        <span> / </span>
                        <a href="#" class="rent" title="Rent this product">Rent</a>
                    </div>
                </li>
                <li class="third">
                	<a href="#" title="Topcon Quick Station - Click here for product detail." class="image"><div class="imageContainer"></div><img src="/images/temp/cat-image.jpg" alt="Optical" /></a>
                    <div class="shortDetail">
                    	<a href="#" class="productTitle" title="Topcon Quick Station - Click here for product detail.">Topcon Quick Station</a>
                    	<span class="price">$45,000 <small>NZD</small></span>
                    </div>
                    
                    <div class="shortOptions">
                        <a href="#" class="enquire" title="Enquire about this product">Enquire</a>
                        <span> / </span>
                        <a href="#" class="rent" title="Rent this product">Rent</a>
                    </div>
                </li>
                <li class="fourth">
                	<a href="#" title="Topcon Quick Station - Click here for product detail." class="image"><div class="imageContainer"></div><img src="/images/temp/cat-image.jpg" alt="Optical" /></a>
                    <div class="shortDetail">
                    	<a href="#" class="productTitle" title="Topcon Quick Station - Click here for product detail.">Topcon Quick Station</a>
                    	<span class="price">$45,000 <small>NZD</small></span>
                    </div>
                    
                    <div class="shortOptions">
                        <a href="#" class="enquire" title="Enquire about this product">Enquire</a>
                        <span> / </span>
                        <a href="#" class="rent" title="Rent this product">Rent</a>
                    </div>
                </li>
                <li class="fifth">
                	<div class="image"><div class="imageContainer"></div><a href="#" title="Topcon Quick Station - Click here for product detail." class="image"><img src="/images/temp/cat-image.jpg" alt="Optical" /></a></div>
                    <div class="shortDetail">
                    	<a href="#" class="productTitle" title="Topcon Quick Station - Click here for product detail.">Topcon Quick Station</a>
                    	<span class="price">$45,000 <small>NZD</small></span>
                    </div>
                    
                    <div class="shortOptions">
                        <a href="#" class="enquire" title="Enquire about this product">Enquire</a>
                        <span> / </span>
                        <a href="#" class="rent" title="Rent this product">Rent</a>
                    </div>
                </li>
            </ul>
            </div>
            
            
            
        	</div>
        </div>
        <div class="clear"></div>
    </div>
</div>

<? include(dirname(__FILE__) . '/templates/footer.php'); ?>            
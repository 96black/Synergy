
<? include(dirname(__FILE__) . '/templates/header.php'); ?>
<div id="content" class="std">
	<div class="middle">
        <div id="sideBar">
            <h3>Services</h3>
            <ul id="sideMenu">
                <li class="current"><a href="#">Roads &amp; Services</a>
                	  <ul class="subMenu">
                        <li><a href="#">Survey &amp; Design</a></li>
                        <li><a href="#">Paving Control</a></li>
                        <li><a href="#">Additional Information</a></li>
                    </ul>
                </li>
                <li><a href="#">Mapping</a></li>
                <li><a href="#">Lorem Ipsum</a> </li>
                <li><a href="#">Lorem Ipsum</a> </li>
                
            </ul>
            
           
        </div>
        
        <div id="copy" class="services servicesDetail">
          	<div class="topPageImage">
            	<img src="/images/temp/services-temp-top.jpg" alt="Roads &amp; Services" />
            </div>
          
            <div class="inner">
            
                <ul class="breadCrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Services</a></li>
                    <li class="current"><a href="#">Roads &amp; Services</a></li>
                
                </ul>
            
                <div class="main">
                <h1>Roads &amp; Services</h1>
                <p class="intro">One of our core services, Synergy Positioning are experts in asphalt paving technologies. We offer a complete solution for all your paving requirements. Using a host of suppliers including: Moba Matic, Topcon & Roadware, we have complete control of any surfacing project.</p>
         		</div>	
            
        	</div>
            
               <div class="additionalBlocks">
                	<ul>
                    	<li>
                        	<h3><a href="#">Survey &amp; Design</a></h3>
                       		<p>Synergy Positioning uses an in-house survey team with many years of experience in the asphalt and roading industries. Our in-house experts in CAD, manages the design and control of final surfacing levels to ensure a smooth and cost effective result, on time and within budget</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                        <li>
                        	<h3><a href="#">Paving Control</a></h3>
                       		<p>Our paving managers are the best in the industry. Taking full control of the paving crew, we manage the project from start to finish.</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                        <li class="third">
                        	<h3><a href="#">Additional Information</a></h3>
                       		<p>Sed sagittis tincidunt lobortis. In non ligula a urna blandit tempor. Aenean a leo ut mauris posuere aliquet. Phasellus faucibus convallis aliquet. Proin velit mi, tempus quis varius eu, suscipit sit amet nisl.</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
					</ul>
                    <div class="clear"></div>
              </div>
          
            
        </div>
        <div class="clear"></div>
    </div>
</div>

<? include(dirname(__FILE__) . '/templates/footer.php'); ?>            

<? include(dirname(__FILE__) . '/templates/header.php'); ?>
<div id="content" class="std">
	<div class="middle">
        <div id="sideBar">
            <h3>About us</h3>
            <ul id="sideMenu">
                <li class="current"><a href="#">Management</a> </li>
                <li><a href="#">History</a></li>
                <li><a href="#">Vision</a> </li>
                <li><a href="#">Case Studies</a> </li>
                
            </ul>
            
           
        </div>
        
        <div id="copy" class="management">
          	
            <div class="inner">
            
            <ul class="breadCrumb">
            	<li><a href="#">Home</a></li>
                <li><a href="#">About Us</a></li>
                <li class="current"><a href="#">Management</a></li>
            
            </ul>
            
            <div class="main">
           		<h1>Management</h1>
                
                <p class="intro">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet.</p>

<hr />

<h2>New Zealand</h2>

<ul class="profileList">
	<li class="first">  
    	<div class="image"> <a href="#" class="imageContainer viewProfile" title="Mike Milne - Click here for profile detail."></a><img src="/images/profile-placeholder.jpg" alt="Mike Milne" /></div>
        <div class="shortDetail">
           
                <strong>Mike Milne 3 </strong>
                <span class="title">Title</span>
             <a href="#" class="profileTitle" title="Mike Milne - Click here for profile detail."><span class="viewProfile">View Profile</span>
            </a>
       </div>
       <div class="profileDetail">
       <p class="intro">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>

<h3>Lorem ipsum dolor sit amet</h3>

<ul>
	<li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
</ul>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>


       </div>
     
    </li>
    
    
    <li class="second">  
    	<div class="image"> <a href="#" class="imageContainer viewProfile" title="Mike Milne - Click here for profile detail."></a><img src="/images/profile-placeholder.jpg" alt="Mike Milne" /></div>
        <div class="shortDetail">
           
                <strong>Mike Milne 4</strong>
                <span class="title">Title 2</span>
             <a href="#" class="profileTitle" title="Mike Milne - Click here for profile detail."><span class="viewProfile">View Profile</span>
            </a>
       </div>
       <div class="profileDetail">
       <p class="intro">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>

<h3>Lorem ipsum dolor sit amet</h3>

<ul>
	<li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
</ul>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>


       </div>
     
    </li>
    
    <li class="third">  
    	<div class="image"> <a href="#" class="imageContainer viewProfile" title="Mike Milne - Click here for profile detail."></a><img src="/images/profile-placeholder.jpg" alt="Mike Milne" /></div>
        <div class="shortDetail">
           
                <strong>Mike Milne 4</strong>
                <span class="title">Title 2</span>
             <a href="#" class="profileTitle" title="Mike Milne - Click here for profile detail."><span class="viewProfile">View Profile</span>
            </a>
       </div>
       <div class="profileDetail">
       <p class="intro">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>

<h3>Lorem ipsum dolor sit amet</h3>

<ul>
	<li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
</ul>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>


       </div>
     
    </li>
    
    <li class="fourth">  
    	<div class="image"> <a href="#" class="imageContainer viewProfile" title="Mike Milne - Click here for profile detail."></a><img src="/images/profile-placeholder.jpg" alt="Mike Milne" /></div>
        <div class="shortDetail">
           
                <strong>Mike Milne 4</strong>
                <span class="title">Title 2</span>
             <a href="#" class="profileTitle" title="Mike Milne - Click here for profile detail."><span class="viewProfile">View Profile</span>
            </a>
       </div>
       <div class="profileDetail">
       <p class="intro">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>

<h3>Lorem ipsum dolor sit amet</h3>

<ul>
	<li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
</ul>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>


       </div>
     
    </li>
    
    <li class="fifth">  
    	<div class="image"> <a href="#" class="imageContainer viewProfile" title="Mike Milne - Click here for profile detail."></a><img src="/images/profile-placeholder.jpg" alt="Mike Milne" /></div>
        <div class="shortDetail">
           
                <strong>Mike Milne 4</strong>
                <span class="title">Title 2</span>
             <a href="#" class="profileTitle" title="Mike Milne - Click here for profile detail."><span class="viewProfile">View Profile</span>
            </a>
       </div>
       <div class="profileDetail">
       <p class="intro">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>

<h3>Lorem ipsum dolor sit amet</h3>

<ul>
	<li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
</ul>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>


       </div>
     
    </li>
</ul>

<hr />

<h2>Australia</h2>

<ul class="profileList">
	<li class="first">  
    	<div class="image"> <a href="#" class="imageContainer viewProfile" title="Mike Milne - Click here for profile detail."></a><img src="/images/profile-placeholder.jpg" alt="Mike Milne" /></div>
        <div class="shortDetail">
           
                <strong>Mike Milne 3 </strong>
                <span class="title">Title</span>
             <a href="#" class="profileTitle" title="Mike Milne - Click here for profile detail."><span class="viewProfile">View Profile</span>
            </a>
       </div>
       <div class="profileDetail">
       <p class="intro">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>

<h3>Lorem ipsum dolor sit amet</h3>

<ul>
	<li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
</ul>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>


       </div>
     
    </li>
    
    
    <li class="second">  
    	<div class="image"> <a href="#" class="imageContainer viewProfile" title="Mike Milne - Click here for profile detail."></a><img src="/images/profile-placeholder.jpg" alt="Mike Milne" /></div>
        <div class="shortDetail">
           
                <strong>Mike Milne 4</strong>
                <span class="title">Title 2</span>
             <a href="#" class="profileTitle" title="Mike Milne - Click here for profile detail."><span class="viewProfile">View Profile</span>
            </a>
       </div>
       <div class="profileDetail">
       <p class="intro">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>

<h3>Lorem ipsum dolor sit amet</h3>

<ul>
	<li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
</ul>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>


       </div>
     
    </li>
    
    <li class="third">  
    	<div class="image"> <a href="#" class="imageContainer viewProfile" title="Mike Milne - Click here for profile detail."></a><img src="/images/profile-placeholder.jpg" alt="Mike Milne" /></div>
        <div class="shortDetail">
           
                <strong>Mike Milne 4</strong>
                <span class="title">Title 2</span>
             <a href="#" class="profileTitle" title="Mike Milne - Click here for profile detail."><span class="viewProfile">View Profile</span>
            </a>
       </div>
       <div class="profileDetail">
       <p class="intro">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>

<h3>Lorem ipsum dolor sit amet</h3>

<ul>
	<li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
</ul>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>


       </div>
     
    </li>
    
    <li class="fourth">  
    	<div class="image"> <a href="#" class="imageContainer viewProfile" title="Mike Milne - Click here for profile detail."></a><img src="/images/profile-placeholder.jpg" alt="Mike Milne" /></div>
        <div class="shortDetail">
           
                <strong>Mike Milne 4</strong>
                <span class="title">Title 2</span>
             <a href="#" class="profileTitle" title="Mike Milne - Click here for profile detail."><span class="viewProfile">View Profile</span>
            </a>
       </div>
       <div class="profileDetail">
       <p class="intro">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>

<h3>Lorem ipsum dolor sit amet</h3>

<ul>
	<li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
</ul>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>


       </div>
     
    </li>
    
    <li class="fifth">  
    	<div class="image"> <a href="#" class="imageContainer viewProfile" title="Mike Milne - Click here for profile detail."></a><img src="/images/profile-placeholder.jpg" alt="Mike Milne" /></div>
        <div class="shortDetail">
           
                <strong>Mike Milne 4</strong>
                <span class="title">Title 2</span>
             <a href="#" class="profileTitle" title="Mike Milne - Click here for profile detail."><span class="viewProfile">View Profile</span>
            </a>
       </div>
       <div class="profileDetail">
       <p class="intro">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellu.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>

<h3>Lorem ipsum dolor sit amet</h3>

<ul>
	<li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
    <li>Lorem ipsum dolor sit amet, consectetur </li>
</ul>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. </p>


       </div>
     
    </li>
</ul>
         	</div>
            </div>
         	
              
            
          
            
        </div>
        <div class="clear"></div>
    </div>
</div>

<? include(dirname(__FILE__) . '/templates/footer.php'); ?>            
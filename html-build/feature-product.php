
<? include(dirname(__FILE__) . '/templates/header.php'); ?>
<div id="content" class="std">
	<div class="middle">
        <div id="sideBar">
            <h3>Products</h3>
            <ul id="sideMenu">
                <li><a href="#">UAV</a></li>
                <li><a href="#">Faro</a></li>
                <li class="current"><a href="#">Optical</a>
                    <ul class="subMenu">
                        <li><a href="#">Total Station</a>
                        	 <ul class="subMenu">
                                <li><a href="#">Robotic</a></li>
                                <li><a href="#">Windows Based</a></li>
                                <li><a href="#">Construction</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Levels</a></li>
                        <li><a href="#">Theodolites</a></li>
                        <li><a href="#">Binoculars</a></li>
                    </ul>
                </li>
                <li><a href="#">GPS Equipment</a>
                    <ul class="subMenu">
                        <li><a href="#">Geodetic Receivers</a></li>
                        <li><a href="#">Mapping &amp GIS</a></li>
                        <li><a href="#">GPS Accessories</a></li>
                        <li><a href="#">Radio Range Extenders &amp; Repeaters</a></li>
                        <li><a href="#">Geodetic Antennas</a></li>
                    </ul>
                </li>
                <li><a href="#">Machine Control</a>
                    <ul class="subMenu">
                        <li><a href="#">3D Systems</a></li>
                        <li><a href="#">Automatic Systems</a></li>
                        <li><a href="#">Indicate Systems</a></li>
                        <li><a href="#">Add-On Equipment</a></li>
                    </ul>
                </li>
                <li><a href="#">Lasers</a>
                    <ul class="subMenu">
                        <li><a href="#">Construction Lasers</a></li>
                        <li><a href="#">Grade lasers</a></li>
                        <li><a href="#">Interior Lasers</a></li>
                        <li><a href="#">Pipe Lasers</a></li>
                        <li><a href="#">Lasers Accessories</a></li>
                    </ul>
                </li>
                <li><a href="#">Hand-Held Devices</a></li>
                <li><a href="#">Survey Accessories</a>
                    <ul class="subMenu">
                        <li><a href="#">Tripods</a></li>
                        <li><a href="#">Prisims and holders</a></li>
                        <li><a href="#">Tribrachs &amp Adapters</a></li>
                        <li><a href="#">Leveling Staves &amp; Range Poles</a></li>
                        <li><a href="#">Calculators</a></li>
                        <li><a href="#">Mapping</a></li>
                        <li><a href="#">Two Way Radios</a></li>
                    </ul>
                </li>
                <li><a href="#">Software</a>
                    <ul class="subMenu">
                        <li><a href="#">CAD</a></li>
                        <li><a href="#">Survey - Field Software</a></li>
                        <li><a href="#">Survey - Office Software</a></li>
                        <li><a href="#">Mapping &amp; GIS</a></li>
                    </ul>
                </li>
                <li><a href="#">Measuring Instruments</a>
                    <ul class="subMenu">
                        <li><a href="#">Measuring Wheels</a></li>
                        <li><a href="#">Laser Tape Measurers</a></li>
                        <li><a href="#">Digital Spirit Level</a></li>
                        <li><a href="#">Measuring Tapes</a></li>
                        <li><a href="#">Moisture Meters</a></li>
                        <li><a href="#">Range Finders</a></li>
                        <li><a href="#">Protractors</a></li>
                        <li><a href="#">Magnetic Locators</a></li>
                        <li><a href="#">Thermometer</a></li>
                        <li><a href="#">Height Poles</a></li>
                    </ul>
                </li>
                <li><a href="#">Protective Cases</a></li>
                <li><a href="#">Ground Testing Equipment</a>
                    <ul class="subMenu">
                        <li><a href="#">Penetrometer</a></li>
                        <li><a href="#">Impact Tester</a></li>
                        <li><a href="#">Shear Vane</a></li>
                    </ul>
                </li>
                <li><a href="#">Industrial Lighting</a></li>
                <li><a href="#">Telematics</a></li>
                <li><a href="#">Aerial Mapping</a></li>
                <li><a href="#">Used Instruments</a></li>
            </ul>
            
            <div id="enquireForm" class="sideForm">
            	<h3>Your enquiry</h3>
                <form action="" method="get">
                	<ul>
                    	<li><label>Name*</label> <input name="name" type="text" /></li>
                        <li><label>Company</label> <input name="company" type="text" /></li>
                        <li><label>Email*</label> <input name="email" type="text" /></li>
                        <li><label>Phone</label> <input name="phone" type="text" /></li>
                        <li><label>Message</label> <textarea name="" cols="" rows=""></textarea></li>
                        <li><input name="productName" type="hidden" value="" /></li>
                        <li class="send"><input name="" type="submit" value="Send Enquiry" class="largeBtn" /> <a href="#" class="hideForm readMore">Hide Form</a></li>
                    </ul>
                </form>
            </div>
        </div>
        
        <div id="copy" class="productDetail featured">
            <ul id="featureSlider">
                    <li><a href="#"><img src="/images/temp/cat-feature-slide1.jpg" alt="Feature Product 1" /></a></li>
                    <li><a href="#"><img src="/images/temp/cat-feature-slide2.jpg" alt="Feature Product 2" /></a></li>
                    <li><a href="#"><img src="/images/temp/cat-feature-slide3.jpg" alt="Feature Product 3" /></a></li>
            </ul>
            <div class="inner">
            
            <ul class="breadCrumb">
            	<li><a href="#">Home</a></li>
                <li><a href="#">Products</a></li>
                <li class="current"><a href="#">UAV</a></li>
            </ul>
            
            <span class="note">* Please note all prices exclude GST</span>
            
           
           <div class="main">
            <div class="topProductInformation">
            	 <div class="top">
                	<h1>UAV</h1>
            		<span class="price">$45,000 <small>NZD</small></span>
               	</div>
               
               <div class="options">
               		<a href="#" class="enquire largeBtn">Enquire about this product</a>
                	<a href="#" class="rent largeBtn">Rent this product</a>
               </div>
                
              
               
               <div class="description">
					 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam ipsum nibh, ullamcorper eget scelerisque eget, consequat vel ipsum. Donec in nisl ut sem porttitor faucibus non in ante. Vivamus sem lorem, cursus in malesuada nec, eleifend et elit. Integer placerat dictum urna, eu pellentesque purus sollicitudin placerat. Aliquam erat volutpat. Donec justo metus, consequat at congue quis, fringilla eget erat. Suspendisse tincidunt vestibulum ligula, vitae condimentum orci blandit quis. Suspendisse potenti. Morbi nec leo a nisl laoreet fringilla et quis risus. Pellentesque a nisl sem, at tempus augue. Ut nec mauris nec dolor rhoncus tempor et ac mi. Duis ut augue lorem, eget sodales libero. Vivamus vitae vehicula nunc. Fusce non tellus mauris, tempus fermentum nibh.</p>

<p>Donec tempus sapien vel augue aliquet condimentum. Sed sit amet ipsum eget ipsum gravida condimentum. Mauris dapibus molestie sodales. Nullam egestas enim eget nisl consectetur pellentesque elementum velit ultrices. Praesent nec hendrerit tortor. Sed id ligula eros. Curabitur pretium volutpat rutrum. Donec erat mi, viverra quis pharetra vel, tempor pulvinar neque.</p>

					<ul>
                 	<li>Lorem ipsum dolor sit amet, consectetur</li>
                    <li>Lorem ipsum dolor sit amet, consectetur</li>
                    <li>Lorem ipsum dolor sit amet, consectetur</li>
                    <li>Lorem ipsum dolor sit amet, consectetur</li>
                 </ul>

               </div>
               
            </div>
            
            <div class="productMedia">
            		<span class="imageCover"></span>
            		<ul id="productImageGallery">
                    	<li><a href="#"><img src="/images/temp/large-product-image.jpg" alt="Topcon GTS-100N" /></a></li>
                        <li><a href="#"><img src="/images/temp/large-product-image.jpg" alt="Topcon GTS-100N" /></a></li>
                        <li><a href="#"><img src="/images/temp/large-product-image.jpg" alt="Topcon GTS-100N" /></a></li>
                        <li><a href="#"><img src="/images/temp/large-product-image.jpg" alt="Topcon GTS-100N" /></a></li>
                    </ul>
            </div>
            
            <div class="productInformation">
           
                <div class="tabs">
                    <ul class="tabsNav">
                        <li><a href="#first">Technical Specifications</a></li>
                        <li><a href="#second">Downloads</a></li>
                        <li><a href="#third">Videos</a></li>
                        <li><a href="#fourth">Rental</a></li>
                    </ul>
                    <div class="block" id="first">
                       <h4>Telescope</h4>
                        <ul class="table specs">
                        	<li><strong>Length:</strong> <span>150 Millimeters</span></li>
                            <li><strong>Objective Lens Diameter:</strong> <span>45mm (EDM:50mm)</span></li>
                            <li><strong>Magnification:</strong> <span>30x</span></li>
                            <li><strong>Image:</strong> <span>Erect</span></li>
                            <li><strong>Field of View:</strong> <span>130</span></li>
                            <li><strong>Resolve Power:</strong> <span>3"</span></li>
                            <li><strong>Min Focus Distance:</strong> <span>1.3 meters</span></li>
                        </ul>
                        
                        <h4>Measurement Range</h4>
                        <ul class="table">
                        	<li><strong>1 Prisim:</strong> <span>6500 ft, 2000 meters</span></li>
                            <li><strong>3 Prisim:</strong> <span>6500 ft, 2000 meters</span></li>
                        </ul>
                    </div>
                    
                    <div class="block" id="second">
                        <h4>Title of Downloads section</h4>
                        <ul class="table downloads">
                        	<li><strong>Title of file</strong> <span>1.2mb</span><span>PDF</span></li>
                            <li><strong>Title of file</strong> <span>1.2mb</span><span>DOC</span></li>
                            <li><strong>Title of file</strong> <span>1.2mb</span><span>ZIP</span></li>
                            <li><strong>Title of file</strong> <span>1.2mb</span><span>PDF</span></li>
                            <li><strong>Title of file</strong> <span>1.2mb</span><span>PDF</span></li>
                            <li><strong>Title of file</strong> <span>1.2mb</span><span>PDF</span></li>
                        </ul>
                    </div>
                    
                    <div class="block" id="third">
                      <h4>Title of Video</h4>
                       <iframe width="410" height="231" src="http://www.youtube.com/embed/39zYdKe6vLs" frameborder="0" allowfullscreen></iframe>
                       
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        
                        <p><a href="http://topconpositioning.com" target="_blank">www.topconpositioning.com</a></p>
                    </div>
                    
                    <div class="block" id="fourth">
                        <h4>General Rental Information</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        
                        <ul class="table rental">
                        	<li><strong>1-3 Days</strong> <span>$10,000 NZD</span></li>
                            <li><strong>1-3 Days</strong> <span>$10,000 NZD</span></li>
                            <li><strong>1-3 Days</strong> <span>$10,000 NZD</span></li>
                            <li><strong>1-3 Days</strong> <span>$10,000 NZD</span></li>
                            <li><strong>1-3 Days</strong> <span>$10,000 NZD</span></li>
                            <li><strong>1-3 Days</strong> <span>$10,000 NZD</span></li>
                        </ul>
                        
                         <h4>Additional Information</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        
                    </div>
    			</div>
               
            </div>
           
            </div>
        	</div>
            
              <div class="additionalBlocks">
                	<ul>
                    	<li>
                        	<h3><a href="#">BRAMOR Othophoto</a></h3>
                       		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultrices, sem nec suscipit sodales, diam lacus tempus elit, sed pretium purus nulla ac neque. Nullam nec elementum arcu. Lorem ipsum dolor sit amet</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                        <li>
                        	<h3><a href="#">Applications</a></h3>
                       		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultrices, sem nec suscipit sodales, diam lacus tempus elit, sed pretium purus nulla ac neque. Nullam nec elementum arcu. Lorem ipsum dolor sit amet</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                        <li class="third">
                        	<h3><a href="#">News</a></h3>
                       		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultrices, sem nec suscipit sodales, diam lacus tempus elit, sed pretium purus nulla ac neque. Nullam nec elementum arcu. Lorem ipsum dolor sit amet</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                        <li>
                        	<h3><a href="#">BRAMOR Othophoto</a></h3>
                       		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultrices, sem nec suscipit sodales, diam lacus tempus elit, sed pretium purus nulla ac neque. Nullam nec elementum arcu. Lorem ipsum dolor sit amet</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                        <li>
                        	<h3><a href="#">Applications</a></h3>
                       		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultrices, sem nec suscipit sodales, diam lacus tempus elit, sed pretium purus nulla ac neque. Nullam nec elementum arcu. Lorem ipsum dolor sit amet</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                        <li class="third">
                        	<h3><a href="#">News</a></h3>
                       		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultrices, sem nec suscipit sodales, diam lacus tempus elit, sed pretium purus nulla ac neque. Nullam nec elementum arcu. Lorem ipsum dolor sit amet</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                        <li>
                        	<h3><a href="#">BRAMOR Othophoto</a></h3>
                       		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultrices, sem nec suscipit sodales, diam lacus tempus elit, sed pretium purus nulla ac neque. Nullam nec elementum arcu. Lorem ipsum dolor sit amet</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                        <li>
                        	<h3><a href="#">Applications</a></h3>
                       		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultrices, sem nec suscipit sodales, diam lacus tempus elit, sed pretium purus nulla ac neque. Nullam nec elementum arcu. Lorem ipsum dolor sit amet</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                      <li class="third">
                        	<h3><a href="#">News</a></h3>
                       		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultrices, sem nec suscipit sodales, diam lacus tempus elit, sed pretium purus nulla ac neque. Nullam nec elementum arcu. Lorem ipsum dolor sit amet</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                    </ul>
                    <div class="clear"></div>
                </div>
            
        </div>
        <div class="clear"></div>
    </div>
</div>

<? include(dirname(__FILE__) . '/templates/footer.php'); ?>            
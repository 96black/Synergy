
<? include(dirname(__FILE__) . '/templates/header.php'); ?>
<div id="content" class="std">
	<div class="middle">
        <div id="sideBar">
            <h3>Services</h3>
            <ul id="sideMenu">
                <li><a href="#">Roads &amp; Services</a></li>
                <li><a href="#">Mapping</a></li>
                <li><a href="#">Lorem Ipsum</a> </li>
                <li><a href="#">Lorem Ipsum</a> </li>
                
            </ul>
            
           
        </div>
        
        <div id="copy" class="services">
          
            <div class="inner">
            
            <ul class="breadCrumb">
            	<li><a href="#">Home</a></li>
                <li class="current"><a href="#">Services</a></li>
            
            </ul>
            
            <div class="main">
            <h1>Services</h1>
         	</div>
         	<ul class="servicesList">
            	<li><img src="/images/temp/services-temp.jpg" alt="Roads &amp; Services" />
                	<h2><a href="#">Roads and Services</a></h2>
                    <div class="excerpt"><p>One of our core services, Synergy Positioning are experts in asphalt paving technologies. We offer a complete solution for all your paving requirements. Using a host of suppliers including: Moba Matic, Topcon & Roadware, we have complete control of any surfacing project.</p></div>
            		<a href="#" class="readMore">Read More</a>
                </li>
                <li class="second"><img src="/images/temp/services-temp.jpg" alt="Mapping" />
                	<h2><a href="#">Mapping</a></h2>
                    <div class="excerpt"><p>One of our core services, Synergy Positioning are experts in asphalt paving technologies. We offer a complete solution for all your paving requirements. Using a host of suppliers including: Moba Matic, Topcon & Roadware, we have complete control of any surfacing project.</p></div>
            		<a href="#" class="readMore">Read More</a>
                </li>
                <li><img src="/images/temp/services-temp.jpg" alt="Lorem Ipsum Dolor" />
                	<h2><a href="#">Lorem Ipsum Dolor</a></h2>
                   <div class="excerpt"><p>One of our core services, Synergy Positioning are experts in asphalt paving technologies. We offer a complete solution for all your paving requirements. Using a host of suppliers including: Moba Matic, Topcon & Roadware, we have complete control of any surfacing project.</p></div>
            		<a href="#" class="readMore">Read More</a>
                </li>
                <li class="second"><img src="/images/temp/services-temp.jpg" alt="Lorem Ipsum Dolor" />
                	<h2><a href="#">Lorem Ipsum Dolor</a></h2>
                    <div class="excerpt"><p>One of our core services, Synergy Positioning are experts in asphalt paving technologies. We offer a complete solution for all your paving requirements. Using a host of suppliers including: Moba Matic, Topcon & Roadware, we have complete control of any surfacing project.</p></div>
            		<a href="#" class="readMore">Read More</a>
                </li>
            </ul>	
            
          
        	</div>
            
          
            
        </div>
        <div class="clear"></div>
    </div>
</div>

<? include(dirname(__FILE__) . '/templates/footer.php'); ?>            

<? include(dirname(__FILE__) . '/templates/header.php'); ?>
<div id="content" class="std">
	<div class="middle">
        <div id="sideBar">
            <h3>News</h3>
            
            
           
        </div>
        
        <div id="copy" class="newsDetail">
          	<div class="topPageImage">
            	<img src="/images/temp/news-temp-top.jpg" alt="Topcon Magnet Released" />
            </div>
          
            <div class="inner">
            
            <ul class="breadCrumb">
            	<li><a href="#">Home</a></li>
                <li><a href="#">News</a></li>
                <li class="current"><a href="#">Topcon Magnet Released</a></li>
            
            </ul>
            
            <div class="main">
           		<h1>Topcon Magnet Released</h1>
                
                <p class="intro">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo.</p>

<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin ultricies neque non sem pharetra dignissim. Integer sit amet arcu libero, non porttitor lacus. Fusce lectus nunc, egestas vitae imperdiet et, suscipit quis justo. Praesent ut libero justo. Aliquam id bibendum leo. Donec hendrerit orci quis diam suscipit sit amet fermentum urna aliquam. In sit amet tortor in odio euismod cursus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Duis mattis, dui eu posuere adipiscing, felis neque pellentesque mauris, non aliquam est lorem id ante. Vivamus nisi nunc, aliquam ut venenatis nec, gravida vel diam. Nunc dapibus mollis enim. Vestibulum suscipit, risus sed convallis dignissim, nibh diam egestas dui, aliquam sollicitudin velit diam eget ipsum. Donec nibh metus, tempor sit amet rutrum at, viverra sed leo. Sed id turpis sed purus egestas volutpat nec eu leo. Vestibulum ligula nisi, malesuada eget tempus vel, scelerisque vitae nunc. Quisque in arcu sit amet dui pulvinar posuere.</p>

<h2>Lorem ipsum dolor sit</h2>
<p>Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere sed ullamcorper a, consequat nec dolor. Aliquam erat volutpat. Donec et quam vel enim mollis gravida. Nullam et diam id turpis tincidunt fringilla eu non arcu. Morbi orci mi, elementum vel dapibus vitae, bibendum in nibh. Curabitur mollis magna id felis iaculis lacinia. Nullam aliquet orci in massa pretium ac congue enim porta. Ut sagittis ipsum ut ligula imperdiet scelerisque. Sed dolor sem, pulvinar quis convallis quis, ullamcorper ut risus. Donec libero tortor, dignissim id gravida ac, aliquam sollicitudin ante. Mauris augue felis, ultrices et hendrerit in, condimentum eu sapien.</p>

<p>Donec vestibulum tincidunt dictum. Nam tristique lectus sed justo fermentum vel placerat odio luctus. Donec sit amet magna ante, sed molestie ante. Suspendisse vitae risus ac dolor congue feugiat. Phasellus accumsan purus eu dui scelerisque eget vulputate felis consectetur. Donec ut elit nibh, non congue sem. Curabitur aliquam cursus euismod. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae</p>

<a href="#" class="readMore">Back to news listings</a>

         	</div>
            </div>
         	
            
            
          
            
        </div>
        <div class="clear"></div>
    </div>
</div>

<? include(dirname(__FILE__) . '/templates/footer.php'); ?>            
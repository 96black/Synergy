
<? include(dirname(__FILE__) . '/templates/header.php'); ?>
<div id="content" class="std">
	<div class="middle">
        <div id="sideBar">
            <h3>Products</h3>
            <ul id="sideMenu">
                <li><a href="#">UAV</a></li>
                <li><a href="#">Faro</a></li>
                <li class="current"><a href="#">Optical</a>
                    <ul class="subMenu">
                        <li><a href="#">Total Station</a>
                        	 <ul class="subMenu">
                                <li><a href="#">Robotic</a></li>
                                <li><a href="#">Windows Based</a></li>
                                <li><a href="#">Construction</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Levels</a></li>
                        <li><a href="#">Theodolites</a></li>
                        <li><a href="#">Binoculars</a></li>
                    </ul>
                </li>
                <li><a href="#">GPS Equipment</a>
                    <ul class="subMenu">
                        <li><a href="#">Geodetic Receivers</a></li>
                        <li><a href="#">Mapping &amp GIS</a></li>
                        <li><a href="#">GPS Accessories</a></li>
                        <li><a href="#">Radio Range Extenders &amp; Repeaters</a></li>
                        <li><a href="#">Geodetic Antennas</a></li>
                    </ul>
                </li>
                <li><a href="#">Machine Control</a>
                    <ul class="subMenu">
                        <li><a href="#">3D Systems</a></li>
                        <li><a href="#">Automatic Systems</a></li>
                        <li><a href="#">Indicate Systems</a></li>
                        <li><a href="#">Add-On Equipment</a></li>
                    </ul>
                </li>
                <li><a href="#">Lasers</a>
                    <ul class="subMenu">
                        <li><a href="#">Construction Lasers</a></li>
                        <li><a href="#">Grade lasers</a></li>
                        <li><a href="#">Interior Lasers</a></li>
                        <li><a href="#">Pipe Lasers</a></li>
                        <li><a href="#">Lasers Accessories</a></li>
                    </ul>
                </li>
                <li><a href="#">Hand-Held Devices</a></li>
                <li><a href="#">Survey Accessories</a>
                    <ul class="subMenu">
                        <li><a href="#">Tripods</a></li>
                        <li><a href="#">Prisims and holders</a></li>
                        <li><a href="#">Tribrachs &amp Adapters</a></li>
                        <li><a href="#">Leveling Staves &amp; Range Poles</a></li>
                        <li><a href="#">Calculators</a></li>
                        <li><a href="#">Mapping</a></li>
                        <li><a href="#">Two Way Radios</a></li>
                    </ul>
                </li>
                <li><a href="#">Software</a>
                    <ul class="subMenu">
                        <li><a href="#">CAD</a></li>
                        <li><a href="#">Survey - Field Software</a></li>
                        <li><a href="#">Survey - Office Software</a></li>
                        <li><a href="#">Mapping &amp; GIS</a></li>
                    </ul>
                </li>
                <li><a href="#">Measuring Instruments</a>
                    <ul class="subMenu">
                        <li><a href="#">Measuring Wheels</a></li>
                        <li><a href="#">Laser Tape Measurers</a></li>
                        <li><a href="#">Digital Spirit Level</a></li>
                        <li><a href="#">Measuring Tapes</a></li>
                        <li><a href="#">Moisture Meters</a></li>
                        <li><a href="#">Range Finders</a></li>
                        <li><a href="#">Protractors</a></li>
                        <li><a href="#">Magnetic Locators</a></li>
                        <li><a href="#">Thermometer</a></li>
                        <li><a href="#">Height Poles</a></li>
                    </ul>
                </li>
                <li><a href="#">Protective Cases</a></li>
                <li><a href="#">Ground Testing Equipment</a>
                    <ul class="subMenu">
                        <li><a href="#">Penetrometer</a></li>
                        <li><a href="#">Impact Tester</a></li>
                        <li><a href="#">Shear Vane</a></li>
                    </ul>
                </li>
                <li><a href="#">Industrial Lighting</a></li>
                <li><a href="#">Telematics</a></li>
                <li><a href="#">Aerial Mapping</a></li>
                <li><a href="#">Used Instruments</a></li>
            </ul>
            
            <div id="enquireForm" class="sideForm">
            	<h3>Your enquiry</h3>
                <form action="" method="get">
                	<ul>
                    	<li><label>Name*</label> <input name="name" type="text" /></li>
                        <li><label>Company</label> <input name="company" type="text" /></li>
                        <li><label>Email*</label> <input name="email" type="text" /></li>
                        <li><label>Phone</label> <input name="phone" type="text" /></li>
                        <li><label>Message</label> <textarea name="" cols="" rows=""></textarea></li>
                        <li><input name="productName" type="hidden" value="" /></li>
                        <li class="send"><input name="" type="submit" value="Send Enquiry" class="largeBtn" /> <a href="#" class="hideForm readMore">Hide Form</a></li>
                    </ul>
                </form>
            </div>
        </div>
        
        <div id="copy" class="productDetail featured subPage">
          
            <div class="inner">
            
            <ul class="breadCrumb">
            	<li><a href="#">Home</a></li>
                <li><a href="#">Products</a></li>
                <li><a href="#">UAV</a></li>
                <li class="current"><a href="#">BRAMOR Orthophoto</a></li>
            </ul>
            
            <span class="note">* Please note all prices exclude GST</span>
            
           
           
            <div class="topProductInformation">
            	 <div class="top">
                	<h1>UAV</h1>
            		<span class="price">$45,000 <small>NZD</small></span>
               	</div>
               
               <div class="options">
               		<a href="#" class="enquire largeBtn">Enquire about this product</a>
                	<a href="#" class="rent largeBtn">Rent this product</a>
               </div>
                
              
               
               <div class="main">
               
               <h2>BRAMOR Orthophoto</h2>
               		<p><img src="/images/temp/UAV-feature-sub.jpg" alt="UAV"  class="alignCenter" /></p>
               
					 <p>The BRAMOR ORTHOPHOTO UA (unmanned aerial) SYSTEM, ideally suited for remote sensing applications is comprised of a blended wing body modular airframe, with the emphasis on the smallest possible T/O weight, advanced aerodynamics, electric propulsion, completely autonomous operation, endurance, turbulent air penetration and stability, ergonomic and user friendly ground control station, durability and mobility. The airframe is manufactured with CAD/CAM technology from advanced composite materials (Kevlar/Carbon/Vectran) that provide a high level of survivability. A high visibility color scheme and strobe and NAV lights are optional. Embedded autonomous flight procedures include an array of failsafe options based on man-rated standards.</p>

<p>The system consists of the air vehicle with a 25 megapixel DSLR E/O sensor, a foldable portable take off ramp and rugged ground control station. 
The whole system fits into one rugged transport case. The system is flight ready in under five minutes. The system is safely operated by one operator/pilot in command.</p>

	<hr />
	<p><img src="/images/temp/UAV-feature-sub-2.jpg" class="alignLeft" /></p>   
    
    <h3>Lorem Ipsum dolor sit</h3>
    
    <p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>

<p>Solutpat metus, nec auctor mi metus eu nibh. Integer id nibh natis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malsapien, et vulputate libero. Praesent interdum risus a libero consequat dapibus. Nulla facilisi. Aliquam erat nisi, placerat id laoreet quis, fringilla ac dolor. Nam interdum tristique augue.</p>

<p>Nullam accumsan suscipit magna, non vestibulum tortor eleifend eget. Fusce mollis venenatis tortor eget pellentesque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. Pellentesque orci velit, sagittis non pellentesque eu, congue sed.</p>     

<hr />

<p><img src="/images/temp/UAV-feature-sub-3.jpg" class="alignRight" /></p>   
    
    <h3>Lorem Ipsum dolor sit</h3>
    
    <p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed et tellus enim. Maecenas rhoncus cursus ligula, nec elementum tellus volutpat imperdiet. Pellentesque vitae sapien mauris. Etiam ac ipsum a libero pharetra scelerisque. Aenean at neque et felis rhoncus volutpat pharetra et antebi tristique senectus et netus et malesuada fames ac turpis egestas. Pellentesque congue feugiat velit, auctor scelerisque lacus tempor vel. </p>

<p>Pellentesque orci velit, sagittis non pellentesque eu, congue sed.. Integer sapien augue, aliquet eu volutpat et, suscipit quis nibh. Nulla facilisi.Praesent tincidunt, nulla non consectetur dapibus, nisi odio.</p>
 

               </div>
               
           
            
            </div>
        	</div>
            
           <div class="additionalBlocks">
                	<ul>
                    	<li>
                        	<h3><a href="#">BRAMOR Othophoto</a></h3>
                       		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultrices, sem nec suscipit sodales, diam lacus tempus elit, sed pretium purus nulla ac neque. Nullam nec elementum arcu. Lorem ipsum dolor sit amet</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                        <li>
                        	<h3><a href="#">Applications</a></h3>
                       		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultrices, sem nec suscipit sodales, diam lacus tempus elit, sed pretium purus nulla ac neque. Nullam nec elementum arcu. Lorem ipsum dolor sit amet</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                        <li class="third">
                        	<h3><a href="#">News</a></h3>
                       		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultrices, sem nec suscipit sodales, diam lacus tempus elit, sed pretium purus nulla ac neque. Nullam nec elementum arcu. Lorem ipsum dolor sit amet</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                        <li>
                        	<h3><a href="#">BRAMOR Othophoto</a></h3>
                       		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultrices, sem nec suscipit sodales, diam lacus tempus elit, sed pretium purus nulla ac neque. Nullam nec elementum arcu. Lorem ipsum dolor sit amet</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                        <li>
                        	<h3><a href="#">Applications</a></h3>
                       		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultrices, sem nec suscipit sodales, diam lacus tempus elit, sed pretium purus nulla ac neque. Nullam nec elementum arcu. Lorem ipsum dolor sit amet</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                        <li class="third">
                        	<h3><a href="#">News</a></h3>
                       		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultrices, sem nec suscipit sodales, diam lacus tempus elit, sed pretium purus nulla ac neque. Nullam nec elementum arcu. Lorem ipsum dolor sit amet</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                        <li>
                        	<h3><a href="#">BRAMOR Othophoto</a></h3>
                       		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultrices, sem nec suscipit sodales, diam lacus tempus elit, sed pretium purus nulla ac neque. Nullam nec elementum arcu. Lorem ipsum dolor sit amet</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                        <li>
                        	<h3><a href="#">Applications</a></h3>
                       		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultrices, sem nec suscipit sodales, diam lacus tempus elit, sed pretium purus nulla ac neque. Nullam nec elementum arcu. Lorem ipsum dolor sit amet</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                      <li class="third">
                        	<h3><a href="#">News</a></h3>
                       		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultrices, sem nec suscipit sodales, diam lacus tempus elit, sed pretium purus nulla ac neque. Nullam nec elementum arcu. Lorem ipsum dolor sit amet</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                    </ul>
                    <div class="clear"></div>
                </div>
            
        </div>
        <div class="clear"></div>
    </div>
</div>

<? include(dirname(__FILE__) . '/templates/footer.php'); ?>            

<? include(dirname(__FILE__) . '/templates/header.php'); ?>
<div id="homeBanner">
<span class="megaCover"></span>
        <ul>
            <li>
                <div class="cta"><h1>Services.</h1>
                <h2>Marketing Message Here</h2>
                <a href="#" class="largeBtn"><span>Read more about our Services</span></a></div>
                <img src="/images/home-banners/services.jpg" alt="Services - Marketing Message Here" />
            </li>
            <li>
                <div class="cta"><h1>Trusted Precision and Control.</h1>
                <h2>Marketing Message Here</h2>
                <a href="#" class="largeBtn"><span>Browse, Enquire &amp; Hire from our Product range</span></a></div>
                <img src="/images/home-banners/products.jpg" alt="Products - Marketing Message Here" />
            </li>
            <li>
                <div class="cta"><h1>Synergy Positioning Systems.</h1>
                <h2>Marketing Message Here</h2>
                <a href="#" class="largeBtn"><span>Read more about Synergy</span></a></div>
                <img src="/images/home-banners/consultancy.jpg" alt="Consultancy - Marketing Message Here" />
            </li>
        </ul>
    </div>
    
    <div id="homeTabs" class="left">
        <ul class="middle">
            <li class="services"><a href="#"><h3>Services.</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Qui sque ultric es placerat interdum. Praesent malesuada risus et justo sollicitudin eu pulvinar metus blandit. Aliquam est nisi.</p>
                <span class="readMore">read More about our services</span></a><span class="currentArrow"></span>
            </li>
            <li class="products"><a href="#"><h3>Products.</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Qui sque ultric es placerat interdum. Praesent malesuada risus et justo sollicitudin eu pulvinar metus blandit. Aliquam est nisi.</p>
                <span class="readMore">Browse, enquire &amp; Hire from our product range</span></a><span class="currentArrow"></span>
            </li>
            <li class="about"><a href="#"><h3>About Us.</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Qui sque ultric es placerat interdum. Praesent malesuada risus et justo sollicitudin eu pulvinar metus blandit. Aliquam est nisi.</p>
                <span class="readMore">Read more about synergy positioning systems</span></a><span class="currentArrow"></span>
            </li>
        </ul>
    </div>
<div id="content">
    <div id="homeBlock">
    	<ul class="middle">
        	<li class="contact nz">
            	<h3>Contact Us</h3>
                <h4>New Zealand</h4>
				<span class="subHead">- Synergy Positioning Systems Ltd</span>

				<ul>
                	<li><strong>Free Call:</strong>  0800-867-266</li>
                    <li><strong>P:</strong>  +64-9-476-5151</li>
                    <li><strong>F:</strong>  +64-9-476-5140</li>
                    <li><strong>E:</strong>  <a href="mailto:info@synergypositioning.co.nz">info@synergypositioning.co.nz</a></li>
               </ul>
				
                <a href="#" class="readMore">Full contact details</a>
            </li>
            
            <li class="news">
            	<div class="postImage">
                	<span class="cover"></span>
                	<h3>News</h3>
                    <img src="/images/temp/home-news.jpg" alt="Temp News Image" />
                </div>
                
                <div class="postExcerpt">
                <h4>Topcon magnet released</h4>
                
                <p>The Topcon Group announces a unique cloud-based enterprise solution and cloud-enabled family of software applications – Magnet™ – for all precise positioning projects. The new software solution makes...</p>
            	<a href="#" class="readMore">Read full article</a>
                </div>
            </li>
            
            <li class="caseStudies">
            	<div class="postImage">
                	<span class="cover"></span>
                	<h3>Case Studies</h3>
                      <img src="/images/temp/home-case-studies.jpg" alt="Temp Case Studies Image" />
                </div>
                
                <div class="postExcerpt">
                <h4>BCNZ Construction Ltd</h4>
                
                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque ultrices placerat interdum. Praesent malesuada risus et justo sollicitudin eu pulvinar metus blandit. Aliquam est nisi, convallis eu congu”</p>
            	<a href="#" class="readMore">Read full case study</a>
                </div>
            </li>
        
        </ul>
    </div>
    

</div>

<? include(dirname(__FILE__) . '/templates/footer.php'); ?>            
// place any jQuery/helper plugins in here, instead of separate, slower script files.
jQuery(document).ready(function($){
	
	
	// Home Page Large Banner Area 
	$('#homeBanner').cycle({
		fx: 'fade',
		delay: 3000,
		slideExpr: 'li',
		speed: 1000,
		timeout: 12000,
		 pager:  '#homeTabs ul',
		  pagerEvent: 'mouseover', 
    	pagerAnchorBuilder: function(idx, slide) { 
        return '#homeTabs ul li:eq(' + idx + ') a'; 
    } 
	});
	
	// Category Listings Feature Slider
	$('#featureSlider').after('<div class="featureSliderNav"></div>').cycle({
		fx: 'fade',
		delay: 3000,
		slideExpr: 'li',
		speed: 1000,
		timeout: 12000,
		pager:  '.featureSliderNav',
		pagerEvent: 'mouseover', 
    	pagerAnchorBuilder: function(index, el) {
        return '<a href="#"> </a>'; 
    }
	});
	
	// Product Detail Image Gallery
	
	$('#productImageGallery').after('<span id="slidePagerNext"></span><span id="slidePagerPrev" class="disabled"></span><div id="slidePager"><ul id="productImageGalleryNav"></ul></div>').cycle({
		fx: 'fade',
		delay: 9999999,
		speed: 1000,
		timeout: 12000,
		pagerEvent: 'mouseover', 
    	pager:  '#productImageGalleryNav', 
		pagerAnchorBuilder: function(idx, slide) { 
		
		var s=$(slide);
		var imgsource=s.find('img').attr('src');
	   	var new_src = imgsource.substring(0,imgsource.length-4);
	   
		new_src += imgsource.substring(imgsource.length-4,imgsource.length);      
			return '<li><a href="#" style="background-image:url('+ new_src +');"></a></li>'; 
		} ,
    	onPrevNextEvent: function(dir, id, el) {
        if (dir === true) {
		if (id >= 3) { //replace '3' with your number of visible elements
			$('#slidePagerNext').click();
		}
        }
        else {
		if (id >= 1) {
			$('#slidePagerPrev').click();
		}
        }
    }
});

$("#slidePager").jCarouselLite({
    btnNext: "#slidePagerNext",
    btnPrev: "#slidePagerPrev",
    circular: false,
    visible: 3
});
	
	
	
	 
	
    
    // give every first and last li a selector as IE doesn't support the CSS3 selectors
    $("ul li:first-child").addClass("firstChild");
    $("ul li:last-child").addClass("lastChild");
    

    // PIE using JS
    // Include all elements that need PIE below.
    $(function() {
        if (window.PIE) {
            $('.example, #example').each(function() {
                PIE.attach(this);
            });
        }
    });

        
    // IE6 li hover fix
    if($('#menu')) {
        var sfEls = document.getElementById("menu").getElementsByTagName("li");
        for (var i=0; i<sfEls.length; i++)
        {
            sfEls[i].onmouseover=function()
            {
                this.className+=" sfhover";
            }
            sfEls[i].onmouseout=function()
            {
                this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
            }
        }
    }
    

	
$('li.services').hover(
       function(){ $('#homeTabs').addClass('left') },
       function(){ $('#homeTabs').removeClass('left') }
)

$('li.about').hover(
       function(){ $('#homeTabs').addClass('right') },
       function(){ $('#homeTabs').removeClass('right') }
)


// Mega Menu Display



$("#menu li").hover(function() {
    $(this).children("div").fadeIn("slow");
    },function(){
    $(this).children("div").fadeOut("slow");
});

$('#menu li').hover(
       function(){ $('.megaCover').fadeIn('fast') },
       function(){ $('.megaCover').fadeOut('fast') }
)

$('#menu li').hover(
       function(){ $('#content.std').animate({'opacity':'0.4'}, 100) },
       function(){ $('#content.std').animate({'opacity':'1.0'}, 100) }
)



// Sidebar Enquire, Rent Form


$("a.enquire").click(function() {
  $("div.sideForm").fadeToggle("slow", "linear");
});

$("a.hideForm").click(function() {
  $("div.sideForm").fadeToggle("slow", "linear");
});

	
// Product Detail Tabs

				$(function () {
                        var tabContainers = $('div.tabs > div.block');
                        tabContainers.hide().filter(':first').show();
                        
                        $('div.tabs ul.tabsNav a').click(function () {
                                tabContainers.hide();
                                tabContainers.filter(this.hash).show();
                                $('div.tabs ul.tabsNav a').removeClass('current');
                                $(this).addClass('current');
                                return false;
                        }).filter(':first').click();
                });	
	
	
// Product Detail Top Options

$('.options a.enquire').hover(
       function(){ $('.options a.rent').animate({'opacity':'0.4'}, 100) },
       function(){ $('.options a.rent').animate({'opacity':'1.0'}, 100) }
)	

$('.options a.rent').hover(
       function(){ $('.options a.enquire').animate({'opacity':'0.4'}, 100) },
       function(){ $('.options a.enquire').animate({'opacity':'1.0'}, 100) }
)	
	
	
	
// Product Detail Read More / Read Less 

	// override default options (also overrides global overrides)
    $('.description').expander({
        slicePoint:       1000,  
        expandPrefix:     ' ', 
        expandText:       'Read more', 
        collapseTimer:    0, 
        userCollapseText: 'Read less',  
		expandEffect: 'fadeIn',
		expandSpeed: 250,
		collapseEffect: 'fadeOut',
    });
	  
	
	//Management team page profile pop up
	
			
	//add class to a tag with no image in it to be able to remove in lightbox
	$('ul.profileList li a').not(':has(img)').addClass('viewProfile');
	
	
	$('ul.profileList li').click(function(e){
	    e.preventDefault()
	    var thisLi = $(this)
	    thisLi.addClass('current').siblings().removeClass()
	    $.fancybox({
    		'content'		: thisLi.clone(),
    		'autoDimensions': false,
    		'width'         : 800,
			'padding'		: 20,
			'overlayColor'	: '#ffffff',
    		'height'        : 'auto',
    		'transitionIn'	: 'elastic',
    		'transitionOut'	: 'elastic'
	    });
	});
		
	

	
    /* Synergy Auckland MAP */
 
 
    var map;
    var infowindow = false;
    var initialLocation = new google.maps.LatLng(-36.865116, 174.737624);
    var mapOptions = {
        zoom: 16,
        center: initialLocation,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        mapTypeControl:false
    };
    
    function initiateMap() {
        map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
        // Custom marker icon
        var image = new google.maps.MarkerImage('/images/map-pin.png',
            // This marker is 20 pixels wide by 32 pixels tall.
            new google.maps.Size(63, 70),
            // The origin for this image is 0,0.
            new google.maps.Point(0,0),
            // The anchor for this image is the base of the flagpole at 0,32.
            new google.maps.Point(34, 70)
            );
        var shadow = new google.maps.MarkerImage('/images/map-pin-shadow.png',
            // The shadow image is larger in the horizontal dimension
            // while the position and offset are the same as for the main image.
            new google.maps.Size(31, 14),
            new google.maps.Point(0,0),
            new google.maps.Point(16, 6)
            );
        var close_image = '/images/close-map.png';
        var marker = new google.maps.Marker({
            position: initialLocation,
            map: map,
            icon: image,
            shadow: shadow
        });

    }
    
   
    
    
	
	
	
});

// Fixes Ipad Portrait/Landscape Scale issue
    
if (navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)) {
    var viewportmeta = document.querySelector('meta[name="viewport"]');
    if (viewportmeta) {
        viewportmeta.content = 'width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0';
        document.body.addEventListener('gesturestart', function () {
            viewportmeta.content = 'width=device-width, minimum-scale=0.25, maximum-scale=1.6';
        }, false);
    }
}

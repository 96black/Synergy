
<? include(dirname(__FILE__) . '/templates/header.php'); ?>
<div id="content" class="std">
	<div class="middle">
        <div id="sideBar">
            <h3>About us</h3>
            <ul id="sideMenu">
                <li><a href="#">Management</a> </li>
                <li><a href="#">History</a></li>
                <li><a href="#">Vision</a> </li>
                <li><a href="#">Case Studies</a> </li>
                
            </ul>
            
           
        </div>
        
        <div id="copy" class="about">
          	<div class="topPageImage">
            	<img src="/images/temp/about-temp.jpg" alt="About Synergy Positioning Systems" />
            </div>
          
            <div class="inner">
            
            <ul class="breadCrumb">
            	<li><a href="#">Home</a></li>
                <li class="current"><a href="#">About Us</a></li>
            
            </ul>
            
            <div class="main">
           		<h1>About Us</h1>
                
                <p class="intro">Synergy Positioning Systems Ltd is 100% New Zealand owned and operated. It was formed when two of the industry’s most trusted companies merged in 2006 - Synergy Equipment and Systems Ltd and Geodetic Instruments Ltd. The result is a company that brings you over 30 years experience delivering the most advanced and innovative technologies combined with a commitment to outstanding customer service.</p>

<p>Our aim is to make your job faster and easier, more accurate and cost efficient. We'll do this by giving you expert and honest advice and by offering you the best solution from our range of innovative positioning systems.</p>

<p>Reliability is paramount - equipment breakages cost time and money. There are many low quality brands on the market that just don't stand up to the rigours of our construction industry. We only sell reliable brands we can trust and service.</p>

<p>Our experienced team are always on hand for after sales support, product training and servicing, giving you the confidence and skills to get the best out of your equipment. Our product range is extensive and always growing.</p>

<h2>Why choose synergy?</h2>


         	</div>
            </div>
         	
              <div class="additionalBlocks">
                	<ul>
                    	<li>
                        	<h3>Simple really....</h3>
                       		<p>You are dealing with an established company representing world renowned brands that have been tried and tested in increasing productivity and reducing costs.</p>
                          
						</li>
                        <li>
                        	<h3>Your profit...</h3>
                       		<p>With our equipment and systems, you gain efficiency &amp; reduce costs, saving you money on all aspects of the job including: fuel, wages, wear &amp; tear, materials &amp; processing. </p>
                            
						</li>
                        <li>
                        	<h3>Our Products</h3>
                       		<p>We only represent equipment &amp; brands that we trust. Topcon, world leaders in positioning equipment for over 80 years &amp; well established throughout the world, offers reliable &amp; robust equipment for any application.</p>
                           
						</li>
                        <li>
                        	<h3>Our Experience</h3>
                       		<p>Synergy Positioning Systems Ltd is 100% New Zealand owned and operated. Our industry exprience spans more than 30 years. </p>
                            
						</li>
                        <li>
                        	<h3>Our Service</h3>
                       		<p>We are commited to our customers. Your success is our reward. Our factory trained technicians and in-house industry experts ensure that what we sell is fully supported.</p>
                           
						</li>
                    </ul>
                    <div class="clear"></div>
             </div>
            
          
            
        </div>
        <div class="clear"></div>
    </div>
</div>

<? include(dirname(__FILE__) . '/templates/footer.php'); ?>            
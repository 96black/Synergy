
            </div>
<? include(dirname(__FILE__) . '/templates/header.php'); ?>
<div id="content" class="std contact">
	<div class="middle">
        <div id="sideBar">
            
            <div id="enquireForm" class="sideForm">
            	<h3>Send us a message</h3>
                <form action="" method="get">
                	<ul>
                    	<li><label>Name*</label> <input name="name" type="text" /></li>
                        <li><label>Company</label> <input name="company" type="text" /></li>
                        <li><label>Email*</label> <input name="email" type="text" /></li>
                        <li><label>Phone</label> <input name="phone" type="text" /></li>
                        <li><label>Message</label> <textarea name="" cols="" rows=""></textarea></li>
                        <li><input name="productName" type="hidden" value="" /></li>
                        <li class="send"><input name="" type="submit" value="Send Enquiry" class="largeBtn" /></li>
                    </ul>
                </form>
            </div>
           
        </div>
        
        <div id="copy" class="contact">
          	<div class="topMap" id="auckland">
            	  <div class="map" style="width:100%; height:305px;">
                 	<div id="map_canvas"></div>
                  </div>
            </div>
            
            
          
            <div class="inner">
            
                <ul class="breadCrumb">
                    <li><a href="#">Home</a></li>
                    <li class="current"><a href="#">Contact Us</a></li>
                
                </ul>
            
                <div class="main">
                    <h1>Contact Us</h1>
                    
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quam justo, egestas sed tincidunt id, feugiat eu urna. Suspendisse ornare, magna cursus tristique ullamcorper, massa velit ultricies metus, eu ultrices massa eros nec tortor. Sed auctor vehicula quam, ac condimentum mauris tristique non. Sed sed sodales nisi. Aliquam erat volutpat. Vivamus sit amet felis et urna lacinia ullamcorper eget et metus. Nunc laoreet eleifend molestie.</p>
                </div>
            
            </div>
         	
            <div class="location auckland current">
                	<h2><a href="#">Auckland</a></h2>
                    <ul class="first">
                    	<li><strong>Free Call:</strong> <span>0800-867-266</span></li>
                        <li><strong>Phone:</strong> <span>+64-9-476-5151</span></li>
                        <li><strong>Fax:</strong> <span>+63-9-476-5140</span></li>
                        <li><strong>Email:</strong> <span><a href="mailto:info@synergypositioning.co.nz">info@synergypositioning.co.nz</a></span></li>
                    </ul>
                    
                    <ul class="second">
                    	<li><strong>Physical Address:</strong> <span> 3/52 Arrenway Drive <br /> Albany <br /> Auckland <br /> New Zealand</span></li>
                        <li><strong>Operating Hours:</strong> <span>8:00am to 5:00pm</span></li>
                   	</ul>
                    
                    <ul class="third">
                    	<li><strong>Postal Address:</strong> <span>P O Box 100450 <br /> NSMC <br /> Auckland <br /> New Zealand <br /> 0745</span></li>
                    </ul>
                    <div class="clear"></div>
                </div>
                
                <div class="location christchurch">
                	<h2><a href="#">Christchurch</a></h2>
                    <ul class="first">
                    	<li><strong>Free Call:</strong> <span>0800-867-266</span></li>
                        <li><strong>Phone:</strong> <span>+64-9-476-5151</span></li>
                        <li><strong>Fax:</strong> <span>+63-9-476-5140</span></li>
                        <li><strong>Email:</strong> <span><a href="mailto:info@synergypositioning.co.nz">info@synergypositioning.co.nz</a></span></li>
                    </ul>
                    
                    <ul class="second">
                    	<li><strong>Physical Address:</strong> <span> 3/52 Arrenway Drive <br /> Albany <br /> Auckland <br /> New Zealand</span></li>
                        <li><strong>Operating Hours:</strong> <span>8:00am to 5:00pm</span></li>
                   	</ul>
                    
                    <ul class="third">
                    	<li><strong>Postal Address:</strong> <span>P O Box 100450 <br /> NSMC <br /> Auckland <br /> New Zealand <br /> 0745</span></li>
                    </ul>
                    <div class="clear"></div>
                </div>
                
                <div class="location ams">
                <h2>Aerial Mapping Services</h2>
                
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quam justo, egestas sed tincidunt id, feugiat eu urna. Suspendisse ornare, magna cursus tristique ullamcorper, massa velit ultricies metus, eu ultrices massa eros nec tortor. Sed auctor vehicula quam, ac condimentum mauris tristique non. Sed sed sodales nisi. Aliquam erat volutpat. Vivamus sit amet felis et urna lacinia ullamcorper eget et metus. Nunc laoreet eleifend molestie. </p>
                
                <ul>
                	<li><strong>Email:</strong> <span><a href="mailto:sales@synergyus.com">sales@synergyus.com</a></span></li>
                </ul>
                <div class="clear"></div>
                </div>
             
        </div>
        <div class="clear"></div>
    </div>
</div>

<? include(dirname(__FILE__) . '/templates/footer.php'); ?>            
<!doctype html>
<!--[if IE 6]>    <html class="no-js lt-ie6" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if IE 9]>    <html class="no-js ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]--><head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1">
    
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="SKYPE_TOOLBAR" CONTENT="SKYPE_TOOLBAR_PARSER_COMPATIBLE">

        <!-- Dynamic noindex/index meta tag to hide non-live sites from Google etc. -->
        <? if (preg_match('/dev\.96black\.co\.nz/', $_SERVER['HTTP_HOST']) || preg_match('/\.ahcdigitaldev\.com/', $_SERVER['HTTP_HOST'])): ?>
            <meta name="robots" content="noindex, nofollow" /> 
        <? else: ?>
            <meta name="robots" content="index, follow" /> 
        <? endif; ?>

        <link rel="shortcut icon" href="/images/favicon.ico">
        <link rel="apple-touch-icon" href="/images/apple-touch-icon.png">
        <!--[if lte IE 9]><link rel="stylesheet" href="css/ie-grid.css" type="text/css" media="screen" /><![endif]-->
        <link rel="stylesheet" href="/css/main.css?v=2017"  type="text/css" media="screen" />
        <link rel="stylesheet" href="/css/print.css?v=2017" type="text/css" media="print" />

        <script src="/js/libs/modernizr-2.5.3.min.js"></script>

    </head>

    <body>
        <div id="wrapper">
            <header>
            <div class="middle">
                <a id="logo" href="/"><img src="/images/synergy-positioning-systems-logo.gif" alt="Synergy Positioning Systems" width="243" height="75" /></a>
                <nav id="menu">
                    <ul>                                                                                         
                        <li class="current"><a href="#">Home</a></li>
                        <li class="services"><a href="#">Services</a>
                        	<div class="megaMenu">
                            	<div class="featuredProducts">
                                	<h3>Skynet</h3>
                                    <ul>
                                    	<li>
                                       	<div class="image"><img src="/images/temp/mega-menu-feature-product-image.jpg" alt="C-Astral - Product Title"></div>
                                            <div class="details">
                                            	<div class="logo"><img src="/images/temp/mega-menu-feature-product-logo.jpg" alt="C-Astral - Product Title"></div>
                                                <a href="#" class="readMore">View Product</a>
                                            </div>
                                       </li>
                                    </ul>
                                </div>
                                <div class="menuCategories">
                                	<h3>View our services below</h3>
                                    <ul class="first">
                                    	<li><a href="#">Roads &amp; Surfaces</a>
                                       		<ul class="subMenu">
                                            	<li><a href="#">Survey &amp; Design</a></li>
                                                <li><a href="#">Paving Control</a></li>
                                                <li><a href="#">Additional Information</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <ul class="second">
                                    	<li><a href="#">Mapping</a>
                                        	<ul class="subMenu">
                                            	<li><a href="#">Lorem ipsum</a></li>
                                                <li><a href="#">Lorem Ipsum Dolor</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <ul class="third">
                                    	<li><a href="#">Lorem Ipsum</a>
                                        	<ul class="subMenu">
                                            	<li><a href="#">Lorem ipsum</a></li>
                                                <li><a href="#">Lorem Ipsum Dolor</a></li>
                                                <li><a href="#">Lorem ipsum</a></li>
                                                <li><a href="#">Lorem Ipsum Dolor</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <ul class="fourth">
                                    	<li><a href="#">Lorem Ipsum Dolor Sir</a>
                                        	<ul class="subMenu">
                                            	<li><a href="#">Lorem ipsum</a></li>
                                                <li><a href="#">Lorem Ipsum Dolor</a></li>
                                                <li><a href="#">Lorem ipsum</a></li>
                                                <li><a href="#">Lorem Ipsum Dolor</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                   
                                </div>
                            </div>
                        </li>
                        <li class="products"><a href="#">Products</a>
                        	<div class="megaMenu">
                            	<div class="featuredProducts">
                                	<h3>Featured Products</h3>
                                    <ul>
                                    	<li>
                                        	<div class="image"><img src="/images/temp/mega-menu-feature-product-image.jpg" alt="C-Astral - Product Title"></div>
                                            <div class="details">
                                            	<div class="logo"><img src="/images/temp/mega-menu-feature-product-logo.jpg" alt="C-Astral - Product Title"></div>
                                                <a href="#" class="readMore">View Product</a>
                                            </div>
                                       </li>
                                       <li>
                                        	<div class="image"><img src="/images/temp/mega-menu-feature-product-image.jpg" alt="C-Astral - Product Title"></div>
                                            <div class="details">
                                            	<div class="logo"><img src="/images/temp/mega-menu-feature-product-logo.jpg" alt="C-Astral - Product Title"></div>
                                                <a href="#" class="readMore">View Product</a>
                                            </div>
                                       </li>
                                       <li>
                                        	<div class="image"><img src="/images/temp/mega-menu-feature-product-image.jpg" alt="C-Astral - Product Title"></div>
                                            <div class="details">
                                            	<div class="logo"><img src="/images/temp/mega-menu-feature-product-logo.jpg" alt="C-Astral - Product Title"></div>
                                                <a href="#" class="readMore">View Product</a>
                                            </div>
                                       </li>
                                    </ul>
                                </div>
                                <div class="menuCategories">
                                	<h3>Browse, enquire &amp; hire from our product range below</h3>
                                    <ul class="first">
                                    	<li><a href="#">UAV</a></li>
                                    </ul>
                                    <ul class="second">
                                    	<li><a href="#">Faro</a></li>
                                    </ul>
                                    <ul class="third">
                                    	<li><a href="#">Optical</a>
                                        	<ul class="subMenu">
                                            	<li><a href="#">Total Station</a></li>
                                                <li><a href="#">Levels</a></li>
                                                <li><a href="#">Theodolites</a></li>
                                                <li><a href="#">Binoculars</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <ul class="fourth">
                                    	<li><a href="#">GPS Equipment</a>
                                        	<ul class="subMenu">
                                            	<li><a href="#">Geodetic Receivers</a></li>
                                                <li><a href="#">Mapping &amp GIS</a></li>
                                                <li><a href="#">GPS Accessories</a></li>
                                                <li><a href="#">Radio Range Extenders &amp; Repeaters</a></li>
                                                <li><a href="#">Geodetic Antennas</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    
                                    <ul class="first">
                                    	<li><a href="#">Machine Control</a>
                                        	<ul class="subMenu">
                                            	<li><a href="#">3D Systems</a></li>
                                                <li><a href="#">Automatic Systems</a></li>
                                                <li><a href="#">Indicate Systems</a></li>
                                                <li><a href="#">Add-On Equipment</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <ul class="second">
                                    	<li><a href="#">Lasers</a>
                                        	<ul class="subMenu">
                                            	<li><a href="#">Construction Lasers</a></li>
                                                <li><a href="#">Grade lasers</a></li>
                                                <li><a href="#">Interior Lasers</a></li>
                                                <li><a href="#">Pipe Lasers</a></li>
                                                <li><a href="#">Lasers Accessories</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <ul class="third">
                                    	<li><a href="#">Hand-Held Devices</a></li>
                                    </ul>
                                    <ul class="fourth">
                                    	<li><a href="#">Survey Accessories</a>
                                        	<ul class="subMenu">
                                            	<li><a href="#">Tripods</a></li>
                                                <li><a href="#">Prisims and holders</a></li>
                                                <li><a href="#">Tribrachs &amp Adapters</a></li>
                                                <li><a href="#">Leveling Staves &amp; Range Poles</a></li>
                                                <li><a href="#">Calculators</a></li>
                                                <li><a href="#">Mapping</a></li>
                                                <li><a href="#">Two Way Radios</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    
                                    <ul class="first">
                                    	<li><a href="#">Software</a>
                                        	<ul class="subMenu">
                                            	<li><a href="#">CAD</a></li>
                                                <li><a href="#">Survey - Field Software</a></li>
                                                <li><a href="#">Survey - Office Software</a></li>
                                                <li><a href="#">Mapping &amp; GIS</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <ul class="second">
                                    	<li><a href="#">Measuring Instruments</a>
                                        	<ul class="subMenu">
                                            	<li><a href="#">Measuring Wheels</a></li>
                                                <li><a href="#">Laser Tape Measurers</a></li>
                                                <li><a href="#">Digital Spirit Level</a></li>
                                                <li><a href="#">Measuring Tapes</a></li>
                                                <li><a href="#">Moisture Meters</a></li>
                                                <li><a href="#">Range Finders</a></li>
                                                <li><a href="#">Protractors</a></li>
                                                <li><a href="#">Magnetic Locators</a></li>
                                                <li><a href="#">Thermometer</a></li>
                                                <li><a href="#">Height Poles</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    <ul class="third">
                                    	<li><a href="#">Protective Cases</a></li>
                                    </ul>
                                    <ul class="fourth">
                                    	<li><a href="#">Ground Testing Equipment</a>
                                        	<ul class="subMenu">
                                            	<li><a href="#">Penetrometer</a></li>
                                                <li><a href="#">Impact Tester</a></li>
                                                <li><a href="#">Shear Vane</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                    
                                    <ul class="first">
                                    	<li><a href="#">Industrial Lighting</a></li>
                                    </ul>
                                    <ul class="second">
                                    	<li><a href="#">Telematics</a></li>
                                    </ul>
                                    <ul class="third">
                                    	<li><a href="#">Aerial Mapping</a></li>
                                    </ul>
                                    <ul class="fourth">
                                    	<li><a href="#">Used Instruments</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                        <li><a href="#">News</a></li>
                        <li><a href="#">About Us </a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </nav>
                
                <div class="search">
                	<form action="" method="get">
                    	<input name="" class="text" type="text" placeholder="Type your search here...">
                        <input name="" type="submit" value="" class="submit">
                    </form>
                </div>

                <div class="contact">
                    <ul>
                        <li class="phone">0800-867-266</li>
                        <li class="email"><a href="mailto:info@96black.co.nz" title="info@96black.co.nz">Email Us</a></li>
                    </ul>
                </div>
            </div>
            </header>
<footer>
<div class="middle">
	<a id="footerLogo" href="/"><img src="/images/footer-synergy-positioning-systems-logo.gif" alt="Synergy Positioning Systems" width="146" height="44" /></a>
    
    <div class="socialMedia">
    	<h3>Follow us on Twitter</h3>
        
    </div>
    
    <div class="mailingList">
    	<h3>Join mailing list for news &amp; Updates</h3>
        <form action="" method="get">
        	<input name="" type="text" class="text" placeholder="Your Name" />
            <input name="" type="text" class="text" placeholder="Your Email Address" />
            <input name="" type="submit" class="submit" value="Join" />
        </form>
    </div>
    
    <nav id="menu">
        <ul>                                                                                         
            <li class="current"><a href="#">Home</a></li>
            <li><a href="#">Services</a>
                <ul class="subMenu">
                     <li class="current"><a href="#">Roads and Surfaces</a></li>
                     <li><a href="#">Mapping</a></li>
                </ul>
            </li>
            <li><a href="#">Products</a>
            	<ul class="subMenu">
                	 <li><a href="#">UAV</a></li>
                     <li><a href="#">Faro</a></li>
                     <li><a href="#">Optical</a></li>
                     <li><a href="#">GPS Equipment</a></li>
                     <li><a href="#">Machine Control</a></li>
                     <li><a href="#">Lasers</a></li>
                     <li><a href="#">Hand-held Devices</a></li>
                     <li><a href="#">Survey Accessories</a></li>
                     <li><a href="#"> Software</a></li>
                     <li><a href="#">Measuring Instruments</a></li>
                     <li><a href="#">Protective Cases</a></li>
                     <li><a href="#">Ground Testing Equipment</a></li>
                     <li><a href="#">Industrial Lightning</a></li>
                     <li><a href="#">Telematics</a></li>
                     <li><a href="#">Aerial Mapping</a></li>
                </ul>
            </li>
            <li><a href="#">News</a></li>
            <li><a href="#">About Us </a>
                <ul class="subMenu">
                    <li><a href="#">Management</a></li>
                    <li><a href="#">History</a></li>
                    <li><a href="#">Vision</a></li>
                    <li><a href="#">Case Studies</a></li>
                </ul>
            </li>
            <li><a href="#">Contact Us</a>
                <ul class="subMenu">
                        <li><a href="#">Auckland</a></li>
                        <li><a href="#">Christchurch</a></li>
                </ul>
            </li>
        </ul>
    </nav>
    </div>
    
    <div class="base">
        <div class="middle">
            <ul class="logos nz">
                <li class="first"><a href="#" title="FARO"><img src="/images/logos/footer-faro.gif" alt="FARO" /></a></li>
                <li class="second"><a href="#" title="Topcon"><img src="/images/logos/footer-topcon.gif" alt="Topcon" /></a></li>
                <li class="third"><a href="#" title="MOBA"><img src="/images/logos/footer-moba.gif" alt="Moba" /></a></li>
                <li class="fourth"><a href="#" title="C-Astral"><img src="/images/logos/footer-c-astral.gif" alt="C-Astral" /></a></li>
                <li class="fifth"><a href="#" title="Gatewing"><img src="/images/logos/footer-gatewing.gif" alt="Gatewing" /></a></li>
            </ul>
        </div>    
    
        <div class="smallPrint">
              <div class="middle">  
                    <span class="copyright">&copy; 2012 Synergy Positioning Systems Ltd. All rights reserved.</span>
                    <span class="websiteDesign"><a href="http://www.96black.co.nz" target="_blank">Website Design</a> by 96black</span>
              </div>
        </div>  
    </div>	


</footer>
</div>


<!--[if IE 6]> 
<div class="upgradeNotice">
        <h2>Upgrade your browser!</h2>
    <p>You will need to either download the latest version of <a href="http://www.microsoft.com/en-us/download/details.aspx?id=13950" target="_blabk">Internet Explorer</a> or download one of our prefered browsers which we have listed below to view our website.</p>
        <ul>
        <li><a href="https://www.google.com/chrome" target="_blank">- Chrome</a></li>
        <li><a href="http://www.apple.com/safari/download/" target="_blank">- Safari</a></li>
        <li><a href="http://www.mozilla.org/en-US/firefox/new/" target="_blank">- Firefox</a></li>
   </ul>
</div>
<![endif]-->


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/js/libs/jquery-1.7.1.min.js"><\/script>')</script>

<script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

<script type="text/javascript" src="/js/jquery.cycle.all.js"></script>
<script type="text/javascript" src="/js/jcarousellite_1.0.1.js"></script>
<script type="text/javascript" src="/js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
<script type="text/javascript" src="/js/fancybox/jquery.mousewheel-3.0.4.pack.js"></script>
<script type="text/javascript" src="/js/fancybox/jquery.easing-1.3.pack.js"></script>
<script type="text/javascript" src="/js/jquery.expander.js"></script>
<script src="/js/custom.js?v=2017"></script>

<script>
    var _gaq=[['_setAccount','UA-xxxxxx-xx'],['_trackPageview']];
    (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
        g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
        s.parentNode.insertBefore(g,s)}(document,'script'));
</script>

</body>
</html>
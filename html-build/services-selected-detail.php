
<? include(dirname(__FILE__) . '/templates/header.php'); ?>
<div id="content" class="std">
	<div class="middle">
        <div id="sideBar">
            <h3>Services</h3>
            <ul id="sideMenu">
                <li class="current"><a href="#">Roads &amp; Services</a>
                	  <ul class="subMenu">
                        <li class="current"><a href="#">Survey &amp; Design</a></li>
                        <li><a href="#">Paving Control</a></li>
                        <li><a href="#">Additional Information</a></li>
                    </ul>
                </li>
                <li><a href="#">Mapping</a></li>
                <li><a href="#">Lorem Ipsum</a> </li>
                <li><a href="#">Lorem Ipsum</a> </li>
                
            </ul>
            
           
        </div>
        
        <div id="copy" class="services servicesDetail">
          	<div class="topPageImage">
            	<img src="/images/temp/services-temp-top.jpg" alt="Roads &amp; Services" />
            </div>
          
            <div class="inner">
            
                <ul class="breadCrumb">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Roads &amp; Services</a></li>
                    <li class="current"><a href="#">Survey &amp; Design</a></li>
                
                </ul>
            
                <div class="main">
                <h1>Survey &amp; Design</h1>
                <p class="intro">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus.</p>
         	
            	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper neque, vitae luctus nibh volutpat et. Sed ornare dignissim aliquet. Aliquam eleifend enim eleifend lacus pretium ac pretium risus sodales. Aliquam mi nisi, suscipit in ornare in, tempus a justo. Vivamus porta, justo in sollicitudin vulputate, velit massa congue mi, a vehicula enim metus a lectus. Sed felis tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>
                
                <hr />
            
            	<p><img src="/images/temp/services-temp-detail-1.jpg" alt="Survey &amp; Design" class="alignLeft" /></p>
                
                <h3>Lorem Ipsum Dolor</h3>
            	
            	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullam]is tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>
                
                 <h3>Pellentesque mattis ultrices dapibu uis ullamcorp</h3>
                 <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit m]is tortor, pellentesque eu tempor vel, sollicitudin a ipsum. Cras eget purus felis.</p>
               
                 
                 <hr />
                 <iframe class="alignRight" width="417" height="267" src="http://www.youtube.com/embed/39zYdKe6vLs" frameborder="0" allowfullscreen></iframe>
                  <h3>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus.</h3>
                 
       <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque mattis ultrices dapibus. Duis ullamcorper dolor id orci volutpat quis pellentesque ligula placerat. Aenean a leo id tellus semper semper et non lectus. Praesent lobortis, ipsum eu sodales aliquam, tortor lorem condimentum tellus, sit amet aliquet nisl nulla quis felis. Quisque pretium ullamcorper</p>          
                </div>
        	</div>
            
               <div class="additionalBlocks">
                	<ul>
                    	<li>
                        	<h3><a href="#">Survey &amp; Design</a></h3>
                       		<p>Synergy Positioning uses an in-house survey team with many years of experience in the asphalt and roading industries. Our in-house experts in CAD, manages the design and control of final surfacing levels to ensure a smooth and cost effective result, on time and within budget</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                        <li>
                        	<h3><a href="#">Paving Control</a></h3>
                       		<p>Our paving managers are the best in the industry. Taking full control of the paving crew, we manage the project from start to finish.</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
                        <li class="third">
                        	<h3><a href="#">Additional Information</a></h3>
                       		<p>Sed sagittis tincidunt lobortis. In non ligula a urna blandit tempor. Aenean a leo ut mauris posuere aliquet. Phasellus faucibus convallis aliquet. Proin velit mi, tempus quis varius eu, suscipit sit amet nisl.</p>
                            <a href="#" class="readMore">Read More</a>
						</li>
					</ul>
                    <div class="clear"></div>
              </div>
          
            
        </div>
        <div class="clear"></div>
    </div>
</div>

<? include(dirname(__FILE__) . '/templates/footer.php'); ?>            
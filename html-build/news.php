
<? include(dirname(__FILE__) . '/templates/header.php'); ?>
<div id="content" class="std">
	<div class="middle">
        <div id="sideBar">
            <h3>News</h3>
            
            
           
        </div>
        
        <div id="copy" class="news">
          
            <div class="inner">
            
            <ul class="breadCrumb">
            	<li><a href="#">Home</a></li>
                <li class="current"><a href="#">News</a></li>
            
            </ul>
            
            <div class="main">
           		<h1>News</h1>
         	</div>
            </div>
         	<ul class="newsList">
            	<li class="first"><img src="/images/temp/news-temp-thumb.jpg" alt="Topcon Magnet Released" />
                	<h2><a href="#">Topcon Magnet Released</a></h2>
                    <div class="excerpt"><p>Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere...</p></div>
            		<a href="#" class="readMore">Read More</a>
                </li>
                <li class="second"><img src="/images/temp/news-temp-thumb.jpg" alt="Topcon Magnet Released" />
                	<h2><a href="#">Topcon Magnet Released Topcon Magnet Released</a></h2>
                    <div class="excerpt"><p>Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere...</p></div>
            		<a href="#" class="readMore">Read More</a>
                </li>
                <li class="first"><img src="/images/temp/news-temp-thumb.jpg" alt="Topcon Magnet Released" />
                	<h2><a href="#">Topcon Magnet Released</a></h2>
                    <div class="excerpt"><p>Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere...</p></div>
            		<a href="#" class="readMore">Read More</a>
                </li>
                <li class="second"><img src="/images/temp/news-temp-thumb.jpg" alt="Topcon Magnet Released" />
                	<h2><a href="#">Topcon Magnet ReleasedTopcon Magnet Released Topcon Magnet Released</a></h2>
                    <div class="excerpt"><p>Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere...</p></div>
            		<a href="#" class="readMore">Read More</a>
                </li>
                <li class="first"><img src="/images/temp/news-temp-thumb.jpg" alt="Topcon Magnet Released" />
                	<h2><a href="#">Topcon Magnet Released Topcon Magnet ReleaTopcon Magnet Released Topcon Magnet Releasedsed Topcon Magnet Released</a></h2>
                    <div class="excerpt"><p>Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere...</p></div>
            		<a href="#" class="readMore">Read More</a>
                </li>
                <li class="second"><img src="/images/temp/news-temp-thumb.jpg" alt="Topcon Magnet Released" />
                	<h2><a href="#">Topcon Magnet Released</a></h2>
                    <div class="excerpt"><p>Vivamus tristique aliquet elit in feugiat. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Curabitur cursus elementum interdum. Nam felis orci, posuere...</p></div>
            		<a href="#" class="readMore">Read More</a>
                </li>
            </ul>	
            
            <div class="pagination">
            	 <p>&nbsp;<strong>1</strong>&nbsp;<a href="#">2</a>&nbsp;<a href="#">3</a>&nbsp;<a href="#">4</a>&nbsp;<a href="#">5</a>&nbsp;<a href="#">6</a>&nbsp;<a href="#">&gt;</a>&nbsp;</p>
            </ul>
            
          
            
        </div>
        <div class="clear"></div>
    </div>
</div>

<? include(dirname(__FILE__) . '/templates/footer.php'); ?>            
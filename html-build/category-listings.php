
<? include(dirname(__FILE__) . '/templates/header.php'); ?>
<div id="content" class="std">
	<div class="middle">
        <div id="sideBar">
            <h3>Products</h3>
            <ul id="sideMenu">
                <li><a href="#">UAV</a></li>
                <li><a href="#">Faro</a></li>
                <li class="current"><a href="#">Optical</a>
                    <ul class="subMenu">
                        <li class="current"><a href="#">Total Station</a>
                        	 <ul class="subMenu">
                                <li><a href="#">Robotic</a></li>
                                <li><a href="#">Windows Based</a></li>
                                <li class="current"><a href="#">Construction</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Levels</a></li>
                        <li><a href="#">Theodolites</a></li>
                        <li><a href="#">Binoculars</a></li>
                    </ul>
                </li>
                <li><a href="#">GPS Equipment</a>
                    <ul class="subMenu">
                        <li><a href="#">Geodetic Receivers</a></li>
                        <li><a href="#">Mapping &amp GIS</a></li>
                        <li><a href="#">GPS Accessories</a></li>
                        <li><a href="#">Radio Range Extenders &amp; Repeaters</a></li>
                        <li><a href="#">Geodetic Antennas</a></li>
                    </ul>
                </li>
                <li><a href="#">Machine Control</a>
                    <ul class="subMenu">
                        <li><a href="#">3D Systems</a></li>
                        <li><a href="#">Automatic Systems</a></li>
                        <li><a href="#">Indicate Systems</a></li>
                        <li><a href="#">Add-On Equipment</a></li>
                    </ul>
                </li>
                <li><a href="#">Lasers</a>
                    <ul class="subMenu">
                        <li><a href="#">Construction Lasers</a></li>
                        <li><a href="#">Grade lasers</a></li>
                        <li><a href="#">Interior Lasers</a></li>
                        <li><a href="#">Pipe Lasers</a></li>
                        <li><a href="#">Lasers Accessories</a></li>
                    </ul>
                </li>
                <li><a href="#">Hand-Held Devices</a></li>
                <li><a href="#">Survey Accessories</a>
                    <ul class="subMenu">
                        <li><a href="#">Tripods</a></li>
                        <li><a href="#">Prisims and holders</a></li>
                        <li><a href="#">Tribrachs &amp Adapters</a></li>
                        <li><a href="#">Leveling Staves &amp; Range Poles</a></li>
                        <li><a href="#">Calculators</a></li>
                        <li><a href="#">Mapping</a></li>
                        <li><a href="#">Two Way Radios</a></li>
                    </ul>
                </li>
                <li><a href="#">Software</a>
                    <ul class="subMenu">
                        <li><a href="#">CAD</a></li>
                        <li><a href="#">Survey - Field Software</a></li>
                        <li><a href="#">Survey - Office Software</a></li>
                        <li><a href="#">Mapping &amp; GIS</a></li>
                    </ul>
                </li>
                <li><a href="#">Measuring Instruments</a>
                    <ul class="subMenu">
                        <li><a href="#">Measuring Wheels</a></li>
                        <li><a href="#">Laser Tape Measurers</a></li>
                        <li><a href="#">Digital Spirit Level</a></li>
                        <li><a href="#">Measuring Tapes</a></li>
                        <li><a href="#">Moisture Meters</a></li>
                        <li><a href="#">Range Finders</a></li>
                        <li><a href="#">Protractors</a></li>
                        <li><a href="#">Magnetic Locators</a></li>
                        <li><a href="#">Thermometer</a></li>
                        <li><a href="#">Height Poles</a></li>
                    </ul>
                </li>
                <li><a href="#">Protective Cases</a></li>
                <li><a href="#">Ground Testing Equipment</a>
                    <ul class="subMenu">
                        <li><a href="#">Penetrometer</a></li>
                        <li><a href="#">Impact Tester</a></li>
                        <li><a href="#">Shear Vane</a></li>
                    </ul>
                </li>
                <li><a href="#">Industrial Lighting</a></li>
                <li><a href="#">Telematics</a></li>
                <li><a href="#">Aerial Mapping</a></li>
                <li><a href="#">Used Instruments</a></li>
            </ul>
        </div>
        
        <div id="copy" class="categoryListing">
        	<ul id="featureSlider">
            	<li><a href="#"><img src="/images/temp/cat-feature-slide1.jpg" alt="Feature Product 1" /></a></li>
                <li><a href="#"><img src="/images/temp/cat-feature-slide2.jpg" alt="Feature Product 2" /></a></li>
                <li><a href="#"><img src="/images/temp/cat-feature-slide3.jpg" alt="Feature Product 3" /></a></li>
            </ul>
            
            <div class="inner">
            
            <ul class="breadCrumb">
            	<li><a href="#">Home</a></li>
                <li class="current"><a href="#">Products</a></li>
            </ul>
            
            <h1>Products</h1>
            
            <ul class="listings">
            	<li>
                	<h2><a href="#">Optical</a></h2>
                    <div class="image"><a href="#" class="imageContainer"></a><img src="/images/temp/cat-image.jpg" alt="Optical" /></div>
                    <div class="excerpt">
                    	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget turpis id metus lacinia malesuada vitae a magna. Aliquam eget dolor a sem porta fermentum non in dui. Nunc tempor nibh vitae augue elementum feugiat. </p>
                        <a href="#" class="viewProducts">View Products</a>
                    </div>
                </li>
                <li>
                	<h2><a href="#">GPS Equipment</a></h2>
                  	  <div class="image"><a href="#" class="imageContainer"></a><img src="/images/temp/cat-image.jpg" alt="Optical" /></div>
                    <div class="excerpt">
                    	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget turpis id metus lacinia malesuada vitae a magna. Aliquam eget dolor a sem porta fermentum non in dui. Nunc tempor nibh vitae augue elementum feugiat. </p>
                        <a href="#" class="viewProducts">View Products</a>
                    </div>
                </li>
                <li>
                	<h2><a href="#">Optical</a></h2>
                    <div class="image"><a href="#" class="imageContainer"></a><img src="/images/temp/cat-image.jpg" alt="Optical" /></div>
                    <div class="excerpt">
                    	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget turpis id metus lacinia malesuada vitae a magna. Aliquam eget dolor a sem porta fermentum non in dui. Nunc tempor nibh vitae augue elementum feugiat. </p>
                        <a href="#" class="viewProducts">View Products</a>
                    </div>
                </li>
                <li>
                	<h2><a href="#">GPS Equipment</a></h2>
                  	  <div class="image"><a href="#" class="imageContainer"></a><img src="/images/temp/cat-image.jpg" alt="Optical" /></div>
                    <div class="excerpt">
                    	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget turpis id metus lacinia malesuada vitae a magna. Aliquam eget dolor a sem porta fermentum non in dui. Nunc tempor nibh vitae augue elementum feugiat. </p>
                        <a href="#" class="viewProducts">View Products</a>
                    </div>
                </li>
                <li>
                	<h2><a href="#">Optical</a></h2>
                    <div class="image"><a href="#" class="imageContainer"></a><img src="/images/temp/cat-image.jpg" alt="Optical" /></div>
                    <div class="excerpt">
                    	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget turpis id metus lacinia malesuada vitae a magna. Aliquam eget dolor a sem porta fermentum non in dui. Nunc tempor nibh vitae augue elementum feugiat. </p>
                        <a href="#" class="viewProducts">View Products</a>
                    </div>
                </li>
                <li>
                	<h2><a href="#">GPS Equipment</a></h2>
                  	  <div class="image"><a href="#" class="imageContainer"></a><img src="/images/temp/cat-image.jpg" alt="Optical" /></div>
                    <div class="excerpt">
                    	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget turpis id metus lacinia malesuada vitae a magna. Aliquam eget dolor a sem porta fermentum non in dui. Nunc tempor nibh vitae augue elementum feugiat. </p>
                        <a href="#" class="viewProducts">View Products</a>
                    </div>
                </li>
                <li>
                	<h2><a href="#">Optical</a></h2>
                    <div class="image"><a href="#" class="imageContainer"></a><img src="/images/temp/cat-image.jpg" alt="Optical" /></div>
                    <div class="excerpt">
                    	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget turpis id metus lacinia malesuada vitae a magna. Aliquam eget dolor a sem porta fermentum non in dui. Nunc tempor nibh vitae augue elementum feugiat. </p>
                        <a href="#" class="viewProducts">View Products</a>
                    </div>
                </li>
                <li>
                	<h2><a href="#">GPS Equipment</a></h2>
                  	  <div class="image"><a href="#" class="imageContainer"></a><img src="/images/temp/cat-image.jpg" alt="Optical" /></div>
                    <div class="excerpt">
                    	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget turpis id metus lacinia malesuada vitae a magna. Aliquam eget dolor a sem porta fermentum non in dui. Nunc tempor nibh vitae augue elementum feugiat. </p>
                        <a href="#" class="viewProducts">View Products</a>
                    </div>
                </li>
                <li>
                	<h2><a href="#">Optical</a></h2>
                    <div class="image"><a href="#" class="imageContainer"></a><img src="/images/temp/cat-image.jpg" alt="Optical" /></div>
                    <div class="excerpt">
                    	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget turpis id metus lacinia malesuada vitae a magna. Aliquam eget dolor a sem porta fermentum non in dui. Nunc tempor nibh vitae augue elementum feugiat. </p>
                        <a href="#" class="viewProducts">View Products</a>
                    </div>
                </li>
                <li>
                	<h2><a href="#">GPS Equipment</a></h2>
                  	  <div class="image"><a href="#" class="imageContainer"></a><img src="/images/temp/cat-image.jpg" alt="Optical" /></div>
                    <div class="excerpt">
                    	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget turpis id metus lacinia malesuada vitae a magna. Aliquam eget dolor a sem porta fermentum non in dui. Nunc tempor nibh vitae augue elementum feugiat. </p>
                        <a href="#" class="viewProducts">View Products</a>
                    </div>
                </li>
                <li>
                	<h2><a href="#">Optical</a></h2>
                    <div class="image"><a href="#" class="imageContainer"></a><img src="/images/temp/cat-image.jpg" alt="Optical" /></div>
                    <div class="excerpt">
                    	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget turpis id metus lacinia malesuada vitae a magna. Aliquam eget dolor a sem porta fermentum non in dui. Nunc tempor nibh vitae augue elementum feugiat. </p>
                        <a href="#" class="viewProducts">View Products</a>
                    </div>
                </li>
                <li>
                	<h2><a href="#">GPS Equipment</a></h2>
                  	  <div class="image"><a href="#" class="imageContainer"></a><img src="/images/temp/cat-image.jpg" alt="Optical" /></div>
                    <div class="excerpt">
                    	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer eget turpis id metus lacinia malesuada vitae a magna. Aliquam eget dolor a sem porta fermentum non in dui. Nunc tempor nibh vitae augue elementum feugiat. </p>
                        <a href="#" class="viewProducts">View Products</a>
                    </div>
                </li>
            </ul>
        	</div>
        </div>
        <div class="clear"></div>
    </div>
</div>

<? include(dirname(__FILE__) . '/templates/footer.php'); ?>            